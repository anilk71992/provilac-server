package com.vishwakarma.provilac.utils;

import org.junit.Test;

import junit.framework.Assert;

public class StringHelperTest {

	@Test
	public void testArrayToCsv() {
		String[] elems = {"one", "two", "three"};
		Assert.assertEquals("one,two,three", StringHelper.arrayToCsv(elems));	
		Assert.assertEquals("", StringHelper.arrayToCsv(null));
		Assert.assertEquals("one", StringHelper.arrayToCsv(new String[] {"one"}));
	}
}
