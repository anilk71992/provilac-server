
DROP TABLE IF EXISTS `subscriptionrequest`;

CREATE TABLE `subscriptionrequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `startDate` datetime NOT NULL,
  `permanantNote` varchar(255) DEFAULT NULL,
  `internalNote` varchar(255) DEFAULT NULL,
  `isCurrent` tinyint(1) DEFAULT '0',
  `isSubscriptionRequest` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_USER` (`userId`),
  CONSTRAINT `FK_USER` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `requestlineitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `currentIndex` int(11) NOT NULL,
  `customPattern` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  `subscriptionRequestId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_SUBSCRIPTION_REQUEST` (`subscriptionRequestId`),
  KEY `FK_PRODUCT` (`productId`),
  CONSTRAINT `FK_PRODUCT` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_SUBSCRIPTION_REQUEST` FOREIGN KEY (`subscriptionRequestId`) REFERENCES `subscriptionrequest` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;