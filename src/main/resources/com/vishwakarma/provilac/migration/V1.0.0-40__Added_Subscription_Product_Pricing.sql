CREATE TABLE `usersubscription_productpricing` (
  `UserSubscription_id` bigint(20) NOT NULL,
  `productPricing` double DEFAULT NULL,
  `productPricing_KEY` bigint(20) NOT NULL,
  PRIMARY KEY (`UserSubscription_id`,`productPricing_KEY`),
  KEY `FK1491D44ECA205762` (`UserSubscription_id`),
  CONSTRAINT `FK1491D44ECA205762` FOREIGN KEY (`UserSubscription_id`) REFERENCES `usersubscription` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;