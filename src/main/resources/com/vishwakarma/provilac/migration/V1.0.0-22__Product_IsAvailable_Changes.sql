alter table `Product` add column `isAvailableSubscription` tinyint(1) default '0';

alter table `Product` add column `isAvailableOneTimeOrder` tinyint(1) default '0';