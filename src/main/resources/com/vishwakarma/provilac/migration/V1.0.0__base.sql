# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: provilac-dbinstance.c2cpqf14oods.us-west-2.rds.amazonaws.com (MySQL 5.6.27-log)
# Database: provilac
# Generation Time: 2016-07-21 10:19:47 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table deliverylineitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deliverylineitem`;

CREATE TABLE `deliverylineitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `deliveryScheduleId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKED77155BCF16BA19` (`deliveryScheduleId`),
  KEY `FKED77155BBBC0E3B1` (`productId`),
  CONSTRAINT `FKED77155BBBC0E3B1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FKED77155BCF16BA19` FOREIGN KEY (`deliveryScheduleId`) REFERENCES `deliveryschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table deliveryschedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deliveryschedule`;

CREATE TABLE `deliveryschedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `deliveryTime` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `permanantNote` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `reason` mediumtext,
  `status` varchar(255) DEFAULT NULL,
  `tempararyNote` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `customerId` bigint(20) NOT NULL,
  `deliverySheetId` bigint(20) DEFAULT NULL,
  `invoiceId` bigint(20) DEFAULT NULL,
  `routeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK7D0922CB566B7DE5` (`routeId`),
  KEY `FK7D0922CB3EC94FCC` (`customerId`),
  KEY `FK7D0922CB1A0D38AD` (`invoiceId`),
  KEY `FK7D0922CB37A10489` (`deliverySheetId`),
  CONSTRAINT `FK7D0922CB1A0D38AD` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`id`),
  CONSTRAINT `FK7D0922CB37A10489` FOREIGN KEY (`deliverySheetId`) REFERENCES `deliverysheet` (`id`),
  CONSTRAINT `FK7D0922CB3EC94FCC` FOREIGN KEY (`customerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK7D0922CB566B7DE5` FOREIGN KEY (`routeId`) REFERENCES `route` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table deliverysheet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deliverysheet`;

CREATE TABLE `deliverysheet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `isCompleted` tinyint(1) NOT NULL,
  `userRouteId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK1BA3D9AB578B96AF` (`userRouteId`),
  CONSTRAINT `FK1BA3D9AB578B96AF` FOREIGN KEY (`userRouteId`) REFERENCES `userroute` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `fromDate` date DEFAULT NULL,
  `isPaid` tinyint(1) DEFAULT '0',
  `lastPendingDues` double NOT NULL,
  `provilacOutStanding` double NOT NULL,
  `toDate` date DEFAULT NULL,
  `totalAmount` double NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `collectionBoyNote` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKD80EDB0D3EC94FCC` (`customerId`),
  CONSTRAINT `FKD80EDB0D3EC94FCC` FOREIGN KEY (`customerId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table messagelog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messagelog`;

CREATE TABLE `messagelog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `sendTimestamp` datetime DEFAULT NULL,
  `senderUserId` bigint(20) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `adjustmentAmount` double NOT NULL,
  `amount` double NOT NULL,
  `bankName` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `isBounced` tinyint(1) DEFAULT '0',
  `paymentMethod` varchar(255) DEFAULT NULL,
  `receiptNo` varchar(255) DEFAULT NULL,
  `txnId` varchar(255) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `invoiceId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK3454C9E63EC94FCC` (`customerId`),
  KEY `FK3454C9E61A0D38AD` (`invoiceId`),
  KEY `FK3454C9E66B337479` (`userId`),
  CONSTRAINT `FK3454C9E61A0D38AD` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`id`),
  CONSTRAINT `FK3454C9E63EC94FCC` FOREIGN KEY (`customerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK3454C9E66B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table picklistitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `picklistitem`;

CREATE TABLE `picklistitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `displayValue` varchar(255) DEFAULT NULL,
  `itemValue` varchar(255) DEFAULT NULL,
  `listType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table privilege
# ------------------------------------------------------------

DROP TABLE IF EXISTS `privilege`;

CREATE TABLE `privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table privileges_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `privileges_roles`;

CREATE TABLE `privileges_roles` (
  `roleId` bigint(20) NOT NULL,
  `privilegeId` bigint(20) NOT NULL,
  PRIMARY KEY (`privilegeId`,`roleId`),
  KEY `FK7C99342017BA90B5` (`privilegeId`),
  KEY `FK7C99342065DE1F0F` (`roleId`),
  CONSTRAINT `FK7C99342017BA90B5` FOREIGN KEY (`privilegeId`) REFERENCES `privilege` (`id`),
  CONSTRAINT `FK7C99342065DE1F0F` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `colorCode` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK50C664CFDFFD121F` (`categoryId`),
  CONSTRAINT `FK50C664CFDFFD121F` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table route
# ------------------------------------------------------------

DROP TABLE IF EXISTS `route`;

CREATE TABLE `route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table routerecord
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routerecord`;

CREATE TABLE `routerecord` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `routeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKE2E917DA566B7DE5` (`routeId`),
  KEY `FKE2E917DA6B337479` (`userId`),
  CONSTRAINT `FKE2E917DA566B7DE5` FOREIGN KEY (`routeId`) REFERENCES `route` (`id`),
  CONSTRAINT `FKE2E917DA6B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table servicecredit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicecredit`;

CREATE TABLE `servicecredit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `credits` int(11) NOT NULL,
  `serviceName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table subscriptionlineitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptionlineitem`;

CREATE TABLE `subscriptionlineitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `currentIndex` int(11) NOT NULL,
  `customPattern` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  `userSubscriptionId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK4B220984F8B38A93` (`userSubscriptionId`),
  KEY `FK4B220984BBC0E3B1` (`productId`),
  CONSTRAINT `FK4B220984BBC0E3B1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FK4B220984F8B38A93` FOREIGN KEY (`userSubscriptionId`) REFERENCES `usersubscription` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table summarysheet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `summarysheet`;

CREATE TABLE `summarysheet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `fromDate` datetime DEFAULT NULL,
  `toDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table summarysheetrecord
# ------------------------------------------------------------

DROP TABLE IF EXISTS `summarysheetrecord`;

CREATE TABLE `summarysheetrecord` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `adjustment` double NOT NULL,
  `balanceAmount` double NOT NULL,
  `chequeDate` datetime DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `paymentMethod` varchar(255) DEFAULT NULL,
  `previousMonth` double NOT NULL,
  `receiptNumber` varchar(255) DEFAULT NULL,
  `receivedDate` datetime DEFAULT NULL,
  `totalAmount` double NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `invoiceId` bigint(20) DEFAULT NULL,
  `receivedById` bigint(20) DEFAULT NULL,
  `summarySheetId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKDDA08B2AB02D0C35` (`summarySheetId`),
  KEY `FKDDA08B2A6F48B366` (`receivedById`),
  KEY `FKDDA08B2A3EC94FCC` (`customerId`),
  KEY `FKDDA08B2A1A0D38AD` (`invoiceId`),
  CONSTRAINT `FKDDA08B2A1A0D38AD` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`id`),
  CONSTRAINT `FKDDA08B2A3EC94FCC` FOREIGN KEY (`customerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FKDDA08B2A6F48B366` FOREIGN KEY (`receivedById`) REFERENCES `user` (`id`),
  CONSTRAINT `FKDDA08B2AB02D0C35` FOREIGN KEY (`summarySheetId`) REFERENCES `summarysheet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table systemproperty
# ------------------------------------------------------------

DROP TABLE IF EXISTS `systemproperty`;

CREATE TABLE `systemproperty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `propName` varchar(255) NOT NULL,
  `propValue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `alterNateMobileNumber` varchar(255) DEFAULT NULL,
  `buildingAddress` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `consumptionQty` double DEFAULT '0',
  `dob` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fbId` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `gPlusId` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `isEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `isRouteAssigned` tinyint(1) DEFAULT '0',
  `isVerified` tinyint(1) NOT NULL DEFAULT '0',
  `joiningDate` datetime DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `lastPendingDues` double DEFAULT '0',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `mobileNumber` varchar(255) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `otherBrand` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `paymentMethod` varchar(255) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `reasonToStop` varchar(255) DEFAULT NULL,
  `referredMedia` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `twitterId` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `verificationCode` varchar(255) DEFAULT NULL,
  `assignedTo` bigint(20) DEFAULT NULL,
  `referredBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `mobileNumber` (`mobileNumber`),
  KEY `FK285FEBF0480A7B` (`referredBy`),
  KEY `FK285FEBDA30231C` (`assignedTo`),
  CONSTRAINT `FK285FEBDA30231C` FOREIGN KEY (`assignedTo`) REFERENCES `user` (`id`),
  CONSTRAINT `FK285FEBF0480A7B` FOREIGN KEY (`referredBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table userrole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userrole`;

CREATE TABLE `userrole` (
  `userId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `FKF3F7670165DE1F0F` (`roleId`),
  KEY `FKF3F767016B337479` (`userId`),
  CONSTRAINT `FKF3F7670165DE1F0F` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`),
  CONSTRAINT `FKF3F767016B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table userroute
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userroute`;

CREATE TABLE `userroute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `collectionBoyId` bigint(20) DEFAULT NULL,
  `deliveryBoyId` bigint(20) NOT NULL,
  `routeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK8AF59D1E566B7DE5` (`routeId`),
  KEY `FK8AF59D1EDC664E5C` (`collectionBoyId`),
  KEY `FK8AF59D1E89E3C6C6` (`deliveryBoyId`),
  CONSTRAINT `FK8AF59D1E566B7DE5` FOREIGN KEY (`routeId`) REFERENCES `route` (`id`),
  CONSTRAINT `FK8AF59D1E89E3C6C6` FOREIGN KEY (`deliveryBoyId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK8AF59D1EDC664E5C` FOREIGN KEY (`collectionBoyId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table usersubscription
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usersubscription`;

CREATE TABLE `usersubscription` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `isCurrent` tinyint(1) NOT NULL,
  `permanantNote` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKB3FA84A86B337479` (`userId`),
  CONSTRAINT `FKB3FA84A86B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
