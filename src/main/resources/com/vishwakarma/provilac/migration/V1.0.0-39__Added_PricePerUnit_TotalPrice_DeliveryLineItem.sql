alter table `deliverylineitem` 
	add column `pricePerUnit` double default null,
	add column `totalPrice` double default null;