alter table `Payment` drop column `collectionId`;
alter table `Payment` drop column `depositeId`;
alter table `Payment` drop column `isDeposite`;

alter table `Payment` add column `hasDeposited` tinyint(1) default '0';
alter table `Payment` add column `isVerified` tinyint(1) default '0';
alter table `Payment` add column `collectionId` bigint(20) default null;
alter table `Payment` add column `depositId` bigint(20) default null;
alter table `Payment` add column `pointsRedemed` int(11) default '0';
alter table `Payment` add column `pointsAmount` double default '0.0';

alter table `Payment` add constraint `FK_PAYMENT_COLLECTION` foreign key(`collectionId`) references `Collection`(`id`);
alter table `Payment` add constraint `FK_PAYMENT_DEPOSIT` foreign key(`depositId`) references `Deposit`(`id`);