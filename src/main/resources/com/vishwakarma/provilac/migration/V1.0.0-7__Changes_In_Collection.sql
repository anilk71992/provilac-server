alter table `Collection` drop column `noOfCheque`;
alter table `Collection` add column `noOfCheques` int(11) default 0;
alter table `Collection` add column `routeId` bigint(20) not null;
alter table `Collection` add constraint `FK_COLLECTION_ROUTE` foreign key(`routeId`) references `Route`(`id`);