alter table `activitylog` add column `changeStatus` varchar(255) DEFAULT NULL;

alter table `activitylog` add column `miscellaneous` varchar(255) DEFAULT NULL;