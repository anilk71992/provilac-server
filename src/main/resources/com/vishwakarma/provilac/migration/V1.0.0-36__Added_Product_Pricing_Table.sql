CREATE TABLE `product_pricingmap` (
  `Product_id` bigint(20) NOT NULL,
  `pricingMap` double DEFAULT NULL,
  `pricingMap_KEY` int(11) NOT NULL,
  PRIMARY KEY (`Product_id`,`pricingMap_KEY`),
  KEY `FK2600592662EF53D2` (`Product_id`),
  CONSTRAINT `FK2600592662EF53D2` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;