alter table `usersubscription` 
	add column `subscriptionType` varchar(255) default 'Postpaid',
	add column `endDate` date default null;