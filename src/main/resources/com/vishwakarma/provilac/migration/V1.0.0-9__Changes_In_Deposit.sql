alter table `Deposit` drop column `noOfCheque`;
alter table `Deposit` add column `noOfCheques` int(11) default 0;
alter table `Deposit` add column `cashTransactionId` varchar(255) default null;