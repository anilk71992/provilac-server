alter table `Product` add column `productCartPicUrl` varchar(255) default null;
alter table `Product` add column `productListPicUrl` varchar(255) default null;
alter table `Product` add column `productDetailsPicUrl` varchar(255) default null;