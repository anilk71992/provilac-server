alter table `Product` add column `tax` double default 0;
alter table `Product` add column `description` mediumtext default null;
alter table `Product` add column `productPicUrl` varchar(255) default null;