alter table `UserSubscription` add column `txnId` varchar(255) default null,
								add column `totalAmount` double default '0.0' not null,
								add column `pointsDiscount` double default '0.0' not null,
								add column `finalAmount` double default '0.0' not null,
								add column `pointsRedemed` double default '0.0' not null,
								add column `paymentGateway` varchar(255) default null,
								add column `isApprovedByPG` tinyint(1) default '1' not null;