# Dump of table Collection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Collection`;

CREATE TABLE `Collection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `cashAmount` double NOT NULL,
  `chequeAmount` double NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `noOfCheque` int(11) DEFAULT NULL,
  `totalAmount` double NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKF078ABE6B337479` (`userId`),
  CONSTRAINT `FKF078ABE6B337479` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Collection_Payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Collection_Payment`;

CREATE TABLE `Collection_Payment` (
  `Collection_id` bigint(20) NOT NULL,
  `payments_id` bigint(20) NOT NULL,
  UNIQUE KEY `payments_id` (`payments_id`),
  KEY `FKC78C5D65823E65EB` (`payments_id`),
  KEY `FKC78C5D65D58AD582` (`Collection_id`),
  CONSTRAINT `FKC78C5D65823E65EB` FOREIGN KEY (`payments_id`) REFERENCES `Payment` (`id`),
  CONSTRAINT `FKC78C5D65D58AD582` FOREIGN KEY (`Collection_id`) REFERENCES `Collection` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Deposit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Deposit`;

CREATE TABLE `Deposit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `cashAmount` double NOT NULL,
  `chequeAmount` double NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `noOfCheque` int(11) DEFAULT NULL,
  `totalAmount` double NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKBFDFAE7E6B337479` (`userId`),
  CONSTRAINT `FKBFDFAE7E6B337479` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Deposit_Payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Deposit_Payment`;

CREATE TABLE `Deposit_Payment` (
  `Deposit_id` bigint(20) NOT NULL,
  `payments_id` bigint(20) NOT NULL,
  UNIQUE KEY `payments_id` (`payments_id`),
  KEY `FKAEE4C125823E65EB` (`payments_id`),
  KEY `FKAEE4C12581CE4172` (`Deposit_id`),
  CONSTRAINT `FKAEE4C12581CE4172` FOREIGN KEY (`Deposit_id`) REFERENCES `Deposit` (`id`),
  CONSTRAINT `FKAEE4C125823E65EB` FOREIGN KEY (`payments_id`) REFERENCES `Payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
