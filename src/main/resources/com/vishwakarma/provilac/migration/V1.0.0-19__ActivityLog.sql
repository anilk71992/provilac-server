
DROP TABLE IF EXISTS `activitylog`;

CREATE TABLE IF NOT EXISTS `activitylog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `activity` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `entityId` bigint(20) DEFAULT NULL,
  `entityType` varchar(255) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK2CBCD1956B337479` (`userId`),
  CONSTRAINT `FK2CBCD1956B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1307 DEFAULT CHARSET=latin1;
