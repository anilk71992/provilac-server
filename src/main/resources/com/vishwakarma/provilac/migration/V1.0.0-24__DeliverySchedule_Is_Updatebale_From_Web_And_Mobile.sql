alter table `DeliverySchedule` add column `isUpdatedFromMobile` tinyint(1) default '0';

alter table `DeliverySchedule` add column `isUpdatedFromWeb` tinyint(1) default '0';