--
-- Table structure for table `DeletedRouteRecord`
--

CREATE TABLE `deletedRouteRecord` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `routeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FKFD5DCA41566B7DE5` (`routeId`),
  KEY `FKFD5DCA416B337479` (`userId`),
  CONSTRAINT `FKFD5DCA41566B7DE5` FOREIGN KEY (`routeId`) REFERENCES `Route` (`id`),
  CONSTRAINT `FKFD5DCA416B337479` FOREIGN KEY (`userId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
