# Dump of table provilacimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `provilacimage`;

CREATE TABLE `provilacimage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `PicUrl` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;