alter table `User` add column `allocatedFirstTimeReferralPoints` tinyint(1) default '0';
alter table `User` add column `referralCode` varchar(255) default null;
alter table `User` add column `accumulatedPoints` int(11) default 0;