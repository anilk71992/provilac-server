# Dump of table onetimeorder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimeorder`;

CREATE TABLE `onetimeorder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `deliveryTime` datetime DEFAULT NULL,
  `finalAmount` double NOT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `paymentGateway` varchar(255) DEFAULT NULL,
  `pointsDiscount` double NOT NULL,
  `pointsRedemed` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `totalAmount` double NOT NULL,
  `txnId` varchar(255) NOT NULL,
  `addressId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `deliveryBoyId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK33CF94DB3EC94FCC` (`customerId`),
  KEY `FK33CF94DB89E3C6C6` (`deliveryBoyId`),
  KEY `FK33CF94DBFC9CD47B` (`addressId`),
  CONSTRAINT `FK33CF94DB3EC94FCC` FOREIGN KEY (`customerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK33CF94DB89E3C6C6` FOREIGN KEY (`deliveryBoyId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK33CF94DBFC9CD47B` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table orderlineitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderlineitem`;

CREATE TABLE `orderlineitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `amount` double NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `productId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FKE3027275BBC0E3B1` (`productId`),
  KEY `FKE3027275DF87882C` (`orderId`),
  CONSTRAINT `FKE3027275BBC0E3B1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FKE3027275DF87882C` FOREIGN KEY (`orderId`) REFERENCES `onetimeorder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;