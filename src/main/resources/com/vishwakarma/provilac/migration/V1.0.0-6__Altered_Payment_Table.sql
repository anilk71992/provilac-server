alter table Payment add column `collectionId` bigint(20) DEFAULT NULL;
alter table Payment add column `depositeId` bigint(20) DEFAULT NULL;
alter table Payment add column `isDeposite` tinyint(1) DEFAULT '0';
