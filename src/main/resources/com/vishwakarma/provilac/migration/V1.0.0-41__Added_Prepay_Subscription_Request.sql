CREATE TABLE `prepaysubscriptionrequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `finalAmount` double NOT NULL,
  `internalNote` varchar(255) DEFAULT NULL,
  `isApprovedByPG` tinyint(1) DEFAULT '1',
  `isChangeRequest` tinyint(1) DEFAULT '0',
  `paymentGateway` varchar(255) NOT NULL,
  `permanantNote` varchar(255) DEFAULT NULL,
  `pointsDiscount` double NOT NULL,
  `pointsRedemed` int(11) NOT NULL,
  `startDate` datetime NOT NULL,
  `totalAmount` double NOT NULL,
  `txnId` varchar(255) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK6491132D6B337479` (`userId`),
  CONSTRAINT `FK6491132D6B337479` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `prepaysubscriptionrequest_productpricing` (
  `PrepaySubscriptionRequest_id` bigint(20) NOT NULL,
  `productPricing` double DEFAULT NULL,
  `productPricing_KEY` bigint(20) NOT NULL,
  PRIMARY KEY (`PrepaySubscriptionRequest_id`,`productPricing_KEY`),
  KEY `FK7F15CB2910B498D2` (`PrepaySubscriptionRequest_id`),
  CONSTRAINT `FK7F15CB2910B498D2` FOREIGN KEY (`PrepaySubscriptionRequest_id`) REFERENCES `prepaysubscriptionrequest` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `prepayrequestlineitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `customPattern` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `preapySubscriptionRequestId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FKDF13ED1BBC0E3B1` (`productId`),
  KEY `FKDF13ED1ED5E8EEB` (`preapySubscriptionRequestId`),
  CONSTRAINT `FKDF13ED1BBC0E3B1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FKDF13ED1ED5E8EEB` FOREIGN KEY (`preapySubscriptionRequestId`) REFERENCES `prepaysubscriptionrequest` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;