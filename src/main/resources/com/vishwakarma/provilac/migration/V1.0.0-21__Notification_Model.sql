# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.15)
# Database: provilac_test
# Generation Time: 2017-01-07 14:21:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(255) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `emailSubject` varchar(255) DEFAULT NULL,
  `mailchimpCampaignId` varchar(255) DEFAULT NULL,
  `pushMessage` varchar(255) DEFAULT NULL,
  `pushTitle` varchar(255) DEFAULT NULL,
  `smsMessage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table notification_notificationtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_notificationtypes`;

CREATE TABLE `notification_notificationtypes` (
  `Notification_id` bigint(20) NOT NULL,
  `notificationTypes` varchar(255) DEFAULT NULL,
  KEY `FKC693741AD1759462` (`Notification_id`),
  CONSTRAINT `FKC693741AD1759462` FOREIGN KEY (`Notification_id`) REFERENCES `notification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table notification_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_status`;

CREATE TABLE `notification_status` (
  `Notification_id` bigint(20) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  KEY `FK91C23DA6D1759462` (`Notification_id`),
  CONSTRAINT `FK91C23DA6D1759462` FOREIGN KEY (`Notification_id`) REFERENCES `notification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table notificationroute
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationroute`;

CREATE TABLE `notificationroute` (
  `routeId` bigint(20) NOT NULL,
  `notificationId` bigint(20) NOT NULL,
  PRIMARY KEY (`notificationId`,`routeId`),
  KEY `FK8C5953FE566B7DE5` (`routeId`),
  KEY `FK8C5953FE4FECDEB9` (`notificationId`),
  CONSTRAINT `FK8C5953FE4FECDEB9` FOREIGN KEY (`notificationId`) REFERENCES `notification` (`id`),
  CONSTRAINT `FK8C5953FE566B7DE5` FOREIGN KEY (`routeId`) REFERENCES `route` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
