insert  into `role`(`id`,`description`,`role`,`version`) values (1,'Super Admin Role','ROLE_SUPER_ADMIN',0),
(2,'Admin Role','ROLE_ADMIN',0),
(3,'CCE Role','ROLE_CCE',0),
(4,'Delivery Boy Role','ROLE_DELIVERY_BOY',0),
(5,'Customer Role','ROLE_CUSTOMER',0),
(6,'Collection Boy Role','ROLE_COLLECTION_BOY',0),
(7,'Intern Role','ROLE_INTERN',0),
(8,'Accountant Role','ROLE_ACCOUNTANT',0);

insert into SystemProperty(propname, propvalue) values ('MASTER_DATA_VERSION', '0'), ('DEFAULT_TIMEZONE', 'IST');

/*Data for the table `user` */
insert  into `User`(`id`, `code`,`firstname`, `lastname`, `username`, `gender`, `password`, `version`, `email`, `mobileNumber` ) values
(1,'U-1','Vishal', 'Pawale', 'admin', 1, '21232f297a57a5a743894a0e4a801fc3', 0, 'vishalpawale123@gmail.com', '9923427766' ); 

/*Data for the table `userrole` */
insert  into `userrole`(`userId`,`roleId`) values (1,1);

/* MENU Privileges */
insert into privilege (id, name) value (1, 'PRIV_SUPER_ADMIN');

insert into Privileges_Roles (roleId, privilegeId) values (1,1);provilac