<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><decorator:title default="Welcome" /></title>
<meta name="keyword" content="Pure Cow milk in Pune Cow Milk in Mumbai Desi milk Pune Desi milk Mumbai A2 milk Pune A2 milk Mumbai Home delivery of milk pure milk in Pune pure milk in Mumbai organic milk Indian cow milk Gir cow Jersey cow pride of cows best milk farm fresh milk doorstep delivery pure desi cow" />
<meta name="description" content="Pure Cow milk in Pune Cow Milk in Mumbai Desi milk Pune Desi milk Mumbai A2 milk Pune A2 milk Mumbai Home delivery of milk pure milk in Pune pure milk in Mumbai organic milk Indian cow milk Gir cow Jersey cow pride of cows best milk farm fresh milk doorstep delivery pure desi cow" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-57x57.png'/>" rel="apple-touch-icon" sizes="57x57" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-60x60.png'/>" rel="apple-touch-icon" sizes="60x60" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-72x72.png'/>" rel="apple-touch-icon" sizes="72x72" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-76x76.png'/>" rel="apple-touch-icon" sizes="76x76" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-114x114.png'/>" rel="apple-touch-icon" sizes="114x114" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-120x120.png'/>" rel="apple-touch-icon" sizes="120x120" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-144x144.png'/>" rel="apple-touch-icon" sizes="144x144" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-152x152.png'/>" rel="apple-touch-icon" sizes="152x152" />
<link href="<c:url value='/assets/endUser-assets/favicons/apple-icon-180x180.png'/>" rel="apple-touch-icon" sizes="180x180" />
<link href="<c:url value='/assets/endUser-assets/favicons/android-icon-192x192.png'/>" rel="icon" sizes="192x192" type="image/png" />
<link href="<c:url value='/assets/endUser-assets/favicons/favicon-32x32.png'/>" rel="icon" sizes="32x32" type="image/png" />
<link href="<c:url value='/assets/endUser-assets/favicons/favicon-96x96.png'/>" rel="icon" sizes="96x96" type="image/png" />
<link href="<c:url value='/assets/endUser-assets/favicons/favicon-16x16.png'/>" rel="icon" sizes="16x16" type="image/png" />
<link href="<c:url value='/assets/endUser-assets/favicons/manifest.json'/>" rel="manifest" />
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link href="<c:url value='/assets/endUser-assets/css/bootstrap.min.css'/>" rel="stylesheet" />
<link href="<c:url value='/assets/endUser-assets/css/subscribe-better.css'/>" rel="stylesheet" />
<link href="<c:url value='/assets/endUser-assets/css/fontsGoogleapis.css'/>" rel="stylesheet" />
<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" /> -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<!-- Font Awesome -->
<link href="<c:url value='/assets/endUser-assets/css/fullcalendar.css'/>" rel="stylesheet" />
<link href="<c:url value='/assets/endUser-assets/css/fullcalendar.print.css'/>" rel="stylesheet" media="print"/>
<link href="<c:url value='/assets/endUser-assets/css/main.css'/>" rel="stylesheet" />
<link href="<c:url value='/assets/endUser-assets/css/responsive.css'/>" rel="stylesheet" />
<script src="<c:url value='/assets/endUser-assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js'/>"></script>
<script src="<c:url value='/assets/endUser-assets/js/vendor/jquery-1.11.2.min.js'/>"></script>
<script src="<c:url value="/assets/endUser-assets/js/toastr.min.js"/>"></script>
<script src="<c:url value="/assets/js/moment.js"/>"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />    
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
 
<script type="text/javascript">
_.templateSettings = {
	    interpolate: /\<\@\=(.+?)\@\>/gim,
	    evaluate: /\<\@(.+?)\@\>/gim,
	    escape: /\<\@\-(.+?)\@\>/gim
	};
</script>
<%@ include file="../pages/enduser-pages/underscore-templates.tmpl" %>
<decorator:head />
</head>
<body
	<decorator:getProperty property="body.id" writeEntireProperty="true"/>
	<decorator:getProperty property="body.class" writeEntireProperty="true"/>>
	<c:url value="/home" var="homeUrl" />
	<c:url value="/termAndCondition" var="termAndConditionUrl" />
	<c:url value="/pincode" var="pincodeUrl" />
	<c:url value="/signIn" var="signInUrl" />
	<c:url value="/signUp" var="signUpUrl" />
	<c:url value="/cart" var="cartUrl" />
	<!-- Header -->
	<header data-offset-top="30" data-spy="affix" class="affix">
       <nav class="navbar">
          <div class="container">
             <!-- Brand and toggle get grouped for better mobile display -->
             <div class="navbar-header pull-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${homeUrl}"><img src="<c:url value='/assets/endUser-assets/img/logo.png'/>" alt=""></a>
             </div>
              <div class="navbar-header pull-right">
                <ul class="nav navbar-nav navbar-right nav-responsive">
                   <c:set var="checkLoged" value="1" />
				   <sec:authorize access="isAnonymous()">
				   	  <c:set var="checkLoged" value="0" />
				   </sec:authorize>
				   <c:choose>
				   		<c:when test="${checkLoged eq 0}">
				   			<li><a href="${signInUrl}">Sign In</a></li>
							<li><a href="${signUpUrl}">Sign Up</a></li>
				   		</c:when>
				   		<c:otherwise>
				   		   <li><a href='<c:url value="/end-user/welcome" />'>Dashboard</a></li>
		                   <li class="dropdown">
		                      <a href="#">My Account<i class="fa fa-angle-down"></i></a>
		                      <div class="dropdown-content">
		                      	 <c:if test="${sessionScope.isSubsriptionUser != null}">
		                      	 	<a href='<c:url value="/change-delivery-pattern/by-customer"/>'>Change Delivery Pattern</a>
		                         </c:if>
		                         <a href='<c:url value="/end-user/invoice-billing"/>'>Account &amp; Billings</a>
		                         <a href='<c:url value="/end-user/my-profile"/>'>My profile</a>
		                         <c:if test="${sessionScope.showExtendPrepay != null}">
		                         	<a href='<c:url value="/end-user/extend-prepaid/summary-subscription"/>'>Extend Prepay</a>
		                         </c:if>
		                         <c:if test="${sessionScope.isSubsriptionUser != null}">
		                      	 	<a href="<c:url value="/end-user/hold-delivery-schedule" />">Hold Delivery</a>
		                         </c:if>
		                         <a href='<c:url value="/end-user/logout" />'>Logout</a>
		                      </div>
		                   </li>
				   		</c:otherwise>
				   </c:choose>
                   <li><a href="${cartUrl}" id="cart"><i class="fa fa-shopping-cart cart-qty"></i> 
                    <span class="cart-items" id="cartItemCount">
						<c:set var="cartItemCount" value="0"/>
						<c:forEach items="${sessionScope.cartItems}" var="item">
							<c:set var="cartItemCount" value="${cartItemCount + 1}"/>
						</c:forEach>
						<c:out value="${cartItemCount}"/>
					</span>
                    </a></li>
                </ul>
             </div>
             <!-- Collect the nav links, forms, and other content for toggling -->
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   <li><a href="#">About Us</a></li>
                   <li><a href="#">Product</a></li>
                   <li><a href="#">Process</a></li>
                </ul>
             </div>
             <!-- /.navbar-collapse -->
          </div>
          <!-- /.container -->
       </nav>
    </header>
	<!--  End Header -->
	<!-- Page Body/Content -->
	<decorator:body />
	<!-- End Page Body/Content -->

	<!-- Footer -->
	<footer class="tac">
		<div class="container">
			<div class="row">
				<div class="footer-part-1 col-xs-12">
					<ul class="menu-footer">
						<li><a href="https://www.facebook.com/provilac"
							target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/ProvilacMilk"
							target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/provilacmilk"
							target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<!-- /.footer-part-1 -->
				<div class="footer-part-2 col-xs-12">
					<div class="copyright">
						<p>
							Copyright � 2017 <a href="http://provilac.com">Provilac</a> - All
							Rights Reserved
						</p>
						<p>
							<a href="${termAndConditionUrl}">Terms &amp; Privacy Policy</a>
						</p>
						<p>&nbsp;</p>
					</div>
				</div>
				<!-- /.footer-part-2 -->
			</div>
		</div>
		<!-- /.footer-container -->
		<a class="backtotop smooth" href="#top"> <span
			class="fa fa-angle-up"></span>
		</a>
		<div id="snackbar"><i class="fa fa-check" style=" color: #46bf45;"></i> This quantity of Product added to cart.</div>
	</footer>
	<c:if test="${checkLoged eq 1}">
		<c:choose>
			<c:when test="${sessionScope.showExtendPrepay == null}">
				<section>
				<c:if test="${sessionScope.showAmount > 0}">
			        <div class="sticky-bottom" id="myHeader">
			           <div class="due-box">
			              <div class="pending">
			                <span class="pending-text">Outstanding Amount :  <i class="fa fa-inr"></i> <b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${sessionScope.showAmount}"/></b></span>
			              </div>
			              <div class="due-btn">
			                 <a href="#" type="button" class="btn btn-primary">Pay Now</a>
			              </div>
			           </div>
			        </div>
			     </c:if>
			     </section>
			</c:when>
			<c:otherwise>
				<section>
				<c:if test="${sessionScope.showAmount > 0}">
			        <div class="sticky-bottom" id="myHeader">
			           <div class="due-box">
			              <div class="pending">
			                <span class="pending-text">Remaining Prepay Balance :  <i class="fa fa-inr"></i> <b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${sessionScope.showAmount}"/></b></span>
			              </div>
			           </div>
			        </div>
			     </c:if>
			     </section>
			</c:otherwise>
		</c:choose>
	</c:if>
	
	<!-- End Footer -->

	<section class="floating-form"></section>
	<div class="back-top">
		<a href="#" class="back-top-button"></a>
	</div>
	<script src="<c:url value='/assets/endUser-assets/js/vendor/bootstrap.min.js'/>"></script>
	<script src="<c:url value='/assets/endUser-assets/js/vendor/jquery.subscribe-better.js'/>"></script>
	<script src="<c:url value='/assets/endUser-assets/js/fullcalendar.js'/>"></script>
	<script src="<c:url value='/assets/endUser-assets/js/main.js'/>"></script>
	<script type="text/javascript">
	
	function addToCart(productName, productCode, quantity, successCallback, failureCallback){

		var paramString = 'productCode=' + productCode;
		if(null != quantity) {
			paramString = paramString + "&qty=" + quantity;
		}
		if(quantity < 1){
			return;
		}
		$.ajax({
			url : '<c:url value="/endUser-page/addToCard"/>',
			dataType : 'json',
			data : paramString,
			success : function(response) {
				if (null != response && response.success) {
					var x = document.getElementById("snackbar")
				    x.className = "show";
				    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
				    updateCartUI(response);
					if(successCallback != null) {
						successCallback(response);
					}
				} else {
					
					if(failureCallback != null) {
						failureCallback(reponse);
					} 
				}
			}
		});
	}
	
	function deleteItemFromCart(productName, productCode, successCallback, failureCallback){	
		$.ajax({
			url: '<c:url value="/end-user/deleteItemFromCart"/>',
			dataType: 'json',
			data : 'productCode=' +productCode ,		
			success: function(response) {
				if (null != response && response.success) {	
					$('#snackbar').text(productName + ' has been deleted from your shopping cart.');
					var x = document.getElementById("snackbar")
				    x.className = "show";
				    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
					updateCartUI(response);
					if(successCallback != null) {
						successCallback(response);
					}
				}else{
					showToast('error','Error Occured!');
					if(failureCallback != null) {
						failureCallback(reponse);
					}
				} 
			}
		}); 
	}
	
	function updateCartUI(response) {
		var cart = response.data.cart;
		var totalItems = 0;
		for(var i = 0; i < cart.length; i++) {
			totalItems = totalItems + 1;
		}
		$('#cartItemCount').text('' + totalItems + '');
	}
	</script>
</body>
</html>
