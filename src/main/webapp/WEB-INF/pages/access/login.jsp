<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/taglibs.jsp"%>

<html>
<head>
<title>Login</title>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<link href="<c:url value='/assets/end-user/css/signin-page.min.css'/>" rel="stylesheet">
<script type="text/javascript">
function submitLoginForm() {
	$('#loginForm').submit();
}
</script>
</head>

<body>
	<sec:authorize access="isAuthenticated()">
		<c:redirect url="/home" />
	</sec:authorize>

	<div style='float: right;'>
		<a href="<c:url value="/assets/apk/Provilac-Internal-App.apk" />">download Provilac - Internal App</a>
	</div>
	<div class="container">
	<div class="row">
	<div class="col-lg-12">
		<div class="well bs-component" style="width:50%; margin:130px auto">
			<form action="j_spring_security_check" class="form-horizontal" method="post">
				<fieldset>
					<legend>Admin Login</legend>
					<div class="form-group">
						<label for="username" class="col-lg-2 control-label">Username</label>
						<div class="col-lg-10">
							<input class="form-control" id="username" name="j_username" type="text" placeholder="Enter Username">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-lg-2 control-label">Password</label>
						<div class="col-lg-10">
							<input class="form-control" id="password" name="j_password" type="password" placeholder="***********">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<input class="btn btn-primary" type="submit" value="Login" />
						</div>
					</div>
				</fieldset>
				<a href="<c:url value="/admin/access/forgotPassword" />">Recover my password</a>
			</form>
		</div>
	</div>
	</div>
	</div>
	

	<div class="wrapper fixed__footer">
		<!-- Start Login Register Area -->
		<div class="htc__login__register bg__white ptb--130" style="background: rgba(0, 0, 0, 0) url(assets/endUser-assets/images/bg/5.jpg) no-repeat scroll center center/cover;">
			<div class="container">
				<%-- <div class="row">
					<div class="col-md-12">
					<div class="login-head">
							<h2>Admin Login</h2>
							<div></div>
						</div>
						<div class="htc__login__register__wrap">
							<!-- Start Single Content -->
							<div id="login" role="tabpanel" class="single__tabs__panel tab-pane fade in active">
								<div id="otpText">
									<div style="font-style: italic; color: red">${message}</div>
									<form id="loginForm" class="otp" method="post" action="j_spring_security_check">
										<div class="single-checkout-box">
											<label>Username</label> <input id="username" type="text" name="j_username" placeholder="Enter Username">
										</div>
										<div class="single-checkout-box">
											<label>Password</label> <input id="password" type="password" name="j_password" placeholder="***********">
										</div>
										<div class="htc__login__btn mt--30">
											<a href="javascript:submitLoginForm()">Login</a>
										</div>
									</form>
									<a href="<c:url value="/admin/access/forgotPassword" />" class="center-block">Recover my password</a>
								</div>
							</div>
							<!-- End Single Content -->
						</div>
					</div>
				</div> --%>
			</div>
		</div>
		<!-- End Login Register Area -->

		<!-- End Footer Area -->
	</div>
	<!-- Body main wrapper end -->

</body>
</html>