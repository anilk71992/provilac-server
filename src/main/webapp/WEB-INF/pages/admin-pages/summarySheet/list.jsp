<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - SummarySheet</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>SummarySheet</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>SummarySheet</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/summarySheet/list" />
	<c:set var="urlPrefix" value="/admin/summarySheet/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/summarySheet/show" var="summarySheetDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="summarySheets" class="footable" requestURI="" id="summarySheet" export="false">
			<display:column title="Code" sortable="true">
				<a href="${summarySheetDetailLinkPrefix}/${summarySheet.cipher}">${summarySheet.code}</a>
			</display:column>
			
			<display:column title="FromDate" sortable="true">
				<fmt:formatDate value="${summarySheet.fromDate}" pattern="dd-MM-yyyy" />
			</display:column>
			<display:column title="ToDate" sortable="true">
				<fmt:formatDate value="${summarySheet.toDate}" pattern="dd-MM-yyyy" />
			</display:column>
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
							<!-- dropdown menu links -->
						 <%-- <li><a href="<c:url value="/admin/summarySheet/update/${summarySheet.cipher}"/>">Edit</a>
						</li>  --%>
					<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ACCOUNTANT','ROLE_ADMIN')">
						<li><a href="<c:url value="/admin/summarySheet/export/${summarySheet.code}"/>">Export</a>
						</li>
					</sec:authorize>
											
					</ul>
				</div>

			</display:column>
		</display:table>
	</form>
</body>
</html>