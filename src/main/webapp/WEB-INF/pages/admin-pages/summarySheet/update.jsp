<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update SummarySheet</title>
<style>
tr{
 height: 20px;
}
td {
	width: 350px;
	padding: 0 5px 0 5px;
}

</style>
<script type="text/javascript">
$(document).ready(function() {
	$('#chequeDate').datepicker();
	$('#receivedDate').datepicker();
});
</script>
</head>

<c:url value="/admin/summarySheet/update" var="summarySheetUpdate" />
<c:url value="/admin/summarySheet/list" var="summarySheetList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/summarySheet/update" var="updateSummarySheet"/>
	<form:form id="updateSummarySheet" method="post" modelAttribute="summarySheet" action="${updateSummarySheet}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<c:choose> 
			<c:when test="${not empty summarySheet.summarySheetRecords}">
				<table cellSpacing ="10">
					<thead>
						<tr>
							<th> Code</th>
							<th> Customer </th>
							<th> Total Amount</th>
							<th> Adjustment </th>
							<th>Payment Method </th>
							<th> Cheque Number </th>
							<th> Cheque Date </th>
							<th> Received Date </th>
							<th> Receipt Number </th>
							<th> Received By </th>
							
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${summarySheet.summarySheetRecords}" var="record">
							<tr>
								<td>
									<input type="text" name="record" value="${record.code}" readonly="true" class="form-control">
								</td>
								<td>
									<input type="text" name="customer" value="${record.customer.fullName}" placeholder="user" readonly="true" class="form-control">
								</td>
								<td>
									<input type="text" name="totalAmount" value="${record.totalAmount}" placeholder="totalAmount" readonly="true" class="form-control">
								</td>
								<td>
								 	<input type="text" name="adjustment" value="${record.adjustment}" placeholder="adjustment" class="form-control">
								</td>
								<td>
									<select name="paymentMethod" class="form-control">
										<option value="Cash" <c:if test="${record.paymentMethod == 'Cash'}">selected='selected'</c:if> >Cash</option>
										<option value="Cheque" <c:if test="${record.paymentMethod == 'Cheque'}">selected='selected'</c:if>  >Cheque</option>
									</select>
								</td>
								<td>
									<input type="text" name="chequeNumber" value="${record.chequeNumber}" placeholder="chequeNumber" class="form-control">	
								</td>
								<td>
									<input type="text" name="chequeDate" id="chequeDate" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${record.chequeDate}" pattern="dd-MM-yyyy" />"class="form-control">
								</td>
								<td>
									<input type="text" name="receivedDate" placeholder="Received Date" id="receivedDate" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${record.receivedDate}" pattern="dd-MM-yyyy" />"class="form-control">
								</td>
								<td>
									<input type="text" name="receiptNumber"  value="${record.receiptNumber}" placeholder="receiptNumber" class="form-control">
								</td>
								<td>
									<select name="receivedBy" class="form-control">
									<c:forEach items="${collectionBoys}" var="customer">
										<option value="${customer.id}" <c:if test="${record.receivedBy.id == customer.id}">selected='selected'</c:if>>${customer.fullName}</option>
									</c:forEach>
								</select>
								</td>
							</tr>
								
							<tr>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<span>Fields marked with * are mandatory</span>
				</c:when>
				<c:otherwise>
					<script type="text/javascript">
					showToast("warning","No Records Found!!!");
					</script>
					No Records Found!!
				</c:otherwise>
			</c:choose>
		<br />
		<br />
		<c:if  test="${not empty summarySheet.summarySheetRecords}">
			<button class="btn btn-info" type="submit">Update</button> &nbsp;&nbsp;&nbsp;&nbsp;
		</c:if>
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>