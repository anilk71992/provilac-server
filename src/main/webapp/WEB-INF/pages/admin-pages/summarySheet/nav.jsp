<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/summarySheet/list" var="summarySheetList" />
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>