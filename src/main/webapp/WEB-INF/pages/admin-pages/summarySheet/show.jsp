<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - SummarySheet</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">SummarySheet- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Code</label>
				<p class="col-md-2">${summarySheet.code}</p>
				<label class="col-md-2">From Date</label>
				<%-- <p class="col-md-2">${summarySheet.fromDate}</p> --%>
				<p class="col-md-2">
					<fmt:formatDate value="${summarySheet.fromDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
				<label class="col-md-2">To Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${summarySheet.toDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			
		</div>
	</div>

	<div class="panel panel-primary" id="primary-info summarySheet">
		<div class="panel-heading">
			<h3 class="panel-title">SummarySheetRecords</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
		<c:forEach items="${summarySheet.summarySheetRecords}" var="record">
			<div class="panel panel-primary" style="margin: 10px;">
			<fieldset style="margin: 10px;" >
			 <legend>${record.id}</legend>
				<div class="row">
				<label class="col-md-2">Name: </label>
				<p class="col-md-2">${record.customer.firstName}  ${record.customer.lastName}</p>
				<label class="col-md-2">Address: </label>
				<p class="col-md-2">${record.customer.address}</p>
				</div>
				<br>
				<strong>Invoice details:</strong>
			<div class="row">
				<label class="col-md-2">code</label>
				<p class="col-md-2">${record.invoice.code}</p>
				<label class="col-md-2">fromDate</label>
			<%-- 	<p class="col-md-2">${record.invoice.fromDate}</p> --%>
			<p class="col-md-2">
				<fmt:formatDate value="${record.invoice.fromDate}" pattern="dd-MM-yyy hh:mm:ss a" />
			</p>		
				<label class="col-md-2">toDate</label>
				<%-- <p class="col-md-2">${record.invoice.toDate}</p>	 --%>
			<p class="col-md-2">
				<fmt:formatDate value="${record.invoice.toDate}" pattern="dd-MM-yyy hh:mm:ss a" />
			</p>			
			</div>
			<%-- <div class="row">
			<c:forEach items="${summarySheet.customer.payment}" var="payment">
			
			</c:forEach> --%>
			<div class="row">
				
				<label class="col-md-2">totalAmount</label>
				<p class="col-md-2">${record.invoice.totalAmount}</p>		
				<label class="col-md-2">lastPendingDues</label>
				<p class="col-md-2">${record.invoice.lastPendingDues}</p>		
				<label class="col-md-2">provilacOutStanding</label>
				<p class="col-md-2">${record.invoice.provilacOutStanding}</p>			
			</div>
			<div class="row">
				<label class="col-md-2">collectionBoyNote</label>
				<p class="col-md-2">${record.invoice.collectionBoyNote}</p>
				<label class="col-md-2">Received By</label>
				<p class="col-md-2">${record.receivedBy}</p>		
			</div>
			</fieldset>
			</div>
		</c:forEach>
				
				</div>
			</div>
			</div>
	<%-- <div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${summarySheet.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${summarySheet.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${summarySheet.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${summarySheet.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div> --%>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>