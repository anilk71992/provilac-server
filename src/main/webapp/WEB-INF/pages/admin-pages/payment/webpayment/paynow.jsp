<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Provilac - Pay Now</title>
	<link href="<c:url value="/assets/inspinia/css/bootstrap.min.css"/>" rel="stylesheet">
	<link href="<c:url value="/assets/inspinia/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
	<script src="<c:url value="/assets/js/jquery-2.2.1.min.js"/>"></script>
	<script type="text/javascript">
		var isRequestInProgress = false;
		var isFormValidated = false;
	
		$(document).ready(function() {
			
			$('#paymentForm').submit(function() {
				/**
					1. Validate Fields
					2. Send Ajax Request
					3. Get Hash & TransactionId
					4. Submit Form
				*/
				if(isFormValidated) {
					return true;
				}
				if(isRequestInProgress) {
					return false;
				}
				//Disable form submit
				isRequestInProgress = true;
				changeFormState(false);
				
				var message = validateForm();
				if(message != "OK") {
					alert(message);
					isRequestInProgress = false;
					changeFormState(true);
					return false;
				}
				
				var dataObj = new Object();
				dataObj['key'] = $('[name="key"]').val();
				dataObj['productInfo'] = $('[name="productinfo"]').val();
				dataObj['surl'] = $('[name="surl"]').val();
				dataObj['furl'] = $('[name="furl"]').val();
				dataObj['service_provider'] = $('[name="service_provider"]').val();
				dataObj['firstName'] = $('[name="firstname"]').val();
				dataObj['lastName'] = $('[name="lastname"]').val();
				dataObj['address1'] = $('[name="address1"]').val();
				dataObj['address2'] = $('[name="address2"]').val();
				dataObj['city'] = $('[name="city"]').val();
				dataObj['state'] = $('[name="state"]').val();
				dataObj['country'] = $('[name="country"]').val();
				dataObj['zipcode'] = $('[name="zipcode"]').val();
				dataObj['udf1'] = $('[name="udf1"]').val();
				dataObj['udf2'] = $('[name="udf2"]').val();
				dataObj['udf3'] = $('[name="udf3"]').val();
				dataObj['udf4'] = $('[name="udf4"]').val();
				dataObj['udf5'] = $('[name="udf5"]').val();
				dataObj['pg'] = $('[name="pg"]').val();
				dataObj['phone'] = $('[name="phone"]').val();
				dataObj['email'] = $('[name="email"]').val();
				dataObj['amount'] = $('[name="amount"]').val();
				//dataObj['city'] = $('[name="city"]').val();
				
				$.ajax({
					url: '<c:url value="/openapi/payment/getHashAndTxnId"/>',
					dataType: 'json', //Type of data expected back from server
					data: dataObj,
					success: function(response) {
						if(response.success) {
							var data = response["data"];
							$('[name="hash"]').val(data["hash"]);
							$('[name="txnid"]').val(data["txnId"]);
							$('[name="amount"]').val(data["amount"]);
							isFormValidated = true;
							$('#paymentForm').submit();
						}
					},
					error: function(jqXHR, exception) {
						changeFormState(true);
					}
				});
				return false;
			});
		});
		
		function changeFormState(isEnabled) {
			$('#paymentForm').find(':input[type=submit]').prop('disabled', !isEnabled);
		}
		
		function validateForm() {
			
			//Check for blank fields
			if($('[name="key"]').val() == "" || $('[name="productinfo"]').val() == "" || $('[name="surl"]').val() == "" || 
					$('[name="furl"]').val() == "" || $('[name="service_provider"]').val() == "" || $('[name="firstname"]').val() == "" || 
					$('[name="phone"]').val() == "") {
				return "Form has been tampered. Please reload the page.";
			}
			
			if($('[name="email"]').val() == "" || $('[name="amount"]').val() == "") {
				return "Please enter valid email/amount";
			}
			
			//Check for field wise contents
			if($('[name="key"]').val() != "${key}" || $('[name="productinfo"]').val() != "${invoice.code}" || $('[name="surl"]').val() != "${surl}" || 
					$('[name="furl"]').val() != "${furl}" || $('[name="service_provider"]').val() != "payu_paisa" || $('[name="firstname"]').val() != "${invoice.customer.firstName}" || 
					$('[name="phone"]').val() != "${invoice.customer.mobileNumber}") {
				return "Form has been tampered. Please reload the page.";
			}
			
			var onlyAlphabetsPattern = /^[a-zA-Z]+$/;
			var onlyNumbersPattern = /^[0-9]+$/;
			var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
			var amountPattern = /^[+]?([0-9]*[.])?[0-9]+$/;
			
			if($('[name="lastname"]').val() != "") {
				var lastName = $('[name="lastname"]').val();
				if(!onlyAlphabetsPattern.test(lastName)) {
					return "Form has been tampered. Please reload the page.";
				}				
			}
			
			if($('[name="address1"]').val() != "" || $('[name="address2"]').val() != "" || $('[name="city"]').val() != "" || 
					$('[name="state"]').val() != "" || $('[name="country"]').val() != "") {
				return "Form has been tampered. Please reload the page.";
			}
			
			if($('[name="zipcode"]').val() != "") {
				var zipcode = $('[name="zipcode"]').val();
				if(!onlyNumbersPattern.test(zipcode)) {
					return "Form has been tampered. Please reload the page.";
				}				
			}
			
			if($('[name="udf1"]').val() != "" || $('[name="udf2"]').val() != "" || $('[name="udf3"]').val() != "" || 
					$('[name="udf4"]').val() != "" || $('[name="udf5"]').val() != "" || $('[name="pg"]').val() != "") {
				return "Form has been tampered. Please reload the page.";
			}
			
			var phone = $('[name="phone"]').val();
			if(!onlyNumbersPattern.test(phone)) {
				return "Form has been tampered. Please reload the page.";
			}
			
			var email = $('[name="email"]').val();
			if(!emailPattern.test(email)) {
				return "Please enter valid email id.";
			}
			
			var amount = $('[name="amount"]').val();
			if(!amountPattern.test(amount)) {
				return "Please enter valid amount";
			}
			
			if(parseFloat(amount) < 1) {
				return "Please enter valid amount";
			}
			
			return "OK";
		}
	</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="http://provilac.com">Provilac</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="jumbotron">
			<form action="https://secure.payu.in/_payment" id="paymentForm" method="post">
				<fieldset>
					<legend>Provilac - Pay Now</legend>
					<div class="col-md-6">
						<input name="key" id="key" value="${key}" type="hidden"/>
						<!-- <input name="hash_string" id="hash_string" type="hidden"/> -->
						<input type="hidden" name="hash" id="hash" />
						<input type="hidden" name="txnid" id="txnid"/>
						<input type="hidden" name="productinfo" id="productinfo" value="${invoice.code}"/>
						<input type="hidden" name="surl" id="surl" value="${surl}"/>
						<input type="hidden" name="furl" id="furl" value="${furl}"/>
						<input type="hidden" name="service_provider" value="payu_paisa"/>
						<input type="hidden" name="firstname" id="firstname" value="${invoice.customer.firstName}"/>
						<input type="hidden" name="lastname" id="lastname"/>
						<input type="hidden" name="address1"/>
						<input type="hidden" name="address2"/>
						<input type="hidden" name="city" id="city"/>
						<input type="hidden" name="state" id="state"/>
						<input type="hidden" name="country" id="country"/>
						<input type="hidden" name="zipcode" id="zipcode"/>
						<input type="hidden" name="udf1"/>
						<input type="hidden" name="udf2"/>
						<input type="hidden" name="udf3"/>
						<input type="hidden" name="udf4"/>
						<input type="hidden" name="udf5"/>
						<input type="hidden" name="pg"/>
						
						<!-- User Input Fields -->
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Name</span>
							<input name="name" id="name" Class="form-control" disabled="disabled" value="${invoice.customer.fullName}"/>
						</div>
						<br>
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Mobile Number</span>
							<input name="phone" id="phone" Class="form-control" readonly="readonly" value="${invoice.customer.mobileNumber}"/>
						</div>
						<br>
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Email</span>
							<input name="email" id="email" Class="form-control" value="${invoice.customer.email}"/>
						</div>
						<br>
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Total Due</span>
							<input name="totaldue" id="totalDue" Class="form-control" disabled="disabled" value="${invoice.customer.lastPendingDues}"/>
						</div>
						<br>
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Amount To Pay</span>
							<input name="amount" id="amount" Class="form-control" value="${invoice.customer.lastPendingDues}"/>
						</div>
						<br>
						<button type="submit" value="submit" class="btn btn-success">Make Payment</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</body>
</html>