<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/assets/inspinia/css/bootstrap.min.css"/>" rel="stylesheet">
<link href="<c:url value="/assets/inspinia/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
<script src="<c:url value="/assets/js/jquery-2.2.1.min.js"/>"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Provilac - Payment Success</title>
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top"></nav>
	<div class="container">
		<div class="jumbotron alert alert-warning text-center">
			<img src="<c:url value="/assets/custom-icons/warning.png"/>">
			<p class="lead">Something went wrong. Please check with our customer care executive.</p>
		</div>
	</div>
</body>
</html>