<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Payment</title>
<c:set var="pointValue" value="${pointValue}"></c:set>
<c:out value="${pointValue}"/>
<script type="text/javascript">
$(document).ready(function() {
	$('#date').datepicker({autoclose:true});
	$('#chequeNumber').hide();
	$('#bankName').hide();
	$('#customer').select2();
	$('#receivedByDiv').hide();
	$('#dateDiv').hide();
	onCustomerChange(this);
});
var route=null;

function  onPaymentMethodChange() {
	
	/*
	1. Cash
	2. NEFT
	3. Cheque
	4. Online
	5. PAYTM
	6. PAYU
	*/
	var paymentMethod= document.getElementById('paymentMethod').value;
	
	if(paymentMethod=="Cash"){
		$('#chequeNumber').hide();
		$('#chequeNumberId').val(" ");
		
		$('#bankName').hide();
		$('#bankNameId').val(" ");
		
		$("#date").val("${today}");
		$('#date').prop('readonly', true);
		$('#dateDiv').hide();
		
		$('#receivedBy').val(<%=((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()%>);
		$('#receivedByDiv').show();
		
	}else if (paymentMethod=="NEFT"){
		$('#chequeNumber').hide();
		$('#chequeNumberId').val(" ");
		
		$('#bankName').show();
		
		$('#date').prop('readonly', false);
		$('#dateDiv').show();

		$('#receivedByDiv').hide();
		$('#receivedBy').val(-1);
		
	}else if(paymentMethod=="Cheque"){
		$('#chequeNumber').show();
		
		$('#bankName').show();
		
		$("#date").val("${today}");
		$('#date').prop('readonly', true);
		$('#dateDiv').hide();
		
		$('#receivedBy').val(<%=((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()%>);
		$('#receivedByDiv').show();
		
	}else if(paymentMethod == 'Online') {
		$('#chequeNumber').hide();
		$('#chequeNumberId').val(" ");
		
		$('#bankName').show();
		
		$('#date').prop('readonly', false);
		$('#dateDiv').show();

		$('#receivedByDiv').hide();
		$('#receivedBy').val(-1);
		
	}else {
		$('#chequeNumber').hide();
		$('#chequeNumberId').val(" ");
		
		$('#bankName').hide();
		$('#bankNameId').val(" ");

		$('#date').prop('readonly', false);
		$('#dateDiv').show();
		
		$('#receivedByDiv').hide();
		$('#receivedBy').val(-1);
	}
	
}

function calculatePointAmount(){
	var	pointsRedemed=parseFloat($('#pointsRedemed').val());
	var ponitsAmount=pointsRedemed*parseFloat(("${pointValue}"));
	
	
	var accumulatedPoints=parseFloat($('#accumulatedPoints').val());
	if(pointsRedemed>accumulatedPoints || pointsRedemed<0){
		showToast('warning','Please Enter Valid Points Redemed');
		return false;
	}
	$('#pointsAmount').val(ponitsAmount);
	
}

function onCustomerChange(select) {
	route=null;
	var customer = document.getElementById("customer").selectedIndex;  
	var customerId = document.getElementsByTagName("option")[customer].value;
	
	$.ajax({
		url: '<c:url value="/restapi/invoice/getInvoiceByCustomer"/>',
		dataType: 'json',
		data: 'customerId=' + customerId,
		success: function(response) {
			if (null != response) {
				if(response.success) {
					var data = response.data;
					route=data.route;
					$("#invoice").empty();
					for (var i = 0; i < data.invoices.length; i++) {
						var invoice  = data.invoices[i];
						var x = document.getElementById("invoice");
						var invId = invoice.code;
					    var option = document.createElement("option");
					    option.text= invoice.customerCode +"("+invId +")";
					    option.value = invoice.id;
					    x.add(option);
					}
					$('#due').val(data.due);
					$('#accumulatedPoints').val(data.accumulatedPoints);
					calculateCashback();
				} else {
					alert(response.error.message);
				}
			} else {
				alert("Unable to get invoices for selected user.");
			} 
		}
	}); 
}

function calculateCashback() {
	var amount = parseFloat($('#amount').val());
	var due = parseFloat($('#due').val());
	var minBalanceForCashback = parseFloat('<c:out value="${minBalanceForCashback}"/>');
	var cashbackPercentage = parseFloat('<c:out value="${cashbackPercentage}"/>')/100;
	
	var cashback = 0.0;
	
	if(due > 0 && (amount - due) >= minBalanceForCashback) {
		//User has paid amount which makes dues zero and supasses min amount required for cashback
		
		var extraAmount = amount - due;
		cashback = extraAmount * cashbackPercentage;
		
	} else if (due <= 0 && amount >= minBalanceForCashback) {
		//If dues are less than zero, and still user is paying more than min amount required for cashback
		cashback = amount * cashbackPercentage;
	}
	$('#cashback').val(cashback);
}

function submitForm() {
	if(validate()){
		$("#addPayment").on("click", function() {
			$('#addPayment').submit();
			$(this).prop()
	        $(this).attr("disabled", "disabled");
	    });
	}
}

function validate(){
	
	var totalDue=parseFloat($('#due').val());
	var totalamount=parseFloat($('#amount').val());
	var totaladjustmentAmount=parseFloat($('#adjustmentAmount').val());
	
	if(totalDue>=0){
		if(totalamount<0){
			showToast('warning','Please Enter Valid Amount ');
			return false;
			}
		
		/*if(totaladjustmentAmount<0){
			showToast('warning','Please Enter Valid Adjustment Amount ');
			return false;
			}*/
	}
	var paymentMethod= document.getElementById('paymentMethod').value;
	if(paymentMethod=='-1'){
		showToast('warning','Please Select Payment Method ');
		return false;
		
	}
	var  regExp=/^[a-zA-Z ]*$/;
	if(paymentMethod=='Online'){
		if(!$('#bankNameId').val().trim() || $('#bankNameId')==""||(!regExp.test($('#bankNameId').val().trim()))){
			showToast('warning','Please add Valid bank name ');
			return false;
		}
	}
	
	if(paymentMethod=="Cheque"){
		
		if(!$('#chequeNumberId').val().trim()){
			showToast('warning','Please Enter Cheque Number ');
			return false;
		}
		
		if(!$('#bankNameId').val().trim() || $('#bankNameId')==""||(!regExp.test($('#bankNameId').val().trim()))){
			showToast('warning','Please add Valid bank name ');
			return false;
		}
		
	}
	
	if($('#txnIdId').val().trim()==""){
		showToast('warning','Please Enter Transaction Number! ');
		return false;
	}
	return true;
}

</script>
</head>
<c:url value="/payment/add" var="paymentAdd" />
<c:url value="/payment/list" var="paymentList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/payment/add" var="addPayment" />

	<form:form id="addPayment" action="${addPaymentPic}" method="post" modelAttribute="payment" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Payment</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<form:errors path="customer" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<select name="customer" id="customer"  onchange="onCustomerChange(this);" class="form-control">
							<option value="-1">Select Customer</option>
								<c:forEach items="${customers}" var="customer">
									<option value="${customer.id}">${customer.firstName}-${customer.lastName}(${customer.mobileNumber})</option>
								</c:forEach>
							</select>
						</div> 
						<br>			
						
						<form:errors path="invoice" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Invoice*</span> 
							<select name="invoice" id="invoice" class="form-control">
							</select>
						</div>
						<br>
						
						<form:errors path="receiptNo" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Receipt No</span>
							<form:input path="receiptNo" cssClass="form-control" />
						</div>
						<br>
						
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Total Due</span>
							<input name="totaldue" id="due" disabled="disabled" Class="form-control" />
						</div>
						<br>
						
						<form:errors path="amount" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Amount</span>
							<form:input path="amount" id="amount" cssClass="form-control" onchange="calculateCashback()" />
						</div>
						<br>
						
						<form:errors path="adjustmentAmount" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon"> Adjustment Amount</span>
							<form:input path="adjustmentAmount" id="adjustmentAmount" cssClass="form-control" />
						</div>
						<br>
						
						<form:errors path="cashback" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon"> Cashback</span>
							<form:input path="cashback" id="cashback" cssClass="form-control" />
						</div>
						<br>
						
						<form:errors path="hasDeposited" cssClass="text-danger"/>
						<div class="input-group width-xlarge">
							<span class="input-group-addon">HasDeposited</span>
								<form:select path="hasDeposited" id="hasDeposited"  cssClass="form-control" disabled="true">
										<option value="true">Yes</option>
										<option value="false">No</option>
								</form:select>
						</div>
						<br>
						
						<form:errors path="isVerified" cssClass="text-danger"/>
						<div class="input-group width-xlarge">
							<span class="input-group-addon">isVerified</span>
								<form:select path="isVerified" id="isVerified" cssClass="form-control" disabled="true">
										<option value="true">Yes</option>
										<option value="false">No</option>
								</form:select>
						</div>
						<br>
					</div>
					<div class="col-md-6">
						<div class="input-group width-xlarge">
								<span class="input-group-addon">Your Accumulated Points </span> 
								<input  class="form-control" type="text" id="accumulatedPoints" name="accumulatedPoints" readonly="true">
						</div>
						<br>
						
						<form:errors path="pointsRedemed" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Points Redemed</span>
							<form:input path="pointsRedemed"  id="pointsRedemed" onchange="calculatePointAmount();" cssClass="form-control" />
						</div>
						<br>
						
						<form:errors path="pointsAmount" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Points Amount</span>
							<form:input path="pointsAmount"  id="pointsAmount" disabled="disabled"  cssClass="form-control" readonly="true"/>
						</div>
						<br>
						
						<form:errors path="paymentMethod" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Payment Method</span> 
							<form:select path="paymentMethod" cssClass="form-control" onchange=" onPaymentMethodChange()" id="paymentMethod">
								<form:option value="-1">Select PaymentMethod</form:option>
								<form:option value="Cash">Cash</form:option>
								<form:option value="Online">Online</form:option>
								<form:option value="Cheque">Cheque</form:option>
								<form:option value="NEFT">NEFT</form:option>
								<form:option value="PAYTM">PAYTM</form:option>
								<form:option value="PAYU">PAYU</form:option>
							</form:select>
						</div>
						<br>
						
						<form:errors path="txnId" cssClass="text-danger" />
						<div class="input-group width-xlarge" id="txnId">
							<span class="input-group-addon">Transaction Id*</span>
							<form:input path="txnId" cssClass="form-control"  id="txnIdId"/>
						</div>
						<br>
						
						<div id="chequeNumber">
							<form:errors path="chequeNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Cheque Number</span>
								<form:input path="chequeNumber" cssClass="form-control" id="chequeNumberId"/>
							</div>
							<br>
						</div>
						
						<div id="bankName">
							<form:errors path="bankName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Bank Name</span>
								<form:input path="bankName" cssClass="form-control"  id="bankNameId"/>
							</div>
							<br>
						</div>
						
						<input type="hidden" name="isBounced" id="isBounced" value="false"/>
						
						<div id="receivedByDiv">
							<form:errors path="receivedBy" cssClass="text-danger" />
							<div class="input-group width-xlarge" >
								<span class="input-group-addon">Received By</span>
								<input class="form-control" value="<%=((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getFullName()%>" readonly="readonly"/>
								<input type="hidden" value="<%=((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()%>" name="receivedBy" id="receivedBy"/>
							</div>
							<br>
						</div>
			
						<div id="dateDiv">
							<form:errors path="date" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Date</span>
								<form:input path="date" id="date" class="form-control" data-date-format="dd-mm-yyyy" value="${today}"/>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
			<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>