<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Payment</title>
<c:set var="pointValue" value="${pointValue}"></c:set>
<c:out value="${pointValue}"/>
<script type="text/javascript">
$(document).ready(function() {
	$('#date').datepicker({autoclose:true});
	$('#chequeNumber').hide();
	$('#bankName').hide();
	$('#isBounced').hide();
	onCustomerChange(this);
	onPaymentMethodChange(this);
});
function  onPaymentMethodChange() {
	var paymentMethod= document.getElementById('paymentMethod').value;
	
	if(paymentMethod=="Cash"){
		$('#chequeNumber').hide();
		$('#chequeNumber').val(" ");
		$('#bankName').hide();
		$('#bankName').val(" ");
		$('#isBounced').hide();
		$('#isBounced').val(false);
		
	}else if (paymentMethod=="NEFT"){
		$('#chequeNumber').hide();
		$('#bankName').hide();
		$('#isBounced').hide();
		$('#receivedByDiv').show();
	}else if(paymentMethod=="Cheque"){
		$('#chequeNumber').show();
		$('#bankName').show();
		$('#isBounced').show();
	}else{
		$('#chequeNumber').hide();
		$('#chequeNumber').val(" ");
		$('#isBounced').hide();
		$('#isBounced').val(false);
		$('#bankName').show();
	}
	
}

function onCustomerChange(select) {
	
	var customer = document.getElementById("customer").selectedIndex;  
	var customerId = document.getElementsByTagName("option")[customer].value;
	
	$.ajax({
		url: '<c:url value="/restapi/invoice/getInvoiceByCustomer"/>',
		dataType: 'json',
		data: 'customerId=' + customerId,
		success: function(response) {
		if (null != response) {
			if(response.success) {
				var data = response.data;
				$("#invoice").empty();
				if(data!=0){
				for (var i = 0; i < data.invoices.length; i++) {
					var invoice  = data.invoices[i];
					var x = document.getElementById("invoice");
					var invId = invoice.code;
				    
				    var option = document.createElement("option");
				    option.text= invoice.customerCode +"("+invId +")";
				    option.value = invoice.id;
				    x.add(option);
				}
			}
			} else {
				alert(response.error.message);
			}
		} else {
			alert("Unable to get invoices for selected user.");
		} 
		}
	}); 
}

function submitForm() {
	if(validate()){
		$('#updateUser').submit();
	}
}

function validate(){
	
	var paymentMethod= document.getElementById('paymentMethod').value;
	if(paymentMethod=='-1'){
		showToast('warning','Please Select Payment Method ');
		return false;
		
	}
	var  regExp=/^[a-zA-Z ]*$/;
	if(paymentMethod=='Online'){
		if(!$('#bankNameId').val().trim() || $('#bankNameId')==""||(!regExp.test($('#bankNameId').val().trim()))){
			showToast('warning','Please add Valid bank name ');
			return false;
		}
	}
	if(paymentMethod=="Cheque"){
		
		if(!$('#chequeNumberId').val().trim()){
			showToast('warning','Please Enter Cheque Number ');
			return false;
		}
		
		if(!$('#bankNameId').val().trim() || $('#bankNameId')==""||(!regExp.test($('#bankNameId').val().trim()))){
			showToast('warning','Please add Valid bank name ');
			return false;
		}
		
	}
	
	if(paymentMethod=="Cheque"||paymentMethod=="NEFT"||paymentMethod=="Cash"){
	var receivedById = $('#receivedBy').val();
	if(receivedById =="-1"){
		showToast('warning','Please select Payment Received By User ');
		return false;
	}
	
	}
	return true;
	
}


function calculatePointAmount(){
	var	pointsRedemed=$('#pointsRedemed').val().trim();
	var ponitsAmount=pointsRedemed*("${pointValue}");
	
	
	var accumulatedPoints=$('#accumulatedPoints').val();
	if(pointsRedemed>accumulatedPoints || pointsRedemed<0){
		showToast('warning','Please Enter Valid Points Redemed');
		return false;
	}
	$('#pointsAmount').val(ponitsAmount);
	
}

</script>
</head>

<c:url value="/admin/payment/update" var="paymentUpdate" />
<c:url value="/admin/payment/list" var="paymentList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/payment/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="payment" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

	<fieldset>
			<h3 class="page-header">Payment</h3>
			<form:errors path="customer" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Customer*</span> 
				<select name="customer" id="customer"  onchange="onCustomerChange(this)" class="form-control">
					<c:forEach items="${customers}" var="customer">
						<option value="${customer.id}"
						<c:if test="${customer.id == payment.customer.id}">selected="selected"</c:if>>${customer.firstName}-${customer.lastName}(${customer.mobileNumber})</option>
					</c:forEach>
				</select>
			</div> 
			<br>			
			<form:errors path="invoice" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Invoice*</span> 
				<select name="invoice" id="invoice" class="form-control">
				</select>
			</div>
			<br>
			<form:errors path="txnId" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Transaction Id</span>
				<form:input path="txnId" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="receiptNo" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Receipt No</span>
				<form:input path="receiptNo" cssClass="form-control" />
			</div>
			<br>
			
			<form:errors path="date" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Date</span>
				<input name="date" id="date" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${payment.date}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="amount" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Amount</span>
				<form:input path="amount" cssClass="form-control" />
			</div>
			<br>
			<div class="input-group width-xlarge">
					<span class="input-group-addon">Your Accumulated Points </span> 
					<input  class="form-control" type="text" id="accumulatedPoints" name="accumulatedPoints" readonly="true">
			</div>
				<br>
			<form:errors path="pointsRedemed" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Points Redemed</span>
				<form:input path="pointsRedemed"  id="pointsRedemed" onchange="calculatePointAmount();" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="pointsAmount" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Points Amount</span>
				<form:input path="pointsAmount"  id="pointsAmount" disabled="disabled"  cssClass="form-control" readonly="true"/>
			</div>
			<br>
			
			<form:errors path="paymentMethod" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Payment Method</span> 
				<form:select path="paymentMethod" cssClass="form-control" onchange="onPaymentMethodChange()">
					<form:option value="Cash">Cash</form:option>
					<form:option value="Online">Online</form:option>
					<form:option value="Cheque">Cheque</form:option>
					<form:option value="NEFT">NEFT</form:option>
					<form:option value="PAYTM">PAYTM</form:option>
					<form:option value="PAYU">PAYU</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="chequeNumber" cssClass="text-danger" />
			<div class="input-group width-xlarge" id="chequeNumber">
				<span class="input-group-addon">Cheque Number</span>
				<form:input path="chequeNumber" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="bankName" cssClass="text-danger" />
			<div class="input-group width-xlarge" id="bankName">
				<span class="input-group-addon">Bank Name</span>
				<form:input path="bankName" cssClass="form-control" id="bankNameId"/>
			</div>
			<br>

			<form:errors path="isBounced" cssClass="text-danger" />
			<div class="input-group width-xlarge" id="isBounced">
				<span class="input-group-addon">Bounced</span>
				<form:select path="isBounced" cssClass="form-control">
					<form:option value="true">Yes</form:option>
					<form:option value="false">No</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="receivedBy" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Received By</span>
				<form:select path="receivedBy" cssClass="form-control"  id="receivedBy">
					<c:forEach items="${users}" var="customer">
						<option value="${customer.id}"
							<c:if test="${customer.id == payment.customer.id}">selected="selected"</c:if>>${customer.firstName} ${customer.lastName} - ${customer.mobileNumber}</option>
					</c:forEach>
				</form:select>
			</div>
			<br>

		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>