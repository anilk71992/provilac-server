<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Payment</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Payment</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Payment</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/payment/list" />
	<c:set var="urlPrefix" value="/admin/payment/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/payment/show" var="paymentDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="payments" class="footable" requestURI="" id="payment" export="false">
			<display:column title="Code" sortable="true">
				<a href="${paymentDetailLinkPrefix}/${payment.cipher}">${payment.code}</a>
			</display:column>
			
			<display:column title="Customer" sortable="true">${payment.customer.fullName}</display:column>
			<display:column title="TransactionId" sortable="true">${payment.txnId}</display:column>
			<display:column title="receiptNo" sortable="true">${payment.receiptNo}</display:column>
			<display:column title="Amount" sortable="true">${payment.amount}</display:column>
			<display:column title="Cashback" sortable="true">${payment.cashback}</display:column>
			<display:column title="Date" sortable="true">
				<fmt:formatDate value="${payment.date}" pattern="dd-MM-yyyy" />
			</display:column>
			<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
			<%--<display:column title="Actions" sortable="false">
				 <div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/payment/update/${payment.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/payment/delete/${payment.cipher}">
							<li><a href="#ConfirmModal_${payment.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${payment.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Payment?</h3>
							</div>
							<div class="modal-body">
								<p>The Payment will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/payment/delete/${payment.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column> --%>
		</sec:authorize>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete payments?</h3>
		</div>
		<div class="modal-body">
			<p>The selected payment will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>