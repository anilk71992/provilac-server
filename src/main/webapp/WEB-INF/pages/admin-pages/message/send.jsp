<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Message</title>
</head>
<c:url value="/admin/message/sendMessage" var="sendMessage" />
<body>
 <h2>Provilac Message </h2>
	<sec:authorize access="isAnonymous()">
	
	    <c:redirect url="/login"/>
	</sec:authorize>
	<form id="importUser" method="post" action="${sendMessage}" enctype="multipart/form-data">
		<div>
		    <c:if test="${not empty message}">
		        <div id="message" class="success">${message}</div>  
		    </c:if>
		</div>
		<fieldset>
		<div class="panel panel-info" id="primary-info">
							<div class="panel-heading clearfix">
								<h3 class="panel-title ">Message *</h3>
							</div>
							<div class="panel-body">
								<div class="row">
								<div class="col-md-12">
									<textarea name="notificationMessage" class="form-control" id="notificationMessage" rows="11" style="width:90%"  required></textarea>
							</div>
						</div>
							</div>
				</div>
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Excel File*</span>
				<input name="file" id="file" type="file" class="form-control" required />
			</div>
			<br>
		</fieldset>
	
		<a href="<c:url value="/assets/import-templates/Contact_Template.xlsx" />">Download Contact template files from here!!</a>
		<br/>
		<br>
		<button class="btn btn-info" type="submit">Send</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-danger" href="javascript:history.back(1)">Cancel </a>
	</form>
</body>