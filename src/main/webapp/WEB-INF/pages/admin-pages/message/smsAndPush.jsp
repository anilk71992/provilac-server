<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Message</title>
</head>
<script type="text/javascript">
$(document).ready(function() {
	$('#imageBorder').hide();
	$("#route").select2();
	$("#customers").select2();
	getRoutes();
});

function getRoutes(){
	$('.routesClass').html('');
	$.ajax({
		url:'<c:url value="/restapi/route/all"/>',
		dataType:'json',
		success: function(response) {
			if (null != response) {
				if(response.success) {
					/* $('.routesClass').append('<option val="-1">Select Route</option>'); */
					$('.routesClass').append('<option val="All">All</option>');
					routes =   response.data.routes;
					for(var i=0;i<routes.length;i++){
						var route=routes[i];
						 $('.routesClass').append($('<option>', {
		                        value: route.code,
		                        text: route.name
		                 }));
					}	 
				}
			}
		}
	});
}

function getCustomers(){
	var route=$("#route").val();
	$("#routeSelect").val(route);
	var routeSelect=$("#routeSelect").val();
	var status=$("#status").val();
	$("#customers option:selected").removeAttr("selected");
	$("#customers").html('');
	$.ajax({
		url:'<c:url value="/restapi/message/getCustomer"/>',
		data:'status=' + status + '&routeSelect=' + routeSelect,
		dataType:'json',
		success:function(response){
			if(null != response){
				if(response.success){
					var customers=response.data.users;
					$('#customers').append('<option val="All">All</option>');
					for(var i=0; i<customers.length; i++){
						var customer=customers[i];
						$("#customers").append($('<option>',{
							value:customer.id,
							text:customer.mobileNumber +" - "+customer.firstName+" "+customer.lastName
						}));
					}
				}
			}
		}
	});
}

function submitForm(){
	if(validate()){
		$("#sendMessageCustomers").submit();
	}
}

function validate(){
	var customers=$("#customers").val();
	if(customers=="" || customers==null){
		showToast('warning','please select at least one customer');
		return false;
	}
	
	if(!$("#sms").is(":checked")){
		$("#sms").val(false);
	}

	if(!$("#push").is(":checked")){
		$("#push").val(false);
	}

	if(!$("#email").is(":checked")){
		$("#email").val(false);
	}
	

	if(!$("#sms").is(":checked") && !$("#push").is(":checked") && !$("#email").is(":checked") ){
		showToast('warning','please select Type');
		return false;
	}
	if($("#message").val().trim()==""){
		showToast('warning','please enter message');
		return false;
	}
	return true;
}

function validateFileUpload(){
	
	var fuData=document.getElementById("notificationPic");
	var fileUploadPath = fuData.value;
	
	if(fileUploadPath=""){
		alert("Please Upload an Image");
	}else if(fuData.files&&fuData.files[0]){
		var reader =new FileReader();
		reader.onload = function(e){
			$('#displayImage').attr('src',e.target.result);
			$('#imageBorder').show();
		}
		reader.readAsDataURL(fuData.files[0]);
	}
}
</script>
<c:url value="/admin/message/sendByRoute" var="sendMessage" />
<body>
 <h2>Provilac Message </h2>
	<sec:authorize access="isAnonymous()">
	
	    <c:redirect url="/login"/>
	</sec:authorize>
	<form id="sendMessageCustomers" method="post" action="${sendMessage}" modelAttribute="notification" enctype="multipart/form-data">
		<div>
		    <c:if test="${not empty message}">
		        <div id="message" class="success">${message}</div>  
		    </c:if>
		</div>
		<div class="row"> 
		<div class="col-md-3">
		<div class=" input-group">
		<span class="input-group-addon">Routes : </span> 
		<select id="route" name="route" class="routesClass form-control" onchange="getCustomers();">
		<!-- <option value="-1">Select Route</option> -->
		<option value="All">All</option>
		 </select></div>
		
		</div>
		<input id="routeSelect" hidden="true" name="routeSelect"></input>
		<div class="col-md-3">
		<div class=" input-group">
		<span class="input-group-addon">Status:</span>
		<select id="status" name="status" class="statusClass form-control" onchange="getCustomers();"> 
									<!-- <option value="null">Select Status</option> -->
									<option value="All">All</option>
									<option value="NEW">NEW</option>
									<option value="HOLD">HOLD</option>
									<option value="ACTIVE">ACTIVE</option>
									<option value="INACTIVE">INACTIVE</option>
		</select>
		</div>
		</div>
		
		<div class="col-md-6">
		<div class=" input-group">
		<span class="input-group-addon">Customers: </span> 
		<select name="customers[]" id="customers" multiple="multiple" cssClass="form-control" class="form-control">
		<option value="All">All</option></select>
		</div>
		</div>
		
		</div>
		<br>
					<div class="row">
							<div class="col-md-4">
								<div class="input-group width-xlarge">
									 <input type="checkbox" id="push" name="push"> &nbsp;&nbsp;
									 <label for="push" class=""> Push</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group width-xlarge">
									 <input type="checkbox" id="sms" name="sms"> &nbsp;&nbsp;
									 <label for="sms" class=""> SMS</label>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="input-group width-xlarge">
									 <input type="checkbox" id="email" name="email"> &nbsp;&nbsp;
									 <label for="email" class=""> E-Mail</label>
								</div>
							</div>
					</div>
					<br>
						<div class=" input-group">
							<span class="input-group-addon"><b> Title : </b></span>
							<input name="title" class="col-md-12 form-control" id="title" placeholder="Title"/>
						</div>
						<br>
						<div class=" input-group">
							<span class="input-group-addon"><b> Template Name : </b></span>
							<input name="mandrillTemplate" class="col-md-12 form-control" id="mandrillTemplate" placeholder="Template Name"/>
						</div>
						<br>
						<div class="panel panel-info" id="primary-info">
							<div class="panel-heading clearfix">
								<h3 class="panel-title ">Message *</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<textarea name="message" class="form-control" id="message" rows="11" style="width:90%" placeholder="Type a message..." required></textarea>
									</div>
								</div>
							</div>
						</div>
			 <div class="panel panel-info servicePanel"  id="primary-info">
							<div class="panel-heading clearfix">
								<h3 class="panel-title "> Image </h3>
							</div>
							<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
								<fieldset>
									<div class="input-group width-xlarge">
										<div class="row">
											<input name="notificationPic" class="form-control imageClass" onChange="validateFileUpload()" id="notificationPic" type="file" accept="image/*"/>
										</div>
									</div>
									<br>
									<div class="col-md-8" id="imageBorder">
										<img id="displayImage" border="0" height="200" width="300" >
									</div>
								</fieldset>
						   </div>	
					</div>
		<a class="btn btn-info" href="javascript:submitForm();">Send</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-danger" href="javascript:history.back(1)">Cancel </a>
	</form>
</body>