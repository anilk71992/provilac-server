<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/deliverySchedule/add" var="deliveryScheduleAdd" />
<c:url value="/admin/undeliveredOrder/list" var="deliveryScheduleList" />
<c:url value="/admin/deliverySchedule/showCalender" var="deliveryScheduleShowCalender" />
<body>

<div class="row">
	<div class="span4">
	<a class="btn btn-primary" href="${deliveryScheduleShowCalender}">Show Calender</a>&nbsp;
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>