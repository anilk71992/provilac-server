<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>HelloDhobi - Update User</title>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>

<c:url value="/admin/undeliveredOrder/update" var="deliveryScheduleUpdate" />
<c:url value="/admin/undeliveredOrder/list" var="deliveryScheduleList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/undeliveredOrder/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="deliverySchedule" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">DeliverySchedule</h3>
			<form:errors path="customer" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">User*</span>
				<form:select path="customer" cssClass="form-control">
					<c:forEach items="${users}" var="customer">
						<option value="${customer.id}"
							<c:if test="${customer.id == deliverySchedule.customer.id}">selected="selected"</c:if>>${customer.username}</option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="invoice" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">User*</span>
				<form:select path="invoice" cssClass="form-control">
					<c:forEach items="${invoices}" var="invoice">
						<option value="${invoice.id}"
							<c:if test="${invoice.id == deliverySchedule.invoice.id}">selected="selected"</c:if>>${invoice.code}</option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="status" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Status</span> 
				<form:select path="status" cssClass="form-control">
					<form:option value="Delivered">Delivered</form:option>
					<form:option value="NotDelivered">NotDelivered</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="type" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Type</span> 
				<form:select path="type" cssClass="form-control">
					<form:option value="Normal">Normal</form:option>
					<form:option value="Free_Trial">Free_Trial</form:option>
					<form:option value="Paid_Trial">Paid_Trial</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="date" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Date</span>
				<input name="date" id="date" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${deliverySchedule.date}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="deliveryTime" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Delivery Time</span>
				<input name="deliveryTime" id="deliveryTime" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${deliverySchedule.deliveryTime}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="lat" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Lattitude</span>
				<form:input path="lat" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="lng" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Longitude</span>
				<form:input path="lng" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="reason" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Reason</span>
				<form:input path="reason" cssClass="form-control" />
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Update</button>
           	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>