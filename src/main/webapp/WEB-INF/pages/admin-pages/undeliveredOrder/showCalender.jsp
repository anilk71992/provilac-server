<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - DeliverySchedule</title>
<script type="text/javascript">
var events = new Array();
$(document).ready(function(){
	$('#calendar').fullCalendar({
		dayClick: function (date, jsEvent, view) {
	        $(".fc-state-highlight").removeClass("fc-state-highlight");
	        $(view.target).addClass("fc-state-highlight");
	   }
	
		
	});
	$.ajax({
		url: '<c:url value="/routeRecord/getusers"/>',
		dataType: 'json',
		
		success: function(response){
			if(null!=response && response.success){
			var	customers = response.data.customers;
				for(var i = 0; i < customers.length; i++) {
					var customer  = customers[i];
					var x = document.getElementById("customer");
				    var option = document.createElement("option");
				    option.text = customer.firstName + " "+ customer.lastName;
				    option.value = customer.id;
				    x.add(option);
				}
				
			}else {
				alert("Error while retrieving users");
			}
		}
	
	});
});


function onSelectCustomer(){
	var customerId = document.getElementById('customer').value;
	$.ajax({
		url: '<c:url value="/deliverySchedule/getLineItems"/>',
		dataType: 'json',
		data: 'customerId='+customerId,
		
		success: function(response){
			if(null!=response && response.success){
			var	data = response.data;
			for(var i=0;i<data.deliverySchedules.length;i++){
				var deliverySchedule = data.deliverySchedules[i];
				for(var j=0;j<deliverySchedule.deliveryLineItemDTOs.length;j++){
					var lineItem = deliverySchedule.deliveryLineItemDTOs[j];
					var title = lineItem.quantity + " " + lineItem.productName;
					var event = new Object();
					event['title'] = title;
					event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
					event['id'] = lineItem.code;
					event['scheduleId'] = deliverySchedule.id;
					events.push(event);
				}
			}
			initializeCalendar();
			 events = new Array();
			}else {
				alert("Error while retrieving events");
			}
		}
	
	});
 
	
	
}
function initializeCalendar(){
	$('#calendar').empty();
	$('#calendar').fullCalendar({
	      events: events,
	   	   
	});
	
}



</script>
<style type="text/css">
#calendar{
margin-left:   20px;
width: 800px;
height: 800px;
margin-right: 10px;
}

</style>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">DeliverySchedule- Calender</h3>
		</div>
	</div>
	<!-- <div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Calender</h3>
		</div>
		<div class="panel-body" style="margin-left: 20px; height: 1000px;width: col-md-12;margin-right: 50px;">
			<div class="row">
				<div id="calendar" ></div>
			</div>
		</div> 
	</div>
	 -->
	<div class="panel-body" style="margin-left: 10px;">
		<div class="row">
			<div class="col-md-3">
				<!-- <div id="user">User</div> -->
				<div class="input-group width-large">
					<span class="input-group-addon">Customer</span> 
					<select name="customer" id="customer" onchange="onSelectCustomer()" class="form-control">
						<option value="-1">-- Select Customers --</option>
					</select>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="panel panel-primary" id="primary-info">
					<div class="panel-heading">
						<h3 class="panel-title">Calender</h3>
					</div>
					<div class="panel-body"
						style=" height: 670px; width: 800px;">
						<div class="row">
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>