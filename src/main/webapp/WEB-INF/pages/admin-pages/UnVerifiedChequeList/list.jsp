<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Payment</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Payment</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="#">Home</a>
	            </li>
	            <li class="active">
	                <strong>Un Verified Cheques</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/payment/unVerifiedChequelist" />
	<c:set var="urlPrefix" value="/admin/payment/unVerifiedChequelist?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/payment/show" var="paymentDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="payments" class="table" requestURI="" id="payment" export="false">
			<display:column title="Code" sortable="true">
				<a href="${paymentDetailLinkPrefix}/${payment.cipher}">${payment.code}</a>
			</display:column>
			
			<display:column title="Customer" sortable="true">${payment.customer.username}</display:column>
			<display:column title="Cheque Number" sortable="true">${payment.chequeNumber}</display:column>
			<display:column title="Bank Name" sortable="true">${payment.bankName}</display:column>
			<display:column title="TransactionId" sortable="true">${payment.txnId}</display:column>
			<display:column title="Amount" sortable="true">${payment.amount}</display:column>
			<display:column title="Date" sortable="true">${payment.date}</display:column>
			<display:column title="amount" sortable="true">${payment.amount}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/unVerifiedChequePayment/verify/${payment.cipher}"/>">Verify Cheque</a>
						</li>
					</ul>
				</div>
				
			</display:column>
		</display:table>
	</form>

	<!-- modal -->

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>