<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Locality</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Locality</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Locality</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/locality/list" />
	<c:set var="urlPrefix" value="/admin/locality/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/locality/show" var="localityDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="localities" class="footable" requestURI="" id="locality" export="false">
			<display:column title="Code" sortable="true">
				<a href="${localityDetailLinkPrefix}/${locality.cipher}">${locality.code}</a>
			</display:column>
			
			<display:column  title="Name" sortable="true">${locality.name}</display:column>
			<display:column  title="Pincode" sortable="true">${locality.pincode}</display:column>
			<display:column  title="City" sortable="true">${locality.city.name}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/locality/update/${locality.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/locality/delete/${locality.cipher}">
							<li><a href="#ConfirmModal_${locality.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${locality.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Locality?</h3>
							</div>
							<div class="modal-body">
								<p>The Locality will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/locality/delete/${locality.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete localitys?</h3>
		</div>
		<div class="modal-body">
			<p>The selected locality will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>