<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Locality</title>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#city').select2();
});

function submitForm() {
	if(validate()){
		$('#addLocality').submit();
	}
}
function validate() {
	
	var cityValue = $('#city').val();
	if (! cityValue|| cityValue == -1 ) {
		showToast("warning", "Please select city");
		return false;
	}
	
	var locality =$('#name').val(), chardig= /^[a-zA-Z ]*$/;
	var  localityVal=locality.trim();
	if(!chardig.test(locality)|| (!localityVal)){
		showToast('warning','Please Enter Valid Locality');
		return false;
		}
	$('#name').val(localityVal);
	
	var reg=/^\d{6}$/;
	var pincode=$("#pincode").val().trim();
		if(!reg.test(pincode)){
			showToast('warning','Please enter valid pincode (6 Digits )');
			showToast('warning','This Pincode is Not Valid:-'+pincode);
			$($("#pincode")[i]).focus();
			return false;
		}
		
	return true;
}
</script>
</head>
<c:url value="/admin/locality/add" var="addLocality" />
<c:url value="/admin/locality/list" var="localityList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<form:form id="addLocality" action="${addLocality}" method="post" modelAttribute="locality" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
			<h3 class="page-header">Locality</h3>
			<form:errors path="city" cssClass="text-danger"/>
			<div class="input-group width-xlarge">
			<form:select path="city" class="form-control">
				<option value="-1"> -- Select City-- </option>
				<c:forEach items="${cities}" var="city">
					<form:option value="${city.id}"> ${city.name} </form:option>
				</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Name*</span> 
				<form:input path="name" cssClass="form-control"/>
			</div>
			<br>
			<form:errors path="pincode" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Pincode*</span> 
				<form:input path="pincode" id="pincode" cssClass="form-control"/>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>