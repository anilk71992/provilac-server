<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - New Subscription Request</title>
</head>
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Subscription Request</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>New Subscription Request</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/subscriptionRequest/list" />
	<c:set var="urlPrefix" value="/admin/subscriptionRequest/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/subscriptionRequest/show" var="subscriptionRequestDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="subscriptionRequest" class="footable" requestURI="" id="subscriptionRequest" export="false">
			<display:column title="Code" sortable="true">
				<a href="${subscriptionRequestDetailLinkPrefix}/${subscriptionRequest.cipher}">${subscriptionRequest.code}</a>
			</display:column>
			
			<display:column title="User" sortable="true">${subscriptionRequest.user.firstName} ${subscriptionRequest.user.lastName} - ${subscriptionRequest.user.mobileNumber}</display:column>
			<display:column title="StartDate" sortable="true">
				<fmt:formatDate value="${subscriptionRequest.startDate}" pattern="dd-MM-yyyy" />
			</display:column>
			
			<sec:authorize access="!hasAnyAuthority('ROLE_INTERN','ROLE_CCE')">
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/subscriptionRequest/assignRoute/${subscriptionRequest.user.code}/${subscriptionRequest.code}"/>">Assign Route</a></li>
					<sec:authorize url="/admin/subscriptionRequest/deleteNew/${subscriptionRequest.cipher}">
							<li><a href="#ConfirmModal_${subscriptionRequest.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${subscriptionRequest.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete subscriptionRequest?</h3>
							</div>
							<div class="modal-body">
								<p>The subscriptionRequest will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/subscriptionRequest/deleteNew/${subscriptionRequest.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
			</sec:authorize>
		</display:table>
	</form>

	
</body>
</html>