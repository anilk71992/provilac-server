<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Subscription Request</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Subscription Request- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">User</label>
				<p class="col-md-2">${subscriptionRequest.user.firstName} ${subscriptionRequest.user.lastName} - ${subscriptionRequest.user.mobileNumber}</p>
				<%-- <label class="col-md-2">User Address</label>
				<p class="col-md-2">${subscriptionRequest.user.address.name}</p> --%>
			</div>
		</div>
	</div>

	<div class="panel panel-info" id="Order Line Item-info">
		<div class="panel-heading">
			<h3 class="panel-title">Subscription Request Line Item Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<c:forEach items="${subscriptionRequest.requestLineItems}" var="requestLineItem">
				<fieldset>
					<legend>Request Line Item Id -${requestLineItem.id}</legend>
					<div class="row">
						<label class="col-md-2">Product</label>
						<p class="col-md-2">${requestLineItem.product.name}</p>
						<label class="col-md-2">Quantity</label>
						<p class="col-md-2">${requestLineItem.quantity}</p>
						<label class="col-md-2">Type</label>
						<p class="col-md-2">${requestLineItem.type}</p>
					</div>
					<div class="row">
						<label class="col-md-2">Day</label>
						<p class="col-md-2">${requestLineItem.day}</p>
						<label class="col-md-2">Custom Pattern</label>
						<p class="col-md-2">${requestLineItem.customPattern}</p>
						<%-- <label class="col-md-2">Current Index</label>
					<p class="col-md-2">${requestLineItem.currentIndex}</p> --%>
					</div>
				</fieldset>
			</c:forEach>
		</div>
	</div>

	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${subscriptionRequest.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${subscriptionRequest.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${subscriptionRequest.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${subscriptionRequest.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>