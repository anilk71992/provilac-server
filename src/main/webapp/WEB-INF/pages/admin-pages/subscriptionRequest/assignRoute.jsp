<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Assign Route</title>
<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 1100px;         
    margin: 0.6em;       
}
</style>
<c:set value="${user.address}" var="address"></c:set>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
<script type="text/javascript">

var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer({
	suppressMarkers: true
});
var map;
var latlng;
var routeRecordMap = new Object();
var markers = new Array();
var infoWindows = new Array();
var customers= new Array();
var routeCode ;
var userCode;

$(document).ready(function(){
	
	userCode = $('#user').val();
	var existingRoutes = new Array();
	$.ajax({
		url : '<c:url value="/routeRecord/routes"/>',
		dataType : 'json',
		data : 'customerCode=' + userCode,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					existingRoutes = data.routes;
					showRouteFunction(existingRoutes);
				} else {
					alert(response.error.message);
				}
		}
	});
	
});

function showRouteFunction(existingRoutes){
	$.ajax({
		url : '<c:url value="/restapi/route/fetchRouteByUser"/>',
		dataType : 'json',
		data : 'customerCode=' + userCode,
		success : function(response) {
			if (response.success) {
				var data = response.data;
				$('#route').empty();
				var routeRecords = new Array();
				for(var j=0 ;j< existingRoutes.length;j++){
					var route = existingRoutes[j];
					if(null!=data){
						var route1 = data.route;
						if(route.name==route1.name){
							var html = '<input type="radio" checked="checked" onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" name="selectedRoute" class="clearSelection" id="route_'+ j	+ '" value="'+ route.code+ '" />'
								+ '<label>&nbsp;'+ route.name + '</label> <br>';
							$('#route').append(html);
						}else{
							var html = '<input type="radio"  onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" name="selectedRoute"  class="clearSelection"  id="route_'+ j	+ '" value="'+ route.code+ '" />'
							+ '<label>&nbsp;' + route.name + '</label> <br>';
							$('#route').append(html);
						}
					}else{
						var html = '<input type="radio"  onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" name="selectedRoute"  class="clearSelection"  id="route_'+ j	+ '" value="'+ route.code+ '" />'
						+ '<label>&nbsp;' + route.name + '</label> <br>';
						$('#route').append(html);
					}
				
					routeRecordMap[route.code] = route.routeRecordDTOs;
				}
				if(null!=data){
				 var priorityOfSelectedCustomer = 0; ;	
				customers= new Array();
				for (var i = 0; i < data.route.routeRecordDTOs.length; i++) {
					var customer = data.route.routeRecordDTOs[i].customerDTO;
					 customers[i]=customer;
					if(customer.code==userCode){
						priorityOfSelectedCustomer = data.route.routeRecordDTOs[i].priority; 
					}
				}
				 routeRecords = data.route.routeRecordDTOs;
				routeRecords.sort(function(a, b){
					 return a.priority-b.priority
					}); 
				}
				
				if(routeRecords.length!=0){
				 	for (var i = 0; i < routeRecords.length; i++) {
					 
				 		var customer = routeRecords[i].customerDTO;
						customers[i]=customer;
						var priority = data.route.routeRecordDTOs[i].priority;
					 	if(customer.code !=userCode){
					 	if((priorityOfSelectedCustomer-1)==priority){
							var html = '<input type="radio" checked="checked" name="selectedCustomer" onchange="getSelecteRouteAndCustomer('+ i + ')"  id="customer_'+ i	+ '" value="'+ customer.code + '" />'+
										'<label>&nbsp;'	+ customer.firstName+ " "+ customer.lastName + " - "+ customer.address + '</label> <br>';
							$('#customer').append(html);
					  	}else{
							var html = '<input type="radio" name="selectedCustomer" onchange="getSelecteRouteAndCustomer('+ i + ')"  id="customer_'+ i	+ '" value="'+ customer.code + '" />'+
							'<label>&nbsp;'	+ customer.firstName+ " "+ customer.lastName + " - "+ customer.address + '</label> <br>';
							$('#customer').append(html);
						 }
					   }
					}
				}
			
			mapFunction();
			} else {
				alert(response.error.message);
			}
		}
	});
}
function submitForm() {
	if(validate())
		$('#changeRoute').submit();
}

function validate() {
	return true;	
}
</script>
</head>

<c:url value="/admin/subscriptionRequest/assignRoute" var="routeChange" />
<c:url value="/admin/user/list" var="routeList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/subscriptionRequest/assignRoute" var="changeRoute"/>
	<form id="changeRoute" method="post" action="${changeRoute}">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">Change Route</h3>
			</div>
		</div>
		<div class="panel panel-primary" id="primary-info">
			<div class="panel-heading">
				<h3 class="panel-title">Map</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<div class="col-md-6">
					<div class="input-group width-xlarge">
						 <input type="hidden" name="user" id="user" readonly="readonly" value="${user.code}" class="form-control" />
					</div>
					<br>
					<div class="input-group width-xlarge" id="latDiv">
							<input type="hidden" name="lat" id="latId" readonly="readonly" value="${user.lat}" class="form-control" />
						</div>
						<br>
						<div class="input-group width-xlarge" id="lngDiv">
							<input type="hidden" name="lng" id="lngId" readonly="readonly" value="${user.lng}" class="form-control" />
						</div>
						<br>
						<div class="input-group width-xlarge" id="requestDiv">
							<input type="hidden" name="requestCode" id="requestCode" readonly="readonly" value="${request.code}" class="form-control" />
						</div>
						<br>
				</div>
			</div>
				<br>
				<div class="input-group width-xlarge">
				<input type="hidden" name="routeName" id="modalRoute"class="form-control" />
				</div>
				<br>
				<div id="map_canvas"></div>
			</div>
			<a style="margin-left: 30px;margin-bottom: 10px;" class="btn btn-info" href="javascript:onSuggestRouteClick();">Suggest Route</a> &nbsp;
			<a style="margin-left: 30px;margin-bottom: 10px;" class="btn btn-info" href="javascript:addNewRoute();">Add New Route</a> <br />
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Routes</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group width-xxlarge" id="route" ></div>
					</div>
					<div id="customer" class="col-md-6"></div>
					<div class="row">
					<div class="col-md-12">
					  <a class="btn btn-danger btn-xs pull-right" href="javascript:clearSelectedValues();">Clear Selection</a>
					</div>
				</div>
				</div>
			</div>
		</div>
		<a class="btn btn-info" href="javascript:submitForm();" >Change Route</a>&nbsp;&nbsp;&nbsp;&nbsp;
	</form>
	<div class="modal" id="ConfirmModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"  onclick="hideModel('ConfirmModal')">�</button>
					<h3 id="myModalLabel" style="margin-left: 60px">Add Route</h3>
				</div>
				<div class="modal-body" style="margin-left: 70px">
					<p>No Routes  found!</p>
				</div>
				<div>
				<a class="btn btn-info" href="javascript:addRoute();" style="margin-left: 80px;">Add Route</a> <br />
				</div>
				<br>
				<div class="modal-footer">
					<!-- <button class="btn" data-dismiss="modal" onclick="hideModel('ConfirmModal')">Cancel</button> -->
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="addRouteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="hideModel('addRouteModal')">�</button>
					<h3 id="myModalLabel" style="margin-left: 40px">Add Route</h3>
				</div>
				<div>
					<div class="input-group width-xlarge" style="margin-left: 40px;margin-top: 20px">
						<span class="input-group-addon">Route</span> <input name="route1" id="route1" class="form-control" />
					</div>
					<br>
					<div id="errorMessage" class="input-group width-xlarge" style="margin-left: 40px;">
					</div>
					<br>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" onclick="showRoutes();">OK</button>
						<button class="btn" data-dismiss="modal"onclick="hideModel('addRouteModal')">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">

	function mapFunction(){
		
		var  lat = document.getElementById('latId').value, 
			 lng = document.getElementById('lngId').value;	
		
		if(lat == "" && lng == ""){
			document.getElementById('latId').value = "18.5203";
			lng = document.getElementById('lngId').value = "73.8567";
			lat=18.5203; 
			lng=73.8567;
		}
		
		latlng = new google.maps.LatLng(lat, lng),
		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

	
	map = new google.maps.Map(document.getElementById('map_canvas'), {
	  zoom: 6,
	  center: {lat: 18.5203, lng: 73.8567}
	});
	directionsDisplay.setMap(map);
	
	google.maps.event.addDomListener(window, "load", map);
	
	var marker = new google.maps.Marker({
		position : latlng,
		map : map,
		icon : image,
		draggable : true
	});
	directionsDisplay.setMap(map);
	
	 drawRoute();  
		
}
	function onSuggestRouteClick() {
		var lat = document.getElementById('latId').value;
		var lng = document.getElementById('lngId').value;
		$('#customer').empty();
		$.ajax({
					url : '<c:url value="/routeRecord/routes"/>',
					dataType : 'json',
					data : 'lat=' + lat + '&lng=' + lng,
					success : function(response) {
						if (response.data.routes.length != 0) {
							if (response.success) {
								var data = response.data;
								routes1 = data.routes;
								$('#route').empty();

								for (var i = 0; i < data.routes.length; i++) {
									var route = data.routes[i];
									var html = '<input type="radio" onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" class="clearSelection"  name="selectedRoute" id="route_'+ i	+ '" value="'+ route.code+ '" />'
											+ '<label>&nbsp;'
											+ route.name + '</label> <br>';
									$('#route').append(html);
									position=i;
									routeRecordMap[route.code] = route.routeRecordDTOs;
								}

							} else {
								alert(response.error.message);
							}
						} else {
							$('#ConfirmModal').show();
						}
					}
				});
	}

	function addRoute() {
		$('#ConfirmModal').hide();
		$('#addRouteModal').show();
	}
	function addNewRoute() {
		$('#route1').val("");
		$('#errorMessage').empty();
		$('#addRouteModal').show();
	}
	function showRoutes() {
		var lat = $("#latId").val();
		var lng = $("#lngId").val();
		
		if(lat == "" || lng == ""){
			showToast('warning','Select Latitude or Longitude');
			return false;
		}
		var routeName = document.getElementById('route1').value;
		$.ajax({
			url : '<c:url value="/routeRecord/routes"/>',
			dataType : 'json',
			data : 'lat=' + lat + '&lng=' + lng + '&routeName=' + routeName,
			success : function(response) {
					if (response.success) {
						var data = response.data;
						routes1 = data.routes;
						$('#route').empty();
						$('#customer').empty();
						routeRecordMap = new Object();
						for (var i = 0; i < data.routes.length; i++) {
							var route = data.routes[i];
							if(route.name==routeName){
								var html = '<input type="radio" checked="checked" onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" class="clearSelection" name="selectedRoute" id="route_'+ i	+ '" value="'+ route.code+ '" />'
									+ '<label>&nbsp;'
									+ route.name + '</label> <br>';
								$('#route').append(html);
							}else{
								var html = '<input type="radio"  onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" class="clearSelection" name="selectedRoute" id="route_'+ i	+ '" value="'+ route.code+ '" />'
								+ '<label>&nbsp;'
								+ route.name + '</label> <br>';
							$('#route').append(html);
							}
							routeRecordMap[route.code] = route.routeRecordDTOs;
						}
						$('#addRouteModal').hide();
						
					} else {
						$('#errorMessage').empty();
						/* var html = '<label>&nbsp;'+ response.error.message + '</label> <br>'; */
						 var html= '<font size="3" color="red">' + response.error.message  + '</font>';
						$('#errorMessage').append(html);
					}
			}
		});
		
	}
	
	function onRouteSelect(code) {

		$('#customer').empty();
		var userCode = $('#user').val();
		removeAllMarkers();
		var routeRecords = routeRecordMap[code];
		routeRecords.sort(function(a, b){
			 return a.priority-b.priority
			});
		customers= new Array();
		var priorityOfSelectedCustomer = 0 ;	
		customers= new Array();
		for (var i = 0; i < routeRecords.length; i++) {
			var customer = routeRecords[i].customerDTO;
			
			if(customer.code==userCode){
				
				priorityOfSelectedCustomer = routeRecords[i].priority; 
			}
		}
		for (var i = 0; i < routeRecords.length; i++) {
			
			var customer = routeRecords[i].customerDTO;
			customers[i]=customer;
			var priority = routeRecords[i].priority;
			 if(customer.code !=userCode){
			 if((priorityOfSelectedCustomer-1)==priority){
				var html = '<input type="radio" checked="checked" name="selectedCustomer" onchange="getSelecteRouteAndCustomer('+ i + ')"  id="customer_'+ i	+ '" value="'+ customer.code + '" />'+
				'<label>&nbsp;'	+ customer.firstName+ " "+ customer.lastName + " - "+ customer.address + '</label> <br>';
				$('#customer').append(html);
			  }else{
					var html = '<input type="radio" name="selectedCustomer" onchange="getSelecteRouteAndCustomer('+ i + ')"  id="customer_'+ i	+ '" value="'+ customer.code + '" />'+
					'<label>&nbsp;'	+ customer.firstName+ " "+ customer.lastName + " - "+ customer.address + '</label> <br>';
					$('#customer').append(html);
				 }
			 }
		}
		
		drawRoute();
	}

	function drawRoute() {
		var wayPoints = [];
		
		directionsDisplay.setMap(null);
		if(customers.length <= 0){
			return;
		}
		
		for(var i = 0; i < 7; i++) {
			var customer = customers[i];
			if(customer.lat!=null && customer.lng!=null){
				wayPoints.push({
					location: new google.maps.LatLng(customer.lat, customer.lng),
			    	stopover: true
				});
   		  		var start = wayPoints[0].location;
		  		var end = wayPoints[wayPoints.length-1].location;
			}else{
				return;
			}
		}
			var request= {
				 origin: start,
				    destination: end,
				    waypoints: wayPoints,
				    travelMode: google.maps.TravelMode.DRIVING
			};
			directionsService.route(request, function(response, status) {
			      if (status == google.maps.DirectionsStatus.OK) {
			        directionsDisplay.setDirections(response);
			        directionsDisplay.setMap(map);
			        addMarkers();
			      } else {
			        alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
			      }
			});
			
	}

	function addMarkers() {
		for(var i = 0; i < customers.length; i++) {
			var customer = customers[i];
			
// 			var infowindow = new google.maps.InfoWindow();
			
			var marker = new google.maps.Marker({
		        position: new google.maps.LatLng(customer.lat, customer.lng),
		        map: map,
		        title: customer.firstName + ' ' + customer.lastName
		    });
			
// 				infowindow.open(map, marker);
				markers.push(marker);
				
// 				infoWindows.push(infowindow);
		}
	}

	function removeAllMarkers() {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
		markers = new Array();
		infoWindows = new Array();
	}
	var index;
	var customerCode;
	
	function clearSelectedValues(){
		$('#customer').empty();
		$(".clearSelection").attr('checked', false);
	}
</script>
</body>
</html>