<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - DeliverySheet</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>DeliverySheet</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>DeliverySheet</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/deliverySheet/list" />
	<c:url var="urlDeliverySheetsExport" value="/admin/deliverySheet/exportForToday" />
	<c:set var="urlPrefix" value="/admin/deliverySheet/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/deliverySheet/show" var="deliverySheetDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="deliverySheets" class="footable" requestURI="" id="deliverySheet" export="false">
			<display:column title="Code" sortable="true">
				<a href="${deliverySheetDetailLinkPrefix}/${deliverySheet.cipher}">${deliverySheet.code}</a>
			</display:column>
			<display:column title="Route Name" sortable="true">${deliverySheet.userRoute.route.name}</display:column>
			<display:column title="Delivery Boy" sortable="true">${deliverySheet.userRoute.deliveryBoy.firstName} ${deliverySheet.userRoute.deliveryBoy.lastName}</display:column>
			<display:column title="Date" sortable="true">${deliverySheet.date}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/deliverySheet/update/${deliverySheet.cipher}"/>">Mark Status</a>
						</li>
						<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
							<li><a href="<c:url value="/admin/deliverySheet/export/${deliverySheet.code}"/>">Export</a>
							</li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${deliverySheet.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete DeliverySheet?</h3>
							</div>
							<div class="modal-body">
								<p>The DeliverySheet will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/deliverySheet/delete/${deliverySheet.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete deliverySheets?</h3>
		</div>
		<div class="modal-body">
			<p>The selected deliverySheet will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>