<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Delivery Sheet</title>
<style>
tr{
 height: 20px;
}
td {
	width: 350px;
	padding: 0 5px 0 5px;
}

</style>
<script type="text/javascript">
var deliverySchedules = new Array();
<c:forEach items="${deliverySheet.deliverySchedules}" var="schedule">
	var record = new Object();
	record['id']= "${schedule.id}"; 
	deliverySchedules.push(record);
</c:forEach>

$(document).ready(function() {
	for (var i = 0; i < deliverySchedules.length; i++) {
		var schedule = deliverySchedules[i];
		 onStatusChange(schedule.id);
	}
});

function onStatusChange(scheduleId) {
	var statusSelect= document.getElementById('statusId_'+scheduleId);
	var onStatus = statusSelect.options[statusSelect.selectedIndex].getAttribute('numVal');
	if(onStatus==2){
		$("#reasonTd_"+scheduleId).show();	
	}else{
		$("#reasonTd_"+scheduleId).hide();	
	}
}


function submitForm() {
	if(validate()){
		$('#updateDeiverySheet').submit();
	}
}

function validate(){
	for (var i = 0; i < deliverySchedules.length; i++) {
		var schedule = deliverySchedules[i];
		 var statusSelect= document.getElementById('statusId_'+schedule.id);
		var onStatus = statusSelect.options[statusSelect.selectedIndex].getAttribute('numVal');
		if(onStatus==2){
			if(!$('#reason_'+schedule.id).val() || $('#reason_'+schedule.id).val().trim()==""){
				showToast('warning','Please add note for not delivered deliveryschedule.');
				return false;
			}
		}
	}
	return true;
}
</script>
</head>

<c:url value="/admin/deliverySheet/update" var="deliverySheetUpdate" />
<c:url value="/admin/deliverySheet/list" var="deliverySheetList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/deliverySheet/update" var="updateDeliverySheet"/>
	<form:form id="updateDeiverySheet" method="post" modelAttribute="deliverySheet" action="${updateDeliverySheet}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<c:choose> 
			<c:when test="${not empty deliverySheet.deliverySchedules}">
				<table cellSpacing ="10">
					<thead>
						<tr>
							<th> Code </th>
							<th> Customer </th>
							<th> Products</th>
							<th> Address </th>
							<th> Status </th>
							<th id="reasonId "> Reason </th>
							
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${deliverySheet.deliverySchedules}" var="schedule">
							<tr>
								<td>
									<input type="text" name="code" value="${schedule.code}" placeholder="code" readonly="true" class="form-control">
								</td>
								<td>
									<input type="text" name="customer" value="${schedule.customer.fullName}" placeholder="user" readonly="true" class="form-control">
								</td>
								<td>
									<c:set var="procuctNames"></c:set>
									<c:forEach items="${schedule.deliveryLineItems}" var="lineItem" varStatus="status">
										<c:choose>
										    <c:when test="${empty procuctNames}" >
										       <c:set var="procuctNames" value="${lineItem.product.name}"></c:set>
										    </c:when>
										    <c:otherwise>
										       <c:set var="procuctNames" value="${procuctNames},${lineItem.product.name}"></c:set>
										    </c:otherwise>
										</c:choose>
									</c:forEach>
									<input type="text" name="products" value="${procuctNames}" placeholder="products" readonly="true" class="form-control">
								</td>
								<td>
								 	<input type="text" name="address" value="${schedule.customer.address}" placeholder="address"   readonly="true" class="form-control">
								</td>
								<td>
									<select name="status" id="statusId_${schedule.id}" class="form-control"  onchange="onStatusChange(${schedule.id})" >
										<option value="Delivered" numVal="1" <c:if test="${schedule.status == 'Delivered'}">selected='selected'</c:if> >Delivered</option>
										<option value="NotDelivered" numVal="2" <c:if test="${schedule.status == 'NotDelivered'}">selected='selected'</c:if>  >NotDelivered</option>
									</select>
								</td>
								<td id="reasonTd_${schedule.id}" class="reasonClass">
									<input type="text" name="reason" value="${schedule.reason}" id="reason_${schedule.id}" placeholder="reason" class="form-control reasonClass" hidden="true">	
								</td>
							</tr>
								
							<tr>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<span>Fields marked with * are mandatory</span>
				</c:when>
				<c:otherwise>
					<script type="text/javascript">
					showToast("warning","No Schedules Found!!!");
					</script>
					No Schedules Found!!
				</c:otherwise>
			</c:choose>
		<br />
		<br />
		<c:if  test="${not empty deliverySheet.deliverySchedules}">
			<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
		</c:if>
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>