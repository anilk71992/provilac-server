<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Address</title>
<c:set value="${address.customer }" var="customer"></c:set>
<script type="text/javascript">

$(document).ready(function() {
	$('#customer').select2();
	$('#city').select2();
	$('#locality').select2();
	});

function onCitySelect(){
	var cityId = $('#city').val();
	 $(".localityClass").select2("val", "");
	$.ajax({
	     url:'<c:url value="/restapi/locality/byCity"/>',
	     datatype:'json',
	     data: 'cityId='+cityId,
	     success: function(response) {
	         if(response.success){
	            var localities = response.data.localities;
	             
	            $('.localityClass').html("");
	            if(localities.length !=0){
	            for(var i = 0; i < localities.length; i++) {
	                var locality  = localities[i];
	                $('.localityClass').append($('<option>', {
	                    value: locality.id,
	                    text: locality.name
	                }));
	            }
	           
	          }
	            $(".localityClass").select2("val", "");
	         }else {
	                showToast("warning","No locality found for selected city!!");
	         }
	         
	 }
	 });
	}
function submitForm() {
	if(validate()){
		$('#updateAddress').submit();
	}
}
function validate(){
	
	if($('#city').val()=="-1" || $('#city').val()==" "){
		showToast('warning','Please select city');
		return false;
	}
	
	if($('#locality').val()=="-1" || !$('#locality').val()){
		showToast('warning','Please select Locality');
		return false;
	}
	
	if(!$('#name').val() || $('#name').val() == ""){
		showToast('warning','Please enter address name');
		return false;
	}else{
		$('#name').val($('#name').val().trim());
	}
	if($('#addressLine1Id').val() =="" || !$('#addressLine1Id').val()){
		showToast('warning','Please enter address line 1');
		return false;
	}
	
	return true;
	
}
</script>
<style type="text/css">
#map_canvas {         
    height: 500px;         
    width: 1250px;         
    margin: 0.6em;       
}
</style>
</head>

<c:url value="/admin/address/update" var="addressUpdate" />
<c:url value="/admin/address/list" var="addressList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<form:form id="updateAddress" method="post" modelAttribute="address" action="${addressUpdate}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">Customer Address</h3>
			<div class="row">
				<div class="col-md-6">
						<form:errors path="customer" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Customer</span> <input
								name="user" value="${customer.firstName} ${customer.lastName} ${customer.mobileNumber} " readonly="true"
								class="form-control" /> <input type="hidden" name="selectedCustomer"
								value="${customer.id}" readonly="true">
						</div>
						<br>

					<form:errors path="city" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">City</span>
						<select id="city" name="city"  class="form-control" onchange="onCitySelect();">
							<option value="-1">Select City</option> 
							<c:forEach items="${cities}" var="city">
								<option value="${city.id}" <c:if test="${city.id==address.city.id}" > selected="selected"</c:if>>${city.name}</option> 
							</c:forEach>
						</select>
						</div>
						<br>
						
					<form:errors path="name" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Name *</span>
						<form:input path="name" id="name"
							cssClass="form-control" />
					</div>
					<br>
					
					<form:errors path="landmark" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Landmark</span>
						<form:input path="landmark" cssClass="form-control" />
					</div>
					<br>
					
					<form:errors path="isDefault" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Default</span>
						<form:select path="isDefault" cssClass="form-control">
							<form:option value="true">Yes</form:option>
							<form:option value="false">No</form:option>
						</form:select>
					</div>
					<br>
					<form:errors path="locality" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Locality *</span>
						<form:input path="locality" id="locality"
							cssClass="form-control" />
					</div>
					<br>
				</div>
				<div class="col-md-6">
					
						<form:errors path="pincode" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Pincode *</span>
						<form:input path="pincode" id="pincode"
							cssClass="form-control" />
					</div>
					<br>
					<form:errors path="addressLine1" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">AddressLine1 *</span>
						<form:input path="addressLine1" id="addressLine1Id"
							cssClass="form-control" />
					</div>
					<br>
					<form:errors path="addressLine2" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">AddressLine2</span>
						<form:input path="addressLine2" id="addressLine2Id"
							cssClass="form-control" />
					</div>
					<br>
					<div id="latlngDiv">
						<form:errors path="lat" cssClass="text-danger" />
						<div class="input-group width-xlarge">
						<span class="input-group-addon">lat</span>
							<form:input path="lat" cssClass="form-control" readonly="true"/>
						</div>
						<br>
						<form:errors path="lng" cssClass="text-danger" />
						<div class="input-group width-xlarge">
						<span class="input-group-addon">lng</span>
							<form:input path="lng" cssClass="form-control"  readonly="true"/>
						</div>
						<br>
						</div>
					<br> <a class="btn btn-info" href="javascript:findLocation();">Find
						location On Map</a>
					
				</div>

				</div>
			<div class="row">
				<div id="map_canvas"></div>
			</div>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>
	<script
		src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
	<c:set value="${address.lat}" var="latVal"></c:set>
	<c:set value="${address.lng}" var="lngVal"></c:set>
	<script type="text/javascript">
	var lat = "${latVal}";
	var	lng = "${lngVal}";
	if(lat=="" && lng==""){
		lat = 18.5203;
		lng = 73.8567;
	}
	var latlng = new google.maps.LatLng(lat, lng),
		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';
		
	var geocoder = new google.maps.Geocoder();
	var directionsService = new google.maps.DirectionsService;

	var directionsDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers: true
	});
	var map = new google.maps.Map(document.getElementById('map_canvas'), {
		center : new google.maps.LatLng(lat, lng),
		zoom : 15,
	});
	
	directionsDisplay.setMap(map);
	var marker = new google.maps.Marker({
		position:latlng,
		map : map,
		icon : image,
		draggable : true
	});

	google.maps.event.addListener(map, 'click', function(event) {
		 marker.setMap(map);  
		placeMarker(event.latLng);
		});

	function placeMarker(location) {
	  if ( marker ) {
	    marker.setPosition(location);
	  } else {
	    marker = new google.maps.Marker({
	      position: location,
	      map: map
	    });
	  }
	}
	
	directionsDisplay.setMap(map);
	google.maps.event.addListener(marker, 'position_changed', function() {
		marker.setMap(map);
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		$('#latId').val(marker.getPosition().lat());
		$('#lngId').val(marker.getPosition().lng());
	});
	
	function findLocation(){
		if (($('#addressLine1Id').val()==" "||!$('#addressLine1Id').val())&&($('#addressLine2Id').val()=="" ||!$('#addressLine2Id').val())) {
			showToast('error','Please Enter addressField1 and addressField2');
			$('#latId').val("");
			$('#lngId').val("");
			marker.setMap(null);
			return false;
		}
		var user1Location = $('#addressLine1Id').val()+ $('#addressLine2Id').val();
	  geocoder.geocode( { 'address': user1Location}, function(results, status) {
	      if (status == 'OK') {
	    	  marker.setMap(map);
	        map.setCenter(results[0].geometry.location);
	        marker.setPosition(results[0].geometry.location);
	    	 $('#latId').val(results[0].geometry.location.lat());
	         $('#lngId').val(results[0].geometry.location.lng());
	         placeMarker(results[0].geometry.location);
	         map.setZoom(15);
	      }else{
	    	  showToast('error','Lattitude and Logitude for entered position are not found, please select nearest location');
	           	$('#latId').val("");
	    		$('#lngId').val("");
	    		marker.setMap(null);
	    		return false;
	      }
	      
	  });
	  
	}
</script>
</body>