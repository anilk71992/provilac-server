<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Address</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Address - Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Customer</label>
				<p class="col-md-2">${address.customer.firstName} ${address.customer.lastName } ${address.customer.mobileNumber }</p>
				<label class="col-md-2">LandMark</label>
				<p class="col-md-2">${address.landmark}</p>
				<label class="col-md-2">Locality</label>
				<p class="col-md-2">${address.locality}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${address.name}</p>
				<label class="col-md-2">AddressLine1</label>
				<p class="col-md-2">${address.addressLine1}</p>
				<label class="col-md-2">AddressLine2</label>
				<p class="col-md-2">${address.addressLine2}</p>
			</div>
			<div class="row">
			<label class="col-md-2">lat</label>
				<p class="col-md-2">${address.lat}</p>
				<label class="col-md-2">lng</label>
				<p class="col-md-2">${address.lng}</p>
				<label class="col-md-2">Default</label>
				<p class="col-md-2">${address.isDefault}</p>
			</div>
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${address.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${address.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${address.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${address.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>