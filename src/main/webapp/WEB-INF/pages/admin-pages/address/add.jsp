<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Address</title>
<c:set value="${customer}" var="customer"></c:set>
<script type="text/javascript">

$(document).ready(function() {
	var customer1 = "${customer}";
	$('#customer').select2();
	$('#city').select2();
	$('#locality').select2();
	//$('#latlngDiv').hide();
	if(customer1 != ""){
		$('#customerDiv').hide();
	}else{
		$('#customerDiv').show();
	}
	
});


function submitForm() {
	if(validate()){
		$('#addAddress').submit();
	}
}
 function onCitySelect(){
var cityId = $('#city').val();

$.ajax({
     url:'<c:url value="/restapi/locality/byCity"/>',
     datatype:'json',
     data: 'cityId='+cityId,
     success: function(response) {
         if(response.success){
            var localities = response.data.localities;
             
            $('.localityClass').html("");
            if(localities.length !=0){
            for(var i = 0; i < localities.length; i++) {
                var locality  = localities[i];
                $('.localityClass').append($('<option>', {
                    value: locality.id,
                    text: locality.name
                }));
            }
            $("#locality").select2("val", "");
          }
         }else {
                showToast("warning","No pincodes found for selected city!!");
         }
         
 }
 });
}

function validate(){
	
	if($('#city').val()=="-1" || $('#city').val()==" "){
		showToast('warning','Please select city');
		return false;
	}
	
	if($('#locality').val()=="-1" || !$('#locality').val()){
		showToast('warning','Please select Locality');
		return false;
	}
	
	if(!$('#name').val() || $('#name').val() == ""){
		showToast('warning','Please enter address name');
		return false;
	}else{
		$('#name').val($('#name').val().trim());
	}
	if($('#addressLine1Id').val() =="" || !$('#addressLine1Id').val()){
		showToast('warning','Please enter address line 1');
		return false;
	}
	
	return true;
	
}
</script>
<style type="text/css">
#map_canvas {         
    height: 500px;         
    width: 1250px;         
    margin: 0.6em;       
}
</style>
</head>
<c:url value="/admin/address/add" var="addressAdd" />
<c:url value="/admin/address/list" var="addressList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<form:form id="addAddress" action="${addressAdd}" method="post" modelAttribute="address" enctype="application/x-www-form-urlencoded">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">Customer Address</h3>
			<div class="row">
				<div class="col-md-6">
					<div id="customerDiv">
						<form:errors path="customer" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Customer</span>
							<select id="customer" name="customer"  class="form-control">
							<c:forEach items="${users}" var="customer">
							<option value="${customer.id}">${customer.firstName}-${customer.lastName}-${customer.mobileNumber}</option> 
							</c:forEach>
							</select>
						</div>
						<br>
					</div>
				<form:errors path="city" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">City</span>
						<select id="city" name="city"  class="form-control" onchange="onCitySelect();">
							<option value="-1">Select City</option> 
							<c:forEach items="${cities}" var="city">
								<option value="${city.id}">${city.name}</option> 
							</c:forEach>
						</select>
						</div>
						<br>
						
					<form:errors path="name" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Name *</span>
						<form:input path="name" id="name"
							cssClass="form-control" />
					</div>
					<br>
					
					<form:errors path="landmark" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Landmark</span>
						<form:input path="landmark" cssClass="form-control" />
					</div>
					<br>
						
					<form:errors path="isDefault" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Default</span>
						<form:select path="isDefault" cssClass="form-control">
							<form:option value="true">Yes</form:option>
							<form:option value="false">No</form:option>
						</form:select>
					</div>
					<br>
					<form:errors path="locality" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Locality *</span>
						<form:input path="locality" id="locality"
							cssClass="form-control" />
					</div>
					<br>
				</div>
				<div class="col-md-6">
				
						<form:errors path="pincode" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Pincode *</span>
						<form:input path="pincode" id="pincode"
							cssClass="form-control" />
					</div>
					<br>
					<form:errors path="addressLine1" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">AddressLine1 *</span>
						<form:input path="addressLine1" id="addressLine1Id"
							cssClass="form-control" />
					</div>
					<br>
					<form:errors path="addressLine2" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">AddressLine2</span>
						<form:input path="addressLine2" id="addressLine2Id"
							cssClass="form-control" />
					</div>
					<br>
					<div id="latlngDiv">
						<form:errors path="lat" cssClass="text-danger" />
						<div class="input-group width-xlarge">
						<span class="input-group-addon">lat</span>
							<form:input path="lat" cssClass="form-control" readonly="true"/>
						</div>
						<br>
						<form:errors path="lng" cssClass="text-danger" />
						<div class="input-group width-xlarge">
						<span class="input-group-addon">lng</span>
							<form:input path="lng" cssClass="form-control"  readonly="true"/>
						</div>
						<br>
						</div>
					<br> <a class="btn btn-info" href="javascript:findLocation();">Find
						location On Map</a>
					
				</div>
			</div>
			<div class="row">
				<div id="map_canvas"></div>
			</div>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
	

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
	<script type="text/javascript">
	
	var lat = 18.5203, 
		lng = 73.8567, latlng = new google.maps.LatLng(lat, lng),
		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';

	var geocoder = new google.maps.Geocoder();
	var directionsService = new google.maps.DirectionsService;

	var directionsDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers: true
	});
	var map = new google.maps.Map(document.getElementById('map_canvas'), {
		center : new google.maps.LatLng(lat, lng),
		zoom : 15,
	});
	
	var marker = new google.maps.Marker({
		position : latlng,
		map : map,
		icon : image,
		draggable : true
	});
	directionsDisplay.setMap(map);

	google.maps.event.addListener(marker, 'position_changed', function() {
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		
		geocodePosition(marker.getPosition());
		$('#lat').val(lat);
		$('#lng').val(lng);
	});
	
	function geocodePosition(pos) {
	  geocoder.geocode({
	    latLng: pos
	  }, function(responses) {
	    if (responses && responses.length > 0) {
	    	$("#searchTextField").val(responses[0].formatted_address);
	    }
	  });
	}
	function findLocation(){
		if (($('#addressLine1Id').val()==" "||!$('#addressLine1Id').val())&&($('#addressLine2Id').val()=="" ||!$('#addressLine2Id').val())) {
			showToast('error','Please Enter addressField1 and addressField2');
			$('#lat').val("");
			$('#lng').val("");
			marker.setMap(null);
			return false;
		}
		
	 var user1Location = $('#addressLine1Id').val() + $('#addressLine2Id').val();
	  geocoder.geocode( { 'address': user1Location}, function(results, status) {
	      if (status == 'OK') {
	         marker.setMap(map);
		        map.setCenter(results[0].geometry.location);
		        marker.setPosition(results[0].geometry.location);
		    	 $('#lat').val(results[0].geometry.location.lat());
		         $('#lng').val(results[0].geometry.location.lng());
		         map.setZoom(15);
	      }else{
	    	  showToast('error','Lattitude and Logitude for entered position are not found, please select nearest location');
	           	$('#lat').val("");
	    		$('#lng').val("");
	    		marker.setMap(null);
	    		return false;
	      }
	      
	  });
	  
	}
</script>


</body>