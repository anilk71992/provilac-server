<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Address</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Customer Address</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Address</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/address/list" />
	<c:set var="urlPrefix" value="/admin/address/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/address/show" var="addressDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="addresses" class="footable" requestURI="" id="address" export="false">
			<display:column title="Code" sortable="true">
				<a href="${addressDetailLinkPrefix}/${address.cipher}">${address.code}</a>
			</display:column>
			
			<display:column title="Customer Name" sortable="true">${address.customer.firstName} ${address.customer.lastName}-${address.customer.mobileNumber}</display:column>
			<display:column title="Name" sortable="true">${address.name}</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/address/update/${address.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/address/delete/${address.cipher}">
							<li><a href="#ConfirmModal_${address.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${address.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Address?</h3>
							</div>
							<div class="modal-body">
								<p>The Country will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/address/delete/${address.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>


</body>
</html>