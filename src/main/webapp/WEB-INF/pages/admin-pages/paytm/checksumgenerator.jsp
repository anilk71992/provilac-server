<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="com.vishwakarma.provilac.model.SystemProperty"%>
<%@page import="com.vishwakarma.provilac.utils.SystemPropertyHelper"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.google.gson.*"%>
<%@ page import="java.util.*, java.net.URLEncoder, com.paytm.merchant.CheckSumServiceHelper"%>

<%
	WebApplicationContext context = RequestContextUtils.getWebApplicationContext(request);
	SystemPropertyHelper systemPropertyHelper = context.getBean("systemPropertyHelper", SystemPropertyHelper.class);
	
	Enumeration<String> paramNames = request.getParameterNames();
	Map<String, String[]> mapData = request.getParameterMap();
	TreeMap<String,String> parameters = new TreeMap<String,String>();
	TreeMap<String,String> parametersOut = new TreeMap<String,String>();
	String paytmChecksum =  "";
	parameters.put("MID","");	
	parameters.put("ORDER_ID","");
	parameters.put("INDUSTRY_TYPE_ID","");
	parameters.put("CHANNEL_ID","");
	parameters.put("TXN_AMOUNT","");
	parameters.put("CUST_ID","");
	parameters.put("WEBSITE","");
	while(paramNames.hasMoreElements()) {
		String paramName = (String)paramNames.nextElement();
		String paramValue = (String)mapData.get(paramName)[0];
		//// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
		if(paramValue.toLowerCase().contains("refund")){
			continue;
		}
		parameters.put(paramName,paramValue);	
	}
	
	SystemProperty systemProperty = systemPropertyHelper.getSystemProperty(SystemProperty.PAYTM_MERCHANT_KEY);
	String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(systemProperty.getPropValue(), parameters);
	
	parametersOut.put("CHECKSUMHASH",checkSum);
	parametersOut.put("payt_STATUS","1");
	parametersOut.put("ORDER_ID",mapData.get("ORDER_ID")[0]);
	Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	out.println(gson.toJson(parametersOut));
%>