<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Activity Log</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Activity Log - Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">User</label>
				<p class="col-md-2">${activityLog.user.firstName} ${activityLog.user.lastName} - ${activityLog.user.mobileNumber}</p>
				<label class="col-md-2">Activity</label>
				<p class="col-md-2">${activityLog.activity}</p>
				<label class="col-md-2">Entity Type</label>
				<p class="col-md-2">${activityLog.entityType}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Entity Id</label>
				<p class="col-md-2">${activityLog.entityId}</p>
				<label class="col-md-2">Date</label>
				<p class="col-md-2">${activityLog.date}</p>
			</div>
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${activityLog.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${activityLog.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${activityLog.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${activityLog.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>