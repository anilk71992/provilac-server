<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Delivery Boys</title>
<style type="text/css">
#map1 {         
    height: 400px;         
    width: 1100px;         
    margin: 0.6em;       
}
.infoWindow {
      background: #fff;
      }
</style>
</head>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
<script type="text/javascript">
var latlng ;
var map;
var customers = new Array();

var markers = new Array();
var infoWindows = new Array();

$(document).ready(function() {
	$.ajax({
		url: '<c:url value="/restapi/user/deliveryBoys"/>',
		dataType: 'json',
		success: function(response) {
			if (null != response) {
				if(response.success) {
					var data = response.data;
					deliveryBoys = data.routes;
						  addMarkers(data.deliveryBoys);
				} else {
					alert(response.error.message);
				}
			} else {
				alert("Nearest Routes are not found.");
			} 
		}
	}); 
	
});

function addMarkers(customerArray) {
	
	map = new google.maps.Map(document.getElementById('map1'), {
		  zoom: 6,
		  center: {lat: 18.5203, lng: 73.8567}
		});
	google.maps.event.addDomListener(window, "load", map);

	for(var i=0;i<customerArray.length;i++){
    	var customer= customerArray[i];
    	
	if (0 != customer.lat && 0 != customer.lng) {
				var latlng = new google.maps.LatLng(customer.lat, customer.lng);
				var infowindow = new google.maps.InfoWindow({
					content : customer.firstName + ' ' + customer.lastName
				});

				var marker = new google.maps.Marker({
					position : latlng,
					map : map,
					title : customer.firstName + ' ' + customer.lastName
				});

				infowindow.open(map, marker);
			}
		}
	}
</script>
<body>
	
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Delivry -Boys</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
		<div id="map1"></div></div>
	</div>

	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>