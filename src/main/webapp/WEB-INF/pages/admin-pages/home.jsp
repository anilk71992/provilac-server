<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="/taglibs.jsp"%>
<%@ include file="/messages.jsp"%>
<html>
<head>
<title>Home</title>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script> -->

<c:set var="isAdmin" value="false"></c:set>
<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
<c:set var="isAdmin" value="true"></c:set>
</sec:authorize>
<script type="text/javascript">

$(document).ready(function() {
	$('#startDate').datepicker({ autoclose: true});
	$('#endDate').datepicker({ autoclose: true});
	$('#fromDate').datepicker({ autoclose: true});
	$('#toDate').datepicker({ autoclose: true});
	$('#dailyStartDate').datepicker({ autoclose: true});
	$('#dailyEndDate').datepicker({ autoclose: true});
// 	$('#fromDate1').datepicker({ autoclose: true});
// 	$('#toDate1').datepicker({ autoclose: true});
	 var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1; //January is 0!

	    var yyyy = today.getFullYear();
	    if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 
	    var today = dd+'-'+mm+'-'+yyyy;
	     
// 		    document.getElementById("fromDate1").value = today;
// 		    document.getElementById("toDate1").value = today; 
		    document.getElementById("startDate").value = today;
		    document.getElementById("endDate").value = today;
		    document.getElementById("fromDate").value = today;
		    document.getElementById("toDate").value = today;
		    	
		    	$.ajax({
		    		url : '<c:url value="/home/getDelivererySchedulesForPastEightDays"/>',
		    		dataType : 'json',
		    		success : function(response) {
		    				if (response.success) {
		    					var data = response.data;
		    					var deliveredSchedules =  data.reports[0];
		    					var  notDeliveredSchedules =  data.reports[1];
		    					var dates = data.reports[2];
		    					getDelivererySchedulesForPastEightDays(deliveredSchedules,notDeliveredSchedules,dates);
		    				} else {
		    					alert(response.error.message);
		    					
		    				}
		    		}
		    	});
		    	
	     
	    $.ajax({
			url : '<c:url value="/user/assignedToUsers"/>',
			dataType : 'json',
			success : function(response) {
					if (response.success) {
						var data = response.data;
						for(var i=0; i<data.assignedToUsers.length;i++){
							var user1  = data.assignedToUsers[i];
							 $('.user').append($('<option>', {
								value: user1.id,
						        text: user1.firstName + " "+ user1.lastName +" - "+user1.mobileNumber
						    }));
						}
						
						var selectedUser = data.assignedToUsers[0];
						$("#assignedUser").val(selectedUser.id);
						getCustomers();
					} else {
						alert(response.error.message);
					}
			}
		});
	    $.ajax({
			url : '<c:url value="/user/collectionBoys"/>',
			dataType : 'json',
			success : function(response) {
					if (response.success) {
						var data = response.data;
						for(var i=0; i<data.collectionBoys.length;i++){
							var user1  = data.collectionBoys[i];
							 $('.cBoy').append($('<option>', {
								value: user1.id,
						        text: user1.firstName + " "+ user1.lastName +" - "+user1.mobileNumber
						    }));
							 $('.collectionBoyClass').append($('<option>', {
									value: user1.id,
							        text: user1.firstName + " "+ user1.lastName +" - "+user1.mobileNumber
							    }));
						}
						var selectedUser = data.collectionBoys[0];
						$("#collectionBoy").val(selectedUser.id);
						$("#collectionBoyId").val(selectedUser.id);
						getCollectionBoyReport();
						getCollectionReport();
					} else {
						alert(response.error.message);
					}
			}
		});
	   
		
	    
	    $.ajax({
    		url : '<c:url value="/home/getReasonWiseCustomers"/>',
    		dataType : 'json',
    		success : function(response) {
    				if (response.success) {
    					var data = response.data;
    					var reasons =  data.reports[0];
    					var  reasonCount =  data.reports[1];
    					getReasonWiseCustomers(reasons,reasonCount);
    				} else {
    					alert(response.error.message);
    					
    				}
    		}
    	});
	    $.ajax({
    		url : '<c:url value="/home/getCashFlowReport"/>',
    		dataType : 'json',
    		success : function(response) {
    				if (response.success) {
    					var data = response.data;
    					var reportParameter =  data.report[0];
    					var  reportValue =  data.report[1];
    					getCashFlowReport(reportParameter,reportValue);
    				} else {
    					alert(response.error.message);
    					
    				}
    		}
    	});
	    
// 	    $.ajax({
// 			url : '<c:url value="/home/getCustomerAndSubscriptionReport"/>',
// 			dataType : 'json',
// 			success : function(response) {
// 					if (response.success) {
// 						var data = response.data;
// 						var reportName =  data.report[0];
// 						var reportValue =  data.report[1];
// 						generateReport(reportName,reportValue);
// 					} else {
// 						alert(response.error.message);
						
// 					}
// 			}
// 		});
	 GetProductReportInDateRange();
	 getProductsOnLoad();
	   $.ajax({
   		url : '<c:url value="/home/getSalesPerformanceOfLoggedInUser"/>',
   		dataType : 'json',
   		success : function(response) {
   				if (response.success) {
   					var data = response.data;
   					var loggedInUserPerformance =  data.report[0];
   					var  loggedInUserPerformanceForNewCustomers =  data.report[1];
   					getLoggedInUserPerformance(loggedInUserPerformance,loggedInUserPerformanceForNewCustomers)
   				} else {
   					alert(response.error.message);
   					
   				}
   		}
   	});
	 
});

function getProductsOnLoad(){
	  $.ajax({
			url : '<c:url value="/home/getTotalProducts"/>',
			dataType : 'json',
			success : function(response) {
					if (response.success) {
						var data = response.data;
						createAreaChartJson(data.allProducts);
					} else {
						alert(response.error.message);
						
					}
			}
		});
}

function getCustomers(){
	
	var user2= document.getElementById('assignedUser');
	var userId =  user2.options[user2.selectedIndex].value;
	$.ajax({
		url : '<c:url value="/home/getCustomersByAssingedToUser"/>',
		dataType : 'json',
		data : 'userId=' + userId,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					var customerStatus = new Array();
					var customerCount = new Array();
						 customerStatus = data.customers[0];
						customerCount = data.customers[1];
					populateGraphForAssignedToUsers(customerStatus,customerCount);
				} else {
					alert(response.error.message);
				}
		}
	});
}

function getCollectionBoyReport(){
	var user2= document.getElementById('collectionBoy');
	var userId =  user2.options[user2.selectedIndex].value;
	$.ajax({
		url : '<c:url value="/home/getCollectionBoyReport"/>',
		dataType : 'json',
		data : 'userId=' + userId,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					var routeName = new Array();
					var pendingDues = new Array();
						 routeName = data.collectionBoyReport[0];
						pendingDues = data.collectionBoyReport[1];
					populateGraphForCollectionBoyReport(routeName,pendingDues);
				} else {
					alert(response.error.message);
				}
		}
	});
}

function getCollectionReport(){
	var user2= document.getElementById('collectionBoyId');
	var userId =  user2.options[user2.selectedIndex].value;
	$.ajax({
		url : '<c:url value="/home/getCollectionReport"/>',
		dataType : 'json',
		data : 'userId=' + userId,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					var amount = new Array();
					var paymentType = new Array();
						 amount = data.collectionReport[0];
						paymentType = data.collectionReport[1];
						populateGraphForCollectionReport(amount,paymentType);
				} else {
					alert(response.error.message);
				}
		}
	});
}

function GetProductReportInDateRange(){
	
	var startDate = document.getElementById('startDate').value;
	var endDate = document.getElementById('endDate').value;
	
	$.ajax({
		url : '<c:url value="/home/getDeliveredProductsForRange"/>',
		dataType : 'json',
		data : 'startDate=' + startDate + '&endDate=' + endDate,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					var products = new Array();
					var quantity = new Array();
					var dates = new Array();
					
					var productstoarChart = data.products[0];
					for(var i =0;i<productstoarChart.length;i++){
						var productQuantity = productstoarChart[i];
						products[i] = productQuantity[0];
						quantity[i] = productQuantity[1];
					}
					var quantityLen = 0;
					for(var i =0;i<quantity.length;i++){
						var quantity1 = quantity[i];
						if(quantityLen<quantity1.length)
							quantityLen = quantity1.length;
					}
					for(var i =0;i<quantityLen;i++){
						dates[i] =" ";
					}
					
					dates[0] = startDate;
					dates[dates.length-1] = endDate;
					
					var productQuantityString="" ;
					for(var i=0;i<products.length;i++){
						productQuantityString= productQuantityString+"{\"name\":\""+ products[i]+"\", \"data\": ["+ quantity[i]+"]},"
					}
					
					if(productQuantityString.length!=0){
						var len = productQuantityString.length;
				    	productQuantityString = productQuantityString .substring(0,len-1);
				    	var object = JSON.parse("["+productQuantityString+"]");
				   
					 	populateGraph(dates,object);
					}
					 getProductWiseSchedulesForCurrentDay(data.products[1]);
				} else {
					alert(response.error.message);
					
				}
		}
	});
}

function GetProductsInDateRange(){
	
	var fromDate = document.getElementById('fromDate').value;
	var toDate = document.getElementById('toDate').value;
	
	$.ajax({
		url : '<c:url value="/home/getAllProductsForRange"/>',
		dataType : 'json',
		data : 'fromDate=' + fromDate + '&toDate=' + toDate,
		success : function(response) {
				if (response.success) {
					var data = response.data;
					createAreaChartJson(data.allProducts);
				} else {
					alert(response.error.message);
					
				}
		}
	});	
}

// function getReportInDateRange(){
	
// 	var fromDate = document.getElementById('fromDate1').value;
// 	var toDate = document.getElementById('toDate1').value;
	
// 	$.ajax({
// 		url : '<c:url value="/home/getReportForRange"/>',
// 		dataType : 'json',
// 		data : 'fromDate=' + fromDate + '&toDate=' + toDate,
// 		success : function(response) {
// 				if (response.success) {
// 					var data = response.data;
// 					var reportName =  data.reports[0];
// 					var reportValue =  data.reports[1];
// 					generateReport(reportName,reportValue);
// 				} else {
// 					alert(response.error.message);
					
// 				}
// 		}
// 	});
	
// }
function createAreaChartJson(productQuantityArray){
	
	var products = new Array();
	var quantity = new Array();
	var productObject = new Object();
	var productValue = productQuantityArray[0];
	for(var i =0;i<productValue.length;i++){
		var productQuantity = productValue[i];
		products[i] = productQuantity[0];
		quantity[i] = productQuantity[1];
	}
	var productQuantityString ;
	for(var i=0;i<productValue.length;i++){
		productQuantityString= productQuantityString+"{\"name\":\""+ products[i]+"\", \"data\": ["+ quantity[i]+"]},"
	}
	var len = productQuantityString.length;
    productQuantityString = productQuantityString .substring(9,len-1);
    var object = JSON.parse("["+productQuantityString+"]");
	populateAreaGraphForProducts(productQuantityArray[1],object);
	
}

function dailyDeliveriesbyDateRange(){
	var startDate = $("#dailyStartDate").val();
	var endDate = $("#dailyEndDate").val();
	if(startDate == "" || endDate == ""){
		showToast("warning","Please Select valid Date Range");
		return;
	}
	
	$.ajax({
		url : '<c:url value="/home/getDelivererySchedulesForPastEightDays"/>',
		data : 'fromDate=' + endDate + '&toDate=' + startDate,
		dataType : 'json',
		success : function(response) {
				if (response.success) {
					var data = response.data;
					var deliveredSchedules =  data.reports[0];
					var  notDeliveredSchedules =  data.reports[1];
					var dates = data.reports[2];
					getDelivererySchedulesForPastEightDays(deliveredSchedules,notDeliveredSchedules,dates);
				} else {
					alert(response.error.message);
				}
		}
	});
}
</script>
</head>
<c:set var="newCustomerCount" value="${newCustomerCount}"/>
<body>
	<%-- <%@ include file="/messages.jsp"%> --%>
	<sec:authorize access="isAuthenticated()">
		<c:url var="urlBase" value="/home" />
		<c:set var="urlPrefix"
			value="/home?searchTerm=${searchTerm}&pageNumber=" scope="page" />
		<div style="padding: 10px;
  					margin-bottom: 10px;
					font-size: 18px;
					font-weight: 100;
					line-height: 20px;
					vertical-align: middle;
					color: inherit;
					background-color: #eeeeee;
					-webkit-border-radius: 6px;
					-moz-border-radius: 6px;
					border-radius: 6px;">
			<h1 style="margin-bottom: 0;
  					   font-size: 30px;
  					   line-height: 1;
  					   letter-spacing: -1px;
  					   color: inherit;">Dashboard</h1>
			<br>
			<h2>
				Welcome, <span id="username"><%=((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getFullName()%></span>
			</h2>
			<br/>
		</div>
		
		<div class="wrapper wrapper-content animated fadeInRight">
				
				<!-- <div class="row">
			<div class="col-md-4">
				<div class="input-group width-xlarge">
					<span class="input-group-addon" style="color:red">New Customers</span>
					<input  id="newCustomers" class="form-control"  />
			</div>
			</div>
		</div> -->
		<br>
		<div class="row">
					<div class="col-lg-2">
                		<div class="ibox float-e-margins">
                   		 <div class="ibox-title">
                       	 	<span class="label label-info pull-right">Today</span>
                       		 <h5>Customers</h5>
                   		 </div>
                    	<div class="ibox-content">
                      	  <h1 class="no-margins">${newCustomerCount}</h1>
                      	  <small>new Customers</small>
                   		 </div>
               		 	</div>
            		</div>
					
					<div class="col-lg-4">
                		<div class="ibox float-e-margins">
                   		 <div class="ibox-title">
                       		 <h5>Subscription Requests</h5>
                   		 </div>
                   		 <div class="row">
                    	<div class="content col-lg-4">
                      	  <h1 class="no-margins">${newSubscriptionRequestCount}</h1>
                      	  <small>New Subscription Request</small>
                   		 </div>
                   		 <div class="content col-lg-5">
                      	  <h1 class="no-margins">${changeSubscriptionRequestCount}</h1>
                      	  <small>Change Subscription Request</small>
                   		 </div>
                   		 </div>
               		 	</div>
            		</div>
				
				</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Daily Deliveries in Liter </h5>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Start Date</span>
								<input  id="dailyStartDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">End Date</span>
								<input  id="dailyEndDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-3">
							<a class="btn btn-info" href="javascript:dailyDeliveriesbyDateRange();">Get Report </a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="award-container" id="container1" >
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" >
			<br>
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Product wise Daily Deliveries in Liter</h5>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Start Date</span>
								<input  id="startDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">End Date</span>
								<input  id="endDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-3">
							<a class="btn btn-info" href="javascript:GetProductReportInDateRange();">Get Report </a>
						</div>
					</div>
					<div class="ibox-content">
						<div id="wrapper">
							<div class="ibox-container" id="container3"></div>
						</div>
					</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Total Reason wise Stopped Users </h5>
					</div>
					<div class="ibox-content">
						<div class="award-container" id="container4" >
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Today's Status wise Customers </h5>
					</div>
					<div class="ibox-content">
						<div class="award-container" id="container5" >
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Today's Cash Flow Graph in INR </h5>
					</div>
					<div class="ibox-content">
						<div class="award-container" id="container6" >
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		<div class="row"> 
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>AssignedTo wise Customers by status </h5>
				</div>
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
					<div class="input-group width-large">
						<select name="assignedUser" id="assignedUser" onchange="getCustomers();" class="form-control user">
						</select>
					</div>
					</div>
				</div>
				<br>
				<div class="ibox-content">
					<div class="award-container" id="container7" >
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5><%=((UserInfo)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getFullName()%> Sales Performance </h5>
					</div>
					<div class="ibox-content">
						<div class="award-container" id="container8" >
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row" >
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Total Products </h5>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">From Date</span>
								<input  id="fromDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group width-xlarge">
								<span class="input-group-addon">To Date</span>
								<input  id="toDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
							</div>
						</div>
						<div class="col-md-3">
							<a class="btn btn-info" href="javascript:GetProductsInDateRange();">Get Report </a>
						</div>
					</div>
					<div class="ibox-content">
						<div id="wrapper">
							<div class="ibox-container" id="container9"></div>
						</div>
					</div>
			</div>
		</div>
		<br>
<!-- 		<div class="row" > -->
<!-- 			<div class="ibox float-e-margins"> -->
<!-- 					<div class="ibox-title"> -->
<!-- 						<h5>Report of New Subscription,NewCustomers,Stoppedand ohHold Customers</h5> -->
<!-- 					</div> -->
<!-- 					<div class="ibox-content"> -->
<!-- 						<div id="wrapper"> -->
<!-- 							<div class="ibox-container" id="container10"></div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
		<div class="row"> 
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Route wise Pending Dues </h5>
				</div>
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<div class="input-group width-large">
							<select name="collectionBoy" id="collectionBoy" onchange="getCollectionBoyReport();" class="form-control cBoy">
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="ibox-content">
					<div class="award-container" id="container11" >
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row"> 
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Collection  Report </h5>
				</div>
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
					<div class="input-group width-large">
						<select name="collectionBoy" id="collectionBoyId" onchange="getCollectionReport();" class="form-control collectionBoyClass">
						</select>
					</div>
					</div>
				</div>
				<br>
				<div class="ibox-content">
					<div class="award-container" id="container12">
					</div>
				</div>
			</div>
		</div>
		<br>
	</div>
</sec:authorize>

<c:set var="customerCount" value="${customerCount}"/>

<script src="<c:url value="/assets/inspinia/js/reports/highcharts.js"/>"></script>
<script src="<c:url value="/assets/inspinia/js/reports/highcharts-bar-data.js"/>"></script>
	<script type="text/javascript">
	
	
	var status1 = new Array();
	<c:forEach items="${status}" var="status1">
		status1.push('${status1}');
	</c:forEach>
	
	var newCustomerCount = ${newCustomerCount};
	$("#newCustomers").val(newCustomerCount);
	
	function getDelivererySchedulesForPastEightDays(deliveredSchedules,notDeliveredSchedules,dates){
		$('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: dates,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
          enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Delivered Schedules',
                data: deliveredSchedules

            }, {
                name: 'Not Delivered Schedules',
                data: notDeliveredSchedules

            }]
        });
	}
	
	function getProductWiseSchedulesForCurrentDay(products){
		var chart=$('#container2').highcharts({
            chart: {
                type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                 marginTop: 10,
                marginBottom: 100,
                marginLeft: 50,
                marginRight: 110
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 50
            },
            credits: {
          enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,                    
                        style: {
                            fontWeight: 'bold',
                            color: 'black',
                        }
                    },
                    size: '100%',
                    startAngle: -180,
                    endAngle: 180,
                    center: ['50%', '75%']
                }
            },
            series: [{
                type: 'pie',
                size: '100%',
                name: 'Reports',
                marginTop:0,
                marginLeft:-100,
                innerSize: '50%',
                data: products
            }]
        });
	}
	
	function getReasonWiseCustomers(reasons,reasonCount){
		 $('#container4').highcharts({
             chart: {
                 type: 'column'
             },
             title: {
                 text: ''
             },
             subtitle: {
                 text: ''
             },
             xAxis: {
                 categories: reasons,
                 crosshair: true
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: ''
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             credits: {
           enabled: false
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.4,
                     borderWidth: 0
                 }
             },
             series: [{
                 data: reasonCount

             }]
         });
	}
	
	$(function () {	
		 $('#container5').highcharts({
             chart: {
                 type: 'column'
             },
             title: {
                 text: ''
             },
             subtitle: {
                 text: ''
             },
             xAxis: {
                 categories: status1,
                 crosshair: true
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: ''
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             credits: {
           enabled: false
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.4,
                     borderWidth: 0
                 }
             },
             series: [{
                 data: ${customerCount}

             }]
         });
	});
	function getCashFlowReport(cashFlowParameter,cashFlowValue){
		
		$('#container6').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: cashFlowValue,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
          enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.4,
                    borderWidth: 0
                }
            },
            series: [{
                data: cashFlowParameter
            }]
        });
	}
	
	function getLoggedInUserPerformance(loggedInUserPerformance,loggedInUserPerformanceForNewCustomers){
		
   	 $('#container8').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: loggedInUserPerformance,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
          enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.4,
                    borderWidth: 0
                }
            },
            series: [{
                data: loggedInUserPerformanceForNewCustomers
            }]
        });
	}
	
	function populateGraphForAssignedToUsers(customerStatus0,customerCount0){
		$('#container7').highcharts({
			colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories:customerStatus0,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        credits: {
	      enabled: false
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.4,
	                borderWidth: 0
	            }
	        },
	        series: [{
	        	name:"customers",
	            data: customerCount0,
	        }]
	    });
	}
	
	function populateGraphForCollectionBoyReport(routeName,pendingDues){
		$('#container11').highcharts({
			colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories:routeName,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        credits: {
	      enabled: false
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.4,
	                borderWidth: 0
	            }
	        },
	        series: [{
	        	name:"Routes",
	            data: pendingDues,
	        }]
	    });
	}
	function populateGraphForCollectionReport(amount,paymentType){
		$('#container12').highcharts({
			colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories:paymentType,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        credits: {
	      enabled: false
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.4,
	                borderWidth: 0
	            }
	        },
	        series: [{
	        	name:"Report",
	            data: amount,
	        }]
	    });
	}
	
	
	function populateGraph(dates,quantity){
		/* $('#container3').highcharts({
			colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
			chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories:products,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
          enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.4,
                    borderWidth: 0
                }
            },
            series: quantity,
        }); */
		
		$('#container3').highcharts({
	        title: {
	            text: ' Delivered Products',
	        },
	      
	        xAxis: {
	            categories: dates
	        },
	        yAxis: {
	           
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: quantity,
	    });
	}
function populateAreaGraphForProducts(products,object){
	
	$('#container9').highcharts({
		colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
		chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories:products,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        credits: {
      enabled: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.4,
                borderWidth: 0
            }
        },
        series: object,
        	
    });

	
}	

// function generateReport(reportName,reportValue){
// 	$('#container10').highcharts({
// 		colors: ['#2f7ed8','#910000','#8bbc21','#1aadce'],
// 		chart: {
//             type: 'column'
//         },
//         title: {
//             text: ''
//         },
//         subtitle: {
//             text: ''
//         },
//         xAxis: {
//             categories:reportName,
//             crosshair: true
//         },
//         yAxis: {
//             min: 0,
//             title: {
//                 text: ''
//             }
//         },
//         tooltip: {
//             headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//             pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//                 '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
//             footerFormat: '</table>',
//             shared: true,
//             useHTML: true
//         },
//         credits: {
//       enabled: false
//         },
//         plotOptions: {
//             column: {
//                 pointPadding: 0.4,
//                 borderWidth: 0
//             }
//         },
//         series: [{
//         	name:"Report",
//             data: reportValue,
//         }]
//     });
	
// }	


    </script>
</body>
</html>