<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add City</title>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#country').select2();
	
});

function submitForm() {
	if(validate()){
		$('#addCity').submit();

	}
		}

function validate() {
	
	var countryValue = $('#country').val();
	if (! countryValue|| countryValue == -1 ) {
		showToast("warning", "Please select country");
		return false;
	}
	
	var city =$('#name').val(), chardig= /^[a-zA-Z ]*$/;
	var cityVal=city.trim();
	if(!chardig.test(city)|| (!cityVal)){
		showToast('warning','Please Enter Valid City');
		return false;
		}
	
	var state =$('#state').val().trim(), chardig= /^[a-zA-Z ]*$/;
	if(!chardig.test(state)){
		showToast('warning','Please Enter Valid State');
		return false;
		}
	
	$('#name').val(cityVal);
	return true;
}
</script>
</head>
<c:url value="/admin/city/add" var="sportAdd" />
<c:url value="/admin/city/list" var="sportList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/city/add" var="addsport" />

	<form:form id="addCity" action="${addCityPic}" method="post" modelAttribute="city" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">City</h3>
			<form:errors path="country" cssClass="text-danger"/>
			<div class="input-group width-xlarge">
			<form:select path="country" class="form-control"  >
				<option value="-1"> -- Select Country-- </option>
				<c:forEach items="${countries}" var="country">
					<form:option value="${country.id}"> ${country.name} </form:option>
				</c:forEach>
			</form:select>
			</div>
			<br>
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Name*</span> 
				<form:input path="name" cssClass="form-control"/>
			</div>
			<br>
			<form:errors path="state" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">State</span> 
				<form:input path="state" cssClass="form-control"/>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>