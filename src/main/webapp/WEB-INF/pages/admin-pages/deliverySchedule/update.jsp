<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Delivery Schedule</title>
<%@ include file="../underscore-templates.tmpl"%>
<script type="text/javascript">
var products;
var lineItemNumber1=0;
var existingLineItems = new Array();

<c:forEach items="${deliverySchedule.deliveryLineItems}" var="lineItem">
	var lineItemObj = new Object();
	lineItemObj['product'] = '${lineItem.product.name}';
	lineItemObj['quantity'] = ${lineItem.quantity};
	existingLineItems.push(lineItemObj);
</c:forEach>

$(document).ready(function() {
 	$("#addLineItemButton").hide();
 	$("#customer2").hide();
 	onRouteSelect1();
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getAllProducts"/>',
		dataType:'json',
		
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products
				addLineItem1();
			}else{
				alert("Error while retrieving products");
			}
		}
		

	});
	
	$('#date').datepicker();
	$('#deliveryTime').datepicker();
	
	var dateToMatch=$('#date').val();
	
});
function addLineItem1() {
	var lineItems ;
	
	var template = _.template($("#lineItemTemplateForTrialDeliverySchedule").html());
	var htmlText = template(products,lineItemNumber1,existingLineItems);
	$('#lineItem1Div').append(htmlText);
	lineItemNumber1=existingLineItems.length;
	
	$("#updateLineItemButton").hide();
	$("#addLineItemButton").show();
}
function addLineItem2() {
	
	var dateToMatch=$('#date').val();
	var toDay=new Date();
		var dateToMatchValidate=moment(dateToMatch, ["DD-MM-YYYY"]).format("YYYY-MM-DD");
		/* var toDayValidate= moment(toDay, ["DD-MM-YYYY"]).format("YYYY-MM-DD");
		 */
		if (moment(dateToMatchValidate).isBefore(toDay)){
			showToast('warning','This is Previous Date Subcription');
			return false;
			}
		
	var lineItems ;
	
	var template = _.template($("#lineItemTemplateForDeliverySchedule").html());
	var htmlText = template(products,lineItemNumber1);
	$('#lineItem1Div').append(htmlText);
	lineItemNumber1++;
}
var deleteCount=0;
function removeLineItem(deliveryLineItem) {
	deliveryLineItem.remove();
	deleteCount++;
}
function onRouteSelect1() {
	
	var routeId = document.getElementById('route').value;
	var id = document.getElementById('deliveryScheduleId').value;
	$.ajax({
		url : '<c:url value="/route/getRecords"/>',
		dataType : 'json',
		data : 'routeId=' + routeId + '&id='+ id,
		success : function(response) {
			if (response.data.records.length != 0) {
				if (response.success) {
					var data = response.data;
					records = data.records;
					$('#customer1').empty();
					$("#customer2").show();
					records.sort(function(a, b){
						 return a.priority-b.priority
						});
					var selectedUser;
					for (var i = 0; i < records.length; i++) {
						var user1 = records[i].customerDTO;
						 $('.user').append($('<option>', {
								value: user1.code,
						        text: user1.firstName + " "+ user1.lastName
						    }));
					}
					$("#customer1").val(data.customer.code);

				} else {
					alert(response.error.message);
				}
			} else {
				//alert("Nearest Routes are not found.");
			}
		}
	});
	
	
}

function onRouteSelect() {
	
	var routeId = document.getElementById('route').value;
	var id = document.getElementById('deliveryScheduleId').value;
	$.ajax({
		url : '<c:url value="/route/records"/>',
		dataType : 'json',
		data : 'routeId=' + routeId ,
		success : function(response) {
			if (response.data.records.length != 0) {
				if (response.success) {
					var data = response.data;
					records = data.records;
					$('#customer1').empty();
					$("#customer2").show();
					records.sort(function(a, b){
						 return a.priority-b.priority
						});
					for (var i = 0; i < records.length; i++) {
						var user1 = records[i].customerDTO;
						 $('.user').append($('<option>', {
								value: user1.code,
						        text: user1.firstName + " "+ user1.lastName
						    }));
					}

				} else {
					alert(response.error.message);
				}
			} else {
				//alert("Nearest Routes are not found.");
			}
		}
	});
	
	
}
function submitForm() {
	if(validate()){
		$('#updateUser').submit();
	}
}

function validate() {
	
	/* if(!$('#customer').val() || $('#customer').val()=="-1"){
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	} */
	
	var selectedDate = $("#date").val();
	
	if(selectedDate == "" || !selectedDate){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Date");
		$("#date").focus();
		return false;
	}
	if(!$('#route').val()){
		showToast("warning","Select Route");
		$("#route").focus();
		return false;
	}
	
	/* if(!$('#customer1').val()){
		showToast("warning","Select After");
		$("#route").focus();
		return false;
	} */
	
	if(lineItemNumber1-deleteCount<1){
		showToast("warning","Add At least One Line Item");
		return false;
	}
	
	var products = new Array();
	for(var i=0;i<lineItemNumber1-deleteCount;i++){
		
		if(!$($(".productClass")[i]).val()||$($(".productClass")[i]).val()==""){
			showToast("warning","Please select product");
			return false;
		}
		$($(".quantityClass")[i]).val($($(".quantityClass")[i]).val().trim());
		if(!$($(".quantityClass")[i]).val()||$($(".quantityClass")[i]).val().trim()=="" || $($(".quantityClass")[i]).val()<=0){
			showToast("warning","Please Enter Valid Quantity");
			($(".quantityClass")[i]).focus();
			return false;
		}
		
		var typeAndProduct = ($($(".productClass")[i]).val());
		if($.inArray(typeAndProduct, products) > -1){
			($(".productClass")[i]).focus();
			showToast('warning','Duplicate Product');
			return false;
		}
		
		products.push($($(".productClass")[i]).val());
	}
	
	return true;	
}
</script>
</head>

<c:url value="/admin/deliverySchedule/update" var="deliveryScheduleUpdate" />
<c:url value="/admin/deliverySchedule/list" var="deliveryScheduleList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/deliverySchedule/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="deliverySchedule" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
		
			 <h3 class="page-header">DeliverySchedule</h3>
			 <div class="row">
			<div class="col-md-4">
			<form:errors path="customer" cssClass="alert-danger" />
			 <div class="input-group width-xlarge">
				<span class="input-group-addon">Customer</span>
				<input name="customer" id="customer" value="${deliverySchedule.customer.firstName} ${deliverySchedule.customer.lastName} - ${deliverySchedule.customer.mobileNumber}" disabled="disabled" Class="form-control" />
			</div>
			<br>
			<input type="hidden" name="customer" id="customer" value="${deliverySchedule.customer.id}" Class="form-control" />
			
			<form:errors path="type" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Type</span> 
				<form:select path="type" cssClass="form-control">
					<form:option value="Free_Trial">Free_Trial</form:option>
					<form:option value="Paid_Trial">Paid_Trial</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="date" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Date</span>
				<input name="date" id="date" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${deliverySchedule.date}" pattern="dd-MM-yyyy" />"/>
			</div>
			<input type="hidden" id="deliveryScheduleId" value="${deliverySchedule.id}">
			<br>
			<form:errors path="route" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Route</span>
				<form:select path="route" cssClass="form-control" id="route" onchange="onRouteSelect()">
					<c:forEach items="${routes}" var="route">
						<option value="${route.id}" <c:if test="${route.id == deliverySchedule.route.id}">selected="selected"</c:if>>${route.name} </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<div class="input-group width-xlarge" id="customer2">
				<span class="input-group-addon">After</span> 
				<select name="customer1" id="customer1" class="form-control user">
				
				</select>
			</div>
			<br>
		</div>
		<div class="col-md-8">
			<h4>Delivery Line Items</h4>
			<div id="lineItem1Div">
				<form:errors path="deliveryLineItems" cssClass="text-danger"/>
				<a href="javascript:addLineItem1()" id="updateLineItemButton" class="btn btn-info"> Line Item</a>
				<a href="javascript:addLineItem2()" id="addLineItemButton" class="btn btn-info"> Add Line Item</a>
			</div>
		</div>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>