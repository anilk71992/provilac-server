<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - DeliverySchedule</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>DeliverySchedule</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>DeliverySchedule</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/deliverySchedule/list" />
	<c:set var="urlPrefix" value="/admin/deliverySchedule/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/deliverySchedule/show" var="deliveryScheduleDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="deliverySchedules" class="footable" requestURI="" id="deliverySchedule" export="false">
			<display:column title="Code" sortable="true">
				<a href="${deliveryScheduleDetailLinkPrefix}/${deliverySchedule.cipher}">${deliverySchedule.code}</a>
			</display:column>
			
			<display:column title="Customer" sortable="true">${deliverySchedule.customer.firstName} ${deliverySchedule.customer.lastName} - ${deliverySchedule.customer.mobileNumber}</display:column>
			<display:column title="Type" sortable="true">${deliverySchedule.type}</display:column>
			<display:column title="Date" sortable="true">
				<fmt:formatDate value="${deliverySchedule.date}" pattern="dd-MM-yyyy" />
			</display:column>
			<sec:authorize access="!hasAnyAuthority('ROLE_CCE')">
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/deliverySchedule/update/${deliverySchedule.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/deliverySchedule/delete/${deliverySchedule.cipher}">
							<li><a href="#ConfirmModal_${deliverySchedule.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${deliverySchedule.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete DeliverySchedule?</h3>
							</div>
							<div class="modal-body">
								<p>The DeliverySchedule will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/deliverySchedule/delete/${deliverySchedule.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
			</sec:authorize>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete deliverySchedules?</h3>
		</div>
		<div class="modal-body">
			<p>The selected deliverySchedule will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>