<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - DeliverySchedule</title>
<style type="text/css">
$('.subscriptionResumetDate').datepicker{z-index:1151 ;display: block;}
</style> 
<c:set var="isAdminRight" value="false"></c:set>
<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
	<c:set var="isAdminRight" value="true"></c:set>
</sec:authorize> 
<script type="text/javascript">
var events = new Array();
var products = new Array();
var deliverySchedules= new Array();
var customerMap = new Object();
var defaultAddress = false;
var isPrepaidCustomer = false;
var subscriptionCipher = '';

function onDateCheckChange(event, element) {
	event.stopPropagation();
	if($('.myhtml:checkbox:checked').length == 0){
		$(".customButton").hide();
	}else{
		$(".customButton").show();
		var dateArray = new Array();
		$('.myhtml:checkbox:checked').each(function() {
			dateArray.push(this.value);
		});
		$("#selectedDate").val(dateArray);
	}
} 

$(document).ready(function(){
	$('#customer').select2();
	$('#calendar').fullCalendar({});
	$('.subscriptionResumetDate').datepicker({ autoclose: true});
	
	$.ajax({
		url: '<c:url value="/routeRecord/getusers"/>',
		dataType: 'json',
		success: function(response){
			if(null!=response && response.success){
			var	customers = response.data.customers;
				for(var i = 0; i < customers.length; i++) {
					var customer  = customers[i];
					customerMap[customer.id] = customer;
					 $('.customer').append($('<option>', {
						value: customer.id,
				        text: customer.firstName + " "+ customer.lastName +" - "+customer.mobileNumber +" - "+customer.address
				    }));
				}
				
			}else {
				showToast("warning","No Users Found!!");
			}
		}
	
	});
	
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getAllProducts"/>',
		dataType:'json',
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products;
				for(var i = 0; i < products.length; i++) {
					var product  = products[i];
					
				    $('.product').append($('<option>', {
				        value: product.code,
				        text: product.name
				    }));
				}
			}else{
				showToast("warning","No Products Found!!");
			}
		}
	}); 
	
	$('.subscriptionStartDate').datepicker({autoclose: true,startDate: new Date()}); 
	
	$('.putonholdDate').datepicker({autoclose: true,startDate: new Date()});
	$(function($) {
	    $('.myhtml').checkboxes('range', true);
	}); 
});

function onSelectCustomer(){
	$(".popup-loader").show(); 

	var customerId = document.getElementById('customer').value;
	if(customerId == "-1"){
		$('#calendar').empty();
		$('#calendar').fullCalendar({});
		$(".changeSubscriptionBtn").hide();
		$(".putOnHoldBtn").hide(); 
 		$(".resumeBtn").hide();
 		$(".popup-loader").hide();
	}else{
		$(".changeSubscriptionBtn").show();
		$('#subscriptionTypeDiv').empty();
		$('#refreshBtn').hide();
		$(".viewSubscriptionDetailsBtn").hide();
		
		$.ajax({
			url: '<c:url value="/restapi/user/getDetails"/>',
			dataType: 'json',
			data: 'userId='+customerId,
			
			success: function(response){
				if(null!=response && response.success){
					var	data = response.data;
					var customer = data.user;
					defaultAddress = data.defaultAddress;
					if(customer.status!="HOLD"){
				 		$(".putOnHoldBtn").show(); 
				 		$(".resumeBtn").hide();
				 	} else { 	
				 		$(".resumeBtn").show();
				 		$(".putOnHoldBtn").hide();
					}
					isPrepaidCustomer = false;
					if(customer.userSubscriptionDTO) {
						if(customer.userSubscriptionDTO.subscriptionType == 'Prepaid') {
							$('#subscriptionTypeDiv').html(customer.userSubscriptionDTO.subscriptionType + ' Balance: ' + customer.remainingPrepayBalance + '<br>Delivery Balance: ' + customer.lastPendingDues);
							isPrepaidCustomer = true;
						} else {
							$('#subscriptionTypeDiv').html(customer.userSubscriptionDTO.subscriptionType + ' Dues: ' + customer.lastPendingDues);
						}
						subscriptionCipher = customer.userSubscriptionDTO.cipher;
						$(".viewSubscriptionDetailsBtn").show();
					} else {
						$(".viewSubscriptionDetailsBtn").hide();
					}
					$('#refreshBtn').show();
				}else {
					showToast("error","No User Found!!");
				}
			}
		});
		
		$.ajax({
			url: '<c:url value="/deliverySchedule/getLineItems"/>',
			dataType: 'json',
			data: 'customerId='+customerId,
			
			success: function(response){
				if(null!=response && response.success){
				var	data = response.data;
				deliverySchedules= data.deliverySchedules;
				for(var i=0;i<data.deliverySchedules.length;i++){
					var deliverySchedule = data.deliverySchedules[i];
					var web="";
					var mobile="";
					
					var event=new Object();
					if(deliverySchedule.updatedFromWeb || deliverySchedule.updatedFromMobile){
						var title="";
						
						if(deliverySchedule.updatedFromMobile){
							title=title+ " MOBILE ";
						}
						if(deliverySchedule.updatedFromWeb){
							title=title+ " WEB ";
						}
						event['title']=title;
						event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
						event['color'] = "#ffffff ";
					}
					
					events.push(event);
					
					for(var j=0;j<deliverySchedule.deliveryLineItemDTOs.length;j++){
						var lineItem = deliverySchedule.deliveryLineItemDTOs[j];
						var event = new Object();

						event['title'] = lineItem.quantity + " " + lineItem.productName;
						event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
						event['id'] = lineItem.code;
						event['scheduleId'] = deliverySchedule.id;
						event['productId'] = lineItem.productCode;
						event['quantity'] = lineItem.quantity;
						event['color'] = "#"+lineItem.productColorCode;
						event['description']="description";
						events.push(event);
					}
					
				}

				initializeCalendar(data.isAuthorized);
				events = new Array();
				}else {
					showToast("error","No Users Found!!");
				}
				$(".popup-loader").hide(); 
			}
		});
	}
 	
	
}
	
function initializeCalendar(isAuthorized) {
	$('#calendar').empty();
	$('#calendar').fullCalendar({
		events : events,
		
		eventClick : function(event, jsEvent, view) {
			var selectedDate = $.fullCalendar.formatDate(event.start, 'dd-MM-yyyy');
			
			var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
	    	var checkDate = $.fullCalendar.formatDate(event.start,'yyyy-MM-dd');
	    	var nextDate = moment(today, "YYYY-MM-DD").add(60,'days').format('YYYY-MM-DD');
	    	$(this).offset().top;
	    	if(isAuthorized == true){
	    		$("#selectedDate").val(selectedDate);
				getDeliveryScheduleByCustomerAndDate(selectedDate);
	    	}else{
		    	if(checkDate > today && nextDate >= checkDate){
		    		
					$("#selectedDate").val(selectedDate);
					getDeliveryScheduleByCustomerAndDate(selectedDate);
		    	}
	    	}
	    	
		}, 
		dayClick: function(date, allDay, jsEvent, view) { 
			var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
			var hours = new Date().getHours();
	    	var checkDate = $.fullCalendar.formatDate(date,'yyyy-MM-dd');
	    	var nextDate = moment(today, "YYYY-MM-DD").add(60,'days').format('YYYY-MM-DD');
	    	
	    	if(isAuthorized == true){
	    		$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
				getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
	    	}else{
		    	if(checkDate > today  && nextDate >= checkDate){
					$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
					getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
		        }
		    	if(checkDate == today  && hours<=2 ){
					$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
					getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
		        }
	    	}
        }, 
	    dayRender: function(date, cell) {
	    	var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
	    	var hours = new Date().getHours();
		
	    	var checkDate = $.fullCalendar.formatDate(date,'yyyy-MM-dd');
	    	var nextDate = moment(today, "YYYY-MM-DD").add(60,'days').format('YYYY-MM-DD');
	    	
	    	if(isAuthorized){
		    	cell.prepend('<input type="checkbox" name="checkedDates[]" value="'+$.fullCalendar.formatDate(date, "dd-MM-yyyy")+'" class="myhtml"  onclick="onDateCheckChange(event, this)"/>');	    		
	    	}else{
		    	if(checkDate >= today  && nextDate >= checkDate){ 
			    	cell.prepend('<input type="checkbox" name="checkedDates[]" value="'+$.fullCalendar.formatDate(date, "dd-MM-yyyy")+'" class="myhtml"  onclick="onDateCheckChange(event, this)"/>');
		    	} else{
			    	cell.prepend('<input type="checkbox" name="checkedDates[]" value="'+$.fullCalendar.formatDate(date, "dd-MM-yyyy")+'" class="myhtml" disabled=true onclick="onDateCheckChange(event, this)"/>');
		    	}
	    	}
	    }, 
	    eventRender: function (event, jsEvent, view) {
	    	jsEvent.find('.fc-title').append('<div class="hr-line-solid-no-margin"></div><span style="font-size: 10px">'+event.description+'</span></div>');
	    } 
	});
}

//Change + Add Delivery Schedule - Calendar Click + Button Click
function addDeliveryScheduleForDay(offset){
	var customerId = $("#customer").val();
	var isValidate = false;
	if(customerId == "" || !customerId || customerId == "-1"){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	var selectedDate = $("#selectedDate").val();
	
	if(selectedDate == "" || !selectedDate){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Date");
		return false;
	}
	
	var dateArray = selectedDate.split(",");
	
	var quantityArray = new Array();
	var productArray = new Array();
	var tempNote = new Object()
	var permNote = new Object();
	if(offset == 'add'){
		 tempNote = $("#tNote").val();
		 permNote = $("#pNote").val();
		$("input[name=quantity\\[\\]]").each(function() {
			if(!(/^[0-9]+$/.test($(this).val()))){
				showToast("error","Enter Numbers Only!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if(!$(this).val() || $(this).val() == ""){
				showToast("warning","Enter Quantity!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			if($(this).val()==0){
				showToast("warning","Enter Quantity Greater Than 0!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			quantityArray.push($(this).val());
		});
		
		$("select[name=product\\[\\]]").each(function() {
			if(!$(this).val() || $(this).val() == "" || $(this).val() == "-- Select Product --"){
				showToast("warning","Select Product!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if($.inArray($(this).val(), productArray) > -1){
				$(this).focus();
				showToast('warning','Product Already added');
				isValidate = true;
				return false;
			}
			
			productArray.push($(this).val());
		});
	}else{
		 tempNote = $("#tNoteUpdate").val();
		 permNote = $("#pNoteUpdate").val();
		$("input[name=quantityUpdate\\[\\]]").each(function() {
			if(!(/^[0-9]+$/.test($(this).val()))){
				showToast("error","Enter Numbers Only!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if(!$(this).val() || $(this).val() == ""){
				showToast("warning","Enter Quantity!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			quantityArray.push($(this).val());
		});
		
		$("select[name=productUpdate\\[\\]]").each(function() {
			if(!$(this).val() || $(this).val() == "" || $(this).val() == "-- Select Product --"){
				showToast("warning","Select Product!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			if($.inArray($(this).val(), productArray) > -1){
				$(this).focus();
				showToast('warning','Product Already added');
				isValidate = true;
				return false;
			}
			
			productArray.push($(this).val());
		});
	}
	
	if(isValidate){
		return false;
	}
	$('#addEventModal').modal('hide');
	$('#UpdateEventModal').modal('hide');

	$.post({
		url : '<c:url value="/restapi/addNewDeliverySchedule"/>',
		data : 'customerId=' + customerId + '&productCodes=' + productArray
				+ '&quantity=' + quantityArray+'&dates='+dateArray+'&tempNote='+ tempNote + '&permNote=' +permNote,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				
				if(offset == "add"){
					$("#tNote").val("");
					 $("#pNote").val("");
					 showToast("success","Delivery Schedule Added Successfully");
					var html = $("#firstDiv").html();
					$("#mainContentAdd").html("");
				 	$("#mainContentAdd").append("<br><div class='row' id='firstDiv'>"+html+"</div>");
				 	
				}else{
					$("#tNoteUpdate").val("");
				 	$("#pNoteUpdate").val("");
				 	showToast("success","Delivery Schedule Updated Successfully");
				}
				onSelectCustomer();
			} else if(null != response && response.hasOwnProperty('error')) {
				showToast("warning", response.error.message);
			} else {
				showToast("warning","Something went Wrong. Please try again!!");
			}
		}
	});
} 	

//Populates modal on clicking date
function getDeliveryScheduleByCustomerAndDate(date){

	if(${isAdminRight} == false){
		return false;
	}
	
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	if(date == "" || !date){
		showToast("warning","Select Date");
		return false;
	}
	
	
	$.ajax({
		url : '<c:url value="/restapi/getDeliveryScheduleByCustomerAndDate"/>',
		data : 'customerId=' + customerId + '&date='+date,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				$("#mainContentUpdate").html("");
				var data = response.data.deliverySchedule.deliveryLineItemDTOs;
				var permNote = new Object();
				var tempNote = new Object();
				if(null != data){
					 permNote = response.data.deliverySchedule.permanantNote;
					 tempNote = response.data.deliverySchedule.tempararyNote;
					if(null!=permNote){
						$("#pNoteUpdate").val(permNote);
					}
					if(null!=tempNote){
						$("#tNoteUpdate").val(tempNote);
					}
					permNote = new Object();
					tempNote = new Object();
					$.each(data,function(i,item){
						var html = '<br><div class="row" id="'+data[i].productCode+'"><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
						html += '<select name="productUpdate[]" class="form-control product" id="product_'+i+'">'+$(".product").html();
						html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
						html += '<input name="quantityUpdate[]" type="text" class="form-control quantity" value="'+data[i].quantity+'">';
						html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+data[i].productCode+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
						html += '<a class="btn btn-primary" href="javascript:addProductRow();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
						$("#mainContentUpdate").append(html);
						$("#product_"+i).val(data[i].productCode);
					});
				}
				
					$('#UpdateEventModal').modal('show');
			} else {
					$('#addEventModal').modal('show');
			}
		}
	});
} 

//Selecting dates on calendar and clicking on hold
function deleteDeliveryScheduleForDays(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	var dateArray = new Array();
	$('.myhtml:checkbox:checked').each(function() {
		dateArray.push(this.value);
	});
	$("#confirmationModal").modal('hide');
	$.ajax({
		url : '<c:url value="/restapi/deletedDeliverySchedule"/>',
		data : 'customerId=' + customerId +'&dates='+dateArray,
		dataType : 'json',
		success : function(response) {
			showToast("success","Delivey Schedule Put On Hold Successfully!!");
			if (null != response && response.success) {
				onSelectCustomer();
			}
			
		}
	});
} 

//Put on hold - Date range + Indefinite
function deleteDeliveryScheduleForIndefiniteTime(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		$("#putOnHoldModal").modal('hide');
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	var startDate = $("#putonholdStartDate").val();
	var endDate = $("#putonholdEndDate").val();
	
	var isIndefiniteTime = $("#putonholdIndefiniteTime").is(':checked');
	
	if(isIndefiniteTime == false){
		if(endDate == ""){
			showToast("warning","Select End Date");
			$("#putonholdEndDate").focus();
			return false;
		}
	}else{
		$("#putonholdEndDate").val("");
	}
	$("#putOnHoldModal").modal('hide');
	
	$.ajax({
		url : '<c:url value="/restapi/deliverySchedule/putonhold"/>',
		data : "customerId=" + customerId +"&startDate=" +startDate+ "&endDate=" +endDate+ "&isIndefiniteTime="+isIndefiniteTime,
		dataType : 'json',
		success : function(response) {
			showToast("success","Delivey Schedule Put On Hold Successfully!!");
			if (null != response && response.success) {
				$('#calendar').empty();
				if(isIndefiniteTime == true){
					$('.putOnHoldBtn').hide();
					$('.resumeBtn').show();
				
				}else{
					$('.putOnHoldBtn').show();
					$('.resumeBtn').hide();
				}
// 				onSelectCustomer();
			}
			
		}
	});
	$("#putonholdEndDate").val("");
} 

function addUserSubscription(){
	
	var typeAndProductArray = new Array();
	var productArray = new Array();
	var types = new Array();
	var days = new Array();
	var isValidate = false;
	var lineItemLength = $(".lineItemClass").length;
	if(defaultAddress == false){
		showToast("warning","Please Add default address for selected customer.");
		$("#changeUserSubscription").modal('hide');
		$("#resumeModal").modal('hide');
		$('#lineItemsBody').html("");
			return false;
	}
	
	if(lineItemLength==0){
		showToast('warning','Please add lineItems.');
		return false;
	}
	
	for(var i=0;i<=lineItemLength-1;i++){
		
		if(!$($(".lineItemClass")[i]).val()||$($(".lineItemClass")[i]).val()==""){
			isValidate=true;
			showToast("warning","Please select type");
			return false;
		}
		if(!$($(".productLineItemClass")[i]).val()||$($(".productLineItemClass")[i]).val()==""){
			isValidate=true;
			showToast("warning","Please select product");
			return false;
			
		}
		var typeAndProduct = ($($(".lineItemClass")[i]).val()+""+$($(".productLineItemClass")[i]).val()+""+$($(".dayLineItemClass")[i]).val());
		if($.inArray(typeAndProduct, typeAndProductArray) > -1){
			($(".productLineItemClass")[i]).focus();
			showToast('warning','Duplicate Type with this Product');
			isValidate = true;
			return false;
		}
		
		typeAndProductArray.push($($(".lineItemClass")[i]).val()+""+$($(".productLineItemClass")[i]).val()+""+$($(".dayLineItemClass")[i]).val());
		productArray.push($($(".productLineItemClass")[i]).val());
		types.push($($(".lineItemClass")[i]).val());
		days.push($($(".dayLineItemClass")[i]).val());
	}
	
	var customerId = $("#customer").val();
	var permenantNote = $("#permenantNote").val();
	if(permenantNote == "" || !permenantNote){
		permenantNote = $("#permenantNoteId").val();
	}
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#changeUserSubscription").modal('hide');
		$("#customer").focus();
		$(".popup-loader").hide();
		return false;
	}
	
	
	var date = $("#subscriptionStartDate1").val();
	if(date == ""){
		 date = $("#subscriptionStartDate").val();
		 if(date == ""){
		showToast("warning","Please select Date");
		$("#subscriptionStartDate1").focus();
		$(".popup-loader").hide();
		return false;
		 }
	}
	
	var quantityArray = new Array();
	
	$("input[name=quantityLineItem\\[\\]]").each(function() {
		if(!$(this).val() || $(this).val() == ""){
			showToast("warning","Enter Quantity!!");
			$(this).focus();
			isValidate = true;
			return false;
		}
		quantityArray.push($(this).val());
	});

	var param = "startDate="+date+"&customerId="+customerId+"&types="+types+"&productCodes="+productArray+"&quantity="+quantityArray+"&permenantNote="+permenantNote;
	param += "&days="+days;
	if(isPrepaidCustomer) {
		var convertToPostpay = $('#convertToPostpay').val();
		param = param + "&convertToPostpay="+convertToPostpay;
	}
	
	if(isValidate){
		$("#changeUserSubscription").modal('hide');
		$(".popup-loader").hide();
		return false;
	}
	$("#changeUserSubscription").modal('hide');
	$("#resumeModal").modal('hide');
	
	$.ajax({
		url : '<c:url value="/restapi/userSubscription/addUserSubscription"/>',
		data : param,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				showToast("error","UserSubscription Added Successfully!! It will take few moments to generate Delivery Schedules. Please check after few time!!");
				onSelectCustomer();
				$('#calendar').empty();
				$("#changeUserSubscription").modal('hide');
				$("#resumeModal").modal('hide');
			//	$("#subscriptionType").val('');
				$("#lineItemsBody").html("");
				$("#resumeUserSubscription").html("");
				$('.putOnHoldBtn').show();
				$('.resumeBtn').hide();
			}else{
				showToast('error','Error Occured! Please try again later');
			}
			$(".popup-loader").hide();
		}
	});
}
function clearData(){
	$("#tNoteUpdate").val(" ");
	$("#pNoteUpdate").val(" ");
} 
function showPermenantNote(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#changeUserSubscription").modal('hide');
		$("#customer").focus();
		return false;
	}
	var customer1 = customerMap[customerId];
	var html ='<div class="input-group width-large"><span class="input-group-addon">Customer</span>';
	html += '<input name="customer0" type="text" class="form-control " value="'+ customer1.firstName+ ""+ customer1.lastName + " - "+ customer1.mobileNumber + '" >';
	html += '</div>';
	
	$('#customerForPNote').html(html);
	
	$("#addPermenantNote").modal('show');
} 
function showResumeModal(){
	var customerId = $("#customer").val();
	
	if(isPrepaidCustomer) {
		
		$.ajax({
			url : '<c:url value="/restapi/prepay/markasactive"/>',
			data : "customerId=" + customerId,
			dataType : 'json',
			success : function(response) {
				if (null != response && response.success) {
					showToast("success","Customer has been marked as active. System will start generating prepaid schedules from tomorrow.!!");
	 				onSelectCustomer();
				} else {
					showToast("warning", "Something went wrong, please try again later.");
				}
			}
		});
		return;
	} 
	
	$.ajax({
		url : '<c:url value="/restapi/userSubscription/getCurrentUserSubscription"/>',
		data : 'customerId=' + customerId,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				$("#resumeUserSubscription").html("");
				$("#permenantNoteId").val(response.data.subscription.permenantNote);
				$("#subscriuptionCode1").val(response.data.subscription.code); 
				var data = response.data.subscription;
				if(null != data){
					$.each(data.lineItemDTOs,function(i,item){
						if(item.type=="Weekly"){ 
							var id = Math.floor((Math.random() * 1000) + 12);
							var html = '<div class="row" id="'+id+'" style="margin-left: 20px; margin-right: 20px "><br><div class="col-md-3 "><div class="input-group" ><span class="input-group-addon">Type*</span>';
							html += '<select name="typeLineItem[]" class="form-control lineItemClass" id="type_'+id+'" onchange="javascript:showAddLineItemsBtn(\''+id+'\')"><option value=""> -- Select Type --</option><option value="Daily" numVal="0">  Daily </option><option value="Weekly" numVal="1">  WeekDays </option><option value="Custom" numVal="2">  Alternate </option> </select></div></div><div class="col-md-3"><div class="input-group"><span class="input-group-addon">Product*</span>';
							html += '<select name="productLineItem[]" id="product_'+id+'" class="form-control productLineItemClass">'+$(".product").html()+'</select></div></div><div class="col-md-5"><div class="row"><div class="col-md-6"><div class="input-group">';
							html += '<span class="input-group-addon">Quantity</span><input name="quantityLineItem[]" value="'+item.quantity+'" class="form-control" type="text">';
							html += '</div></div><div class="col-md-6 '+id+'"><div class="input-group">';
							html += '<span class="input-group-addon">Days</span><select name="day[]" id="day_'+id+'" class="form-control dayLineItemClass ">';
							html += '<option value="0">Sunday</option><option value="1">Monday</option><option value="2">Tuesday</option><option value="3">Wednesday</option>';
							html += '<option value="4">Thursday</option><option value="5">Friday</option><option value="6">Saturday</option></select>';
							html += '</div></div></div></div>';
							html += '<div class="col-md-1"><a class="btn btn-danger" href="javascript:deleteProductRow(\''+id+'\')">';
							html += '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></div></div>';
							$("#resumeUserSubscription").append(html);
							$("#product_"+id).val(item.productCode);
							$("#type_"+id).val(item.type);
							var day = (item.day);
							if(day!=null){
								var dayValue = day.substring(4, 5);
								
								
								$("#day_"+id).val(dayValue-1);
							} 
					}else{ 
							var id = Math.floor((Math.random() * 1000) + 12);
							var html = '<div class="row" id="'+id+'" style="margin-left: 20px; margin-right: 20px "><br><div class="col-md-3 "><div class="input-group" ><span class="input-group-addon">Type*</span>';
							html += '<select name="typeLineItem[]" class="form-control lineItemClass" id="type_'+id+'" onchange="javascript:showAddLineItemsBtn(\''+id+'\')"><option value=""> -- Select Type --</option><option value="Daily" numVal="0">  Daily </option><option value="Weekly" numVal="1">  WeekDays </option><option value="Custom" numVal="2">  Alternate </option> </select></div></div><div class="col-md-3"><div class="input-group"><span class="input-group-addon">Product*</span>';
							html += '<select name="productLineItem[]" id="product_'+id+'" class="form-control productLineItemClass">'+$(".product").html()+'</select></div></div><div class="col-md-5"><div class="row"><div class="col-md-6"><div class="input-group">';
							if(item.type=="Custom"){
								html += '<span class="input-group-addon">Quantity</span><input name="quantityLineItem[]" value="'+item.customPattern+'" class="form-control" type="text">';
							}else{
								html += '<span class="input-group-addon">Quantity</span><input name="quantityLineItem[]" value="'+item.quantity+'" class="form-control" type="text">';
							}
							html += '</div></div><div class="col-md-6 '+id+'" style="display:none;"><div class="input-group">';
							html += '<span class="input-group-addon">Days</span><select name="day[]" id="day" class="form-control dayLineItemClass">';
							html += '<option value="0">Sunday</option><option value="1">Monday</option><option value="2">Tuesday</option><option value="3">Wednesday</option>';
							html += '<option value="4">Thursday</option><option value="5">Friday</option><option value="6">Saturday</option></select>';
							html += '</div></div></div></div>';
							html += '<div class="col-md-1"><a class="btn btn-danger" href="javascript:deleteProductRow(\''+id+'\')">';
							html += '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></div></div>';
							
							$("#resumeUserSubscription").append(html);
							$("#product_"+id).val(item.productCode);
							$("#type_"+id).val(item.type); 
			 			}
						
					});
			}
				 

				$("#resumeModal").modal('show');				
			} else {
					$('#addEventModal').modal('show'); 
			}
		}
	});
	
}

function addPermenantNote(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#changeUserSubscription").modal('hide');
		$("#customer").focus();
		return false;
	}
	var permenantNote = $("#permNote1").val();
	if( permenantNote== "" || !permenantNote ){
		showToast("warning","Enter Permenant Note");
		$("#permenantNote1").focus();
		return false;
	}
	$("#addPermenantNote").modal('hide');
	$("#addPermenantNote").html("");
	$.ajax({
		url : '<c:url value="/restapi/userSubscription/addPermenantNoteToSubscription"/>',
		data : "permenantNote="+permenantNote +"&customerId="+customerId,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				showToast("success","Permenant Note Added Successfully!!");
			}else{
				showToast('error','Error Occured! Please try again later');
			}
		}
	});
	
} 
function showStopCustomerModal(){
	$("#confirmationModalForStop").modal('hide');
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#confirmationModalForStop").modal('hide');
		$("#customer").focus();
		return false;
	}
	var customer1 = customerMap[customerId];
	var html ='<div class="input-group width-large"><span class="input-group-addon">Customer</span>';
		html += '<input name="customertoStop" type="text" class="form-control " value="'+ customer1.firstName+ " "+ customer1.lastName + " - "+ customer1.mobileNumber + '" >';
		html += '</div>';
	
	$('#customerToStop').html(html);
	
	var html1 = '<div class="input-group width-xlarge"><div class="input-group">';
		html1 += '<span class="input-group-addon">Reason To Stop</span><select name="reasonToStop" id="reasonToStop" class="form-control reasonToStopClass">';
		html1 += '<option value="Shifted_City">Shifted City</option><option value="Purchase_From_Competitor">Purchase From Competitor</option><option value="Milk_Quality_Or_Taste">Milk Quality Or Taste</option><option value="Bottle_Quality">Bottle Quality</option>';
		html1 += '<option value="Expensive">Expensive</option><option value="Variants_Not_Available">Variants_Not_Available</option><option value="Others">Others</option></select>';
		html1 += '</div></div>';
		
	
	$('#reasonToStopDiv').html(html1);
	
	html1 = "";
	html1 += '<br><div class="row"><div class="col-md-3 col-md-offset-1 ">';
	html1 += '<div class="input-group width-large">';
	html1 += '<span class="input-group-addon">Select Date</span>';
	html1 += '<input name="dateFromStop" id="dateFromStop" type="text" class="form-control putonholdDate" data-date-format="dd-mm-yyyy">';
	html1 += '</div></div></div>';
	$("#stopCalendarDateDiv").html(html1);
	
	$('.putonholdDate').datepicker({autoclose: true,startDate: new Date()});
	$('#stopCustomerModal').modal('show');
		
} 
function StopCustomer() {
	$('#stopCustomerModal').modal('hide');
	var customerId = $("#customer").val();
	var reasonToStop = $('#reasonToStop').val();
	var dateFromStop = $("#dateFromStop").val();
	
	if(dateFromStop == ""){
		alert("Please select date");
		return false;
	}
	
	$.ajax({
		url : '<c:url value="/restapi/user/stopCustomer"/>',
		data : "customerId="+customerId +"&reasonToStop="+reasonToStop+"&fromDate="+dateFromStop,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				showToast("success","Customer Stopped Successfully!!");
				$('#calendar').empty();
			}else{
				showToast('error','Error Occured! Please try again later');
			}
		}
	});
	
}
 
function showChangeSubscriptionModal() {
	$('#changeUserSubscription').modal('show');
	if(isPrepaidCustomer) {
		$('#convertToPostPaidDiv').show();
	} else {
		$('#convertToPostPaidDiv').hide();
	}
}

function viewSubscriptionDetails() {
	var url = "<c:url value='/admin/userSubscription/show'/>" + "/" + subscriptionCipher;
	window.open(url, '_blank');
} 
</script>
<style type="text/css">
#calendar{
margin-left:   20px;
margin-right: 10px;
}

.datepicker { 
	z-index: 1151 !important; 
}
</style> 
</head>
<body>
<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>DeliverySchedule</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Calendar</strong>
	            </li>
	        </ol>
	    </div>
	</div>
	
	<div class="panel-body" style="margin-left: 10px;">
	
		<div class="row">
			<div class="col-md-3">
				<div class="input-group width-large">
					<!-- <span class="input-group-addon">Customer</span>  -->
					<select name="customer" id="customer" onchange="onSelectCustomer()" class="form-control customer">
						<option value="-1">-- Select Customer --</option>
					</select>
				</div>
				<div id="subscriptionTypeDiv"></div>
			</div>
			<sec:authorize access="!hasAnyAuthority('ROLE_CCE','ROLE_ACCOUNTANT')">
				<div class="col-md-2 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-medium">
						<button class="form-control btn btn-primary" onclick="showChangeSubscriptionModal()">Change Subscription</button>
					</div>
				</div>
				<div class="col-md-2 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-medium">
						<button class="form-control btn btn-primary" onclick="javascript:showPermenantNote();">Permanent Note</button>
					</div>
				</div>
				<div class="col-md-2 resumeBtn" style="display: none;">
					<div class="input-group width-medium">
						<button class="form-control btn btn-success" onclick="javascript:showResumeModal();">Resume</button>
					</div>
				</div>
				<div class="col-md-2 putOnHoldBtn" style="display: none;">
					<div class="input-group width-medium">
						<button class="form-control btn btn-danger" onclick="javascript:putOnHoldDeliverySchedule();">Put on Hold</button>
					</div>
				</div>
				
				<div class="col-md-2 customButton" style="display: none;">
					<div class="input-group width-small">
						<button class="form-control btn btn-success" data-toggle="modal" data-target="#addEventModal">Change DeliverySchedule</button>
					</div>
				</div>
				<div class="col-md-2 changeSubscriptionBtn" id="stopBtn" style="display: none;">
					<div class="input-group width-small">
						<button class="form-control btn btn-danger" data-toggle="modal" data-target="#confirmationModalForStop">Stop</button>
					</div>
				</div>
				<div class="col-md-2 refreshBtn" id="refreshBtn" style="display: none;">
					<div class="input-group width-small">
						<button class="form-control btn btn-primary" onclick="javascript:onSelectCustomer();">Refresh</button>
					</div>
				</div>
				<div class="col-md-2 viewSubscriptionDetailsBtn" id="subscriptionDetailsBtn" style="display: none;">
					<div class="input-group width-small">
						<button class="form-control btn btn-primary" onclick="javascript:viewSubscriptionDetails();">Subscription Details</button>
					</div>
				</div>
			</sec:authorize>
		</div>
		<br>
		<div class="row">	
			<div class="col-md-12">
				<div class="panel panel-primary" id="primary-info">
					<div class="panel-heading">
						<h3 class="panel-title">Calender</h3>
					</div>
					<div class="panel-body" >
						<div class="row">
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="UpdateEventModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="javascript:clearData()">�</button>
					<h3 id="myModalLabel">Update Event</h3>
				</div>
				<br>
					<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<div class="input-group width-large">
							<span class="input-group-addon">Temporary Note</span> <input
								class="form-control" name="tNote" id="tNoteUpdate"  type="text">
						</div>
					</div>
					
					<div class="col-md-3 col-md-offset-1 ">
						<div class="input-group width-large">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="pNote" id="pNoteUpdate" type="text">
						</div>
					</div>
				</div>
				<div id="mainContentUpdate">
					
				</div>
				<br>
				<div class="input-group width-medium">
					<input name="event" id="event" type="hidden" class="form-control"
						value=" " />
				</div>
					
				<div class="input-group width-medium">
					<input	name="event" id="event" type="hidden"class="form-control" value=" " />
				</div>
					
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addDeliveryScheduleForDay('update');">OK</a>
					<button class="btn btn-danger" data-dismiss="modal" onclick="javascript:clearData()" >Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- This field for users latest date selection -->	
	<input type="hidden" id="selectedDate" >
	
	<div class="modal" id="changeUserSubscription">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="javascript:hideAddSubscriptionModel();">x</button>
					<h4>Change User Subscription</h4>
				</div>
				<br>
				<div class="row">
					<fieldset>
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group">
								<span class="input-group-addon">Start Date</span>
								<input class="form-control subscriptionStartDate" value="${today}" data-date-format="dd-mm-yyyy" id="subscriptionStartDate"/>
							</div>
						</div>
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group">
								<span class="input-group-addon">Permanent Note</span>
								<input class="form-control" name="permenantNote"  id="permenantNote"/>
							</div>
						</div>
					</fieldset>
				</div>
				<br>
				<div class="row" id="convertToPostPaidDiv">
					<fieldset>
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group">
								<span class="input-group-addon">Convert to Postpaid</span>
								<select class="form-control" id="convertToPostpay">
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
				<br>
				<div id="lineItemsDiv">
					<div class="row">
						<fieldset>
								<div class="col-md-10 col-md-offset-1">
									<button class="btn btn-primary" onclick="javascript:getLineItemHTML();">Add Line Item</button>
									<span class="label label-warning customWarning" style="display: none;">please add quantity in this format 2-4 where 2 is a actual quantity for selected date</span>
								</div>
						</fieldset>
					</div>
					<div id="lineItemsBody">
				    </div>
				</div>
				<br>
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addUserSubscription();">OK</a>
					<button class="btn" onclick="javascript:hideAddSubscriptionModel();">Cancel</button>
				</div>
			</div>
		</div>
	</div>	
	<div class="modal" id="addEventModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('addEventModal')">�</button>
					<h4 >Add Delivery Schedule</h4>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 col-md-offset-1">
						<div class="input-group width-large">
							<span class="input-group-addon">Temporary Note</span> <input
								class="form-control" name="tNote" id="tNote" type="text">
						</div>
					</div>
					
					<div class="col-md-4 ">
						<div class="input-group width-large">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="pNote" id="pNote" type="text">
						</div>
					</div>
				</div>
				<br>
				<div id="mainContentAdd">
					<div class="row" id="firstDiv">
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group width-large">
								<span class="input-group-addon">Product</span>
								<select name="product[]" class="form-control product" >
									<option >-- Select Product --</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group width-large">
								<span class="input-group-addon">Quantity</span>
								<input name="quantity[]" type="text" class="form-control quantity" placeholder="Enter Quantity">
								</div>
							</div>
						<div class="col-md-3">
							<a class="btn btn-primary" href="javascript:addMoreProducts();">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<br>
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addDeliveryScheduleForDay('add');">OK</a>
					<button class="btn btn-danger" onclick="javascript:hideAddModel('addEventModal')">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="confirmationModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 >Confirmation</h3>
				</div>
				<div class="modal-body">
					<p>Are you sure? You want to Put on Hold Selected Delivery Schedule.</p>
				</div>
				<div class="modal-footer">
					<a class="btn btn-info" onclick="javascript:deleteDeliveryScheduleForDays();">OK</a>
					<button  class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="putOnHoldModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3>Put On Hold</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<fieldset>
							<div class="col-md-4 col-md-offset-1">
								<div class="input-group">
									<span class="input-group-addon">Start Date</span>
									<input class="form-control putonholdDate" value="${today}" data-date-format="dd-mm-yyyy" id="putonholdStartDate"/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">End Date</span>
									<input class="form-control putonholdDate" data-date-format="dd-mm-yyyy" id="putonholdEndDate"/>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="row">
						<fieldset>
							<div class="col-md-4 col-md-offset-1">
								<div class="checkbox">
			                        <label>
										<input id="putonholdIndefiniteTime" type="checkbox" />
										Put on Hold Indefinite Time
			                        </label>
			                    </div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="modal-footer">
					<a class="btn btn-info" onclick="javascript:deleteDeliveryScheduleForIndefiniteTime();">OK</a>
					<button  class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="resumeModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3>Resume UserSubscription</h3>
				</div>
				<div class="modal-body">
					<div class="row " style=" margin-left: 20px; margin-right: 20px ">
					<fieldset>
						<div class="col-md-4 ">
							<div class="input-group">
								<span class="input-group-addon">Start Date</span>
								<input class="form-control subscriptionResumetDate"  data-date-format="dd-mm-yyyy" id="subscriptionStartDate1"/>
							</div>
						</div>
						<div class="col-md-5 col-md-offset-1">
							<div class="input-group">
								<span class="input-group-addon">Permenant Note</span>
								<input class="form-control" name="permenantNote"  id="permenantNoteId"/>
							</div>
						</div>
						<div class="col-md-2 ">
							<div class="input-group">
								<input class="form-control" name="subscriuptionCode1" value="${today}" type="hidden" id="subscriuptionCode1"/>
							</div>
						</div>
					</fieldset>
				</div>
					<br>
					<div id="resumeUserSubscriptionDiv">
					<div class="row" style=" margin-left: 20px; margin-right: 20px">
						<fieldset>
								<div class="col-md-10 ">
									<button class="btn btn-primary" onclick="javascript:getSubscriptionLineItemHTML();">Add Line Item</button>
									<span class="label label-warning customWarning" style="display: none;">please add quantity in this format 2-4 where 2 is a actual quantity for tuesday</span>
								</div>
						</fieldset>
					</div>
					<div id="resumeUserSubscription">
				    </div>
				</div>
					
				</div>
				<div class="modal-footer">
					<a class="btn btn-info" onclick="javascript:addUserSubscription();">Continue</a>
					<button  class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="addPermenantNote">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('addPermenantNote')">�</button>
					<h4 >Permenant Note</h4>
				</div>
				<br>
				<div class="modal-body">
				<div class="row">
				<div class="col-md-5 col-md-offset-1 ">
						<div id="customerForPNote">
						</div>
					</div>
					<div class="col-md-5  ">
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="permNote1" id="permNote1" type="text">
						</div>
					</div>
				</div>
				<br>
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addPermenantNote();">OK</a>
					<button class="btn btn-danger" onclick="javascript:hideAddModel('addPermenantNote')">Cancel</button>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="confirmationModalForStop">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 >Confirmation</h3>
				</div>
				<div class="modal-body">
					<p>Are you sure?  You want to Stop selected Customer.</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info" onclick="javascript:showStopCustomerModal()">OK</button>
					<button  class="btn btn-danger" data-dismiss="modal" onclick="javascript:hideAddModel('confirmationModalForStop')">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="stopCustomerModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('stopCustomerModal')">�</button>
					<h4 >Stop Customer</h4>
				</div>
				<br>
				<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-md-offset-1 ">
							<div id="customerToStop">
							</div>
						</div>
						<div class="col-md-3 col-md-offset-1">
							<div id="reasonToStopDiv">
								
							</div>
						</div>
					</div>
						<div id="stopCalendarDateDiv">
						</div>
					<br>
					<div class="modal-footer">
						<a class="btn btn-info" href="javascript: StopCustomer();">Stop</a>
						<button class="btn btn-danger" onclick="javascript:hideAddModel('stopCustomerModal')">Cancel</button>
					</div>
				
			</div>
		</div>
	</div>
</div>
<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
<script type="text/javascript">
function addMoreProducts(){
 	var id = Math.floor((Math.random() * 1000) + 12);
	var html = '<div class="row" id="'+id+'"><br><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
	html += '<select name="product[]" class="form-control">'+$(".product").html();
	html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
	html += '<input name="quantity[]" type="text" class="form-control quantity" placeholder="Enter Quantity">';
	html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+id+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	html += '<a class="btn btn-primary" href="javascript:addMoreProducts();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
	$("#mainContentAdd").append(html);
}

function addProductRow(){
	var id = Math.floor((Math.random() * 1000) + 12);
	var html = '<div class="row" id="'+id+'"><br><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
	html += '<select name="productUpdate[]" class="form-control">'+$(".product").html();
	html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
	html += '<input name="quantityUpdate[]" type="text" class="form-control quantity" placeholder="Enter Quantity">';
	html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+id+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	html += '<a class="btn btn-primary" href="javascript:addProductRow();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
	$("#mainContentUpdate").append(html);
}

function deleteProductRow(offsetId){
	$("#"+offsetId).remove();
}

function hideAddModel(offset){
	$('#'+offset).modal('hide');
	var html = $("#firstDiv").html();
	$("#mainContentAdd").html("");
 	$("#mainContentAdd").append("<div class='row' id='firstDiv'>"+html+"</div>");
}

function hideAddSubscriptionModel(){
	$('#changeUserSubscription').modal('hide');
	$("#lineItemsBody").html("");
}

function showAddLineItemsBtn(id){
	$(".customWarning").hide();
	
	var selectedType = $("#type_"+id).val();
	if(selectedType == "Weekly"){
		$("."+id).show();
	}else{
		$("."+id).hide();
	}
	if(selectedType == "Custom"){
		$(".customWarning").show();
	}else{
		$(".customWarning").hide();
	}
}

function getLineItemHTML(){
	var id = Math.floor((Math.random() * 1000) + 12);
		var html = '<div class="row" id="'+id+'" style="margin-left: 20px; margin-right: 20px "><br><div class="col-md-3"><div class="input-group" ><span class="input-group-addon">Type*</span>';
		html += '<select name="typeLineItem[]" class="form-control lineItemClass" id="type_'+id+'" onchange="javascript:showAddLineItemsBtn(\''+id+'\')"><option value=""> -- Select Type --</option><option value="Daily" numVal="0">  Daily </option><option value="Weekly" numVal="1">  WeekDays </option><option value="Custom" numVal="2">  Alternate </option> </select></div></div><div class="col-md-3"><div class="input-group"><span class="input-group-addon">Product*</span>';
		html += '<select name="productLineItem[]" class="form-control productLineItemClass">'+$(".product").html()+'</select></div></div><div class="col-md-5"><div class="row"><div class="col-md-6"><div class="input-group">';
		html += '<span class="input-group-addon">Quantity</span><input name="quantityLineItem[]" class="form-control" type="text">';
		html += '</div></div><div class="col-md-6 '+id+'" style="display:none;"><div class="input-group">';
		html += '<span class="input-group-addon">Days</span><select name="day[]" id="day" class="form-control dayLineItemClass">';
		html += '<option value="0">Sunday</option><option value="1">Monday</option><option value="2">Tuesday</option><option value="3">Wednesday</option>';
		html += '<option value="4">Thursday</option><option value="5">Friday</option><option value="6">Saturday</option></select>';
		html += '</div></div></div></div>';
		html += '<div class="col-md-1"><a class="btn btn-danger" href="javascript:deleteProductRow(\''+id+'\')">';
		html += '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></div></div>';
		$("#lineItemsBody").append(html);
		$("#lineItemsBody").show();
}

function getSubscriptionLineItemHTML(){
	var id = Math.floor((Math.random() * 1000) + 12);
	var html = '<div class="row" id="'+id+'" style="margin-left: 20px; margin-right: 20px "><br><div class="col-md-3"><div class="input-group" ><span class="input-group-addon">Type*</span>';
	html += '<select name="typeLineItem[]" class="form-control lineItemClass" id="type_'+id+'" onchange="javascript:showAddLineItemsBtn(\''+id+'\')"><option value=""> -- Select Type --</option><option value="Daily" numVal="0">  Daily </option><option value="Weekly" numVal="1">  WeekDays </option><option value="Custom" numVal="2">  Alternate </option> </select></div></div><div class="col-md-3"><div class="input-group"><span class="input-group-addon">Product*</span>';
	html += '<select name="productLineItem[]" class="form-control productLineItemClass">'+$(".product").html()+'</select></div></div><div class="col-md-5"><div class="row"><div class="col-md-6"><div class="input-group">';
	html += '<span class="input-group-addon">Quantity</span><input name="quantityLineItem[]" class="form-control" type="text">';
	html += '</div></div><div class="col-md-6 '+id+'" style="display:none;"><div class="input-group">';
	html += '<span class="input-group-addon">Days</span><select name="day[]" id="day" class="form-control dayLineItemClass">';
	html += '<option value="0">Sunday</option><option value="1">Monday</option><option value="2">Tuesday</option><option value="3">Wednesday</option>';
	html += '<option value="4">Thursday</option><option value="5">Friday</option><option value="6">Saturday</option></select>';
	html += '</div></div></div></div>';
	html += '<div class="col-md-1"><a class="btn btn-danger" href="javascript:deleteProductRow(\''+id+'\')">';
	html += '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></div></div>';
	$("#resumeUserSubscription").append(html);
	$("#resumeUserSubscription").show();
	
}
function putOnHoldDeliverySchedule(){
	if($('.myhtml:checkbox:checked').length == 0){
		$('#putOnHoldModal').modal('show');
		$('#putonholdEndDate').val("");
		$("#putonholdIndefiniteTime").prop('checked', false);
	}else{
		$('#confirmationModal').modal('show');
	}
}

</script>
</body>
</html>