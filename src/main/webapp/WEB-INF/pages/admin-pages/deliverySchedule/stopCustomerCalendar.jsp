<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - DeliverySchedule</title>
<c:set var="isAdminRight" value="false"></c:set>
<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
	<c:set var="isAdminRight" value="true"></c:set>
</sec:authorize>
<script type="text/javascript">
var events = new Array();
var products = new Array();
var deliverySchedules= new Array();
var customerMap = new Object();

$(document).ready(function(){
	$('#customer').select2();
	$('#calendar').fullCalendar({});
	
	$.ajax({
		url: '<c:url value="/restapi/user/getStopCustomers"/>',
		dataType: 'json',
		success: function(response){
			if(null!=response && response.success){
			var	customers = response.data.customers;
				for(var i = 0; i < customers.length; i++) {
					var customer  = customers[i];
					customerMap[customer.id] = customer;
					 $('.customer').append($('<option>', {
						value: customer.id,
				        text: customer.firstName + " "+ customer.lastName +" - "+customer.mobileNumber +" - "+customer.address
				    }));
				}
				
			}else {
				showToast("warning","No Users Found!!");
			}
		}
	
	});
	
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getAllProducts"/>',
		dataType:'json',
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products;
				for(var i = 0; i < products.length; i++) {
					var product  = products[i];
					
				    $('.product').append($('<option>', {
				        value: product.code,
				        text: product.name
				    }));
				}
			}else{
				showToast("warning","No Products Found!!");
			}
		}
	});
	
});

function onSelectCustomer(){
	$(".popup-loader").show();

	var customerId = document.getElementById('customer').value;
	if(customerId == "-1"){
		$('#calendar').empty();
		$('#calendar').fullCalendar({});
		$(".changeSubscriptionBtn").hide();
 		$(".popup-loader").hide();
	}else{
		$(".changeSubscriptionBtn").show();
		
		$.ajax({
			url: '<c:url value="/deliverySchedule/getLineItems"/>',
			dataType: 'json',
			data: 'customerId='+customerId,
			
			success: function(response){
				if(null!=response && response.success){
				var	data = response.data;
				deliverySchedules= data.deliverySchedules;
				for(var i=0;i<data.deliverySchedules.length;i++){
					var deliverySchedule = data.deliverySchedules[i];
					for(var j=0;j<deliverySchedule.deliveryLineItemDTOs.length;j++){
						var lineItem = deliverySchedule.deliveryLineItemDTOs[j];
						var event = new Object();
						event['title'] = lineItem.quantity + " " + lineItem.productName;
						event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
						event['id'] = lineItem.code;
						event['scheduleId'] = deliverySchedule.id;
						event['productId'] = lineItem.productCode;
						event['quantity'] = lineItem.quantity;
						event['color'] = "#"+lineItem.productColorCode;
						events.push(event);
					}
				}

				initializeCalendar(data.isAuthorized);
				events = new Array();
				}else {
					showToast("error","No Users Found!!");
				}
				$(".popup-loader").hide();
			}
		});
	}
}
	
function initializeCalendar(isAuthorized) {
	$('#calendar').empty();
	$('#calendar').fullCalendar({
		events : events		
	});
}
</script>
<style type="text/css">
#calendar{
margin-left:   20px;
margin-right: 10px;
}

.datepicker { 
	z-index: 1151 !important; 
}
</style>
</head>
<body>
<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Calendar For Stop Customer</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Calendar</strong>
	            </li>
	        </ol>
	    </div>
	</div>
	
	<div class="panel-body" style="margin-left: 10px;">
	
		<div class="row">
			<div class="col-md-3">
				<div class="input-group width-large">
					<!-- <span class="input-group-addon">Customer</span>  -->
					<select name="customer" id="customer" onchange="onSelectCustomer()" class="form-control customer">
						<option value="-1">-- Select Customer --</option>
					</select>
				</div>
			</div>
		</div>
		<br>
		<div class="row">	
			<div class="col-md-12">
				<div class="panel panel-primary" id="primary-info">
					<div class="panel-heading">
						<h3 class="panel-title">Calender</h3>
					</div>
					<div class="panel-body" >
						<div class="row">
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- This field for users latest date selection -->	
	<input type="hidden" id="selectedDate" >
	
<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>