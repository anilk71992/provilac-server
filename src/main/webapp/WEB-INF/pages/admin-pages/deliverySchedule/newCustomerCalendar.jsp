<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - DeliverySchedule</title>
<c:set var="isAdminRight" value="false"></c:set>
<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
	<c:set var="isAdminRight" value="true"></c:set>
</sec:authorize>
<script type="text/javascript">
var events = new Array();
var products = new Array();
var deliverySchedules= new Array();
var customerMap = new Object();

$(document).ready(function(){
	$('#customer').select2();
	$('#calendar').fullCalendar({});
	
	$.ajax({
		url: '<c:url value="/restapi/user/getNewCustomers"/>',
		dataType: 'json',
		success: function(response){
			if(null!=response && response.success){
			var	customers = response.data.customers;
				for(var i = 0; i < customers.length; i++) {
					var customer  = customers[i];
					customerMap[customer.id] = customer;
					 $('.customer').append($('<option>', {
						value: customer.id,
				        text: customer.firstName + " "+ customer.lastName +" - "+customer.mobileNumber +" - "+customer.address
				    }));
				}
				
			}else {
				showToast("warning","No Users Found!!");
			}
		}
	
	});
	
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getAllProducts"/>',
		dataType:'json',
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products;
				for(var i = 0; i < products.length; i++) {
					var product  = products[i];
					
				    $('.product').append($('<option>', {
				        value: product.code,
				        text: product.name
				    }));
				}
			}else{
				showToast("warning","No Products Found!!");
			}
		}
	});
	
	$('.subscriptionStartDate').datepicker({autoclose: true,startDate: new Date()});
	$(function($) {
	    $('.myhtml').checkboxes('range', true);
	});
});


function onSelectCustomer(){
	$(".popup-loader").show();
	var customerId = document.getElementById('customer').value;
	if(customerId == "-1"){
		$('#calendar').empty();
		$('#calendar').fullCalendar({});
		$(".changeSubscriptionBtn").hide();
 		$(".popup-loader").hide();
	}else{
		$(".changeSubscriptionBtn").show();
		
		var baseUrl = '<c:url value="/admin/userSubscription/add"/>' + '?selectedCustomerId=' + customerId;
		var prepayUrl = baseUrl + '&createPrepay=true';
		
		$('#prepayButton').attr('href', prepayUrl);
		$('#postpaidButton').attr('href', baseUrl);
		
		$.ajax({
			url: '<c:url value="/deliverySchedule/getLineItems"/>',
			dataType: 'json',
			data: 'customerId='+customerId,
			
			success: function(response){
				if(null!=response && response.success){
				var	data = response.data;
				deliverySchedules= data.deliverySchedules;
				for(var i=0;i<data.deliverySchedules.length;i++){
					var deliverySchedule = data.deliverySchedules[i];
					for(var j=0;j<deliverySchedule.deliveryLineItemDTOs.length;j++){
						var lineItem = deliverySchedule.deliveryLineItemDTOs[j];
						var event = new Object();
						event['title'] = lineItem.quantity + " " + lineItem.productName;
						event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
						event['id'] = lineItem.code;
						event['scheduleId'] = deliverySchedule.id;
						event['productId'] = lineItem.productCode;
						event['quantity'] = lineItem.quantity;
						event['color'] = "#"+lineItem.productColorCode;
						events.push(event);
					}
					if(deliverySchedule.type === 'Free_Trial') {
						var event = new Object();
						event['title'] = 'Free Trial';
						event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
						event['color'] = "#FFFFFF";
						events.push(event);
					}
				}
				initializeCalendar(data.isAuthorized);
				events = new Array();
				}else {
					showToast("error","No Users Found!!");
				}
				$(".popup-loader").hide();
			}
		});
	}
}
	
function initializeCalendar(isAuthorized) {
	$('#calendar').empty();
	$('#calendar').fullCalendar({
		events : events,
		
		eventClick : function(event, jsEvent, view) {
			var selectedDate = $.fullCalendar.formatDate(event.start, 'dd-MM-yyyy');
			
			var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
	    	var checkDate = $.fullCalendar.formatDate(event.start,'yyyy-MM-dd');
	    	var nextDate = moment(today, "YYYY-MM-DD").add(60,'days').format('YYYY-MM-DD');
	    	$(this).offset().top;
	    	if(isAuthorized == true){
	    		$("#selectedDate").val(selectedDate);
				getDeliveryScheduleByCustomerAndDate(selectedDate);
	    	}else{
		    	if(checkDate > today && nextDate >= checkDate){
		    		
					$("#selectedDate").val(selectedDate);
					getDeliveryScheduleByCustomerAndDate(selectedDate);
		    	}
	    	}
	    	
		},
		dayClick: function(date, allDay, jsEvent, view) { 
			var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
			var hours = new Date().getHours();
	    	var checkDate = $.fullCalendar.formatDate(date,'yyyy-MM-dd');
	    	var nextDate = moment(today, "YYYY-MM-DD").add(60,'days').format('YYYY-MM-DD');
	    	
	    	if(isAuthorized == true){
	    		$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
				getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
	    	}else{
		    	if(checkDate > today  && nextDate >= checkDate){
					$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
					getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
		        }
		    	if(checkDate == today  && hours<=2 ){
					$("#selectedDate").val(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
					getDeliveryScheduleByCustomerAndDate(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear());
		        }
	    	}
        }
	});
}

function addDeliveryScheduleForDay(offset){
	var customerId = $("#customer").val();
	var isValidate = false;
	if(customerId == "" || !customerId || customerId == "-1"){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	var selectedDate = $("#selectedDate").val();
	
	if(selectedDate == "" || !selectedDate){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Date");
		return false;
	}
	
	var dateArray = selectedDate.split(",");
	
	var quantityArray = new Array();
	var productArray = new Array();
	var tempNote = new Object()
	var permNote = new Object();
	var freeTrial = false;
	if(offset == 'add'){
		//For new delivery schedule
		 tempNote = $("#tNote").val();
		 permNote = $("#pNote").val();
		$("input[name=quantity\\[\\]]").each(function() {
			if(!(/^[0-9]+$/.test($(this).val()))){
				showToast("error","Enter Numbers Only!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if(!$(this).val() || $(this).val() == ""){
				showToast("warning","Enter Quantity!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			if($(this).val()==0){
				showToast("warning","Enter Quantity Greater Than 0!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			quantityArray.push($(this).val());
		});
		
		$("select[name=product\\[\\]]").each(function() {
			if(!$(this).val() || $(this).val() == "" || $(this).val() == "-- Select Product --"){
				showToast("warning","Select Product!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if($.inArray($(this).val(), productArray) > -1){
				$(this).focus();
				showToast('warning','Product Already added');
				isValidate = true;
				return false;
			}
			
			productArray.push($(this).val());
		});
		
		freeTrial = $('#freeTrial').val();
	}else{
		//For update
		 tempNote = $("#tNoteUpdate").val();
		 permNote = $("#pNoteUpdate").val();
		$("input[name=quantityUpdate\\[\\]]").each(function() {
			if(!(/^[0-9]+$/.test($(this).val()))){
				showToast("error","Enter Numbers Only!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			
			if(!$(this).val() || $(this).val() == ""){
				showToast("warning","Enter Quantity!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			quantityArray.push($(this).val());
		});
		
		$("select[name=productUpdate\\[\\]]").each(function() {
			if(!$(this).val() || $(this).val() == "" || $(this).val() == "-- Select Product --"){
				showToast("warning","Select Product!!");
				$(this).focus();
				isValidate = true;
				return false;
			}
			if($.inArray($(this).val(), productArray) > -1){
				$(this).focus();
				showToast('warning','Product Already added');
				isValidate = true;
				return false;
			}
			
			productArray.push($(this).val());
		});
	}
	
	if(isValidate){
		return false;
	}
	$('#addEventModal').modal('hide');
	$('#UpdateEventModal').modal('hide');

	$.post({
		url : '<c:url value="/restapi/addNewDeliverySchedule"/>',
		data : 'customerId=' + customerId + '&productCodes=' + productArray
				+ '&quantity=' + quantityArray+'&dates='+dateArray+'&tempNote='+ tempNote + '&permNote=' +permNote + '&isFreeTrial=' + freeTrial,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				
				if(offset == "add"){
					$("#tNote").val("");
					 $("#pNote").val("");
					 showToast("success","Delivery Schedule Added Successfully");
					var html = $("#firstDiv").html();
					$("#mainContentAdd").html("");
				 	$("#mainContentAdd").append("<br><div class='row' id='firstDiv'>"+html+"</div>");
				 	
				}else{
					$("#tNoteUpdate").val("");
				 	$("#pNoteUpdate").val("");
				 	showToast("success","Delivery Schedule Updated Successfully");
				}
				onSelectCustomer();
			} else {
				showToast("warning","Something went Wrong. Please try again!!");
			}
		}
	});
}	
function getDeliveryScheduleByCustomerAndDate(date){

	if(${isAdminRight} == false){
		return false;
	}
	
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	if(date == "" || !date){
		showToast("warning","Select Date");
		return false;
	}
	
	
	$.ajax({
		url : '<c:url value="/restapi/getDeliveryScheduleByCustomerAndDate"/>',
		data : 'customerId=' + customerId + '&date='+date,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				$("#mainContentUpdate").html("");
				var data = response.data.deliverySchedule.deliveryLineItemDTOs;
				var permNote = new Object();
				var tempNote = new Object();
				if(null != data){
					 permNote = response.data.deliverySchedule.permanantNote;
					 tempNote = response.data.deliverySchedule.tempararyNote;
					if(null!=permNote){
						$("#pNoteUpdate").val(permNote);
					}
					if(null!=tempNote){
						$("#tNoteUpdate").val(tempNote);
					}
					permNote = new Object();
					tempNote = new Object();
					$.each(data,function(i,item){
						var html = '<br><div class="row" id="'+data[i].productCode+'"><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
						html += '<select name="productUpdate[]" class="form-control product" id="product_'+i+'">'+$(".product").html();
						html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
						html += '<input name="quantityUpdate[]" type="text" class="form-control quantity" value="'+data[i].quantity+'">';
						html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+data[i].productCode+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
						html += '<a class="btn btn-primary" href="javascript:addProductRow();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
						$("#mainContentUpdate").append(html);
						$("#product_"+i).val(data[i].productCode);
					});
				}
				
					$('#UpdateEventModal').modal('show');
			} else {
					$('#addEventModal').modal('show');
			}
		}
	});
}

function clearData(){
	$("#tNoteUpdate").val(" ");
	$("#pNoteUpdate").val(" ");
}
function showPermenantNote(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#changeUserSubscription").modal('hide');
		$("#customer").focus();
		return false;
	}
	var customer1 = customerMap[customerId];
	var html ='<div class="input-group width-large"><span class="input-group-addon">Customer</span>';
	html += '<input name="customer0" type="text" class="form-control " value="'+ customer1.firstName+ ""+ customer1.lastName + " - "+ customer1.mobileNumber + '" >';
	html += '</div>';
	
	$('#customerForPNote').html(html);
	
	$("#addPermenantNote").modal('show');
}

function addPermenantNote(){
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#changeUserSubscription").modal('hide');
		$("#customer").focus();
		return false;
	}
	var permenantNote = $("#permNote1").val();
	if( permenantNote== "" || !permenantNote ){
		showToast("warning","Enter Permenant Note");
		$("#permenantNote1").focus();
		return false;
	}
	$("#addPermenantNote").modal('hide');
	$("#addPermenantNote").html("");
	$.ajax({
		url : '<c:url value="/restapi/userSubscription/addPermenantNoteToSubscription"/>',
		data : "permenantNote="+permenantNote +"&customerId="+customerId,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				showToast("success","Permenant Note Added Successfully!!");
			}else{
				showToast('error','Error Occured! Please try again later');
			}
		}
	});
	
}
function showStopCustomerModal(){
	$("#confirmationModalForStop").modal('hide');
	var customerId = $("#customer").val();
	if(customerId == "" || !customerId || customerId == "-1"){
		showToast("warning","Select Customer");
		$("#confirmationModalForStop").modal('hide');
		$("#customer").focus();
		return false;
	}
	var customer1 = customerMap[customerId];
	var html ='<div class="input-group width-large"><span class="input-group-addon">Customer</span>';
		html += '<input name="customertoStop" type="text" class="form-control " value="'+ customer1.firstName+ " "+ customer1.lastName + " - "+ customer1.mobileNumber + '" >';
		html += '</div>';
	
	$('#customerToStop').html(html);
	
	var html1 = '<div class="input-group width-xlarge"><div class="input-group">';
		html1 += '<span class="input-group-addon">Reason To Stop</span><select name="reasonToStop" id="reasonToStop" class="form-control reasonToStopClass">';
		html1 += '<option value="Shifted_City">Shifted City</option><option value="Purchase_From_Competitor">Purchase From Competitor</option><option value="Milk_Quality_Or_Taste">Milk Quality Or Taste</option><option value="Bottle_Quality">Bottle Quality</option>';
		html1 += '<option value="Expensive">Expensive</option><option value="Variants_Not_Available">Variants_Not_Available</option><option value="Others">Others</option></select>';
		html1 += '</div></div>';
		
	
	$('#reasonToStopDiv').html(html1);
	
	html1 = "";
	html1 += '<br><div class="row"><div class="col-md-3 col-md-offset-1 ">';
	html1 += '<div class="input-group width-large">';
	html1 += '<span class="input-group-addon">Select Date</span>';
	html1 += '<input name="dateFromStop" id="dateFromStop" type="text" class="form-control subscriptionStartDate" data-date-format="dd-mm-yyyy">';
	html1 += '</div></div></div>';
	$("#stopCalendarDateDiv").html(html1);
	$('.subscriptionStartDate').datepicker({autoclose: true,startDate: new Date()});
	$('#stopCustomerModal').modal('show');
		
}
function StopCustomer() {
	$('#stopCustomerModal').modal('hide');
	var customerId = $("#customer").val();
	var reasonToStop = $('#reasonToStop').val();
	var dateFromStop = $("#dateFromStop").val();
	
	if(dateFromStop == ""){
		alert("Please select date");
		return false;
	}
	
	$.ajax({
		url : '<c:url value="/restapi/user/stopCustomer"/>',
		data : "customerId="+customerId +"&reasonToStop="+reasonToStop+"&fromDate="+dateFromStop,
		dataType : 'json',
		success : function(response) {
			if (null != response && response.success) {
				showToast("success","Customer Stopped Successfully!!");
				$('#calendar').empty();
			}else{
				showToast('error','Error Occured! Please try again later');
			}
		}
	});
	
}
</script>
<style type="text/css">
#calendar{
margin-left:   20px;
margin-right: 10px;
}

.datepicker { 
	z-index: 1151 !important; 
}
</style>
</head>
<body>
<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Calendar For New Customer</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Calendar</strong>
	            </li>
	        </ol>
	    </div>
	</div>
	
	<div class="panel-body" style="margin-left: 10px;">
	
		<div class="row">
			<div class="col-md-3">
				<div class="input-group width-large">
					<!-- <span class="input-group-addon">Customer</span>  -->
					<select name="customer" id="customer" onchange="onSelectCustomer()" class="form-control customer">
						<option value="-1">-- Select Customer --</option>
					</select>
				</div>
			</div>
			<sec:authorize access="!hasAnyAuthority('ROLE_CCE','ROLE_ACCOUNTANT')">
				<div class="col-md-2 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-medium">
						<button class="form-control btn btn-primary" onclick="javascript:showPermenantNote();">Permenant Note</button>
					</div>
				</div>
				<div class="col-md-1 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-small">
						<button class="form-control btn btn-danger" data-toggle="modal" data-target="#confirmationModalForStop">Stop</button>
					</div>
				</div>
				<div class="col-md-3 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-small">
						<a class="form-control btn btn-danger" id="prepayButton" target="_blank">Create Prepay Subscription</a>
					</div>
				</div>
				<div class="col-md-3 changeSubscriptionBtn" style="display: none;">
					<div class="input-group width-small">
						<a class="form-control btn btn-danger" id="postpaidButton" target="_blank">Create Postpaid Subscription</a>
					</div>
				</div>
			</sec:authorize>
		</div>
		<br>
		<div class="row">	
			<div class="col-md-12">
				<div class="panel panel-primary" id="primary-info">
					<div class="panel-heading">
						<h3 class="panel-title">Calender</h3>
					</div>
					<div class="panel-body" >
						<div class="row">
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="UpdateEventModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="javascript:clearData()">�</button>
					<h3 id="myModalLabel">Update Event</h3>
				</div>
				<br>
					<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<div class="input-group width-large">
							<span class="input-group-addon">Temporary Note</span> <input
								class="form-control" name="tNote" id="tNoteUpdate"  type="text">
						</div>
					</div>
					
					<div class="col-md-3 col-md-offset-1 ">
						<div class="input-group width-large">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="pNote" id="pNoteUpdate" type="text">
						</div>
					</div>
				</div>
				<div id="mainContentUpdate">
					
				</div>
				<br>
				<div class="input-group width-medium">
					<input name="event" id="event" type="hidden" class="form-control"
						value=" " />
				</div>
					
				<div class="input-group width-medium">
					<input	name="event" id="event" type="hidden"class="form-control" value=" " />
				</div>
					
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addDeliveryScheduleForDay('update');">OK</a>
					<button class="btn btn-danger" data-dismiss="modal" onclick="javascript:clearData()" >Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- This field for users latest date selection -->	
	<input type="hidden" id="selectedDate" >
	
	<div class="modal" id="addEventModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('addEventModal')">�</button>
					<h4 >Add Delivery Schedule</h4>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 col-md-offset-1">
						<div class="input-group width-large">
							<span class="input-group-addon">Temporary Note</span> <input
								class="form-control" name="tNote" id="tNote" type="text">
						</div>
					</div>
					
					<div class="col-md-4 ">
						<div class="input-group width-large">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="pNote" id="pNote" type="text">
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 col-md-offset-1">
						<div class="input-group width-large">
							<span class="input-group-addon">Is Free Trial</span>
							<select id="freeTrial" class="form-control">
								<option value="true">Yes</option>
								<option value="false">No</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div id="mainContentAdd">
					<div class="row" id="firstDiv">
						<div class="col-md-4 col-md-offset-1">
							<div class="input-group width-large">
								<span class="input-group-addon">Product</span>
								<select name="product[]" class="form-control product" >
									<option >-- Select Product --</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group width-large">
								<span class="input-group-addon">Quantity</span>
								<input name="quantity[]" type="text" class="form-control quantity" placeholder="Enter Quantity">
								</div>
							</div>
						<div class="col-md-3">
							<a class="btn btn-primary" href="javascript:addMoreProducts();">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<br>
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addDeliveryScheduleForDay('add');">OK</a>
					<button class="btn btn-danger" onclick="javascript:hideAddModel('addEventModal')">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="addPermenantNote">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('addPermenantNote')">�</button>
					<h4 >Permenant Note</h4>
				</div>
				<br>
				<div class="modal-body">
				<div class="row">
				<div class="col-md-5 col-md-offset-1 ">
						<div id="customerForPNote">
						</div>
					</div>
					<div class="col-md-5  ">
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Permenant Note</span> <input
								class="form-control" name="permNote1" id="permNote1" type="text">
						</div>
					</div>
				</div>
				<br>
				<div class="modal-footer">
					<a class="btn btn-info" href="javascript:addPermenantNote();">OK</a>
					<button class="btn btn-danger" onclick="javascript:hideAddModel('addPermenantNote')">Cancel</button>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="confirmationModalForStop">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 >Confirmation</h3>
				</div>
				<div class="modal-body">
					<p>Are you sure?  You want to Stop selected Customer.</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info" onclick="javascript:showStopCustomerModal()">OK</button>
					<button  class="btn btn-danger" data-dismiss="modal" onclick="javascript:hideAddModel('confirmationModalForStop')">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="stopCustomerModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button"  class="close" onclick="hideAddModel('stopCustomerModal')">�</button>
					<h4 >Stop Customer</h4>
				</div>
				<br>
				<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-md-offset-1 ">
							<div id="customerToStop">
							</div>
						</div>
						<div class="col-md-3 col-md-offset-1">
							<div id="reasonToStopDiv">
								
							</div>
						</div>
					</div>
						<div id="stopCalendarDateDiv">
						</div>
					<br>
					<div class="modal-footer">
						<a class="btn btn-info" href="javascript: StopCustomer();">Stop</a>
						<button class="btn btn-danger" onclick="javascript:hideAddModel('stopCustomerModal')">Cancel</button>
					</div>
				
			</div>
		</div>
	</div>
</div>
<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
<script type="text/javascript">
function addMoreProducts(){
 	var id = Math.floor((Math.random() * 1000) + 12);
	var html = '<div class="row" id="'+id+'"><br><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
	html += '<select name="product[]" class="form-control">'+$(".product").html();
	html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
	html += '<input name="quantity[]" type="text" class="form-control quantity" placeholder="Enter Quantity">';
	html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+id+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	html += '<a class="btn btn-primary" href="javascript:addMoreProducts();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
	$("#mainContentAdd").append(html);
}

function addProductRow(){
	var id = Math.floor((Math.random() * 1000) + 12);
	var html = '<div class="row" id="'+id+'"><br><div class="col-md-4 col-md-offset-1"><div class="input-group width-large"><span class="input-group-addon">Product</span>';
	html += '<select name="productUpdate[]" class="form-control">'+$(".product").html();
	html += '</select></div></div><div class="col-md-4"><div class="input-group width-large"><span class="input-group-addon">Quantity</span>';
	html += '<input name="quantityUpdate[]" type="text" class="form-control quantity" placeholder="Enter Quantity">';
	html += '</div></div><div class="col-md-3" ><a class="btn btn-danger" style="margin-right: 2%;" href="javascript:deleteProductRow(\''+id+'\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	html += '<a class="btn btn-primary" href="javascript:addProductRow();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div></div>';
	$("#mainContentUpdate").append(html);
}

function deleteProductRow(offsetId){
	$("#"+offsetId).remove();
}

function hideAddModel(offset){
	$('#'+offset).modal('hide');
	var html = $("#firstDiv").html();
	$("#mainContentAdd").html("");
 	$("#mainContentAdd").append("<div class='row' id='firstDiv'>"+html+"</div>");
}

function showAddLineItemsBtn(id){
	$(".customWarning").hide();
	
	var selectedType = $("#type_"+id).val();
	if(selectedType == "Weekly"){
		$("."+id).show();
	}else{
		$("."+id).hide();
	}
	if(selectedType == "Custom"){
		$(".customWarning").show();
	}else{
		$(".customWarning").hide();
	}
}
</script>
</body>
</html>