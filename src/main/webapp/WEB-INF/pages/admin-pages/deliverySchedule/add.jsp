<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add DeliverySchedule</title>
<%@ include file="../underscore-templates.tmpl"%>
<script type="text/javascript">
var products;
var lineItemNumber1=1;
var defaultAddress = false;

$(document).ready(function() {
	$("#customer2").hide();
	$("#customer").select2();
	$('#days').hide();
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getAllProducts"/>',
		dataType:'json',
		
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products
			}else{
				alert("Error while retrieving products");
			}
		}
	});
	$('#date').datepicker({autoclose:true,startDate: new Date()});
	$('#deliveryTime').datepicker({autoclose:true});
	
});
var test;
function addLineItem1() {
	var template = _.template($("#lineItemTemplateForDeliverySchedule").html());
	var htmlText = template(products,lineItemNumber1);
	$('#lineItem1Div').append(htmlText);
	test=htmlText;
	lineItemNumber1++;
}

function removeLineItem(subscriptionLineItem) {
	subscriptionLineItem.remove();
}


function onCustomerSelect(){
	var customerId = $('#customer').val();
	$.ajax({
		url:'<c:url value="/restapi/routeRecord/checkRouteRecordForCustomer"/>',
		dataType:'json',
		data : 'customerId=' + customerId,
		success: function(response){
			if(null!=response &&response.success){
				var record = response.data.customerRecord;
			    defaultAddress = record.defaultAddress;
			}else{
				showToast('warning','SomeThing went wrong !! Please refresh page.');
			}
		}
	});
	
}
var recordsLength=0;
function onRouteSelect() {
	
	recordsLength=0;
	var routeId = document.getElementById('route').value;
	var customerId= document.getElementById('customer').value;
	
	$('#customer1').empty();
	
	$.ajax({
		url : '<c:url value="/route/records"/>',
		dataType : 'json',
		data : 'routeId=' + routeId,
		success : function(response) {
			if (response.data.records.length != 0) {
				
				if (response.success) {
					var data = response.data;
					records = data.records;
					
					$('#customer1').append($('<option>', {value:-1, text:'Select Customer'}));
					records.sort(function(a, b){
						 return a.priority-b.priority
						});
					for (var i = 0; i < records.length; i++) {
						var customer = records[i].customerDTO;
						if(customer.id!=customerId){
							$("#customer2").show();
							recordsLength=response.data.records.length;
							var x = document.getElementById("customer1");
							var option = document.createElement("option");
							option.text = customer.firstName;
							option.value = customer.code;
							x.add(option);
						}
					}

				} else {
					alert(response.error.message);
				}
			} else {
				$("#customer2").hide();
				//alert("Nearest Routes are not found.");
			}
		}
	});
}
function showDays(){
	$('#days').show();
}
function addTrialDeliverySchedule() {
	if(validate()){
		$('#addDeliverySchedule').submit();
	}
}

function validate() {
	
	if(!$('#customer').val()){
		showToast("warning","Select Customer");
		$("#customer").focus();
		return false;
	}
	
	var selectedDate = $("#date").val();
	
	if(selectedDate == "" || !selectedDate){
		$("#addEventModal").modal('hide');
		showToast("warning","Select Date");
		$("#date").focus();
		return false;
	}
	if(!$('#route').val()){
		showToast("warning","Select Route");
		$("#route").focus();
		$("#customer2").hide();
		return false;
	}
 	
	if(recordsLength != 0){
		if($('#customer1').val()=="-1"){
			showToast("warning","Select Customer");
			$("#customer1").focus();
			return false;
		}
	}
	
	if(defaultAddress ==false){
		showToast("warning","Please Add Address for selected customer");
		return false;
	}
	
	if(lineItemNumber1<=1){
		showToast("warning","Add At least One Line Item");
		return false;
	}
	
	var products = new Array();
	for(var i=0;i<lineItemNumber1-1;i++){
		
		if(!$($(".productClass")[i]).val()||$($(".productClass")[i]).val()==""){
			showToast("warning","Please select product");
			return false;
		}
		
		var regEx=/[0-9]/;
		
		$($(".quantityClass")[i]).val($($(".quantityClass")[i]).val().trim());
		if(!$($(".quantityClass")[i]).val()||$($(".quantityClass")[i]).val().trim()=="" || $($(".quantityClass")[i]).val()<=0 || !regEx.test($($(".quantityClass")[i]).val())){
			showToast("warning","Please Enter Valid Quantity");
			($(".quantityClass")[i]).focus();
			return false;
		}
		
		var typeAndProduct = ($($(".productClass")[i]).val());
		if($.inArray(typeAndProduct, products) > -1){
			($(".productClass")[i]).focus();
			showToast('warning','Duplicate Product');
			return false;
		}
		
		products.push($($(".productClass")[i]).val());
	}
	
	return true;	
}
</script>
</head>
<c:url value="/admin/deliverySchedule/add" var="deliveryScheduleAdd" />
<c:url value="/admin/deliverySchedule/list" var="deliveryScheduleList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/deliverySchedule/add" var="addDeliverySchedule" />

	<form:form id="addDeliverySchedule" action="${addDeliverySchedulePic}" method="post" modelAttribute="deliverySchedule" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
	<fieldset>
			<h3 class="page-header">DeliverySchedule</h3>
			<div class="row ">
				<div class="col-md-4">
				<form:errors path="customer" cssClass="alert-danger" />
				<div class="input-group width-xlarge">
				<span class="input-group-addon">User</span>
				<form:select path="customer" cssClass="form-control" id="customer" onchange="javaScript:onCustomerSelect()">
					<form:option value="" >Select Customer</form:option>
					<c:forEach items="${users}" var="user">
						<option value="${user.id}">${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
					</c:forEach>
				</form:select>
				</div>
			<br>
			<form:errors path="type" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Type</span> 
				<form:select path="type" cssClass="form-control">
					<form:option value="Free_Trial">Free_Trial</form:option>
					<form:option value="Paid_Trial">Paid_Trial</form:option>
				</form:select>
			</div>
			<br>
			<form:errors path="date" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Date</span>
				<input name="date" id="date" class="form-control" onchange="showDays()" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${deliverySchedule.date}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="" cssClass="alert-danger" />
			<div class="input-group width-xlarge" id="days">
				<span class="input-group-addon">No of Days</span>
				<select name="days" class="form-control"  >
					<c:forEach begin="${1}" end="${7}" var="day">
						<option value="${day}">${day} </option>
					</c:forEach>
				</select>
			</div>
			<br>
			<form:errors path="route" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Route</span>
				<form:select path="route" cssClass="form-control" id="route" onchange="onRouteSelect()">
					<form:option value="" >Select Route</form:option>
					<c:forEach items="${routes}" var="route">
						<option value="${route.id}">${route.name} </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<div class="input-group width-xlarge" id="customer2">
				<span class="input-group-addon">After</span> 
				<select name="customer1" id="customer1" class="form-control">
				</select>
			</div>
			<br>
		</div>
		<div class="col-md-8">
			<h4>Delivery Line Items</h4>
				<div id="lineItem1Div">
					<form:errors path="deliveryLineItems" cssClass="text-danger"/>
					<a href="javascript:addLineItem1()" class="btn btn-info">Add Line Item</a>
				</div>
		</div>
		</div>
		</fieldset>
		<div class="row border-bottom white-bg dashboard-header">
			<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:addTrialDeliverySchedule()">Add</a> &nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
		</div>
		
	</form:form>
</body>