<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add UserRoute</title>
<script type="text/javascript">
$(document).ready(function() {
	
	var route=$('#route').val();
	if(route==null||route==""){
		
		$('.warningDiv').show();
		$('.routeDiv').hide();
		$('.createbutton').unbind('click', false);
	}else{
		$('.warningDiv').hide();
		$('.routeDiv').show();
	}
});


function submitForm() {
	if(validate())
		$('#addUserRoute').submit();
}

function validate() {
	
	var route=$('#route').val();
	
	if(route!=null){
	 if(!$('#deliveryBoy').val() || $('#deliveryBoy').val()=="-1"){
		  showToast('warning','Please select Delivery Boy');
		  return false;
	  }
	 
	 if(!$('#route').val() || $('#route').val()=="-1"){
		  showToast('warning','Please select route');
		  return false;
	  }
	}else{
		showToast('warning','Add Routes then Try again...!!!');
		  return false;
	}
	
	return true;
	
}
</script>
</head>
<c:url value="/userRoute/add" var="userRouteAdd" />
<c:url value="/userRoute/list" var="userRouteList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/userRoute/add" var="addUserRoute" />

	<form:form id="addUserRoute" action="${addUserRoutePic}" method="post" modelAttribute="userRoute" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">UserRoute</h3>
			<div class="routeDiv">
			<form:errors path="deliveryBoy" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Delivery Boy*</span>
				<form:select path="deliveryBoy" cssClass="form-control">
					<option value="-1">Select Delivery Boy</option>
					<c:forEach items="${users}" var="user">
						<option value="${user.id}">${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="collectionBoy" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Collection Boy</span>
				<form:select path="collectionBoy" cssClass="form-control">
				<option value="-1">Select Collection Boy</option>
					<c:forEach items="${collectionBoys}" var="user">
						<option value="${user.id}">${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="route" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Route*</span>
				<form:select path="route" items="${routes}" itemLabel="name"
					itemValue="id" cssClass="form-control"></form:select>
			</div>
			<br>
		
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		</div>
		</fieldset>
		<div class="warningDiv">
			<span class="alert alert-warning">No Route to be Assign to Delivery Boy And Collection Boy</span>
		</div>
		<br />
		<br />
		<a class="btn btn-info createbutton" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>