<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - UserRoute</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>UserRoute</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>UserRoute</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/userRoute/list" />
	<c:set var="urlPrefix" value="/admin/userRoute/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/userRoute/show" var="userRouteDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="userRoutes" class="footable" requestURI="" id="userRoute" export="false">
			<display:column title="Code" sortable="true">
				<a href="${userRouteDetailLinkPrefix}/${userRoute.cipher}">${userRoute.code}</a>
			</display:column>
			
			<display:column title="Route" sortable="true">${userRoute.route.name}</display:column>
			<display:column title="Delivery Boy" sortable="true">${userRoute.deliveryBoy.firstName} - ${userRoute.deliveryBoy.lastName} - ${userRoute.deliveryBoy.mobileNumber}</display:column>
			<display:column title="Collection Boy" sortable="true"><c:if test="${not empty userRoute.collectionBoy}"> ${userRoute.collectionBoy.firstName}-${userRoute.collectionBoy.lastName} - ${userRoute.collectionBoy.mobileNumber}</c:if></display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/userRoute/update/${userRoute.cipher}/${userRoute.route.id}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/userRoute/delete/${userRoute.cipher}">
							<li><a href="#ConfirmModal_${userRoute.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${userRoute.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete UserRoute?</h3>
							</div>
							<div class="modal-body">
								<p>The UserRoute will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/userRoute/delete/${userRoute.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete userRoutes?</h3>
		</div>
		<div class="modal-body">
			<p>The selected userRoute will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>