<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update UserRoute</title>
<script type="text/javascript">
$(document).ready(function() {
});

function submitForm() {
	if(validate())
		$('#updateUser').submit();
}

function validate() {
	
	
	 if(!$('#deliveryBoy').val() || $('#deliveryBoy').val()=="-1"){
		  showToast('warning','Please select Delivery Boy');
		  return false;
	  }
	 
	 return true;
	}
</script>
</head>

<c:url value="/admin/userRoute/update" var="userRouteUpdate" />
<c:url value="/admin/userRoute/list" var="userRouteList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/userRoute/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="userRoute" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">UserRoute</h3>
				
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Route</span>
				 <input type="text"	name="routeName" id="routeName" value="${route.name}" readonly="true" class="form-control">
				
			</div>
			<br>
			 <input type="hidden"name="route1" id="route1" value="${route.id}" readonly="true" class="form-control">
			<form:errors path="deliveryBoy" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Delivery Boy*</span>
				<form:select path="deliveryBoy" cssClass="form-control">
					<option value="-1">Select Delivery Boy</option>
					<c:forEach items="${users}" var="user">
						<option value="${user.id}" <c:if test="${user.id == userRoute.deliveryBoy.id}">selected="selected"</c:if>>${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="collectionBoy" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Collection Boy</span>
				<form:select path="collectionBoy" cssClass="form-control">
					<option value="-1">Select Collection Boy</option>
					<c:forEach items="${collectionBoys}" var="user">
						<option value="${user.id}" <c:if test="${user.id == userRoute.collectionBoy.id}">selected="selected"</c:if>>${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			
		
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info createbutton" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
          	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>