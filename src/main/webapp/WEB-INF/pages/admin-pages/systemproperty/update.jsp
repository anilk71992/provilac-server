<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update System Property</title>

</head>

<c:url value="/admin/systemProperty/update" var="propertyUpdate" />
<c:url value="/admin/systemProperty/list" var="propertyList" />

<body>

	<%--@ include file="nav.jsp"--%>


	<form:form id="updateProperty" method="post" modelAttribute="systemProperty" action="${propertyUpdate}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="alert alert-error">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
            	<h3 class="page-header">System Property</h3>
            
			<form:errors path="propName" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Property Name</span>
				<form:input path="propName" cssClass="form-control" readonly="true" />
			</div>
			<br>
			<form:errors path="propValue" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Property Value</span>
				<form:input path="propValue" cssClass="form-control" />
			</div>
			<br>

        </fieldset>
		<c:choose>
			<c:when test="${readOnly}">
				<input type="submit" class="btn btn-info" title="OK">
<%-- 				<a class="btn btn-info" href="<c:url value="/admin/systemproperty/list"/>">Ok --%>
<!-- 				</a> -->
			</c:when>
			<c:otherwise>
				<button class="btn btn-info" type="submit">Update</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
			</c:otherwise>
		</c:choose>

		<input type="hidden" name="validateForUpdate" value="true" />
	</form:form>

</body>