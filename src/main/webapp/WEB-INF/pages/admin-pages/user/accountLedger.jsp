<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - User</title>
</head>
<c:url value="/admin/invoice/show" var="invoiceDetailLinkPrefix" />
<c:url value="/admin/payment/show" var="paymentDetailLinkPrefix" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Account Ledger</h3>
			<h4>CustomerName : - ${customer}</h4>
			<h4>Total Dues : - ${totalDues}</h4>
		</div>
	</div>
<div class="row">
	<div class="col-md-6">
		<h5>Invoice Details</h5>
			<table class="footable">
				<thead>
					<tr>
						<th data-class="expand"><u>Invoice Code</u></th>
						<th><u>From Date</u></th>
						<th><u>To Date</u></th>
						<th><u>Amount</u></th>
						<th><u>Last Pending Dues</u></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${invoices}" var="invoice">
					<tr>
						<td><a href="${invoiceDetailLinkPrefix}/${invoice.cipher}">${invoice.code}</a></td>
						<td>${invoice.fromDate}</td>
						<td>${invoice.toDate}</td>
						<td>${invoice.totalAmount}</td>
						<td>${invoice.lastPendingDues}</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
	</div>
	<div class="col-md-6">
		<fieldset>
			<h5>Payment Details</h5>
			<table class="footable">
				<thead>
					<tr>
						<th><u>Code</u></th>
						<th><u>Amount(Adj.)</u></th>
						<th><u>Type</u></th>
						<th><u>Date</u></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${payments}" var="payment">
					<tr>
						<td><a href="${paymentDetailLinkPrefix}/${payment.cipher}">${payment.code}</a></td>
						<c:if test="${payment.adjustmentAmount<=0}">
						<td>${payment.amount}</td>
						</c:if>
						<c:if test="${payment.adjustmentAmount>0}">
						<td>${payment.amount} (${payment.adjustmentAmount})</td>
						</c:if>
						<c:if test="${payment.paymentMethod !='Cheque'}">
						<td>${payment.paymentMethod}</td>
						</c:if>
						<c:if test="${payment.paymentMethod == 'Cheque'}">
							<c:if test="${payment.isBounced}">
								<td>${payment.paymentMethod}(Bounced)</td>
							</c:if>
						</c:if>
						<c:if test="${payment.paymentMethod == 'Cheque'}">
							<c:if test="${!payment.isBounced}">
								<td>${payment.paymentMethod}</td>
							</c:if>
						</c:if>
						<td><fmt:formatDate value="${payment.date}" pattern="dd-MM-yyyy"/></td>					
					</tr>
						</c:forEach>
				</tbody>
			</table>
		</fieldset>
		<span class="label label-warning customWarning">Amount(Adj.) is Amount And (adjustment Amount) & if cheque bounce then type is Cheque(Bounced). </span>
	</div>
	
	<vr>
	
</div>
<br />
<br />
     <a class="btn btn-danger" href="javascript:history.back(1)">OK</a>
</body>
</html>