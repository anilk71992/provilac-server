<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Pending Dues Customers</title>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Customers</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Pending Dues Customers</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/user/pendingDuesList" />
	<c:set var="urlPrefix" value="/admin/user/pendingDuesList?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/user/show" var="userDetailLinkPrefix" />
	<%@ include file="navLastPendingList.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table  name="users" class="footable" requestURI="" id="user" export="false">
			<display:column title="Code" sortable="true">
				<a href="${userDetailLinkPrefix}/${user.cipher}">${user.code}</a>
			</display:column>
			
			<display:column title="Name" sortable="true">${user.firstName} ${user.lastName}</display:column>
			<display:column property="username" title="Number" sortable="true"/>
			<display:column property="email" title="Email" sortable="true"/>
			<display:column property="lastPendingDues" title="Pending Due" sortable="true"/>
		</display:table>
	</form>
</body>
</html>