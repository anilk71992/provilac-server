<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/user/addUser" var="userAdd" />
<c:url value="/admin/user/list" var="userList" />
<c:url value="/admin/user/import" var="userImport"/>
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	 <form class="form-inline" action="${urlBase}" action="get" role="form" id="getStatusReport">
	  			<div class="row">
	  			<div class="col-md-4">
								<input  id="startDate" placeholder="Start Date" name="startDate"class="form-control" 
									data-date-format="dd-mm-yyyy" />
						<br><br>
								<input  id="endDate" placeholder="End Date" name="endDate" class="form-control" 
									data-date-format="dd-mm-yyyy" />
					</div>
					<div class="col-md-4">
	 				<div class="input-group">
								<span class="input-group-addon">Status</span> <select
									name="status1" id="status" cssClass="form-control"
									class="form-control">
									<option value="NEW">NEW</option>
									<option value="HOLD">HOLD</option>
									<option value="ACTIVE">ACTIVE</option>
									<option value="INACTIVE">INACTIVE</option>
								</select>
							</div>
							<br>
							<span>To</span><br>
							<div class=" input-group">
								<span class="input-group-addon">Status</span> <select
									name="status2" id="status" cssClass="form-control"
									class="form-control">
									<option value="NEW">NEW</option>
									<option value="HOLD">HOLD</option>
									<option value="ACTIVE">ACTIVE</option>
									<option value="INACTIVE">INACTIVE</option>
								</select>
							</div>
							</div><br>
							<div class="col-md-4">
							 <a href="javascript:submitForm();" class="btn btn-info">GET</a></div>
							</div>
						
	</form>
	</div>
	
</div>