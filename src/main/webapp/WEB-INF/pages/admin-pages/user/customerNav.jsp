<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/user/add" var="userAdd" />
<c:url value="/admin/user/import" var="userImport"/>
<c:url value="/admin/user/paymentModeImport" var="paymentModeImport"/>
<c:url value="/admin/customer/show" var="userDetailLinkPrefix" />
<script type="text/javascript">
/* var availableTags=Array();
$(document).ready(function() {
	
	$("#searchTerm").select2();
	 
	$.ajax({
		url : '<c:url value="/restapi/user/getAllCustomers"/>',
		dataType : 'json',
		success : function(response) {
			if (response.data.customers.length != 0) {
				if (response.success) {
					for(var i=0;i<response.data.customers.length;i++){
						var customer =response.data.customers[i];
						
						  $('.searchTermClass').append($('<option>', {
						         value: customer.code,
						         text: customer.code +" - "+ customer.firstName + " - " + customer.lastName +" - "+customer.mobileNumber
						     }));
					}
				
				} else {
					alert(response.error.message);
				}
			} else {
			}
		}
	});
	
});

function showPageCustomer(){
	var customerCode=$("#searchTerm").val();
	var model ="modelDemo";
	window.location.href = "/provilac/admin/customer/show?customerCode=" + customerCode, "model=" + model;
	
 } */
</script>

<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<a class="btn btn-primary" href="${userAdd}">Add New Customer</a>&nbsp;
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
		<a class="btn btn-primary" href="${userImport}">Import Customers</a>&nbsp;
	</sec:authorize>
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
		<a class="btn btn-primary" href="${paymentModeImport}">Import Payment Mode</a>&nbsp;
	</sec:authorize>
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>