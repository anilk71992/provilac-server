<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - User</title>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>User</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>User</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/user/list" />
	<c:set var="urlPrefix" value="/admin/user/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/user/show" var="userDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table  name="users" class="footable" requestURI="" id="user" export="false">
			<display:column title="Code" sortable="false">
				<a href="${userDetailLinkPrefix}/${user.cipher}">${user.code}</a>
			</display:column>
			
			<display:column title="Name" sortable="false">${user.firstName} ${user.lastName}</display:column>
			<display:column property="username" title="Username" sortable="false"/>
			<display:column property="email" title="Email" sortable="false"/>
			<display:column title="Verified" sortable="false">
				<c:choose>
					<c:when test="${user.isVerified}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Enabled" sortable="false">
				<c:choose>
					<c:when test="${user.isEnabled}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Roles" sortable="false">
				<c:forEach items="${user.userRoles}" var="role" varStatus="loopVar">
					<c:if test="${not loopVar.first}">,</c:if>
					${role.role}
				</c:forEach>
			</display:column>
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/user/updateUser/${user.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/user/delete/${user.cipher}">
							<li><a href="#ConfirmModal_${user.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
						
						<%-- <li>
							<a href="#changeStatusModal_${user.id}" data-toggle="modal">Change Status</a>
						</li> --%>
						<%-- <li><a href="<c:url value="/admin/invoice/list?userCode=${user.code}"/>">Show Invoices</a>
						</li>
						<li><a href="<c:url value="/admin/payment/list?userCode=${user.code}"/>">Show Payments</a>
						</li>
						<li><a href="<c:url value="/admin/payment/accountLedger/${user.code}"/>">Show accountLedger</a>
						</li> --%>
					</ul>
				</div>

				<!-- modal -->
				<%-- <div class="modal" id="changeStatusModal_${user.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Change Status</h3>
							</div>
							<div class="modal-body">
								<div class="row">
									<fieldset>
										<div class="col-md-10 col-md-offset-1">
											<div class="input-group">
												<span class="input-group-addon">User</span>
												<input class="form-control" value="${user.email}" readonly="readonly" />
											</div>
										</div>
									</fieldset>
								</div>
								<br>
								<div class="row">
									<fieldset>
										<div class="col-md-10 col-md-offset-1">
											<div class="input-group">
												<span class="input-group-addon" >Status</span>
												<select name="status" id="status_${user.id}" class="form-control">
													<option value="NEW" <c:if test="${user.status == 'NEW'}">selected='selected'</c:if>>NEW</option>
													<option value="HOLD" <c:if test="${user.status == 'HOLD'}">selected='selected'</c:if>>HOLD</option>
													<option value="INACTIVE" <c:if test="${user.status == 'INACTIVE'}">selected='selected'</c:if>>INACTIVE</option>
												</select>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button" href='javascript:changeStatus("${user.id}");'>ok</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div> --%>
				<div class="modal" id="ConfirmModal_${user.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete User?</h3>
							</div>
							<div class="modal-body">
								<p>The User will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/user/delete/${user.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete users?</h3>
		</div>
		<div class="modal-body">
			<p>The selected user will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->
</body>
</html>