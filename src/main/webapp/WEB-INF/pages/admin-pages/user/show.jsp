<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - User</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">User- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${user.firstName} ${user.lastName}</p>
				<label class="col-md-2">Mobile Number</label>
				<p class="col-md-2">${user.mobileNumber}</p>
				<label class="col-md-2">Verified</label>
				<p class="col-md-2">
					<c:choose>
						<c:when test="${user.isVerified}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
					</c:choose>
				</p>
				
			</div>
			<div class="row">
				<label class="col-md-2">Email</label>
				<p class="col-md-2 ">${user.email}</p>
				
				<label class="col-md-2 col-md-offset-2">Gender</label>
				<p class="col-md-2">
					<c:choose>
						<c:when test="${user.gender}">Male</c:when>
						<c:otherwise>Female</c:otherwise>
					</c:choose>
				</p>
			</div>
			<div class="row">
				
				<label class="col-md-2">Date of Birth</label>
				<p class="col-md-2"><fmt:formatDate value="${user.dob}" pattern="dd-MM-yyy"/></p>
				
				<label class="col-md-2">Address</label>
				<p class="col-md-4">${user.buildingAddress}</p>
				
			</div>

			<div class="row">
				<label class="col-md-2">Joining Date</label>
				<p class="col-md-2"><fmt:formatDate value="${user.joiningDate}" pattern="dd-MM-yyy hh:mm:ss a"/></p>
				<label class="col-md-2">Status</label>
				<p class="col-md-2">${user.status}</p>
				<label class="col-md-2">ReferredMedia</label>
				<p class="col-md-2">${user.referredMedia}</p>
			</div>	
			<div class="row">
				<label class="col-md-2">Referred By</label>
				<p class="col-md-2">${user.referredBy.username}</p>
				<label class="col-md-2">Route Assigned</label>
				<p class="col-md-2">
					<c:choose>
						<c:when test="${user.isRouteAssigned}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
					</c:choose>
				</p>
				<label class="col-md-2">AlterNate Mobile</label>
				<p class="col-md-2">${user.alterNateMobileNumber}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Google Address</label>
				<p class="col-md-4">${user.address}</p>
				<label class="col-md-2">Payment Method</label>
				<p class="col-md-4">${user.paymentMethod}</p>
				<label class="col-md-2">Last Pending Dues</label>
				<p class="col-md-2">${user.lastPendingDues}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Referral Code</label>
				<p class="col-md-2">${user.referralCode}</p>
				<label class="col-md-2">Verification Code</label>
				<p class="col-md-2">${user.verificationCode}</p>
				
			<label class="col-md-2">Enabled</label>
					<c:choose>
						<c:when test="${user.isEnabled}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
					</c:choose>
			</div>
				
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${user.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${user.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${user.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${user.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>