<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link rel="stylesheet" href="<c:url value="/assets/end-user/css/select2.min.css"/>" rel="stylesheet">
<title>Provilac - Add User</title>

<script type="text/javascript">
//var routes1 = new array();
var routeRecordMap = new Object();
var markers = new Array();
var infoWindows = new Array();
var customers= new Array();

$(document).ready(function() {
 	$('#referredBy').select2();
	$('#errorMessage').empty();
	$('#dob').datepicker({ autoclose: true});
	$('#joiningDate').datepicker({ autoclose: true});
	$('#referredByDiv').hide();
	$('#searchTextField').keypress(function(event) {
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13') {
	      $("#latId").focus();
	    	// continue;
	    }
	});
	 
	 onReferredMediaChange();
});

	function onReferredMediaChange() {
		var onReferredMediaSelect = document.getElementById('referredMedia');
		var onReferredMedia = onReferredMediaSelect.options[onReferredMediaSelect.selectedIndex]
				.getAttribute('numVal');
		if (onReferredMedia == 6) {
			$('#referredByDiv').show();
		} else {
			$('#referredByDiv').hide();
		}
	}
	function onSuggestRouteClick() {
		var lat = document.getElementById('latId').value;
		var lng = document.getElementById('lngId').value;
		$
				.ajax({
					url : '<c:url value="/routeRecord/routes"/>',
					dataType : 'json',
					data : 'lat=' + lat + '&lng=' + lng,
					success : function(response) {
						if (response.data.routes.length != 0) {
							if (response.success) {
								var data = response.data;
								routes1 = data.routes;
								$('#route').empty();

								for (var i = 0; i < data.routes.length; i++) {
									var route = data.routes[i];
									var html = '<input type="radio" onchange="onRouteSelect(\''+ route.code.trim()+ '\')" name="route" class="clearSelection" id="route_'+ i+ '" value="'+ route.code+ '" />'+ '<label>&nbsp;'+ route.name + '</label> <br>';
									$('#route').append(html);

									routeRecordMap[route.code] = route.routeRecordDTOs;
								}

							} else {
								alert(response.error.message);
							}
						} else {
							//alert("Nearest Routes are not found.");
							$('#ConfirmModal').show();
						}
					}
				});
	}

	function onRouteSelect(code) {

		$('#customer').empty();
		removeAllMarkers();
		var routeRecords = routeRecordMap[code];
		routeRecords.sort(function(a, b){
			 return a.priority-b.priority
			});
		for (var i = 0; i < routeRecords.length; i++) {
			var customer = routeRecords[i].customerDTO;
				customers[i] = customer;
				var html = '<input type="radio" name="customer" class="clearSelection" onchange="onCustomerChange('+ i	+ ')" value="'+ customer.code+ '" />'+ '<label>&nbsp;'+ customer.firstName+ " "	+ customer.lastName +"-"+customer.address+ '</label> <br>';
				$('#customer').append(html);

		}
		drawRoute();
	}

	function drawRoute() {
		var wayPoints = new Array();

		for (var i = 0; i < customers.length; i++) {
			var customer = customers[i];
			wayPoints.push({
				location : new google.maps.LatLng(customer.lat, customer.lng),
				stopover : true
			});
		}
		var start = wayPoints[0].location;
		var end = wayPoints[wayPoints.length - 1].location;

		var request = {
			origin : start,
			destination : end,
			waypoints : wayPoints,
			travelMode : google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				directionsDisplay.setMap(map);
				addMarkers();
			} else {
				alert("Directions Request from " + start.toUrlValue(6) + " to "
						+ end.toUrlValue(6) + " failed: " + status);
			}
		});
	}

	function addMarkers() {
		for (var i = 0; i < customers.length; i++) {
			var customer = customers[i];

			var infowindow = new google.maps.InfoWindow({
				content : customer.firstName + ' ' + customer.lastName
			});

			var marker = new google.maps.Marker({
				position : new google.maps.LatLng(customer.lat, customer.lng),
				map : map,
				title : customer.firstName + ' ' + customer.lastName
			});

			infowindow.open(map, marker);

			markers.push(marker);
			infoWindows.push(infowindow);
		}
		customers = new Array();
	}
	function onCustomerChange(index) {
		infoWindows[index].open(map, markers[index]);
	}
	
	function removeAllMarkers() {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
		markers = new Array();
		infoWindows = new Array();
	}

	function addRoute() {
		$('#ConfirmModal').hide();
		$('#addRouteModal').show();
	}
	
	function addNewRoute() {
		$('#route1').val("");
		$('#errorMessage').empty();
		$('#addRouteModal').show();
	}

	function getRouteValue() {
		var routeName = document.getElementById('route1').value;
		$('#modalRoute').val(routeName);
	}
	
	function showRoutes() {
		var lat = $("#latId").val();
		var lng = $("#lngId").val();
		
		if(lat == "" || lng == ""){
			showToast('warning','Select Latitude or Longitude');
			return false;
		}
		var routeName = document.getElementById('route1').value;
		
		if(routeName == "" ){
			showToast('warning','Enter Route Name');
			return false;
		}
		
		$.ajax({
			url : '<c:url value="/routeRecord/routes"/>',
			dataType : 'json',
			data : 'lat=' + lat + '&lng=' + lng + '&routeName=' + routeName,
			success : function(response) {
					if (response.success) {
						var data = response.data;
						routes1 = data.routes;
						$('#route').empty();

						for (var i = 0; i < data.routes.length; i++) {
							var route = data.routes[i];
							if(route.name==routeName){
								var html = '<input type="radio" checked="checked" onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')"  class="clearSelection" name="route" id="route_'+ i	+ '" value="'+ route.code+ '" />'
									+ '<label>&nbsp;'
									+ route.name + '</label> <br>';
								$('#route').append(html);
							}else{
								var html = '<input type="radio"  onchange="onRouteSelect(\''	+ route.code.trim()	+ '\')" name="route"  class="clearSelection" id="route_'+ i	+ '" value="'+ route.code+ '" />'
								+ '<label>&nbsp;'
								+ route.name + '</label> <br>';
							$('#route').append(html);
							}
							routeRecordMap[route.code] = route.routeRecordDTOs;
						}
						$('#addRouteModal').hide();

					} else {
						$('#errorMessage').empty();
						 var html= '<font size="3" color="red">' + response.error.message  + '</font>';
						$('#errorMessage').append(html);
						
						 /* alert(response.error.message); */ 
						
					}
			}
		});
		
	}
	
	function submitForm() {
		if(validate()) {
		$("#addUser").on("click", function() {
			$('#addUser').submit();
	        $(this).attr("disabled", "disabled");
	        doWork(); //this method contains your logic
	    });
		}
	}

	function doWork() {
	    setTimeout('$("#btn").removeAttr("disabled")', 1500);
	}
	
	function validate() {
		var mobileNumValid=$('#mobileNumber').val().trim();
		var IndNum = /^(\+91-|\+91|0)?\d{10}$/;
		
		if (!mobileNumValid.match(IndNum)) {
			$('#mobileNumber').focus();
			showToast('warning','Please Enter Valid mobile Number of 10 digits');
			return false;
		}
		
		if (!$('#buildingAddress').val()) {
			$('#buildingAddress').focus();
			showToast('warning','Please enter Building Address');
			return false;
		}
		
		  if(!$('#city').val() || $('#city').val()=="-1"){
			  showToast('warning','Please select city');
			  return false;
		  }
		  
		if (!$('#searchTextField').val()) {
			$('#searchTextField').focus();
			showToast('warning','Please enter address');
			return false;
		}
		
		var emailid = $('#email').val();
		var atpos = emailid.indexOf("@");
	    var dotpos = emailid.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailid.length) {
	    	showToast('warning',"Not a valid e-mail address");
	        return false;
	    }
		
		 var selected = $("#route input[type='radio']:checked");
		  if(selected.length==0){
			  showToast('warning','Please Select Route');
				return false;
		  }
		
			return true;	
	}
</script>
<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 450px;         
    margin: 0.6em;       
}

</style>

</head>
<c:url value="/user/add" var="userAdd" />
<c:url value="/user/list" var="userList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/user/add" var="addUser" />

	<form:form id="addUser" action="${addUserPic}" method="post" modelAttribute="user" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Cutomer</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<fieldset>
							<form:errors path="firstName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">First Name</span>
								<form:input path="firstName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="lastName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Name</span>
								<form:input path="lastName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="mobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Mobile Number*</span>
								<form:input path="mobileNumber" id="mobileNumber"
								  			onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57)"  cssClass="form-control" />
							</div>
							<br>

							<form:errors path="alterNateMobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Alternate Mobile</span>
								<form:input path="alterNateMobileNumber" id="alterNateMobileNumber" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57)" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="email" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Email*</span>
								<form:input path="email" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="password" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Password*</span>
								<form:input path="password" type="password" cssClass="form-control" />
							</div>
							<br>

							
							
							<form:errors path="buildingAddress" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Building Address*</span>
								<form:input path="buildingAddress" cssClass="form-control" />
							</div>
							<br>
							
							<form:errors path="dob" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Date of Birth*</span>
								<form:input path="dob" id="dob" cssClass="form-control" value ="01-03-1991"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>

							<form:errors path="gender" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Gender*</span>
								<form:select path="gender" cssClass="form-control">
									<form:option value="true">Male</form:option>
									<form:option value="false">Female</form:option>
								</form:select>
							</div>
							<br>
							<form:errors path="joiningDate" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Joining Date*</span>
								<form:input path="joiningDate" cssClass="form-control" value ="${today}"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Status*</span> <select
									name="status" id="status" cssClass="form-control"
									class="form-control">
									<option value="NEW">NEW</option>
									<option value="HOLD">HOLD</option>
									<option value="ACTIVE">ACTIVE</option>
									<option value="INACTIVE">INACTIVE</option>
								</select>
							</div>
							<br>
							<div id="assignedTo" class="input-group width-xlarge">
								<span class="input-group-addon">Assigned To</span>
								<select name="assignedTo" id="assignedTo" class="form-control">
									<option value="-1">Select User </option>
									<c:forEach items="${assignedToUsers}" var="assignedUser">
										<option value="${assignedUser.id}" >${assignedUser.fullName} - ${assignedUser.mobileNumber}</option>
									</c:forEach>
								</select>
							</div>
							<br>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Pending Dues*</span>
								 <form:input path="lastPendingDues" class="form-control" />
							</div>
							<br>
								
						<form:errors path="paymentMethod" cssClass="text-danger" />
						<div class="input-group width-xlarge">
							<span class="input-group-addon">Payment Method</span> 
							<form:select path="paymentMethod" cssClass="form-control">
								<form:option value=" ">Select Payment Method</form:option>
								<form:option value="Cash">Cash</form:option>
								<form:option value="Online">Online</form:option>
								<form:option value="Cheque">Cheque</form:option>
							</form:select>
						</div>
						<br>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Referred Media</span> <select
									name="referredMedia" id="referredMedia" cssClass="form-control"
									class="form-control" onchange="onReferredMediaChange()">
									<option value="Social_Media" numVal="1">Social_Media</option>
									<option value="Website" numVal="2">Website</option>
									<option value="Newspaper" numVal="3">Newspaper</option>
									<option value="Radio" numVal="4">Radio</option>
									<option value="Event" numVal="5">Event</option>
									<option value="Friend" numVal="6">Friend</option>
								</select>
							</div>
							<br>
						<div id="referredByDiv" class="input-group width-xlarge">
								<span class="input-group-addon">Referred By</span>
								<select name="referredBy" id="referredBy" class="form-control">
									<option value="-1">Select Customer </option>
									<c:forEach items="${users}" var="customer">
										<option value="${customer.id}" >${customer.fullName} - ${customer.mobileNumber}</option>
									</c:forEach>
								</select>
								<br>
							</div>
							<br>
							<form:errors path="isEnabled" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Enabled</span>
								<form:select path="isEnabled" cssClass="form-control">
									<form:option value="true">Yes</form:option>
									<form:option value="false">No</form:option>
								</form:select>
								<br>
							</div>
							<br>
							<div class="input-group width-xlarge">
							<span class="input-group-addon">City*</span>
							<select name="city" id="city" class="form-control">
								<option value="-1">Select City </option>
								<c:forEach items="${cities}" var="city">
									<option value="${city.id}" >${city.name}</option>
								</c:forEach>
							</select>
							<br>
							</div>
							<div class="input-group width-xlarge">
								<input type="hidden" name="routeName" id="modalRoute"
									class="form-control" />
							</div>
							<br>
							<form:errors path="" cssClass="text-danger" />
							<div class="input-group width-mediumlarge" id="select2City">
								<span class="input-group-addon">Google Address*</span> <input
									id="searchTextField" name="address" type="text" size="50"  class="form-control" />
							</div>
							<br>
							<div class="row">
								<div class="col-md-5">
									<div class="input-group width-xmedium" id="latDiv">
										<span class="input-group-addon">Lattitude*</span> 
										<input name="lat" id="latId"  value="18.5203" class="form-control" />
									</div>	
								</div>
								<div class="col-md-5">
									<div class="input-group width-xmedium" id="lngDiv">
										<span class="input-group-addon">Longitude*</span>
									 	<input	name="lng" id="lngId"  value="73.8567" class="form-control" />
									</div>
								</div>
							</div>
							
							<br>
							<div id="map_canvas"></div>

							<a class="btn btn-info" href="javascript:onSuggestRouteClick();">Suggest Route</a> &nbsp;
							<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
								<a class="btn btn-info" href="javascript:addNewRoute();">Add New Route</a> <br />
							</sec:authorize>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Routes</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group width-xxlarge" id="route">
						</div>
					</div>

					<div id="customer" class="col-md-6"></div>

				</div>
				<div class="row">
					<div class="col-md-12">
					  <a class="btn btn-danger btn-xs pull-right" href="javascript:clearSelectedValues();">Clear Selection</a>
					</div>
				</div>
			</div>
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
	<div class="modal" id="ConfirmModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="hideModel('ConfirmModal')">�</button>
					<h3 id="myModalLabel" style="margin-left: 60px">Add Route</h3>
				</div>
				<div class="modal-body" style="margin-left: 70px">
					<p>No Routes  found!</p>
				</div>
				<div>
				<a class="btn btn-info" href="javascript:addRoute();" style="margin-left: 80px;">Add Route</a> <br />
				</div>
				<br>
				<div class="modal-footer">
					<!-- <button class="btn" data-dismiss="modal" onclick="hideModel('ConfirmModal')">Cancel</button> -->
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="addRouteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="hideModel('addRouteModal')">�</button>
					<h3 id="myModalLabel" style="margin-left: 40px">Add Route</h3>
				</div>
				<div>
					<div class="input-group width-xlarge" style="margin-left: 40px; margin-top: 20px" id="routeToAdd">
						<span class="input-group-addon">Route</span> <input name="route1" id="route1" class="form-control" />
					</div>
					<br>
					<div id="errorMessage" class="input-group width-xlarge" style="margin-left: 40px;">
					</div>
					<br>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" onclick="showRoutes();">OK</button>
						<button class="btn" data-dismiss="modal"onclick="hideModel('addRouteModal')">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
	<script type="text/javascript">
	
	var lat = 18.5203, 
		lng = 73.8567, latlng = new google.maps.LatLng(lat, lng),
		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

	var geocoder = new google.maps.Geocoder();
	var directionsService = new google.maps.DirectionsService;

	var directionsDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers: true
	});
	var map = new google.maps.Map(document.getElementById('map_canvas'), {
		center : new google.maps.LatLng(lat, lng),
		zoom : 15,
	});
	
	var marker = new google.maps.Marker({
		position : latlng,
		map : map,
		icon : image,
		draggable : true
	});
	directionsDisplay.setMap(map);
	var searchBox = new google.maps.places.SearchBox(document.getElementById('searchTextField'));

	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();
		var bounds = new google.maps.LatLngBounds();
		var place;
		for (var i = 0; place = places[i]; i++) {
			bounds.extend(place.geometry.location);
			marker.setPosition(place.geometry.location);
		}
		map.fitBounds(bounds);
		map.setZoom(17);
	});

	google.maps.event.addListener(marker, 'position_changed', function() {
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		
		geocodePosition(marker.getPosition());
		$('#latId').val(lat);
		$('#lngId').val(lng);
	});
	
	function geocodePosition(pos) {
	  geocoder.geocode({
	    latLng: pos
	  }, function(responses) {
	    if (responses && responses.length > 0) {
	    	$("#searchTextField").val(responses[0].formatted_address);
	    }
	  });
	}
	
	function clearSelectedValues(){
		$('#customer').empty();
		$(".clearSelection").attr('checked', false);
	}
</script>
</body>