<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update User</title>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dob').datepicker({
			autoclose : true
		});
		$('#joiningDate').datepicker({
			autoclose : true
		});
		$("#reasonToStop").hide();
		$("#isEnabledDiv").hide();
	var onReferredMediaSelect = document.getElementById('referredMedia');
	var onReferredMedia = onReferredMediaSelect.options[onReferredMediaSelect.selectedIndex].getAttribute('numVal');
	if (onReferredMedia == 6) {
			$('#referredByDiv').show();
		} else {
			$('#referredByDiv').hide();
			$('#referredByDiv').val(" ");
		}
		showReasonDiv();
});

	function submitForm() {
		if (validate())
			$('#updateUser').submit();
	}

	function validate() {
		var mobileNumValid = $('#mobileNumber').val().trim();
		var IndNum = /^(\+91-|\+91|0)?\d{10}$/;

		if (!mobileNumValid.match(IndNum)) {
			showToast('warning', 'Please Enter Valid mobileNumber 10 Digit');
			return false;
		}
		
		var emailid = $('#email').val();
		
		var atpos = emailid.indexOf("@");
	    var dotpos = emailid.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailid.length) {
	    	showToast('warning',"Not a valid e-mail address");
	        return false;
	    }
	    
		return true;
	}

	function onReferredMediaChange() {
		var onReferredMediaSelect = document.getElementById('referredMedia');
		var onReferredMedia = onReferredMediaSelect.options[onReferredMediaSelect.selectedIndex]
				.getAttribute('numVal');
		if (onReferredMedia == 6) {
			$('#referredByDiv').show();
		} else {
			$('#referredByDiv').hide();
			$('#referredByDiv').val(" ");
		}
	}
	function showReasonDiv() {
		var onStatusSelect = document.getElementById('status');
		var status = onStatusSelect.options[onStatusSelect.selectedIndex]
				.getAttribute('numVal');
		if (status == 3) {
			$("#reasonToStop").show();
		} else {
			$("#reasonToStop").hide();
		}

	}
</script>
<style type="text/css">
#map_canvas {
	height: 400px;
	width: 400px;
	margin: 0.6em;
}
</style>
</head>

<c:url value="/admin/user/update" var="userUpdate" />
<c:url value="/admin/user/list" var="userList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<c:url value="/admin/user/update" var="updateUser" />
	<form:form id="updateUser" method="post" modelAttribute="user"
		action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Customer</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<fieldset>
							<form:errors path="firstName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">First Name</span>
								<form:input path="firstName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="lastName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Name</span>
								<form:input path="lastName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="mobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Mobile Number*</span>
								<form:input path="mobileNumber" readonly="true" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="alterNateMobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Alternate Mobile</span>
								<form:input path="alterNateMobileNumber" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57)"
									id="alterNateMobileNumber" cssClass="form-control" />
							</div>
							<br>
							<form:errors path="email" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Email*</span>
								<form:input path="email" cssClass="form-control" />
							</div>
							<br>
							<form:errors path="gender" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Gender*</span>
								<form:select path="gender" cssClass="form-control">
									<form:option value="true">Male</form:option>
									<form:option value="false">Female</form:option>
								</form:select>
							</div>
							<br>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Referred Media</span> <select
									name="referredMedia" id="referredMedia" cssClass="form-control"
									class="form-control" onchange="onReferredMediaChange()">
									<option value="Social_Media" numVal="1"
										<c:if test="${user.referredMedia == 'Social_Media'}">selected='selected'</c:if>>Social_Media</option>
									<option value="Website" numVal="2"
										<c:if test="${user.referredMedia == 'Website'}">selected='selected'</c:if>>Website</option>
									<option value="Newspaper" numVal="3"
										<c:if test="${user.referredMedia == 'Newspaper'}">selected='selected'</c:if>>Newspaper</option>
									<option value="Radio" numVal="4"
										<c:if test="${user.referredMedia == 'Radio'}">selected='selected'</c:if>>Radio</option>
									<option value="Event" numVal="5"
										<c:if test="${user.referredMedia == 'Event'}">selected='selected'</c:if>>Event</option>
									<option value="Friend" numVal="6"
										<c:if test="${user.referredMedia == 'Friend'}">selected='selected'</c:if>>Friend</option>
								</select>
							</div>
							<br>
							<div id="referredByDiv" class="input-group width-xlarge">
								<span class="input-group-addon">Referred By</span> <select
									name="referredBy" id="referredBy" class="form-control">
									<c:forEach items="${users}" var="customer">
										<c:if test="${user.id != customer.id}">
											<option value="${customer.id}"
												<c:if test="${user.referredBy.id == customer.id}">selected='selected'</c:if>>${customer.fullName}</option>
										</c:if>
									</c:forEach>
								</select>
								<br>
							</div>
							<div id="isEnabledDiv">
							<form:errors path="isEnabled" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<form:select path="isEnabled" cssClass="form-control">
									<form:option value="true">Yes</form:option>
									<form:option value="false">No</form:option>
								</form:select>
							</div>
							
							</div>
							<br>
							<form:errors path="dob" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Date of Birth*</span>
								<form:input path="dob" id="dob" cssClass="form-control" value ="${dob}" data-date-format="dd-mm-yyyy" />
							</div>
							<br>

							<form:errors path="joiningDate" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Joining Date*</span>
								<form:input path="joiningDate" cssClass="form-control"
									value="${today}" data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<form:errors path="address" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Address</span>
								<form:input path="address" cssClass="form-control" readonly="true"/>
							</div>
							<br>
							<form:errors path="buildingAddress" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Building Address*</span>
								<form:input path="buildingAddress" cssClass="form-control" readonly="true"/>
							</div>
							<br>
							<form:errors path="lastPendingDues" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Pending Dues*</span>
								<form:input path="lastPendingDues" cssClass="form-control" readonly="true"/>
							</div>
							<br>
							
							<form:errors path="isRouteAssigned" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<input path="isRouteAssigned" name="isRouteAssigned"id="isRoutteAssigned" type="hidden" readonly="true"	value="${user.isRouteAssigned}" cssClass="form-control">
							</div>
							<br>
							<div class="input-group width-xlarge">
								<input path="status" name="status"id="status" type="hidden" readonly="true"	value="${user.status}" cssClass="form-control">
							</div>
							<br>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();">Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>