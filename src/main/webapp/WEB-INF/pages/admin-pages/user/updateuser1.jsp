<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update User</title>
<script type="text/javascript">
$(document).ready(function() {
	$('#dob').datepicker({
		autoclose:true
	});
	$('#joiningDate').datepicker({
		autoclose:true
	});
});
function submitForm() {
	if(validate())
		$('#updateUser').submit();
}
function validate() {
	var mobileNumValid=$('#mobileNumber').val().trim();
	var IndNum = /^(\+91-|\+91|0)?\d{10}$/;
	
	var lastPendingDuesid=$('#lastPendingDues').val().trim();
	
	var emailid = $('#email').val();
	
	var atpos = emailid.indexOf("@");
    var dotpos = emailid.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailid.length) {
    	showToast('warning',"Not a valid e-mail address");
        return false;
    }
    
    if (lastPendingDuesid.length == 0) {
    	showToast('warning','Please Enter Valid last pending due');
		return false;
    }
    
	if (!mobileNumValid.match(IndNum)) {
		showToast('warning','Please Enter Valid mobileNumber 10 Digit');
		return false;
	}
		return true;	
	}
</script>
<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 400px;         
    margin: 0.6em;       
}

</style>
</head>

<c:url value="/admin/user/updateUser1" var="userUpdate" />
<c:url value="/admin/user/list" var="userList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/user/updateUser1" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="user" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">User</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<fieldset>
							<form:errors path="firstName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">First Name</span>
								<form:input path="firstName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="lastName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Name</span>
								<form:input path="lastName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="mobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Mobile Number</span>
								<form:input path="mobileNumber" readonly="true" cssClass="form-control" />
							</div>
							<br>
	
							<form:errors path="alterNateMobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Alternate Mobile</span>
								<form:input path="alterNateMobileNumber" id="alterNateMobileNumber" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="email" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Email</span>
								<form:input path="email" cssClass="form-control" />
							</div>
							<br>
							<form:errors path="buildingAddress" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Address</span>
								<form:input path="buildingAddress" cssClass="form-control" />
							</div>
							
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
						<form:errors path="dob" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Date of Birth</span>
								<form:input path="dob" id="dob" cssClass="form-control" value ="${dob}" data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<form:errors path="gender" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Gender</span>
								<form:select path="gender" cssClass="form-control">
									<form:option value="true">Male</form:option>
									<form:option value="false">Female</form:option>
								</form:select>
							</div>
							<br>
							<form:errors path="joiningDate" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Joining Date</span>
								<form:input path="joiningDate" cssClass="form-control" value ="${today}" data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Pending Dues</span>
								 <form:input path="lastPendingDues" class="form-control" />
							</div>
							<%-- <br>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Status</span>
								 <select name="status" id="status" cssClass="form-control"	class="form-control">
									<option value="NEW" <c:if test="${user.status == 'NEW'}">selected='selected'</c:if>>NEW</option>
									<option value="HOLD" <c:if test="${user.status == 'HOLD'}">selected='selected'</c:if>>HOLD</option>
									<option value="INACTIVE" <c:if test="${user.status == 'INACTIVE'}">selected='selected'</c:if>>INACTIVE</option>
								</select>
							</div> --%>
							<br>
							<sec:authorize access="!hasAnyAuthority('ROLE_ADMIN')">
								<form:errors path="userRoles" cssClass="text-danger" />
								<div class="input-group width-xlarge">
									<span class="input-group-addon">User Roles</span>
									<form:select path="userRoles" multiple="true" items="${roles}"
										itemLabel="role" itemValue="id" cssClass="form-control" />
								</div>
								<br>
							</sec:authorize>
							<form:errors path="isEnabled" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Enabled</span>
								<form:select path="isEnabled" cssClass="form-control">
									<form:option value="true">Yes</form:option>
									<form:option value="false">No</form:option>
								</form:select>
							</div>
							<br>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>
	
</body>