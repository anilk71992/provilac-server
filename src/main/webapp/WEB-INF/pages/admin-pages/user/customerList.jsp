<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Customer</title>
<style type="text/css">
.modal-body{
height:400px;
overflow-y:auto;
}

.dropdown-menu{
left: -82px;
}
</style>
<script type="text/javascript">
var customerId;
function noteAddClick(){
	var callDetails=$("#callDetails").val().trim();
	if($("#callDetails").val().trim()==""){
		showToast('warning','please enter call details to add Note');
		$("#callDetails").focus();
		return false;
	}
	var customerCode="U-";
	var postData="callDetails="+callDetails+"&"+"customerCode="+customerCode+customerId;
	 $.ajax({
		 url : '<c:url value="/admin/note/add"/>',
         type: "POST",
         data: postData,
         success : function(response) {
        	 
        	 $('#myModal').modal('hide');
        	 showNoteModal(customerId);
         }
     });
}

function showNoteModal(userId){
	 customerId=userId;
	 $("#noteTableId").html('');
	 $('#NoteModal').modal('show');
	 $("#customerCodeNoteModal").val("U-"+customerId);
	 $.ajax({
		 url : '<c:url value="/restapi/note/getNotes"/>',
		 data: "id="+userId,
		 dataType : 'json',
			success : function(response) {
				if (response.data.notes.length != 0) {
					if (response.success) {
						var data = response.data;
						notes = data.notes;
						var head="<thead><tr> <th style='width:25%'>Date   </th> <th>CallDetails</th> </tr> </thead>";
						 $("#noteTableId").html(head);
						for(var i=0;i<notes.length;i++){
							var note=notes[i];
							
							 row = $("<tr></tr>");
							   col1 = $("<td>"+note.date+"</td>");
							   col2 = $("<td>"+note.callDetails+"</td>");
							   row.append(col1,col2).prependTo("#noteTableId");
						}
					} 
				} 
			}
     });
}
</script>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Customer</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Customer</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/user/getCustomerlist" />
	<c:set var="urlPrefix" value="/admin/user/getCustomerlist?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/customer/show" var="userDetailLinkPrefix" />
	<%@ include file="customerNav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="users" class="footable" requestURI="" id="user" export="false">
			<display:column title="Code" sortable="false">
				<a href="${userDetailLinkPrefix}/${user.cipher}">${user.code}</a>
			</display:column>

			<display:column title="Name" sortable="false">${user.firstName} ${user.lastName}</display:column>
			<display:column title="Mobile Number" sortable="false">${user.mobileNumber}</display:column>
			<display:column title="Building Address" sortable="false">${user.buildingAddress}</display:column>
			<display:column title="RouteAssign" sortable="false">
				<c:choose>
					<c:when test="${user.isRouteAssigned}">Yes</c:when>
				</c:choose>
			</display:column>
			<display:column title="Status"  sortable="false">${user.status}</display:column>
			<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
				<display:column title="Actions" sortable="false">
					<div class="btn-group">
						<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#"> Action <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<sec:authorize access="!hasAnyAuthority('ROLE_ACCOUNTANT')">
								<!-- dropdown menu links -->
								<li><a href="<c:url value="/admin/user/update/${user.cipher}"/>">Edit</a></li>
								<sec:authorize url="/admin/user/delete/${user.cipher}">
									<li><a href="#ConfirmModal_${user.id}" data-toggle="modal">Delete</a></li>
								</sec:authorize>
								<li><a href="<c:url value="/admin/user/changeRoute/${user.code}"/>">Change Route</a></li>
								<li><a href="<c:url value="/admin/invoice/list?userCode=${user.code}"/>">Show Invoices</a></li>
								<li><a href="<c:url value="/admin/payment/list?userCode=${user.code}"/>">Show Payments</a></li>
							</sec:authorize>
							<li><a href="<c:url value="/admin/payment/accountLedger/${user.code}"/>">Show accountLedger</a></li>
							<li><a  data-toggle="modal"  href="javascript:showNoteModal('${user.id}');">Call Notes</a></li>
						</ul>
					</div>

					<!-- modal -->
					<div class="modal" id="ConfirmModal_${user.id}">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">�</button>
									<h3 id="myModalLabel">Delete User?</h3>
								</div>
								<div class="modal-body">
									<p>The User will be permanently deleted. This action cannot be undone.</p>
									<p>Are you sure?</p>
								</div>
								<div class="modal-footer">
									<a class="btn btn-primary" type="button" href="<c:url value="/admin/user/delete/${user.cipher}"/>">Delete</a>
									<button class="btn" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				
					<!--Note modal -->
					<div class="modal" id="NoteModal">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">�</button>
									<h2 id="myModalLabel">Customer Notes
										<div class="input-group width-large">
											<span class="input-group-addon">Code</span> 
											<input name="customerCodeNoteModal" id="customerCodeNoteModal" class="form-control" readonly="readonly"/>
										</div>
									</h2>
									
									<div class="container">
	  									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add New Note</button>
										<a class="btn btn-primary" href="<c:url value="/admin/note/list"/>">View All Notes</a>
	  									<!-- Modal -->
	 									<div class="modal fade" id="myModal" role="dialog">
	  						 				<div class="modal-dialog modal-large">
	     										<div class="modal-content">
	      						 					<div class="modal-header">
	        											<button type="button" class="close" data-dismiss="modal">&times;</button>
	         											<h4 class="modal-title">Add Note</h4>
	       											</div>
	        										<div class="modal-body">
	          											<textarea rows="8" id="callDetails" name="callDetails" cols="35" style="margin: 0px; width: 500px; height: 250px;" required="true"></textarea>
	        										</div>
	        										<div class="modal-footer">
	        											<a class="btn btn-primary" type="button"  href="javascript:noteAddClick();">Add</a>
	          											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	       								 			</div>
	     				 						</div>
	    									</div>
	  									</div>
									</div>
								</div>
								
								<div class="modal-body">
									<table id="noteTableId" border="1" width="100%" >
										<tbody></tbody>
	   								</table>
								</div>
								
								<div class="modal-footer">
									<button class="btn" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</display:column>
			</sec:authorize>
		</display:table>
	</form>
</body>
</html>