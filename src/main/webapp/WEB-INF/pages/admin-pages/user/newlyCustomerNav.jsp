<%@ include file="/taglibs.jsp"%>
<script type="text/javascript">
var availableTags=Array();
$(document).ready(function() {
	
	$("#searchTerm").select2();
	 
	$.ajax({
		url : '<c:url value="/restapi/user/getNewCustomers"/>',
		dataType : 'json',
		success : function(response) {
			if (response.data.customers.length != 0) {
				if (response.success) {
					for(var i=0;i<response.data.customers.length;i++){
						var customer =response.data.customers[i];
						
						  $('.searchTermClass').append($('<option>', {
						         value: customer.code,
						         text: customer.code +" - "+ customer.firstName + " - " + customer.lastName +" - "+customer.mobileNumber
						     }));
					}
				
				} else {
					alert(response.error.message);
				}
			} else {
			}
		}
	});
	
});

function showPageCustomer(){
	var customerCode=$("#searchTerm").val();
	var model ="modelDemo";
	window.location.href = "/provilac/admin/customer/show?customerCode=" + customerCode, "model=" + model;
	
 }

</script>
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form" id="searchForm">
	  <select type="text" class="form-control width-xlarge searchTermClass" style="width: 300px" "searchTerm" id="searchTerm" placeholder="search" onchange="showPageCustomer()"></select>
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>