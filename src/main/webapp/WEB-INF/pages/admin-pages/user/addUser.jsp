<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link rel="stylesheet" href="<c:url value="/assets/end-user/css/select2.min.css"/>" rel="stylesheet">

<title>Provilac - Add User</title>
<script type="text/javascript">
//var routes1 = new array();

$(document).ready(function() {
	$('#dob').datepicker({
		 autoclose: true
	});
	$('#joiningDate').datepicker({
		 autoclose: true
	});
});
	
	
	function submitForm() {
		if(validate())
			$('#addUser').submit();
	}
	
	function validate() {
		var mobileNumValid=$('#mobileNumber').val().trim();
		var IndNum = /^(\+91-|\+91|0)?\d{10}$/;
		
		if (!mobileNumValid.match(IndNum)) {
			showToast('warning','Please Enter Valid mobileNumber');
			return false;
		}
		
		if (!$('#addressId').val().trim()) {
			showToast('warning','Please enter address');
			return false;
		}
		
		if (!$('#userRoles').val()) {
			showToast('warning','Please Select Role for user');
			return false;
		}
		
		
			return true;	
		}
</script>

<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 400px;         
    margin: 0.6em;       
}

</style>

</head>
<c:url value="/user/addUser" var="userAdd" />
<c:url value="/user/list" var="userList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/user/add" var="addUser" />

	<form:form id="addUser" action="${addUserPic}" method="post" modelAttribute="user" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">User</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<fieldset>
							<form:errors path="firstName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">First Name</span>
								<form:input path="firstName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="lastName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Name</span>
								<form:input path="lastName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="mobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Mobile Number</span>
								<form:input path="mobileNumber" id="mobileNumber" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57)" 
											cssClass="form-control" />
							</div>
							<br>

							<form:errors path="alterNateMobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Alternate Mobile</span>
								<form:input path="alterNateMobileNumber" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57)"
											id="alterNateMobileNumber" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="email" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Email</span>
								<form:input path="email" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="password" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Password</span>
								<form:input path="password"  type="password" cssClass="form-control" />
							</div>
							<br>

													
							<form:errors path="buildingAddress" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Address</span>
								<form:input path="buildingAddress" cssClass="form-control" id="addressId"/>
							</div>
							<br>
							
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
						<form:errors path="dob" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Date of Birth</span>
								<form:input path="dob" id="dob" cssClass="form-control" value ="01-03-1991"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<form:errors path="gender" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Gender</span>
								<form:select path="gender" cssClass="form-control">
									<form:option value="true">Male</form:option>
									<form:option value="false">Female</form:option>
								</form:select>
							</div>
							<br>
							<sec:authorize access="!hasAnyAuthority('ROLE_ADMIN')">
								<form:errors path="userRoles" cssClass="text-danger" />
								<div class="input-group width-xlarge">
									<span class="input-group-addon">User Roles</span>
									<form:select path="userRoles" multiple="true" items="${roles}"
										itemLabel="role" itemValue="id" cssClass="form-control" />
								</div>
								<br>
							</sec:authorize>
							<sec:authorize access="hasAnyAuthority('ROLE_ADMIN')">
								<form:errors path="userRoles" cssClass="text-danger" />
								<div class="input-group width-xlarge">
									<span class="input-group-addon">User Roles</span>
									<form:select path="userRoles" multiple="true" items="${rolesForAdmin}"
										itemLabel="role" itemValue="id" cssClass="form-control" id="userRoles"/>
								</div>
								<br>
							</sec:authorize>
							<form:errors path="joiningDate" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Joining Date</span>
								<form:input path="joiningDate" cssClass="form-control" value ="${today}"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<form:errors path="isEnabled" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Enabled</span>
								<form:select path="isEnabled" cssClass="form-control">
									<form:option value="true">Yes</form:option>
									<form:option value="false">No</form:option>
								</form:select>
							</div>
							<br>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>