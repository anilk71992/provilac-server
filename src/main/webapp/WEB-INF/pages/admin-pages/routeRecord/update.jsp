<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update RouteRecord</title>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>

<c:url value="/admin/routeRecord/update" var="routeRecordUpdate" />
<c:url value="/admin/routeRecord/list" var="routeRecordList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/routeRecord/update" var="updateRouteRecord"/>
	<form:form id="updateRouteRecord" method="post" modelAttribute="routeRecord" action="${updateRouteRecord}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
			<h3 class="page-header">RouteRecord</h3>
			
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Customer</span> 
				<input type="text" name="customer" id="customer" value="${customer.username}" readonly="true" class="form-control">
			</div>
			<br>
			<form:errors path="route" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Route*</span>
				<form:select path="route" cssClass="form-control">
					<c:forEach items="${routes}" var="route">
						<option value="${route.id}"
							<c:if test="${route.id == routeRecord.route.id}">selected="selected"</c:if>>${route.name}</option>
					</c:forEach>
				</form:select>
			</div>
			<br>
			<form:errors path="priority" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Priority</span> 
				<form:input path="priority" cssClass="form-control"/>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Update</button>
           	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>