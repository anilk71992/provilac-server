<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - RouteRecord</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>RouteRecord</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>RouteRecord</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/routeRecord/list" />
	<c:set var="urlPrefix" value="/admin/routeRecord/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/routeRecord/show" var="routeRecordDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="routeRecords" class="footable" requestURI="" id="routeRecord" export="false">
			<display:column title="Code" sortable="true">
				<a href="${routeRecordDetailLinkPrefix}/${routeRecord.cipher}">${routeRecord.code}</a>
			</display:column>
			
			<display:column  title="Route" sortable="true">${routeRecord.route.name}</display:column>
			<display:column  title="Customer" sortable="true">${routeRecord.customer.firstName} ${routeRecord.customer.lastName} - ${routeRecord.customer.mobileNumber}</display:column>
			<display:column  title="Priority" sortable="true">${routeRecord.priority}</display:column>
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<%-- <li><a href="<c:url value="/admin/routeRecord/update/${routeRecord.cipher}/${routeRecord.customer.code}"/>">Edit</a>
						</li --%>
						<sec:authorize url="/admin/routeRecord/delete/${routeRecord.cipher}">
							<li><a href="#ConfirmModal_${routeRecord.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${routeRecord.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete RouteRecordRecord?</h3>
							</div>
							<div class="modal-body">
								<p>The RouteRecordRecord will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/routeRecord/delete/${routeRecord.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete routeRecords?</h3>
		</div>
		<div class="modal-body">
			<p>The selected routeRecord will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>