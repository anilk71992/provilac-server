<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - RouteRecord</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">RouteRecord- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<lable class="col-md-2">Id</lable>
				<p class="col-md-2">${routeRecord.id}</p>
				<label class="col-md-2">Customer</label>
				<p class="col-md-2">${routeRecord.customer.firstName} ${routeRecord.customer.lastName} - ${routeRecord.customer.mobileNumber}</p>
				<label class="col-md-2">Route</label>
				<p class="col-md-2">${routeRecord.route.name}</p>
				<label class="col-md-2">Priority</label>
				<p class="col-md-2">${routeRecord.priority}</p>
			</div>
			<div class="row">
			</div>
			<div class="row">
			</div>
		</div>

	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${routeRecord.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${routeRecord.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${routeRecord.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${routeRecord.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>