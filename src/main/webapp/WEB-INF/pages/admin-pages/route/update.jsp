<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Route</title>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>

<c:url value="/admin/route/update" var="routeUpdate" />
<c:url value="/admin/route/list" var="routeList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/route/update" var="updateRoute"/>
	<form:form id="updateRoute" method="post" modelAttribute="route" action="${updateRoute}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
			<h3 class="page-header">Route</h3>
			
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Name</span> 
				<form:input path="name" cssClass="form-control"/>
			</div>
			<br>
			
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Update</button>
           	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>