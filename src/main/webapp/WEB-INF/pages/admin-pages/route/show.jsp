<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Route</title>
<style type="text/css">
#map1 {         
    height: 400px;         
    width: 1100px;         
    margin: 0.6em;       
}
</style>
</head>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
<script type="text/javascript">
var latlng ;
var map;
var customers = new Array();

<c:forEach items="${route.routeRecords}" var="routeRecord">
var customerObj = new Object();
customerObj.lat = ${routeRecord.customer.lat};
customerObj.lng = ${routeRecord.customer.lng};
customerObj.firstName = '${routeRecord.customer.firstName}';
customerObj.lastName = '${routeRecord.customer.lastName}';
customerObj.priority = ${routeRecord.priority};
customers.push(customerObj);
</c:forEach>

$(document).ready(function() {
	
});

var directionsService = new google.maps.DirectionsService;

var directionsDisplay = new google.maps.DirectionsRenderer({
	suppressMarkers: true
});
function initMap() {
	
	
	map = new google.maps.Map(document.getElementById('map1'), {
	  zoom: 6,
	  center: {lat: 18.5203, lng: 73.8567}
	});
	directionsDisplay.setMap(map);
	drawRoute();
}

google.maps.event.addDomListener(window, "load", initMap);

function drawRoute() {
// 	var wayPoints = [];
	
// 	directionsDisplay.setMap(null);
	if(customers.length <= 0){
		return;
	}
	addMarkers();
// 	for(var i = 0; i < 7; i++) {
// 		var customer = customers[i];
// 		wayPoints.push({
// 			location: new google.maps.LatLng(customer.lat, customer.lng),
// 		    stopover: true
// 		});
// 	}
// 	    var start = wayPoints[0].location;
// 	    var end = wayPoints[wayPoints.length-1].location;
		
// 		var request= {
// 			 origin: start,
// 			    destination: end,
// 			    waypoints: wayPoints,
// 			    travelMode: google.maps.TravelMode.DRIVING
// 		};
// 		directionsService.route(request, function(response, status) {
// 		      if (status == google.maps.DirectionsStatus.OK) {
// 		        directionsDisplay.setDirections(response);
// 		        directionsDisplay.setMap(map);
// 		        addMarkers();
// 		      } else {
// 		        alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
// 		      }
// 		});
}

function addMarkers() {
	for(var i = 0; i < customers.length; i++) {
		var customer = customers[i];
		
		var infowindow = new google.maps.InfoWindow({
		    content: customer.firstName + ' ' + customer.lastName
		});
		
		var marker = new google.maps.Marker({
	        position: new google.maps.LatLng(customer.lat, customer.lng),
	        map: map,
	        title: customer.firstName + ' ' + customer.lastName
	    });
		
		//marker.addListener('click', function() {
			infowindow.open(map, marker);
	  //});
	}
}

</script>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Route- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Id</label>
				<p class="col-md-2">${route.id}</p>
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${route.name}</p>
			</div>
			<div class="row">
			<label class="col-md-2"></label>
				<p class="col-md-2"></p>
				<label class="col-md-2">Customers</label>
				<c:forEach items="${route.routeRecords}" var="record">
					<p class="col-md-6"></p>
					<c:out  value="${record.customer.firstName}  ${record.customer.lastName}"></c:out>
					<br>
				</c:forEach>
				
				
			</div>
			
		</div>

	</div>

	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Route</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
		<div id="map1">Map</div></div>
	</div>

	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${route.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${route.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${route.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${route.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>