<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/route/add" var="routeAdd" />
<c:url value="/admin/route/list" var="routeList" />
<c:url value="/admin/route/import" var="routeImport"/>
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<%-- <a class="btn btn-primary" href="${routeAdd}">Add New Route</a>&nbsp; --%>
	<a class="btn btn-primary" href="${routeImport}"> Import routes</a>&nbsp;
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>