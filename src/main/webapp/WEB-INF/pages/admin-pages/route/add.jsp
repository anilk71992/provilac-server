<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Route</title>
<%@ include file="../underscore-templates.tmpl"%>
<script type="text/javascript">
var routeRecordNumber = 1;
var customers;
$(document).ready(function() {

	$.ajax({
		url: '<c:url value="/routeRecord/getusers1"/>',
		dataType: 'json',
		
		success: function(response){
			if(null!=response && response.success){
				customers = response.data.customers
			}else {
				alert("Error while retrieving users");
			}
		}
	
	});

	
});
function addRouteRecord() {
	var template = _.template($('#routeRecordTemplate').html());
	var htmlText = template(customers,routeRecordNumber);
	$('#routeRecordDiv').append(htmlText);
	routeRecordNumber++;
}

function removeRouteRecord(routeRecord) {
	routeRecord.remove();
}


function submitClick(){
	if (validate()) {
		$('#addRoute').submit();
	}
}

function validate() {
if (!$('#priority').val()) {
	showToast('warning','Please enter route Records Customer and priority');
	return false;
}
	return true;	
}


</script>
<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 400px;         
    margin: 0.6em;       
}
</style>


</head>
<c:url value="/route/add" var="routeAdd" />
<c:url value="/route/list" var="routeList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/route/add" var="addsport" />

	<form:form id="addRoute" action="${addRoutePic}" method="post" modelAttribute="route" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="raw">
			<div class="col-md-6">
				<fieldset>
					<h3 class="page-header">Route</h3>
					 <form:errors path="name" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Name</span>
						<form:input path="name" cssClass="form-control" />
					</div>
					<br> 
					<%-- <form:errors path="" cssClass="text-danger" />
					<div class="input-group width-xlarge" id="select2City">
					<span class="input-group-addon">Route Name</span>
					<input id="searchTextField" name="name" type="text" size="50" class="form-control"/>             
					</div>
					<br>
					<div id="map_canvas" ></div>--%>
				</fieldset>
			</div>
			<div class="col-md-6">
				<fieldset>
					<h3 class="page-header">RouteRecord</h3>
					<form:errors path="routeRecords" cssClass="text-danger" />
					<div id="routeRecordDiv"></div>
					<a href="javascript:addRouteRecord()" class="btn btn-info">Add Route Record</a>
				</fieldset>
			</div>
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" href="javascript:submitClick();">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>

</body>