<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Invoice</title>
</head>
<c:url value="/admin/invoice/customer" var="generateInvoice" />
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Invoice- Details</h3>
		</div>
	</div>
	
	<div class="panel panel-success">
             <div class="panel-heading">
                 Send Invoice
             </div>
             <div class="panel-body">
			 <form role="form" action="${generateInvoice}" class="form-inline">
			    <div class="form-group">
			        <label for="exampleInputEmail2" class="sr-only">Select Month</label>
			        <input class="form-control" id="datepicker" type="text" name="date">
			        <input type="hidden" name="customerId" value="${invoice.customer.id}">
			    </div>
			    
			    <div class="checkbox m-r-xs">
			        <input type="checkbox" id="emailNotification" name="emailNotification">
			        <label for="emailNotification">
			            Email
			        </label>
			    </div>
			    
			    <div class="checkbox m-r-xs">
			        <input type="checkbox" id="smsNotification" name="smsNotification">
			        <label for="smsNotification">
			            SMS
			        </label>
			    </div>
			    <button class="btn btn-success" type="submit">Send &amp; Download</button>
			</form>               
		</div>
	</div>
	
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Customer</label>
				<p class="col-md-2">${invoice.customer.fullName} - ${invoice.customer.mobileNumber}</p>
				<label class="col-md-2">FromDate</label>
				<p class="col-md-2">${invoice.fromDate}</p>
				<label class="col-md-2">ToDate</label>
				<p class="col-md-2">${invoice.toDate}</p>
			</div>
			<div class="row">
				<label class="col-md-2">TotalAmount</label>
				<p class="col-md-2">${invoice.totalAmount}</p>
				<label class="col-md-2">Last Pending Dues</label>
				<p class="col-md-2">${invoice.lastPendingDues}</p>
				<label class="col-md-2">Provilac OutStanding</label>
				<p class="col-md-2">${invoice.provilacOutStanding}</p>
			</div>
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${invoice.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${invoice.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${invoice.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${invoice.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
<script type="text/javascript">
$("#datepicker").datepicker( {
    format: "MM-yyyy",
    startView: "months", 
    minViewMode: "months",
    endDate: '+0d',
    autoclose: true
});
</script>
</body>
</html>