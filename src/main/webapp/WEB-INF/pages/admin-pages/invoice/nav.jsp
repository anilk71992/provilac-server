<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/invoice/add" var="invoiceAdd" />
<c:url value="/admin/invoice/generate" var="invoiceGenerate" />
<c:url value="/admin/invoice/list" var="invoiceList" />
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
<%-- 	<a class="btn btn-primary" href="${invoiceAdd}">Add New Invoice</a>&nbsp; --%>
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
		<form class="form-inline pull-left" action="${invoiceGenerate}" action="get" role="form">
	  		<input class="form-control" id="datepicker" type="text" name="date">
	  		<label class="checkbox-inline">
	  			<input value="TRUE" type="checkbox" name="isNotify">Is Notify Customers 
			</label>
	  		<button class="btn btn-primary">Generate Invoice</button>
		</form>
	</sec:authorize>
	
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>
<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_ACCOUNTANT')">
	<div class="row  border-bottom white-bg dashboard-header">
		<p class="text-left" style="color: red;">Note:Strictly generate Invoice at the end of month!</p>
	</div>
</sec:authorize>
<script type="text/javascript">
$("#datepicker").datepicker( {
    format: "MM-yyyy",
    startView: "months", 
    minViewMode: "months",
    endDate: '+0d',
    autoclose: true
});
</script>