<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Invoice</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Invoice</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Invoice</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/invoice/list" />
	<c:set var="urlPrefix" value="/admin/invoice/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/invoice/show" var="invoiceDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="invoices" class="footable" requestURI="" id="invoice" export="false">
			<display:column title="Code" sortable="true">
				<%-- "${invoiceDetailLinkPrefix}/${invoice.cipher}"> --%>${invoice.code}
			</display:column>
			<display:column title="Customer" sortable="true">${invoice.customer.fullName}</display:column>
			<display:column title="User Name" sortable="true">${invoice.customer.username}</display:column>
			<display:column title="From Date" sortable="true">${invoice.fromDate}</display:column>
			<display:column title="To Date" sortable="true">${invoice.toDate}</display:column>
			<display:column title="Total Amount" sortable="true">${invoice.totalAmount}</display:column>
			
			<%-- <display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/invoice/update/${invoice.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/invoice/delete/${invoice.cipher}">
							<li><a href="#ConfirmModal_${invoice.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${invoice.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete Invoice?</h3>
							</div>
							<div class="modal-body">
								<p>The Invoice will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
<!-- 								<a class="btn btn-primary" type="button" -->
									href="<c:url value="/admin/invoice/delete/${invoice.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column> --%>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete invoices?</h3>
		</div>
		<div class="modal-body">
			<p>The selected invoice will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>