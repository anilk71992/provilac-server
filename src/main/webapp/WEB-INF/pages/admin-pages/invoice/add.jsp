<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Invoice</title>
<script type="text/javascript">
$(document).ready(function() {
	$('#fromDate').datepicker();
	$('#toDate').datepicker();
});
</script>
</head>
<c:url value="/invoice/add" var="invoiceAdd" />
<c:url value="/invoice/list" var="invoiceList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/invoice/add" var="addInvoice" />

	<form:form id="addInvoice" action="${addInvoicePic}" method="post" modelAttribute="invoice" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">Invoice</h3>
			
			<form:errors path="customer" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Customer</span>
				<form:select path="customer" items="${users}" itemLabel="username"
					itemValue="id" cssClass="form-control"></form:select>
			</div>
			<br>
			
			<form:errors path="fromDate" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">From Date</span>
				<input name="fromDate" id="fromDate" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${invoice.fromDate}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="toDate" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">To Date</span>
				<input name="toDate" id="toDate" class="form-control" data-date-format="dd-mm-yyyy" value="<fmt:formatDate value="${invoice.toDate}" pattern="dd-MM-yyyy" />"/>
			</div>
			<br>
			<form:errors path="totalAmount" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Total Amount</span>
				<form:input path="totalAmount" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="lastPendingDues" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Last Pending Dues</span>
				<form:input path="lastPendingDues" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="provilacOutStanding" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Provilac OutStanding</span>
				<form:input path="provilacOutStanding" cssClass="form-control" />
			</div>
			<br>
					<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Add</button>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>