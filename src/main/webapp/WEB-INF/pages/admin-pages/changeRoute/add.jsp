<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link rel="stylesheet" href="<c:url value="/assets/end-user/css/select2.min.css"/>" rel="stylesheet">

<title>Provilac - ChangeRoute</title>
<script type="text/javascript">
//var routes1 = new array();
var routeRecordMap = new Object();
var markers = new Array();
var infoWindows = new Array();
var customers= new Array();

$(document).ready(function() {
	$('#dob').datepicker();
	$('#joiningDate').datepicker();
	$('#referredByDiv').hide();
	$('#referredByDiv').val(" ");
});

	function onReferredMediaChange() {
		var onReferredMediaSelect = document.getElementById('referredMedia');
		var onReferredMedia = onReferredMediaSelect.options[onReferredMediaSelect.selectedIndex]
				.getAttribute('numVal');
		if (onReferredMedia == 6) {
			$('#referredByDiv').show();
		} else {
			$('#referredByDiv').hide();
			$('#referredByDiv').val(" ");
		}
	}

	function onSuggestRouteClick() {
		var lat = document.getElementById('latId').value;
		var lng = document.getElementById('lngId').value;
		$
				.ajax({
					url : '<c:url value="/routeRecord/routes"/>',
					dataType : 'json',
					data : 'lat=' + lat + '&lng=' + lng,
					success : function(response) {
						if (response.data.routes.length != 0) {
							if (response.success) {
								var data = response.data;
								routes1 = data.routes;
								$('#route').empty();

								for (var i = 0; i < data.routes.length; i++) {
									var route = data.routes[i];
									var html = '<input type="radio" onchange="onRouteSelect(\''
											+ route.code.trim()
											+ '\')" name="route" id="route_'
											+ i
											+ '" value="'
											+ route.code
											+ '" />'
											+ '<label>&nbsp;'
											+ route.name + '</label> <br>';
									$('#route').append(html);

									routeRecordMap[route.code] = route.routeRecordDTOs;
								}

							} else {
								alert(response.error.message);
							}
						} else {
							//alert("Nearest Routes are not found.");
							$('#ConfirmModal').show();
						}
					}
				});

	}

	function onRouteSelect(code) {

		$('#customer').empty();
		removeAllMarkers();
		var routeRecords = routeRecordMap[code];
		for (var i = 0; i < routeRecords.length; i++) {
			var customer = routeRecords[i].customerDTO;
			customers[i] = customer;
			var html = '<input type="radio" name="customer" onchange="onCustomerChange('
					+ i
					+ ')" value="'
					+ customer.code
					+ '" />'
					+ '<label>&nbsp;'
					+ customer.firstName
					+ " "
					+ customer.lastName +
					+ " - "
					+ customer.address + '</label> <br>';
			$('#customer').append(html);

			//addMarkers(customer);
		}
		drawRoute();
	}

	function drawRoute() {
		var wayPoints = new Array();

		for (var i = 0; i < customers.length; i++) {
			var customer = customers[i];
			wayPoints.push({
				location : new google.maps.LatLng(customer.lat, customer.lng),
				stopover : true
			});
		}
		var start = wayPoints[0].location;
		var end = wayPoints[wayPoints.length - 1].location;

		var request = {
			origin : start,
			destination : end,
			waypoints : wayPoints,
			travelMode : google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				directionsDisplay.setMap(map);
				addMarkers();
			} else {
				alert("Directions Request from " + start.toUrlValue(6) + " to "
						+ end.toUrlValue(6) + " failed: " + status);
			}
		});
	}

	function addMarkers() {
		for (var i = 0; i < customers.length; i++) {
			var customer = customers[i];

			var infowindow = new google.maps.InfoWindow({
				content : customer.firstName + ' ' + customer.lastName
			});

			var marker = new google.maps.Marker({
				position : new google.maps.LatLng(customer.lat, customer.lng),
				map : map,
				title : customer.firstName + ' ' + customer.lastName
			});

			infowindow.open(map, marker);

			markers.push(marker);
			infoWindows.push(infowindow);
		}
		customers = new Array();
	}
	function onCustomerChange(index) {
		infoWindows[index].open(map, markers[index]);
	}

	/* function addMarkers(customer) {
		var latlng = new google.maps.LatLng(customer.lat, customer.lng);

		const
		infowindow = new google.maps.InfoWindow({
			content : customer.firstName + ' ' + customer.lastName
		});

		var marker = new google.maps.Marker({
			position : latlng,
			map : map,
			title : customer.firstName + ' ' + customer.lastName
		});

		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});

		markers.push(marker);
		infoWindows.push(infowindow);
	} */

	function removeAllMarkers() {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
		markers = new Array();
		infoWindows = new Array();
	}

	function addRoute() {
		$('#ConfirmModal').hide();
		$('#addRouteModal').show();
	}

	function getRouteValue() {
		var routeName = document.getElementById('route1').value;
		$('#modalRoute').val(routeName);
	}
</script>
<style type="text/css">
#map_canvas {         
    height: 400px;         
    width: 400px;         
    margin: 0.6em;       
}

</style>

</head>
<c:url value="/user/add" var="userAdd" />
<c:url value="/user/list" var="userList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/user/add" var="addUser" />

	<form:form id="addUser" action="${addUserPic}" method="post" modelAttribute="user" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">User</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="raw">
					<div class="col-md-6">
						<fieldset>
							<form:errors path="firstName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">First Name</span>
								<form:input path="firstName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="lastName" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Last Name</span>
								<form:input path="lastName" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="mobileNumber" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">mobileNumber</span>
								<form:input path="mobileNumber" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="email" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Email</span>
								<form:input path="email" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="password" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Password</span>
								<form:input path="password" cssClass="form-control" />
							</div>
							<br>

							<form:errors path="" cssClass="text-danger" />
							<div class="input-group width-xlarge" id="select2City">
								<span class="input-group-addon">Address</span> <input
									id="searchTextField" name="address" type="text" size="50"
									class="form-control" />
							</div>
							<br>
							<div class="input-group width-xlarge" id="latDiv">
								<span class="input-group-addon">Lattitude</span> <input
									name="lat" id="latId" readonly="readonly" value="18.5203"
									class="form-control" />
							</div>
							<br>

							<div class="input-group width-xlarge" id="lngDiv">
								<span class="input-group-addon">Longitude</span> <input
									name="lng" id="lngId" readonly="readonly" value="73.8567"
									class="form-control" />
							</div>
							<br>
							<form:errors path="dob" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">DoB</span>
								<form:input path="dob" cssClass="form-control"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>

							<form:errors path="gender" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Gender</span>
								<form:select path="gender" cssClass="form-control">
									<form:option value="true">Male</form:option>
									<form:option value="false">Female</form:option>
								</form:select>
							</div>
							<br>

							<form:errors path="userRoles" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">User Roles</span>
								<form:select path="userRoles" multiple="true" items="${roles}"
									itemLabel="role" itemValue="id" cssClass="form-control" />
							</div>
							<br>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							<form:errors path="joiningDate" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">JoiningDate</span>
								<form:input path="joiningDate" cssClass="form-control"
									data-date-format="dd-mm-yyyy" />
							</div>
							<br>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Status</span> <select
									name="status" id="status" cssClass="form-control"
									class="form-control">
									<option value="NEW">NEW</option>
									<option value="HOLD">HOLD</option>
									<option value="ACTIVE">ACTIVE</option>
									<option value="INACTIVE">INACTIVE</option>
								</select>
							</div>
							<br>
							<div class="input-group width-xlarge">
								<span class="input-group-addon">ReferredMedia</span> <select
									name="referredMedia" id="referredMedia" cssClass="form-control"
									class="form-control" onchange="onReferredMediaChange()">
									<option value="Social_Media" numVal="1">Social_Media</option>
									<option value="Website" numVal="2">Website</option>
									<option value="Newspaper" numVal="3">Newspaper</option>
									<option value="Radio" numVal="4">Radio</option>
									<option value="Event" numVal="5">Event</option>
									<option value="Friend" numVal="6">Friend</option>
								</select>
							</div>
							<br>
							<div id="referredByDiv">
								<form:errors path="referredBy" cssClass="text-danger" />
								<div class="input-group width-xlarge">
									<span class="input-group-addon">Referred By</span>
									<form:select path="referredBy" items="${users}"
										itemLabel="username" itemValue="id" cssClass="form-control" />
								</div>
							</div>
							<br>
							<form:errors path="isEnabled" cssClass="text-danger" />
							<div class="input-group width-xlarge">
								<span class="input-group-addon">Enabled</span>
								<form:select path="isEnabled" cssClass="form-control">
									<form:option value="true">Yes</form:option>
									<form:option value="false">No</form:option>
								</form:select>
							</div>
							<br>
							<div class="input-group width-xlarge">
								<input type="hidden" name="routeName" id="modalRoute"
									class="form-control" />
							</div>
							<br>
							<div id="map_canvas"></div>

							<a class="btn btn-info" href="javascript:onSuggestRouteClick();">Suggest
								Route</a> <br />
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Routes</h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group width-xxlarge" id="route">
						</div>
					</div>

					<div id="customer" class="col-md-6"></div>

				</div>
			</div>
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Add</button>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
	<div class="modal" id="ConfirmModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 id="myModalLabel">Add Route</h3>
				</div>
				<div class="modal-body">
					<p>Nearest Records are not found!!</p>
				</div>
				<div>
				<a class="btn btn-info" href="javascript:addRoute();" style="margin-left: 20px;">Add Route</a> <br />
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" onclick="hideModel('ConfirmModal')">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="addRouteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 id="myModalLabel">Add Route</h3>
				</div>
				<div>
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Route</span> <input name="route1"
							id="route1" class="form-control" />
					</div>
					<br>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal"
							onclick="getRouteValue();hideModel('addRouteModal');">OK</button>
						<button class="btn" data-dismiss="modal"
							onclick="hideModel('addRouteModal')">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<script src="http://maps.google.com/maps/api/js?libraries=places"></script>
		<script type="text/javascript">
	
	var lat = 18.5203, 
		lng = 73.8567, latlng = new google.maps.LatLng(lat, lng),
		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

	var directionsService = new google.maps.DirectionsService;

	var directionsDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers: true
	});
	var map = new google.maps.Map(document.getElementById('map_canvas'), {
		center : new google.maps.LatLng(lat, lng),
		zoom : 15,
	});
	
	var marker = new google.maps.Marker({
		position : latlng,
		map : map,
		icon : image,
		draggable : true
	});
	directionsDisplay.setMap(map);
	var searchBox = new google.maps.places.SearchBox(document.getElementById('searchTextField'));

	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();
		var bounds = new google.maps.LatLngBounds();
		var place;
		for (var i = 0; place = places[i]; i++) {
			bounds.extend(place.geometry.location);
			marker.setPosition(place.geometry.location);
		}
		map.fitBounds(bounds);
		map.setZoom(17);
	});

	google.maps.event.addListener(marker, 'position_changed', function() {
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		$('#latId').val(lat);
		$('#lngId').val(lng);
	});
</script>
</body>