<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - SubscriptionLineItem</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>SubscriptionLineItem</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>SubscriptionLineItem</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/subscriptionLineItem/list" />
	<c:set var="urlPrefix" value="/admin/subscriptionLineItem/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/subscriptionLineItem/show" var="subscriptionLineItemDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="subscriptionLineItems" class="footable" requestURI="" id="subscriptionLineItem" export="false">
			<display:column title="Code" sortable="true">
				<a href="${subscriptionLineItemDetailLinkPrefix}/${subscriptionLineItem.cipher}">${subscriptionLineItem.code}</a>
			</display:column>
			<display:column title="UserSubscription" sortable="true">${subscriptionLineItem.userSubscription.code}</display:column>
			<display:column title="Product" sortable="true">${subscriptionLineItem.product.name}</display:column>
			<display:column title="Day" sortable="true">${subscriptionLineItem.day}</display:column>
			<display:column title="Quantity" sortable="true">${subscriptionLineItem.quantity}</display:column>
			
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/subscriptionLineItem/update/${subscriptionLineItem.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/subscriptionLineItem/delete/${subscriptionLineItem.cipher}">
							<li><a href="#ConfirmModal_${subscriptionLineItem.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${subscriptionLineItem.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete SubscriptionLineItem?</h3>
							</div>
							<div class="modal-body">
								<p>The SubscriptionLineItem will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/subscriptionLineItem/delete/${subscriptionLineItem.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete subscriptionLineItems?</h3>
		</div>
		<div class="modal-body">
			<p>The selected subscriptionLineItem will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>