<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Note</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Note</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>All Note</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/note/list" />
	<c:set var="urlPrefix" value="/admin/note/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/note/show" var="noteDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="notes" class="footable" requestURI="" id="note" export="false">
			<display:column title="Code" sortable="true">
				<a href="${noteDetailLinkPrefix}/${note.cipher}">${note.code}</a>
			</display:column>
			<display:column title="Name" sortable="true">${note.customer.firstName} ${note.customer.lastName} ${note.customer.mobileNumber}</display:column>
			<display:column  title="Date" sortable="true">${note.date}</display:column>
			<display:column  title="CallDetails" sortable="true">${note.callDetails}</display:column>
		<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<sec:authorize url="/admin/note/delete/${note.cipher}">
							<li><a href="#ConfirmModal_${note.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${note.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete note?</h3>
							</div>
							<div class="modal-body">
								<p>The note will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/note/delete/${note.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
			
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete notes?</h3>
		</div>
		<div class="modal-body">
			<p>The selected note will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>