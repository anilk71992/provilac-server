<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - One Time Order</title>
<script type="text/javascript">
$(document).ready(function() {
//	getDeliveryBoys();
	//getLocalities();
//	$('#locality').select2();
	$('#deliveryboy').select2();
	  $("#deliveryboy").select2("val", "");
	  $("#deliveryboy").select2({
		   placeholder: "Select Service Boy"
		});
	//  myFunction();
	$("#tableId").hide();
});

/* 

<c:forEach items="${todayOneTimeOrders}" var="onetimeorder">
var onetimeorderObj = new Object();
onetimeorderObj.code = '${onetimeorder.code}';
onetimeorderObj.priority = '${onetimeorder.priority}';
onetimeorderObj.status = '${onetimeorder.status}';
todayOneTimeOrdersArray.push(onetimeorderObj);
</c:forEach>

 */
 var todayOneTimeOrdersArray=new Array();
 var orderLength;
function getOrdersBydeliveryBoy(){
	todayOneTimeOrdersArray=new Array();
	var deliveryBoyId=$('#deliveryboy').val();
	$("#tableId td").remove();
	$.ajax({
	     url:'<c:url value="/restapi/onetimeorder/byDeliveryBoy"/>',
	     datatype:'json',
	     data : 'deliveryBoyId='+deliveryBoyId,
	     success: function(response) {
	         if(response.success){
	            var ordersByDeliveryBoy = response.data.ordersByDeliveryBoy;
	            orderLength=ordersByDeliveryBoy.length;
	            if(ordersByDeliveryBoy==""){
	            	 $("#otoDiv").html("");
	            }
	            if(ordersByDeliveryBoy.length !=0){
	            for(var i = 0; i < ordersByDeliveryBoy.length; i++) {
	                var order  = ordersByDeliveryBoy[i];
	                var onetimeorderObj = new Object();
	                onetimeorderObj.code = order.code;
	                onetimeorderObj.priority = order.priority;
	                if(order.status==undefined){
	                	onetimeorderObj.status="";
	                }else{
	                	onetimeorderObj.status = order.status;
	                }
	                onetimeorderObj.lat = order.lat;
	                onetimeorderObj.lng = order.lng;
	                onetimeorderObj.customerCode = order.customerCode;
	                onetimeorderObj.addressCode = order.addressCode;
	                onetimeorderObj.instructions = order.instructions;
	                onetimeorderObj.paymentOrderId = order.paymentOrderId;
	                onetimeorderObj.finalAmount = order.finalAmount;
	                todayOneTimeOrdersArray.push(onetimeorderObj);
	                
	            }
	            $("#tableId").show();
	            todayOneTimeOrdersArray.sort();
	            initMap();
	            $("#otoDiv").html("");
	            addOTO();
	            addTodayOTO();
	        //    $('.createbutton').unbind('click', false);
	          }
	         }else {
	                showToast("warning","No Orders  found !!");
	         }
	         
	 }
	 });
	} 
	
/* function getDeliveryBoys(){
	$.ajax({
	     url:'<c:url value="/restapi/onetimeorder/deliveryboy"/>',
	     datatype:'json',
	     success: function(response) {
	         if(response.success){
	            var deliveryboys = response.data.deliveryboys;
	             
	            $('.deliveryBoyClass').html("");
	            if(deliveryboys.length !=0){
	            for(var i = 0; i < deliveryboys.length; i++) {
	                var deliveryboy  = deliveryboys[i];
	                $('.deliveryBoyClass').append($('<option>', {
	                    value: deliveryboy.id,
	                    text: deliveryboy.firstName +"-"+ deliveryboy.mobileNumber
	                }));
	            }
	          }
	           $("#deliveryBoy").select2("val", "");
	           $("#otoDiv").html("");
	         }else {
	                showToast("warning","No delivery Boy  found !!");
	         }
	         
	 }
	 });
	} */

function submitForm() {
	if(validate()){
		$('#createdeliverysheet').submit();
	}
	
}

function validate(){
	
	if(orderLength==undefined||orderLength==0){
		
		showToast('warning','Select Service Boy To Gegerate Delivery Sheet');
		showToast('warning','Orders not Found!!!');
		return false;
	}
	
	 var priorityArray=new Array();
	 
	   var priorityCount=$('.priorityClass').length;
		for(var i=0;i<priorityCount;i++){
			var reg=/^\d$/;
			var priority=$($(".priorityClass")[i]).val();
				if(!reg.test(priority)){
					showToast('warning','This Priority is Not Valid:-'+priority);
					$($(".priorityClass")[i]).focus();
					return false;
				}
				if(priority>orderLength){
					showToast('warning','Please Enter Priority Less Than Orders-'+priority);
					$($(".priorityClass")[i]).focus();
					return false;
				}
				
				if ($.inArray(priority, priorityArray) > -1) {
					if(priority!=0){
					showToast('warning', ''+priority+' Priority is Alredy Enter');
					$($(".priorityClass")[i]).focus();
					return false;
					}
				}
				priorityArray.push(priority);
			}
			
		return true;
	} 

//Todays Order With Delivery Boy assign with priority
var id;
function addOTO(){

	$.each(todayOneTimeOrdersArray,function(i, lineItem) {
	 	id=Math.floor((Math.random()*1000)+12);
	var html='<div class="panel panel-info onletimeorder" id="'+id+'">';
	html  +='<div class="panel-body><div class="row" style="margin-left:20px; margin-right:40px"><div class="row">';
	/*
	html+= '<div class="row" style=" margin-right: 10px "><div class="col-md-4"><div class="input-group">';
	html += '<input name="orderCode[]" value="'+todayOneTimeOrdersArray[i].code+'" readonly="true" class="form-control otoCodeClass" id="otoCode_'+id+'"></input></div></div>';
	
	 html+= '<div class="col-md-4"><div class="statusDiv"><div class="input-group">';
	html += '<input name="otoStatus[]" value="'+todayOneTimeOrdersArray[i].status+'" readonly="true" id="otoStatus_'+id+'" class="form-control pincodeClass "></input></div></div></div>';
	
	html+= '<div class="col-md-3"><div class="priorityDiv"><div class="input-group"></span>';
	html += '<input name="prioritySet[]" id="priority_'+id+'" value="'+todayOneTimeOrdersArray[i].priority+'" placeholder="priority" class="form-control priorityClass "></input></div></div></div>';
	 */
	html += '</div></div></div>'; 	
	if(todayOneTimeOrdersArray[i].priority!=0){
		
		//table.innerHTML(html);
	//$("#otoDiv").append(html);
		var table = document.getElementById("tableId");
		var rowCount=table.rows.length;
	    var row = table.insertRow(rowCount);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    cell1.innerHTML = '<input name="orderCode[]" value="'+todayOneTimeOrdersArray[i].code+'" readonly="true" class="form-control otoCodeClass" id="otoCode_'+id+'"></input></div></div>';
	    cell2.innerHTML = '<input name="prioritySet[]" id="priority_'+id+'" value="'+todayOneTimeOrdersArray[i].priority+'" placeholder="priority" class="form-control priorityClass "></input></div></div></div>';
	    
		
	} 
		 
	 
	
	});
}

//Todays Order not assign with priority 0
var id;
function addTodayOTO(){
	
	$.each(todayOneTimeOrdersArray,function(i, lineItem) {
		id=Math.floor((Math.random()*1000)+12);
	var html='<div class="panel panel-info onletimeorder" id="'+id+'">';
	/* html  +='<div class="panel-body><div class="row" style="margin-left:20px; margin-right:40px"><div class="row">';
	
	html+= '<div class="row" style=" margin-right: 10px "><div class="col-md-4"><div class="input-group">';
	html += '<input name="orderCode[]" value="'+todayOneTimeOrdersArray[i].code+'" readonly="true" class="form-control otoCodeClass" id="otoCode_'+id+'"></input></div></div>';
	
	html+= '<div class="col-md-4"><div class="statusDiv"><div class="input-group">';
	html += '<input name="otoStatus[]" value="'+todayOneTimeOrdersArray[i].status+'" readonly="true" id="otoStatus_'+id+'" class="form-control pincodeClass "></input></div></div></div>';
	
	html+= '<div class="col-md-4"><div class="priorityDiv"><div class="input-group"></span>';
	html += '<input name="prioritySet[]" id="priority_'+id+'" value="'+todayOneTimeOrdersArray[i].priority+'" placeholder="priority" class="form-control priorityClass "></input></div></div></div>';
	 */
	html += '</div></div></div>'; 	
	if(todayOneTimeOrdersArray[i].priority==0){
		//table.innerHTML=html;
		//$("#otoDiv").append(html);
		 var table = document.getElementById("tableId");
		 var rowCount=table.rows.length;
		    var row = table.insertRow(rowCount);
		    var cell1 = row.insertCell(0);
		    var cell2 = row.insertCell(1);
		    cell1.innerHTML = '<input name="orderCode[]" value="'+todayOneTimeOrdersArray[i].code+'" readonly="true" class="form-control input-sm otoCodeClass" id="otoCode_'+id+'"></input></div></div>';
		    cell2.innerHTML = '<input name="prioritySet[]" id="priority_'+id+'" value="'+todayOneTimeOrdersArray[i].priority+'" placeholder="priority" class="form-control priorityClass "></input></div></div></div>';
		    
			
		}
		
	});
}
</script>
<style type="text/css">
#map_canvas {         
    height: 600px;         
    width: 650px;         
    margin: 0.6em;       
}

</style>

</head>
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Today</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Create Delivery Sheet</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url value="/admin/onetimeorder/exportorder" var="createdeliverysheet" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<form:form id="createdeliverysheet" action="${createdeliverysheet}" method="post" modelAttribute="onetimeorder" enctype="multipart/form-data">
		
		<div class="row">
		<div class="col-md-3" >
		<div class="input-group width-xlarge" >
			<form:select path="deliveryBoy" class="form-control" id="deliveryboy" onchange="getOrdersBydeliveryBoy();" placeholder="Service Boy">
				<c:forEach items="${deliveryboys}" var="deliveryboy">
					<form:option value="${deliveryboy.id}"> ${deliveryboy} </form:option>
				</c:forEach>
			</form:select>
		</div>
		</div>
		<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">Priority With 0 will not assign to Delivery Boy</span>
		</div>
		
		</div>
			
		<br>
		<div class="row">
		
		 <div class="col-md-4" >
		<table id="tableId" class="tableClass" style="margin-left: 1cm;">
					<col width="80"></col>
  					<col width="150"></col>
						<tr>
							<th>  Code </th>
							<th>  Priority  </th>
							
						</tr>
				</table>
		</div> 
		 <div class="col-md-8"  id="map_canvas"  ></div> 
		</div>
		
		<a class="btn btn-info createbutton" href="javascript:submitForm();">Create & Export</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
	 <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM"></script>
<script type="text/javascript">
var latlng ;
var map;
var markers = [];
var infoWindows = [];
/*  var onetimeorders = new Array();

<c:forEach items="${todayOneTimeOrders}" var="onetimeorder">
var onetimeorderObj = new Object();
onetimeorderObj.lat = ${onetimeorder.address.lat};
onetimeorderObj.lng = ${onetimeorder.address.lng};
onetimeorderObj.code = '${onetimeorder.code}';
onetimeorderObj.priority = '${onetimeorder.priority}';
onetimeorderObj.name = '${onetimeorder.customer}';
onetimeorders.push(onetimeorderObj);
</c:forEach>  */

var directionsService = new google.maps.DirectionsService;

var directionsDisplay = new google.maps.DirectionsRenderer({
	suppressMarkers: true
});
 function initMap() {
	
	map = new google.maps.Map(document.getElementById('map_canvas'), {
	  zoom: 10,
	  center: {lat: 18.5203, lng: 73.8567}
	});
	directionsDisplay.setMap(map);
	drawRoute();
} 

google.maps.event.addDomListener(window, "load", initMap);

function drawRoute() {
	
	if(todayOneTimeOrdersArray.length <= 0){
		return;
	}
	addMarkers();
}

function addMarkers() {
	markers = [];
	infoWindows = [];
	
	for(var i = 0; i < todayOneTimeOrdersArray.length; i++) {
		var onetimeorder = todayOneTimeOrdersArray[i];
		
		var infowindow = new google.maps.InfoWindow({
		    content: onetimeorder.code
		});
		
		var marker = new google.maps.Marker({
	        position: new google.maps.LatLng(onetimeorder.lat, onetimeorder.lng),
	        map: map,
	        title: onetimeorder.code + ' ' + "Click for More Info.",
	        description:'<b>Order Code : '+ onetimeorder.code + ' </b> <br> Customer Code : '+ onetimeorder.customerCode + '<br> Address Code :' +  onetimeorder.addressCode + ' <br> Final Amount :' +onetimeorder.finalAmount
	    });
		
		/* marker.addListener('click', function(i) {
			openInfoWindow(i);
	  	}); */
	  	infoWindows[marker]=infowindow;
		var data=infoWindows[i];
		markers[i] = marker;
		//infoWindows[marker.position] = infowindow;
		
		(function(marker,data){
			google.maps.event.addListener(marker,'click',function(e){
				infowindow.setContent(marker.description);
				infowindow.open(map,marker);
			});
		})(marker,data);
		
		
		
	}
}
</script> 
</body>