<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - One Time Order</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>One Time Order</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Today Orders</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/onetimeorder/todaylist" />
	<c:set var="urlPrefix" value="/admin/onetimeorder/todaylist?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/onetimeorder/show" var="onetimeorderDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="todayonetimeorders" class="footable" requestURI="" id="onetimeorder" export="false">
			<display:column title="Code" sortable="true">
				<a href="${onetimeorderDetailLinkPrefix}/${onetimeorder.cipher}">${onetimeorder.code}</a>
			</display:column>
			
			<display:column  title="Customer" sortable="true">${onetimeorder.customer.code} - ${onetimeorder.customer.firstName} - ${onetimeorder.customer.mobileNumber}</display:column>
			<display:column  title="Final Amount" sortable="true">${onetimeorder.finalAmount}</display:column>
			<display:column  title="Status" sortable="true">${onetimeorder.status}</display:column>
			<display:column  title="Delivery Boy" sortable="true">${onetimeorder.deliveryBoy}</display:column>
			<display:column  title="Date of Delivery" sortable="true"><fmt:formatDate value="${onetimeorder.date}" pattern="dd-MM-yyyy" /></display:column>
			<display:column title="Address">${onetimeorder.address.fullAddress}</display:column>
			
			<%-- <display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/locality/update/${locality.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/locality/delete/${locality.cipher}">
							<li><a href="#ConfirmModal_${locality.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				
			</display:column> --%>
		</display:table>
	</form>

</body>
</html>