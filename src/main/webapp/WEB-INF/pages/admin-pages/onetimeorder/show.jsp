<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - One Time Order</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">One Time Order- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Code</label>
				<p class="col-md-2">${oneTimeOrder.code}</p>
				<label class="col-md-2">Customer Name</label>
				<p class="col-md-2">${oneTimeOrder.customer.firstName}  ${oneTimeOrder.customer.lastName} - ${oneTimeOrder.customer.mobileNumber} </p>
				<label class="col-md-2">Date</label>
				<p class="col-md-2">
				<fmt:formatDate value="${oneTimeOrder.date}" pattern="dd-MM-yyy hh:mm:ss a" /></p>
				<label class="col-md-2">Total Amount</label>
				<p class="col-md-2">${oneTimeOrder.totalAmount}</p>
				<label class="col-md-2">Final Amount</label>
				<p class="col-md-2">${oneTimeOrder.finalAmount}</p>
				<label class="col-md-2">Status</label>
				<p class="col-md-2">${oneTimeOrder.status}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Points Redemed</label>
				<p class="col-md-2">${oneTimeOrder.pointsRedemed}</p>
				<label class="col-md-2">Points Discount</label>
				<p class="col-md-2">${oneTimeOrder.pointsDiscount}</p>
				<label class="col-md-2">Delivery Boy</label>
				<p class="col-md-2">${oneTimeOrder.deliveryBoy}</p>
				
			</div>
			<div class="row">
			</div>
			<div class="row">
			</div>
		</div>

	</div>


	
	<div class="panel panel-info" id="Order Line Item-info">
		<div class="panel-heading">
			<h3 class="panel-title">Order Line Item Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<c:forEach items="${oneTimeOrder.orderLineItems}" var="orderLineItem">
		<fieldset>
			 <legend>Order Line Item Id -${orderLineItem.id}</legend>
			<div class="row">
				<label class="col-md-1">Product :</label>
					<p class="col-md-3">${orderLineItem.product.code} - ${orderLineItem.product.name} </p>
				<label class="col-md-2">Quantity</label>
					<p class="col-md-2">${orderLineItem.quantity}</p>
				<label class="col-md-2">Amount</label>
					<p class="col-md-2">${orderLineItem.amount}</p>
				</div>
			</fieldset>
				</c:forEach>
			</div>
			</div>
			<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${oneTimeOrder.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${oneTimeOrder.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${oneTimeOrder.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${oneTimeOrder.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>