<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/provilacimage/add" var="provilacimageadd" />
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN')">
	<a class="btn btn-primary" href="${provilacimageadd}">Add Provilac Images </a>&nbsp;
	</sec:authorize>
	</div>
	
</div>