<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Images</title>
<script type="text/javascript">
$(document).ready(function() {
});

function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}

function ValidateFileUpload() {
    
	var fuData = document.getElementById('image');
	//To check if user upload any file
    var fileArray = fuData.files;
    if (fileArray.length == 0) {
        alert("Please upload an image");
        return false;
    } else {
    	var imageContainerDiv = $('#imageContainer');
    	for(var index = 0; index < fileArray.length; index++) {
            var file = fileArray[index];
            var extension = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
    		//The file uploaded is an image
    		if (extension == "gif" || extension == "png" || extension == "bmp" || extension == "jpeg" || extension == "jpg") {
    			//To Display
   			    var reader = new FileReader();
    			var imgHtml = '<img id="displayImage_' + index + '" border="0" class="col-md-3" style="margin:5px;" onload="resizeImg(this,200, 300);"/>';
    			imageContainerDiv.append(imgHtml);
   			    reader.onload = (function(imgIndex) {
   			    	return function(e) {
   	   			        $('#displayImage_' + imgIndex).attr('src', e.target.result);
   	   			    }; 
   			    })(index);
   			    reader.readAsDataURL(file);
            } else {
    			//The file upload is NOT an image
                alert("Image only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
            }
    	}
    }
    return true;
}
function removeImage(index) {
	$("#" + index).remove();
}
function submitForm(){
	if(validate()){
		if(ValidateFileUpload()){
			$('#addProvilacImage').submit();
			toastr.success("Successfully Added Image");
		}
	}
}

function validate(){
	var imageClassLength= $(".imageClass").length;
	if(imageClassLength==0){
	 showToast('warning','Please Select Image');
		return false;
	}
	return true;
}

</script>
</head>
<c:url value="/admin/provilacimage/add" var="provilacImageAdd" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

	<form:form id="addProvilacImage" action="${provilacImageAdd}" method="post" modelAttribute="provilacimage" enctype="multipart/form-data">
		
		<fieldset>
		<h3 class="page-header">Add Provilac Image</h3>
			<form:errors path="provilacImage" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Select Image *</span> 
				<input name="provilacimage" class="form-control imageClass" onchange="javascript:ValidateFileUpload()" id="image" type="file"/>
			</div>
			<br>
			
			<div class="row">
        <div class="col-md-12"  id ="imageContainer" > 
		</div>
		<br>
		<form:errors path="linkTo" cssClass="text-danger" />
				<div class="input-group width-xlarge" style="margin-left: 15px;">
						<span class="input-group-addon">LinkTo:-</span> 
						<select name="linkTo" class="form-control">
							<option value=" ">Select linkTo</option>
							<option value="Product">Product</option>
							<option value="Subscription">Subscription</option>
							<option value="ReferAndEarn">ReferAndEarn</option>
							<option value="Calendar">Calendar</option>
						</select>
				</div>
				<br>
		</div>
		</fieldset>
		<div>
		<a class="btn btn-info" style="margin-left:1%" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
		</div>		
		<br />
		
	</form:form>
</body>