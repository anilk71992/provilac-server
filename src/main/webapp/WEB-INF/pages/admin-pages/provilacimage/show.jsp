<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Images</title>
<script type="text/javascript">
$(document).ready(function() {
	getImages();
});

function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}
function getImages(){
	$.ajax({
        url:'<c:url value="/openapi/provilacimages/list"/>',
        datatype:'json',
       	data : 'id=',
        success: function(response) {
            if(response.success){
                images = response.data.images;
                showImages();
            }else {
                showToast("warning",response.error.message);
            }
        }
    });
	
}
var linkToArray=Array();
function showImages(){
	$.each(	images,function(i, lineItem) {
		
		var html ='<div class="col-md-3" style="margin-left: 5px;" id ="imageBorder" >Image No:' + images[i].id+'<img id="displayImage"  src="<c:url value="/openapi/getProvilacImage?id=' + images[i].id+'"/>" style="width:100%";"height:100%"; onload="resizeImg(this,150, 500);"><br>';
		if(images[i].linkTo!=undefined){
			html +='<div><b>LinkTo:- '+images[i].linkTo+'<b></div>';
			linkToArray[images[i].id]=images[i].linkTo;
		}
		html += '<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" onclick="deleteImageRow(' + images[i].id + ')"><span class="glyphicon glyphicon-trash"></span></button>';
		html +='<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" onclick="showImageModal(' + images[i].id + ')"><span class="glyphicon glyphicon-pencil"></span></button></p></td></div></div>';
		
		
		$('#displayImageDiv').append(html);
	});
}
function showSingleImage(imageId){
	
	var html ='<div class="col-md-3" style="margin-left: 5px;" id ="imageBorder" >Image No:' +imageId+'<img id="displayImage"  src="<c:url value="/openapi/getProvilacImage?id=' +imageId+'"/>" style="width:100%";"height:100%"; onload="resizeImg(this,150, 500);"><br>';
	$('#displaySingleImage').append(html);
	$('#addImageModal').toggle();
	}
function deleteImageRow(id) {
	var deleteImage =confirm('Are you absolutely sure you want to delete?');

    if (deleteImage) {
    	var saveData= $.ajax({
    		type: 'POST',
            url:'<c:url value="/admin/provilacimage/delete"/>',
            datatype:'text',
            data : 'id='+id
    	});
    	toastr.success("Image Deleted...");
    	location.reload();
    }
}
function showImageModal(imageId) {
		showSingleImage(imageId);
		$('#imageid').val(imageId);
		var linkTo="";
		linkTo=linkToArray[imageId];
		if(linkTo!=undefined){
			$("#linkTo").val(linkTo);
		}
		
}

function hideModal(){
	$('#addImageModal').hide();
	location.reload();
	}
function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}
function submitForm(){
	if(validate()){
		/* if(ValidateFileUpload()){
			$('#updateProvilacImage').submit();
			toastr.success("Update Image SuccessFully...");
	} */
	
	}
	$('#updateProvilacImage').submit();
}
function validate(){
	
	var imageClassLength= $(".imageClass").length;
	 if(imageClassLength==0){
		 showToast('warning','Please Select Image');
			return false;
	 }
	 return true;
}

function ValidateFileUpload() {
    var fuData = document.getElementById('newprovilacimage');
    var FileUploadPath = fuData.value;

//To check if user upload any file
    if (FileUploadPath == '') {
        alert("Please upload an image");
        return false;

    } else {
        var Extension = FileUploadPath.substring(
                FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                || Extension == "jpeg" || Extension == "jpg") {

//To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#displayImageNew').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
            }
		return true;
        } 

//The file upload is NOT an image
else {
            alert("Image only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
            return false;

        }
    }
    return true;
}

</script>
</head>
<c:url value="/admin/provilacimage/update" var="provilacImageUpdate"/>
<body>

	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<%@ include file="nav.jsp"%>
		<div class="panel-body" style="margin-left: 5px;">
			<div class="row"  style="margin-left: 5px" id ="imageBorder" >
			<label >Images</label> 
			<div id ="displayImageDiv">
			
			</div>
			</div>
		</div>
		<div class="modal" id="addImageModal">
	<form:form id="updateProvilacImage" action="${provilacImageUpdate}" method="post" modelAttribute="provilacimage" enctype="multipart/form-data">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close"  onclick="javascript:hideModal();" data-dismiss="modal">�</button>
			<div class="panel-body" style="margin-left: 5px;">
			<div class="row"  style="margin-left: 5px" id ="imageBorder" >
			
			<input type="hidden" id="imageid" name="imageid"></input>
			<label >Update Image</label> 
			<div id ="displaySingleImage">
			
			</div>
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Select Image *</span> 
				<input name="newprovilacimage" class="form-control imageClass" onchange="javascript:ValidateFileUpload()" id="newprovilacimage"  type="file" />
			</div>
			<br>
       
       <form:errors path="linkTo" cssClass="text-danger" />
				<div class="input-group width-xlarge" style="margin-left: 15px;">
						<span class="input-group-addon">LinkTo:-</span> 
						<select name="linkTo" id="linkTo" class="form-control">
							<option value=" ">Select linkTo</option>
							<option value="Product">Product</option>
							<option value="Subscription">Subscription</option>
							<option value="ReferAndEarn">ReferAndEarn</option>
							<option value="Calendar">Calendar</option>
						</select>
				</div>
				<br>
			<img id="displayImageNew" border="0" height="real_height" width="real_width" onload="resizeImg(this,200, 300);">
		
			</div>
		</div>
			
		<a class="btn btn-danger" type="button" href="javascript:hideModal();" data-dismiss="modal">Cancel</a>
		<a  class="btn btn-info"  type="button" href="javascript:submitForm();">Update Image</a>&nbsp;&nbsp;
		
		</div>
		</div>
		</div>
		</form:form>
	</div>
		
</body>