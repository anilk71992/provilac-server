<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - UserSubscription</title>
</head>
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>UserSubscription</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>UserSubscription</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/userSubscription/list" />
	<c:set var="urlPrefix" value="/admin/userSubscription/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/userSubscription/show" var="userSubscriptionDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="userSubscriptions" class="footable" requestURI="" id="userSubscription" export="false">
			<display:column title="Code" sortable="true">
				<a href="${userSubscriptionDetailLinkPrefix}/${userSubscription.cipher}">${userSubscription.code}</a>
			</display:column>
			
			<display:column title="User" sortable="true">${userSubscription.user.firstName} ${userSubscription.user.lastName} - ${userSubscription.user.mobileNumber}</display:column>
			<display:column title="StartDate" sortable="true">
				<fmt:formatDate value="${userSubscription.startDate}" pattern="dd-MM-yyyy" />
			</display:column>
			<display:column title="IsCurrent" sortable="true">
				<c:choose>
					<c:when test="${userSubscription.isCurrent}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column>
			<sec:authorize access="!hasAnyAuthority('ROLE_INTERN','ROLE_CCE')">
			<%-- <display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<sec:authorize url="/admin/userSubscription/delete/${userSubscription.cipher}">
							<c:choose>
						<c:when test="${userSubscription.isCurrent}">Current Subscription</c:when>
						
						<c:otherwise>	<li><a href="#ConfirmModal_${userSubscription.id}" data-toggle="modal">Delete</a></c:otherwise>
					
						</c:choose>
							</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${userSubscription.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete UserSubscription?</h3>
							</div>
							<div class="modal-body">
								<p>The UserSubscription will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/userSubscription/delete/${userSubscription.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column> --%>
			</sec:authorize>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete userSubscriptions?</h3>
		</div>
		<div class="modal-body">
			<p>The selected userSubscription will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>