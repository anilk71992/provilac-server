<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - UserSubscription</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">UserSubscription - Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">User</label>
				<p class="col-md-2">${userSubscription.user.firstName} ${userSubscription.user.lastName} - ${userSubscription.user.mobileNumber}</p>
				<label class="col-md-2">Type</label>
				<p class="col-md-2">${userSubscription.subscriptionType}</p>
				<label class="col-md-2">Is Current</label>
				<p class="col-md-2">
					<c:choose>
						<c:when test="${userSubscription.isCurrent}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
					</c:choose>
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Start Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${userSubscription.startDate}" pattern="dd-MM-yyyy"/>
				</p>
				<label class="col-md-2">End Date</label>
				<p class="col-md-2">
					<c:choose>
						<c:when test="${userSubscription.subscriptionType == 'Prepaid'}">
							<fmt:formatDate value="${userSubscription.endDate}" pattern="dd-MM-yyyy"/>
						</c:when>
						<c:otherwise>N/A</c:otherwise>
					</c:choose>
				</p>
				<label class="col-md-2">Final Amount</label>
				<p class="col-md-2">${userSubscription.finalAmount}</p>
			</div>
		</div>
	</div>
	
	<div class="panel panel-info" id="Order Line Item-info">
		<div class="panel-heading">
			<h3 class="panel-title">Subscription Line Items</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<c:forEach items="${userSubscription.subscriptionLineItems}" var="requestLineItem">
				<fieldset>
					<legend>Request Line Item Id - ${requestLineItem.id}</legend>
					<div class="row">
						<label class="col-md-2">Product</label>
						<p class="col-md-2">${requestLineItem.product.name}</p>
						<label class="col-md-2">Quantity</label>
						<c:choose>
							<c:when test="${requestLineItem.type == 'Custom' }">
								<p class="col-md-2">${requestLineItem.customPattern}</p>
							</c:when>
							<c:otherwise>
								<p class="col-md-2">${requestLineItem.quantity}</p>
							</c:otherwise>
						</c:choose>						
						<label class="col-md-2">Type</label>
						<p class="col-md-2">${requestLineItem.type}</p>
					</div>
					<div class="row">
						<c:choose>
							<c:when test="${requestLineItem.type == 'Weekly' }">
								<label class="col-md-2">Day</label>
								<p class="col-md-2">${requestLineItem.day}</p>
							</c:when>
						</c:choose>
						<%-- <label class="col-md-2">Current Index</label>
					<p class="col-md-2">${requestLineItem.currentIndex}</p> --%>
					</div>
				</fieldset>
			</c:forEach>
		</div>
	</div>
	
	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${userSubscription.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${userSubscription.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${userSubscription.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${userSubscription.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>