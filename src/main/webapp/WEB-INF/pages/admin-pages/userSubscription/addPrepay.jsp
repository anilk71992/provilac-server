<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Prepay Subscription</title>
<%@ include file="../underscore-templates.tmpl"%>
<script type="text/javascript">
var products;
var lineItemNumber= 1;
var lineItemNumber1=1;
var lineItemNumber2=1

var existingLineItems = new Array();
<c:forEach items="${userSubscription.subscriptionLineItems}" var="lineItem">
	var lineItemObj = new Object();
	lineItemObj['product'] = ${lineItem.product.name};
	lineItemObj['quantity'] = ${lineItem.quantity};
	existingLineItems.push(lineItemObj);
</c:forEach>

$(document).ready(function() {
	$('#user').select2();
	$.ajax({
		url:'<c:url value="/subscriptionLineItem/getProductsForSubscription"/>',
		dataType:'json',
		
		success: function(response){
			if(null!=response &&response.success){
				products = response.data.products
			}else{
				alert("Error while retrieving products");
			}
		}
	});
	$('#startDate').datepicker({autoclose:true});
	$('.productClass').change(function() {
		$('#submitBtn').hide();
	});
	$('.quantityClass').change(function() {
		$('#submitBtn').hide();
	});
	onCustomerChange(this);
});

var isRoutePresent = false;
var defaultAddress = false;
function onCustomerChange(select, callback){
	
	hideAddButton();
	
	var customer= document.getElementById('user');
	var customerId =  customer.options[customer.selectedIndex].value;
	
	if(!customerId || customerId=="-1"){
		showToast('warning','Please select customer.');
		return false;
	}
	$.ajax({
		url:'<c:url value="/restapi/routeRecord/checkRouteRecordForCustomer"/>',
		dataType:'json',
		data : 'customerId=' + customerId,
		success: function(response){
			if(null!=response &&response.success){
				var record = response.data.customerRecord;
				isRoutePresent = record.isRouteAssign;
			    defaultAddress = record.defaultAddress;
				if(null != callback) {
					callback();
				}
			}else{
				showToast('warning','Please assign route for selected customer');
			}
		}
	});	
}

function addLineItem1() {
	hideAddButton();
	var template = _.template($("#lineItemTemplateForWeekly").html());
	var htmlText = template(products,lineItemNumber1);
	$('#lineItem1Div').append(htmlText);
	lineItemNumber1++;
}

function removeLineItem(subscriptionLineItem) {
	hideAddButton();
	subscriptionLineItem.remove();
}

function onTypeChange(id){
	
	hideAddButton();
	
	var typeSelect= document.getElementById('type_'+id);
	var onType = typeSelect.options[typeSelect.selectedIndex].getAttribute('numVal');
	
	 if(onType==1){
		$('#day1_'+id).show();
	}else{
		$('#day1_'+id).hide();
		$('#day1_'+id).val(" ");
	}
}

function hideAddButton() {
	$('#submitBtn').hide();
} 

function submitClick(){
	
	if($('#pendingDues').val() > 0 && $('#existingSubscriptionType').val() != 'Prepaid') {
		showToast('warning','Customer has pending dues. Please clear them first.');
		return;
	}
	
	if($('#txnId').val() == '') {
		showToast('warning','Please enter transaction id/receipt number');
		return;
	}
	
	if (validate()) {
		$('#addUserSubscription').submit();
	}
}

function validate() {
	
	if(validateLineItemsAndDate()) {
		if (!isRoutePresent) {
			showToast('warning','Please assign route for selected customer');
			return false;
		}
		  
		if(defaultAddress==false){
			showToast('warning','Please Add default address for selected customer');
			return false;
		}
		return true;
	}
	return false;
}

function calculatePrice() {
	
	if(!validateLineItemsAndDate()) {
		return;
	}

	var queryString = '';
	var children = $('#lineItem1Div').children('.lineItemContainer');
	var productTypeMap = new Object();
	var weeklyProductDaysMap = new Object();
	for(var i = 0; i < children.length; i++) {
		var lineItemDiv = children[i];
		var productSelect = $(lineItemDiv).find('.productClass');
		var productCode = $(productSelect).val();
		var typeSelect = $(lineItemDiv).find('.typeClass');
		var type = $(typeSelect).val();
		var qtyInput = $(lineItemDiv).find('.quantityClass');
		var qty = $(qtyInput).val();
		var daySelect = $(lineItemDiv).find('.dayClass');
		var day = $(daySelect).val();
		
		if(productCode in productTypeMap) {
			//Allow lineitems with same product only in the case of weekly type
			if(!(productTypeMap[productCode] == 'Weekly')) {
				alert('You can add lineitems with same product only for weekly type');
				return;
			}
			//Check for same day
			var days = weeklyProductDaysMap[productCode];
			if($.inArray(day, days) >= 0) {
				alert('You can not add lineitem for same weekday for weekly subscription');
				return;
			}
		} else {
			productTypeMap[productCode] = type;
		}
		if(type == 'Weekly') {
			var days = new Array();
			if(productCode in weeklyProductDaysMap) {
				days = weeklyProductDaysMap[productCode];
			}
			days.push(day);
			weeklyProductDaysMap[productCode] = days
		}
		var param = 'productCodes=' + productCode + '&types=' + type + '&quantities=' + qty + '&days=' + day;
		if(queryString == '') {
			queryString = param;
		} else {
			queryString = queryString + '&' + param;
		}
	}
	
	var customer= document.getElementById('user');
	var customerId =  customer.options[customer.selectedIndex].value;
	
	if(!customerId || customerId=="-1"){
		showToast('warning','Please select customer.');
		return false;
	}
	
	var startDate = $('#startDate').val();
	var duration = $('#duration').val();
	queryString = queryString + '&customerId=' + customerId + '&startDate=' + startDate + '&duration=' + duration; 
	
	$.ajax({
		url:'<c:url value="/restapi/prepay/calculatePrice"/>',
		dataType:'json',
		data : queryString,
		success: function(response){
			if(null!=response &&response.success){
				
				var data = response.data;
				$('#totalAmount').val(data.subscriptionPrice);
				$('#pendingDues').val(data.pendingDues);
				if(data.isActive) {
					$('#existingSubscriptionType').val(data.existingSubscriptionType);
				} else {
					$('#existingSubscriptionType').val('N/A');
				}
				$('#submitBtn').show();
			}else{
				showToast('warning','Something went wrong, please try again later.');
			}
		}
	});
}

function validateLineItemsAndDate() {
	
	if(!$('#startDate').val().trim()){
		showToast("warning","Please  Enter Start date");
		return false;
	}
	
	var products = new Array();
	
	var typeAndProductArray = new Array();
	var lineItemLenght = $(".typeClass").length;
	var regEx=/[0-9]/;
	for(var i=0;i<=lineItemLenght-1;i++){
		
		if(!$($(".typeClass")[i]).val()||$($(".typeClass")[i]).val()==""){
			isValidate=true;
			return false;
			showToast("warning","Please select type");
		}
		if(!$($(".productClass")[i]).val()||$($(".productClass")[i]).val()==""){
			isValidate=true;
			return false;
			showToast("warning","Please select product");
		}
		$($(".quantityClass")[i]).val($($(".quantityClass")[i]).val().trim());
		if(!$($(".quantityClass")[i]).val()||$($(".quantityClass")[i]).val().trim()=="" || $($(".quantityClass")[i]).val()<=0 || !regEx.test($($(".quantityClass")[i]).val().trim())){
			showToast("warning","Please Enter Valid Quantity");
			($(".quantityClass")[i]).focus();
			return false;
		}
		
		var typeAndProduct = ($($(".typeClass")[i]).val()+""+$($(".productClass")[i]).val()+""+$($(".dayClass")[i]).val());
		if($.inArray(typeAndProduct, typeAndProductArray) > -1){
			($(".productClass")[i]).focus();
			showToast('warning','Duplicate Type with this Product');
			isValidate = true;
			return false;
		}
		
		typeAndProductArray.push($($(".typeClass")[i]).val()+""+$($(".productClass")[i]).val()+""+$($(".dayClass")[i]).val());
		products.push($($(".productClass")[i]).val());
	}
	var str= null;
	str = $('#lineItem1Div').html();
	if(str.indexOf(name="product")<=0){
		showToast('warning','Please add lineItem');
		return false;
	}
	if(str.indexOf(name="type")<=0){
		showToast('warning','Please select type');
		return false;
	}
	return true;
}

</script>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/prepaySubscription/add" var="addUserSubscription" />

	<form:form id="addUserSubscription" action="${addUserSubscription}" method="post" modelAttribute="userSubscription" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<div class="raw">
			<!-- <div class="col-md-4"> -->
			<fieldset>
				<h3 class="page-header">Prepay Subscription</h3>
				<div class="col-md-6">
					<sec:authorize access="!hasAnyAuthority('ROLE_INTERN')">
						<form:errors path="user" cssClass="alert-danger" />
						<div class="input-group width-xlarge">
							<!-- <span class="input-group-addon">User*</span> -->
							<form:select path="user" cssClass="form-control" onchange="onCustomerChange(this)" id="user">
								<option value=" ">Select Customer</option>
								<c:forEach items="${users}" var="user">
									<option value="${user.id}" <c:if test="${user.id == userSubscription.user.id}">selected="selected"</c:if> >${user.firstName}${user.lastName} - ${user.mobileNumber}</option>
								</c:forEach>
							</form:select>
						</div>
						<br>
					</sec:authorize>
					<sec:authorize access="hasAnyAuthority('ROLE_INTERN')">
						<form:errors path="user" cssClass="alert-danger" />
						<div class="input-group width-xlarge">
							<!-- <span class="input-group-addon">User*</span> -->
							<form:select path="user" cssClass="form-control" onchange="onCustomerChange(this)" id="user">
								<option value=" ">Select Customer</option>
								<c:forEach items="${newCustomers}" var="user">
									<option value="${user.id}" <c:if test="${user.id == userSubscription.user.id}">selected="selected"</c:if> >${user.firstName}${user.lastName} - ${user.mobileNumber}</option>
								</c:forEach>
							</form:select>
						</div>
						<br>
					</sec:authorize>
					<form:errors path="startDate" cssClass="text-danger" />
					<span class="label label-warning customWarning">Please select start date later than last prepaid delivery otherwise it will overwrite those deliveries.</span>
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Start Date*</span>
						<form:input path="startDate" cssClass="form-control" value="${today}" data-date-format="dd-mm-yyyy" />
					</div>
					<br>
					<form:errors path="endDate" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Duration*</span>
						<select name="duration" id="duration" class="form-control">
							<option value="3">3 MONTHS</option>
							<option value="6">6 MONTHS</option>
							<option value="9">9 MONTHS</option>
							<option value="12">12 MONTHS</option>
						</select>
					</div>
					<br>
					<form:errors path="permanantNote" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Permenant Note</span> <input class="form-control" name="permanantNote" id="permanantNote" type="text">
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Pending Dues</span>
						<input id="pendingDues" class="form-control" readonly/>
					</div>
					<br>
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Current Subscription</span>
						<input id="existingSubscriptionType" class="form-control" readonly/>
					</div>
					<br>
					<form:errors path="totalAmount" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Total Amount*</span>
						<form:input path="totalAmount" cssClass="form-control" readonly="true"/>
					</div>
					<br>
					<form:errors path="txnId" cssClass="text-danger" />
					<div class="input-group width-xlarge">
						<span class="input-group-addon">Receipt No/Txn Id*</span>
						<form:input path="txnId" cssClass="form-control"/>
					</div>
					<br>
				</div>
			</fieldset>
			
			<fieldset>
				<div id="lineItem1Div">
					<h3 class="page-header">Subscription Line Items</h3>
					<form:errors path="subscriptionLineItems" cssClass="text-danger" />
					<a href="javascript:addLineItem1()" class="btn btn-info">Add Line Item</a> <span class="label label-warning customWarning">For Type-Alternate, add quantity in this format 2-4 where 2 is a actual quantity for selected date</span>
				</div>
			</fieldset>
			<!-- </div> -->
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" id="calculatePriceBtn" href="javascript:calculatePrice();">Calculate Price</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-info" id="submitBtn" href="javascript:submitClick();">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
	<c:set value="${userSubscription.subscriptionLineItems}" var="lineItems"></c:set>
</body>