<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac- Update User</title>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>
<c:url value="/admin/userSubscription/update" var="userSubscriptionUpdate" />
<c:url value="/admin/userSubscription/list" var="userSubscriptionList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/userSubscription/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="userSubscription" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>
		<fieldset>
			<h3 class="page-header">UserSubscription</h3>
			<form:errors path="user" cssClass="alert-danger" />
			<div class="input-group width-xlarge">
			<span class="input-group-addon">User*</span>
			<form:select path="user" cssClass="form-control">
				<c:forEach items="${users}" var="user">
					<option value="${user.id}">${user.firstName} ${user.lastName} - ${user.mobileNumber}   </option>
				</c:forEach>
			</form:select>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Update</button>
           	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>