<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/category/add" var="categoryAdd" />
<c:url value="/admin/category/list" var="categoryList" />
<c:url value="/admin/category/import" var="categoryImport" />
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN','ROLE_ADMIN')">
	<a class="btn btn-primary" href="${categoryAdd}">Add New Category</a>&nbsp;
	<a class="btn btn-primary" href="${categoryImport}">Import Categories</a>&nbsp;
	</sec:authorize>
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>