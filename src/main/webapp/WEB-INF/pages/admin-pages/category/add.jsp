<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Category</title>
<script type="text/javascript">
$(document).ready(function() {
});

function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}

function ValidateFileUpload(id) {
	var fuData;
	if(id==1){
		fuData = document.getElementById('image');
	}
    var FileUploadPath = fuData.value;

//To check if user upload any file
    if (FileUploadPath == '') {
        alert("Please upload an image");

    } else {
        var Extension = FileUploadPath.substring(
                FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                || Extension == "jpeg" || Extension == "jpg") {

//To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                	if(id==1){
                    $('#displayImage').attr('src', e.target.result);
                	}
                }

                reader.readAsDataURL(fuData.files[0]);
            }

        } 

//The file upload is NOT an image
else {
            alert("Image only allows file types of GIF, PNG, JPG, JPEG and BMP. ");

        }
    }
}

function submitForm(){
	if(validate()){
		$('#addCategory').submit();
	}
}

function isAlphaOrParen(str) {
	  return /^[a-zA-Z ]+$/.test(str);
}
function validate(){
	
	if (!$('#name').val().trim()) {
		showToast('warning','Please enter service Category name');
		return false;
	}else{
		var alphaNumericTest = isAlphaOrParen($('#name').val());		
		if(!alphaNumericTest){
		showToast('warning','Please enter valid Category name');
		return false;
		}
	}
	
	var imageClassLength= $(".imageClass").length;
	 if(imageClassLength==0){
		 showToast('warning','Please Select Image For Product ');
			return false;
	 }
	 
	return true;
}
</script>
</head>
<c:url value="/category/add" var="categoryAdd" />
<c:url value="/category/list" var="categoryList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/category/add" var="addCategory" />

	<form:form id="addCategory" action="${addCategoryPic}" method="post" modelAttribute="category" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">Category</h3>

			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name</span>
				<form:input path="name" cssClass="form-control" />
			</div>
			<br>
			
			<form:errors path="description" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Description</span>
				<%-- <form:input path="description" id="description" cssClass="form-control" type="hidden"/> --%>
				<textarea rows="8" cols="40" id="descriptionTextArea" path="description" name="description"></textarea>
			</div>
			<br>
			
			<form:errors path="categoryPicUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Category Pic *</span> 
				<input name="categoryPic" class="form-control imageClass categoryPicUrl" onchange="javascript:ValidateFileUpload(1)" id="image"  type="file" />
			</div>
			<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">(Pic Dimensions) Width 640 x Height 640</span>
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			<br>
			
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" style="margin-left:1%" href="javascript:submitForm();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>