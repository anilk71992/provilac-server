<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Prepay Request</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Prepay Request- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">User</label>
				<p class="col-md-2">${prepaySubscriptionRequest.user.firstName} ${prepaySubscriptionRequest.user.lastName} - ${prepaySubscriptionRequest.user.mobileNumber}</p>
				<%-- <label class="col-md-2">User Address</label>
				<p class="col-md-2">${prepaySubscriptionRequest.user.address.name}</p> --%>
			</div>
		</div>
	</div>
	
	<div class="panel panel-info" id="Order Line Item-info">
		<div class="panel-heading">
			<h3 class="panel-title">Prepay Request Line Item Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<c:forEach items="${prepaySubscriptionRequest.prepayRequestLineItems}" var="prepayRequestLineItem">
		<fieldset>
			 <legend>Request Line Item Id -${prepayRequestLineItem.id}</legend>
			<div class="row">
				<label class="col-md-2">Product</label>
					<p class="col-md-2">${prepayRequestLineItem.product.name}</p>
				<label class="col-md-2">Quantity</label>
					<p class="col-md-2">${prepayRequestLineItem.quantity}</p>
				<label class="col-md-2">Type</label>
					<p class="col-md-2">${prepayRequestLineItem.type}</p>
				</div>
				<div class="row">
				<label class="col-md-2">Day</label>
					<p class="col-md-2">${prepayRequestLineItem.day}</p>
				<label class="col-md-2">Custom Pattern</label>
					<p class="col-md-2">${prepayRequestLineItem.customPattern}</p>
				<%-- <label class="col-md-2">Current Index</label>
					<p class="col-md-2">${prepayRequestLineItem.currentIndex}</p> --%>
				</div>
			</fieldset>
				</c:forEach>
			</div>
		</div>
			
	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${prepaySubscriptionRequest.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${prepaySubscriptionRequest.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${prepaySubscriptionRequest.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${prepaySubscriptionRequest.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>