<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - DeliveryLineItem</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>DeliveryLineItems</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>DeliveryLineItems</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/deliveryLineItem/list" />
	<c:set var="urlPrefix" value="/admin/deliveryLineItem/list?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<c:url value="/admin/deliveryLineItem/show" var="deliveryLineItemDetailLinkPrefix" />
	<%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="deliveryLineItems" class="footable" requestURI="" id="deliveryLineItem" export="false">
			<display:column title="Code" sortable="true">
				<a href="${deliveryLineItemDetailLinkPrefix}/${deliveryLineItem.cipher}">${deliveryLineItem.code}</a>
			</display:column>
			<display:column title="DeliverySchedule" sortable="true">${deliveryLineItem.deliverySchedule.code}</display:column>
			<display:column title="Product" sortable="true">${deliveryLineItem.product.name}</display:column>
			<display:column title="Day" sortable="true">${deliveryLineItem.day}</display:column>
			<display:column title="Quantity" sortable="true">${deliveryLineItem.quantity}</display:column>
			
			
			<display:column title="Actions" sortable="false">
				<div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/deliveryLineItem/update/${deliveryLineItem.cipher}"/>">Edit</a>
						</li>
						<sec:authorize url="/admin/deliveryLineItem/delete/${deliveryLineItem.cipher}">
							<li><a href="#ConfirmModal_${deliveryLineItem.id}" data-toggle="modal">Delete</a></li>
						</sec:authorize>
					</ul>
				</div>

				<!-- modal -->
				<div class="modal" id="ConfirmModal_${deliveryLineItem.id}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">�</button>
								<h3 id="myModalLabel">Delete DeliveryLineItem?</h3>
							</div>
							<div class="modal-body">
								<p>The DeliveryLineItem will be permanently deleted. This action cannot
									be undone.</p>
								<p>Are you sure?</p>
							</div>
							<div class="modal-footer">
								<a class="btn btn-primary" type="button"
									href="<c:url value="/admin/deliveryLineItem/delete/${deliveryLineItem.cipher}"/>">Delete</a>
								<button class="btn" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete deliveryLineItems?</h3>
		</div>
		<div class="modal-body">
			<p>The selected deliveryLineItem will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>