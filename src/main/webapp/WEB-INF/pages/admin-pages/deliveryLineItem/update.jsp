<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>HelloDhobi - Update DeliveryLineItem</title>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>

<c:url value="/admin/deliveryLineItem/update" var="deliveryLineItemUpdate" />
<c:url value="/admin/deliveryLineItem/list" var="deliveryLineItemList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/deliveryLineItem/update" var="updateUser"/>
	<form:form id="updateUser" method="post" modelAttribute="deliveryLineItem" action="${updateUser}">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">DeliveryLineItem</h3>

			<form:errors path="day" cssClass="text-danger"/>
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Day</span> 
				<form:select path="day" cssClass="form-control">
					<form:option value=" ">Select Day</form:option>
					<form:option value="Day_1">Sunday</form:option>
					<form:option value="Day_2">Monday</form:option>
					<form:option value="Day_3">Tuesday</form:option>
					<form:option value="Day_4">Wednesday</form:option>
					<form:option value="Day_5">Thursday</form:option>
					<form:option value="Day_6">Friday</form:option>
					<form:option value="Day_7">Saturday</form:option>
					
				</form:select>
			</div>
			<br>
			<form:errors path="quantity" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Quantity</span>
				<form:input path="quantity" cssClass="form-control" />
			</div>
			<br>
			
			<form:errors path="product" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Product</span>
				<form:select path="product" items="${products}" itemLabel="name" itemValue="id" cssClass="form-control"></form:select>
			</div>
			<br>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<button class="btn btn-info" type="submit">Update</button>
           	&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel
		</a>
	</form:form>

</body>