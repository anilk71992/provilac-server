<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Update Product</title>
<c:set value="${product.id}" var="id"></c:set>
<script type="text/javascript">
$(document).ready(function() {
	displayImage();
	
	$('#category').select2();
	
	$("#descriptionTextArea").val("${product.description}");
});


function displayImage(){
	var productId = ${product.id};
	var html ='<img id="displayImage" src="<c:url value="/restapi/getProductPicture?id=' + productId+'"/>" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">';
	$('#displayImageDiv').append(html);
}

function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}

function ValidateFileUpload(id) {
	var fuData;
	if(id==1){
		fuData = document.getElementById('image');
	} else if(id==2){
		 fuData = document.getElementById('imageBan');
	} else if(id==3){
		fuData = document.getElementById('image3');
	} else if(id==4){
		fuData = document.getElementById('image4');
	} else if(id==5){
		fuData = document.getElementById('image5');
	}
    var FileUploadPath = fuData.value;

//To check if user upload any file
    if (FileUploadPath == '') {
        alert("Please upload an image");

    } else {
        var Extension = FileUploadPath.substring(
                FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                || Extension == "jpeg" || Extension == "jpg") {

//To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                	if(id==1){
                       $('#displayImage').attr('src', e.target.result);
                   	} else if(id==2){
                   		 $('#displayImage1').attr('src', e.target.result);
                   	} else if(id==3){
                		 $('#displayImage2').attr('src', e.target.result);
                	} else if(id==4){
                		 $('#displayImage3').attr('src', e.target.result);
                	} else if(id==5){
                		 $('#displayImage4').attr('src', e.target.result);
                	}
                }

                reader.readAsDataURL(fuData.files[0]);
            }

        } 

//The file upload is NOT an image
else {
            alert("Image only allows file types of GIF, PNG, JPG, JPEG and BMP. ");

        }
    }
}

function submitForm(){
	if(validate()){
		document.getElementById('description').value='';
		$("#description").val($("#descriptionTextArea").val());
		$('#updateProduct').submit();
	}
}
function isAlphaOrParen(str) {
	  return /^[a-zA-Z() ]+$/.test(str);
}

function validate(){
	
	if ($('#category').val()==-1) {
		showToast('warning','Please Select Category');
		return false;
	}
	
	
	if (!$('#name').val().trim()) {
		showToast('warning','Please enter product name');
		return false;
	}/* else{
		var alphaNumericTest = isAlphaOrParen($('#name').val());		
		if(!alphaNumericTest){
		showToast('warning','Please enter valid product Name');
		return false;
		}
	} */
	
	if ($('#price').val()<=0) {
		showToast('warning','Please Enter Price greater Than 0');
		return false;
	}
	

	if ($('#tax').val()<0) {
		showToast('warning','Please Enter Valid Tax ');
		return false;
	}
	
	/* if($("#AvailableOneTimeOrder").val()=="false" && $("#AvailableSubscription").val()=="false"){
		 showToast('warning','Please Choose Product Type (One Time Order OR Subscription )');
		 return false;
	 } */
	
	return true;
}
</script>
</head>

<c:url value="/admin/product/update" var="productUpdate" />
<c:url value="/admin/product/list" var="productList" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>

<c:url value="/admin/product/update" var="updateUser"/>
	<form:form id="updateProduct" method="post" modelAttribute="product" action="${updateUser}" enctype="multipart/form-data">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<fieldset>
			<h3 class="page-header">Product</h3>
			<div class="row">
			<div class="col-md-6">
			<form:errors path="category" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Category</span>
				<form:select path="category" itemLabel="name" itemValue="id" cssClass="form-control">
					<option value="-1"> -- Select Category -- </option>
					<c:forEach items="${categories}" var="category">
						<option value="${category.id}"<c:if test="${category.id == product.category.id}">selected="selected"</c:if>>${category.name}</option>
					</c:forEach>
					</form:select>
					
			</div>
			<br>
			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name</span>
				<form:input path="name" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="price" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Price</span>
				<form:input path="price" cssClass="form-control" />
			</div>
			<br>
			<form:errors path="colorCode" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Color</span>
				<input name="colorCode" value="${product.colorCode}" class="form-control jscolor" />
			</div>
			<br>
			<form:errors path="AvailableSubscription" cssClass="text-danger" />
				<div class="input-group width-xlarge">
				<span class="input-group-addon">Available For Subscription</span>
				<form:select path="AvailableSubscription" cssClass="form-control">
					<form:option value="true">Yes</form:option>
					<form:option value="false">No</form:option>
					</form:select>
				</div>
			<br>
						
			<form:errors path="AvailableOneTimeOrder" cssClass="text-danger" />
				<div class="input-group width-xlarge">
					<span class="input-group-addon">Available For OneTimeOrder</span>
					<form:select path="AvailableOneTimeOrder" cssClass="form-control">
						<form:option value="true">Yes</form:option>
							<form:option value="false">No</form:option>
					</form:select>
				</div>
			<br>
			
				<form:errors path="productPicUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Product Pic *</span> 
				<input name="productPic" class="form-control imageClass" onchange="javascript:ValidateFileUpload(1)" id="image"  type="file" />
			</div>
			<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">(Pic Dimensions) Width 640 x Height 640</span>
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			<br>
			<form:errors path="productBannerUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Product Banner Pic *</span> 
				<input name="productBan" class="form-control imageClass" onchange="javascript:ValidateFileUpload(2)" id="imageBan"  type="file" />
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage1" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			
			</div>
			<div class="col-md-6">
				<form:errors path="tax" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Tax</span>
				<form:input path="tax" cssClass="form-control" />
			</div>
			
			<br>
			<form:errors path="description" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Description</span>
				<form:input path="description" id="description" cssClass="form-control" type="hidden"/>
				<textarea rows="8" cols="40" id="descriptionTextArea"></textarea>
			</div>
			<br>
			
			<form:errors path="productCartPicUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">product Cart Pic *</span> 
				<input name="productCart" class="form-control imageClass" onchange="javascript:ValidateFileUpload(3)" id="image3"  type="file" />
			</div>
			<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">(Pic Dimensions) Width 120 x Height 80</span>
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage2" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			<br>
			<form:errors path="productListPicUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">product List Pic *</span> 
				<input name="productList" class="form-control imageClass" onchange="javascript:ValidateFileUpload(4)" id="image4"  type="file" />
			</div>
			<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">(Pic Dimensions) Width 270 x Height 270</span>
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage3" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			<br>
			<form:errors path="productDetailsPicUrl" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon">product Details Pic *</span> 
				<input name="productDetails" class="form-control imageClass" onchange="javascript:ValidateFileUpload(5)" id="image5"  type="file" />
			</div>
			<div class="col-md-3 col-xs-offset-1">
			<span class="label label-warning customWarning">(Pic Dimensions) Width 353 x Height 608</span>
			</div>
			<br>
			<div class="col-md-8" id ="imageBorder" > 
				<img id="displayImage4" border="0" height="real_height" width="real_width" onload="resizeImg(this,150, 290);">
			</div>
			
			</div>
		</div>
		</fieldset>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		<a class="btn btn-info" style="margin-left:1%" href="javascript:submitForm();" >Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>

</body>