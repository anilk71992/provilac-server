<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Import Products</title>
</head>
<c:url value="/admin/product/import" var="importUrl" />
<body>
	<sec:authorize access="isAnonymous()">
	    <c:redirect url="/login"/>
	</sec:authorize>
	<form id="importProduct" method="post" action="${importUrl}" enctype="multipart/form-data">
		<div>
		    <c:if test="${not empty message}">
		        <div id="message" class="success">${message}</div>  
		    </c:if>
		</div>
		<fieldset>
			<h3 class="page-header">Product - Import</h3>
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Excel File*</span>
				<input name="file" id="file" type="file" class="form-control" />
			</div>
			<br>
		</fieldset>
	
		<a href="<c:url value="/assets/import-templates/Products_Import_Template.xlsx" />">Download import template files from here!!</a>
		<br/>
		<br>
		<button class="btn btn-info" type="submit">Import</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-danger" href="javascript:history.back(1)">Cancel </a>
	</form>
</body>