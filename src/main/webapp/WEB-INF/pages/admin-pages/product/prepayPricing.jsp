<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Prepay Pricing</title>

<style type="text/css">
.input-color .color-box {
    width: 10px;
    height: 10px;
    display: inline-block;
    background-color: #ccc;
    position: absolute;
    left: 5px;
    top: 5px;
    border: 1px solid #000;
}
</style>
</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Prepay Pricing</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Prepay Pricing</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="formUrl" value="/admin/prepayPricing" />
	
	<form id="bulkSelect" action="${formUrl}" method="post">
		
		<br>
		<div class="input-group width-xlarge">
			<span class="input-group-addon">Prepay Amount</span>
			<input name="prepayAmount" class="form-control" value="${prepayAmount}" type="number"/>
		</div>
		<br>
			
		<display:table name="products" class="footable" requestURI="" id="product" export="false">
			<display:column title="Code" sortable="true" property="code"/>
			<display:column title="Name" sortable="true">${product.name}</display:column>
			<display:column title="Price" sortable="true">${product.price}</display:column>
			<display:column title="Category" sortable="true">${product.category.name}</display:column>
			<display:column title="Color">
				<c:choose>
					<c:when test="${empty product.colorCode}">
					N/A
				</c:when>
					<c:otherwise>
						<input style="background-color: #${product.colorCode}" disabled="disabled"/>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Effective Price" sortable="false">
				<c:choose>
					<c:when test="${not empty pricingMap[product.code] }">
						<input class="form-control" name="productPrice" type="number" value="${pricingMap[product.code]}">
					</c:when>
					<c:otherwise>
						<input class="form-control" name="productPrice" type="number" value="0.0">
					</c:otherwise>
				</c:choose>
				<input type="hidden" name="productCode" value="${product.code}">
			</display:column>
		</display:table>
		<br>
		<button class="btn btn-info" type="submit">Submit</button>
		<a class="btn btn-default" href="#prepayPricingDetails" data-toggle="modal">View Existing Pricings</a>
	</form>
	
	<!-- modal -->
	<div class="modal" id="prepayPricingDetails">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 id="myModalLabel">Prepay Pricing</h3>
				</div>
				<div class="modal-body">
					<c:choose>
						<c:when test="${not empty products}">
							<c:set var="pricingMap" value="${products[0].pricingMap}" />
							<table border="1" style="text-align: center;">
								<tr>
									<th>Prepay Amount</td>
									<c:forEach items="${products}" var="product">
										<th>${product.name}</th>
									</c:forEach>
								</tr>
								<c:forEach items="${pricingMap}" var="entry">
									<tr>
										<td>${entry.key}</td>
										<c:forEach items="${products}" var="product">
											<c:forEach items="${product.pricingMap}" var="pricingEntry">
												<c:if test="${pricingEntry.key eq entry.key}">
													<td>${pricingEntry.value}</td>
												</c:if>
											</c:forEach>
										</c:forEach>
									</tr>	
								</c:forEach>
							</table>
						</c:when>
						<c:otherwise>No products available.</c:otherwise>
					</c:choose>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">Ok</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>