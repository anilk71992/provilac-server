<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Product</title>

<script type="text/javascript">
$(document).ready(function() {
	displayImage();
});
function displayImage(){
	var productId = ${product.id};
	var html ='<img id="displayImage" src="<c:url value="/restapi/getProductPicture?id=' + productId+'"/>" border="0" height="real_height" width="real_width" onload="resizeImg(this,300, 500);">';
	$('#displayImageDiv').append(html);
}

function resizeImg(img, height, width) {
	img.height = height;
    img.width = width;
}

</script>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Product- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Name</label>
				<p class="col-md-2">${product.name}</p>
				<label class="col-md-2">Price</label>
				<p class="col-md-2">${product.price}</p>
				<label class="col-md-2">Category</label>
				<p class="col-md-2">${product.category.name}</p>
			</div>
			
			
			<div class="row">
				<label class="col-md-2">Tax</label>
				<p class="col-md-2">${product.tax}</p>
				<label class="col-md-2">Description</label>
				<p class="col-md-4">${product.description}</p>
				
			</div>
			<div class="row">
				<label class="col-md-3">Available For Subscription</label>
					<c:choose>
						<c:when test="${product.availableSubscription}"><p class="col-md-2">Yes</p></c:when>
						<c:otherwise><p class="col-md-2">No</p></c:otherwise>
					</c:choose>
				<label class="col-md-3">Available For One Time Order</label>
				<c:choose>
						<c:when test="${product.availableOneTimeOrder}"><p class="col-md-2">Yes</p></c:when>
						<c:otherwise><p class="col-md-2">No</p></c:otherwise>
					</c:choose>
			</div>
			<div class="row"  style="margin-left: 5px" id ="imageBorder" >
			<label >Image</label> 
			<div id ="displayImageDiv"></div>
				
			</div>
			
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${product.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${product.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${product.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${product.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>