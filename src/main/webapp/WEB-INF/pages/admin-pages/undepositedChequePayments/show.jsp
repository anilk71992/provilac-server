<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Payment</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Payment- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Customer</label>
				<p class="col-md-2">${payment.customer.fullName} - ${payment.customer.mobileNumber}</p>
				<label class="col-md-2">Transaction Id</label>
				<p class="col-md-2">${payment.txnId}</p>
				<label class="col-md-2">Date</label>
				<p class="col-md-2">${payment.date}</p>
			</div>
			
			<div class="row">
				<label class="col-md-2">Amount</label>
				<p class="col-md-2">${payment.amount}</p>
				<label class="col-md-2">PaymentMethod</label>
				<p class="col-md-2">${payment.paymentMethod}</p>
				<label class="col-md-2">Cheque Number</label>
				<p class="col-md-2">${payment.chequeNumber}</p>
			</div>
			<div class="row">
				<label class="col-md-2">Bank Name</label>
				<p class="col-md-2">${payment.bankName}</p>
				<label class="col-md-2">Received By</label>
				<p class="col-md-2">${payment.receivedBy}</p>
				<label class="col-md-2">Bounced</label>
				<p class="col-md-2">${payment.isBounced}</p>
			</div>
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${payment.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${payment.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${payment.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${payment.modifiedDate}" pattern="dd-MM-yyy hh:mm:ss a" />
				</p>
			</div>
		</div>
	</div>
	<a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
</body>
</html>