<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Payment Details</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Payment Details</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Payment Details</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/payment/details" />
	<c:set var="urlPrefix" value="/admin/payment/details?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<%-- <c:url value="/admin/route/show" var="routeDetailLinkPrefix" /> --%>
    <%@ include file="nav.jsp"%>
	<%@ include file="../include/pagination.jsp"%>

	<form id="bulkSelect">
		<display:table name="routes" class="footable" requestURI="" id="route" export="false">
			
			<display:column  title="Name" sortable="true">${route.routeName}</display:column>
			<display:column  title="Total Amount" sortable="true">${route.totalAmount}</display:column>
			<display:column  title="Total Pending Dues" sortable="true">${route.totalPendingDues}</display:column>
			
			
			<display:column title="Actions" sortable="false">
			<a class="btn btn-info" href="<c:url value="/admin/payment/showCustomers/${route.id}"/>">View Customers</a>
				<%-- <div class="btn-group">
					<a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
						href="#"> Action <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- dropdown menu links -->
						<li><a href="<c:url value="/admin/payment/showCustomers/${route.id}"/>">View Customers</a>
						</li>
					</ul>
				</div> --%>
			</display:column>
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete routes?</h3>
		</div>
		<div class="modal-body">
			<p>The selected route will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>