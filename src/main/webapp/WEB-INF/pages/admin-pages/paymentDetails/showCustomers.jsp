<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Payment Details</title>

</head>

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Payment Details</h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="${homeUrl}">Home</a>
	            </li>
	            <li class="active">
	                <strong>Customers</strong>
	            </li>
	        </ol>
	    </div>
	</div>

	<c:url var="urlBase" value="/admin/payment/showCustomers/${routeId}" />
	<c:set var="urlPrefix" value="/admin/payment/showCustomers/${routeId}?searchTerm=${searchTerm}&pageNumber=" scope="page" />
	<%-- <c:url value="/admin/route/show" var="routeDetailLinkPrefix" /> --%>
     <%@ include file="showCustomerNav.jsp"%>
	 <%@ include file="../include/pagination.jsp"%> 

	<form id="bulkSelect">
		<display:table name="customers" class="footable" requestURI="" id="user" export="false">
			
			<display:column title="Code" sortable="true">${user.code}</a></display:column>

			<display:column title="Name" sortable="true">${user.firstName} ${user.lastName}</display:column>
			<display:column title="Mobile Number" sortable="true">${user.mobileNumber}</display:column>
			<display:column title="Building Address" sortable="true">${user.buildingAddress}</display:column>
			<display:column title="Pending Dues" sortable="true">${user.lastPendingDues}</display:column>
			<display:column title="RouteAssign" sortable="true">
				<c:choose>
					<c:when test="${user.isRouteAssigned}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Status"  sortable="true">${user.status}</display:column>
			
		</display:table>
	</form>

	<!-- modal -->
	<div class="modal hide" id="ConfirmMultiDelete" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3 id="myModalLabel">Delete routes?</h3>
		</div>
		<div class="modal-body">
			<p>The selected route will be permanently deleted. This action
				cannot be undone.</p>
			<p>Are you sure?</p>
		</div>
		<div class="modal-footer">
			<a class="btn btn-primary" type="button"
				href="javascript:bulkDelete()">Yes, Delete</a>
			<button class="btn" data-dismiss="modal">No, Cancel</button>
		</div>
	</div>

	<!-- <div id="dialog-form" class="modal hide">
</div> -->

</body>
</html>