<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Email</title>
</head>
<script type="text/javascript">
$(document).ready(function() {
	$('#imageBorder').hide();
});

function submitForm(){
	if(validate()){
		$("#sendEmails").submit();
	}
}

function validate(){
	
	if($("#subject").val().trim() == "") {
		showToast('warning','Please enter subject line.');
		return false;
	}
	if($("#emailBody").val().trim()=="" && $("#mandrillTemplate").val().trim() == ""){
		showToast('warning','Please enter either a message or mandrill template name.');
		return false;
	}
	if($("#emailsExcel").val().trim() == "") {
		showToast('warning','Please select valid excel file.');
		return false;
	}
	return true;
}

function validateFileUpload(){
	
	var fuData=document.getElementById("attachementFile");
	var fileUploadPath = fuData.value;
	
	if(fileUploadPath=""){
		alert("Please Upload an Image");
	}else if(fuData.files&&fuData.files[0]){
		var reader =new FileReader();
		reader.onload = function(e){
			$('#displayImage').attr('src',e.target.result);
			$('#imageBorder').show();
		}
		reader.readAsDataURL(fuData.files[0]);
	}
}
</script>
<c:url value="/admin/email/compose" var="sendEmail" />
<body>
 <h2>Provilac Email</h2>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login"/>
	</sec:authorize>
	
	<form id="sendEmails" method="post" action="${sendEmail}" enctype="multipart/form-data">
		<div>
		</div>
		<br>
		<div class=" input-group">
			<span class="input-group-addon"><b> Subject : </b></span>
			<input name="subject" class="col-md-12 form-control" id="subject" placeholder="subject"/>
		</div>
		<br>
		<div class=" input-group">
			<span class="input-group-addon"><b> Template Name : </b></span>
			<input name="mandrillTemplate" class="col-md-12 form-control" id="mandrillTemplate" placeholder="Template Name"/>
		</div>
		<br>
		<div class="panel panel-info" id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Message *</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<textarea name="emailBody" class="form-control" id="emailBody" rows="11" style="width:90%" placeholder="Type a message..." required></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title "> Image </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
				<fieldset>
					<div class="input-group width-xlarge">
						<div class="row">
							<input name="attachmentFile" class="form-control imageClass" onChange="validateFileUpload()" id="attachmentFile" type="file" accept="image/*"/>
						</div>
					</div>
					<br>
					<div class="col-md-8" id="imageBorder">
						<img id="displayImage" border="0" height="200" width="300" >
					</div>
				</fieldset>
		   </div>	
		</div>
		<div class="panel panel-info"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title "> Emails Excel </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
				<fieldset>
					<div class="input-group width-xlarge">
						<div class="row">
							<input name="emailsExcel" class="form-control" id="emailsExcel" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" required="required"/>
						</div>
					</div>
				</fieldset>
		   </div>	
		</div>
		<a class="btn btn-info" href="javascript:submitForm();">Send</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-danger" href="javascript:history.back(1)">Cancel </a>
	</form>
</body>