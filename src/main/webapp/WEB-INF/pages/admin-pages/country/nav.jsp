<%@ include file="/taglibs.jsp"%>
<c:url value="/admin/country/add" var="countryAdd" />
<c:url value="/admin/country/list" var="countryList" />
<c:url value="/admin/country/import" var="countryImport" />
<body>

<div class="row  border-bottom white-bg dashboard-header">
	<div class="span4">
	<sec:authorize access="hasAnyAuthority('ROLE_SUPER_ADMIN')">
	<a class="btn btn-primary" href="${countryAdd}">Add New Country</a>&nbsp;
<%-- 	<a class="btn btn-primary" href="${countryImport}">Import Categories</a>&nbsp; --%>
	</sec:authorize>
	<form class="form-inline pull-right" action="${urlBase}" action="get" role="form">
	  <input type="text" class="form-control width-xlarge" name="searchTerm" id="searchTerm" placeholder="search">
	  <button type="submit" class="btn btn-success">Search</button>
	  <a class="btn btn-info" href="${urlBase}">View All</a>
	</form>
	</div>
	
</div>