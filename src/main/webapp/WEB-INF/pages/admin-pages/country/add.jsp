<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Add Country</title>
<script type="text/javascript">
	$(document).ready(function() {
	});
	
	function submitForm() {
		if (validate())
			$('#addCountry').submit();
	}
	
	function validate()
	{
		
		var country = $('#name').val().trim(), chardig= /^[a-zA-Z ]*$/;
		
		if(!chardig.test(country))
			{
			showToast('warning','Please enter Valid Country in Characters');
			return false;
			}
		else if(!country)
			{
			showToast('warning','Please enter Valid Country');
			return false;
			}
		return true;
		
	}

</script>
</head>
<c:url value="/admin/country/add" var="countryAdd" />
<c:url value="/admin/country/list" var="countryList" />

<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<c:url value="/admin/country/add" var="addCountry" />

	<form:form id="addCountry" action="${countryAdd}" method="post"
		modelAttribute="country" enctype="application/x-www-form-urlencoded">
		<div>
			<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>
			</c:if>
			<s:bind path="*">
				<c:if test="${status.error}">
					<div id="message" class="text text-danger">Form has errors</div>
				</c:if>
			</s:bind>
		</div>

		<div class="panel panel-info servicePanel"  id="primary-info">
			<div class="panel-heading clearfix">
				<h3 class="panel-title ">Country </h3>
			</div>
			<div class="panel-body" style="margin-left: 10px;margin-right:20px;">
			<fieldset>

			<form:errors path="name" cssClass="text-danger" />
			<div class="input-group width-xlarge">
				<span class="input-group-addon"> Name *</span>
				<form:input path="name" id ="name" cssClass="form-control" />
			</div>
			<br>

		</fieldset>
		</div>	
		</div>
		<span>Fields marked with * are mandatory</span>
		<br />
		<br />
		 <a class="btn btn-info" href="javascript:submitForm()">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-danger" href="javascript:history.back(1)">Cancel</a>
	</form:form>
</body>