<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Provilac - Payment</title>
<script type="text/javascript">
$(document).ready(function() {
	
});

</script>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Deposite Collection- Details</h3>
		</div>
	</div>
	<div class="panel panel-primary" id="primary-info">
		<div class="panel-heading">
			<h3 class="panel-title">Primary Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">collectionBoy</label>
				<p class="col-md-2">${deposit.collectionBoy.firstName}-${deposit.collectionBoy.lastName} - ${deposit.collectionBoy.mobileNumber}</p>
				
				<label class="col-md-2">Date</label>
				<p class="col-md-2">
				<fmt:formatDate value="${deposit.date}" pattern="dd-MM-yyy hh:mm:ss a" /></p>
			</div>
			
			<div class="row">
				<label class="col-md-2">Total Amount</label>
				<p class="col-md-2">${deposit.totalAmount}</p>
				<label class="col-md-2">cash Amount </label>
				<p class="col-md-2">${deposit.cashAmount}</p>
				<label class="col-md-2">cheque Amount </label>
				<p class="col-md-2">${deposit.chequeAmount}</p>
			</div>
			
			<div class="row">
				<label class="col-md-2">noOfCheques</label>
				<p class="col-md-2">${deposit.noOfCheques}</p>
				<label class="col-md-2">cashTransactionId</label>
				<p class="col-md-2">${deposit.cashTransactionId}</p>
			</div>
			
		</div>
	</div>


	<div class="panel panel-info" id="base-info">
		<div class="panel-heading">
			<h3 class="panel-title">Base Info</h3>
		</div>
		<div class="panel-body" style="margin-left: 10px;">
			<div class="row">
				<label class="col-md-2">Created By</label>
				<p class="col-md-4">${deposit.createdBy}</p>
				<label class="col-md-2">Created Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${deposit.createdDate}"  />
				</p>
			</div>
			<div class="row">
				<label class="col-md-2">Last Modified By</label>
				<p class="col-md-4">${deposit.lastModifiedBy}</p>
				<label class="col-md-2">Last Modified Date</label>
				<p class="col-md-2">
					<fmt:formatDate value="${deposit.modifiedDate}"  />
				</p>
			</div>
		</div>
	</div>
	
	 <a class="btn btn-success" href="javascript:history.back(1)">Ok</a>
	
</body>
</html>