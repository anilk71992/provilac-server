<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>Provilac - Account Profile</title>
</head>
<c:url value="/admin/collection/show" var="collectionDetailLinkPrefix" />
<c:url value="/admin/deposit/show" var="depositDetailLinkPrefix" />
<body>
	<sec:authorize access="isAnonymous()">
		<c:redirect url="/login" />
	</sec:authorize>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Account Profile</h3>
			<c:if test="${not empty totalDues}">
				<h4>Total Collection Pending : - ${totalDues}</h4>
			</c:if>
			
			<div class="">
				<label class="col-sm-1 control-label" style="padding-left: 0%;">Select</label>
				<div class="col-sm-2" style="padding-left: 0%;">
					<form id="collectionBoyForm" action="<c:url value="/admin/deposit/accountProfile"/>" method="post">
						<select class="form-control m-b" name="code" id="collectionBoyCode" onchange="javascript:getCollectionBoyDetails();">
	                         <option value="none">Select Collection Boy</option>
	                         <c:forEach items="${collectionBoys}" var="collectionBoy">
	                         	<option value="${collectionBoy.code}" <c:if test="${collectionBoyCode==collectionBoy.code}">selected='selected'</c:if> >${collectionBoy.firstName} ${collectionBoy.lastName}</option>
	                         </c:forEach>
	                    </select>
                    </form>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="collectionDiv">
		<c:if test="${not empty collections}">
			<div class="col-md-6">
				<h5>Collection Details</h5>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th data-class="expand"><u>Code</u></th>
								<th><u>Date</u></th>
								<th><u>Cash Amount</u></th>
								<th><u>Cheque Amount</u></th>
								<th><u>No. of Cheques</u></th>
								<th><u>Total Amount</u></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${collections}" var="collection">
								<tr>
									<td><a href="${collectionDetailLinkPrefix}/${collection.cipher}">${collection.code}</a></td>
									<td>${collection.date}</td>
									<td>${collection.cashAmount}</td>
									<td>${collection.chequeAmount}</td>
									<td>${collection.noOfCheques}</td>
									<td>${collection.totalAmount}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</div>
		</c:if>
		<c:if test="${not empty deposits}">
			<div class="col-md-6">
				<fieldset>
					<h5>Deposit Details</h5>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th><u>Code</u></th>
								<th><u>Date</u></th>
								<th><u>Cash Amount</u></th>
								<th><u>Cheque Amount</u></th>
								<th><u>No. of Cheques</u></th>
								<th><u>Total Amount</u></th>
								<th><u>Action</u></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${deposits}" var="deposit">
								<tr>
									<td><a href="${depositDetailLinkPrefix}/${deposit.cipher}">${deposit.code}</a></td>
									<td>${deposit.date}</td>
									<td>${deposit.cashAmount}</td>
									<td>${deposit.chequeAmount}</td>
									<td>${deposit.noOfCheques}</td>
									<td>${deposit.totalAmount}</td>
									<td>${deposit.date}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</fieldset>
			</div>
		</c:if>
	</div>
<a class="btn btn-danger" href="javascript:history.back(1)">OK</a>
<script type="text/javascript">

function getCollectionBoyDetails(){
	/* if($("#collectionBoyCode").val() == "none"){
		$('#collectionDiv').html("");
		return;
		
	} */
	$("#collectionBoyForm").submit();
}
</script>
</body>
</html>