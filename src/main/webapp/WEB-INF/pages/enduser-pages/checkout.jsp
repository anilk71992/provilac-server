<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Checkout</title>
      
      <!-- Include Date Range Picker -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			var date_input=$('input[name="date"]'); //our date input has the name "date"
			var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			$('#date').datepicker({
				format: 'dd-mm-yyyy',
				container: container,
				todayHighlight: true,
				autoclose: true
			});
		});
	</script>
   </head>
   <body class="home-page" id="top">
   
   <sec:authorize access="isAnonymous()">
   	   <c:redirect url="/index" />
   </sec:authorize>
	
    <c:set value="0" var="subTotal" />
    <c:url value="/ForwardOTPaymentRequest" var="ForwardOTPaymentRequestUrl" />
    <c:url value="/createAddress" var="createAddressUrl" />
    <c:url value="/address/delete/" var="deleteAddressUrl" />
    <c:url value="/updateAddress" var="updateAddressUrl" />
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
               <form action="${ForwardOTPaymentRequestUrl}" id="paymentForm" class="js-customer-form" method="post">
                  <div class="cart-left-panel">
                  	<div>
						<c:if test="${not empty message}">
							<center><div id="message" class="success">${message}</div></center>
						</c:if>			
					</div>
					<input class="form-control" id="today" value="${today}" type="hidden"/>
                     <div class="cart-left">
                         <div class="address-head">
                           <h2>Delivery Date</h2>
                        </div>
                            <div class="form-group date-selection">
                              <div class="col-xs-8 col-sm-6 col-md-4">
                               <div class="input-group">
                                <div class="input-group-addon">
                                 <i class="fa fa-calendar">
                                 </i>
                                </div>
                                <input class="form-control" id="date" name="date" placeholder="dd-MM-yyyy" type="text"/>
                               </div>
                              </div>
                             </div>
                        </div>
                     <hr>
                     <div class="cart-left">
                        <div class="address-head">
                           <h2>My Addresses</h2>
                        </div>
                        <div class="row">
                           <c:forEach items="${addresss}" var="address" varStatus="loop">	
                           <div class="col-md-6" onclick="javascript:setSessionForAddress('${address.code}');" >
                           <input type="hidden" id="selectedAddress_${address.code}" value="${address.code}" />
                              <div class="addresses">
                                 <div class="delivery-address">
                                    <p>${address.addressLine1} ${address.addressLine2} ${address.landmark} ${address.locality} ${address.pincode}</p>
                                    <div class="address-footer">
                                       <a class="edit-address" href="${updateAddressUrl}/${address.id}"><i class="fa fa-pencil"></i>Edit</a>
                                       <a class="delete-address" href="${deleteAddressUrl}/${address.id}"><i class="fa fa-trash"></i>Delete</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </c:forEach>
                        </div>
                        <div class="new-address-add">
                           <a href="${createAddressUrl}">Add New Address +</a>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-left" >
                        <div class="address-head">
                           <h2>Payment</h2>
                        </div>
                        <div class="payment-options">
                           <div class="form-group">
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios1"> <img src="<c:url value='/assets/endUser-assets/img/paytm.png'/>" alt="paypal" for="optionsRadios1" style="margin-bottom: 10px;"/></label>
                              </div>
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios2" checked value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios2">DEBIT CARD/CREDIT CARD/NET BANKING/UPI</label>
                              </div>
                           </div>
                        </div>
                        <div class="checkout-btn">
	                    	<a href="javascript:submitForm();">Make Payment</a>
	                    </div>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="col-md-4">
                  <div class="cart-right-panel">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Summary</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                     	<c:forEach items="${sessionScope.cartItems}" var="item" varStatus="loopVar">
	                        <div class="cart-summary">
	                           <span class="product-name">${item.product.name} x <span style="font-weight: bold;" id="checkoutProductQty_${item.product.code}">${item.quantity}</span></span>
	                           <span class="product-prices" id="checkoutProductPrice_${item.product.code}"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.product.price * item.quantity}"/></span>
	                           <c:set value="${(item.product.price * item.quantity) + subTotal}" var="subTotal"></c:set>
	                           <input id="cartItemUnitPrice_${item.product.code}" type="hidden" value="${item.product.price}">
	                        </div>
                        </c:forEach>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="cart-summary">
                           <span class="product-name">Final Amount</span>
                          <span class="product-prices" id="checkoutFinalAmount" ><i class="fa fa-inr"></i> ${subTotal}</span>
                        </div>
                     </div>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript">
      
      var selectedDeliveryAddress = false;
      
      function setSessionForAddress(addressCode){
    	  var shippingOrdersAddress = $('#selectedAddress_' + addressCode).val();
    	  
    		$.ajax({
	    		url: '<c:url value="/end-user/shippingOrdersAddress"/>',
	    		dataType: 'json',
	    		data : 'shippingOrdersAddress=' + shippingOrdersAddress,
	    		success: function(response) {
	    			if (null != response && response.success) {	
	    				selectedDeliveryAddress = true;
	    			}else{
	    				showToast('error','Error Occured!');
	    			} 
	    		}
	    	});
      }
      
      function submitForm() {
			
    	  if( !$('#date').val()|| $('#date').val()==""){
    		  toastr.warning('Select delivery date.');
	   	      ($("#date")).focus();
	   	      return;
	   	  }
    	  
    	  var currentDate = moment($('#today').val(), ["DD-MM-YYYY"]).format("YYYY-MM-DD");
    	  var orderDate = moment($('#date').val(), ["DD-MM-YYYY"]).format("YYYY-MM-DD");
    	  if(moment(currentDate).isAfter(orderDate) || currentDate == orderDate){
	   		  toastr.warning('Please select Delivery Date later than current date');
	  	      ($("#date")).focus();
	  	      return;
	  	  }
	   		
    	  if(selectedDeliveryAddress == false){
    		  toastr.warning('Select one address.');
    		  return;
    	  }
		  $('#paymentForm').submit();		
		}
      
      </script>
      
   </body>
</html>