<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Summary Of Existing Subscription</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="subscription-bill">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Your Current Subscription Pattern</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <c:forEach items="${subscriptionItems}" var="item">
                     	<div class="cart-summary">
                           <span class="sub-product-name">${item.product.name} ${item.quantity} ${item.type} </span>
                        </div>	
                     </c:forEach>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="sub-checkout-btn">
                            <a href='<c:url value="/end-user/extend-prepaid/subscription-duration" />'>Continue Same Pattern</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>