<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Subscription Payment Success</title>
      <script type="text/javascript">
      function mailRecieptAsAjax() {
			
			$.ajax({
				url : '<c:url value="/end-user/extend-prepaid/mail-prepaid"/>',
				dataType : 'json',
				data : 'companyAddress=' + "",
				success : function(response) {
					if (null != response && response.success) {
						$('#myModal').modal('show');					
					} else {
						toastr.warning('Your receipt did not send.');
					}
				}
			});
		}
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container" id="myDiv">
            <div class="row">
            <div class="col-md-12">
                  <div class="cart-right-panel1">
                     <div class="cart-right1">
                         <div class="cart-head1">
                            <img src="<c:url value='/assets/endUser-assets/img/success.png'/>">
                            <h3>Order Confirmed</h3>
                         </div>
                         <div class="success-details">
                         	<p><b>Payment Id : </b> ${txnid}</p>
                            <p><b>Payment Method : </b>${paymentMethod}</p>
                            <p><b>Amount Paid : </b><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${amount}"/></p>
                         </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <c:forEach items="${subscriptionItems}" var="item">
	                        <div class="cart-summary">
	                           <span class="sub-product-name">${item.product.name} ${item.totalQuantity} bottles x <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.productPrice}"/></span>
	                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.totalQuantity * item.productPrice}"/></span>
	                        </div>
                        </c:forEach>
                     </div>
                     <hr>
                    <div class="cart-right">
                        <div class="cart-summary">
                           <span class="product-name">Final Amount</span>
                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${amount}"/></span>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right" id="button-section">
                        <div class="checkout-btn">
                           <a href="javascript:print()">Print</a>
                             <a href="javascript:mailRecieptAsAjax()">Send Mail</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /row -->
            </div>
         </div>
         <!-- /container -->
         <!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Receipt Sent</h4>
		        </div>
		        <div class="modal-body">
		          <p>Your Invoice/Receipt has been sent to your mail id.</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
      </section>
   </body>
</html>