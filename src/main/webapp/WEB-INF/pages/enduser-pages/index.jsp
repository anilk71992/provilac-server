<%@ include file="/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

</head>
<body id="index" class="lang-en country-us currency-usd layout-full-width page-index tax-display-disabled ">
	<c:url value="/index" var="indexUrl" />
<section id="section1">
   <div class="container">
      <div class="row">
         <div class="col-sm-8 col-xs-12 tar h100vh">
            <div class="abs-vert-center">
               <h3 class="ttu">As Fresh<br class="hidden-xs" />
                  as nature intended it to be
               </h3>
               <p class="maw400 dib">At Provilac, we&rsquo;re all about The Good Stuff. And we believe that can&rsquo;t get any better than nature itself. Which is why we are committed towards providing you with completely pure, healthy, organic, nutritious, untouched &amp; unadulterated milk. Just the way nature intended it to be.<span style="color:#B22222;"> </span></p>
            </div>
         </div>
         <div class="col-sm-4 col-xs-12 h100vh tac-xs"><img alt="" class="img-reponsive abs-vert-center" src="<c:url value='/assets/endUser-assets/img/bottle-1.png'/>" /></div>
      </div>
      <!-- /row -->
   </div>
   <!-- /container -->
</section>
<!-- /#slide-1 -->
<section id="section2">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6 col-xs-12 hidden-xs p0 hero-image"></div>
         <div class="col-sm-6 col-xs-12 maw485 h100vh">
            <div class="abs-vert-center">
               <h3>The No Nonsense Milk</h3>
               <p>Farm fresh gets a whole new meaning at Provilac. Our state-of-the-art farm is designed to provide an optimum environment for our cows and we automatically monitor their well-being every second. Moreover, the fully automated, highly advanced &amp; scientific infrastructure at our farm ensures that every drop of Provilac cow milk is untouched to retain all of its nutritious qualities &amp; more importantly is free of any added hormones, preservatives &amp; GMOs. So go ahead and take a sip of Goodness!&nbsp;<span style="color:#FFFFFF;"><span class="marker"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:8px;">Pure Cow milk in Pune Cow Milk in Mumbai Desi milk Pune Desi milk Mumbai A2 milk Pune A2 milk Mumbai Home delivery of milk pure milk in Pune pure milk in Mumbai organic milk Indian cow milk Gir cow Jersey cow pride of cows best milk farm fresh milk doorstep delivery pure desi cow</span></span></span></span></p>
            </div>
         </div>
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container -->
</section>
<!-- /slide-2 -->
<section id="section3">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 col-xs-12 tar h100vh">
            <div class="text abs-vert-center">
               <h3>Served Fresh!</h3>
               <p class="maw270 dib">We really do go that extra mile to ensure that our milk is as pure as it gets, but what&rsquo;s the point of all that work if you can&rsquo;t savour the freshness of our milk!? So we ensure that our bottled milk is delivered at your doorstep within 12 hours of milking! That&rsquo;s as fresh as it gets. So go ahead and take a sip of Goodness!</p>
            </div>
         </div>
      </div>
      <!-- /row -->
   </div>
   <!-- /container -->
</section>
<!-- /slide-3 -->
</body>
</html>