<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Payment Success</title>
      <script type="text/javascript">
      function mailRecieptAsAjax() {
			
			$.ajax({
				url : '<c:url value="/end-user/oneTimeOrder/sent-invoice-mail"/>',
				dataType : 'json',
				data : 'companyAddress=' + "",
				success : function(response) {
					if (null != response && response.success) {
						$('#myModal').modal('show');					
					} else {
						toastr.warning('Your receipt did not send.');
					}
				}
			});
		}
      </script>
   </head>
   <body class="home-page" id="top">
   <sec:authorize access="isAnonymous()">
   	   <c:redirect url="/index" />
   </sec:authorize>
      <section id="cart-details">
         <div class="container">
            <div class="row">
            <div class="col-md-12">
                  <div class="cart-right-panel1">
                     <div class="cart-right1">
                        <div class="cart-head1">
                            <img src="<c:url value='/assets/endUser-assets/img/success.png'/>">
                           <h3>Order Confirm</h3>
                        </div>
                         <div class="success-details">
                         	 <p><b>Payment Id: </b> ${oneTimeOrder.txnId}</p>
                             <p><b>Payment Method: </b>${oneTimeOrder.paymentGateway}</p>
                             <p><b>Amount Paid: </b><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${oneTimeOrder.finalAmount}"/></p>
                             <p><b>Order Date: </b><fmt:formatDate value="${oneTimeOrder.createdDate}" pattern="dd-MM-yyy hh:mm:ss a" /></p>
                             <p><b>Delivery Date: </b><fmt:formatDate value="${oneTimeOrder.date}" pattern="dd-MM-yyy" /></p>
                         </div>
                     </div>
                     <hr>
                     <div class="cart-right">
	                     <c:forEach items="${orderLineItems}" var="item" varStatus="loopVar">
	                        <div class="cart-summary">
	                           <span class="product-name">${item.product.name} x ${item.quantity}</span>
	                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.amount}"/></span>
	                        </div>
	                     </c:forEach>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="cart-summary">
                           <span class="product-name">Final Amount</span>
                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${oneTimeOrder.finalAmount}"/></span>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right" id="button-section">
                        <div class="checkout-btn">
                           <a href="javascript:print()">Print</a>
                             <a href="javascript:mailRecieptAsAjax()">Send Mail</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Invoice/Receipt Sent</h4>
		        </div>
		        <div class="modal-body">
		          <p>Your Invoice/Receipt has been sent to your mail id.</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
      </section>
   </body>
</html>