<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Address</title>
      <script type="text/javascript">
	      $(document).ready(function() {
	    	  $('#addAddress').submit(function() {
	  			$('#addAddress').find(':input[type=submit]').prop('disabled', true);
	  			if (validate()) {				
	  				$('#addAddress').submit();	
	  				return true;
	  			}	
	  			$('#addAddress').find(':input[type=submit]').prop('disabled', false);
	  			return false;
	  			});	
	      });
	      
	      function validate() {
	    	  
	    	  if($('#pincode').val() == "") {
	    		  $('#pincode').focus();
	    		  toastr.warning('Please enter zip code.');  
	  			  return false;
	  		  }
	    	  var zipcodePattern = /^[0-9]{6}$/;
	    	  
	    	  if(!zipcodePattern.test($('#pincode').val())) {
 					$('#pincode').focus();
 					toastr.warning('Please enter valid zip code');
 					return;
 			  } 
	    	  
	    	  if($('#addressLine1').val() == "") {
	    		  $('#addressLine1').focus();
	    		 	toastr.warning('Please enter building*.');  
	  			  return false;
	  		  }
	    	  
	    	  if($('#locality').val() == "") {
	    		  $('#locality').focus();
	    		  toastr.warning('Please enter street/locality');  
	  			  return false;
	  		  }
	    	  
	    	  return true;
	      }
      
      </script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeOg8--kjI8V34MUkm4bzGLOtaAhfPBVE&callback=initMap"></script>
      <script async defer type="text/javascript" src="test.js"></script>
      <style type="text/css">
		#map_canvas {         
		    height: 500px;         
		    width: 1250px;         
		    margin: 0.6em;       
		}
	</style>
   </head>
   <body class="home-page" id="top">
   <sec:authorize access="isAnonymous()">
   	   <c:redirect url="/index" />
   </sec:authorize>
   <c:url value="/createAddress" var="submitAddress" />
      <section id="address-section">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-sm-12 col-xs-12">
                  <div class="address-page">
                     <div class="address-form">
                        <c:if test="${not empty message}">
					   		 <div style="color: #ef6767;" class="success"><center>${message}</center></div><br>
						</c:if>
                        <h2 style="color: #7d7878;">Add Address</h2>
                        <form action="${submitAddress}?retUrl=${retUrl}" id="addAddress" class="login-form" method="Post" >
                           <input type="text" name="pincode" id="pincode" placeholder="Zip Code*" maxlength="6"/>
                           <input type="text" name="addressLine2" placeholder="Flat/House No"/>
                           <input type="text" id="addressLine1" name="addressLine1" placeholder="Building*"/>
                           <input type="text" name="landmark" id="landmark" onchange="findLocation();" placeholder="Landmark"/>
                           <input type="text" name="locality" id="locality" onchange="findLocation();" placeholder="Street/locality*"/>
                           
                           <input type="hidden" name="lat" id="lat" />
                           <input type="hidden" name="lng" id="lng" />
                           <button class="submit" type="submit">Add Address</button>
                        </form>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-12 col-xs-12">
                  <div class="address-page">
                     <div id="map" style="width:100%;height:500px;"></div>
                     <script type="text/javascript">
                        	var lat = 18.5203, 
                    		lng = 73.8567, 
                    		latlng = new google.maps.LatLng(lat, lng),
                    		image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';
                        	
                        	var geocoder = new google.maps.Geocoder();
                        	var directionsService = new google.maps.DirectionsService;

                        	var directionsDisplay = new google.maps.DirectionsRenderer({
                        		suppressMarkers: true
                        	});
                        	
                          	var map = new google.maps.Map(document.getElementById('map'), {
                      		center : new google.maps.LatLng(lat, lng),
                      		zoom : 15,
                      		});
                          	
                          	var marker = new google.maps.Marker({
                      		position : latlng,
                      		map : map,
                      		icon : image,
                      		draggable : true
                      		});
                          	
                          	directionsDisplay.setMap(map);
                          	
                          	google.maps.event.addListener(marker, 'position_changed', function() {
                        		var lat = marker.getPosition().lat();
                        		var lng = marker.getPosition().lng();
                        		
                        		geocodePosition(marker.getPosition());
                        		$('#lat').val(lat);
                        		$('#lng').val(lng);
                        	});
                          	
                          	function geocodePosition(pos) {
                          	  geocoder.geocode({
                          	    latLng: pos
                          	  }, function(responses) {
                          	    if (responses && responses.length > 0) {
                          	    	$("#searchTextField").val(responses[0].formatted_address);
                          	    }
                          	  });
                          	}
                        
                    	function findLocation(){
                    		if ($('#locality').val()=="" ||!$('#locality').val()) {
                    			toastr.warning("Please Enter locality");
                    			$('#lat').val("");
                    			$('#lng').val("");
                    			marker.setMap(null);
                    			return false;
                    		}
                    		
                    	 var user1Location = $('#locality').val();
                    	  geocoder.geocode( { 'address': user1Location}, function(results, status) {
                    	      if (status == 'OK') {
                    	         marker.setMap(map);
                    		        map.setCenter(results[0].geometry.location);
                    		        marker.setPosition(results[0].geometry.location);
                    		    	 $('#lat').val(results[0].geometry.location.lat());
                    		         $('#lng').val(results[0].geometry.location.lng());
                    		         map.setZoom(15);
                    	      }else{
                    	    	  toastr.warning("Lattitude and Logitude for entered position are not found, please select nearest location");
                    	           	$('#lat').val("");
                    	    		$('#lng').val("");
                    	    		marker.setMap(null);
                    	    		return false;
                    	      }
                    	      
                    	  });
                    	  
                    	}
                    	
                     </script>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>