<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Payment Failed</title>
   </head>
   <body class="home-page" id="top">
   <sec:authorize access="isAnonymous()">
   	   <c:redirect url="/index" />
   </sec:authorize>
   <c:set value="0" var="subTotal"></c:set>
      <section id="cart-details">
         <div class="container">
            <div class="row">
            <div class="col-md-12">
                  <div class="cart-right-panel1">
                     <div class="cart-right1">
                        <div class="cart-head1">
                            <img src="<c:url value="/assets/custom-icons/error.png"/>">
                            <p class="lead">Payment failed. Something went wrong. Please try again later.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>
