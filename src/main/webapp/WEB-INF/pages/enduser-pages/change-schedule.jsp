<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Change Schedule</title>
      <script type="text/javascript">
      	  var deliveryScheduleCipher = '${deliveryScheduleCipher}';	
         
	      function incrementQty(productCode) {
	  		  var currentQty = parseInt($('#qty_'+productCode).val());
	  		  if(currentQty > 3)
	  			  return;
		  	  var qty = currentQty + 1;
		  	  ajaxCallForChangeQty(productCode, qty);
	  	  }
	      
	      function decrementQty(productCode) {
	    	  var currentQty = parseInt($('#qty_'+productCode).val());
		  	  if (currentQty <= 1) {
		  		return;
		  	  }
		  	  var qty = currentQty - 1;
		  	  ajaxCallForChangeQty(productCode, qty);
	  	  }
	      
	      function ajaxCallForChangeQty(productCode, qty){
	    	  
	    	  $.ajax({
		    		url: '<c:url value="/restapi/changeQtyOfRelatedProduct"/>',
		    		dataType: 'json',
		    		data : 'productCode=' + productCode + '&qty=' + qty,
		    		success: function(response) {
		    			if (null != response && response.success) {			
		    				$('#qty_'+productCode).val(qty);
		    			}else{
		    				showToast('error','Error Occured!');
		    			} 
		    		}
		    	});
	      }
	      
	      function submitForm(){
	    	  $('<input type="hidden" name="deliveryScheduleCipher" value="' + deliveryScheduleCipher + '"/>').appendTo('#patternForm');
	    	  $('#changeScheduleForm').submit();
	      }
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="cart-change-schedule-panel">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>Change Schedule of <fmt:formatDate value="${deliverySchedule.date}" pattern="dd MMM" /></h3>
                        </div>
                     </div>
                     <hr>
                     <c:forEach items="${setOfdeliveryLineItems}" var="deliveryLineItem">
                     	<div class="cart-left">
                        <div class="cart-product-summary">
                           <div class="row">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="products-name">
                                    <span>${deliveryLineItem.product.name}</span>
                                 </div>
                              </div>
                              <div class="col-md-6 col-xs-12">
                                 <div class="row">
                                    <div class="col-md-8 col-xs-6">
                                       <div class="input-group btn-incre1">
                                          <span class="input-group-btn">
                                          <button type="button" class="quantity-left-minus btn btn-number btn-pm" onclick="decrementQty('${deliveryLineItem.product.code}')" data-type="minus" data-field="">
                                          <span class="fa fa-minus"></span>
                                          </button>
                                          </span>
                                          <input type="text" id="qty_${deliveryLineItem.product.code}" name="quantity" class="form-control input-number" value="${deliveryLineItem.quantity}">
                                          <span class="input-group-btn">
                                          <button type="button" class="quantity-right-plus btn btn-number btn-pm" onclick="incrementQty('${deliveryLineItem.product.code}')" data-type="plus" data-field="">
                                          <span class="fa fa-plus"></span>
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <hr>
                     </c:forEach>
                     <div class="cart-left">
                        <div class="checkout-btn">
                        	<c:url value="/end-user/deliverySchedule/update" var="updateScheduleUrl" />
                           <a href="${updateScheduleUrl}?deliveryScheduleCipher=${deliveryScheduleCipher}">ok</a>
                           <a href='<c:url value="/end-user/welcome" />'>Cancel</a>
                           <%-- <form action='<c:url value="/end-user/deliverySchedule/update"/>' id="changeScheduleForm"></form> --%>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /row -->
            </div>
         </div>
         <!-- /container -->
      </section>
   </body>
</html>