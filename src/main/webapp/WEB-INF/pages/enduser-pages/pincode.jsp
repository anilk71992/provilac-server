<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Provilac - Pure Cow milk in Pune Cow Milk in Mumbai Desi milk Pune Desi milk Mumbai A2 milk Pune A2 milk Mumbai Home delivery of milk pure milk in Pune pure milk in Mumbai organic milk Indian cow milk Gir cow Jersey cow pride of cows best milk farm fresh milk doorstep delivery pure desi cow</title>
   </head>
   <body class="home-page" id="top">
   <c:url value="/pincode" var="pincodePostUrl" />
      <section id="section1">
         <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="login-page">
                      <div class="pincode-form">
                        <form class="pin-form" id="pincode-form" action="${pincodePostUrl}?retUrl=${retUrl}" method="post">
                            <h2>Pincode</h2>
                            <p>Please enter your area pincode</p>
                          	<input type="text" placeholder="Enter Pincode"/>
                          	<button class="submit" data-link-action="sign-in" type="submit">Submit</button>
                        </form>
                        <p class="note"><strong>Please Note:</strong> We are currently delivering only in Mumbai & Pune.</p>
                      </div>
                    </div>
                  </div>
            </div>
            <!-- /row -->
         </div>
         <!-- /container -->
      </section>
   </body>
</html>