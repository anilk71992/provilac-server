<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Subscription Checkout</title>
      <script type="text/javascript">
		var selectedAddress;
		
		function onAddressSelected(cipher) {
			selectedAddress = cipher;
		}
		
		function submitForm() {
			
			if( !$('#date').val()|| $('#date').val()==""){
				toastr.warning("Please choose from date !");
		       	($("#date")).focus();
		        return;
			}
			
			var currentDate = moment($('#today').val());
    	  	var orderDate = moment($('#date').val());
    	  	if(moment(currentDate).isAfter(orderDate) || currentDate == orderDate){
	   		  	toastr.warning('Please select Delivery Date later than current date');
	  	      	($("#date")).focus();
	  	      	return;
	  	    }
    	  	
			if(selectedAddress == null || selectedAddress == '') {
				toastr.warning('Please select delivery address');
				return;
			}
			
			$('<input type="hidden" name="deliveryFromDate" value="' + $('#date').val() + '"/>').appendTo('#paymentForm');
			$('<input type="hidden" name="addressCipher" value="' + selectedAddress + '"/>').appendTo('#paymentForm');
			$('#paymentForm').submit();		
		}
      	
      </script>
   </head>
   <body class="home-page" id="top">
   	<c:set value="/postpay-subscription-checkout" var="checkoutUrl" />
   	<c:url value="/updateAddress" var="updateAddressUrl" />
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="subscription-bill">
                  	<div class="cart-right">
                        <div class="cart-head">
                           <h3>Average Monthly Bill</h3>
                           <h3 class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${averageMonthlyBill}"/></h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-left">
                     	<input class="form-control" id="todate" value="${today}" type="hidden"/>
                         <div class="address-head">
                           <h2>Choose Start Date</h2>
                        </div>
                            <div class="form-group date-selection">
                              <div class="col-xs-8 col-sm-6 col-md-4">
                               <div class="input-group">
                                <div class="input-group-addon">
                                 <i class="fa fa-calendar">
                                 </i>
                                </div>
                                <input class="form-control" id="date" name="date" placeholder="YYYY-MM-DD" type="text"/>
                               </div>
                              </div>
                             </div>
                        </div>
                     <hr>
                     <div class="cart-left">
                        <div class="address-head">
                           <h2>Choose Delivery Address</h2>
                        </div>
                        <div class="row">
                           <c:forEach items="${addresss}" var="address" varStatus="loop">	
	                          <div class="col-md-6" onclick="onAddressSelected('${address.cipher}')" >
	                          <input type="hidden" id="selectedAddress_${address.code}" value="${address.code}" />
	                             <div class="addresses">
	                                <div class="delivery-address">
	                                   <p>${address.addressLine1} ${address.addressLine2} ${address.landmark} ${address.locality} ${address.pincode}</p>
	                                   <div class="address-footer">
	                                      <a class="edit-address" href="${updateAddressUrl}/${address.id}?retUrl=${checkoutUrl}"><i class="fa fa-pencil"></i>Edit</a>
	                                      <c:url value="/address/delete" var="deleteAddressUrl" />
	                                      <a class="delete-address" href="${deleteAddressUrl}/${address.id}?retUrl=${checkoutUrl}"><i class="fa fa-trash"></i>Delete</a>
	                                   </div>
	                                </div>
	                             </div>
	                          </div>
	                          </c:forEach>
                        </div>
                        <div class="new-address-add">
                           <c:url value="/createAddress" var="createNewAddressdUrl" />
                           <a href="${createNewAddressdUrl}?retUrl=${checkoutUrl}">Add New Address +</a>
                        </div>
                     </div>
                      <hr>
                      <div class="cart-left" >
                         <div class="checkout-btn">
                           <a href="javascript:submitForm();">Confirm Order</a>
                        </div>
                        <c:url value="/postpay/setaddress" var="paymentUrl" />
                     	<form action="${paymentUrl}" id="paymentForm"></form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	  <script>
		  $(document).ready(function(){
			  var date_input=$('input[name="date"]'); //our date input has the name "date"
			  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			  date_input.datepicker({
				  format: 'yyyy-mm-dd',
				  container: container,
				  todayHighlight: true,
				  autoclose: true,
			  })
		  })
	</script>
   </body>
</html>