<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Terms & Condition</title>
      <script type="text/javascript">
	  	function submitForm() {
	  		if(!$("#terms").is(':checked')){
	  			toastr.warning('Please agree to our terms & conditions');
	  			return;
	  		}
	  		$('#termsConditionForm').submit();	
		}
	
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                   <div class="page-head">
                   <h2>Terms &amp; Conditions</h2>
                   </div>
                  <div class="postpre-panel">
                     <div class="cart-left">
                         <div class="post-head">
                             <p>TERMS AND CONDITIONS</p>
                         </div>
                         <div class="post-content">
                             <ul>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                             </ul>
                         </div>
                         <hr>
                         <div class="accept-terms">
                         <div class="form-group">
                         <input type="checkbox" id="terms">
                             <label for="terms">I agree to your terms &amp; conditions</label>
                         </div>
                         </div>
                         <div class="go-postpaid">
                           <a href="javascript:submitForm()">Submit</a>
                        </div>
                        <form action='<c:url value="/average-monthly-bill"/>' id="termsConditionForm"></form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>