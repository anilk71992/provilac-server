<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Subscription Type</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="page-head">
                     <h2>Subscription Type</h2>
                  </div>
                  <div class="postpre-panel">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>Prepaid</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-left">
                        <div class="post-head">
                           <p>Benefits of Prepaid</p>
                        </div>
                        <div class="post-content">
                           <ul>
                              <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                              <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                              <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                              <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                              <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
                           </ul>
                        </div>
                        <div class="go-postpaid">
                           <a href='<c:url value="/prepay-subscription-duration"/>'>go for prepaid</a>
                        </div>
                     </div>
                  </div>
                  <div class="cart-left">
                     <div class="cart-head">
                        <div class="add-more-product">
                           <a href='<c:url value="/postpay-terms-conditions"/>' class="read-more">Go for Postpaid</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>