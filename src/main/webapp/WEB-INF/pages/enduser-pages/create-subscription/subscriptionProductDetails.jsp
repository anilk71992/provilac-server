<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Subscribe Product Details</title>
      <script type="text/javascript">
      var selectedPattern;
      
	      function incrementDailyQty() {
	  		  var currentQty = parseInt($('#dailyQty').val());
	  		  if(currentQty > 3)
	  			  return;
		  	  var qty = currentQty + 1;
		  	  $('#dailyQty').val(qty);
	  	  }
	      
	      function decrementDailyQty() {
	    	  var currentQty = parseInt($('#dailyQty').val());
		  	  if (currentQty <= 1) {
		  		return;
		  	  }
		  	  var qty = currentQty - 1;
		  	  $('#dailyQty').val(qty);
	  	  }
	      
	      function incrementWeeklyQty() {
	  		  var currentQty = parseInt($('#weeklyQty').val());
	  		  if(currentQty > 3)
	  			  return;
		  	  var qty = currentQty + 1;
		  	  $('#weeklyQty').val(qty);
	  	  }
	      
	      function decrementWeeklyQty() {
	    	  var currentQty = parseInt($('#weeklyQty').val());
		  	  if (currentQty <= 1) {
		  		return;
		  	  }
		  	  var qty = currentQty - 1;
		  	  $('#weeklyQty').val(qty);
	  	  }
	      
	      function incrementAlternate1Qty() {
	  		  var currentQty = parseInt($('#alt1Qty').val());
	  		  if(currentQty > 3)
	  			  return;
		  	  var qty = currentQty + 1;
		  	  $('#alt1Qty').val(qty);
	  	  }
	      
	      function decrementAlternate1Qty() {
	    	  var currentQty = parseInt($('#alt1Qty').val());
		  	  if (currentQty <= 0) {
		  		return;
		  	  }
		  	  var qty = currentQty - 1;
		  	  $('#alt1Qty').val(qty);
	  	  }
	      
	      function incrementAlternate2Qty() {
	  		  var currentQty = parseInt($('#alt2Qty').val());
	  		  if(currentQty > 3)
	  			  return;
		  	  var qty = currentQty + 1;
		  	  $('#alt2Qty').val(qty);
	  	  }
	      
	      function decrementAlternate2Qty() {
	    	  var currentQty = parseInt($('#alt2Qty').val());
		  	  if (currentQty <= 0) {
		  		return;
		  	  }
		  	  var qty = currentQty - 1;
		  	  $('#alt2Qty').val(qty);
	  	  }
		      
	      function onRadioChange(radioType) {
	
	  		if (radioType == 'daily') {
	  			$('#weeklyQtyDiv').hide();
	  			$('#alternateQtyDiv').hide();
	  			$('#dailyQtyDiv').show();
	  		} else if (radioType == 'weekly') {
	  			$('#dailyQtyDiv').hide();
	  			$('#alternateQtyDiv').hide();
	  			$('#weeklyQtyDiv').show();
	  		} else if (radioType == 'alternate') {
	  			$('#weeklyQtyDiv').hide();
	  			$('#dailyQtyDiv').hide();
	  			$('#alternateQtyDiv').show();
	  		}
	  	}
	      
	    $(document).ready(function() {
	  		$('input[type=radio][name=patternRadio]').change(function() {
	  			if (this.checked) {
	  				onRadioChange(this.value);
	  				selectedPattern = this.value;
	  			}
	  		});
	  	});
      
	  	function submitForm() {
			if(selectedPattern == null || selectedPattern == "") {
				toastr.warning('Please select a subscription pattern');
				return;
			}
			
			if(selectedPattern == 'daily'){
				var currentQty = parseInt($('#dailyQty').val());
				var regex = /[^0-9]/;
				if (regex.test(currentQty)) { 
					toastr.warning('Please enter valid quantity');	
					return;
				}
			  	if (currentQty < 1 || currentQty > 4) {
			  		toastr.warning('Quantity should be have between 1-4 only');
			  		return;
			  	}
			} else if(selectedPattern == 'weekly'){
				var currentQty = parseInt($('#weeklyQty').val());
				var regex = /[^0-9]/;
				if (regex.test(currentQty)) { 
					toastr.warning('Please enter valid quantity');	
					return;
				}
			  	if (currentQty < 1 || currentQty > 4) {
			  		toastr.warning('Quantity should be have between 1-4 only');
			  		return;
			  	}
			} else if(selectedPattern == 'alternate'){
				var alt1Qty = parseInt($('#alt1Qty').val());
				var alt2Qty = parseInt($('#alt2Qty').val());
				
				var regex = /[^0-9]/;
				if (regex.test(alt1Qty)) { 
					toastr.warning('Please enter valid quantity of alternate1');	
					return;
				}
			  	if (alt1Qty > 4) {
			  		toastr.warning('Quantity alternate1 should be have lesh than or equal 4 only');
			  		return;
			  	}
			  	
			  	var regex = /[^0-9]/;
				if (regex.test(alt2Qty)) { 
					toastr.warning('Please enter valid quantity of alternate2');	
					return;
				}
			  	if (alt2Qty > 4) {
			  		toastr.warning('Quantity alternate2 should be have lesh than or equal 4 only');
			  		return;
			  	}
			  	if ((alt1Qty <= 0 && alt2Qty <= 0)) {
			  		toastr.warning('Please enter quantity properly');
			  		return;
			  	}
			}
			
			
			$('<input type="hidden" name="productCipher" value="${product.cipher}"/>').appendTo('#patternForm');
			
			if(selectedPattern == 'daily') {
				$('<input type="hidden" name="subscriptionType" value="Daily"/>').appendTo('#patternForm');
				$('<input type="hidden" name="quantities" value="' + $('#dailyQty').val() + '"/>').appendTo('#patternForm');
			} else if(selectedPattern == 'weekly') {
				var index = 0;
				$('<input type="hidden" name="subscriptionType" value="Weekly"/>').appendTo('#patternForm');
				$('<input type="hidden" name="quantities" value="' + $('#weeklyQty').val() + '"/>').appendTo('#patternForm');
				if($("#check8").is(':checked')){
					$('<input type="hidden" name="days" value="1"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check9").is(':checked')){
					$('<input type="hidden" name="days" value="2"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check10").is(':checked')){
					$('<input type="hidden" name="days" value="3"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check11").is(':checked')){
					$('<input type="hidden" name="days" value="4"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check12").is(':checked')){
					$('<input type="hidden" name="days" value="5"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check13").is(':checked')){
					$('<input type="hidden" name="days" value="6"/>').appendTo('#patternForm');
					index = 1;
				}
				if($("#check14").is(':checked')){
					$('<input type="hidden" name="days" value="7"/>').appendTo('#patternForm');
					index = 1;
				}
				if(index == 0){
					toastr.warning('Please choose delivery days');
			  		return;
				}
				
			} else if(selectedPattern == 'alternate') {
				$('<input type="hidden" name="subscriptionType" value="Custom"/>').appendTo('#patternForm');
				$('<input type="hidden" name="quantities" value="' + $('#alt1Qty').val() + '"/>').appendTo('#patternForm');
				$('<input type="hidden" name="quantities" value="' + $('#alt2Qty').val() + '"/>').appendTo('#patternForm');
			}
			$('#patternForm').submit();
		}
  	
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="details">
         <div class="container">
            <div class="row">
               <div class="description">
                  <div class="col-md-6">
                     <div class="product-image">
                        <a href="#"><img src="<c:url value="restapi/getProductDetailsPicture?id=${product.id}"/>"></a>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="detail-desc">
                        <h3>${product.name}</h3>
                        <span class="product-price"><i class="fa fa-inr"></i> ${product.price}</span>
                        <p>${product.description}</p>
                        <div class="subscription-pattern">
                        <div class="pattern-options">
                           <div class="form-group">
                              <div class="radio-inline">
                                 <input type="radio" name="patternRadio" class="css-checkbox" onchange="onRadioChange('daily')" id="dailyRadio" value="daily" style="width: 20px;height: 20px;">
                                 <label for="dailyRadio" class="css-label green-radio">Daily</label>
                              </div>
                              <div class="radio-inline">
                                 <input type="radio" name="patternRadio" class="css-checkbox" onchange="onRadioChange('weekly')" id="weeklyRadio" value="weekly" style="width: 20px;height: 20px;">
                                 <label for="weeklyRadio" class="css-label green-radio">Weekdays</label>
                              </div>
                              <div class="radio-inline">
                                 <input type="radio" name="patternRadio" class="css-checkbox" id="alternateRadio" value="alternate" style="width: 20px;height: 20px;">
                                 <label for="alternateRadio" class="css-label green-radio">Alternate</label>
                              </div>
                           </div>
                        </div>
                        <div class="pattern-options" id="dailyQtyDiv" style="display:none">
                           <div class="form-group">
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check1">sun</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check2">mon</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check3">tue</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check4">wed</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check5">thu</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check6">fri</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" checked class="selector" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check7">sat</label>
                              </div>
                           </div>
                           <div class="input-group btn-incre">
	                           <span class="input-group-btn">
	                           <button type="button" onclick="javascript:decrementDailyQty()" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
	                           <span class="fa fa-minus"></span>  
	                           </button>
	                           </span>
	                           <input type="text" id="dailyQty" name="quantity" class="form-control input-number" value="1" maxlength="1">
	                           <span class="input-group-btn">
	                           <button type="button" onclick="javascript:incrementDailyQty()" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
	                           <span class="fa fa-plus"></span>
	                           </button>
	                           </span>
                           </div>
                        </div>
                        <div class="pattern-options" id="weeklyQtyDiv" style="display: none">
                           <div class="form-group">
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check8" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check8">sun</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check9"  value="option1" style="width: 20px;height: 20px;">
                                 <label for="check9">mon</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check10" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check10">tue</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check11" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check11">wed</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check12" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check12">thu</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check13" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check13">fri</label>
                              </div>
                              <div class="checkbox-inline">
                                 <input type="checkbox" name="optionsRadios" id="check14" value="option1" style="width: 20px;height: 20px;">
                                 <label for="check14">sat</label>
                              </div>
                           </div>
                           <div class="input-group btn-incre">
	                           <span class="input-group-btn">
	                           <button type="button" onclick="javascript:decrementWeeklyQty()" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
	                           <span class="fa fa-minus"></span>  
	                           </button>
	                           </span>
	                           <input type="text" id="weeklyQty" name="quantity" class="form-control input-number" value="1" maxlength="1">
	                           <span class="input-group-btn">
	                           <button type="button" onclick="javascript:incrementWeeklyQty()" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
	                           <span class="fa fa-plus"></span>
	                           </button>
	                           </span>
                           </div>
                        </div>
                        <div class="pattern-options form-inline" id="alternateQtyDiv" style="display: none">
                            <span style="font-weight: 600">Alternate Between</span>
                            <div class="input-group btn-incre">
                                 <span class="input-group-btn">
                                 <button type="button" onclick="javascript:decrementAlternate1Qty()" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
                                 <span class="fa fa-minus"></span>
                                 </button>
                                 </span>
                                 <input type="text" id="alt1Qty" name="quantity" class="form-control" value="1" maxlength="1">
                                 <span class="input-group-btn">
                                 <button type="button" onclick="javascript:incrementAlternate1Qty()" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
                                 <span class="fa fa-plus"></span>
                                 </button>
                                 </span>
                              </div>
                              <span style="font-weight: 600">&amp;</span>
                              <div class="input-group btn-incre">
                                 <span class="input-group-btn">
                                 <button type="button" onclick="javascript:decrementAlternate2Qty()" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
                                 <span class="fa fa-minus"></span>
                                 </button>
                                 </span>
                                 <input type="text" id="alt2Qty" name="quantity" class="form-control" value="1" maxlength="1">
                                 <span class="input-group-btn">
                                 <button type="button" onclick="javascript:incrementAlternate2Qty()" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
                                 <span class="fa fa-plus"></span>
                                 </button>
                                 </span>
                              </div>
                        </div>
                    </div>
					<div class="add-to-cart-btn">
                    	<a href="javascript:submitForm()">Next</a>
                    </div>
					<form action='<c:url value="/confirm-subscription"/>' id="patternForm"></form>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>