<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Provilac - Categories</title>
	</head>
	<body class="home-page" id="top">
	<c:url value="/product" var="getProductUrl" />
		<section id="category">
			<div class="container">
				<div class="row">
					<div class="category-details">
						<div class="category-head">
							<h2>Our products</h2>
						</div>
						<c:forEach items="${categorys}" var="category">	
						<div class="col-md-4">
							<div class="category-desc">
								<div class="product-image">
									<a href="${getProductUrl}?categoryCode=${category.code}">
										<img src="<c:url value="/restapi/getCategoryPicture?id=${category.id}"/>">
									</a>
								</div>
								<div class="product-details">
									<h3>${category.name}</h3>
								</div>
							</div>
						</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>