<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - SignUp</title>
      <script type="text/javascript">
	      function onSignUpClick() {
	    	  
	    	  var mobile = $('#primaryMobileNumber').val().trim();		    	
	    	  var primaryMobileNumberPattern = /^[1-9]{1}[0-9]{9}$/;
	    		
   			  if(!primaryMobileNumberPattern.test(mobile)) {
   					$('#primaryMobileNumber').focus();
   					toastr.warning('Please enter valid mobile number');
   					return;
   				} 	
	    			  
		    	$.ajax({
		    		url: '<c:url value="/enduser/creatCustomer"/>',
		    		dataType: 'json',
		    		data : 'mobile=' + mobile,
		    		success: function(response) {
		    			if (null != response && response.success) {	
		    				var flag = response.data.flag;
		    				if(flag == true){
		    					var addedMobileNumber = response.data.addedMobileNumber;
			    				$('#mobileNumber').val(addedMobileNumber);
			    				$('.login-form').animate({height: "toggle", opacity: "toggle"}, "slow"); 
			    				$('.register-form').animate({height: "toggle", opacity: "toggle"}, "slow");
			    				$('.pincode-form').stop(true,true).fadeIn(200);
		    				} else {
		    					var message = response.data.message;
			    				$('#message').text(message);
			    				$('#message').show();
		    				}
		    			}else{
		    				showToast('error','Error Occured!');
		    			} 
		    		}
		    	});  
	      }
	      
	      function onOtpClick(){
	    	  
	    	  var mobile = $('#mobileNumber').val().trim();
	    	  var verificationCode = $('#verificationCode').val().trim();	
		    	$.ajax({
		    		url: '<c:url value="/enduser/verifyOtp"/>',
		    		dataType: 'json',
		    		data : 'mobile=' + mobile + '&verificationCode=' + verificationCode,
		    		success: function(response) {
		    			if (null != response && response.success) {	
		    				var flag = response.data.flag;
		    				if(flag == true){
		    					location.href = '<c:url value="/end-user/profile" />';
		    				} else {
		    					var message = response.data.message;
			    				$('#otpMessage').text(message);
			    				$('#otpMessage').show();
		    				}
		    			}else{
		    				showToast('error','Error Occured!');
		    			} 
		    		}
		    	});
	      }
	      
		 function resendClick() {
	    	  
	    	  var mobile = $('#primaryMobileNumber').val().trim();
   			  
		    	$.ajax({
		    		url: '<c:url value="/enduser/checkCustomer"/>',
		    		dataType: 'json',
		    		data : 'mobile=' + mobile,
		    		success: function(response) {
		    			if (null != response && response.success) {	
		    				var flag = response.data.flag;
		    				if(flag == true){
		    					var addedMobileNumber = response.data.addedMobileNumber;
			    				$('#mobileNumber').val(addedMobileNumber);
			    				document.getElementById("verificationCode").value = "";
			    				$('#otpMessage').hide();
			    				$('.pincode-form').stop(true,true).fadeIn(200);
		    				} else {
		    					var message = response.data.message;
			    				$('#message').text(message);
			    				$('#message').show();
		    				}
		    			}else{
		    				showToast('error','Error Occured!');
		    			} 
		    		}
		    	}); 
	      }
      </script>
   </head>
   <body class="home-page" id="top">
   <c:url value="/pincode" var="pincodeUrl" />
   <c:url value="/signUp" var="signUpUrl" />
   <c:url value="/verifyOtp" var="verifyOtpUrl" />
      <section id="section1">
         <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="login-page">
                     	<div class="pincode-form">
	                     	<div class="login-form">
		                        <h2>Sign Up</h2>
		                        <p>Please enter your registered mobile number to receive OTP</p>
		                        <input type="mobile" name="mobileNumber" size="10" maxlength="10" id="primaryMobileNumber" placeholder="Registered Mobile Number"/>
		                        <div id="message" style="color: #ef6767; display:none" class="success"></div><br>
		                        <button onclick="onSignUpClick()"><a href="javascript:onSignUpClick()" class="message" href="#">Sign Up</a></button>
			                    <p class="terms-condition">By Signing up you agree to our <a href="${pincodeUrl}" style="color:#d74123">Terms &amp; Conditions</a></p>
		                    </div>
		                    <div class="register-form">
			                    <h2>Verification Code</h2>
			                    <p>We have sent OTP to your mobile number</p>
			                    <input type="hidden" name="mobileNumber" id="mobileNumber" />
			                    <input type="text" name="verificationCode" size="6" maxlength="6" id="verificationCode" placeholder="Enter OTP"/>
			                    <div id="otpMessage" style="color: #ef6767; display:none" class="success"></div><br>
			                    <p><a href="javascript:resendClick()">Resend OTP</a></p>
			                    <button onclick="onOtpClick()" class="message">Submit</button>
		                    </div>
                      	</div>
                    </div>
                  </div>
            </div>
         </div>
      </section>
   </body>
</html>