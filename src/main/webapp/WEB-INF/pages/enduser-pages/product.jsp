<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Product</title>
   </head>
   <body class="home-page" id="top">
   <c:url value="/productDetails" var="productDetailsUrl" />
      <section id="category">
         <div class="container">
            <div class="row">
                <div class="category-details">
                    <div class="category-head">
                    <h2>${categoryName}</h2>
                    </div>
                    <input type="hidden" name="categoryCode" value="${categoryCode}" />
                    <c:forEach items="${products}" var="product">	
	                    <div class="col-md-4">
	                        <div class="category-desc">
		                        <div class="product-image">
		                        	<a href="${productDetailsUrl}?productCode=${product.code}">
		                        		<img src="<c:url value="restapi/getProductListPicture?id=${product.id}"/>">
		                        	</a>
		                        </div>
		                        <div class="product-details">
		                        	<h3>${product.name}</h3>
		                            <a href="${productDetailsUrl}?productCode=${product.code}" class="read-more">Read More</a>
		                        </div>
	                    	</div>
	                    </div>
                    </c:forEach>
                </div>
            </div>
         </div>
      </section>
   </body>
</html>