<%@ include file="/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>Provilac - Pattern</title>
<script type="text/javascript">
	
	var selectedPattern;
	
	function incrementQty(field) {

		var inputFieldId;
		if (field == 'daily') {
			inputFieldId = "dailyQty";
		} else if (field == 'sunday') {
			inputFieldId = "sundayQty";
		} else if (field == 'monday') {
			inputFieldId = "mondayQty";
		} else if (field == 'tuesday') {
			inputFieldId = "tuesdayQty";
		} else if (field == 'wednesday') {
			inputFieldId = "wednesdayQty";
		} else if (field == 'thursday') {
			inputFieldId = "thursdayQty";
		} else if (field == 'friday') {
			inputFieldId = "fridayQty";
		} else if (field == 'saturdayday') {
			inputFieldId = "saturdayQty";
		} else if (field == 'alt1') {
			inputFieldId = "alt1Qty";
		} else if (field == 'alt2') {
			inputFieldId = "alt2Qty";
		} else {
			return;
		}

		var currentQty = parseInt($('#' + inputFieldId).val());
		if (currentQty >= 4) {
			return;
		}
		var qty = currentQty + 1;
		$('#' + inputFieldId).val(qty);

	}

	function decrementQty(field) {
		var inputFieldId;
		if (field == 'daily') {
			inputFieldId = "dailyQty";
		} else if (field == 'sunday') {
			inputFieldId = "sundayQty";
		} else if (field == 'monday') {
			inputFieldId = "mondayQty";
		} else if (field == 'tuesday') {
			inputFieldId = "tuesdayQty";
		} else if (field == 'wednesday') {
			inputFieldId = "wednesdayQty";
		} else if (field == 'thursday') {
			inputFieldId = "thursdayQty";
		} else if (field == 'friday') {
			inputFieldId = "fridayQty";
		} else if (field == 'saturdayday') {
			inputFieldId = "saturdayQty";
		} else if (field == 'alt1') {
			inputFieldId = "alt1Qty";
		} else if (field == 'alt2') {
			inputFieldId = "alt2Qty";
		} else {
			return;
		}

		var currentQty = parseInt($('#' + inputFieldId).val());
		if (currentQty <= 0) {
			return;
		}
		var qty = currentQty - 1;
		$('#' + inputFieldId).val(qty);
	}

	function onRadioChange(radioType) {

		if (radioType == 'daily') {
			$('#weeklyQtyDiv').hide();
			$('#alternateQtyDiv').hide();
			$('#dailyQtyDiv').show();
		} else if (radioType == 'weekly') {
			$('#dailyQtyDiv').hide();
			$('#alternateQtyDiv').hide();
			$('#weeklyQtyDiv').show();
		} else if (radioType == 'alternate') {
			$('#weeklyQtyDiv').hide();
			$('#dailyQtyDiv').hide();
			$('#alternateQtyDiv').show();
		}

	}

	$(document).ready(function() {
		$('input[type=radio][name=patternRadio]').change(function() {
			if (this.checked) {
				onRadioChange(this.value);
				selectedPattern = this.value;
			}
		});
	});
	
	function submitForm() {
		if(selectedPattern == null || selectedPattern == "") {
			alert('Please select a subscription pattern');
			return;
		}
		
		$('<input type="hidden" name="productCipher" value="${product.cipher}"/>').appendTo('#patternForm');
		
		if(selectedPattern == 'daily') {
			$('<input type="hidden" name="subscriptionType" value="Daily"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#dailyQty').val() + '"/>').appendTo('#patternForm');
		} else if(selectedPattern == 'weekly') {
			$('<input type="hidden" name="subscriptionType" value="Weekly"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#sundayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#mondayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#tuesdayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#wednesdayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#thursdayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#fridayQty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#saturdayQty').val() + '"/>').appendTo('#patternForm');
		} else if(selectedPattern == 'alternate') {
			$('<input type="hidden" name="subscriptionType" value="Custom"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#alt1Qty').val() + '"/>').appendTo('#patternForm');
			$('<input type="hidden" name="quantities" value="' + $('#alt2Qty').val() + '"/>').appendTo('#patternForm');
		}
		$('#patternForm').submit();
	}
</script>
</head>
<body>
	<!-- Start Login Register Area -->
	<div class="htc__login__register bg__white ptb--130" style="background: rgba(0, 0, 0, 0) url(assets/endUser-assets/images/bg/5.jpg) no-repeat scroll center center/cover;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="login-head">
						<h2>Choose Pattern</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Login Register Area -->
	<!-- Start Our Pattern Area -->
	<div class="pattern-area ptb--130">
		<div class="container">
			<div class="daily">
				<div class="row">
					<div class="col-md-6">
						<div class="pattern-choose">
							<input type="radio" id="dailyRadio" name="patternRadio" value="daily" onchange="onRadioChange('daily')"> <label for="dailyRadio">Daily</label>
						</div>
					</div>
					<div id="dailyQtyDiv" class="col-md-6" style="display: none;">
						<div class="input-group btn-incre1">
							<span class="input-group-btn">
								<button type="button" onclick="javascript:decrementQty('daily')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
									<span class="ti-minus"></span>
								</button>
							</span> <input type="text" id="dailyQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
								<button type="button" onclick="javascript:incrementQty('daily')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
									<span class="ti-plus"></span>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="daily">
				<div class="row">
					<div class="col-md-6">
						<div class="pattern-choose" id="pattern-choose">
							<input type="radio" id="weeklyRadio" name="patternRadio" value="weekly" onchange="onRadioChange('weekly')"> <label for="weeklyRadio">Weekly</label>
						</div>
					</div>
				</div>
				<div id="weeklyQtyDiv" class="hide-weekdays" style="display: none;">
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>SUNDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('sunday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="sundayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('sunday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>MONDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('monday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="mondayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('monday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>TUESDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('tuesday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="tuesdayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('tuesday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>WEDNESDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('wednesday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="wednesdayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('wednesday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>THURSDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('thursday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="thursdayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('thursday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>FRIDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('friday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="fridayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('friday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="days">
						<div class="row">
							<div class="col-md-6">
								<div class="week-days">
									<span>SATURDAY</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group btn-incre1">
									<span class="input-group-btn">
										<button type="button" onclick="javascript:decrementQty('saturday')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
											<span class="ti-minus"></span>
										</button>
									</span> <input type="text" id="saturdayQty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
										<button type="button" onclick="javascript:incrementQty('saturday')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
											<span class="ti-plus"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="daily">
				<div class="row">
					<div class="col-md-6">
						<div class="pattern-choose">
							<input type="radio" id="alternateRadio" name="patternRadio" value="alternate"> <label for="alternateRadio">Alternate</label>
						</div>
					</div>
					<div id="alternateQtyDiv" class="col-md-6" style="display: none;">
						<div class="input-group btn-incre1">
							<span class="input-group-btn">
								<button type="button" onclick="javascript:decrementQty('alt1')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
									<span class="ti-minus"></span>
								</button>
							</span> <input type="text" id="alt1Qty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
								<button type="button" onclick="javascript:incrementQty('alt1')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
									<span class="ti-plus"></span>
								</button>
							</span>
						</div>
						<div class="input-group btn-incre1" style="margin-top: 20px">
							<span class="input-group-btn">
								<button type="button" onclick="javascript:decrementQty('alt2')" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
									<span class="ti-minus"></span>
								</button>
							</span> <input type="text" id="alt2Qty" name="quantity" class="form-control input-number" value="0" min="0" max="100"> <span class="input-group-btn">
								<button type="button" onclick="javascript:incrementQty('alt2')" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
									<span class="ti-plus"></span>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="next-btn">
				<a href="javascript:submitForm()"><button type="button" class="btn btn-default city-button" data-dismiss="modal">Next</button></a>
			</div>
			<form action='<c:url value="/confirm-prepay-subscription"/>' id="patternForm"></form>
		</div>
	</div>
	<!-- Start Our Pattern Area -->
</body>
</html>