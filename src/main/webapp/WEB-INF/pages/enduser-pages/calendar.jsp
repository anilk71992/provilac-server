<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Calendar</title>
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

	  <script type="text/javascript">
			var events = new Array();
			var deliverySchedules= new Array();
			var customerId = ${customerId};
			var deliveryScheduleCipher = '${deliveryScheduleCipher}';
			
			$(document).ready(function(){
				var actionButton = ${tomorrowOrder};
				if(actionButton == '' || actionButton == false){
					$('#actionButton').hide();
				} else {
					$('#actionButton').show();
				}
				$('.subscriptionDate').datepicker({ autoclose: true});
				
				$.ajax({
					url: '<c:url value="/deliverySchedule/getLineItems"/>',
					dataType: 'json',
					data: 'customerId='+customerId,
					
					success: function(response){
						if(null!=response && response.success){
						var	data = response.data;
						deliverySchedules= data.deliverySchedules;
						for(var i=0;i<data.deliverySchedules.length;i++){
							var deliverySchedule = data.deliverySchedules[i];
							
							var web="";
							var mobile="";
							
							var event=new Object();
							if(deliverySchedule.updatedFromWeb || deliverySchedule.updatedFromMobile){
								var title="";
								
								if(deliverySchedule.updatedFromMobile){
									title=title+ " MOBILE ";
								}
								if(deliverySchedule.updatedFromWeb){
									title=title+ " WEB ";
								}
								event['title']=title;
								event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
								event['color'] = "#ffffff ";
							}
							
							events.push(event);
							
							for(var j=0;j<deliverySchedule.deliveryLineItemDTOs.length;j++){
								var lineItem = deliverySchedule.deliveryLineItemDTOs[j];
								var event = new Object();
		
								event['title'] = lineItem.quantity + " " + lineItem.productName;
								event['start']= moment(deliverySchedule.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
								event['id'] = lineItem.code;
								event['scheduleId'] = deliverySchedule.id;
								event['productId'] = lineItem.productCode;
								event['quantity'] = lineItem.quantity;
								event['color'] = "#"+lineItem.productColorCode;;
								event['description']="description";
								events.push(event);
							}
						}
		
						initializeCalendar(data.isAuthorized);
						events = new Array();
						}else {
							showToast("error","No Users Found!!");
						}
					}
				});
			});
			
			function initializeCalendar(isAuthorized) {
				$('#calendar').fullCalendar({
					events : events,
					
					eventClick : function(event, jsEvent, view) {
						getScheduleLineItemsAccourdingGivenevDate(event.start);
					},
					dayClick: function(date, allDay, jsEvent, view) { 
						getScheduleLineItemsAccourdingGivenevDate(date);
			        } 
				
				});
			}
			
			function getScheduleLineItemsAccourdingGivenevDate(date){
				var checkDate = $.fullCalendar.formatDate(date,'YYYY-MM-DD');
				var displayDate = $.fullCalendar.formatDate(date,'DD MMM YYYY');
				$('#displaySelectedDate').text(displayDate+' order');
				$.ajax({
					url: '<c:url value="/restapi/end-user/getDeliveryScheduleByCustomerAndDate"/>',
					dataType: 'json',
					data: 'customerId='+customerId + '&date='+checkDate,
					
					success: function(response){
						if(null!=response && response.success){
							$('#displayDeliveryLineItemsDiv').empty();
		    				var deliverySchedules = response.data.deliverySchedule;
		    				deliveryScheduleCipher = response.data.deliveryScheduleCipher;
		    				var selectedButtonflag = response.data.selectedButtonflag;
							for(var i = 0; i < deliverySchedules.deliveryLineItemDTOs.length; i++) {
								var deliverySchedule = deliverySchedules.deliveryLineItemDTOs[i];
								var template = _.template($('#displayLineItemTemplate').html());
								var html = template({'deliverySchedule':deliverySchedule});
								
								$('#displayDeliveryLineItemsDiv').append(html);
							}
							if(selectedButtonflag == true)
								$('#actionButton').show();
							else
								$('#actionButton').hide();
						}else {
							deliveryScheduleCipher = '';
							$('#displayDeliveryLineItemsDiv').empty();
							$('#displayDeliveryLineItemsDiv').text('No orders for '+displayDate);
							$('#actionButton').hide();
							showToast("error","No Users Found!!");
						}
					}
				});
			}
			
			function changeScheduleForSelected(){
				if(deliveryScheduleCipher == '' || deliveryScheduleCipher == null)
					return;
				$('<input type="hidden" name="deliveryScheduleCipher" value="' + deliveryScheduleCipher + '"/>').appendTo('#changeScheduleForm');
				$('#changeScheduleForm').submit();
			}
			
			function cancelScheduleForSelected(){
				if(deliveryScheduleCipher == '' || deliveryScheduleCipher == null)
					return;
				$('<input type="hidden" name="deliveryScheduleCipher" value="' + deliveryScheduleCipher + '"/>').appendTo('#cancelScheduleForm');
				$('#cancelScheduleForm').submit();
			}
	</script> 
      
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="calendar-panel">
                     <div id="calendar"></div>
                  </div>
               </div>
               <input class="form-control subscriptionDate" type="hidden" value="${today}" data-date-format="dd-mm-yyyy" id="subscriptionDate"/>
               <c:if test="${subscriptionTypeFlag == true}">
	               <div class="col-md-4">
	                  <div class="cart-billing-panel">
	                     <div class="cart-right-billing">
	                        <div class="cart-summary">
	                           <span class="product-name">Current Outstanding</span>
	                           <span class="product-prices"><i class="fa fa-inr"></i> ${lastPendingDues}</span>
	                        </div>
	                        <div class="cart-summary">
	                           <span class="product-name">Unbilled Amount</span>
	                           <span class="product-prices"><i class="fa fa-inr"></i> ${totalUnbilledAmount}</span>
	                        </div>
	                        <div class="cart-summary">
	                           <span class="product-name">Total Outstanding</span>
	                           <span class="product-prices"><i class="fa fa-inr"></i> ${lastPendingDues + totalUnbilledAmount}</span>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	           </c:if>
               <div class="col-md-4">
                  <div class="cart-bottom-panel">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Today's Order</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                     	<c:choose>
                     		<c:when test="${setOfdeliveryLineItemsForToday != null}">
                     			<c:forEach items="${setOfdeliveryLineItemsForToday}" var="deliveryLineItemForToday">
		                     		<div class="cart-summary">
			                           <span class="product-name">${deliveryLineItemForToday.product.name}</span>
			                           <span class="product-prices">${deliveryLineItemForToday.quantity}</span>
			                        </div>	
		                     	</c:forEach>
                     		</c:when>
                     		<c:otherwise>
                     			<span>No order for today</span>
                     		</c:otherwise>
                     	</c:choose>
                     </div>
                     <hr>
                     <c:if test="${setOfdeliveryLineItemsForToday != null}">
                     	<div class="cart-right">
	                        <div class="status">
	                           <h3>${deliveryScheduleForToday.status}</h3>
	                        </div>
	                     </div>
                     </c:if>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="cart-bottom-panel">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3 id="displaySelectedDate">Tomorrow's Order</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right" id="displayDeliveryLineItemsDiv">
                     	<c:choose>
                     		<c:when test="${setOfdeliveryLineItemsForTomorrow != null}">
                     			<c:forEach items="${setOfdeliveryLineItemsForTomorrow}" var="deliveryLineItemForTomorrow">
		                     		<div class="cart-summary">
			                           <span class="product-name">${deliveryLineItemForTomorrow.product.name}</span>
			                           <span class="product-prices">${deliveryLineItemForTomorrow.quantity}</span>
			                        </div>	
		                     	</c:forEach>
                     		</c:when>
                     		<c:otherwise>
                     			<span>No order for tomorrow</span>
                     		</c:otherwise>
                     	</c:choose>
                     </div>
                     <hr>
                     <%-- <c:if test="${setOfdeliveryLineItemsForTomorrow != null}"> --%>
                     	<div class="cart-right" id="actionButton">
	                        <div class="order-summary">
	                           <a href="javascript:changeScheduleForSelected()">Change schedule</a>
	                           <a href="javascript:cancelScheduleForSelected()">Cancel Order</a>
	                           <form action='<c:url value="/end-user/change-schedule"/>' id="changeScheduleForm"></form>
	                           <form action='<c:url value="/end-user/cancel-schedule"/>' id="cancelScheduleForm"></form>
	                        </div>
	                     </div>
                     <%-- </c:if> --%>
                  </div>
               </div>
               <!-- /row -->
            </div>
         </div>
         <!-- /container -->
      </section>
      <c:if test="${sessionScope.showExtendPrepay == null}">
	      <section id="postpaid-benefits">
	         <div class="container">
	            <div class="row">
	               <div class="col-md-12">
	                  <div class="cart-left-panel">
	                     <div class="cart-left">
	                        <div class="cart-head">
	                           <h3>Benefits of prepaid</h3>
	                        </div>
	                     </div>
	                     <hr>
	                     <div class="cart-left">
	                        <div class="post-content">
	                           <ul>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</li>
	                           </ul>
	                        </div>
	                     </div>
	                     <hr>
	                     <div class="cart-right">
	                        <div class="sub-checkout-btn">
	                           <a href='<c:url value="/end-user/shift-to-prepay/subscription-duration"/>'>Shift to prepaid</a>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	               <!-- /row -->
	            </div>
	         </div>
	         <!-- /container -->
	      </section>
	   </c:if>
   </body>
</html>