<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Product Details</title>
   </head>
   <body class="home-page" id="top">
   <c:url value="/allCategories" var="allCategoriesUrl" />
      <section id="details">
         <div class="container">
            <div class="row">
               <div class="description">
                  <div class="col-md-6">
                     <div class="product-image">
                        <a href="#">
                        	<img src="<c:url value="restapi/getProductDetailsPicture?id=${product.id}"/>">
                        </a>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="detail-desc">
                        <h3>${product.name}</h3>
                        <span class="product-price"><i class="fa fa-inr"></i> ${product.price}</span>
                        <p>${product.description}</p>
                        <div class="input-group btn-incre">
                           <span class="input-group-btn">
                           <button type="button" onclick="javascript:decrementQty()" class="quantity-left-minus btn btn-number btn-pm" data-type="minus" data-field="">
                           <span class="fa fa-minus"></span>
                           </button>
                           </span>
                           <input type="text" id="qty" name="quantity" class="form-control input-number" value="1" maxlength="1">
                           <span class="input-group-btn">
                           <button type="button" onclick="javascript:incrementQty()" class="quantity-right-plus btn btn-number btn-pm" data-type="plus" data-field="">
                           <span class="fa fa-plus"></span>
                           </button>
                           </span>
                        </div>
                         <div class="add-to-cart-btn">
                          <a href="javascript:addToCartWithQuantity('${product.name}', '${product.code}');" class="read-more">Add to Cart</a>
                         </div>
                     </div>
                     <div class="add-more-product">
                         <a href="${allCategoriesUrl}" class="read-more">Add More Products +</a>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript">
      function incrementQty() {
  		  var currentQty = parseInt($('#qty').val());
  		  if(currentQty > 3)
  			  return;
	  	  var qty = currentQty + 1;
	  	  $('#qty').val(qty);
  	  }
      
      function decrementQty() {
    	  var currentQty = parseInt($('#qty').val());
	  	  if (currentQty <= 1) {
	  		return;
	  	  }
	  	  var qty = currentQty - 1;
	  	  $('#qty').val(qty);
  	  }
      
      function addToCartWithQuantity(productName, productCode){
    	    var quantity = parseInt($('#qty').val());
			var regex = /[^0-9]/;
			if (regex.test(quantity)) { 
				toastr.warning('Please enter valid quantity');	
				return;
			}
		  	if (quantity < 1 || quantity > 4) {
		  		toastr.warning('Quantity should be have between 1-4 only');
		  		return;
		  	}
			
			if(quantity == 1){
				$('#snackbar').text(quantity + ' unit of ' + productName + ' has been added to your shopping cart.');
			} else {
				$('#snackbar').text(quantity + ' units of ' + productName + ' has been added to your shopping cart.');
			}
			addToCart(productName, productCode, quantity);
		}
      
      </script>
   </body>
</html>