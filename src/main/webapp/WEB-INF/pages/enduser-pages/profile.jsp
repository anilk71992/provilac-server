<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Profile</title>
      <script type="text/javascript">	
		
      	$(document).ready(function() {			
			$('#profileId').submit(function() {
			$('#profileId').find(':input[type=submit]').prop('disabled', true);
			if (validate()) {				
				$('#profileId').submit();	
				return true;
			}	
			$('#profileId').find(':input[type=submit]').prop('disabled', false);
			return false;
			});	
		});
      
		function validate() {
			
		if($('#firstName').val() == "") {
			toastr.warning('Please enter user name');
			return false;
		}
		
		var alphaNumericTest = isAlphaOrParen($('#firstName').val());
				
		if(!alphaNumericTest){
			toastr.warning('Please enter valid user name');
		return false;
		}
		
		if($('#primaryEmail').val() == "") {
			toastr.warning('Please enter email.');
			return false;
		}
		var emailId = $('#primaryEmail').val();
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		if(!emailPattern.test(emailId)) {
			$('#primaryEmail').focus();
			toastr.warning('Please enter valid email.');
			return false;
		}
		
		return true;
		
	}
	
	function isAlphaOrParen(str) {
		  return /^[a-zA-Z() ]+$/.test(str);
	}

	</script>
   </head>
   <body class="home-page" id="top">
   <c:url value="/end-user/profile" var="dashbordUrl" />
      <section id="section1">
         <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="login-page">
	                     <div class="pincode-form">
		                     <form action="${dashbordUrl}" id="profileId" class="login-form" method="post">
			                     <h2>Submit Your Details</h2>
			                     <input type="text" name="firstName" id="firstName" placeholder="Enter Name"/>
			                     <input type="text" name="email" id="primaryEmail" placeholder="Enter Email"/>
			                     <button class="submit" data-link-action="sign-up">Submit</button>
		                     </form>
	                     </div>
                     </div>
                </div>
            </div>
         </div>
      </section>
   </body>
</html>