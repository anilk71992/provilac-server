<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Accounts & Billing</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
                <div class="page-head">
                   <h2>Billings</h2>
                   </div>
                <div class="col-md-6">
                  <div class="invoice-billing">
                      <div class="cart-left">
                        <div class="cart-head">
                           <h3>Invoices</h3>
                        </div>
                     </div>
                     <hr>
                     <c:forEach items="${invoceList}" var="invoice">
                     	<div class="cart-right1">
	                         <div class="success-details">
	                              <p><b>Id : </b> ${invoice.id}</p>
	                              <p><b>Invoice Date : </b><fmt:formatDate value="${invoice.createdDate}" pattern="dd-MM-yyyy hh:mm:ss a" /></p>
	                              <p><b>Usage Month : </b><fmt:formatDate value="${invoice.fromDate}" pattern="dd MMM" /> To <fmt:formatDate value="${invoice.toDate}" pattern="dd MMM yyyy" /></p>
	                              <p><b>Total Amount : </b> Rs. ${invoice.totalAmount}</p>
	                              <p><b>Last Pending Dues : </b> Rs. ${invoice.lastPendingDues}</p>
	                         </div>
	                     </div>
	                     <hr>
                     </c:forEach>
                  </div>
               </div>
                <div class="col-md-6">
                  <div class="invoice-billing">
                      <div class="cart-left">
                        <div class="cart-head">
                           <h3>Billings</h3>
                        </div>
                     </div>
                     <hr>
                     <c:forEach items="${billingList}" var="billing" >
	                     <div class="cart-right1">
	                         <div class="success-details">
	                              <p><b>Date : </b><fmt:formatDate value="${billing.date}" pattern="dd-MM-yyyy hh:mm:ss a" /></p>
	                              <p><b>Amount paid : </b> Rs. ${billing.amount}</p>
	                              <p><b>Transaction Id : </b> ${billing.txnId}</p>
	                              <p><b>Payment Method : </b>${billing.paymentMethod}</p>
	                         </div>
	                     </div>
	                     <hr>
                     </c:forEach>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>