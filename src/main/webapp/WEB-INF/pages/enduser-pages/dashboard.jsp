<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Dashboard</title>
   </head>
   <body class="home-page" id="top">
   <c:url value="/dashboardSubmitRequest" var="dashboardSubmitRequestUrl" />
      <section id="section1">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="choose-option">
                     <div class="dashboard-option">
                        <div class="member-area">
                           <form action="${dashboardSubmitRequestUrl}" method="Post">
                              <label class="radio-inline">
                              <input type="radio" id="below" name="optradio" value="subscribe" checked>Subscribe</label>
                              <p>OR</p>
                              <label class="radio-inline">
                              <input type="radio" id="above" name="optradio" value="orderOnce" >Order Once</label>
                              <button class="submit" type="submit">Proceed</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>