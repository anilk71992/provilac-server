<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Average Monthly Bill</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="subscription-bill">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Average Monthly Bill</h3>
                            <h3 class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${averageMonthlyBill}"/></h3>
                        </div>
                     </div>
                     <hr>
                     
                     <div class="cart-right">
                     	<c:forEach items="${subscriptionItems}" var="item">
                        <div class="cart-summary">
                           <span class="sub-product-name">${item.product.name} ${item.totalQuantity} bottles x <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.productPrice}"/></span>
                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.totalQuantity * item.productPrice}"/></span>
                        </div>
                        </c:forEach>
                     </div>
                     <hr>
                     <div class="cart-left" >
                         <div class="checkout-btn">
                           <a href='<c:url value="/end-user/change-delivery-pattern/postpay-success"/>'>Confirm Order</a>
                         </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>