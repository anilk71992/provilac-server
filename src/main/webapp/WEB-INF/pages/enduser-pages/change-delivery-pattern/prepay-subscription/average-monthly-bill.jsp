<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Average Monthly Bill</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="subscription-bill">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Average Monthly Bill</h3>
                            <h3 class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalSubscriptionAmount / durationInMonths}"/></h3>
                        </div>
                     </div>
                     <hr>
                     
                     <div class="cart-right">
                     	<c:forEach items="${subscriptionItems}" var="item">
                        <div class="cart-summary">
                           <span class="sub-product-name">${item.product.name} ${item.totalQuantity} bottles x <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.productPrice}"/></span>
                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.totalQuantity * item.productPrice}"/></span>
                        </div>
                        </c:forEach>
                     </div>
                     <hr>
                     <c:if test="${flag == 'true'}">
                     	<div class="cart-right">
	                         <div class="cart-summary">
	                            <span class="product-name">Sub Total</span>
	                            <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalSubscriptionAmount}"/></span>
	                         </div>
	                         <div class="cart-summary">
	                           <span class="product-name">Pending Dues</span>
	                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${lastPendingdues}"/></span>
	                        </div>
	                        <div class="cart-summary">
	                           <span class="product-name">Unbilled Amount</span>
	                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalUnbilledAmount}"/></span>
	                        </div>
	                     </div>
                     </c:if>
                    <hr>
                    <div class="cart-right">
                        <div class="cart-summary">
                           <span class="product-name">Final Amount to Pay</span>
                           <span class="product-prices"><i class="fa fa-inr rupees-symbol"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${amountToPay}"/></span>
                        </div>
                    </div>
                   	<hr>
                    <div class="cart-left" >
                        <div class="address-head">
                           <h2>Choose Payment Method</h2>
                        </div>
                        <div class="payment-options">
                           <div class="form-group">
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios1"> <img src="<c:url value='/assets/endUser-assets/img/paytm.png'/>" alt="paypal" for="optionsRadios1" style="margin-bottom: 10px;"/></label>
                              </div>
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" checked="true" id="optionsRadios2" value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios2">DEBIT CARD/CREDIT CARD/NET BANKING/UPI</label>
                              </div>
                           </div>
                        </div>
                         <div class="checkout-btn">
                           <a href='<c:url value="/end-user/change-delivery-pattern/forwardPaymanetRequest"/>'>Make Payment</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>