<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Subscription Duration</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="page-head">
                     <h2>Subscription Type</h2>
                  </div>
                  <div class="postpre-panel">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>prepaid</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-left">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="post-left">
                                   <div class="post-head">
			                          <p>Choose Subscription Duration</p>
			                       </div>
	                                 <div class="post-content">
	                                    <div class="payment-options">
	                                       <div class="form-group">
	                                          <div class="radio-inline">
	                                             <input type="radio" name="prepayDuration" class="css-checkbox" id="optionsRadios1" value="3" checked="checked" style="width: 20px;height: 20px;">
	                                             <label for="optionsRadios1" class="css-label green-radio">3 Months</label>
	                                          </div>
	                                          <div class="radio-inline">
	                                             <input type="radio" name="prepayDuration" class="css-checkbox" id="optionsRadios2" value="6" style="width: 20px;height: 20px;">
	                                             <label for="optionsRadios2" class="css-label green-radio">6 Months</label>
	                                          </div>
	                                          <div class="radio-inline">
	                                             <input type="radio" name="prepayDuration" class="css-checkbox" id="optionsRadios3" value="9" style="width: 20px;height: 20px;">
	                                             <label for="optionsRadios3" class="css-label green-radio">9 Months</label>
	                                          </div>
	                                          <div class="radio-inline">
	                                             <input type="radio" name="prepayDuration" class="css-checkbox" id="optionsRadios4" value="12" style="width: 20px;height: 20px;">
	                                             <label for="optionsRadios4" class="css-label green-radio">12 Months</label>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </div>
                              </div>
                           </div>
                        </div>
                        <hr>
                        <div class="row">
                              <div class="cart-left">
                                  <div class="row">
                                  <div class="col-md-12">
                                 <div class="post-head">
                                    <p>Subscription Details</p>
                                 </div>
                                  </div>
                                  </div>
                                   <div class="col-md-6">
                                 <div class="post-content">
                                    <div class="cart-pricing">
                                       <c:forEach items="${subscriptionItems}" var="item" varStatus="loop">
	                                       <div class="cart-summary">
	                                          <span class="product-name">${loop.index + 1}. ${item.product.name} ${item.quantity} ${item.type} </span>
	                                       </div>
                                       </c:forEach>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                              <div class="cart-left">
                                  <div class="row">
                                  <div class="col-md-12">
                                 <div class="post-head">
                                    <p>According to your subscription duration we have applied following discount</p>
                                 </div>
                                  </div>
                                  </div>
                                 <div class="col-md-6">
                                 <div class="post-content">
                                    <div class="cart-pricing" id="effectiveItemsDiv">
                                    	<c:forEach items="${effectiveItems}" var="pricing" varStatus="loop">
	                                       <div class="cart-summary">
	                                          <span class="product-name">${loop.index + 1}. ${pricing} </span>
	                                          <span class="product-prices"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${effectivePrices[loop.index]}"/></span>
	                                          <span class="product-cut"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${prices[loop.index]}"/></span>
	                                       </div>
                                       </c:forEach>
                                       
                                       <%-- <c:forEach items="${effectivePricing}" var="pricing" varStatus="loop">
	                                       <div class="cart-summary">
	                                          <span class="product-name">${loop.index + 1}. ${pricing.key.name} </span>
	                                          <span class="product-prices"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${pricing.value}"/></span>
	                                          <span class="product-cut"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${pricing.key.price}"/></span>
	                                       </div>
                                       </c:forEach> --%>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          <p class="sub-note"><strong>NOTE :- </strong> If you increase your subscription duration you will get more discount</p>
                     </div>
                  </div>
                  <div class="checkout-btn">
                    <a href='<c:url value="/end-user/change-delivery-pattern/prepay-terms-conditions"/>'>Next</a>
                 </div>
               </div>
               <!-- /row -->
            </div>
         </div>
         <!-- /container -->
      </section>
	  <script>
		  $(document).ready(function(){
			    $('input[type="radio"]').click(function(){			    	
			    	var duration = $(this).attr("value");		    	
			    	
			    	$.ajax({
			    		url: '<c:url value="/end-user/prepay/calculatePrice"/>',
			    		dataType: 'json',
			    		data : 'duration=' + duration,
			    		success: function(response) {
			    			if (null != response && response.success) {			
			    				/* location.reload(); */
			    				$('#effectiveItemsDiv').empty();
			    				var effectiveItems = response.data.effectiveItems;
			    				var prices = response.data.prices;
			    				var effectivePrices = response.data.effectivePrices;
								for(var i = 0; i < effectiveItems.length; i++) {
									var effectiveItem = effectiveItems[i];
									var price = prices[i];
									var effectivePrice = effectivePrices[i];
									var template = _.template($('#effectiveItemsSummaryTemplate').html());
									var html = template({'index':(i + 1), 'effectiveItem':effectiveItem, 'price':price, 'effectivePrice':effectivePrice});
									
									$('#effectiveItemsDiv').append(html);
								}
			    				
			    			}else{
			    				showToast('error','Error Occured!');
			    			} 
			    		}
			    	});
			    	
			    });
			});
		  
	</script>
   </body>
</html>