<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Subscription Summary</title>
      <script type="text/javascript">
      	function removeItem(id) {
      		$.ajax({
      			url: '<c:url value="/remove-lineitem"/>',
      			dataType: 'json',
      			data: 'productId=' + id,
      			success: function(response) {
      				if (null != response) {
      					if(response.success) {
      						<c:choose>
      							<c:when test="${fn:length(subscriptionItems) eq 1}">
      								location.href = '<c:url value="/allCategories" />';
      							</c:when>
      							<c:otherwise>
          							location.reload();
      							</c:otherwise>
      						</c:choose>
      					} else {
      						alert(response.error.message);
      					}
      				} else {
      					alert("Unable to remove item. Please try again later.");
      				} 
      			}
      		}); 
      	}
      	
      	function submitForm(){
      		
      		if($("#optionsRadios1").is(':checked')){
				$('<input type="hidden" name="deliveryMode" value="Ring bell & leave the bottle at the doorstep"/>').appendTo('#descriptionForm');
			} else if($("#optionsRadios2").is(':checked')){
				$('<input type="hidden" name="deliveryMode" value="Do not ring bell. Just leave the bottle at the doorstep"/>').appendTo('#descriptionForm');
			} else if($("#optionsRadios3").is(':checked')){
				$('<input type="hidden" name="deliveryMode" value="Put in the bag"/>').appendTo('#descriptionForm');
			}
      		
      		$('#descriptionForm').submit();
      	}
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                   <div class="page-head">
                   <h2>Subscription Summary</h2>
                   </div>
                  <div class="cart-left-panel" style="width: 75%; margin: auto;">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>List of deliveries</h3>
                        </div>
                     </div>
                     <hr>
                     <c:forEach items="${subscriptionItems}" var="item">
                     <div class="cart-left">
                        <div class="cart-product-summary">
                           <div class="row">
                              <div class="col-md-3 col-xs-3">
                                 <div class="product-image">
                                    <%-- <img src="<c:url value="restapi/getProductCartPicture?id=${item.product.id}"/>"> --%>
                                    <img src="<c:url value="/assets/endUser-assets/img/milk1.png"/>">
                                 </div>
                              </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="products-name">
                                    <span>${item.product.name} ${item.quantity} ${item.type} </span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-xs-12">
                                  <div class="row">
                                      <div class="col-md-6 col-xs-6">
                                 	  	 <div class="delete-product">
                                    		<a href="javascript:removeItem('${item.product.id}')"><i class="fa fa-trash-o"></i></a>
                                 		 </div>
                              		  </div>
                                   	  <div class="col-md-6 col-xs-6">
                                      	 <div class="delete-product">
                                         	<a href='<c:url value="/change-delivery-pattern?productId=${item.product.id}"/>'><i class="fa fa-pencil"></i></a>
                                         </div>
                                      </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <hr>
                     </c:forEach>
                      <div class="cart-left">
                         <div class="add-more-product">
	                         <c:url value="/allCategories" var="allCategoriesUrl" />
	                         <a href="${allCategoriesUrl}" class="read-more">Add More Products +</a>
                         </div>
                      </div>
                      <hr>
                      <div class="cart-left" >
                        <div class="address-head">
                           <h2>Delivery Mode</h2>
                        </div>
                        <div class="payment-options">
                           <div class="form-group">
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios1" checked="true" value="Ring bell & leave the bottle at the doorstep" style="width: 20px;height: 20px;">
                                <label for="optionsRadios1">Ring bell & leave the bottle at the doorstep</label>
                              </div>
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios2" value="Do not ring bell. Just leave the bottle at the doorstep" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios2">Do not ring bell. Just leave the bottle at the doorstep</label>
                              </div>
                               <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios3" value="Put in the bag" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios3">Put in the bag</label>
                              </div>
                           </div>
                        </div>
                        <div class="checkout-btn">
                           <a href="javascript:submitForm()">Next</a>
                        </div>
                        <form action='<c:url value="/end-user/change-delivery-pattern/choose-type"/>' id="descriptionForm"></form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>