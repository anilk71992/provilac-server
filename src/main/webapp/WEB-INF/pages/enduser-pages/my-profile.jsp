<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - My Profile</title>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="subscription-bill">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>My Profile</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="cart-summary user-profile">
                          <label>User Name</label>
                           <input type="text" placeholder="${loggedInUser.firstName} ${loggedInUser.lastName}" disabled>
                        </div>
                         <div class="cart-summary user-profile">
                          <label>Email Id</label>
                           <input type="text" placeholder="${loggedInUser.email}" disabled>
                        </div>
                          <div class="cart-summary user-profile">
                          <label>Mobile Number</label>
                           <input type="text" placeholder="${loggedInUser.mobileNumber}" disabled>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>