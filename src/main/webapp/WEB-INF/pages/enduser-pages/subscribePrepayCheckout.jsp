<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Checkout</title>
	
	<script type="text/javascript">
		var selectedAddress;
		
		function onAddressSelected(cipher) {
			selectedAddress = cipher;
		}
		
		function submitForm() {
			
			if(selectedAddress == null || selectedAddress == '') {
				alert('Please select delivery address');
				return;
			}
			
			$('<input type="hidden" name="addressCipher" value="' + selectedAddress + '"/>').appendTo('#paymentForm');
			$('#paymentForm').submit();		
		}
      	
      </script>
   </head>
   <body class="home-page" id="top">
   
   <sec:authorize access="isAnonymous()">
   	   <c:redirect url="/index" />
   </sec:authorize>
	
    <c:set value="0" var="subTotal" />
    <c:url value="/ForwardOTPaymentRequest" var="ForwardOTPaymentRequestUrl" />
    <c:url value="/createAddress" var="createAddressUrl" />
    <c:url value="/address/delete/" var="deleteAddressUrl" />
    <c:url value="/updateAddress" var="updateAddressUrl" />
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="cart-left-panel">
                  	<div>
						<c:if test="${not empty message}">
							<center><div id="message" class="success">${message}</div></center>
						</c:if>			
					</div>
                     <hr>
                     <div class="cart-left">
                        <div class="address-head">
                           <h2>My Addresses</h2>
                        </div>
                        <div class="row">
                           <c:forEach items="${addresss}" var="address" varStatus="loop">	
                           <div class="col-md-6" onclick="onAddressSelected('${address.cipher}')" >
                           <input type="hidden" id="selectedAddress_${address.code}" value="${address.code}" />
                              <div class="addresses">
                                 <div class="delivery-address">
                                    <p>${address.addressLine1} ${address.addressLine2} ${address.landmark} ${address.locality} ${address.pincode}</p>
                                    <div class="address-footer">
                                       <a class="edit-address" href="${updateAddressUrl}/${address.id}"><i class="fa fa-pencil"></i>Edit</a>
                                       <a class="delete-address" href="${deleteAddressUrl}/${address.id}"><i class="fa fa-trash"></i>Delete</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </c:forEach>
                        </div>
                        <div class="new-address-add">
                           <a href="${createAddressUrl}">Add New Address +</a>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-left" >
                        <div class="address-head">
                           <h2>Payment</h2>
                        </div>
                        <div class="payment-options">
                           <div class="form-group">
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios1"> <img src="<c:url value='/assets/endUser-assets/img/paytm.png'/>" alt="paypal" for="optionsRadios1" style="margin-bottom: 10px;"/></label>
                              </div>
                              <div class="radio">
                                 <input type="radio" name="optionsRadios" id="optionsRadios2" checked value="option1" style="width: 20px;height: 20px;">
                                 <label for="optionsRadios2">DEBIT CARD/CREDIT CARD/NET BANKING/UPI</label>
                              </div>
                           </div>
                        </div>
                        <div class="checkout-btn">
                        	<a href="javascript:submitForm();" class="pull-xs-right">Make Payment</a>
	                    </div>
	                    <c:url value="/prepay/setaddress" var="paymentUrl" />
                     	<form action="${paymentUrl}" id="paymentForm"></form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </body>
</html>