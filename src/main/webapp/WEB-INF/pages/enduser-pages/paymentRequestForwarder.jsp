<%@page import="com.vishwakarma.provilac.model.User"%>
<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<% 
	User loggedInUser = ((UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
	pageContext.setAttribute("loggedInUser", loggedInUser);
%>
<head>   
    <title>Provilac - Please wait...</title>
    <script type="text/javascript">
		$(document).ready(function() {
			$('<input type="hidden" name="amount" value="' + ${amount} + '"/>').appendTo('#paymentForm');
			$('#paymentForm').submit();
		});
	</script>
</head>
<body>   
    <!-- Body main wrapper start -->
    <div class="wrapper fixed__footer text-center">
    		<h2>Please wait...</h2>
	    	<form id="paymentForm" method="post" action="https://secure.payu.in/_payment">
			<input name="key" id="key" value="${key}" type="hidden" />
			<!-- <input name="hash_string" id="hash_string" type="hidden"/> -->
			<input type="hidden" name="hash" id="hash" value="${hash}" />
			<input type="hidden" name="txnid" id="txnid" value="${txnId}"/>
			<input type="hidden" name="productinfo" id="productinfo" value='${productInfo}'/>
			<input type="hidden" name="surl" id="surl" value="${surl}"/>
			<input type="hidden" name="furl" id="furl" value="${furl}"/>
			<input type="hidden" name="service_provider" value="payu_paisa"/>
			<input type="hidden" name="firstname" id="firstname" value="${firstName}"/>
			<input type="hidden" name="lastname" id="lastname" value="${lastName}"/>
			<input type="hidden" name="address1"/>
			<input type="hidden" name="address2"/>
			<input type="hidden" name="city" id="city"/>
			<input type="hidden" name="state" id="state"/>
			<input type="hidden" name="country" id="country"/>
			<input type="hidden" name="zipcode" id="zipcode"/>
			<input type="hidden" name="udf1" value="${udf1}"/>
			<input type="hidden" name="udf2" value="${udf1}"/>
			<input type="hidden" name="udf3" value="${udf1}"/>
			<input type="hidden" name="udf4" value="${udf1}"/>
			<input type="hidden" name="udf5" value="${udf1}"/>
			<input type="hidden" name="pg"/>
			
	        <input type="hidden" name="name" id="name" value="${fullName}" disabled>
	        <input type="hidden" name="phone" id="phone" value="${phone}" disabled>
			<input type="hidden" placeholder="Enter Email" name="email" id="email" value="${email}" required>
	    		<%-- <input type="hidden" placeholder="Enter Amount" name="amount" value="${amount}" required> --%>
	  	</form>       
    </div>    
</body>
</html>