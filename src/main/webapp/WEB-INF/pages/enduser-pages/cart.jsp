<%@ include file="/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Provilac - Cart</title>
   </head>
   <body class="home-page" id="top">
   <c:url value="/allCategories" var="allCategoriesUrl" />
   <c:url value="/checkout" var="checkoutUrl" />
   <c:set value="0" var="subTotal"></c:set>
      <section id="cart-details">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="cart-left-panel">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>Shopping Cart</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="" id="shoppingCart">
                     	<c:choose>
                     		<c:when test="${not empty sessionScope.cartItems}">
                     			<c:forEach items="${sessionScope.cartItems}" var="item" varStatus="loopVar">
                     				<div class="cart-left">
			                        <div class="cart-product-summary">
			                           <div class="row">
			                              <div class="col-md-3 col-xs-3">
			                                 <div class="product-image">
			                                    <img src="<c:url value='restapi/getProductCartPicture?id=${item.product.id}'/>">
			                                 </div>
			                              </div>
			                              <div class="col-md-3 col-sm-6 col-xs-12">
			                                 <div class="products-name">
			                                    <span>${item.product.name}</span>
			                                 </div>
			                                 <div class="pro-price">
			                                    <span><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.product.price}"/></span>
			                                 </div>
			                              </div>
			                              <div class="col-md-6 col-xs-12">
			                                  <div class="row">
			                                      <div class="col-md-8 col-xs-6">
			                                          <div class="input-group btn-incre1">
					                                    <span class="input-group-btn">
						                                    <button type="button" class="quantity-left-minus btn btn-number btn-pm" onclick="javascript:decreaseQty('${item.product.name}','${item.product.code}')" data-type="minus" data-field="">
						                                    <span class="fa fa-minus"></span>
						                                    </button>
					                                    </span>
					                                    <input type="text" id="cartItemQty_${item.product.code}" name="quantity" class="form-control input-number" value="${item.quantity}" onchange="changeQtyWithSummory('${item.product.name}','${item.product.code}')" maxlength="1">
					                                    <span class="input-group-btn">
						                                    <button type="button" class="quantity-right-plus btn btn-number btn-pm" onclick="javascript:increaseQty('${item.product.name}','${item.product.code}')" data-type="plus" data-field="">
						                                    <span class="fa fa-plus"></span>
						                                    </button>
					                                    </span>
					                                 </div>
			                                      </div>
			                                      <div class="col-md-4 col-xs-6">
					                                 <div class="delete-product">
					                                 	<a href="javascript:deleteItemFromCartView('${item.product.name}', '${item.product.code}')">
					                                    	<i class="fa fa-trash-o"></i>
					                                    </a>
					                                 </div>
					                              </div>
			                                  </div>
			                              </div>
			                           </div>
			                        </div>
			                     </div>
                     			</c:forEach>
                     		</c:when>
                     		<c:otherwise>
                     			<p style="padding:15px; margin:0px"> No items in cart </p>
                     		</c:otherwise>
                     	</c:choose>	
                     </div>
                  </div>
                  <div class="add-more-product">
                  	<a href="${allCategoriesUrl}" class="read-more">Add More Products +</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="cart-right-panel">
                     <div class="cart-right">
                        <div class="cart-head">
                           <h3>Summary</h3>
                        </div>
                     </div>
                     <hr>
                     <div class="" id="shoppingCartSummary">
	                     <div class="cart-right" id="checkoutProductSummary_${item.product.code}">
	                      <c:forEach items="${sessionScope.cartItems}" var="item" varStatus="loopVar">
	                        <div class="cart-summary">
	                           <span class="product-name">${item.product.name} x <span style="font-weight: bold;" id="checkoutProductQty_${item.product.code}">${item.quantity}</span></span>
	                           <span class="product-prices" id="checkoutProductPrice_${item.product.code}"><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.product.price * item.quantity}"/></span>
	                           <c:set value="${(item.product.price * item.quantity) + subTotal}" var="subTotal"></c:set>
	                           <input id="cartItemUnitPrice_${item.product.code}" type="hidden" value="${item.product.price}">
	                        </div>
	                        </c:forEach>
	                     </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="cart-summary">
                           <span class="product-name">Final Amount</span>
                           <span class="product-prices" id="checkoutFinalAmount" ><i class="fa fa-inr"></i> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${subTotal}"/></span>
                        </div>
                     </div>
                     <hr>
                     <div class="cart-right">
                        <div class="checkout-btn">
                           <a href="${checkoutUrl}">Checkout</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript">
      function increaseQty(productName, productCode) {
			var currentQty = parseInt($('#cartItemQty_' + productCode).val());
			if(currentQty > 3) {
				return;
			}
			var qty = currentQty + 1;
			if(qty > 1){
				$('#snackbar').text('Order for ' + productName +' changed to '+ qty +' units.');
			} else {
				$('#snackbar').text('Order for ' + productName +' changed to '+ qty +' unit.');
			}
			
			addToCart(productName, productCode, qty, function(response) {
				updateProductSummaryUI(productCode, qty);
				updateCheckoutUI(response); 
			});
		}

		function decreaseQty(productName, productCode) {
			var currentQty = parseInt($('#cartItemQty_' + productCode).val());
			if(currentQty == 1) {
				return;
			}
			var qty = currentQty - 1;
			if(qty > 1){
				$('#snackbar').text('Order for ' + productName +' changed to '+ qty +' units.');
			} else {
				$('#snackbar').text('Order for ' + productName +' changed to '+ qty +' unit.');
			}
			addToCart(productName, productCode, qty, function(response) {
				updateProductSummaryUI(productCode, qty);
				updateCheckoutUI(response);
			});
		}
		
		function updateProductSummaryUI(productCode, qty) {
  			
  			$('#cartItemQty_' + productCode).val(qty);
			var pricePerUnit = $('#cartItemUnitPrice_' + productCode).val();
			var price = (qty * pricePerUnit).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
			
			$('#checkoutProductQty_' + productCode).text('' + qty + '');
			$('#checkoutProductPrice_' + productCode).text('');
			$('#checkoutProductPrice_' + productCode).append('<i class="fa fa-inr"></i> ' + price + '');
  		}
		
		function updateCheckoutUI(response) {
  			var cart = response.data.cart;
			var subTotal = 0.00;
			for(var i = 0; i < cart.length; i++) {
				var cartItem = cart[i];
				subTotal = subTotal + (cartItem.quantity * cartItem.product.price);
			}
			$('#checkoutFinalAmount').text('');
			$('#checkoutFinalAmount').append('<i class="fa fa-inr"></i> ' + (subTotal).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));				
  		}
		
		function changeQtyWithSummory(productName, productCode){
			var currentQty = parseInt($('#cartItemQty_' + productCode).val());
			var regex = /[^0-9]/;
			if (regex.test(currentQty)) { 
				toastr.warning('Please enter valid quantity');	
				return;
			}
		  	if (currentQty < 1 || currentQty > 4) {
		  		toastr.warning('Quantity should be have between 1-4 only');
		  		return;
		  	}
		  	
			if(currentQty > 1){
				$('#snackbar').text('Order for ' + productName +' changed to '+ currentQty +' units.');
			} else {
				$('#snackbar').text('Order for ' + productName +' changed to '+ currentQty +' unit.');
			}
  			addToCart(productName, productCode, currentQty, function(response) {
  				updateProductSummaryUI(productCode, currentQty);
  				updateCheckoutUI(response);
  			});
		}
		
		function deleteItemFromCartView(productName, productCode){	
			
			deleteItemFromCart(productName, productCode, function(response) {
				
				$('#shoppingCart').empty();
				
				var cart = response.data.cart;
				var totalItems = 0;
				for(var i = 0; i < cart.length; i++) {
					var cartItem = cart[i];
					var template = _.template($('#shoppingCartItemTemplate').html());
					var html = template({'cartItem':cartItem});
					
					$('#shoppingCart').append(html);
					totalItems = totalItems + cartItem.quantity;
				}
				
				$('#shoppingCartSummary').empty();
				for(var i = 0; i < cart.length; i++) {
					var cartItem = cart[i];
					var amount = cartItem.quantity * cartItem.product.price;
					var template = _.template($('#shoppingCartItemSummaryTemplate').html());
					var html = template({'cartItem':cartItem, 'amount':amount});
					
					$('#shoppingCartSummary').append(html);
				}
				
				if(totalItems == 0) {
					$('#shoppingCart').append('No items in cart')
				}
				
				$('#checkoutProductSummary_' + productCode).remove();
				updateCheckoutUI(response);
			});
		}		
		
      </script>
   </body>
</html>