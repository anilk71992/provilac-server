<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html lang="">
   <head>
      <title>Provilac - Hold Delivery</title>
      <script type="text/javascript">
		function submitForm() {
			
			if( !$('#fromDate').val()|| $('#fromDate').val()==""){
				toastr.warning("Please choose fromDate !");
		       	($("#fromDate")).focus();
		        return;
			}
			
			var currentDate = moment($('#today').val());
    	  	var fromDate = moment($('#fromDate').val());
    	  	if(moment(currentDate).isAfter(fromDate) || $('#today').val() == $('#fromDate').val()){
	   		  	toastr.warning('Please select FromDate later than current date');
	  	      	($("#fromDate")).focus();
	  	      	return;
	  	    }
    	  	
    	  	if( !$('#toDate').val()|| $('#toDate').val()==""){
				toastr.warning("Please choose toDate !");
		       	($("#toDate")).focus();
		        return;
			}
			
    	  	var toDate = moment($('#toDate').val());
    	  	if(moment(currentDate).isAfter(toDate) || $('#today').val() == $('#toDate').val()){
	   		  	toastr.warning('Please select toDate later than current date');
	  	      	($("#toDate")).focus();
	  	      	return;
	  	    }
    	  	
    	  	if(moment(fromDate).isAfter(toDate)){
	   		  	toastr.warning('Please select toDate later than fromDate or same date');
	  	      	($("#toDate")).focus();
	  	      	return;
	  	    }
    	  	
			
			$('<input type="hidden" name="fromDate" value="' + $('#fromDate').val() + '"/>').appendTo('#holdScheduleForm');
			$('<input type="hidden" name="toDate" value="' + $('#toDate').val() + '"/>').appendTo('#holdScheduleForm');
			$('#holdScheduleForm').submit();		
		}
      	
      </script>
   </head>
   <body class="home-page" id="top">
      <section id="cart-details">
         <div class="container">
            <div class="row">
			   <div>
				   <c:if test="${not empty message}">
				   	   <center><div id="message" class="success">${message}</div></center>
				   </c:if>			
			   </div>
               <div class="col-md-12">
                  <div class="hold-delivery-panel">
                     <div class="cart-left">
                        <div class="cart-head">
                           <h3>Hold Delivery</h3>
                        </div>
                     </div>
                     <hr>
                     <input class="form-control" id="today" value="${today}" type="hidden"/>
                     <div class="cart-left">
                         <div class="row">
                             <div class="col-md-6">
                             <div class="address-head">
                           <h2>From Date</h2>
                        </div>
                        <div class="form-group date-selection">
                           <div class="col-xs-12 col-sm-8 col-md-10">
                              <div class="input-group">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                 </div>
                                 <input class="form-control" id="fromDate" name="fromDate" placeholder="YYYY-MM-DD" type="text"/>
                              </div>
                           </div>
                        </div>
                             </div>
                             <div class="col-md-6">
                             <div class="address-head">
                           <h2>To Date</h2>
                        </div>
                        <div class="form-group date-selection">
                           <div class="col-xs-12 col-sm-8 col-md-10">
                              <div class="input-group">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input class="form-control" id="toDate" name="toDate" placeholder="YYYY-MM-DD" type="text"/>
                              </div>
                           </div>
                        </div>
                        </div>
                         </div>
                     </div>
                     <hr>
                     <div class="cart-left">
                          <strong>Note :</strong><p>If you want to STOP or HOLD delivery for more than 15 days, Please contact us <strong style="color: #d74123">1800-002-564</strong></p>
                     </div>
                     <div class="cart-left">
	                     <div class="checkout-btn">
	                     	<a href="javascript:submitForm();">hold delivery</a>
	                     </div>
	                     <c:url value="/end-user/hold-delivery" var="holdScheduleUrl" />
                     	<form action="${holdScheduleUrl}" id="holdScheduleForm"></form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
      <script>
         $(document).ready(function(){
         	var fromDate_input=$('input[name="fromDate"]'); //our fromDate input has the name "fromDate"
         	var toDate_input=$('input[name="toDate"]'); //our toDate input has the name "toDate"
         	var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
         	fromDate_input.datepicker({
         		format: 'yyyy-mm-dd',
         		container: container,
         		todayHighlight: true,
         		autoclose: true,
         	})
         	
         	toDate_input.datepicker({
         		format: 'yyyy-mm-dd',
         		container: container,
         		todayHighlight: true,
         		autoclose: true,
         	})
         	
         })
      </script>
   </body>
</html>