<%@ include file="/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>Provilac - Subscribe</title>
<script type="text/javascript">
var selectedProduct;
function onProductSelected(cipher) {
	selectedProduct = cipher;
}

function submitForm() {
	
	if(selectedProduct == null || selectedProduct == '') {
		alert('Please select a product to subscribe');
		return;
	}
	
	if(!$('#subscriptionDuration').val()) {
		alert('Please select subscription duration');
		return;
	}
	
	$('<input type="hidden" name="productCipher" value="' + selectedProduct + '"/>').appendTo('#productForm');
	$('<input type="hidden" name="subscriptionDuration" value="' + $('#subscriptionDuration').val() + '"/>').appendTo('#productForm');
	$('#productForm').submit();		
}
</script>
</head>
<body>
	<c:url value="/select-subscription-pattern" var="patternForPrePayUrl" />
	<!-- Body main wrapper start -->
	<div class="wrapper fixed__footer">
		<!-- Start Login Register Area -->
		<div class="htc__login__register bg__white ptb--130" style="background: rgba(0, 0, 0, 0) url(assets/endUser-assets/images/bg/5.jpg) no-repeat scroll center center / cover ;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="login-head">
							<h2>Prepay Subscription</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Login Register Area -->

		<!-- Start Our Product Area -->
		<section class="htc__product__area ptb--130 bg__white">
			<div class="container">
				<div class="htc__product__container">
					<!-- Start Login Register Content -->
					<div class="row">
						<div class="col-md-8 col-md-offset-3">
							<div class="quantity">
								<c:choose>
									<c:when test="${empty sessionScope.prepayDuration}">
										<label>Select Your Duration: </label>
										<select id="subscriptionDuration" class="selectpicker select" style="width: 50%;">
											<option value="" selected="">Select Duration</option>
											<option value="3">3 MONTHS</option>
											<option value="6">6 MONTHS</option>
											<option value="9">9 MONTHS</option>
											<option value="12">12 MONTHS</option>
										</select>
									</c:when>
									<c:otherwise>
										<label>Selected Duration:</label>
										<select id="subscriptionDuration" class="selectpicker select" style="width: 50%;">
											<option value="${sessionScope.prepayDuration}">${sessionScope.prepayDuration} MONTHS</option>
										</select>							
									</c:otherwise>								
								</c:choose>
							</div>
						</div>
					</div>
					<!-- End Login Register Content -->
					<div class="row">
						<div class="col-xs-12">
							<div class="section__title section__title--2 text-center">
								<h2 class="title__line">Select Product</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="product__list another-product-style">
							<!-- Start Single Product -->
							<c:forEach items="${products}" var="product">
								<c:if test="${product.availableSubscription}">
									<div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-4 col-xs-12" onclick="onProductSelected('${product.cipher}')">
										<div class="product foo">
											<div class="product__inner1">
												<div class="pro__thumb1">
													<a><img src="<c:url value="/assets/endUser-assets/images/product/1.png"/>" alt="product images" id="image-value"></a>
												</div>
											</div>
											<div class="product__details">
												<h2>
													<a>${product.name}</a>
												</h2>
												<ul class="product__price">
													<!-- <li class="old__price">$16.00</li>-->
													<li class="new__price">Rs.${product.price}</li>
												</ul>
											</div>
										</div>
									</div>
								</c:if>
							</c:forEach>
							<!-- End Single Product -->
						</div>
					</div>
					<div class="next-btn">
						<a href="javascript:submitForm()"><button type="button" class="btn btn-default city-button" data-dismiss="modal">Next</button></a>
					</div>
					<form action='<c:url value="/select-subscription-pattern"/>' id="productForm">
					</form>
				</div>
			</div>
		</section>
		<!-- End Popular Courses -->
	</div>
</body>
</html>