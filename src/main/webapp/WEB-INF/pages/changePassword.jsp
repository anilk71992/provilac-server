<%@ include file="/taglibs.jsp"%>
<html>
<head>
<title>provilac - Change Password</title>

<script type="text/javascript">

$(document).ready(function() {
	$('#submitButton').click(function() {
		if (validate()) {
			$('#changePassword').submit();
		}
	});	
});

function validate() {
	if (!$('#oldPassword').val()) {
		alert('Please enter your old password');
		return false;
	}
	if (!$('#newPassword').val()) {
		alert('Please enter your new password');
		return false;
	}
	if (!$('#newPasswordConfirm').val()) {
		alert('Please confirm your new password');
		return false;
	}
	if ($('#newPassword').val() != $('#newPasswordConfirm').val()) {
		alert('New password / Confirm password do not match');
		return false;		
	}
	
	return true;	
}

</script>

</head>



<body>
<c:url value="/access/changePassword" var="changePassword" />
<sec:authorize access="isAnonymous()">
    <c:redirect url="/login"/>
</sec:authorize>
<form id="changePassword" method="post" action="${changePassword}">
            <div>
                <c:if test="${not empty message}">
                    <div id="message" class="success">${message}</div>  
                </c:if>
               
            </div>
            <fieldset>
            	<h3 class="page-header">Change Password</h3>
            
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Old Password</span>
				 <input	class="form-control" name="oldPassword" id="oldPassword" type="password" class="form-control">
			</div>
			<br>
			<div class="input-group width-xlarge">
				<span class="input-group-addon">New Password</span>
				 <input	class="form-control" name="newPassword" id="newPassword" type="password" class="form-control">
			</div>
			<br>
			<div class="input-group width-xlarge">
				<span class="input-group-addon">Confirm New Password</span>
				 <input	class="form-control" name="newPasswordConfirm" id="newPasswordConfirm" type="password" class="form-control">
			</div>

            </fieldset>
            <br>
           	<a class="btn btn-info" id="submitButton" type="submit">Change Password</a>&nbsp;&nbsp;&nbsp;&nbsp;
           	<a class="btn btn-danger" href="javascript:history.back(1)">Cancel </a>

</body>