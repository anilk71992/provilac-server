// SMOOTH SCROLL
$(document.body).on('click', 'a', function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

//Free-Sample Popup Form on Scroll
$(".popup-on-scroll").subscribeBetter({
  trigger: "onidle", 
  animation: "fade",
  delay: 0,
  showOnce: true,
  autoClose: false,
  scrollableModal: false
});

/*$('.message').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});*/


$('.cart-left-panel .cart-left .delivery-address').click(function() {
   // Set the form value
   $('#address-value').val($(this).attr('data-value'));

   // Unhighlight all the images
   $('.cart-left-panel .cart-left .delivery-address').removeClass('highlighted');

   // Highlight the newly selected image
   $(this).addClass('highlighted');
});

$('.subscription-bill .cart-left .delivery-address').click(function() {
	   // Set the form value
	   $('#address-value').val($(this).attr('data-value'));

	   // Unhighlight all the images
	   $('.subscription-bill .cart-left .delivery-address').removeClass('highlighted');

	   // Highlight the newly selected image
	   $(this).addClass('highlighted');
	});


var quantitiy=0;
   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
            
            $('#quantity').val(quantity + 1);
       // Increment
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
      
            // Increment
            if(quantity>0){
            $('#quantity').val(quantity - 1);
            }
    });
     
     
     /*jQuery('#optionsRadios1').click(function() {
	 jQuery('#pattern-daily').show();
	 jQuery('#pattern-weekdays').hide();
	 jQuery('#pattern-alternate').hide();
	 });
	 jQuery('#optionsRadios2').click(function() {
	 jQuery('#pattern-daily').hide();
	 jQuery('#pattern-weekdays').show();
	 jQuery('#pattern-alternate').hide();
	 });
	 jQuery('#optionsRadios3').click(function() {
	 jQuery('#pattern-daily').hide();
	 jQuery('#pattern-weekdays').hide();
	 jQuery('#pattern-alternate').show();
	 });
	 jQuery('#optionsRadios1').click(function(){
	 jQuery('.selector').prop('checked', true);
	 });
	 jQuery('.selector').change(function(){
	 jQuery('#op5').prop('checked',
	 jQuery('.selector:checked').length == jQuery('.selector').length);
	 });*/