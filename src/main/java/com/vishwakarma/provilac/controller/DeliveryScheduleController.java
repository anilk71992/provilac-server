package com.vishwakarma.provilac.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.DeliveryLineItemDTO;
import com.vishwakarma.provilac.dto.DeliveryScheduleDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.mvc.validator.DeliveryLineItemValidator;
import com.vishwakarma.provilac.mvc.validator.DeliveryScheduleValidator;
import com.vishwakarma.provilac.property.editor.InvoicePropertyEditor;
import com.vishwakarma.provilac.property.editor.RoutePropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.InvoiceRepository;
import com.vishwakarma.provilac.repository.RouteRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.DeliveryLineItemService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class DeliveryScheduleController {

	@Resource
	private DeliveryScheduleValidator deliveryScheduleValidator;

	@Resource
	private DeliveryScheduleService deliveryScheduleService;

	@Resource
	private UserService userService;
	
	@Resource
	private InvoiceRepository invoiceRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private DeliveryLineItemService deliveryLineItemService;
	
	@Resource 
	private RouteRecordService routeRecordService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRepository routeRepository;
	
	@Resource
	private DeliveryLineItemValidator deliveryLineItemValidator;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource 
	private ActivityLogService activityLogService;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@InitBinder(value = "deliverySchedule")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Invoice.class, new InvoicePropertyEditor(invoiceRepository));
		binder.registerCustomEditor(Route.class, new RoutePropertyEditor(routeRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(deliveryScheduleValidator);
	}

	private Model populateModelForAdd(Model model, DeliverySchedule deliverySchedule) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("deliverySchedule", deliverySchedule);
		model.addAttribute("users", userService.getUsersByRoleAndStatusNotEqualToActive( Status.ACTIVE,Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("invoices", invoiceService.getAllInvoices(loggedInUser, false, false));
		model.addAttribute("routes", routeService.getAllRoutes(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/deliverySchedule/add")
	public String addDeliverySchedule(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new DeliverySchedule());
		return "admin-pages/deliverySchedule/add";
	}

	@RequestMapping(value = "/admin/deliverySchedule/add", method = RequestMethod.POST)
	public String deliveryScheduleAdd(DeliverySchedule deliverySchedule, 
									BindingResult formBinding, 
									Model model,
									@RequestParam(required=false,value="product")String productCodes[],
									@RequestParam(required=false, value="quantity")Integer quantity[],
									@RequestParam(required=false,value="customer1")String customerCode,
									@RequestParam(required=true,value="days")int days){

	
		List<DeliverySchedule> deliverySchedules = new ArrayList<DeliverySchedule>();
		if(null!=productCodes){

			for (int i = 0; i < productCodes.length; i++) {
				for (int j = 1; j < productCodes.length; j++) {
					if(i!=j){
						if(productCodes[i].equals(productCodes[j])){
							String message = " Products should not be overlapped";
							return "redirect:/admin/deliverySchedule/list?message=" + message;
						}
					}
				}
			}
			
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customerCode, true, null, false, false);
			if(null != userSubscription && userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				String message = " Can not add delivery schedule for prepaid customer.";
				return "redirect:/admin/deliverySchedule/list?message=" + message;
			}
			
			Date startDate = deliverySchedule.getDate();
			DateHelper.setToStartOfDay(startDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.add(Calendar.DATE, days-1);
			Date endDate = calendar.getTime();
			DateHelper.setToEndOfDay(endDate);
			
			
			DeliverySchedule schedule = null;
			for (;startDate.before(endDate);) {
				 schedule= new DeliverySchedule();
				 schedule.setDate(startDate);
				 schedule.setType(deliverySchedule.getType());
				 schedule.setCustomer(deliverySchedule.getCustomer());
				 schedule.setRoute(deliverySchedule.getRoute());
				 
				for (int i = 0; i < productCodes.length; i++) { 
					String productCode = productCodes[i];
					Product product = productService.getProductByCode(productCode, false, null, false, false);
					
					int quantity1= quantity[i];
					if(quantity1!=0){
						DeliveryLineItem lineItem = new DeliveryLineItem();
						lineItem.setQuantity(quantity1);
						lineItem.setProduct(product);
						
						lineItem.setPricePerUnit(product.getPrice());
						lineItem.setTotalPrice(product.getPrice() * quantity1);
						
						schedule.getDeliveryLineItems().add(lineItem);
					}
				}
				deliverySchedules.add(schedule);
				startDate = DateHelper.addDays(startDate, 1);
			}
		}
		RouteRecord record=null;
		if(StringUtils.isNotBlank(customerCode) || StringUtils.isNotEmpty(customerCode)){
		 record = routeRecordService.getRouteRecordByCustomer(customerCode, false, null, false, false);
		}
		for (DeliverySchedule schedule : deliverySchedules) {
			if(null != record){

				List<DeliverySchedule> deliverySchedules2 = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(schedule.getRoute().getId(), schedule.getDate(), record.getPriority()+1, null, false, false);
				if(!deliverySchedules2.isEmpty()){
					List<DeliverySchedule> schedules = deliveryScheduleService.getScheduleByRouteAndDate(schedule.getRoute().getId(), schedule.getDate(),record.getPriority()+1, null, false, false);
					for (DeliverySchedule deliverySchedule2 : schedules) {
						deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
						deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
					}
				}
				schedule.setPriority(record.getPriority()+1);
			}else{
				schedule.setPriority(1);
			}
			User loggedInUser = userService.getLoggedInUser();
			schedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
			deliveryScheduleService.createDeliverySchedule(schedule, loggedInUser, true,false);
		}
	
		String message = "DeliverySchedule added successfully";
		return "redirect:/admin/deliverySchedule/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deliverySchedule/list")
	public String getDeliveryScheduleList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<DeliverySchedule> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = deliveryScheduleService.searchDeliverySchedule(pageNumber, searchTerm, user,true,false);
		} else {
			page = deliveryScheduleService.searchDeliveryScheduleByType(pageNumber, Type.Normal, user, true, false);
			 
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("deliverySchedules", page.getContent());

		return "/admin-pages/deliverySchedule/list";
	}

	@RequestMapping(value = "/admin/deliverySchedule/show/{cipher}")
	public String showDeliverySchedule(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedIUser,true,false);
		model.addAttribute("deliverySchedule", deliverySchedule);
		return "/admin-pages/deliverySchedule/show";
	}

	@RequestMapping(value = "/admin/deliverySchedule/update/{cipher}")
	public String updateDeliverySchedule(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedInUser,true,true);
		model = populateModelForAdd(model, deliverySchedule);
		model.addAttribute("id", deliverySchedule.getId());
		model.addAttribute("version", deliverySchedule.getVersion());
		return "/admin-pages/deliverySchedule/update";
	}

	//MAYBE_UNUSED
	@Deprecated
	@RequestMapping(value = "/admin/deliverySchedule/update", method = RequestMethod.POST)
	public String deliveryScheduleUpdate(DeliverySchedule deliverySchedule, 
										BindingResult formBinding, 
										Model model,
										@RequestParam(required=false,value="product")String productCodes[],
										@RequestParam(required=false, value="quantity")Integer quantity[],
										@RequestParam(required=false,value="customer1")String customerCode,
										@RequestParam(required = false) String errorMessage) {
		
		
		DeliverySchedule deliverySchedule2 = deliveryScheduleService.getDeliverySchedule(Long.parseLong(model.asMap().get("id") + ""), true, userService.getLoggedInUser(), false, true);
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(deliverySchedule2.getCustomer().getCode(), true, null, false, false);
		if(null != userSubscription && userSubscription.getSubscriptionType() == UserSubscription.SubscriptionType.Prepaid) {
			return "redirect:/admin/deliverySchedule/list?message=Can not edit delivery of prepaid customer from here";
		}
		
		List<DeliveryLineItem> deliveryLineItems = deliveryLineItemService.getAllDeliveryLineItemsByDeliverySchedule(deliverySchedule2.getId(), null, false, false);
		if(deliveryLineItems.size()!=0){
			deliveryLineItemService.deleteAllDeliveryLineItem(deliveryLineItems);
		}
		Set<DeliveryLineItem> deliveryLineItems1 = new HashSet<DeliveryLineItem>();
		DeliveryLineItem lineItem = null;
		for (int i = 0; i < productCodes.length; i++) {
			String productCode = productCodes[i];
			int quantity1= quantity[i];
			if(null!=productCodes){
				lineItem = new DeliveryLineItem();
				lineItem.setQuantity(quantity1);
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				lineItem.setProduct(product);
				
				lineItem.setPricePerUnit(product.getPrice());
				lineItem.setTotalPrice(product.getPrice() * quantity1);
				
				lineItem.setDeliverySchedule(deliverySchedule2);
				BindingResult bindingResult = new DataBinder(lineItem).getBindingResult();
				deliveryLineItemValidator.validate(lineItem, bindingResult);
				if(bindingResult.hasErrors()){
					String errorStr = null;
					for (ObjectError objectError : bindingResult.getAllErrors()) {
						if(StringUtils.isBlank(errorStr)){
							errorStr = objectError.getDefaultMessage();
						}else{
							errorStr = errorStr+ ""+ objectError.getDefaultMessage();
						}
					}
					model = populateModelForAdd(model, deliverySchedule);
					return "redirect:/admin/deliverySchedule/list?message=" + errorStr;
				}else{
					deliveryLineItemService.createDeliveryLineItem(lineItem, null, false, false);
					deliveryLineItems1.add(lineItem);
				}
			}
		}
		User loggedInUser = userService.getLoggedInUser();
		deliverySchedule.setId(Long.parseLong(model.asMap().get("id") + ""));
		deliverySchedule.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		
		if(null!=customerCode){
			RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customerCode, false, null, false, false);
			if(null!=routeRecord){
				List<DeliverySchedule>schedules3 = deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule2.getPriority()+1, null, false, false);
				if(schedules3.size()!=0){
					for (DeliverySchedule schedule3 : schedules3) {
						if(!deliverySchedule2.getCode().equals(schedule3.getCode()) && !(schedule3.getPriority()<=1)){
							schedule3.setPriority(schedule3.getPriority()-1);
							deliveryScheduleService.updateDeliverySchedule(schedule3.getId(), schedule3, loggedInUser, false, false);
						}

					}
				}
				deliverySchedule.setRoute(routeRecord.getRoute());
				deliverySchedule.setPriority(routeRecord.getPriority()+1);
				List<DeliverySchedule>schedules2 = deliveryScheduleService.getScheduleByRouteAndDate(routeRecord.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule.getPriority(), null, false, false);
				if(schedules2.size()!=0){
					for (DeliverySchedule schedule2 : schedules2) {
						if(!deliverySchedule2.getCode().equals(schedule2.getCode())){
							schedule2.setPriority(schedule2.getPriority()+1);
							deliveryScheduleService.updateDeliverySchedule(schedule2.getId(), schedule2, loggedInUser, false, false);
						}
					}
				}
				deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser,true,false);
				
			}
		}else{
			deliverySchedule.setRoute(deliverySchedule2.getRoute());
			deliverySchedule.setPriority(deliverySchedule2.getPriority());
		}
		deliverySchedule.getDeliveryLineItems().clear();
		deliverySchedule.setDeliveryLineItems(deliveryLineItems1);
		deliverySchedule.setUpdateOperation(true);
		deliveryScheduleValidator.validate(deliverySchedule, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deliverySchedule);
			return "admin-pages/deliverySchedule/update";
		}
		
		deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "DeliverySchedule updated successfully";
		return "redirect:/admin/deliverySchedule/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deliverySchedule/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			deliveryScheduleService.deleteDeliverySchedule(id);
			message = "DeliverySchedule deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete DeliverySchedule";
		}
		return "redirect:/admin/deliverySchedule/list?message=" + message;
	}
	/*
	 * Undelivered Order
	 */
	
	@RequestMapping(value = "/admin/undeliveredOrder/list")
	public String getUndeliveredOrderList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<DeliverySchedule> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = deliveryScheduleService.searchDeliverySchedule(pageNumber, searchTerm, user,true,false);
		} else {
			page = deliveryScheduleService.searchDeliveryScheduleByStatus(pageNumber, DeliveryStatus.NotDelivered, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("deliverySchedules", page.getContent());

		return "/admin-pages/undeliveredOrder/list";
	}
	
	@RequestMapping(value = "/admin/undeliveredOrder/show/{cipher}")
	public String showUndeliveredOrder(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedIUser,true,false);
		model.addAttribute("deliverySchedule", deliverySchedule);
		return "/admin-pages/undeliveredOrder/show";
	}
	
	@RequestMapping(value = "/admin/undeliveredOrder/update/{cipher}")
	public String updateUndeliveredOrder(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, deliverySchedule);
		model.addAttribute("id", deliverySchedule.getId());
		model.addAttribute("version", deliverySchedule.getVersion());
		return "/admin-pages/undeliveredOrder/update";
	}

	@RequestMapping(value = "/admin/undeliveredOrder/update", method = RequestMethod.POST)
	public String undeliveredOrderUpdate(DeliverySchedule deliverySchedule, 
										BindingResult formBinding, 
										Model model) {

		deliverySchedule.setUpdateOperation(true);
		deliveryScheduleValidator.validate(deliverySchedule, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deliverySchedule);
			return "admin-pages/undeliveredOrder/update";
		}
		deliverySchedule.setId(Long.parseLong(model.asMap().get("id") + ""));
		deliverySchedule.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "DeliverySchedule updated successfully";
		return "redirect:/admin/undeliveredOrder/list?message=" + message;
	}
	
	@RequestMapping(value="/admin/reports/list")
	public String getDeliveryScheduleReports(Model model,
											@RequestParam(required = false) String date){
		

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		if(null == date){
			return "/admin-pages/reports/list";
		}
		Date selectedDate = DateHelper.parseDate(date);
		Map<Product, Integer> productQuantity = new HashMap<Product, Integer>();
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByDate(selectedDate, loggedInUser, true, true);
		double totalQuantity = 0;
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			for(DeliveryLineItem deliveryLineItem:deliverySchedule.getDeliveryLineItems()){
				
				if(productQuantity.containsKey(deliveryLineItem.getProduct())){
					if(productQuantity.isEmpty()){
						productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
					}else{
						productQuantity.put(deliveryLineItem.getProduct(), (productQuantity.get(deliveryLineItem.getProduct()) != null?productQuantity.get(deliveryLineItem.getProduct()):0)+deliveryLineItem.getQuantity());
					}
					totalQuantity +=deliveryLineItem.getQuantity();
				}else{
					totalQuantity +=deliveryLineItem.getQuantity(); 
					productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
				}
			}
		}
		model.addAttribute("date", date);
		model.addAttribute("products", productQuantity);
		model.addAttribute("totalQuntity", totalQuantity);
		
		return "/admin-pages/reports/list";
	}

	@RequestMapping(value = "/admin/reports/show")
	public String showDeliveryScheduleReports(Model model) {
		return "/admin-pages/reports/show";
	}
	/*
	 * DeliverySchedule Calender
	 */
	
	@RequestMapping(value = "/admin/deliverySchedule/showCalender")
	public String showDeliverySchedule(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("subscriptionTypes", SubscriptionType.values());
		return "/admin-pages/deliverySchedule/showCalender";
	}
	
	/**
	 * Get Calendar for new Customers
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/deliverySchedule/newCustomerCalendar")
	public String showDeliverySchedulesForNewCustomer(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("subscriptionTypes", SubscriptionType.values());
		return "/admin-pages/deliverySchedule/newCustomerCalendar";
	}
	
	/**
	 * Get Calendar for stop customers
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/deliverySchedule/stopCustomerCalendar")
	public String showDeliverySchedulesForStopCustomer(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return "/admin-pages/deliverySchedule/stopCustomerCalendar";
	}
	/*
	 * End  DeliverySchedule Calender
	 */ 
	/*
	
	 * Ajax Method
	 * 
	 */
	
	@RequestMapping(value="/deliverySchedule/getLineItems")
	@ResponseBody
	public ApiResponse getLineItems(@RequestParam(required=true)Long customerId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomer(customerId, null, false, true);
			List<DeliveryScheduleDTO> deliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
				deliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule,deliveryLineItems));
			}
			apiResponse.addData("deliverySchedules", deliveryScheduleDTOs);
			if(userService.getLoggedInUser().hasRole(Role.ROLE_SUPER_ADMIN)){
				apiResponse.addData("isAuthorized", true);
			}else{
				apiResponse.addData("isAuthorized", false);
			}
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	@RequestMapping(value="/deliverySchedule/lineItems")
	@ResponseBody
	public ApiResponse getDeliveryLineItems(@RequestParam(required=true)Long deliveryScheduleId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(deliveryScheduleId, false, null, false, true);
				List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
				List<DeliveryLineItemDTO>deliveryLineItemDTOs = new ArrayList<DeliveryLineItemDTO>();	
				for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
						deliveryLineItemDTOs.add(new DeliveryLineItemDTO(deliveryLineItem));
					}
				apiResponse.addData("lineItems",deliveryLineItemDTOs);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/*
	 * End Ajax Method
	 */

	/**
	 * USED_FROM_DEVICE/MAYBE_UNUSED
	 * REST API Methods Start
	 */
	
	@RequestMapping(value="/restapi/deliverySchedule/updateSheet", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateSheet(@RequestParam(required=true)String date,
									@RequestParam(required=true)String customerCode,
									@RequestParam(required=true)String productCodes[],
									@RequestParam(required=true)Integer quantity[]) throws ParseException{
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customerCode, DateHelper.parseDate(date), false, null, false,true);
			List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
			for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				deliveryLineItemService.deleteDeliveryLineItem(deliveryLineItem.getId());
			}
			
			deliverySchedule.getDeliveryLineItems().clear();
			Set<DeliveryLineItem>lineItems= new HashSet<DeliveryLineItem>();
		
			for (int i = 0; i < productCodes.length; i++) {
				String productCode = productCodes[i];
				int qty = quantity[i];
					
				Date date1=DateHelper.parseDate(date);
				DateFormat format2=new SimpleDateFormat("EEEE"); 
				String day=format2.format(date1);
				
				DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				deliveryLineItem.setProduct(product);
				deliveryLineItem.setQuantity(qty);
				if(day.equals("Sunday"))
					deliveryLineItem.setDay(Day.Day_1);
				if(day.equals("Monday"))
					deliveryLineItem.setDay(Day.Day_2);
				if(day.equals("Tuesday"))
					deliveryLineItem.setDay(Day.Day_3);
				if(day.equals("Wednesday"))
					deliveryLineItem.setDay(Day.Day_4);
				if(day.equals("Thursday"))
					deliveryLineItem.setDay(Day.Day_5);
				if(day.equals("Friday"))
					deliveryLineItem.setDay(Day.Day_6);
				if(day.equals("Saturday"))
					deliveryLineItem.setDay(Day.Day_7);
				
				deliveryLineItem.setDeliverySchedule(deliverySchedule);
				
				deliveryLineItem.setPricePerUnit(product.getPrice());
				deliveryLineItem.setTotalPrice(product.getPrice() * qty);
				
				deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, null, false, false);
				lineItems.add(deliveryLineItem);
			}
			deliverySchedule.setDeliveryLineItems(lineItems);
			deliverySchedule = deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, null, false, true);
			
			apiResponse.addData("deliverySchedule", new DeliveryScheduleDTO(deliverySchedule, lineItems));
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//MAYBE_UNUSED, UNUSED_FOR_SURE
	@Deprecated
	@RequestMapping(value="/restapi/deliverySchedule/getDeliveryScheduleByCustomer")
	@ResponseBody
	public ApiResponse getDeliverySchedules(@RequestParam(required=true)String customerId,
									@RequestParam(required=true)String fromDate,
									@RequestParam(required=true)String toDate,
									@RequestParam(required=false)String deliveryStatus){
		
		ApiResponse apiResponse = new ApiResponse(true);

		try {
			User loggedInUser = userService.getLoggedInUser();
			List<DeliveryScheduleDTO> deliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();

			Date fDate = DateHelper.parseDate(fromDate);
			Date tDate = DateHelper.parseDate(toDate);
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(Long.parseLong(customerId), fDate, tDate,loggedInUser, true, true, null);
			
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
				deliveryLineItems = deliverySchedule.getDeliveryLineItems();
				deliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, deliveryLineItems));
			}
			apiResponse.addData("deliverySchedules", deliveryScheduleDTOs);

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE_SIDE - Internal + EndUser App
	@RequestMapping(value="/restapi/deliverySchedule/getDeliveryScheduleByCustomerbyDateFromlast3MonthTONext3Month")
	@ResponseBody
	public ApiResponse getDeliverySchedule(@RequestParam(required=true)String customerId,
										   @RequestParam(required=true)String date){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try {
			Set<DeliveryScheduleDTO> setofDeliverySchedules =new HashSet<DeliveryScheduleDTO>();
			Map<String, Set<DeliveryScheduleDTO>> map = new HashMap<String, Set<DeliveryScheduleDTO>>();
			Set<String> setOfMonths = new HashSet<String>();

			List<DeliverySchedule> deliverySchedules;
			
			Calendar start = Calendar.getInstance();
			Date last3MonthsofFirstDay = DateHelper.getFirstDatefromDateInPerticularMonth(DateHelper.parseDate(date), -3);
			start.setTime(last3MonthsofFirstDay);

			Calendar end = Calendar.getInstance();
			Date next3MonthsofLastDay = DateHelper.getLastDatefromDateInPerticularMonth(DateHelper.parseDate(date), 3);
			end.setTime(next3MonthsofLastDay);
			
			deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(Long.parseLong(customerId), last3MonthsofFirstDay, next3MonthsofLastDay, null, false, true, null);

			for (Date date1 = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date1 = start.getTime()) {
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(date1);
				String monthName2 = new SimpleDateFormat("MMM yyyy").format(date1);
				setOfMonths.add(monthName2);
			}
			
			Set<DeliveryLineItem> deliveryLineItems =null;
			for (String monthNameString : setOfMonths) {
				setofDeliverySchedules = new HashSet<DeliveryScheduleDTO>();
				for (DeliverySchedule deliverySchedule : deliverySchedules) {
					Calendar cal = Calendar.getInstance();
					cal.setTime(deliverySchedule.getDate());
					String monthName = new SimpleDateFormat("MMM yyyy").format(deliverySchedule.getDate());
					if (monthName.equals(monthNameString)) {
						deliveryLineItems = new HashSet<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
						setofDeliverySchedules.add(new DeliveryScheduleDTO(deliverySchedule, deliveryLineItems));
					}
				}
				map.put(monthNameString, setofDeliverySchedules);
			}
			apiResponse.addData("deliverySchedules",map);

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}
	
	//USED_FROM_DEVICE_SIDE
	@RequestMapping(value="/openapi/deliverySchedule/getDeliveryScheduleByCustomerbyDateFromlast3MonthTONext3MonthForiOS")
	@ResponseBody
	public ApiResponse getDeliveryScheduleForiOS(@RequestParam(required=true)String customerId,
										   @RequestParam(required=true)String date){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try {
			Calendar start = Calendar.getInstance();
			Date last3MonthsofFirstDay = DateHelper.getFirstDatefromDateInPerticularMonth(DateHelper.parseDate(date), -3);
			start.setTime(last3MonthsofFirstDay);

			Calendar end = Calendar.getInstance();
			Date next3MonthsofLastDay = DateHelper.getLastDatefromDateInPerticularMonth(DateHelper.parseDate(date), 3);
			end.setTime(next3MonthsofLastDay);
			
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(Long.parseLong(customerId), last3MonthsofFirstDay, next3MonthsofLastDay, null, false, true, null);
			List<DeliveryScheduleDTO> dtos = new ArrayList<DeliveryScheduleDTO>();
			for(DeliverySchedule deliverySchedule : deliverySchedules) {
				dtos.add(new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
			}
			
			apiResponse.addData("deliverySchedules", dtos);

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}

	/*
	 * AJAX  
	 * @author:vivek
	 * add new Deliveryschedule for single day
	 * 
	 * USED_FROM_WEB
	 * 
	 */
	@RequestMapping(value = "/restapi/addNewDeliverySchedule" ,method =RequestMethod.POST)
	@ResponseBody
	public ApiResponse addNewDeliverySchedule(@RequestParam(required = true) String customerId,
												@RequestParam(required = false) String[] productCodes,
												@RequestParam(required = false) int[] quantity,
												@RequestParam(required = true) String[] dates,
												@RequestParam(required=false) String tempNote,
												@RequestParam(required=false) String permNote,
												@RequestParam(required=false) boolean isFreeTrial){

		ApiResponse apiResponse = new ApiResponse(true);

		try {
			User loggedInUser = userService.getLoggedInUser();
			User customer = userService.getUser(Long.parseLong(customerId), false, loggedInUser, false, false);
			RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customer.getCode(), false, null, false, true);
			
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, null, false, true);
			if(null != userSubscription && userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				
				Date today = new Date();
				DateHelper.setToStartOfDay(today);
				if (productCodes == null || quantity == null || productCodes.length == 0 || quantity.length == 0) {
					//Removing/Deleting Delivery Schedule
					double balanceToAdd = 0, balanceToAddToRemainingPrepay = 0;
					Date nextDeliveryDate = null;
					for (int k = 0; k < dates.length; k++) {
						//TODO: Check if date is before subscription start date
						Date deliveryDate = DateHelper.parseDate(dates[k]);
						DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), deliveryDate, false, loggedInUser, true, true);
						double deliveryScheduleAmount = 0;
						List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
						for (DeliveryLineItem existingDeliveryLineItem : deliveryLineItems) {
							balanceToAdd = balanceToAdd + existingDeliveryLineItem.getTotalPrice();
							deliveryScheduleAmount = deliveryScheduleAmount + existingDeliveryLineItem.getTotalPrice();
							deliveryLineItemService.deleteDeliveryLineItem(existingDeliveryLineItem.getId());
						}
						if(nextDeliveryDate == null || nextDeliveryDate.before(deliveryDate)) {
							nextDeliveryDate = deliveryDate;
						}
						if(deliverySchedule.getDate().getTime() <= today.getTime()) {
							balanceToAddToRemainingPrepay = balanceToAddToRemainingPrepay + deliveryScheduleAmount;
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}
					if(balanceToAdd > 0) {
						System.out.println("Next Delivery Date: " + nextDeliveryDate);
						if(nextDeliveryDate != null) {
							DateHelper.setToStartOfDay(nextDeliveryDate);
							if(nextDeliveryDate.before(new Date())) {
								nextDeliveryDate = new Date();
							}
						}
						customer.setLastPendingDues(customer.getLastPendingDues() + balanceToAdd);
						
						if(balanceToAddToRemainingPrepay > 0) {
							customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balanceToAddToRemainingPrepay);
						}
						
						customer = userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
						asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, nextDeliveryDate == null ? nextDeliveryDate : DateHelper.addDays(nextDeliveryDate, 1));
					}
 				} else {
 					//Editing/Changing Delivery Schedule
 					Map<Long, Double> pricingMap = userSubscription.getProductPricing();
 					double balanceToDeduct = 0, balanceToAdd = 0, balanceToAddToRemainingPrepay = 0;
 					List<DeliverySchedule> schedulesToAdd = new ArrayList<DeliverySchedule>();
 					List<DeliverySchedule> existingSchedulesToDelete = new ArrayList<DeliverySchedule>();
 					
					for (int k = 0; k < dates.length; k++) {
						//TODO: Check if date is before subscription start date
						Date date = DateHelper.parseDate(dates[k]); 
						DeliverySchedule existingSchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), date, false, loggedInUser, true, true);
						double existingScheduleAmount = 0, newDeliveryScheduleAmount = 0;
						if(null != existingSchedule) {
							 for(DeliveryLineItem deliveryLineItem : existingSchedule.getDeliveryLineItems()) {
								 balanceToAdd = balanceToAdd + deliveryLineItem.getTotalPrice();
								 existingScheduleAmount = existingScheduleAmount + deliveryLineItem.getTotalPrice();
							 }
							 existingSchedulesToDelete.add(existingSchedule);
						}
						Set<DeliveryLineItem> lineItemToSave = new HashSet<DeliveryLineItem>();
						for (int i = 0; i < productCodes.length; i++) {
							String productCode = productCodes[i];
							int qty = quantity[i];
							Product product = productService.getProductByCode(productCode, false, null, false, false);
							DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
							deliveryLineItem.setProduct(product);
							
							deliveryLineItem.setQuantity(qty);
							double pricePerUnit = pricingMap.containsKey(product.getId()) ? pricingMap.get(product.getId()) : product.getPrice();
							double lineItemPrice = pricePerUnit * qty;
							deliveryLineItem.setPricePerUnit(pricePerUnit);
							deliveryLineItem.setTotalPrice(lineItemPrice);
							balanceToDeduct = balanceToDeduct + lineItemPrice;
							newDeliveryScheduleAmount = newDeliveryScheduleAmount + lineItemPrice;
							
							lineItemToSave.add(deliveryLineItem);
						}
						DeliverySchedule deliverySchedule = new DeliverySchedule();
						deliverySchedule.setCustomer(customer);
						deliverySchedule.setDate(date);
						deliverySchedule.getDeliveryLineItems().addAll(lineItemToSave);
						deliverySchedule.setRoute(routeRecord.getRoute());
						deliverySchedule.setPriority(routeRecord.getPriority());
						if(null!=userSubscription.getPermanantNote()){
							deliverySchedule.setPermanantNote(userSubscription.getPermanantNote());
						}
						deliverySchedule.setPermanantNote(permNote);
						deliverySchedule.setTempararyNote(tempNote);
						deliverySchedule.setType(Type.Normal);
						deliverySchedule.setUpdatedFromWeb(true);
						deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
						schedulesToAdd.add(deliverySchedule);
						balanceToAddToRemainingPrepay = balanceToAddToRemainingPrepay + (existingScheduleAmount - newDeliveryScheduleAmount);
					}
					
					double finalBalance = customer.getLastPendingDues() + balanceToAdd - balanceToDeduct;
					
					if(finalBalance >= 0) {
						for (DeliverySchedule deliverySchedule : existingSchedulesToDelete) {
							deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
						}
						for (DeliverySchedule deliverySchedule : schedulesToAdd) {
							deliveryScheduleService.createDeliverySchedule(deliverySchedule, loggedInUser, false, false);
						}
						
						customer.setLastPendingDues(finalBalance);
						customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balanceToAddToRemainingPrepay);
						
						customer = userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
						asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, null);
					} else {
						//Delete deliveries starting from last delivery date
						List<DeliverySchedule> deliverySchedulesFromTomorrowOnwards = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(customer.getId(), DateHelper.addDays(new Date(),  1), loggedInUser, false, true);
						Collections.sort(deliverySchedulesFromTomorrowOnwards, new Comparator<DeliverySchedule>() {

							@Override
							public int compare(DeliverySchedule o1, DeliverySchedule o2) {
								//Sort by desc order
								return o2.getDate().compareTo(o1.getDate());
							}
						});
						//Decide on which deliveries to delete
						List<DeliverySchedule> additionSchedulesToDelete = new ArrayList<DeliverySchedule>();
						for (DeliverySchedule deliverySchedule : deliverySchedulesFromTomorrowOnwards) {
							if(existingSchedulesToDelete.contains(deliverySchedule))
								continue;
							for(DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
								finalBalance = finalBalance + deliveryLineItem.getTotalPrice();
							}
							additionSchedulesToDelete.add(deliverySchedule);
							if(finalBalance >= 0) {
								break;
							}
						}
						if(finalBalance >= 0) {
							List<DeliverySchedule> schedulesToDelete = new ArrayList<DeliverySchedule>();
							schedulesToDelete.addAll(existingSchedulesToDelete);
							schedulesToDelete.addAll(additionSchedulesToDelete);
							
							for (DeliverySchedule deliverySchedule : schedulesToDelete) {
								deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
							}
							for (DeliverySchedule deliverySchedule : schedulesToAdd) {
								deliveryScheduleService.createDeliverySchedule(deliverySchedule, loggedInUser, false, false);
							}
							
							customer.setLastPendingDues(finalBalance);
							customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balanceToAddToRemainingPrepay);
							
							customer = userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
							asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, null);
						} else {
							apiResponse.setError("Customer do not have required balance to perform this operation.", "Unsufficient credits");
						}
					}
				}
			} else {
				DeliverySchedule deliverySchedule = null;
				DeliveryLineItem deliveryLineItem = null;
				Set<DeliveryLineItem> setOfdeliveryLineItems = new HashSet<DeliveryLineItem>();
				List<DeliveryScheduleDTO> listOfDeliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();

				String date = null;
				int i = 0;
				for (int k = 0; k < dates.length; k++) {
					date = dates[k];
					deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), DateHelper.parseDate(date), false, loggedInUser, true, true);

					if (productCodes == null || quantity == null || productCodes.length == 0 || quantity.length == 0) {
						List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
						for (DeliveryLineItem existingDeliveryLineItem : deliveryLineItems) {
							deliveryLineItemService.deleteDeliveryLineItem(existingDeliveryLineItem.getId());
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
						
					} else {
						
						if (deliverySchedule == null) {
							deliverySchedule = new DeliverySchedule();
							deliverySchedule.setCustomer(customer);
							deliverySchedule.setDate(DateHelper.parseDate(date));
							deliverySchedule.setDeliveryTime(DateHelper.parseDate(date));
							deliverySchedule.setRoute(routeRecord.getRoute());
							deliverySchedule.setTempararyNote(tempNote);
							deliverySchedule.setPermanantNote(permNote);
							deliverySchedule.setType(isFreeTrial ? Type.Free_Trial : Type.Normal);
							
							List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), routeRecord.getPriority(), null, false, false);
							
							 if(!deliverySchedules.isEmpty()){
								 List<DeliverySchedule> schedules =  new ArrayList<DeliverySchedule>();
								 schedules = deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), routeRecord.getPriority(), null, false, false);
								 if(schedules.size()!= 0){
									 for (DeliverySchedule deliverySchedule2 : schedules) {
										 deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
										 deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
									 }
								 }
							 }
							 deliverySchedule.setPriority(routeRecord.getPriority());
							 deliverySchedule.setUpdatedFromWeb(true);
							 deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
							 deliverySchedule = deliveryScheduleService.createDeliverySchedule(deliverySchedule, null, false, true);
							for (i = 0; i < productCodes.length; i++) {
								String productCode = productCodes[i];
								int qty = quantity[i];
								Product product = productService.getProductByCode(productCode, false, null, false, false);
								deliveryLineItem = new DeliveryLineItem();
								deliveryLineItem.setProduct(product);
								
								deliveryLineItem.setPricePerUnit(product.getPrice());
								deliveryLineItem.setTotalPrice(product.getPrice() * qty);
								
								deliveryLineItem.setQuantity(qty);
								deliveryLineItem.setDeliverySchedule(deliverySchedule);
								deliveryLineItem = deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, null, false, false);
								setOfdeliveryLineItems.add(deliveryLineItem);
							}
							deliverySchedule.setDeliveryLineItems(setOfdeliveryLineItems);
							//send responce when adding new data
							listOfDeliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, setOfdeliveryLineItems));
							
						} else {
							List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
							for (DeliveryLineItem existingDeliveryLineItem : deliveryLineItems) {
								deliveryLineItemService.deleteDeliveryLineItem(existingDeliveryLineItem.getId());
							}
							for (i = 0; i < productCodes.length; i++) {
								String productCode = productCodes[i];
								int qty = quantity[i];
								Product product = productService.getProductByCode(productCode, false, null, false, false);
								deliveryLineItem = new DeliveryLineItem();
								deliveryLineItem.setProduct(product);
								
								deliveryLineItem.setPricePerUnit(product.getPrice());
								deliveryLineItem.setTotalPrice(product.getPrice() * qty);
								
								deliveryLineItem.setQuantity(qty);
								deliveryLineItem.setDeliverySchedule(deliverySchedule);
								deliveryLineItem = deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, null, false, false);
								setOfdeliveryLineItems.add(deliveryLineItem);
							}
							deliverySchedule.setDeliveryLineItems(setOfdeliveryLineItems);
							deliverySchedule.setPermanantNote(permNote);
							deliverySchedule.setTempararyNote(tempNote);
							deliverySchedule.setUpdatedFromWeb(true);
							deliverySchedule = deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, null, false, true);
							listOfDeliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, setOfdeliveryLineItems));
							//apiresponce not needed when adding new data if required send
						}
					}
					//plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear customer,you have Change Your Delivery Schedule On Date:- "+deliverySchedule.getDate());
					firebaseWrapper.sendDeliveryScheduleChangeNotification(deliverySchedule);
					//emailHelper.sendMailForDeliveryScheduleChange(deliverySchedule);
				}
				apiResponse.addData("deliveryscheduleDTO", listOfDeliveryScheduleDTOs);
			}
			
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		
		return apiResponse;
	}
	
	//USED_FROM_WEB showCalendar & newCustomerCalendar
	@RequestMapping(value = "/restapi/getDeliveryScheduleByCustomerAndDate")
	@ResponseBody
	public ApiResponse getDeliveryScheduleByCustomerAndDate(@RequestParam(required = true) String customerId,
															@RequestParam(required = true) String date) {

		ApiResponse apiResponse = new ApiResponse(true);
		DeliveryScheduleDTO deliveryScheduleDTO = new DeliveryScheduleDTO();
		User loggedInUser = userService.getLoggedInUser();
		try {
			User customer = userService.getUser(Long.parseLong(customerId), false, loggedInUser, false, false);
			DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), DateHelper.parseDate(date), false, null, false,true);
			if (deliverySchedule == null ) {
				apiResponse.setSuccess(false);
			}else{
				Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
				deliveryLineItems = deliverySchedule.getDeliveryLineItems();
				deliveryScheduleDTO = new DeliveryScheduleDTO(deliverySchedule, deliveryLineItems);
				apiResponse.addData("deliverySchedule", deliveryScheduleDTO);			
			}

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}
	
	/**
	 * USED_FROM_WEB
	 */
	@RequestMapping(value = "/restapi/deletedDeliverySchedule")
	@ResponseBody
	public ApiResponse deleteDeliverySchedule(@RequestParam(required = true)String customerId,
											  @RequestParam(required = true)String[] dates){

		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			User customer = userService.getUser(Long.parseLong(customerId), false, loggedInUser, false, false);
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, true);
			if(!userSubscriptions.isEmpty() && userSubscriptions.get(0).getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				double balanceToAdd = 0, balanceToAddToRemainingPrepay = 0;
				Date maxDate = null;
				Date today = new Date();
				DateHelper.setToStartOfDay(today);
				for (String date : dates) {
					Date deliveryDate = DateHelper.parseDate(date);
					DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), deliveryDate, false, null, false, true);
					if (deliverySchedule != null) {
						double deliveryScheduleAmount = 0;
						for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
							balanceToAdd = balanceToAdd + lineItem.getTotalPrice();
							deliveryScheduleAmount = deliveryScheduleAmount + lineItem.getTotalPrice();
						}
						if(deliverySchedule.getDate().getTime() <= today.getTime()) {
							balanceToAddToRemainingPrepay = balanceToAddToRemainingPrepay + deliveryScheduleAmount;
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}
					if(maxDate == null || maxDate.before(deliveryDate)) {
						maxDate = deliveryDate;
					}
				}
				if(balanceToAdd > 0) {
					
					customer.setLastPendingDues(customer.getLastPendingDues() + balanceToAdd);
					customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balanceToAddToRemainingPrepay);
					
					customer = userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
					asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, maxDate == null ? maxDate : DateHelper.addDays(maxDate, 1));
				}
			} else {
				asyncJobs.deleteDeliverySchedulesInRange(customer.getCode(), dates);
			}
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * USED_FROM_WEB
	 */
	@RequestMapping("/restapi/prepay/markasactive")
	@ResponseBody
	public ApiResponse markAsActive(@RequestParam(required = true)Long customerId) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		User loggedInUser = userService.getLoggedInUser();
		User customer = userService.getUser(customerId, true, loggedInUser, false, false);
		customer.setStatus(Status.ACTIVE);
		customer = userService.updateUser(customerId, customer, loggedInUser, false, false);
		asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, DateHelper.addDays(new Date(), 1));
		return apiResponse;
	}
	
	/**
	 * USED_FROM_WEB
	 * USED_FROM_DEVICE
	 */
	@RequestMapping(value = "/restapi/deliverySchedule/putonhold")
	@ResponseBody
	public ApiResponse putOnHoldDeliverySchedule(@RequestParam(required = true)Long customerId,
											  @RequestParam(required = true)String startDate,
											  @RequestParam(required = true)String endDate,
											  @RequestParam(required = false)boolean isIndefiniteTime){

		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			User customer = userService.getUser(customerId, true, loggedInUser, false, false);
			
			Date fromDate = DateHelper.parseDate(startDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fromDate);
			calendar.add(Calendar.MONTH, 2);
			Date toDate = new Date();
					
			if(!endDate.isEmpty()){
				toDate = DateHelper.parseDate(endDate);
			}else{
				toDate = calendar.getTime();
			}
			
			UserSubscription subscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, false);
			if(null != subscription && subscription.getSubscriptionType() == UserSubscription.SubscriptionType.Prepaid) {
				
				Date today = new Date();
				DateHelper.setToStartOfDay(today);
				double balanceToAdd = 0, balaneToAddToRemainingPrepay =0;
				if(isIndefiniteTime){
					
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(customer.getId(), fromDate, null, false, true);
					for(DeliverySchedule deliverySchedule:deliverySchedules) {
						double deliveryScheduleAmount = 0;
						for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
							balanceToAdd = balanceToAdd + lineItem.getTotalPrice();
							deliveryScheduleAmount = deliveryScheduleAmount + lineItem.getTotalPrice();
						}
						if(deliverySchedule.getDate().getTime() <= today.getTime()) {
							balaneToAddToRemainingPrepay = balaneToAddToRemainingPrepay + deliveryScheduleAmount;
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}
					if(!customer.getStatus().name().equalsIgnoreCase("HOLD")) {
						activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"HOLD", "deliverySchedule", customer.getId());
						customer.setStatus(Status.HOLD);
						customer = userService.updateUser(customerId, customer, loggedInUser, true, true);
					}

					if(balanceToAdd > 0) {
						
						customer.setLastPendingDues(customer.getLastPendingDues() + balanceToAdd);
						customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balaneToAddToRemainingPrepay);
						
						customer = userService.updateUser(customerId, customer, loggedInUser, true, true);
					}
				} else {
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(customer.getId(), fromDate, toDate, null, false, true, UserSubscription.SubscriptionType.Prepaid);
					for(DeliverySchedule deliverySchedule:deliverySchedules) {
						double deliveryScheduleAmount = 0;
						for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
							balanceToAdd = balanceToAdd + lineItem.getTotalPrice();
							deliveryScheduleAmount = deliveryScheduleAmount + lineItem.getTotalPrice();
						}
						if(deliverySchedule.getDate().getTime() <= today.getTime()) {
							balaneToAddToRemainingPrepay = balaneToAddToRemainingPrepay + deliveryScheduleAmount;
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}
					if(balanceToAdd > 0) {
						
						customer.setLastPendingDues(customer.getLastPendingDues() + balanceToAdd);
						customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() + balaneToAddToRemainingPrepay);
						
						customer = userService.updateUser(customerId, customer, loggedInUser, true, true);
						asyncJobs.createExcessivePrepayDeliveries(customer, loggedInUser, DateHelper.addDays(toDate, 1));
					}
				}
			} else {
				if(isIndefiniteTime){
					asyncJobs.deleteAllDeliverySchedulesFromDate(customer, fromDate);
					if(!customer.getStatus().name().equalsIgnoreCase("HOLD")) {
						activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"HOLD", "deliverySchedule", customer.getId());
						customer.setStatus(Status.HOLD);
						userService.updateUser(customerId, customer, loggedInUser, true, true);
					}
				} else {
					asyncJobs.deletePostpaidDeliverySchedules(customer, fromDate, toDate);
				}
			}
			
			//SMS Notification 
			if(isIndefiniteTime) {
				plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear " + customer.getFullName() + ", your milk subscription has been put on hold starting "+ startDate +".");
			} else {
				plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear " + customer.getFullName() + ", your milk subscription has been put on hold starting "+startDate+" to "+ endDate);
			}
			
			//PUSH Notification
			firebaseWrapper.sendSubscriptionPutOnHoldNotification(subscription, startDate, endDate);
			
			//Email Notification
			emailHelper.sendMailForSubscriptionOnHold(subscription);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * USED_FROM_DEVICE. 
	 */
	@RequestMapping(value = "/restapi/deletedDeliverySchedule/v2" ,method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse deleteDeliverySchedule1(@RequestPart(required = true)String customerCode,
												@RequestPart(required = true)String[] dates){

		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getSuperUser();
			User customer = userService.getUserByCode(customerCode, true, loggedInUser, false, false);
			double balanceToAdd = 0;
			for (String date : dates) {
				DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), DateHelper.parseDate(date), false, null, false, true);
				if (deliverySchedule != null) {
					if(deliverySchedule.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
						for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
							balanceToAdd = balanceToAdd + deliveryLineItem.getTotalPrice();
						}
					} else {
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}
				}
			}
			if(balanceToAdd > 0) {
				customer.setLastPendingDues(customer.getLastPendingDues() + balanceToAdd);
				userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
			}
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * UNUSED
	 */
	@Deprecated
	@RequestMapping(value = "/restapi/deletedDeliverySchedule" ,method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse deleteDeliverySchedule1(@RequestPart(required = true)Long custId,
												@RequestPart(required = true)String[] dates){

		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getSuperUser();
			User customer = userService.getUser(custId, true, null, false, false);
			for (String date : dates) {
				DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), DateHelper.parseDate(date), false, null, false, true);
				if (deliverySchedule != null) {
					deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
				}
			}
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * 
	 * Used from device side
	 * USED_FROM_DEVICE_SIDE
	 * 
	 */
	@RequestMapping(value = "/openapi/addDeliveryScheduleBydate/v2" ,method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse addDeliveryScheduleByDate(@RequestPart(required = true)String customerId,
												 @RequestPart(required = true)String[] productCodes,
												 @RequestPart(required = true)int[] quantity,
												 @RequestPart(required = true)String[] dates){

		ApiResponse apiResponse = new ApiResponse(true);

		try {
			User loggedInUser = userService.getSuperUser();
			User customer = userService.getUser(Long.parseLong(customerId), false, loggedInUser, false, false);
			
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, null, false, true);
			if(null != userSubscription && userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				throw new ProvilacException("Prepaid customers can not change delivery schedule from here. Please call our customer care.");
			}
			
			DeliverySchedule deliverySchedule = null;
			DeliveryLineItem deliveryLineItem = null;
			Set<DeliveryLineItem> setOfdeliveryLineItems = new HashSet<DeliveryLineItem>();
			List<DeliveryScheduleDTO> listOfDeliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();

			Map<String, Integer> productQuantMap = new HashMap<String, Integer>();
			Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>();
			
			for (int i = 0; i < dates.length; i++) {
				String date = dates[i];
				if (!map.containsKey(date)) {
					map.put(date, new HashMap<String, Integer>());
				}
				productQuantMap = map.get(date);
				productQuantMap.put(productCodes[i], quantity[i]);

			}
			Set<String> mapOfset = map.keySet();
			for (String date : mapOfset) {
				RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customer.getCode(), false, null, false, true);
				deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), DateHelper.parseDate(date), false, loggedInUser, true, true);

				if (deliverySchedule == null) {
					deliverySchedule = new DeliverySchedule();
					deliverySchedule.setCustomer(customer);
					deliverySchedule.setDate(DateHelper.parseDate(date));
					deliverySchedule.setDeliveryTime(DateHelper.parseDate(date));
					deliverySchedule.setType(Type.Normal);
					deliverySchedule.setRoute(routeRecord.getRoute());
					
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), routeRecord.getPriority(), null, false, false);
					if(!deliverySchedules.isEmpty()) {
						List<DeliverySchedule> schedules =  new ArrayList<DeliverySchedule>();
						schedules = deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), routeRecord.getPriority(), null, false, false);
						if(schedules.size()!= 0) {
							for (DeliverySchedule deliverySchedule2 : schedules) {
								deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
								deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
							}
						}
					}
					deliverySchedule.setPriority(routeRecord.getPriority());

					productQuantMap = map.get(date);
					Set<String> set = productQuantMap.keySet();
					for (String string2 : set) {
						Product product = productService.getProductByCode(string2, false, null, false, false);
						deliveryLineItem = new DeliveryLineItem();
						deliveryLineItem.setProduct(product);

						if (productQuantMap.get(string2) == 0) {
							continue;
						}
						
						int lineItemQuantity = productQuantMap.get(string2);
						deliveryLineItem.setPricePerUnit(product.getPrice());
						deliveryLineItem.setTotalPrice(lineItemQuantity * product.getPrice());
						deliveryLineItem.setQuantity(lineItemQuantity);
						
						deliveryLineItem = deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, null, false, false);
						setOfdeliveryLineItems.add(deliveryLineItem);
					}
					deliverySchedule.setDeliveryLineItems(setOfdeliveryLineItems);	
					deliverySchedule.setUpdatedFromMobile(true);
					deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
					deliverySchedule = deliveryScheduleService.createDeliverySchedule(deliverySchedule, null, false, true);
					deliveryLineItem.setDeliverySchedule(deliverySchedule);
					listOfDeliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, setOfdeliveryLineItems));
					setOfdeliveryLineItems.clear();
				} else {
					List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>(deliverySchedule.getDeliveryLineItems());
					for (DeliveryLineItem existingDeliveryLineItem : deliveryLineItems) {
						deliveryLineItemService.deleteDeliveryLineItem(existingDeliveryLineItem.getId());
					}
					productQuantMap = map.get(date);
					Set<String> set = productQuantMap.keySet();
					for (String string2 : set) {
						Product product = productService.getProductByCode(string2, false, null, false, false);
						deliveryLineItem = new DeliveryLineItem();
						deliveryLineItem.setProduct(product);

						if (productQuantMap.get(string2) == 0) {
							continue;
						}
						
						int lineItemQuantity = productQuantMap.get(string2);
						deliveryLineItem.setPricePerUnit(product.getPrice());
						deliveryLineItem.setTotalPrice(lineItemQuantity * product.getPrice());
						deliveryLineItem.setQuantity(lineItemQuantity);
						
						deliveryLineItem.setDeliverySchedule(deliverySchedule);
						deliveryLineItem = deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, null, false, false);
						setOfdeliveryLineItems.add(deliveryLineItem);
					}
					if (setOfdeliveryLineItems.isEmpty()) {
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					} else {
						deliverySchedule.setDeliveryLineItems(setOfdeliveryLineItems);
						deliverySchedule.setUpdatedFromMobile(true);
						//TODO: Call update method
						deliverySchedule = deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, null, false, true);
						//plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear customer,you have Change Your Delivery Schedule : ");
						firebaseWrapper.sendDeliveryScheduleChangeNotification(deliverySchedule);
						//emailHelper.sendMailForDeliveryScheduleChange(deliverySchedule);
						listOfDeliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, setOfdeliveryLineItems));
						setOfdeliveryLineItems.clear();
					}
				}
			}
			
			apiResponse.addData("deliveryscheduleDTO", listOfDeliveryScheduleDTOs);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/restapi/deliverySchedule/getCurrentSchedulesByRoute", method= RequestMethod.GET)
	@ResponseBody
	public ApiResponse getDeliverySchedulesByRoute(@RequestParam(required = true) String routeCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date date = calendar.getTime();
			Route route = routeService.getRouteByCode(routeCode, false, null, false, false);
			List<DeliverySchedule>deliverySchedules1 = deliveryScheduleService.getScheduleByRouteAndDate(route.getId(),date, 0, null, false, true);
			List<DeliveryScheduleDTO>deliverySchedules = new ArrayList<DeliveryScheduleDTO>();
			for (DeliverySchedule deliverySchedule : deliverySchedules1) {
				deliverySchedules.add(new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
			}
			apiResponse.addData("deliverySchedules", deliverySchedules);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE_SIDE/MAYBE_UNUSED
	@RequestMapping(value = "/restapi/deliverySchedule/changeStatus" ,method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse changeDeliveryScheduleStatus(@RequestPart(required = true)String[] codes,
												@RequestPart(required = true)String[] reasons,
												@RequestPart(required = true)String[] status){

		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			int count = 0;
			List<DeliverySchedule> schedules = new ArrayList<DeliverySchedule>();
			for (String deliveryScheduleCode : codes) {
				DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleByCode(deliveryScheduleCode, true, loggedInUser, false, false);
				if(status[count].equalsIgnoreCase(DeliveryStatus.NotDelivered.name())){
					deliverySchedule.setStatus(DeliveryStatus.NotDelivered);
					deliverySchedule.setReason(reasons[count]);
				}else{
					deliverySchedule.setStatus(DeliveryStatus.Delivered);
				}
				schedules.add(deliverySchedule);
				count++;
			}
			//TODO: Call update method
			deliveryScheduleService.createDeliverySchedules(schedules, false, loggedInUser, false, false);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/deliverySchedule/populateSubscriptionType")
	@ResponseBody
	public ApiResponse populateSubscriptionType() {
		asyncJobs.populateDeliveryScheduleSubscriptionType();
		return new ApiResponse(true);
	}
	
	@RequestMapping(value = "/end-user/change-schedule")
	public String changeSchedule(Model model, @RequestParam(required = true) String deliveryScheduleCipher) {
		
		if(deliveryScheduleCipher == null || deliveryScheduleCipher.isEmpty())
			return "redirect:/end-user/welcome";
		Long id = Long.valueOf(EncryptionUtil.decode(deliveryScheduleCipher));
		User loggedInUser = userService.getLoggedInUser();
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedInUser,true,true);
		Set<DeliveryLineItem> setOfdeliveryLineItems = new HashSet<DeliveryLineItem>();
		setOfdeliveryLineItems = deliverySchedule.getDeliveryLineItems();
		requestInterceptor.addDataToSession("setOfdeliveryLineItems", setOfdeliveryLineItems);
		
		model.addAttribute("deliverySchedule", deliverySchedule);
		model.addAttribute("setOfdeliveryLineItems", setOfdeliveryLineItems);
		model.addAttribute("deliveryScheduleCipher", deliveryScheduleCipher);
		
		return "/enduser-pages/change-schedule";
	}
	
	@RequestMapping(value = "/end-user/deliverySchedule/update")
	public String deliveryScheduleUpdateByCustomer(Model model, @RequestParam(required = true) String deliveryScheduleCipher) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(deliveryScheduleCipher));
		User loggedInUser = userService.getLoggedInUser();
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedInUser,true,true);
		List<DeliveryLineItem> deliveryLineItems = deliveryLineItemService.getAllDeliveryLineItemsByDeliverySchedule(deliverySchedule.getId(), null, false, false);
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(deliverySchedule.getCustomer().getCode(), true, null, false, false);
		Double totalAmountForExistingDeliveryLineItems = 0.0;
		if(null != deliverySchedule && userSubscription.getSubscriptionType() == UserSubscription.SubscriptionType.Prepaid) {
			for(DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				totalAmountForExistingDeliveryLineItems = totalAmountForExistingDeliveryLineItems + (deliveryLineItem.getTotalPrice());
			}
		}
		
		if(deliveryLineItems.size()!=0){
			deliveryLineItemService.deleteAllDeliveryLineItem(deliveryLineItems);
		}
		Set<DeliveryLineItem> deliveryLineItems1 = new HashSet<DeliveryLineItem>();
		DeliveryLineItem lineItem = null;
		Set<DeliveryLineItem> existingItems = (Set<DeliveryLineItem>) requestInterceptor.getDataFromSession("setOfdeliveryLineItems");
		Double totalAmount = 0.0;
		for(DeliveryLineItem deliveryLineItem : existingItems) {
			lineItem = new DeliveryLineItem();
			lineItem.setQuantity(deliveryLineItem.getQuantity());
			lineItem.setProduct(deliveryLineItem.getProduct());
			
			lineItem.setPricePerUnit(deliveryLineItem.getProduct().getPrice());
			lineItem.setTotalPrice(deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
			lineItem.setDay(deliveryLineItem.getDay());
			lineItem.setDeliverySchedule(deliverySchedule);
			totalAmount = totalAmount + (deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
			deliveryLineItemService.createDeliveryLineItem(lineItem, null, false, false);
			deliveryLineItems1.add(lineItem);
		}
		
		deliverySchedule.getDeliveryLineItems().clear();
		deliverySchedule.setDeliveryLineItems(deliveryLineItems1);
		
		DeliverySchedule updatedDeliverySchedule = deliveryScheduleService.updateDeliveryScheduleByCustomer(deliverySchedule.getId(), deliverySchedule, loggedInUser,true,false);
		
		if(null != deliverySchedule && userSubscription.getSubscriptionType() == UserSubscription.SubscriptionType.Prepaid) {
			
			Double finalAmount = loggedInUser.getRemainingPrepayBalance() + (totalAmountForExistingDeliveryLineItems - totalAmount);
			loggedInUser.setRemainingPrepayBalance(finalAmount);
			userService.updateUser(loggedInUser.getId(), loggedInUser, loggedInUser, false, false);
		}
		
		String message = "DeliverySchedule updated successfully";
		return "redirect:/end-user/welcome?message=" + message;
	}
	
	@RequestMapping(value = "/end-user/cancel-schedule")
	public String cancelSchedule(Model model, @RequestParam(required = true) String deliveryScheduleCipher) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(deliveryScheduleCipher == null || deliveryScheduleCipher.isEmpty())
			return "redirect:/end-user/welcome";
		Long id = Long.valueOf(EncryptionUtil.decode(deliveryScheduleCipher));
		DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, true, loggedInUser,true,true);
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, false, false);
		double balanceToAdd = 0;
		if (deliverySchedule != null) {
			if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
					balanceToAdd = balanceToAdd + deliveryLineItem.getTotalPrice();
				}
			}
			deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
		}
		
		if(balanceToAdd > 0) {
			loggedInUser.setRemainingPrepayBalance(loggedInUser.getRemainingPrepayBalance() + balanceToAdd);
			userService.updateUser(loggedInUser.getId(), loggedInUser, loggedInUser, false, false);
		}
		
		String message = "DeliverySchedule canelled successfully";
		return "redirect:/end-user/welcome?message=" + message;
	}
	
	@RequestMapping(value = "/end-user/hold-delivery-schedule")
	public String holdDeliverySchedule(Model model, @RequestParam(required = false) String message) {
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, false, false);
		if(userSubscription == null) {
			message = "userSubscription is not available";
			return "redirect:/index?message=" + message;
		}
		model.addAttribute("today", DateHelper.getFormattedDateForDevice(new Date()));
		model.addAttribute("message", message);
		return "/enduser-pages/hold-delivery";
	}
	
	@RequestMapping(value = "/end-user/hold-delivery")
	public String holdDelivery(Model model, @RequestParam String fromDate,	@RequestParam String toDate) {
		
		Date startDate = DateHelper.parseDateFormatForDevice(fromDate);
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.parseDateFormatForDevice(toDate);
		DateHelper.setToStartOfDay(endDate);
		
		Date afterFifteenDaysFromStartDate = DateHelper.addDays(startDate, 14);
		if(endDate.after(afterFifteenDaysFromStartDate)) {
			String message = " You are duration of HOLD delivery is more than 15 days, Please contact us 1800-002-564";
			return "redirect:/end-user/hold-delivery-schedule?message=" + message;
		}
		
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, false, false);
		double balanceToAddToRemainingPrepay = 0;
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRange(loggedInUser.getCode(), startDate, endDate, loggedInUser, false, true);
		if(deliverySchedules.isEmpty()) {
			String message = fromDate + " to " + toDate + " deliverySchedules not present";
			return "redirect:/end-user/hold-delivery-schedule?message=" + message;
		}
		for(DeliverySchedule deliverySchedule : deliverySchedules) {
			for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
				balanceToAddToRemainingPrepay = balanceToAddToRemainingPrepay + deliveryLineItem.getTotalPrice();
			}
			deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
		}
		
		if(null != userSubscription && userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
			if(balanceToAddToRemainingPrepay > 0) {
				loggedInUser.setRemainingPrepayBalance(loggedInUser.getRemainingPrepayBalance() + balanceToAddToRemainingPrepay);
				userService.updateRemmaingPrePaid(loggedInUser.getId(), loggedInUser, false, false);
			}
		}
		
		String message = "DeliverySchedule holded successfully";
		return "redirect:/end-user/welcome?message=" + message;
	} 
	
	@RequestMapping(value = "/restapi/changeQtyOfRelatedProduct")
	@ResponseBody
	public ApiResponse changeQtyOfRelatedProduct(@RequestParam(required=true) String productCode, @RequestParam(required=true) int qty) {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		Set<DeliveryLineItem> existingItems = (Set<DeliveryLineItem>) requestInterceptor.getDataFromSession("setOfdeliveryLineItems");
		if(existingItems != null) {
			Set<DeliveryLineItem> newDeliveryLineItem = new HashSet<DeliveryLineItem>();
			for(DeliveryLineItem existingItem : existingItems) {
				if(existingItem.getProduct().getCode().equalsIgnoreCase(productCode)) {
					existingItem.setQuantity(qty);
				}
				newDeliveryLineItem.add(existingItem);
			}
			requestInterceptor.addDataToSession("setOfdeliveryLineItems", newDeliveryLineItem);
		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/restapi/end-user/getDeliveryScheduleByCustomerAndDate")
	@ResponseBody
	public ApiResponse getDeliveryScheduleByCustomerCodeAndDate(@RequestParam(required = true) String customerId,
															@RequestParam(required = true) String date) {

		Date selectedDate = DateHelper.parseDateFormatForDevice(date);
		DateHelper.setToStartOfDay(selectedDate);
		Date today = new Date();
		DateHelper.setToStartOfDay(today);
		
		ApiResponse apiResponse = new ApiResponse(true);
		if(today.after(selectedDate) || today.equals(selectedDate)) {
			apiResponse.addData("selectedButtonflag", false);
		} else {
			apiResponse.addData("selectedButtonflag", true);
		}
		DeliveryScheduleDTO deliveryScheduleDTO = new DeliveryScheduleDTO();
		User loggedInUser = userService.getLoggedInUser();
		try {
			User customer = userService.getUser(Long.parseLong(customerId), false, loggedInUser, false, false);
			DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customer.getCode(), selectedDate, false, loggedInUser, true,true);
			if (deliverySchedule == null ) {
				apiResponse.setSuccess(false);
			}else{
				Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
				deliveryLineItems = deliverySchedule.getDeliveryLineItems();
				deliveryScheduleDTO = new DeliveryScheduleDTO(deliverySchedule, deliveryLineItems);
				apiResponse.addData("deliverySchedule", deliveryScheduleDTO);	
				apiResponse.addData("deliveryScheduleCipher", deliverySchedule.getCipher());	
			}

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}
	
}
