package com.vishwakarma.provilac.controller;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.LocalityDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.model.Locality;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.LocalityValidator;
import com.vishwakarma.provilac.property.editor.CityPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.CityRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.LocalityService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.EncryptionUtil;


@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class LocalityController {

	@Resource
	private LocalityValidator localityValidator;

	@Resource
	private LocalityService localityService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private CityRepository cityRepository;
	
	@Resource
	private CityService cityService;
	
	
	@InitBinder(value = "locality")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(City.class,new  CityPropertyEditor(cityRepository));
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.setValidator(localityValidator);
	}

	private Model populateModelForAdd(Model model, Locality locality) {
		model.addAttribute("locality", locality);
		model.addAttribute("cities", cityService.getAllCities(null, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/locality/add")
	public String addLocality(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		model = populateModelForAdd(model, new Locality());
		return "admin-pages/locality/add";
	}

	@RequestMapping(value = "/admin/locality/add", method = RequestMethod.POST)
	public String localityAdd(Locality locality, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		localityValidator.validate(locality, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, locality);
			return "admin-pages/locality/add";
		}
		localityService.createLocality(locality, loggedInUser, true,false);
		String message = "Locality added successfully";
		return "redirect:/admin/locality/list?message=" + message;
	}

	@RequestMapping(value = "/admin/locality/list")
	public String getLocalityList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("message", message);
		Page<Locality> page = null;
		if (StringUtils.isNotBlank(searchTerm)) {
			page = localityService.searchLocality(pageNumber, searchTerm,loggedInUser,true,false);
		} else {
			page = localityService.getLocalities(pageNumber, loggedInUser,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("localities", page.getContent());

		return "/admin-pages/locality/list";
	}

	@RequestMapping(value = "/admin/locality/show/{cipher}")
	public String showLocality(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		Locality locality = localityService.getLocality(id, true, loggedInUser,true,true);
		model.addAttribute("locality", locality);
		return "/admin-pages/locality/show";
	}

	@RequestMapping(value = "/admin/locality/update/{cipher}")
	public String updateLocality(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Locality locality = localityService.getLocality(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, locality);
		model.addAttribute("id", locality.getId());
		model.addAttribute("version", locality.getVersion());
		return "/admin-pages/locality/update";
	}

	@RequestMapping(value = "/admin/locality/update", method = RequestMethod.POST)
	public String localityUpdate(Locality locality, 
										BindingResult formBinding, 
										Model model) {

		locality.setUpdateOperation(true);
		localityValidator.validate(locality, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, locality);
			return "admin-pages/locality/update";
		}
		locality.setId(Long.parseLong(model.asMap().get("id") + ""));
		locality.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		localityService.updateLocality(locality.getId(), locality, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Locality updated successfully";
		return "redirect:/admin/locality/list?message=" + message;
	}

	@RequestMapping(value = "/admin/locality/delete/{cipher}")
	public String deleteLocality(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			localityService.deleteLocality(id);
			message = "Locality deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Locality";
		}
		return "redirect:/admin/locality/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
	
	/**
	 * REST API Methods
	 */

	@RequestMapping(value="/restapi/locality/list")
	@ResponseBody
	public ApiResponse getAllLocalities(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Locality> localities = localityService.getAllLocalities(null, false, true);
			List<LocalityDTO> localityDTOs = new ArrayList<LocalityDTO>();
			for (Locality locality : localities) {
				localityDTOs.add(new LocalityDTO(locality));
			}
			apiResponse.addData("localities", localityDTOs);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/locality/byCity")
	@ResponseBody
	public ApiResponse getAllLocalitiesByCity(@RequestParam(required= true)Long cityId){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Locality> localities = localityService.getLocalitiesByCity(cityId, null, false, false);
			List<LocalityDTO> localityDTOs = new ArrayList<LocalityDTO>();
			for (Locality locality : localities) {
				localityDTOs.add(new LocalityDTO(locality));
			}
			apiResponse.addData("localities", localityDTOs);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	
	/**
	 * End REST API Methods
	 */
}
