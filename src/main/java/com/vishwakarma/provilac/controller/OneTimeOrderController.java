package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.CartItem;
import com.vishwakarma.provilac.dto.OneTimeOrderDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OneTimeOrder.OrderStatus;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.mvc.validator.OneTimeOrderValidator;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.LocalityRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.LocalityService;
import com.vishwakarma.provilac.service.OneTimeOrderService;
import com.vishwakarma.provilac.service.OrderLineItemService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class OneTimeOrderController {

	@Resource
	private UserService userService;

	@Resource
	private OneTimeOrderService oneTimeOrderService;

	@Resource
	private OrderLineItemService orderLineItemService;

	@Resource
	private AddressService addressService;

	@Resource
	private ProductService productService;

	@Resource
	private LocalityService localityService;

	@Resource
	private LocalityRepository localityRepository;

	@Resource
	private UserRepository userRepository;

	@Resource
	private SystemPropertyHelper systemPropertyHelper;

	@Resource
	private OneTimeOrderValidator oneTimeOrderValidator;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private RequestInterceptor requestInterceptor;
	
	@Resource
	private Environment environment;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource AsyncJobs asyncJobs;

	@InitBinder(value = "onetimeorder")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(User.class, new UserPropertyEditor(
				userRepository));

	}

	private Model populateModelForAdd(Model model, OneTimeOrder oneTimeOrder) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("onetimeorder", oneTimeOrder);
		model.addAttribute("deliveryboys", userService.getUsersForRole(Role.ROLE_DELIVERY_BOY, loggedInUser, false, false));
		model.addAttribute("oneTimeOrders", oneTimeOrderService.getAllOneTimeOrders(loggedInUser, true, true));
		model.addAttribute("todayOneTimeOrders", oneTimeOrderService.getTodayOneTimeOrders(null, null, loggedInUser, true, true));
		return model;
	}

	@RequestMapping(value = "/admin/onetimeorder/exportorder")
	public ResponseEntity<byte[]> deliveryBoyTodayOrders(
			OneTimeOrder onetimeorder,
			BindingResult formBinding,
			Model model,
			@RequestParam(required = false, value = "orderCode[]") String[] orderCodes,
			@RequestParam(required = false, value = "prioritySet[]") Integer[] priorities,
			@RequestParam(required = false, value = "deliveryBoy") Long deliveryBoyId,
			HttpServletResponse httpServletResponse) throws IOException {

		User loggedInUser = userService.getLoggedInUser();
		for (int i = 0; i < priorities.length; i++) {
			Integer priority = priorities[i];
			if (priority != 0) {

				String code = orderCodes[i];
				OneTimeOrder oneTimeOrder2 = oneTimeOrderService.getOneTimeOrderByCode(code, loggedInUser, false, true);
				oneTimeOrder2.setPriority(priority);
				User deliveryBoy = userService.getUser(deliveryBoyId, false, null, false, false);
				oneTimeOrder2.setDeliveryBoy(deliveryBoy);

				oneTimeOrderService.updateOneTimeOrder(oneTimeOrder2.getId(),oneTimeOrder2, loggedInUser, false, false);

			} else {
				String code = orderCodes[i];
				OneTimeOrder oneTimeOrder2 = oneTimeOrderService.getOneTimeOrderByCode(code, loggedInUser, false, true);
				oneTimeOrder2.setPriority(priority);
				oneTimeOrder2.setDeliveryBoy(null);
				oneTimeOrderService.updateOneTimeOrder(oneTimeOrder2.getId(),oneTimeOrder2, loggedInUser, false, false);

			}
		}

		// FOr Generate Delivery Sheet

		org.springframework.core.io.Resource tp = new ClassPathResource("/excelSheet/OneTimeOrder.xls");
		File file = tp.getFile();

		List<OneTimeOrder> oneTimeOrderSheets = oneTimeOrderService.getTodayOneTimeOrdersByDeliveryBoy(deliveryBoyId,loggedInUser, false, false);
		final Comparator<OneTimeOrder> priority = new Comparator<OneTimeOrder>() {

				public int compare(OneTimeOrder o1, OneTimeOrder o2) {

				   int p1 = o1.getPriority();
				   int p2 = o2.getPriority();
				   return p1-p2;
			   }};
		Collections.sort(oneTimeOrderSheets, priority);
	
		ByteArrayOutputStream byteArrayOutputStream = generateDeliverySheetsForAllRoutes(file, oneTimeOrderSheets);
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/xls");
		httpServletResponse.setHeader("Content-Disposition","inline;filename=Delivery_Sheet.xls");
		httpServletResponse.setHeader("Content-Length",
				String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(),httpServletResponse.getOutputStream());
			for(OneTimeOrder order:oneTimeOrderSheets){
			activityLogService.createActivityLog(null, "export", "OneTimeOrder",order.getId());
		}
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException(
					"Unable to write output to excel sheet, please try again");
		}

		String message = "Delivery Sheet Create successfully";
		return null;
	}

	private ByteArrayOutputStream generateDeliverySheetsForAllRoutes(File file,
			List<OneTimeOrder> oneTimeOrderSheets) throws IOException {

		int rowIndex = 1, count = 0;
		Set<OrderLineItem> orderLineItems = new HashSet<OrderLineItem>();
		// Map<String, Integer> productQuantity = new HashMap<String,
		// Integer>();

		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet hssfSheet = hssfWorkbook.createSheet();
		for (OneTimeOrder oneTimeOrderSheet : oneTimeOrderSheets) {
			// productQuantity = new HashMap<String, Integer>();
			
				hssfSheet = hssfWorkbook.getSheetAt(0);
				hssfWorkbook.setSheetName(0, oneTimeOrderSheet.getDeliveryBoy() + ",Date" + DateHelper.getFormattedDate(new Date()));
				
			HSSFRow hssfRow = null;
			HSSFCell hssfCell = null;

			List<String> headers = new ArrayList<String>();
			List<String> data = null;
			List<List> records = new ArrayList<List>();
			headers.add("No.");
			headers.add("Customer Name");
			headers.add("Address");
			headers.add("Phone Number");
			headers.add("Priority");
			headers.add("instructions");
			headers.add("Payment");
			headers.add("Product Name");
			headers.add("Qty");

			// Headers added
			records.add(headers);
			for (OneTimeOrder oneTimeOrder : oneTimeOrderSheets) {

				orderLineItems = oneTimeOrder.getOrderLineItems();
				for (OrderLineItem orderLineItem : orderLineItems) {
					data = new ArrayList<String>();
					data.add(rowIndex + "");
					data.add(oneTimeOrder.getCustomer().getFirstName());
					// Address Add
					String address = "";

					if (oneTimeOrder.getAddress().getName() != null) {
						address = address + " "
								+ oneTimeOrder.getAddress().getName();
					}
					if (oneTimeOrder.getAddress().getAddressLine1() != null) {
						address = address + ":- "
								+ oneTimeOrder.getAddress().getAddressLine1();
					}

					if (oneTimeOrder.getAddress().getAddressLine2() != null) {
						address = address + " "
								+ oneTimeOrder.getAddress().getAddressLine2();
					}

					if (oneTimeOrder.getAddress().getLandmark() != null) {
						address = address + " "
								+ oneTimeOrder.getAddress().getLandmark();
					}

					if (oneTimeOrder.getAddress().getCity().getName() != null) {
						address = address + " "
								+ oneTimeOrder.getAddress().getCity().getName();
					}

					if (oneTimeOrder.getAddress().getPincode() != null) {
						address = address + " "
								+ oneTimeOrder.getAddress().getPincode();
					}

					data.add(address);
					data.add(oneTimeOrder.getCustomer().getMobileNumber());
					data.add(String.valueOf(oneTimeOrder.getPriority()));
					data.add(oneTimeOrder.getInstructions());
					data.add(oneTimeOrder.getPaymentGateway());
					data.add(orderLineItem.getProduct().getName());
					data.add(orderLineItem.getQuantity() + "");
					records.add(data);
					rowIndex++;

				}
			}
			rowIndex = 0;
			HSSFCellStyle hssfCellStyleForContent = hssfWorkbook
					.createCellStyle();

			hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);

			for (int j = 0; j < records.size(); j++) {
				hssfRow = hssfSheet.createRow(rowIndex);
				List<String> l2 = records.get(j);
				for (int k = 0; k < l2.size(); k++) {
					hssfCell = hssfRow.createCell(k);
					hssfCell.setCellStyle(hssfCellStyleForContent);
					hssfCell.setCellValue(l2.get(k));
				}
				rowIndex++;
			}

			for (int colNum = 0; colNum < hssfSheet.getRow(0).getLastCellNum(); colNum++) {
				hssfSheet.autoSizeColumn(colNum);
			}

			hssfSheet.setFitToPage(true);

			rowIndex = 1;
			count++;
		}
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException(
					"Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}

	@RequestMapping(value = "/admin/onetimeorder/list")
	public String getAllOneTimeOrders(Model model,
			@RequestParam(defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false) String message,
			@RequestParam(required = false) String searchTerm) {

		model.addAttribute("message", message);

		Page<OneTimeOrder> page = null;
		User loggedInUser = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = oneTimeOrderService.searchOneTimeOrder(pageNumber,
					searchTerm, loggedInUser, true, false);
		} else {
			page = oneTimeOrderService.getOneTimeOrders(pageNumber,
					loggedInUser, true, false);
		}

		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("onetimeorders", page.getContent());

		return "/admin-pages/onetimeorder/list";
	}

	@RequestMapping(value = "/admin/onetimeorder/todaylist")
	public String getTodaysOneTimeOrders(Model model,
			@RequestParam(defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false) String message,
			@RequestParam(required = false) String searchTerm) {

		model.addAttribute("message", message);

		Page<OneTimeOrder> page = null;
		User loggedInUser = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = oneTimeOrderService.searchOneTimeOrder(pageNumber,
					searchTerm, loggedInUser, true, false);
		} else {
			page = oneTimeOrderService.getTodayOneTimeOrders(pageNumber, null,
					null, loggedInUser, true, false);
		}

		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("todayonetimeorders", page.getContent());

		return "/admin-pages/onetimeorder/todaylist";
	}

	@RequestMapping(value = "/admin/onetimeorder/deliverysheet")
	public String getOneTimeOrderDeliverySheet(Model model) {
		model = populateModelForAdd(model, new OneTimeOrder());

		return "/admin-pages/onetimeorder/deliverysheet";
	}

	@RequestMapping(value = "/admin/onetimeorder/show/{cipher}")
	public String showPricing(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		OneTimeOrder oneTimeOrder = oneTimeOrderService.getOneTimeOrder(id,
				loggedInUser, true, false);
		model.addAttribute("oneTimeOrder", oneTimeOrder);

		return "/admin-pages/onetimeorder/show";
	}

	/**
	 * USED_FROM_DEVICE
	 * REST API START
	 */
	@RequestMapping(value = "/restapi/onetimeorder/add", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse addOneTimeOrder(
			@RequestPart(required = true) OneTimeOrderDTO oneTimeOrderDTO,
			@RequestPart(required = true) String[] productCodes,
			@RequestPart(required = true) Integer[] quantities) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {

			User loggedInUser = userService.getLoggedInUser();
			OneTimeOrder oneTimeOrder = new OneTimeOrder();

			User customer = userService.getUserByCode(loggedInUser.getCode(), true, loggedInUser, false, false);
			oneTimeOrder.setCustomer(customer);

			Address address = addressService.getAddressByCode(oneTimeOrderDTO.getAddressCode(), true, loggedInUser,false, false);
			oneTimeOrder.setAddress(address);
			if(StringUtils.isNotBlank(oneTimeOrderDTO.getInstructions())){
				oneTimeOrder.setInstructions(oneTimeOrderDTO.getInstructions());
			}
			oneTimeOrder.setDate(DateHelper.parseDate(oneTimeOrderDTO.getDate()));
			oneTimeOrder.setTxnId(oneTimeOrderDTO.getTxnId());
			oneTimeOrder.setPaymentGateway(oneTimeOrderDTO.getPaymentGateway());
			if(oneTimeOrderDTO.getPaymentGateway().equalsIgnoreCase("PAYTM")){
				asyncJobs.checkStatusWithPaytmForOneTimeOrder(oneTimeOrderDTO.getPaymentOrderId());
			}
			oneTimeOrder.setPaymentOrderId(oneTimeOrderDTO.getPaymentOrderId());
			// TODO: Calculate total amount and final amount by performing
			// calculations on selected
			// products + redeemed points
			// TODO: referal Balance minus and PointRedem
			// TODO: Take pointsRedeemed from client, and calculate
			// pointsDiscount and final amount using system properties

			double totalAmount = 0.0;
			for (int i = 0; i < productCodes.length; i++) {

				Product product = productService.getProductByCode(
						productCodes[i], true, loggedInUser, false, false);
				OrderLineItem orderLineItem = new OrderLineItem();

				double singleProductPrice = 0.0;
				if (product.getTax() > 0) {
					singleProductPrice = product.getPrice()
							+ ((product.getPrice() * product.getTax())/100);
				} else {
					singleProductPrice = product.getPrice();
				}    
				orderLineItem.setAmount(singleProductPrice * quantities[i]);
				orderLineItem.setQuantity(quantities[i]);
				orderLineItem.setProduct(product);

				totalAmount = totalAmount + orderLineItem.getAmount();
				oneTimeOrder.getOrderLineItems().add(orderLineItem);
			}
			if (customer.getAccumulatedPoints() < oneTimeOrderDTO.getPointsRedemed()) {
				throw new ProvilacException("Enter Valid Points Redemed");
			} else {
				customer.setAccumulatedPoints(customer.getAccumulatedPoints() - oneTimeOrderDTO.getPointsRedemed());
				oneTimeOrder.setPointsRedemed(oneTimeOrderDTO.getPointsRedemed());
			}

			float pointValue = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.POINT_VALUE, "0"));
			oneTimeOrder.setPointsDiscount(Math.round(oneTimeOrderDTO.getPointsRedemed() * pointValue));
			oneTimeOrder.setTotalAmount(totalAmount);
			oneTimeOrder.setFinalAmount(totalAmount - oneTimeOrder.getPointsDiscount());
			
			BindingResult formBinding = new DataBinder(oneTimeOrder).getBindingResult();

			oneTimeOrderValidator.validate(oneTimeOrder, formBinding);

			if (formBinding.hasErrors()) {
				String errorStr = null;
				for (ObjectError objectError : formBinding.getAllErrors()) {

					if (StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
						continue;
					}
					errorStr = errorStr + objectError.getDefaultMessage();
				}
				throw new ProvilacException(errorStr);
			}

			oneTimeOrder = oneTimeOrderService.createOneTimeOrder(oneTimeOrder,loggedInUser, false, false);
			userService.updateUser(customer.getId(), customer,loggedInUser, false, false);
			String message = "Your Order Has Been Created";
			apiResponse.addData("oneTimeOrder", new OneTimeOrderDTO(oneTimeOrder));

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	//USED_FROM_DEVICE
	@RequestMapping(value = "/restapi/onetimeorder/list")
	@ResponseBody
	public ApiResponse getOneTimeOrderHistory(@RequestParam(required = true) String userCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User loggedInUser = userService.getSuperUser();
			User user = userService.getUserByCode(userCode, true, loggedInUser, false, false);
			List<OneTimeOrder> oneTimeOrders = oneTimeOrderService.getOneTimeOrderByCustomer(user.getId(), null, false, false);
			List<OneTimeOrderDTO> oneTimeOrderDTOs = new ArrayList<OneTimeOrderDTO>();
			Collections.reverse(oneTimeOrders);
			for (OneTimeOrder oneTimeOrder : oneTimeOrders) {
				// oneTimeOrderDTOs.add(new OneTimeOrderDTO(oneTimeOrder));
				Set<OrderLineItem> orderLineItems = new HashSet<OrderLineItem>(orderLineItemService.getOrderLineItems(oneTimeOrder.getCode(), loggedInUser, false,false));
				oneTimeOrderDTOs.add(new OneTimeOrderDTO(oneTimeOrder,orderLineItems));
			}
			apiResponse.addData("oneTimeOrders", oneTimeOrderDTOs);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;

	}

	@RequestMapping(value = "/restapi/onetimeorder/details")
	@ResponseBody
	public ApiResponse getOneTimeOrderDetails(
			@RequestParam(required = true) Long orderId) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {

			OneTimeOrder oneTimeOrder = oneTimeOrderService.getOneTimeOrder(orderId, null, false, false);
			apiResponse.addData("oneTimeOrder", new OneTimeOrderDTO(oneTimeOrder));
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;

	}

	@RequestMapping(value = "/restapi/onetimeorder/status")
	@ResponseBody
	public ApiResponse setStatusByTodayOTO(
			@RequestParam(required = true) String orderCode,
			@RequestParam(required = true) String status) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {

			User loggedInUser = userService.getLoggedInUser();
			OneTimeOrder oneTimeOrder = oneTimeOrderService.getOneTimeOrderByCode(orderCode, loggedInUser, true, true);
			oneTimeOrder.setDeliveryBoy(loggedInUser);
			oneTimeOrder.setDeliveryTime(new Date());
			oneTimeOrder.setStatus(OrderStatus.valueOf(status));
			oneTimeOrderService.updateOneTimeOrder(oneTimeOrder.getId(),oneTimeOrder, loggedInUser, true, true);
			apiResponse.addData("oneTimeOrder", new OneTimeOrderDTO(oneTimeOrder));

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;

	}

	/*
	 * @RequestMapping(value="/restapi/onetimeorder/deliveryboy")
	 * 
	 * @ResponseBody public ApiResponse getDeliveryBoys(){
	 * 
	 * ApiResponse apiResponse=new ApiResponse(true); try{ User
	 * logInUser=userService.getLoggedInUser(); List<User>
	 * deliveryBoys=userService.getUsersForRole(Role.ROLE_DELIVERY_BOY,
	 * logInUser); List<UserDTO> dtos=new ArrayList<UserDTO>(); for(User
	 * user:deliveryBoys){ dtos.add(new UserDTO(user)); }
	 * 
	 * apiResponse.addData("deliveryboys", dtos); }catch(ProvilacException e){
	 * e.printStackTrace(); apiResponse.setError(e.getExceptionMsg(),
	 * e.getErrorCode()); } return apiResponse;
	 * 
	 * }
	 */
	@RequestMapping(value="/restapi/onetimeorder/byDeliveryBoy")
    @ResponseBody
    public ApiResponse getTodaysOTOByDeliveryBoy(@RequestParam(required = true)Long deliveryBoyId){
        
        ApiResponse apiResponse=new ApiResponse(true);
        try{
            
            User loggedInUser=userService.getLoggedInUser();
            List<OneTimeOrder> oneTimeOrders=oneTimeOrderService.getTodayOneTimeOrders(null, deliveryBoyId, loggedInUser, true, true);
            List<OneTimeOrderDTO> oneTimeOrderDTOs=new ArrayList<OneTimeOrderDTO>();
            for(OneTimeOrder oneTimeOrder:oneTimeOrders){
                oneTimeOrderDTOs.add(new OneTimeOrderDTO(oneTimeOrder));
            }
            apiResponse.addData("ordersByDeliveryBoy", oneTimeOrderDTOs);
    
        }catch(ProvilacException e){
        e.printStackTrace();
        apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
    }
    return apiResponse;
    
	}
	/*
	 * REST API END
	 */
	

	@RequestMapping(value="/ForwardOTPaymentRequest", method = RequestMethod.POST)
	public String ForwardOTPaymentRequest(Authentication authentication, Model model, HttpServletRequest request,
			@RequestParam(required = true)String date) {
		HttpSession session = request.getSession();	
		if(date.isEmpty() || session.getAttribute("shippingDeliveryAddressCode") == null) {
			String message = "Please select delivery date and address";
			model.addAttribute("message", message);
			return "redirect:/checkout";
		}
		session.setAttribute("deliveryDate", date);
		if(session.getAttribute("shippingDeliveryAddressCode") != null) {
			
			UserInfo userInfo = (UserInfo) authentication.getPrincipal();
			String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
			model.addAttribute("key", key);
			String serverUrl = environment.getProperty("server.url");
			if(serverUrl.endsWith("/")) {
				serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
			}
			model.addAttribute("surl", serverUrl + AppConstants.OT_FRONTEND_PAYU_SURL);
			model.addAttribute("furl", serverUrl + AppConstants.OT_FRONTEND_PAYU_FURL);
			model.addAttribute("firstName", userInfo.getFirstName());
			model.addAttribute("lastName", userInfo.getLastName());
			model.addAttribute("fullName", userInfo.getFullName());
			model.addAttribute("phone", userInfo.getMobileNumber());
			model.addAttribute("email", userInfo.getEmail());
			
			List<CartItem> cartItems = requestInterceptor.getCartItems();
			double amount = 0.0;
			for (CartItem cartItem : cartItems) {
				amount = amount + (cartItem.getProduct().getPrice() * cartItem.getQuantity());
			}

			String txnId = "WEB-OT" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
			String productInfo = txnId;
			model.addAttribute("productInfo", productInfo);
			
			String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
			String amountString = new DecimalFormat("##.##").format(amount);
			model.addAttribute("amount", amountString);
			String udf1 = "";
			model.addAttribute("udf1", udf1);
			String udf2 = "";
			model.addAttribute("udf2", udf2);
			String udf3 = "";
			model.addAttribute("udf3", udf3);
			String udf4 = "";
			model.addAttribute("udf4", udf4);
			String udf5 = ""; 
			model.addAttribute("udf5", udf5);
			String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + userInfo.getFirstName() + "|" + userInfo.getEmail() + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
			String encodedHash = null;
			try {
				encodedHash = hashCal("SHA-512", plainHash);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			model.addAttribute("hash", encodedHash);
			model.addAttribute("txnId", txnId);
			
			return "enduser-pages/paymentRequestForwarder";
		} else {
			return "redirect:/cart";
		}
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.OT_FRONTEND_PAYU_SURL)
	public String handleOTPayUWebPaymentSuccess(Authentication authentication,
												@RequestParam(required=false) String payuMoneyId, Model model,
												@RequestParam(required=false) String mode, HttpServletRequest request) {
		
		String status = "success";
		String firstname = "Golu";
		double amount = 100;
		String txnid = "fdf";
		String hash = "rdr";
		String productinfo = "dfrgr";
		String phone = "fdfb";
		String email = "fdfbd";
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		/*if(!hash.equals(encodedHash)) {
			return "/enduser-pages/payu_success_wrong";
		}

		if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null || oneTimeOrderService.getOneTimeOrderByTxnId(txnid, null, false, false) != null) {
			return "/enduser-pages/payu_success_wrong";
		}*/
		
		HttpSession session = request.getSession();	
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		List<CartItem> cartItems = requestInterceptor.getCartItems();
		if(cartItems == null || cartItems.isEmpty())
			return "redirect:/";
		String addressCode = (String) session.getAttribute("shippingDeliveryAddressCode");
		String deliveryDate = (String) session.getAttribute("deliveryDate");
		Address address = addressService.getAddressByCode(addressCode, true, userService.getLoggedInUser(), false, false);
		
		OneTimeOrder oneTimeOrder = new OneTimeOrder();
		oneTimeOrder.setAddress(address);
		oneTimeOrder.setCustomer(user);
		oneTimeOrder.setDate(DateHelper.parseDate(deliveryDate));
		oneTimeOrder.setFinalAmount(amount);
		oneTimeOrder.setPaymentGateway("PayU");
		oneTimeOrder.setPaymentOrderId(txnid);
		oneTimeOrder.setTotalAmount(amount);
		oneTimeOrder.setTxnId(txnid);
		
		for (CartItem cartItem : cartItems) {
			OrderLineItem orderLineItem = new OrderLineItem();
			orderLineItem.setAmount(cartItem.getProduct().getPrice() * cartItem.getQuantity());
			orderLineItem.setProduct(productService.getProductByCode(cartItem.getProduct().getCode(), true, user, false, false));
			orderLineItem.setQuantity(cartItem.getQuantity());
			oneTimeOrder.getOrderLineItems().add(orderLineItem);
		}
		oneTimeOrder = oneTimeOrderService.createOneTimeOrders(oneTimeOrder, user, false, true);
		String message = "Your Order Has Been Created";
		List<OrderLineItem> orderLineItems = orderLineItemService.getOrderLineItems(oneTimeOrder.getCode(), userService.getLoggedInUser(), false, true);
		model.addAttribute("orderLineItems", orderLineItems);
		model.addAttribute("oneTimeOrder", oneTimeOrder);
		
		requestInterceptor.addDataToSession("OneTimeOrderForInvoice", oneTimeOrder);
		requestInterceptor.addDataToSession("orderLineItemsForInvoice", orderLineItems);
		requestInterceptor.deleteAllItemsFromCart();
		
		return "/enduser-pages/payment";
	}
	
	@RequestMapping(value = "/end-user/oneTimeOrder/sent-invoice-mail")
	@ResponseBody
	public ApiResponse oneTimeOrderInvoice() {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		
		OneTimeOrder oneTimeOrder = (OneTimeOrder) requestInterceptor.getDataFromSession("OneTimeOrderForInvoice");
		List<OrderLineItem> orderLineItems = (List<OrderLineItem>) requestInterceptor.getDataFromSession("orderLineItemsForInvoice");
		String createdDate = DateHelper.getFormattedDateWithMarker(oneTimeOrder.getCreatedDate());
		String deliveryDate = DateHelper.getFormattedDate(oneTimeOrder.getDate());
		emailHelper.sendInvoiceForOneTimeOrder(userService.getLoggedInUser().getEmail(), orderLineItems, oneTimeOrder, "confirmation mail", createdDate, deliveryDate);
		return apiResponse;
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.OT_FRONTEND_PAYU_FURL)
	public String handleOTPayUWebPaymentFailure(@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		return "/enduser-pages/payu_failure";
	}
	
	private String hashCal(String type, String str) throws NoSuchAlgorithmException {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(type);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append("0");
            }
            hexString.append(hex);
        }
        return hexString.toString();
	}
	
}
