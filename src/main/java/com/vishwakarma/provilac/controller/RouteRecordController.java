package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.RouteDTO;
import com.vishwakarma.provilac.dto.RouteRecordDTO;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.mvc.validator.RouteRecordValidator;
import com.vishwakarma.provilac.mvc.validator.RouteValidator;
import com.vishwakarma.provilac.property.editor.RoutePropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.RouteRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.CommonUtils;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class RouteRecordController {

	@Resource
	private RouteRecordValidator routeRecordValidator;

	@Resource
	private RouteRecordService routeRecordService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRepository routeRepository;
	
	@Resource
	private RouteValidator routeValidator;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@InitBinder(value = "routeRecord")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(Route.class, new RoutePropertyEditor(routeRepository));
		binder.setValidator(routeRecordValidator);
	}

	private Model populateModelForAdd(Model model, RouteRecord routeRecord) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("routeRecord", routeRecord);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("routes", routeService.getAllRoutes(null, false, false));
		return model;
	}

	@RequestMapping(value = "/routeRecord/add")
	public String addRouteRecord(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new RouteRecord());
		return "admin-pages/routeRecord/add";
	}

	@RequestMapping(value = "/routeRecord/add", method = RequestMethod.POST)
	public String routeRecordAdd(RouteRecord routeRecord, 
									BindingResult formBinding, 
									Model model) {
		
		routeRecordValidator.validate(routeRecord, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, routeRecord);
			return "admin-pages/routeRecord/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, true,false);
		String message = "RouteRecord added successfully";
		return "redirect:/routeRecord/list?message=" + message;
	}

	@RequestMapping(value = "/admin/routeRecord/list")
	public String getRouteRecordList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<RouteRecord> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = routeRecordService.searchRouteRecord(pageNumber, searchTerm, user,true,false);
		} else {
			page = routeRecordService.getRouteRecords(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("routeRecords", page.getContent());

		return "/admin-pages/routeRecord/list";
	}

	@RequestMapping(value = "/admin/routeRecord/show/{cipher}")
	public String showRouteRecord(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		RouteRecord routeRecord = routeRecordService.getRouteRecord(id, true, loggedIUser,true,false);
		model.addAttribute("routeRecord", routeRecord);
		return "/admin-pages/routeRecord/show";
	}

	@RequestMapping(value = "/admin/routeRecord/update/{cipher}/{customerCode}")
	public String updateRouteRecord(@PathVariable String cipher,
									@PathVariable String customerCode, 
									Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		RouteRecord routeRecord = routeRecordService.getRouteRecord(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, routeRecord);
		User customer = userService.getUserByCode(customerCode, false, loggedInUser, true, false);
		model.addAttribute("customer", customer);
		model.addAttribute("id", routeRecord.getId());
		model.addAttribute("version", routeRecord.getVersion());
		return "/admin-pages/routeRecord/update";
	}

	@RequestMapping(value = "/admin/routeRecord/update", method = RequestMethod.POST)
	public String routeRecordUpdate(RouteRecord routeRecord, 
										BindingResult formBinding, 
										Model model) {

		routeRecord.setUpdateOperation(true);
		routeRecordValidator.validate(routeRecord, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, routeRecord);
			return "admin-pages/routeRecord/update";
		}
		routeRecord.setId(Long.parseLong(model.asMap().get("id") + ""));
		routeRecord.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		Set<RouteRecord> records = new HashSet<RouteRecord>(routeRecord.getRoute().getRouteRecords());
		
		routeRecordService.updateRouteRecord(routeRecord.getId(), routeRecord, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "RouteRecord updated successfully";
		return "redirect:/admin/routeRecord/list?message=" + message;
	}

	@RequestMapping(value = "/admin/routeRecord/delete/{cipher}")
	public String deleteRouteRecord(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		RouteRecord record = routeRecordService.getRouteRecord(id, false);
		String message = "";
		try {
			deletedRouteRecordService.deleteAllRecordsForCustomer(record.getCustomer().getId());
			DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
			deletedRouteRecord.setRoute(record.getRoute());
			deletedRouteRecord.setCustomer(record.getCustomer());
			deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			
			routeRecordService.deleteRouteRecord(id, record.getRoute().getId());
			
			User customer = userService.getUser(record.getCustomer().getId(), false, userService.getLoggedInUser(), false, false);
			customer.setIsRouteAssigned(false);
			userService.updateUser(customer.getId(), customer, null, false, false);
			message = "RouteRecord deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete RouteRecord";
		}
		return "redirect:/admin/routeRecord/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */

	//USED_FROM_WEB showCalendar (deliveryschedule & undelivered order)
	@RequestMapping(value="/routeRecord/getusers")
	@ResponseBody
	public ApiResponse getUsers(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getActiveAndOnHoldUsers(Role.ROLE_CUSTOMER, Status.ACTIVE, Status.HOLD, loggedInUser, false, false);
			List<UserDTO>dtos = new ArrayList<UserDTO>();
			for (User user : customers) {
				if(user.hasRole(Role.ROLE_CUSTOMER)&&(user.getIsRouteAssigned()==true)){
					dtos.add(new UserDTO(user));
				}
			}
			apiResponse.addData("customers", dtos);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/routeRecord/getusers1")
	@ResponseBody
	public ApiResponse getUsers1(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getAllUsers(loggedInUser, false, false);
			List<UserDTO>dtos = new ArrayList<UserDTO>();
			for (User user : customers) {
				dtos.add(new UserDTO(user));
			}
			apiResponse.addData("customers", dtos);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_WEB assignRoute, changeRoute & user add
	@RequestMapping(value="/routeRecord/routes")
	@ResponseBody
	public ApiResponse getRoutes(@RequestParam(required=false)Double lat,
								  @RequestParam(required=false)Double lng,
								  @RequestParam(required =false)String routeName,
								  @RequestParam(required=false)String customerCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try {
				if(null!=routeName) {
					Route routetoAdd = new Route();
					routetoAdd.setName(routeName);
					BindingResult bindingResult = new DataBinder(routetoAdd).getBindingResult();
					routeValidator.validate(routetoAdd, bindingResult);
					if(bindingResult.hasErrors()) {
						String errorStr= null;
						for (ObjectError objectError : bindingResult.getAllErrors()) {
							if(StringUtils.isBlank(errorStr)) {
								errorStr= objectError.getDefaultMessage();
							}else{
								errorStr=errorStr+ ", "+ objectError.getDefaultMessage();
							}
							
						}
							throw new ProvilacException(errorStr, "400");
						
					}
					
					routeService.createRoute(routetoAdd, userService.getLoggedInUser(), false, false);
				}
				List<RouteRecord> records = routeRecordService.getAllRouteRecords(null, false, false);

				Map<Route, List<RouteRecord>> routeRecordMap = new HashMap<Route, List<RouteRecord>>();
				Map<Route, Double> map = new HashMap<Route, Double>();
			
				for (RouteRecord routeRecord : records) {
					double difference =0.0;
					if(null != routeRecord.getCustomer() && null!=routeRecord.getCustomer().getLat() && null!=routeRecord.getCustomer().getLng()){
						if(null!=lat && null!=lng) {
							difference = CommonUtils.getLatLngDifferenceInKms(lat, lng, routeRecord.getCustomer().getLat(),routeRecord.getCustomer().getLng());
						} else {
							if(null!=customerCode){
								User customer = userService.getUserByCode(customerCode, true, userService.getLoggedInUser(), false, false);
								if(null!=customer.getLat()&&null!=customer.getLng()){
									difference = CommonUtils.getLatLngDifferenceInKms(customer.getLat(), customer.getLng(), routeRecord.getCustomer().getLat(),routeRecord.getCustomer().getLng());
								}
							}
						}
						Route route = routeRecord.getRoute();
						if(map.containsKey(route) && map.get(route) < difference) {
							difference = map.get(route);
						}
						map.put(routeRecord.getRoute(), difference);
					
						if(!routeRecordMap.containsKey(route)) {
							routeRecordMap.put(route, new ArrayList<RouteRecord>());
						}
						routeRecordMap.get(route).add(routeRecord);
					}
				}
				List<Entry<Route, Double>> sortedEntries = new ArrayList<Map.Entry<Route,Double>>(map.entrySet());
			
				Collections.sort(sortedEntries, new Comparator<Map.Entry<Route, Double>>() {
					@Override
					public int compare(Entry<Route, Double> o1, Entry<Route, Double> o2) {
						return o1.getValue().compareTo(o2.getValue());
					}
				});
				List<RouteDTO> dtos = new ArrayList<RouteDTO>();
				List<Route> sortedRoutes= new ArrayList<Route>();
				Map<Route,  List<RouteRecord>> sortedMap = new HashMap<Route,  List<RouteRecord>>();
				for(int i = 0; (i < 3 && i < sortedEntries.size()); i++) {
					Route route = sortedEntries.get(i).getKey();
					sortedRoutes.add(route);
					List<RouteRecord> routeRecords = routeRecordMap.get(route);
					Collections.sort(routeRecords, new Comparator<RouteRecord>() {
	
						@Override
						public int compare(RouteRecord arg0, RouteRecord arg1) {
							return arg0.getPriority() - arg1.getPriority();
						}
					});
					dtos.add(new RouteDTO(route, routeRecords));
					sortedMap.put(route, routeRecords);
				}
				List<Route>routes= routeService.getAllRoutes(null, false, true);
				List<RouteDTO> routeDTOs = new ArrayList<RouteDTO>();
				Map<Route,  Set<RouteRecord>> sortedMap1 = new HashMap<Route,  Set<RouteRecord>>();
				for (Route route : routes) {
					if(route.getRouteRecords().size()==0){
						sortedMap1.put(route, route.getRouteRecords());
						routeDTOs.add(new RouteDTO(route));
					}
				}
				Set<Route> routesWithoutLatAndLng = new HashSet<Route>();
				for (Route route : routes) {
					if(!sortedMap.containsKey(route) && !sortedMap1.containsKey(route)){
						routesWithoutLatAndLng.add(route);
					}
				}
			
				Map<Route, List<RouteRecord>> routeRecordMap1 = new HashMap<Route, List<RouteRecord>>();
				for (Route route : routesWithoutLatAndLng) {
					if(route.getRouteRecords().size()!=0){
						for (RouteRecord routeRecord : route.getRouteRecords()) {
							if(routeRecord.getCustomer().getLat()==null && routeRecord.getCustomer().getLng()==null){
								if(!routeRecordMap1.containsKey(route)) {
									routeRecordMap1.put(route, new ArrayList<RouteRecord>());
								}
								routeRecordMap1.get(route).add(routeRecord);
							}
						}
					}
					List<RouteRecord> routeRecords = routeRecordMap1.get(route);
					routeDTOs.add(new RouteDTO(route, routeRecords));
				}
				dtos.addAll(routeDTOs);
				apiResponse.addData("routes", dtos);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_WEB UserSubscription, SubscriptionRequest & DeliverySchedule add.jsp
	@RequestMapping(value="/restapi/routeRecord/checkRouteRecordForCustomer")
	@ResponseBody
	public ApiResponse checkRouteForCustomer(@RequestParam(required = true) String customerId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();

			List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecords(loggedInUser, false, true);
			Map<String,Boolean> customerRecord = new HashMap<String, Boolean>();
			customerRecord.put("isRouteAssign", false);
			customerRecord.put("defaultAddress", false);
			
			for (RouteRecord routeRecord : routeRecords) {
				if (routeRecord.getCustomer().getId() == Long.parseLong(customerId)) {
					customerRecord.put("isRouteAssign", true);
					break;
				}
			}
			User customer = userService.getUser(Long.parseLong(customerId), true, loggedInUser, false, false);
			Address  address = addressService.getDefaultAddressByCustomer(customer.getCode(), false, null, false, false);
			if(null != address){
				customerRecord.put("defaultAddress", true);
			}
			apiResponse.addData("customerRecord", customerRecord);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/routeRecord/listByCustomers")
	@ResponseBody
	public ApiResponse getUsers(@RequestParam(required=true)String customerCodes[]){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<RouteRecordDTO>  dtos = new ArrayList<RouteRecordDTO>();
			 for (String code : customerCodes) {
				RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(code, false, null, false, false);
				if(null!= routeRecord){
					dtos.add(new RouteRecordDTO(routeRecord));
				}
			}
			apiResponse.addData("records", dtos);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*
	 * End Ajax Method
	 * 
	 */
	/**
	 * REST API Methods
	 */

	/**
	 * End REST API Methods
	 */
}
