package com.vishwakarma.provilac.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.ProductValidator;
import com.vishwakarma.provilac.property.editor.CategoryPropertyEditor;
import com.vishwakarma.provilac.repository.CategoryRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CategoryService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.web.RequestInterceptor;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class ProductController {

	@Resource
	private ProductValidator productValidator;

	@Resource
	private ProductService productService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private CategoryService categoryService;
	
	@Resource
	private CategoryRepository categoryRepository;
	
	@Resource
	private Environment environment;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@InitBinder(value = "product")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(Category.class, new CategoryPropertyEditor(categoryRepository));
		binder.setValidator(productValidator);
	}

	private Model populateModelForAdd(Model model, Product product) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("product", product);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("categories", categoryService.getAllCategorys(loggedInUser, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/product/add")
	public String addProduct(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Product());
		return "admin-pages/product/add";
	}

	@RequestMapping(value = "/admin/product/add", method = RequestMethod.POST)
	public String productAdd(Product product, 
									BindingResult formBinding, 
									Model model,
									@RequestParam(required = true, value="productPic")MultipartFile productPic,
									@RequestParam(required = true, value="productBan")MultipartFile productBanner,
									@RequestParam(required = true, value="productCart")MultipartFile productCartPic,
									@RequestParam(required = true, value="productList")MultipartFile productListPic,
									@RequestParam(required = true, value="productDetails")MultipartFile productDetailsPic){
		
		productValidator.validate(product, formBinding);
		
		if (null != productPic && !productPic.isEmpty()) {
			String contentType = productPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productPicUrl", "Please select valid  product pic"));
			} else if (productPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productPicUrl", "Please select product image Picture less than 5MB"));
			}
			try {
				product.setPicData(productPic.getBytes());
				product.setTempFileName(productPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productPicUrl", "Please select valid product Pic"));
		}
		
		if (null != productBanner && !productBanner.isEmpty()) {
			String contentType = productBanner.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productBannerUrl", "Please select valid  product banner"));
			} else if (productBanner.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productBannerUrl", "Please select product banner image Picture less than 5MB"));
			}
			try {
				product.setBannerData(productBanner.getBytes());
				product.setBannerFileName(productBanner.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productBannerUrl", "Please select valid product banner"));
		}
		
		if (null != productCartPic && !productCartPic.isEmpty()) {
			String contentType = productCartPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select valid  product cart pic"));
			} else if (productCartPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select product cart image Picture less than 5MB"));
			}
			try {
				product.setProductCartPicData(productCartPic.getBytes());
				product.setProductCartPicFileName(productCartPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select valid product cart Pic"));
		}
		
		if (null != productListPic && !productListPic.isEmpty()) {
			String contentType = productListPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productListPicUrl", "Please select valid  product list pic"));
			} else if (productListPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productListPicUrl", "Please select product list image Picture less than 5MB"));
			}
			try {
				product.setProductListPicData(productListPic.getBytes());
				product.setProductListPicFileName(productListPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productListPicUrl", "Please select valid product list Pic"));
		}
		
		if (null != productDetailsPic && !productDetailsPic.isEmpty()) {
			String contentType = productDetailsPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select valid  product details pic"));
			} else if (productDetailsPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select product details image Picture less than 5MB"));
			}
			try {
				product.setProductDetailsPicData(productDetailsPic.getBytes());
				product.setProductDetailsPicFileName(productDetailsPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select valid product details Pic"));
		}
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, product);
			return "admin-pages/product/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		productService.createProduct(product, loggedInUser, true,false);
		String message = "Product added successfully";
		return "redirect:/admin/product/list?message=" + message;
	}

	@RequestMapping(value = "/admin/product/list")
	public String getProductList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Product> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = productService.searchProduct(pageNumber, searchTerm, user,true,false);
		} else {
			page = productService.getProducts(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("products", page.getContent());

		return "/admin-pages/product/list";
	}
	
	@RequestMapping(value = "/product")
	public String getProductListByCategoryCode(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm,
										@RequestParam(required = false) String categoryCode) {
		
		/*User loggedInUser = userService.getLoggedInUser();*/
		if (StringUtils.isEmpty(categoryCode)) {
			return "redirect:/allCategories";
		}
		model.addAttribute("message", message);
		Page<Product> page = null;
		/*User user = userService.getLoggedInUser();*/
		if (StringUtils.isNotBlank(searchTerm)) {
			page = productService.searchProduct(pageNumber, searchTerm, null , false, false);
		} else {
			page = productService.getProductByCategoryCode(pageNumber, categoryCode, false, null, false, true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("products", page.getContent());
		model.addAttribute("categoryCode", categoryCode);
		model.addAttribute("categoryName", categoryService.getCategoryByCode(categoryCode, false, null, false, false).getName());
		return "/enduser-pages/product";
	}
	
	
	@RequestMapping(value="/restapi/getProductPicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProductPic(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PRODUCT_PICS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PRODUCT_PICS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value="/restapi/getProductBanner", method=RequestMethod.GET)
	@ResponseBody
	public void getProductBanner(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PRODUCT_BANNERS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PRODUCT_BANNERS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value="restapi/getProductCartPicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProductCartPics(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PRODUCT_CART_PICS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PRODUCT_CART_PICS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value="restapi/getProductListPicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProductListPics(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PRODUCT_LIST_PICS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PRODUCT_LIST_PICS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value="restapi/getProductDetailsPicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProductDetailsPics(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PRODUCT_DETAILS_PICS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PRODUCT_DETAILS_PICS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "/admin/product/show/{cipher}")
	public String showProduct(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Product product = productService.getProduct(id, true, loggedIUser,true,false);
		model.addAttribute("product", product);
		return "/admin-pages/product/show";
	}
	
	@RequestMapping(value = "/productDetails")
	public String showProduct(Model model, @RequestParam(required = false) String productCode,
			@RequestParam(required = false) String categoryCode, HttpServletRequest request) {

		if (StringUtils.isEmpty(productCode)) {
			return "redirect:/product?categoryCode="+categoryCode;
		}
		Product product = new Product();
		requestInterceptor.addDataToSession("prepayDuration", 3);
		HttpSession session = request.getSession();
		if(session.getAttribute("proceedSelected") != null) {
			product = productService.getProductByCode(productCode, false, userService.getLoggedInUser(), true, false);
			model.addAttribute("product", product);
			return "/enduser-pages/create-subscription/subscriptionProductDetails";
		}
		
		if(session.getAttribute("changeDeliveryPattern") != null) {
			product = productService.getProductByCode(productCode, false, userService.getLoggedInUser(), true, false);
			model.addAttribute("product", product);
			return "/enduser-pages/change-delivery-pattern/subscriptionProductDetails";
		}
		product = productService.getProductByCode(productCode, false, null, false, false);
		model.addAttribute("product", product);
		return "/enduser-pages/productDetails";
	}
	
	@RequestMapping(value="/admin/prepayPricing")
	public String updatePrepayPricing(Model model) {
		
		model = populateModelForPrepay(model, null, null, 0);
		return "/admin-pages/product/prepayPricing";
	}
	
	@RequestMapping(value="/admin/prepayPricing", method=RequestMethod.POST)
	public String prepayPricingUpdate(Model model, 
										String[] productCode, 
										Double[] productPrice, 
										Integer prepayAmount) {
		
		if(prepayAmount == null || prepayAmount <= 0) {
			model.addAttribute("errorMessage", "Please enter valid prepay amount");
			model = populateModelForPrepay(model, productCode, productPrice, prepayAmount);
			return "/admin-pages/product/prepayPricing";
		}
		List<Product> productsToUpdate = new ArrayList<Product>();
		for(int i = 0; i < productCode.length; i++) {
			String code = productCode[i];
			Double price = productPrice[i];
			Product product = productService.getProductByCode(code, false, null, false, true);
			if(product == null) {
				model.addAttribute("errorMessage", "Please select valid product");
				model = populateModelForPrepay(model, productCode, productPrice, prepayAmount);
				return "/admin-pages/product/prepayPricing";
			}
			if(price <= 0) {
				model.addAttribute("errorMessage", "Please enter valid price for " + product.getName());
				model = populateModelForPrepay(model, productCode, productPrice, prepayAmount);
				return "/admin-pages/product/prepayPricing";
			}
			product.getPricingMap().put(prepayAmount, price);
			productsToUpdate.add(product);
		}
		productService.updatePrepayPricing(productsToUpdate, null, false, true);
		return "redirect:/admin/product/list";
	}
	
	private Model populateModelForPrepay(Model model, String[] productCodes, Double[] productPrices, Integer prepayAmount) {
		
		if(null != productCodes && null != productPrices && productCodes.length > 0 && productPrices.length > 0) {
			Map<String, Double> productPricing = new HashMap<String, Double>();
			for(int i = 0; i < productCodes.length; i++) {
				productPricing.put(productCodes[i], productPrices[i]);
			}
			model.addAttribute("pricingMap", productPricing);
		}
		if(null != prepayAmount) {
			model.addAttribute("prepayAmount", prepayAmount);
		}
		model.addAttribute("products", productService.getAllProducts(null, false, true));
		return model;
	}

	@RequestMapping(value = "/admin/product/update/{cipher}")
	public String updateProduct(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		Product product = productService.getProduct(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, product);
		model.addAttribute("id", product.getId());
		model.addAttribute("version", product.getVersion());
		return "/admin-pages/product/update";
	}

	@RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
	public String productUpdate(Product product, 
										BindingResult formBinding, 
										Model model,
										@RequestParam(required = true, value="productPic")MultipartFile productPic,
										@RequestParam(required = true, value="productBan")MultipartFile productBanner,
										@RequestParam(required = true, value="productCart")MultipartFile productCartPic,
										@RequestParam(required = true, value="productList")MultipartFile productListPic,
										@RequestParam(required = true, value="productDetails")MultipartFile productDetailsPic) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		product.setUpdateOperation(true);
		product.setId(Long.parseLong(model.asMap().get("id") + ""));
		product.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		productValidator.validate(product, formBinding);
		
		
		if (null != productPic && !productPic.isEmpty()) {
			String contentType = productPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productPicUrl", "Please select valid  product pic"));
			} else if (productPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productPicUrl", "Please select product image Picture less than 5MB"));
			}
			try {
				product.setPicData(productPic.getBytes());
				product.setTempFileName(productPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		if (null != productBanner && !productBanner.isEmpty()) {
			String contentType = productBanner.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productBannerUrl", "Please select valid  product banner"));
			} else if (productBanner.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productBannerUrl", "Please select product banner image Picture less than 5MB"));
			}
			try {
				product.setBannerData(productBanner.getBytes());
				product.setBannerFileName(productBanner.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (null != productCartPic && !productCartPic.isEmpty()) {
			String contentType = productCartPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select valid  product cart pic"));
			} else if (productCartPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select product cart image Picture less than 5MB"));
			}
			try {
				product.setProductCartPicData(productCartPic.getBytes());
				product.setProductCartPicFileName(productCartPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productCartPicUrl", "Please select valid product cart Pic"));
		}
		
		if (null != productListPic && !productListPic.isEmpty()) {
			String contentType = productListPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productListPicUrl", "Please select valid  product list pic"));
			} else if (productListPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productListPicUrl", "Please select product list image Picture less than 5MB"));
			}
			try {
				product.setProductListPicData(productListPic.getBytes());
				product.setProductListPicFileName(productListPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productListPicUrl", "Please select valid product list Pic"));
		}
		
		if (null != productDetailsPic && !productDetailsPic.isEmpty()) {
			String contentType = productDetailsPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select valid  product details pic"));
			} else if (productDetailsPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select product details image Picture less than 5MB"));
			}
			try {
				product.setProductDetailsPicData(productDetailsPic.getBytes());
				product.setProductDetailsPicFileName(productDetailsPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("product", "productDetailsPicUrl", "Please select valid product details Pic"));
		}
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, product);
			return "admin-pages/product/update";
		}
		
		productService.updateProduct(product.getId(), product, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Product updated successfully";
		return "redirect:/admin/product/list?message=" + message;
	}

	@RequestMapping(value = "/admin/product/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			productService.deleteProduct(id);
			message = "Product deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Product";
		}
		return "redirect:/admin/product/list?message=" + message;
	}
	/**
	 * Import Product
	 */
	
	@RequestMapping(value="/admin/product/import", method=RequestMethod.GET)
	public String importProducts(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/product/import";
	}
	
	@RequestMapping(value="/admin/product/import", method=RequestMethod.POST)
	public String importProducts(Model model, @RequestParam(required=true) MultipartFile file) {
		
		String message = "Successfully imported products";
		if(null == file || file.isEmpty() || !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)) {
			
			message = "Please select valid excel 2007 or later file";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/product/import";
		}
		try {
			importProducts(file);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error uploading products, please try with valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/product/import";
		}
		return "redirect:/admin/product/list?message=" + message;
	}
	
	@RequestMapping(value = "/onetimeorder-products")
	public String getProductListByCategoryCodeForOnetimeorder(Model model,
										@RequestParam(required = true) String categoryCode) {
		
		if (StringUtils.isEmpty(categoryCode)) {
			return "redirect:/end-user/onetimeorder/categories";
		}
		Category category = categoryService.getCategoryByCode(categoryCode, true, null, false, true);
		List<Product> productsList = productService.getAllProductsByCategoryForOnetimeorder(null, category, true, false, false);
		model.addAttribute("productsList", productsList);
		model.addAttribute("category", category);
		return "/enduser-pages/oneTimeOrder/product";
	}
	
	private void importProducts(MultipartFile excelFile) throws InvalidFormatException, IOException{
		Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);
		
		int rawCount = 0;
		DataFormatter formatter = new DataFormatter();
		Product product = null;
		for (Row row : sheet) {
			
			if(rawCount==0){
				rawCount++;
				continue;
			}
			
			Cell productNameCell = row.getCell(0);
			String productName = productNameCell.getStringCellValue();
			
			Cell priceCell = row.getCell(1);
			String price = formatter.formatCellValue(priceCell);
			
			Cell categoryNameCell = row.getCell(2);
			String categoryName = categoryNameCell.getStringCellValue();
			
			product =productService.getProductByName(productName, false, null, false, false);
			if(null==product){
				 product = new Product();
			}
			
			Category category = categoryService.getCategoryByName(categoryName, false, userService.getLoggedInUser(), false, false);
			
			if(null == category){
				category = new Category();
				category.setName(categoryName);
				category = categoryService.createCategory(category, userService.getLoggedInUser(), false,false);
			}
			
			product.setCategory(category);
			product.setName(productName);
			product.setPrice(Double.parseDouble(price));
			
			BindingResult bindingResult = new DataBinder(product).getBindingResult();
			productValidator.validate(product, bindingResult);
			
			if(bindingResult.hasErrors()) {
				continue;
			}
			User loggedInUser = userService.getLoggedInUser();
			
			productService.createProduct(product, loggedInUser, false, false);
			
			rawCount++;
		
		}
		
		
	}
	
	
	
	/**
	 * End Import Product
	 */
	
	/*
	 * Ajax Method
	 * 
	 */
	

	/**
	 * REST API Methods
	 */


	@RequestMapping(value="/restapi/product/list")
	@ResponseBody
	public ApiResponse getAllProducts(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Product>products = productService.getAllProducts(null, false, false);
			List<ProductDTO> dtos = new ArrayList<ProductDTO>();
			for (Product product : products) {
				dtos.add(new ProductDTO(product));
			}
			apiResponse.addData("products", dtos);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	

	@RequestMapping(value="/openapi/product/list")
	@ResponseBody
	public ApiResponse productsOpenApi(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Product>products = productService.getAllProducts(null, false, false);
			List<ProductDTO> dtos = new ArrayList<ProductDTO>();
			for (Product product : products) {
				dtos.add(new ProductDTO(product));
			}
			apiResponse.addData("products", dtos);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * End REST API Methods
	 */
}
