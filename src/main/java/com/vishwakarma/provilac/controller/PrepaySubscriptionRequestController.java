package com.vishwakarma.provilac.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.mvc.validator.PrepaySubscriptionRequestValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.PrepayRequestLineItemService;
import com.vishwakarma.provilac.service.PrepaySubscriptionRequestService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class PrepaySubscriptionRequestController {

	@Resource
	private PrepaySubscriptionRequestValidator prepaySubscriptionRequestValidator;

	@Resource
	private PrepaySubscriptionRequestService prepaySubscriptionRequestService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource 
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private PrepayRequestLineItemService prepayPrepayRequestLineItemService;
	
	@Resource AsyncJobs asyncJobs;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private Environment environment;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@InitBinder(value = "prepaySubscriptionRequest")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(prepaySubscriptionRequestValidator);
	}

	private Model populateModelForAdd(Model model, PrepaySubscriptionRequest prepaySubscriptionRequest) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("prepaySubscriptionRequest", prepaySubscriptionRequest);
		List<User> newCustomers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.NEW, loggedInUser, false, false);
		model.addAttribute("newCustomers", newCustomers);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/prepaySubscriptionRequest/list")
	public String getUserSubscriptionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<PrepaySubscriptionRequest> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = prepaySubscriptionRequestService.searchPrepaySubscriptionRequests(pageNumber, searchTerm, user, true, true);
		} else {
			page = prepaySubscriptionRequestService.getPrepaySubscriptionRequests(pageNumber, user, true, true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("prepaySubscriptionRequest", page.getContent());

		return "/admin-pages/prepaySubscriptionRequest/list";
	}
	
	@RequestMapping(value = "/admin/prepaySubscriptionRequest/changeList")
	public String getChangeUserSubscriptionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<PrepaySubscriptionRequest> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = prepaySubscriptionRequestService.searchChangePrepaySubscriptionRequests(pageNumber, searchTerm, user, true, true);
		} else {
			page = prepaySubscriptionRequestService.getChangePrepaySubscriptionRequests(pageNumber, user, true, true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("prepaySubscriptionRequest", page.getContent());

		return "/admin-pages/prepaySubscriptionRequest/changeList";
	}

	@RequestMapping(value = "/admin/prepaySubscriptionRequest/show/{cipher}")
	public String showPrepaySubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		PrepaySubscriptionRequest prepaySubscriptionRequest =prepaySubscriptionRequestService.getPrepaySubscriptionRequest(id, true, loggedInUser,true,true);
		model.addAttribute("prepaySubscriptionRequest", prepaySubscriptionRequest);
		return "/admin-pages/prepaySubscriptionRequest/show";
	}
	
	@RequestMapping(value = "/admin/prepaySubscriptionRequest/deleteNew/{cipher}")
	public String deletePrepaySubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		PrepaySubscriptionRequest prepaySubscriptionRequest =prepaySubscriptionRequestService.getPrepaySubscriptionRequest(id, true, loggedInUser,true,true);
		
		//Create Payment Inward Entry
		Payment payment = new Payment();
		payment.setTxnId(prepaySubscriptionRequest.getTxnId());
		payment.setDate(prepaySubscriptionRequest.getCreatedDate());
		payment.setCustomer(prepaySubscriptionRequest.getUser());
		payment.setAmount(prepaySubscriptionRequest.getFinalAmount());
		payment.setApprovedByPG(prepaySubscriptionRequest.getIsApprovedByPG());
		payment = paymentService.createPayment(payment, loggedInUser, false, false);
		
		User customer = payment.getCustomer();
		customer.setLastPendingDues(customer.getLastPendingDues() - payment.getAmount());
		userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
		
		prepaySubscriptionRequestService.deletePrepaySubscriptionRequest(prepaySubscriptionRequest.getId());
		return "/admin-pages/prepaySubscriptionRequest/list";
	}
	
	@RequestMapping(value = "/admin/prepaySubscriptionRequest/deleteChange/{cipher}")
	public String deleteChangePrepaySubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		PrepaySubscriptionRequest prepaySubscriptionRequest =prepaySubscriptionRequestService.getPrepaySubscriptionRequest(id, true, loggedInUser,true,true);
		
		//Create Payment Inward Entry
		Payment payment = new Payment();
		payment.setTxnId(prepaySubscriptionRequest.getTxnId());
		payment.setDate(prepaySubscriptionRequest.getCreatedDate());
		payment.setCustomer(prepaySubscriptionRequest.getUser());
		payment.setAmount(prepaySubscriptionRequest.getFinalAmount());
		payment.setApprovedByPG(prepaySubscriptionRequest.getIsApprovedByPG());
		payment = paymentService.createPayment(payment, loggedInUser, false, false);
		
		User customer = payment.getCustomer();
		customer.setLastPendingDues(customer.getLastPendingDues() - payment.getAmount());
		userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
		
		prepaySubscriptionRequestService.deletePrepaySubscriptionRequest(prepaySubscriptionRequest.getId());
		return "/admin-pages/prepaySubscriptionRequest/changeList";
	}
	
	@RequestMapping(value="/admin/prepaySubscriptionRequest/assignRoute/{userCode}/{requestCode}")
	public String changeRoute(Model model, @PathVariable String userCode,@PathVariable String requestCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		User user = userService.getUserByCode(userCode, false, loggedInUser, false, false);
		PrepaySubscriptionRequest prepaySubscriptionRequest = prepaySubscriptionRequestService.getPrepaySubscriptionRequestByCode(requestCode, false, loggedInUser, false, false);
		model.addAttribute("request", prepaySubscriptionRequest);
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		return "/admin-pages/prepaySubscriptionRequest/assignRoute";
	}
	
	@RequestMapping(value="/admin/prepaySubscriptionRequest/assignRoute", method=RequestMethod.POST)
	public String assignRoute(@RequestParam(required=false , value="selectedRoute") String selectedRouteCode,
							  @RequestParam(required=false,  value="selectedCustomer") String selectedCustomerCode,
							  @RequestParam(required =true,value="user")String customerCode,
							  @RequestParam(required=false,value="routeName")String routeName,
							  @RequestParam(required=false,value="requestCode")String requestCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		User customer = userService.getUserByCode(customerCode, false, loggedInUser, false, false);
		
		boolean isAlreadyPrepaidCustomer = false;
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customerCode, true, loggedInUser, false, false);
		for (UserSubscription userSubscription : userSubscriptions) {
			if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				isAlreadyPrepaidCustomer = true;
			}
		}
		
		if(customer.getLastPendingDues() > 0 && !isAlreadyPrepaidCustomer) {
			return "redirect:/admin/prepaySubscriptionRequest/list?message=Customer has pending dues.";
		}
		
		Route route = routeService.getRouteByCode(selectedRouteCode, false, null, false, true);
		if(null!=selectedCustomerCode){
			RouteRecord routeRecordForSelectedCustomer = routeRecordService.getRouteRecordByCustomer(selectedCustomerCode, false, loggedInUser, false, false);
			List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);
			RouteRecord	routeRecord = new RouteRecord();
			routeRecord.setCustomer(customer);
			routeRecord.setPriority(routeRecordForSelectedCustomer.getPriority());
			routeRecord.setRoute(route);
			route.getRouteRecords().add(routeRecord);
			routeService.updateRoute(route.getId(), route, loggedInUser, false, false);
			routeRecord = routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			
			//Update route of DeliverySchedules of customer whose route is changed
			 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
			
			//is route assign mark true when route is assign 
			customer.setIsRouteAssigned(true);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
			
			Route lastRoute = null;
			for(RouteRecord existingRouteRecord:existingRouteRecords){
				lastRoute = routeRecord.getRoute();
				routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
			}
			
			if(null != lastRoute) {
				deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
				DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
				deletedRouteRecord.setRoute(lastRoute);
				deletedRouteRecord.setCustomer(customer);
				deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			}
		} else {
				List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);					
				RouteRecord routeRecord = new RouteRecord();
				routeRecord.setCustomer(customer);
				routeRecord.setPriority(1);
				routeRecord.setRoute(route);
				routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);

				//Update route of DeliverySchedules of customer whose route is changed
				 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
				
				customer.setIsRouteAssigned(true);
				userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
				
				Route lastRoute = null;
				for(RouteRecord existingRouteRecord:existingRouteRecords){
					lastRoute = routeRecord.getRoute();
					routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
				}
				
				if(null != lastRoute) {
					deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
					DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
					deletedRouteRecord.setRoute(lastRoute);
					deletedRouteRecord.setCustomer(customer);
					deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
				}
		}
		
		PrepaySubscriptionRequest request = prepaySubscriptionRequestService.getPrepaySubscriptionRequestByCode(requestCode, false, null, false, true);
		
		UserSubscription subscription  = new UserSubscription();
		subscription.setUser(request.getUser());
		
		if(request.getStartDate().before(new Date())) {
			request.setStartDate(new Date());
			DateHelper.addDays(request.getStartDate(), 1);
		}
		subscription.setStartDate(request.getStartDate());
		subscription.setEndDate(DateHelper.addMonths(request.getStartDate(), request.getDuration()));
		
		subscription.setIsCurrent(true);
		subscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
		subscription.setProductPricing(request.getProductPricing());
		
		//Payment details
		subscription.setTxnId(request.getTxnId());
		subscription.setTotalAmount(request.getTotalAmount());
		subscription.setFinalAmount(request.getFinalAmount());
		subscription.setPointsDiscount(request.getPointsDiscount());
		subscription.setPointsRedemed(request.getPointsRedemed());
		subscription.setPaymentGateway(request.getPaymentGateway());
		subscription.setIsApprovedByPG(request.getIsApprovedByPG());
		
		for (UserSubscription userSubscription : userSubscriptions) {
			userSubscription.setIsCurrent(false);
			userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, loggedInUser, false, false);
		}
		
		for (PrepayRequestLineItem item : request.getPrepayRequestLineItems()) {
			SubscriptionLineItem lineItem = new SubscriptionLineItem();
			if(item.getType().equals(SubscriptionType.Daily)){
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
			} else if(item.getType().equals(SubscriptionType.Weekly)) {
				lineItem.setDay(item.getDay());
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
			} else {
				lineItem.setCustomPattern(item.getCustomPattern());
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
			}
		}

		subscription = userSubscriptionService.createUserSubscription(subscription, loggedInUser, false, true);
		if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"ACTIVE", "prepaySubscriptionRequest", customer.getId());
		customer.setStatus(Status.ACTIVE);
		
		//Pending dues calculations
		double pendingDues = customer.getLastPendingDues();
		if(pendingDues < 0) {
			//Assuming prepaid customer will not have -ve pending dues
			customer.setLastPendingDues(pendingDues * -1);
		}
		customer = userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
		subscription.setUser(customer);
		
		//subscription request delete after subscription  create 
		prepaySubscriptionRequestService.deletePrepaySubscriptionRequest(request.getId());
		
		asyncJobs.createFirstTimePrepayDeliverySchedules(subscription, subscription.getSubscriptionLineItems(), userService.getLoggedInUser());
		
		
		//Send welcome email + invoice
		Map<Product, Double> effectivePriceMap = new HashMap<Product, Double>();
		for(Entry<Long, Double> pricingEntry : subscription.getProductPricing().entrySet()) {
			effectivePriceMap.put(productService.getProduct(pricingEntry.getKey(), true), pricingEntry.getValue());
		}
		emailHelper.sendMailForPrepaySubscriptionApproval(subscription, effectivePriceMap);
		if (customer.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "Prepay subscription started successfully";
			return "redirect:/admin/prepaySubscriptionRequest/list?message="+message;	
		}
		
		String message = "Prepay subscription started successfully";
		return "redirect:/admin/prepaySubscriptionRequest/list?message="+message;
	}
	
	@RequestMapping(value="/admin/prepaySubscriptionRequest/approve/{requestCode}")
	public String approveChangePrepaySubscriptionRequest(@PathVariable String requestCode) {
		
		PrepaySubscriptionRequest request = prepaySubscriptionRequestService.getPrepaySubscriptionRequestByCode(requestCode, false, null, false, true);
		if(null != request){
			User customer=request.getUser();
			
			boolean isAlreadyPrepaidCustomer = false;
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, customer, true, true);
			for (UserSubscription userSubscription : userSubscriptions) {
				if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
					isAlreadyPrepaidCustomer = true;
				}
			}

			if(customer.getLastPendingDues() > 0 && !isAlreadyPrepaidCustomer) {
				return "redirect:/admin/prepaySubscriptionRequest/changeList?message=Customer has pending dues";
			}
			
			for (UserSubscription userSubscription : userSubscriptions) {
				userSubscription.setIsCurrent(false);
				userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, customer, true, true);
			}
			
			UserSubscription subscription  = new UserSubscription();
			subscription.setUser(request.getUser());
			
			if(request.getStartDate().before(new Date())) {
				request.setStartDate(new Date());
				DateHelper.addDays(request.getStartDate(), 1);
			}
			subscription.setStartDate(request.getStartDate());
			subscription.setEndDate(DateHelper.addMonths(request.getStartDate(), request.getDuration()));
			
			subscription.setIsCurrent(true);
			subscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
			subscription.setProductPricing(request.getProductPricing());
			
			//Payment details
			subscription.setTxnId(request.getTxnId());
			subscription.setTotalAmount(request.getTotalAmount());
			subscription.setFinalAmount(request.getFinalAmount());
			subscription.setPointsDiscount(request.getPointsDiscount());
			subscription.setPointsRedemed(request.getPointsRedemed());
			subscription.setPaymentGateway(request.getPaymentGateway());
			subscription.setIsApprovedByPG(request.getIsApprovedByPG());
			
			for (PrepayRequestLineItem item : request.getPrepayRequestLineItems()) {
				SubscriptionLineItem lineItem = new SubscriptionLineItem();
				if(item.getType().equals(SubscriptionType.Daily)) {
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				} else if(item.getType().equals(SubscriptionType.Weekly)) {
					lineItem.setDay(item.getDay());
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				} else {
					lineItem.setCustomPattern(item.getCustomPattern());
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				}
			}
	
			subscription = userSubscriptionService.createUserSubscription(subscription, userService.getLoggedInUser(), false, true);
			if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
				activityLogService.createActivityLogForChangeStatus(userService.getLoggedInUser(), "changeStatus", customer.getStatus() + "_" + "ACTIVE", "prepaySubscriptionRequest", customer.getId());
			customer.setStatus(Status.ACTIVE);
			
			//Pending dues calculations
			double pendingDues = customer.getLastPendingDues();
			if(pendingDues < 0) {
				//Assuming prepaid customer will not have -ve pending dues
				customer.setLastPendingDues(pendingDues * -1);
			}
			customer = userService.updateUser(customer.getId(), customer, userService.getLoggedInUser(), true, true);
			subscription.setUser(customer);
			
			
			//subscription request delete after subscription  create 
			prepaySubscriptionRequestService.deletePrepaySubscriptionRequest(request.getId());
			
			asyncJobs.createFirstTimePrepayDeliverySchedules(subscription, subscription.getSubscriptionLineItems(), userService.getLoggedInUser());
			
			//Send welcome email + invoice
			Map<Product, Double> effectivePriceMap = new HashMap<Product, Double>();
			for(Entry<Long, Double> pricingEntry : subscription.getProductPricing().entrySet()) {
				effectivePriceMap.put(productService.getProduct(pricingEntry.getKey(), true), pricingEntry.getValue());
			}
			emailHelper.sendMailForPrepaySubscriptionApproval(subscription, effectivePriceMap);
		}
		String message = "Subscription Request Approved successfully";
		return "redirect:/admin/prepaySubscriptionRequest/changeList?message="+message;
	}
	
	// Shift to Prepaid Subscription request 
	
	@RequestMapping(value = "/end-user/shift-to-prepay/subscription-duration")
	public String shiftPrepaySubscriptionDuration(Authentication authentication, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, true, true);
		if(userSubscription == null) {
			String message = "userSubscription is not available";
			return "redirect:/index?message=" + message;
		}
		
		Set<SubscriptionLineItem> existingItems = new HashSet<SubscriptionLineItem>();
		existingItems = userSubscription.getSubscriptionLineItems();
		
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		requestInterceptor.addDataToSession("prepayDuration", 3);
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		requestInterceptor.addDataToSession("user", userInfo.getUser());
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		//Calculate effective prices
		List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		
		LinkedList<String> effectiveItems = new LinkedList();
		LinkedList<Double> prices = new LinkedList();
		LinkedList<Double> effectivePrices = new LinkedList();
		if(key == null) {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPricingMap().get(key));
			}
		}
		
		model.addAttribute("effectiveItems", effectiveItems);
		model.addAttribute("prices", prices);
		model.addAttribute("effectivePrices", effectivePrices);
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		return "/enduser-pages/shiftToPrepay/subscription-duration";
	}
	
	@RequestMapping(value = "/end-user/shift-to-prepay/terms-conditions")
	public String prepayTermsConditions(Model model) {
		
		return "/enduser-pages/shiftToPrepay/terms-conditions";
	}
	
	@RequestMapping(value = "/end-user/shift-to-prepay/average-monthly-bill")
	public String averageMonthlyBill(Model model, HttpServletResponse response) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		User loggedInUser = userService.getLoggedInUser();
		
		// Calculate unbilled Amount
		Double totalUnbilledAmount = 0.0;
		Calendar calendar = Calendar.getInstance();
		Date toDate = calendar.getTime();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.DATE, 1);
		Date fromDate = calendar.getTime();

		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(loggedInUser.getId(), fromDate, toDate, com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid, loggedInUser, false, true);
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if (deliverySchedule.getStatus().name().equalsIgnoreCase(DeliveryStatus.Delivered.name())) {
				for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
					totalUnbilledAmount = totalUnbilledAmount + (deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
				}
			}
		}
		// End Calculate unbilled Amount
		
		
		Double lastPendingdues = loggedInUser.getLastPendingDues();
		requestInterceptor.addDataToSession("totalSubscriptionAmount", totalSubscriptionAmount);
		requestInterceptor.addDataToSession("lastPendingdues", lastPendingdues);
		requestInterceptor.addDataToSession("totalUnbilledAmount", totalUnbilledAmount);
		requestInterceptor.addDataToSession("amountToPay", (totalSubscriptionAmount + lastPendingdues + totalUnbilledAmount));
		
		model.addAttribute("totalSubscriptionAmount", totalSubscriptionAmount);
		model.addAttribute("lastPendingdues", lastPendingdues);
		model.addAttribute("totalUnbilledAmount", totalUnbilledAmount);
		model.addAttribute("amountToPay", (totalSubscriptionAmount + lastPendingdues + totalUnbilledAmount));
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("durationInMonths", durationInMonths);
		return "/enduser-pages/shiftToPrepay/average-monthly-bill";
	}
	
	@RequestMapping(value="/end-user/shift-to-prepay/forwardPaymanetRequest")
	public String forwardPrepayPaymentRequest(Authentication authentication, Model model) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.SHIFT_TO_PREPAY_FRONTEND_PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.PREPAY_FRONTEND_PAYU_FURL);
		model.addAttribute("firstName", userInfo.getFirstName());
		model.addAttribute("lastName", userInfo.getLastName());
		model.addAttribute("fullName", userInfo.getFullName());
		model.addAttribute("phone", userInfo.getMobileNumber());
		model.addAttribute("email", userInfo.getEmail());
		Double amountToPay = (Double) requestInterceptor.getDataFromSession("amountToPay");
		
		String txnId = "WEB-PP" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
		String productInfo = txnId;
		model.addAttribute("productInfo", productInfo);
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		
		String amountString = new DecimalFormat("##.##").format(amountToPay);
		
		model.addAttribute("amount", amountString);
		String udf1 = "";
		model.addAttribute("udf1", udf1);
		String udf2 = "";
		model.addAttribute("udf2", udf2);
		String udf3 = "";
		model.addAttribute("udf3", udf3);
		String udf4 = "";
		model.addAttribute("udf4", udf4);
		String udf5 = ""; 
		model.addAttribute("udf5", udf5);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + userInfo.getFirstName() + "|" + userInfo.getEmail() + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		model.addAttribute("hash", encodedHash);
		model.addAttribute("txnId", txnId);
		return "/enduser-pages/paymentRequestForwarder";
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.SHIFT_TO_PREPAY_FRONTEND_PAYU_SURL)
	public String handlePayUPrepayPaymentSuccess(Authentication authentication, HttpServletRequest request,
												@RequestParam(required=false) String payuMoneyId, Model model,
												@RequestParam(required=false) String mode) {
		
		String status = "success";
		String firstname = "raj";
		double amount = 1212;
		String txnid = "hdk11";
		String hash = "ddf";
		String productinfo = "rhh";
		String phone = "8720802421";
		String email = "satendra";
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		/*if(!hash.equals(encodedHash)) {
			return "/enduser-pages/payu_success_wrong";
		}*/

		/*if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/enduser-pages/payu_success_wrong";
		}*/
		
		amount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");  
		
		PrepaySubscriptionRequest prepaySubscriptionRequest = new PrepaySubscriptionRequest();
		prepaySubscriptionRequest.setStartDate(startDate);
		prepaySubscriptionRequest.setDuration(durationInMonths);
		prepaySubscriptionRequest.setUser(user);
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, user, false, false);
		prepaySubscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
		
		//Effective price calculation & Lineitem population
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null)
			return "redirect:/";
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			subscribedProductCodes.add(subscribedProduct.getCode());
			
			PrepayRequestLineItem lineItem = new PrepayRequestLineItem();
			lineItem.setType(subscriptionLineItem.getType());
			lineItem.setDay(subscriptionLineItem.getDay());
			lineItem.setQuantity(subscriptionLineItem.getQuantity());
			lineItem.setProduct(subscriptionLineItem.getProduct());
			lineItem.setCustomPattern(subscriptionLineItem.getCustomPattern());
			prepaySubscriptionRequest.getPrepayRequestLineItems().add(lineItem);
		}
		List <Product> products = productService.getAllProducts(user, true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = new Double(amount).intValue();
		Integer mapKey = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			mapKey = integer;
		}
		Map<Long, Double> effectivePriceMap = new HashMap<Long, Double>();
		if(mapKey == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPricingMap().get(mapKey));
			}
		}
		prepaySubscriptionRequest.setProductPricing(effectivePriceMap);
		//End Effective price calculation
		
		prepaySubscriptionRequest.setTotalAmount(amount);
		prepaySubscriptionRequest.setFinalAmount(amount);
		prepaySubscriptionRequest.setTxnId(txnid);
		prepaySubscriptionRequest.setPaymentGateway("PayU");
		
		prepaySubscriptionRequest = prepaySubscriptionRequestService.createPrepaySubscriptionRequest(prepaySubscriptionRequest, user, false, true);
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		Double totalUnbilledAmount = (Double) requestInterceptor.getDataFromSession("totalUnbilledAmount");
		Double totalPayment = lastPendingdues + totalUnbilledAmount;
		
		if(totalPayment > 0 || totalPayment > 0.0) {
			Payment payment = new Payment();
			payment.setCustomer(user);
			payment.setAmount(totalPayment);
			payment.setTxnId(txnid);
			payment.setDate(new Date());
			payment.setPaymentMethod(PaymentMethod.PAYU);
			Payment addedPayment = paymentService.createPayment(payment, user, false, true);
			
			// Update loggedInUser with lastPendingdues
			User updatedLoggedInUser = userService.updateLastPendingDuesForUser(user.getId(), user, false, true);
		}
		
		//Prepare prepaySubscriptionRequest OrderLineItems
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		//End prepare prepaySubscriptionRequest OrderLineItems
		
		requestInterceptor.addDataToSession("prepaySubscriptionRequest", prepaySubscriptionRequest);
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		HttpSession session = request.getSession();
		session.setAttribute("showExtendPrepay", "show"); 
		session.setAttribute("showAmount", user.getRemainingPrepayBalance());
		
		//TODO: Clear session information
		requestInterceptor.addDataToSession("prepayItems", null);
		/*requestInterceptor.addDataToSession("totalSubscriptionAmount", null);
		requestInterceptor.addDataToSession("lastPendingdues", null);
		requestInterceptor.addDataToSession("totalUnbilledAmount", null);
		requestInterceptor.addDataToSession("amountToPay", null);
		*/
		model.addAttribute("prepaySubscriptionRequest", prepaySubscriptionRequest);
		model.addAttribute("lastPendingdues", lastPendingdues);
		model.addAttribute("totalUnbilledAmount", totalUnbilledAmount);
		return "/enduser-pages/shiftToPrepay/subscription-payment-success";
	}
	
	private String hashCal(String type, String str) throws NoSuchAlgorithmException {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(type);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append("0");
            }
            hexString.append(hex);
        }
        return hexString.toString();
	}
	
	// End shift to Prepaid Subscription request
	
	/**
	 * REST API Methods.
	 */
	
	/**
	 * End REST API Methods
	 */
}
