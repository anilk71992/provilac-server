package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.DeliveryScheduleDTO;
import com.vishwakarma.provilac.dto.DeliverySheetDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySheet;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.mvc.validator.DeliveryScheduleValidator;
import com.vishwakarma.provilac.mvc.validator.DeliverySheetValidator;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.DeliverySheetService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class DeliverySheetController {

	@Resource
	private DeliverySheetValidator deliverySheetValidator;

	@Resource
	private DeliverySheetService deliverySheetService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private UserRouteService userRouteService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private DeliveryScheduleValidator deliveryScheduleValidator;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "deliverySheet")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(deliverySheetValidator);
	}

	private Model populateModelForAdd(Model model, DeliverySheet deliverySheet) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("deliverySheet", deliverySheet);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/deliverySheet/list")
	public String getDeliverySheetList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<DeliverySheet> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = deliverySheetService.searchDeliverySheet(pageNumber, searchTerm, user,true,false);
		} else {
			page = deliverySheetService.getDeliverySheets(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("deliverySheets", page.getContent());

		return "/admin-pages/deliverySheet/list";
	}

	@RequestMapping(value = "/admin/deliverySheet/show/{cipher}")
	public String showDeliverySheet(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		DeliverySheet deliverySheet = deliverySheetService.getDeliverySheet(id, true, loggedIUser,true,false);
		model.addAttribute("deliverySheet", deliverySheet);
		return "/admin-pages/deliverySheet/show";
	}

	@RequestMapping(value = "/admin/deliverySheet/update/{cipher}")
	public String updateSummarySheet(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		DeliverySheet deliverySheet = deliverySheetService.getDeliverySheet(id, false, null, false, true);
		
		List<User> users = userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false);
		model = populateModelForAdd(model, deliverySheet);
		model.addAttribute("id", deliverySheet.getId());
		model.addAttribute("customers", users);
		model.addAttribute("version", deliverySheet.getVersion());
		return "/admin-pages/deliverySheet/update";
	}
	
	@RequestMapping(value = "/admin/deliverySheet/update", method = RequestMethod.POST)
	public String deliverySheetUpdate(DeliverySheet deliverySheet, 
										BindingResult formBinding, 
										Model model,
										@RequestParam(required=false,value="code")String[] deiliveryScheduleCodes,
										@RequestParam(required=false,value="status")String[] status,
										@RequestParam(required=false,value="reason")String[] reasons){
		
		
		for(int i=0; i<=deiliveryScheduleCodes.length-1; i++){
			 String deliverySheetCode =  deiliveryScheduleCodes[i];
			 DeliverySchedule deliverySchedule =  deliveryScheduleService.getDeliveryScheduleByCode(deliverySheetCode, false, null, false, false);
			 if(status.length!=0){
				 String status1 = status[i];
				 deliverySchedule.setStatus(DeliveryStatus.valueOf(status1));
			 }
			 
			 if(reasons.length!=0){
				 String reason = reasons[i];
				 deliverySchedule.setReason(reason);
			 }
			  		  
			  deliverySchedule.setUpdateOperation(true);
			  deliveryScheduleValidator.validate(deliverySchedule, formBinding);
			
			  if (formBinding.hasErrors()) {
					model = populateModelForAdd(model, deliverySheet);
					return "admin-pages/deliverySheet/update";
				}
			  deliveryScheduleService.updateDeliveryScheduleStatusAndReason(deliverySchedule.getId(), deliverySchedule, null, false, false);
		}
		String message = "DeliverySheet updated successfully";
		return "redirect:/admin/deliverySheet/list?message=" + message;
	}

	
	@RequestMapping(value = "/admin/deliverySheet/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			deliverySheetService.deleteDeliverySheet(id);
			message = "DeliverySheet deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete DeliverySheet";
		}
		return "redirect:/admin/deliverySheet/list?message=" + message;
	}
	
	@RequestMapping("/admin/deliverySheet/export/{deliverySheetCode}")
	public ResponseEntity<byte []> exportToExcel(HttpServletResponse httpServletResponse, @PathVariable String deliverySheetCode) throws IOException {
		org.springframework.core.io.Resource  tp = new ClassPathResource("/excelSheet/Delivery_Sheet.xls");
		File file = tp.getFile();
		DeliverySheet deliverySheet = deliverySheetService.getDeliverySheetByCode(deliverySheetCode, false, userService.getLoggedInUser(), false, true);
		
		ByteArrayOutputStream byteArrayOutputStream = generateDeliverySheetExcel(file, deliverySheet);
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/xls");
		httpServletResponse.setHeader("Content-Disposition", "inline;filename=Delivery_Sheet.xls");
		httpServletResponse.setHeader("Content-Length", String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(), httpServletResponse.getOutputStream());
			activityLogService.createActivityLog(null, "export", "DeliverySheet",deliverySheet.getId());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return null;
	}
	
	private ByteArrayOutputStream generateDeliverySheetExcel(File file, DeliverySheet deliverySheet) throws IOException {

		int rowIndex = 1;
		Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
		
		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
		HSSFRow hssfRow = null;
		HSSFCell hssfCell = null;
		
		List<String> headers = new ArrayList<String>();
		List<String> data = null;
		List<List> records = new ArrayList<List>();
		Map<String, Integer> productQuantity = new HashMap<String, Integer>();
		
		hssfWorkbook.setSheetName(0, "Date "+DateHelper.getFormattedDate(deliverySheet.getUserRoute().getCreatedDate())+ ", Delivery Boy " +deliverySheet.getUserRoute().getDeliveryBoy().getFullName());
		headers.add("No.");
		headers.add("Customer Name");
		headers.add("Address");
		headers.add("Phone Number");
		headers.add("Route Name");
		headers.add("Priority");
		headers.add("Prmt Note");
		headers.add("Temp Note");
		headers.add("Product Name");
		headers.add("Qty");
		
		//Headers added
		records.add(headers); 
		for(DeliverySchedule deliverySchedule:deliverySheet.getDeliverySchedules()){
			try{
				//TODO: VVVIMP Sync with Provilac with regards to this
				if(deliverySchedule.getCustomer().getStatus().name().equalsIgnoreCase(Status.INACTIVE.name())){
					continue;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			deliveryLineItems = deliverySchedule.getDeliveryLineItems();
			for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				data = new ArrayList<String>();
				data.add(rowIndex+"");
				data.add(deliverySchedule.getCustomer().getFullName());
				data.add(deliverySchedule.getCustomer().getBuildingAddress());
				data.add(deliverySchedule.getCustomer().getMobileNumber());
				data.add(deliverySheet.getUserRoute().getRoute().getName());
				data.add(String.valueOf(deliverySchedule.getPriority()));
				data.add(deliverySchedule.getPermanantNote());
				data.add(deliverySchedule.getTempararyNote());
				data.add(deliveryLineItem.getProduct().getName());
				data.add(deliveryLineItem.getQuantity()+"");
				records.add(data);
				rowIndex++;
				
				if(productQuantity.containsKey(deliveryLineItem.getProduct().getName())){
					if(productQuantity.isEmpty()){
						productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
					}else{
						productQuantity.put(deliveryLineItem.getProduct().getName(), (productQuantity.get(deliveryLineItem.getProduct().getName()) != null?productQuantity.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
					}
				}else{
					productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
				}
			}
		}
		
		rowIndex = 0;
		HSSFCellStyle hssfCellStyleForContent = hssfWorkbook.createCellStyle();
		
		hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);

		for (int j = 0; j < records.size(); j++) {
			hssfRow = hssfSheet.createRow(rowIndex);
			List<String> l2= records.get(j);
			for(int k=0; k<l2.size(); k++){
				hssfCell = hssfRow.createCell(k);
				hssfCell.setCellStyle(hssfCellStyleForContent);
				hssfCell.setCellValue(l2.get(k));
			}
			rowIndex++;
		}
		
		hssfRow = hssfSheet.createRow(rowIndex+1);
		hssfCell = hssfRow.createCell(1);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue("Total Products");
		hssfCell = hssfRow.createCell(2);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		String totalProductQty ="";
		for(Entry<String, Integer> entry:productQuantity.entrySet()){
			totalProductQty += entry.getKey()+"="+entry.getValue()+",";
		}
		hssfCell.setCellValue(totalProductQty.substring(0, totalProductQty.length()-1));
		
		for(int colNum = 0; colNum<hssfSheet.getRow(0).getLastCellNum();colNum++){
			hssfSheet.autoSizeColumn(colNum);
		}

		hssfSheet.setFitToPage(true);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}
	
	
	@RequestMapping(value="/admin/deliverySheet/exportForToday")
	public ResponseEntity<byte []> exportTodaysDeliverySheetToExcel(HttpServletResponse httpServletResponse) throws IOException {
		org.springframework.core.io.Resource  tp = new ClassPathResource("/excelSheet/Delivery_Sheet.xls");
		File file = tp.getFile();
		
		User loggedInUser = userService.getLoggedInUser();
		List<DeliverySheet> deliverySheets = deliverySheetService.getDeliverySheetsByDate(new Date(),loggedInUser, false, true);
		
		ByteArrayOutputStream byteArrayOutputStream = generateDeliverySheetsForAllRoutes(file, deliverySheets);
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/xls");
		httpServletResponse.setHeader("Content-Disposition", "inline;filename=Delivery_Sheet.xls");
		httpServletResponse.setHeader("Content-Length", String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(), httpServletResponse.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return null;
	}
	
	private ByteArrayOutputStream generateDeliverySheetsForAllRoutes(File file, List<DeliverySheet> deliverySheets) throws IOException {

		int rowIndex = 1,count=0;
		Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
		Map<String, Integer> productQuantity = new HashMap<String, Integer>();
		
		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);
		
		for(DeliverySheet deliverySheet:deliverySheets){
			productQuantity = new HashMap<String, Integer>();
			HSSFSheet hssfSheet;
			if(count != 0){
				hssfSheet = hssfWorkbook.createSheet(deliverySheet.getUserRoute().getRoute().getName()+", Date "+DateHelper.getFormattedDate(new Date()));
			}else{
				hssfSheet = hssfWorkbook.getSheetAt(count);
				hssfWorkbook.setSheetName(0, deliverySheet.getUserRoute().getRoute().getName()+", Date "+DateHelper.getFormattedDate(new Date()));
			}
			HSSFRow hssfRow = null;
			HSSFCell hssfCell = null;
			
			List<String> headers = new ArrayList<String>();
			List<String> data = null;
			List<List> records = new ArrayList<List>();
			headers.add("No.");
			headers.add("Customer Name");
			headers.add("Address");
			headers.add("Phone Number");
			headers.add("Route Name");
			headers.add("Priority");
			headers.add("Prmt Note");
			headers.add("Temp Note");
			headers.add("Product Name");
			headers.add("Qty");
			
			
			//Headers added
			records.add(headers); 
			for(DeliverySchedule deliverySchedule:deliverySheet.getDeliverySchedules()){
				try{
					//TODO: VVVIMP Sync with Provilac with regards to this 
					if(deliverySchedule.getCustomer().getStatus().name().equalsIgnoreCase(Status.INACTIVE.name())){
						//deliverySchedule.setStatus(DeliveryStatus.NotDelivered);
						//deliveryScheduleService.updateDeliveryScheduleStatusAndReason(deliverySchedule.getId(), deliverySchedule, null, false, false);
						continue;
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				deliveryLineItems = deliverySchedule.getDeliveryLineItems();
				for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
					data = new ArrayList<String>();
					data.add(rowIndex+"");
					data.add(deliverySchedule.getCustomer().getFullName());
					data.add(deliverySchedule.getCustomer().getBuildingAddress());
					data.add(deliverySchedule.getCustomer().getMobileNumber());
					data.add(deliverySheet.getUserRoute().getRoute().getName());
					data.add(String.valueOf(deliverySchedule.getPriority()));
					data.add(deliverySchedule.getPermanantNote());
					data.add(deliverySchedule.getTempararyNote());
					data.add(deliveryLineItem.getProduct().getName());
					data.add(deliveryLineItem.getQuantity()+"");
					records.add(data);
					rowIndex++;
					
					if(productQuantity.containsKey(deliveryLineItem.getProduct().getName())){
						if(productQuantity.isEmpty()){
							productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
						}else{
							productQuantity.put(deliveryLineItem.getProduct().getName(), (productQuantity.get(deliveryLineItem.getProduct().getName()) != null?productQuantity.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
						}
					}else{
						productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
					}
				}
			}
			
			rowIndex = 0;
			HSSFCellStyle hssfCellStyleForContent = hssfWorkbook.createCellStyle();
			
			hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
			hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	
			for (int j = 0; j < records.size(); j++) {
				hssfRow = hssfSheet.createRow(rowIndex);
				List<String> l2= records.get(j);
				for(int k=0; k<l2.size(); k++){
					hssfCell = hssfRow.createCell(k);
					hssfCell.setCellStyle(hssfCellStyleForContent);
					hssfCell.setCellValue(l2.get(k));
				}
				rowIndex++;
			}
			
			hssfRow = hssfSheet.createRow(rowIndex+1);
			hssfCell = hssfRow.createCell(1);
			hssfCell.setCellStyle(hssfCellStyleForContent);
			hssfCell.setCellValue("Total Products");
			hssfCell = hssfRow.createCell(2);
			hssfCell.setCellStyle(hssfCellStyleForContent);
			String totalProductQty ="";
			for(Entry<String, Integer> entry:productQuantity.entrySet()){
				totalProductQty += entry.getKey()+"="+entry.getValue()+",";
			}
			if(StringUtils.isNotBlank(totalProductQty)) {
				hssfCell.setCellValue(totalProductQty.substring(0, totalProductQty.length()-1));
			} else {
				hssfCell.setCellValue("0");
			}
			
			for(int colNum = 0; colNum<hssfSheet.getRow(0).getLastCellNum();colNum++){
				hssfSheet.autoSizeColumn(colNum);
			}
	
			hssfSheet.setFitToPage(true);
			
			rowIndex = 1;
			count++;
		}
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}
	
	/*
	 *REST API Methods 
	 */
	 @RequestMapping(value="/restapi/deliverySheet/deliverySchedules")
	 @ResponseBody
	 public  ApiResponse getDeliverySchedulesForDeliverySheet(@RequestParam(required=true)String deliverySheetCode){
		 
		 ApiResponse apiResponse = new ApiResponse(true);
		 try{
			 DeliverySheet deliverySheet = deliverySheetService.getDeliverySheetByCode(deliverySheetCode, false, null, false, true);
			 List<DeliveryScheduleDTO> deliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();
			 for (DeliverySchedule deliverySchedule : deliverySheet.getDeliverySchedules()) {
				deliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
			}
			 apiResponse.addData("deliverySchedules", deliveryScheduleDTOs);
		 }catch(ProvilacException e){
			 e.printStackTrace();
			 apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());
		 }
		 
		 return apiResponse;
	 }
	 
	/*
	 * End REST API Methods
	 */
}
