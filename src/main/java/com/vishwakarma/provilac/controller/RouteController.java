package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.RouteDTO;
import com.vishwakarma.provilac.dto.RouteRecordDTO;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.mvc.validator.RouteValidator;
import com.vishwakarma.provilac.repository.RouteRecordRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class RouteController {

	@Resource
	private RouteValidator routeValidator;

	@Resource
	private RouteService routeService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource 
	private RouteRecordRepository routeRecordRepository;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource 
	private UserRouteService userRouteService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@InitBinder(value = "route")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(routeValidator);
	}

	private Model populateModelForAdd(Model model, Route route) {
		model.addAttribute("route", route);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/route/add")
	public String addRoute(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Route());
		return "admin-pages/route/add";
	}

	@RequestMapping(value = "/admin/route/add", method = RequestMethod.POST)
	public String routeAdd(Route route, 
									BindingResult formBinding, 
									Model model,
									@RequestParam(required=false, value="customer")String customerCodes[],
									@RequestParam(required=false,value="priority")int priorities[]){
		
		User loggedInUser = userService.getLoggedInUser();
		if(null!=customerCodes){
			for(int i = 0;i <customerCodes.length;i++){
				String customerCode = customerCodes[i];
				int priority = priorities[i];
				RouteRecord routeRecord = new RouteRecord();
				User customer = userService.getUserByCode(customerCode, false, loggedInUser, false, false);
				routeRecord.setCustomer(customer);
				routeRecord.setRoute(route);
				routeRecord.setPriority(priority);
				route.getRouteRecords().add(routeRecord);
				//update users isrouteAssigned  = true when route is added
				customer.setIsRouteAssigned(true);
				userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
				//plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi, Your has been updated to new Route "+route.getName()+" with Priority"+priority);
			}
		}
		
		routeValidator.validate(route, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, route);
			return "admin-pages/route/add";
		}
		routeService.createRoute(route, loggedInUser, true,false);
		routeRecordService.createRouteRecords(route.getRouteRecords(), loggedInUser, true, false);
		String message = "Route added successfully";
		return "redirect:/admin/route/list?message=" + message;
	}

	@RequestMapping(value = "/admin/route/list")
	public String getRouteList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Route> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = routeService.searchRoute(pageNumber, searchTerm, user,true,false);
		} else {
			page = routeService.getRoutes(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("routes", page.getContent());

		return "/admin-pages/route/list";
	}

	@RequestMapping(value = "/admin/route/show/{cipher}")
	public String showRoute(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Route route = routeService.getRoute(id, true, loggedIUser,true,false);
		model.addAttribute("route", route);
		return "/admin-pages/route/show";
	}

	@RequestMapping(value = "/admin/route/update/{cipher}")
	public String updateRoute(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Route route = routeService.getRoute(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, route);
		model.addAttribute("id", route.getId());
		model.addAttribute("version", route.getVersion());
		return "/admin-pages/route/update";
	}

	@RequestMapping(value = "/admin/route/update", method = RequestMethod.POST)
	public String routeUpdate(Route route, 
										BindingResult formBinding, 
										Model model) {

		route.setUpdateOperation(true);
		route.setId(Long.parseLong(model.asMap().get("id") + ""));
		route.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		routeValidator.validate(route, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, route);
			return "admin-pages/route/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		routeService.updateRoute(route.getId(), route, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Route updated successfully";
		return "redirect:/admin/route/list?message=" + message;
	}

	@RequestMapping(value = "/admin/route/delete/{cipher}")
	public String deleteRouteRecord(@PathVariable String cipher) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			routeService.deleteRoute(id);
			message = "Route deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Route";
		}
		return "redirect:/admin/route/list?message=" + message;
	}
	
	/**
	 * Import Routes
	 */
	
	@RequestMapping(value="/admin/route/import", method=RequestMethod.GET)
	public String importRoutes(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/route/import";
	}
	
	@RequestMapping(value="/admin/route/import", method=RequestMethod.POST)
	public String importRoutes(Model model, @RequestParam(required=true) MultipartFile file) {
		
		String message = "Successfully imported routes";
		if(null == file || file.isEmpty() || !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)) {
			
			message = "Please select valid excel 2007 or later file";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/route/import";
		}
		try {
			importRoutes(file);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error uploading routes, please try with valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/route/import";
		}
		return "redirect:/admin/route/list?message=" + message;
	}
	

	private void importRoutes(MultipartFile excelFile) throws InvalidFormatException, IOException{
		Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		int rowCount = 0;
		DataFormatter formatter = new DataFormatter();
		Route route = null;
		User loggedInUser = userService.getLoggedInUser();
		for (Row row : sheet) {

			if(rowCount == 0){
				rowCount++;
				continue;
			}
			String name = null;
			if(null != row.getCell(0)){
				name = row.getCell(0).getStringCellValue(); 
			}else{
				rowCount++;
				continue;
			}

			Cell userIdCell = row.getCell(1);
			if(null == userIdCell){
				rowCount++;
				continue;
			}
			
			String userNameOrMobileNumber= formatter.formatCellValue(userIdCell);
			
			if(userNameOrMobileNumber.isEmpty()){
				rowCount++;
				continue;
			}

			Cell priorityCell = row.getCell(2);
			String priority= formatter.formatCellValue(priorityCell);
			
			route = null;
			route = routeService.getRouteByName(name.trim(), false, loggedInUser, false, false);
			User customer = userService.getUserByUserNameOrMobileNumber(userNameOrMobileNumber, false, null, false, false);
			
			if(null == customer){
				rowCount++;
				continue;
			}
			if(null!=customer &&(customer.hasRole(Role.ROLE_CUSTOMER))){
				if((customer.getStatus().equals(Status.INACTIVE))||(customer.getStatus().equals(Status.HOLD))){
					rowCount++;
					continue;
				}
			}
			List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);
			RouteRecord routeRecord = new RouteRecord();
			
			if(null == route){
				route = new Route();
				route.setName(name);
				routeRecord.setCustomer(customer);
				routeRecord.setPriority(Integer.parseInt(priority));
				routeRecord.setRoute(route);
				//route.getRouteRecords().add(routeRecord);
				routeService.createRoute(route, null, false, true);
				routeRecord = routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			}else{
				route.setName(name);
				routeRecord.setCustomer(customer);
				routeRecord.setPriority(Integer.parseInt(priority));
				routeRecord.setRoute(route);
				//route.getRouteRecords().add(routeRecord);
				//routeService.updateRoute(route.getId(), route, loggedInUser, false, false);
				routeRecord = routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			}
			if(null != existingRouteRecords && !existingRouteRecords.isEmpty()){
				Route lastRoute = null;
				for(RouteRecord existingRouteRecord:existingRouteRecords){
					lastRoute = routeRecord.getRoute();
					routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
				}
				
				if(null != lastRoute) {
					deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
					DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
					deletedRouteRecord.setRoute(lastRoute);
					deletedRouteRecord.setCustomer(customer);
					deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
				}
			}
			asyncJobs.updateDeliveryScheduleRoute(routeRecord, loggedInUser);
			customer.setIsRouteAssigned(true);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);

			rowCount++;
		}
	}
	
	/**
	 * End Import Routes
	 */
	
	/*
	 * Ajax Method
	 * 
	 */
	 @RequestMapping(value = "/restapi/route/fetchRouteByUser")
	    @ResponseBody
	    public ApiResponse getRouteByUser(@RequestParam(required = true) String customerCode){
	       
	        ApiResponse apiResponse = new ApiResponse(true);
	       
	        try{
	            RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customerCode, false, userService.getLoggedInUser(), false, true);
	            Route route = null;
	            if(null!=routeRecord){
	            	 route = routeService.getRoute(routeRecord.getRoute().getId(), false, userService.getLoggedInUser(),  false, true);
	            	 apiResponse.addData("route", new RouteDTO(route, new ArrayList<RouteRecord>(route.getRouteRecords())));
	            }
	           
	        }catch(ProvilacException e){
	            e.printStackTrace();
	            apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
	        }
	       
	        return apiResponse;
	    }
	 
	 @RequestMapping(value = "/route/records")
	    @ResponseBody
	    public ApiResponse getRouteRecords(@RequestParam(required = true) Long routeId){
	       
	        ApiResponse apiResponse = new ApiResponse(true);
	       
	        try{
	           
	            Route route = routeService.getRoute(routeId, false, userService.getLoggedInUser(),  false, true);
	           List<RouteRecordDTO>records = new ArrayList<RouteRecordDTO>();
	           for (RouteRecord record : route.getRouteRecords()) {
				records.add(new RouteRecordDTO(record));
			}
	            apiResponse.addData("records", records);
	           
	        }catch(ProvilacException e){
	            e.printStackTrace();
	            apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
	        }
	       
	        return apiResponse;
	    }
	 @RequestMapping(value = "/route/getRecords")
	    @ResponseBody
	    public ApiResponse getRouteRecordsAndSelectedCustomer(@RequestParam(required = true) Long routeId,
	    													  @RequestParam(required = true) Long id){
	       
	        ApiResponse apiResponse = new ApiResponse(true);
	       
	        try{
	        	Route route = routeService.getRoute(routeId, false, userService.getLoggedInUser(),  false, true);
	        	List<RouteRecordDTO>records = new ArrayList<RouteRecordDTO>();
	        	for (RouteRecord record : route.getRouteRecords()) {
	        		records.add(new RouteRecordDTO(record));
	        	}
	        	DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliverySchedule(id, false, null, false, true);
				List<DeliverySchedule> deliverySchedules1 = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule.getPriority()-1, null, false, false);
				if(!deliverySchedules1.isEmpty()){
					apiResponse.addData("customer",new UserDTO(deliverySchedules1.get(0).getCustomer()));
				}
	        	apiResponse.addData("records", records);

	        }catch(ProvilacException e){
	        	e.printStackTrace();
	        	apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
	        }
	       
	        return apiResponse;
	    }
	 
	@RequestMapping(value="/admin/route/export/{routeCode}")
	public ResponseEntity<byte []> exportTodaysDeliverySheetToExcel(HttpServletResponse httpServletResponse,
									@PathVariable String routeCode) throws IOException {
		
		User loggedInUser = userService.getLoggedInUser();
		Route route = routeService.getRouteByCode(routeCode, true, loggedInUser, false, true);
		org.springframework.core.io.Resource  tp = new ClassPathResource("/excelSheet/Route_Users.xls");
		File file = tp.getFile();
		
		ByteArrayOutputStream byteArrayOutputStream = generateRouteXLS(file, route);
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		httpServletResponse.setHeader("Content-Disposition", "inline;filename="+route.getName()+".xls");
		httpServletResponse.setHeader("Content-Length", String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(), httpServletResponse.getOutputStream());
			activityLogService.createActivityLog(null, "export", "Route",route.getId());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return null;
	}
	
	private ByteArrayOutputStream generateRouteXLS(File file, Route route) throws IOException {

		int rowIndex = 1;
		
		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
		HSSFRow hssfRow = null;
		HSSFCell hssfCell = null;
		
		List<String> headers = new ArrayList<String>();
		List<String> data = null;
		List<List> records = new ArrayList<List>();
		hssfWorkbook.setSheetName(0, "Route Name - "+route.getName());
		headers.add("Name");
		headers.add("Customer(UserName/Mobile Number)");
		headers.add("Priority");
		headers.add("Customer Name");
		headers.add("Address");
		headers.add("Status");
		
		//Headers added
		records.add(headers); 
		for(RouteRecord routeRecord:route.getRouteRecords()){
			data = new ArrayList<String>();
			data.add(route.getName());
			data.add(routeRecord.getCustomer().getMobileNumber());
			data.add(routeRecord.getPriority()+"");
			data.add(routeRecord.getCustomer().getFullName());
			data.add(routeRecord.getCustomer().getBuildingAddress());
			data.add(routeRecord.getCustomer().getStatus().name());
			records.add(data);
			rowIndex++;
		}
		
		rowIndex = 0;
		HSSFCellStyle hssfCellStyleForContent = hssfWorkbook.createCellStyle();
		
		hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);

		for (int j = 0; j < records.size(); j++) {
			hssfRow = hssfSheet.createRow(rowIndex);
			List<String> l2= records.get(j);
			for(int k=0; k<l2.size(); k++){
				hssfCell = hssfRow.createCell(k);
				hssfCell.setCellStyle(hssfCellStyleForContent);
				hssfCell.setCellValue(l2.get(k));
			}
			rowIndex++;
		}
		
		for(int colNum = 0; colNum<hssfSheet.getRow(0).getLastCellNum();colNum++){
			hssfSheet.autoSizeColumn(colNum);
		}

		hssfSheet.setFitToPage(true);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}
		

	/**
	 * REST API Methods
	 */

	@RequestMapping(value="/restapi/route/details")
	@ResponseBody
	public ApiResponse getRouteDetails(@RequestParam(required =true)Long routeId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			Route route = routeService.getRoute(routeId, false, null, false, true);
			
			apiResponse.addData("route", new RouteDTO(route, new ArrayList<RouteRecord>(route.getRouteRecords())));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*
	 * USED_FROM_DEVICE
	 */
	@RequestMapping(value="/restapi/route/getByDeliveryBoy")
	@ResponseBody
	public ApiResponse getRoutes(@RequestParam(required =true)Long deliveryBoyId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User deliveryBoy = userService.getUser(deliveryBoyId, true, userService.getLoggedInUser(), false, false);
			List<UserRoute>userRoutes = userRouteService.getUserRoutesForDeliveryBoy(deliveryBoy.getCode(), null, false, false);
			List<RouteDTO> routeDTOs = new ArrayList<RouteDTO>();
			for (UserRoute userRoute : userRoutes) {
				routeDTOs.add(new RouteDTO(userRoute.getRoute()));
			}
			
			apiResponse.addData("routes",routeDTOs);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/*
	 * USED_FROM_DEVICE
	 */
	@RequestMapping(value="/restapi/route/getRoutesByCollectionBoy")
	@ResponseBody
	public ApiResponse getRoutesByCollectionBoy(@RequestParam(required =true)Long collectionBoyId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User collectionBoy = userService.getUser(collectionBoyId, true, userService.getLoggedInUser(), false, false);
			List<UserRoute>userRoutes = userRouteService.getUserRoutesForColeectionBoy(collectionBoy.getId(), null, false, false);
			List<RouteDTO> routeDTOs = new ArrayList<RouteDTO>();
			for (UserRoute userRoute : userRoutes) {
				routeDTOs.add(new RouteDTO(userRoute.getRoute()));
			}
			
			apiResponse.addData("routes",routeDTOs);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	
	@RequestMapping(value="/restapi/route/all")
	@ResponseBody
	public ApiResponse getAllRoutes(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			List<Route> routes = routeService.getAllRoutes(null, false, false);
			List<RouteDTO> routeDTOs = new ArrayList<RouteDTO>();
			for (Route route : routes) {
				routeDTOs.add(new RouteDTO(route));
			}
			
			apiResponse.addData("routes",routeDTOs);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * End REST API Methods
	 */
}
