package com.vishwakarma.provilac.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.CategoryValidator;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CategoryService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class CategoryController {

	@Resource
	private CategoryValidator categoryValidator;

	@Resource
	private CategoryService categoryService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private Environment environment;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	
	@InitBinder(value = "category")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(categoryValidator);
	}

	private Model populateModelForAdd(Model model, Category category) {
		model.addAttribute("category", category);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/category/add")
	public String addCategory(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Category());
		return "admin-pages/category/add";
	}

	@RequestMapping(value = "/admin/category/add", method = RequestMethod.POST)
	public String categoryAdd(Category category, 
									BindingResult formBinding, 
									Model model, @RequestParam(required = true, value="categoryPic")MultipartFile categoryPic){
		
		User loggedInUser = userService.getLoggedInUser();
		categoryValidator.validate(category, formBinding);
		
		if (null != categoryPic && !categoryPic.isEmpty()) {
			String contentType = categoryPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("category", "categoryPicUrl", "Please select valid  category pic"));
			} else if (categoryPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("category", "categoryPicUrl", "Please select category image Picture less than 5MB"));
			}
			try {
				category.setCategoryData(categoryPic.getBytes());
				category.setCategoryFileName(categoryPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			formBinding.addError(new FieldError("category", "categoryPicUrl", "Please select valid category Pic"));
		}
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, category);
			return "admin-pages/category/add";
		}
		categoryService.createCategory(category, loggedInUser, true,false);
		String message = "Category added successfully";
		return "redirect:/admin/category/list?message=" + message;
	}

	@RequestMapping(value = "/admin/category/list")
	public String getCategoryList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Category> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = categoryService.searchCategory(pageNumber, searchTerm, user,true,false);
		} else {
			page = categoryService.getCategorys(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("categorys", page.getContent());

		return "/admin-pages/category/list";
	}
	
	@RequestMapping(value = "/allCategories") 
	public String openCategory(Model model, 
			@RequestParam(defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false) String message, 
			@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<Category> page = null;
		/*User user = userService.getLoggedInUser();*/
		if (StringUtils.isNotBlank(searchTerm)) {
			page = categoryService.searchCategory(pageNumber, searchTerm, null, false, false);
		} else {
			page = categoryService.getCategorys(pageNumber, null, false, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("categorys", page.getContent());
		return "/enduser-pages/categories";
	}

	@RequestMapping(value = "/admin/category/show/{cipher}")
	public String showCategory(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Category category = categoryService.getCategory(id, true, loggedIUser,true,false);
		model.addAttribute("category", category);
		return "/admin-pages/category/show";
	}

	@RequestMapping(value = "/admin/category/update/{cipher}")
	public String updateCategory(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Category category = categoryService.getCategory(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, category);
		model.addAttribute("id", category.getId());
		model.addAttribute("version", category.getVersion());
		return "/admin-pages/category/update";
	}

	@RequestMapping(value = "/admin/category/update", method = RequestMethod.POST)
	public String categoryUpdate(Category category, 
										BindingResult formBinding, 
										Model model, @RequestParam(required = true, value="categoryPic")MultipartFile categoryPic) {

		category.setUpdateOperation(true);
		category.setId(Long.parseLong(model.asMap().get("id") + ""));
		category.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		categoryValidator.validate(category, formBinding);

		if (null != categoryPic && !categoryPic.isEmpty()) {
			String contentType = categoryPic.getContentType();
			if (!contentType.contains("image/")) {
				formBinding.addError(new FieldError("category", "categoryPicUrl", "Please select valid  category pic"));
			} else if (categoryPic.getSize() > 5242880) { // Maximum size = 5MB
				formBinding.addError(new FieldError("category", "categoryPicUrl", "Please select category image Picture less than 5MB"));
			}
			try {
				category.setCategoryData(categoryPic.getBytes());
				category.setCategoryFileName(categoryPic.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, category);
			return "admin-pages/category/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		categoryService.updateCategory(category.getId(), category, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Category updated successfully";
		return "redirect:/admin/category/list?message=" + message;
	}

	@RequestMapping(value = "/admin/category/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			categoryService.deleteCategory(id);
			message = "Category deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Category";
		}
		return "redirect:/admin/category/list?message=" + message;
	}
	
	@RequestMapping(value="/admin/category/import", method=RequestMethod.GET)
	public String importCategories(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/category/import";
	}
	
	@RequestMapping(value="/admin/category/import", method=RequestMethod.POST)
	public String importCategories(Model model, @RequestParam(required=true) MultipartFile file) {
		
		String message = "Successfully imported categories";
		if(null == file || file.isEmpty() || !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)) {
			
			message = "Please select valid excel 2007 or later file";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/route/import";
		}
		try {
			importCategory(file);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error uploading categories, please try with valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/category/import";
		}
		return "redirect:/admin/category/list?message=" + message;
	}
	
	@RequestMapping(value = "/end-user/onetimeorder/categories") 
	public String onetimeorderCategory(Model model) {
		
		List<Category> categoryList = categoryService.getAllCategorys(null, false, true);
		model.addAttribute("categoryList", categoryList);
		return "/enduser-pages/oneTimeOrder/category";
	}

	private void importCategory(MultipartFile excelFile) throws InvalidFormatException, IOException{
		Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		int rowCount = 0;
		Category category = null;
		for (Row row : sheet) {

			if(rowCount == 0){
				rowCount++;
				continue;
			}
			String categoryName = null;
			Cell categoryCell = row.getCell(0);
			categoryName = categoryCell.getStringCellValue();
			category = categoryService.getCategoryByName(categoryName, false, userService.getLoggedInUser(), false, false);
			
			if(null == category){
				category = new Category();
			}
			category.setName(categoryName);
			BindingResult bindingResult = new DataBinder(category).getBindingResult();
				categoryValidator.validate(category, bindingResult);
				if(bindingResult.hasErrors()) {
					continue;
				}

			categoryService.createCategory(category, userService.getLoggedInUser(), false, false);
			rowCount++;
		}
	}
	
	/*
	 * Ajax Method
	 * 
	 */
	

	/**
	 * REST API Methods
	 */
	@RequestMapping(value="/restapi/getCategoryPicture", method=RequestMethod.GET)
	@ResponseBody
	public void getCategoryPic(long id, HttpServletResponse httpServletResponse){
		
		if(Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.CATEGORY_PICS_FOLDER, id + "");
				//TODO: Remove this, find some better mechanism for this
				if(redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				//TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.CATEGORY_PICS_FOLDER);
			File file = new File(baseDir, id + "");
			
			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	

	/**
	 * End REST API Methods
	 */
}
