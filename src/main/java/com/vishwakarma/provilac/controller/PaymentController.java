package com.vishwakarma.provilac.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.paytm.merchant.CheckSumServiceHelper;
import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.PaymentDTO;
import com.vishwakarma.provilac.dto.PaymentDetailsDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Invoice.CollectionStatus;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.PaymentValidator;
import com.vishwakarma.provilac.property.editor.InvoicePropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.CategoryRepository;
import com.vishwakarma.provilac.repository.InvoiceRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CategoryService;
import com.vishwakarma.provilac.service.CollectionService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DepositService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class PaymentController {

	@Resource
	private PaymentValidator paymentValidator;

	@Resource
	private PaymentService paymentService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private CategoryService categoryService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private CategoryRepository categoryRepository;
	
	@Resource
	private InvoiceRepository invoiceRepository;
	
	@Resource RouteService routeService;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@Resource
	private com.vishwakarma.provilac.utils.AsyncJobs asyncJobs;
	
	@Resource 
	private CollectionService collectionService;
	
	@Resource
	private DepositService depositService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private Environment environment;
	
	@InitBinder(value = "payment")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Invoice.class,new InvoicePropertyEditor(invoiceRepository));
		binder.setValidator(paymentValidator);
	}

	private Model populateModelForAdd(Model model, Payment payment) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("payment", payment);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("customers",userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		//model.addAttribute("users", userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser));
		model.addAttribute("minBalanceForCashback", systemPropertyHelper.getSystemProperty(SystemProperty.MIN_BALANCE_FOR_CASHBACK, "0.0"));
		model.addAttribute("cashbackPercentage", systemPropertyHelper.getSystemProperty(SystemProperty.CASHBACK_PECENTAGE, "0.0"));
		float pointValue=Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.POINT_VALUE, "0"));
		model.addAttribute("pointValue",pointValue);
		return model;
	}

	@RequestMapping(value = "/admin/payment/add")
	public String addPayment(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(!(loggedInUser.hasAnyRole(Role.ROLE_SUPER_ADMIN) || loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT) || loggedInUser.hasAnyRole(Role.ROLE_ADMIN))){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Payment());
		return "admin-pages/payment/add";
	}

	@RequestMapping(value = "/admin/payment/add", method = RequestMethod.POST)
	public String paymentAdd(Payment payment, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		if(!(loggedInUser.hasAnyRole(Role.ROLE_SUPER_ADMIN) || loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT) || loggedInUser.hasAnyRole(Role.ROLE_ADMIN))){
			return "redirect:/admin/denied";
		}
		
		payment.setHasDeposited(true);
		payment.setIsVerified(true);//discussion 21-4-2016 for gen collection via cron
		payment.setReceivedBy(loggedInUser);
		
		paymentValidator.validate(payment, formBinding);
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, payment);
			return "admin-pages/payment/add";
		}
		payment.setAmount(payment.getAmount() + payment.getPointsAmount() + payment.getCashback());
		payment = paymentService.createPayment(payment, loggedInUser, true,true);
	
		User customer = payment.getCustomer();
		double lastpendingDues = customer.getLastPendingDues();
		
		if(payment.getAdjustmentAmount()!=0){
			lastpendingDues = lastpendingDues - (payment.getAmount()+payment.getAdjustmentAmount());
		}else{
			lastpendingDues = lastpendingDues - payment.getAmount();
		}
		customer.setLastPendingDues(lastpendingDues);
		
		if(payment.getPointsRedemed()>0){
			customer.setAccumulatedPoints(customer.getAccumulatedPoints()-payment.getPointsRedemed());
		}
		userService.updateUser(customer.getId(), customer, loggedInUser, true, true);

		//Update his pending dues
		if(null!=payment.getInvoice()){
			Invoice invoice = payment.getInvoice();
			invoice.setIsPaid(true);
			invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, true, true);
		}
		//notification on Bounce Cheque
		/*if(payment.getIsBounced()){
			plivoSMSHelper.sendSMS(payment.getCustomer().getMobileNumber(), "Dear Provilac Customer your cheque has been dishourned. Please note bank charges will be applied.");
			FirebaseWrapper.sendBouncedPaymentNotification(payment);
			emailHelper.sendMailForPaymentBounced(payment.getCustomer(),payment);
		}*/
		return "redirect:/admin/payment/add";
	}

	@RequestMapping(value = "/admin/payment/list")
	public String getPaymentList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm,
										@RequestParam(required = false) String userCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Payment> page = null;
		User user = userService.getLoggedInUser();
		if(null!=userCode){
			page = paymentService.getPaymentsByCustomerCode(userCode, pageNumber, user, false, false);
		}else{
			if (StringUtils.isNotBlank(searchTerm)) {
				page = paymentService.searchPayment(pageNumber, searchTerm, user,true,false);
			} else {
				page = paymentService.getPayments(pageNumber, user, true, true);
			}
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("payments", page.getContent());

		return "/admin-pages/payment/list";
	}

	@RequestMapping(value = "/admin/payment/show/{cipher}")
	public String showPayment(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Payment payment = paymentService.getPayment(id, true, loggedIUser,true,false);
		model.addAttribute("payment", payment);
		return "/admin-pages/payment/show";
	}

	@RequestMapping(value = "/admin/payment/update/{cipher}")
	public String updatePayment(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Payment payment = paymentService.getPayment(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, payment);
		model.addAttribute("id", payment.getId());
		model.addAttribute("version", payment.getVersion());
		return "/admin-pages/payment/update";
	}

	@RequestMapping(value = "/admin/payment/update", method = RequestMethod.POST)
	public String paymentUpdate(Payment payment, 
										BindingResult formBinding, 
										Model model) {

		payment.setUpdateOperation(true);
		paymentValidator.validate(payment, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, payment);
			return "admin-pages/payment/update";
		}
		payment.setId(Long.parseLong(model.asMap().get("id") + ""));
		payment.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		
		//AccumulatedPoints
		User customer=payment.getCustomer();
		if(payment.getPointsRedemed()>0){
			customer.setAccumulatedPoints(customer.getAccumulatedPoints()-payment.getPointsRedemed());
		}
		userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
		paymentService.updatePayment(payment.getId(), payment, loggedInUser,true,false);
		
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Payment updated successfully";
		return "redirect:/admin/payment/list?message=" + message;
	}

	@RequestMapping(value = "/admin/payment/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			paymentService.deletePayment(id);
			message = "Payment deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Payment";
		}
		return "redirect:/admin/payment/list?message=" + message;
	}
	
	/*
	 * Bounced Payment
	 */
	@RequestMapping(value = "/admin/bouncedPayment/list")
	public String getBouncedPaymentList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Payment> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = paymentService.searchPayment(pageNumber, searchTerm, user,true,false);
		} else {
			page = paymentService.getBouncedPayments(true, pageNumber, user, true, false);
			
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("payments", page.getContent());

		return "/admin-pages/bouncedPayment/list";
	}
	
	@RequestMapping(value = "/admin/bouncedPayment/show/{cipher}")
	public String showBouncedPayment(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Payment payment = paymentService.getPayment(id, true, loggedIUser,true,false);
		model.addAttribute("payment", payment);
		return "/admin-pages/bouncedPayment/show";
	}
	
	@RequestMapping(value = "/admin/bouncedPayment/delete/{cipher}")
	public String deletePayment(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			paymentService.deletePayment(id);
			message = "Payment deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Payment";
		}
		return "redirect:/admin/bouncedPayment/list?message=" + message;
	}
	
	//Undeposited Cheque payments
	@RequestMapping(value = "/admin/payment/undepositedChequelist")
	public String getUndepositedChequeList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Payment> page = null;
			if (StringUtils.isNotBlank(searchTerm)) {
				page = paymentService.searchPayment(pageNumber, searchTerm, loggedInUser,true,false);
			} else {
				page = paymentService.getUndepositedChequePaymants(pageNumber, loggedInUser, true, false);
			}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("payments", page.getContent());

		return "/admin-pages/undepositedChequePayments/list";
	}
	
	//Unverified cheque payments
	@RequestMapping(value = "/admin/payment/unVerifiedChequelist")
	public String getUnVerifiedChequeList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Payment> page = null;
			if (StringUtils.isNotBlank(searchTerm)) {
				page = paymentService.searchPayment(pageNumber, searchTerm, loggedInUser,true,false);
			} else {
				page = paymentService.getUnVerifieddepositedChequePaymants(pageNumber, loggedInUser, true, false);
			}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("payments", page.getContent());

		return "/admin-pages/UnVerifiedChequeList/list";
	}
	
	@RequestMapping(value = "/admin/unVerifiedChequePayment/verify/{cipher}")
	public String veriFyCheque(@PathVariable String cipher, Model model) {
		User loggedInUser = userService.getLoggedInUser();
		
		long id = Long.parseLong(EncryptionUtil.decode(cipher));
		
		Payment payment = paymentService.getPayment(id, false, null, false, false);
		// update collectionBoy's colection outstanding
		if(null != payment.getReceivedBy()){
			User collectionBoy = userService.getUser(payment.getReceivedBy().getId(), false, loggedInUser, false, false);
			double amountToDeduct = payment.getAmount();
			if(payment.getCashback() != 0) {
				amountToDeduct = payment.getAmount() - payment.getCashback();
			}
			collectionBoy.setCollectionOuststanding(collectionBoy.getCollectionOuststanding() - amountToDeduct);
			userService.updateCollectionBoyCollectionOutstanding(collectionBoy.getId(), collectionBoy, loggedInUser, false, false);
		}
		payment.setUpdateOperation(true);
		
		paymentService.verifyChequePayment(payment.getId(), payment, loggedInUser, false, false);
		String message = "Cheque verified successfully";
		return "redirect:/admin/payment/unVerifiedChequelist?message=" + message;
	}
	
	@RequestMapping(value="/admin/payment/accountLedger/{code}")
	public String getUserAccountLedger(@PathVariable String code,Model model){
		
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		User user = userService.getUserByCode(code, false, loggedInUser, false, false);
		List<Payment> payments = paymentService.getAllPaymentsByCustomer(code, loggedInUser, true, true);
		List<Invoice> invoices = invoiceService.getAllInvoicesByCustomer(code, loggedInUser, true, true);
		model.addAttribute("totalDues",user.getLastPendingDues());
		model.addAttribute("customer",user.getFullName());
		model.addAttribute("payments", payments);
		model.addAttribute("invoices", invoices);

		return "admin-pages/user/accountLedger";
	}
	
	/**
	 * Payment Details of routes
	 */
	@RequestMapping(value = "/admin/payment/details")
	public String getRouteWisePaymentDetails(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Route> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = routeService.searchRoute(pageNumber, searchTerm, user,false,true);
		} else {
			page = routeService.getRoutes(pageNumber, user,false,true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.add(Calendar.MONTH, -1);
		calendar.set(calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date fromDate = calendar.getTime();
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date toDate = calendar.getTime();
		
		List<PaymentDetailsDTO>paymentDetailsDTOs = new ArrayList<PaymentDetailsDTO>();
		for (Route route : page.getContent()) {
			Double totalAmount = 0.0;
			Double totalLastPendingDues= 0.0;
			Double totalPaidAmount = 0.0;
			
			for (RouteRecord routeRecord : route.getRouteRecords()) {
				Invoice invoice  =invoiceService.getInvoiceByCustomerAndDateRange(routeRecord.getCustomer().getId(), fromDate, toDate, loggedInUser, false, false);
				if(null!=invoice){
					totalAmount+=invoice.getTotalAmount();
				}
			totalLastPendingDues+=routeRecord.getCustomer().getLastPendingDues();
			
			}
			paymentDetailsDTOs.add(new PaymentDetailsDTO(route.getId(),route.getName(),totalAmount,totalLastPendingDues,totalPaidAmount));
		}
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("routes", paymentDetailsDTOs);

		return "/admin-pages/paymentDetails/list";
	}
	
	@RequestMapping(value="/admin/payment/showCustomers/{routeId}")
	public String showUnPaidCustomers(@PathVariable Long routeId,
										Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm){
		
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		model.addAttribute("message", message);
		Page<Invoice> page = null;
		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.MONTH, -1);
		calendar.set(calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date fromDate = calendar.getTime();
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date toDate = calendar.getTime();

		if (StringUtils.isNotBlank(searchTerm)) {
			page = invoiceService.searchInvoices(pageNumber,searchTerm,CollectionStatus.Not_Collected, loggedInUser,false,true);
		} else {
			page = invoiceService.searchInvoicesByCollectionStatus(pageNumber,routeId,CollectionStatus.Not_Collected, fromDate, toDate, loggedInUser, false, true);
		}

		List<User>customersToSend = new ArrayList<User>();
		for (Invoice invoice : page.getContent()) {
			customersToSend.add(invoice.getCustomer());
		}

		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("customers", customersToSend);
		model.addAttribute("routeId", routeId);
		
		return "/admin-pages/paymentDetails/showCustomers";
	}
	/**
	 * End of Payment of routes
	 */
	
	@RequestMapping(value=AppConstants.WEB_PAYMENT_URL, method=RequestMethod.GET)
	public String payNow(@RequestParam(value=AppConstants.WEB_PAYMENT_PARAM_NAME, required=true) String encodedId, Model model) {

		/*
		 * 1. Change form target
		 * 2. Change to cipher
		 * 3. Check success and failure url's
		 * 4. Check system properties and server.url property
		 */
		Long id = Long.parseLong(EncryptionUtil.decode(encodedId));
		Invoice invoice = invoiceService.getInvoice(id, true, null, false, false);
		model.addAttribute("invoice", invoice);
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.PAYU_FURL);
		return "admin-pages/payment/webpayment/paynow";
	}
	
	@RequestMapping(value=AppConstants.PAYU_SURL)
	public String handlePayUWebPaymentSuccess(@RequestParam String status,
												@RequestParam String firstname,
												@RequestParam double amount,
												@RequestParam String txnid,
												@RequestParam String hash,
												@RequestParam String productinfo,
												@RequestParam String phone,
												@RequestParam String email,
												@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		if(!hash.equals(encodedHash)) {
			return "/admin-pages/payment/webpayment/payu_success_wrong";
		}

		if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/admin-pages/payment/webpayment/payu_success_wrong";
		}
		
		Invoice invoice = invoiceService.getInvoiceByCode(productinfo, true, null, false, false);
		User customer = invoice.getCustomer();
		
		Payment payment = new Payment();
		
		payment.setAmount(amount);
		double cashback = 0.0;
		double dues = customer.getLastPendingDues();
		float minBalanceForCashback = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.MIN_BALANCE_FOR_CASHBACK, "0.0"));
		float cashbackPercentage = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.CASHBACK_PECENTAGE, "0.0")) / 100;
		if(dues > 0 && (payment.getAmount() - dues) >= minBalanceForCashback) {
			//User has paid amount which makes dues zero and supasses min amount required for cashback
			cashback = (payment.getAmount() - dues) * cashbackPercentage;
		} else if(dues <= 0 && payment.getAmount() >= minBalanceForCashback) {
			//If dues are less than zero, and still user is paying more than min amount required for cashback
			cashback = payment.getAmount() * cashbackPercentage;
		}
		payment.setCashback(cashback);
		payment.setAmount(payment.getAmount() + payment.getCashback());
		
		payment.setTxnId(txnid);
		payment.setPaymentMethod(PaymentMethod.PAYU);
		payment.setInvoice(invoice);
		payment.setCustomer(customer);
		payment.setDate(new Date());
		paymentService.createPayment(payment, null, false, false);

		double lastpendingDues = customer.getLastPendingDues();
		lastpendingDues = lastpendingDues - payment.getAmount();
		customer.setLastPendingDues(lastpendingDues);
		userService.updateUser(customer.getId(), customer, null, false, false);
		
		return "/admin-pages/payment/webpayment/payu_success";
	}
	
	@RequestMapping(value=AppConstants.PAYU_FURL)
	public String handlePayUWebPaymentFailure(@RequestParam String status,
												@RequestParam String firstname,
												@RequestParam double amount,
												@RequestParam String txnid,
												@RequestParam String hash,
												@RequestParam String productinfo,
												@RequestParam String phone,
												@RequestParam String email,
												@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		return "/admin-pages/payment/webpayment/payu_failure";
	}
	
	@RequestMapping(value="/paynow/{page}")
	public String showPaymentResult(@PathVariable Integer page) {
		if(page == 1){
			return "/admin-pages/payment/webpayment/payu_success";
		} else if(page == 2) {
			return "/admin-pages/payment/webpayment/payu_success_wrong";
		}
		return "/admin-pages/payment/webpayment/payu_failure";
	}
	
	@RequestMapping(value = "/mobilePayments/success")
	public String paymentSuccess() {
		return "admin-pages/mobilePayment/success";
	}
	
	@RequestMapping(value = "/mobilePayments/failure")
	public String paymentFailure() {
		return "admin-pages/mobilePayment/failure";
	}
	
	@RequestMapping(value = "/openapi/mobilePayments/ios-success")
	public String iOSPaymentSuccess() {
		return "admin-pages/mobilePayment/ios-success";
	}
   
	@RequestMapping(value = "/openapi/mobilePayments/ios-failure")
	public String iOSPaymentFailure() {
		return "admin-pages/mobilePayment/ios-failure";
	}
	
	/**
	 * REST API Methods
	 */

	/**
	 * USED_FROM_DEVICE_SIDE
	 */
	@RequestMapping(value="/restapi/payment/add" ,method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse addPayment(@RequestBody(required=true)PaymentDTO paymentDTO) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			Payment payment = new Payment();
			
			User customer = userService.getUserByCode(paymentDTO.getCustomerCode(), false, loggedInUser, false, false);
			payment.setAmount(paymentDTO.getAmount());
			
			if(paymentDTO.getPointsRedemed()>0){
				payment.setPointsRedemed(paymentDTO.getPointsRedemed());
				float pointValue = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.POINT_VALUE, "0"));
				payment.setPointsAmount(paymentDTO.getPointsRedemed() * pointValue);
				customer.setAccumulatedPoints(customer.getAccumulatedPoints()-payment.getPointsRedemed());
			}
			
			double cashback = 0.0;
			double dues = customer.getLastPendingDues();
			float minBalanceForCashback = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.MIN_BALANCE_FOR_CASHBACK, "0.0"));
			float cashbackPercentage = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.CASHBACK_PECENTAGE, "0.0")) / 100;
			if(dues > 0 && (payment.getAmount() - dues) >= minBalanceForCashback) {
				//User has paid amount which makes dues zero and supasses min amount required for cashback
				cashback = (payment.getAmount() - dues) * cashbackPercentage;
			} else if(dues <= 0 && payment.getAmount() >= minBalanceForCashback) {
				//If dues are less than zero, and still user is paying more than min amount required for cashback
				cashback = payment.getAmount() * cashbackPercentage;
			}
			
			payment.setCashback(cashback);
			payment.setAmount(payment.getAmount() + payment.getPointsAmount() + payment.getCashback());
			
			if(null!=paymentDTO.getPaymentMethod()){
				payment.setPaymentMethod(PaymentMethod.valueOf(paymentDTO.getPaymentMethod()));
			if(paymentDTO.getPaymentMethod().equalsIgnoreCase("PAYTM") && StringUtils.isNotBlank(paymentDTO.getPaytmOrderId()) && StringUtils.isNotEmpty(paymentDTO.getPaytmOrderId())){
				asyncJobs.checkStatusWithPaytm(paymentDTO.getTxnId(),paymentDTO.getPaytmOrderId());
			}
			
			}
			
			if(null!=paymentDTO.getTxnId())
				payment.setTxnId(paymentDTO.getTxnId());
			
			if(null!=paymentDTO.getDate())
				payment.setDate(DateHelper.parseDate(paymentDTO.getDate()));
			
				payment.setCustomer(customer);
				
			if(null!=paymentDTO.getInvoiceCode()){
				Invoice invoice= invoiceService.getInvoiceByCode(paymentDTO.getInvoiceCode(), false, null, false, false);
				payment.setInvoice(invoice);
			}
			if(null!= paymentDTO.getBankName())
				payment.setBankName(paymentDTO.getBankName());
			if(null!=paymentDTO.getChequeNumber())
				payment.setChequeNumber(paymentDTO.getChequeNumber());
			
			BindingResult formBinding = new DataBinder(payment).getBindingResult();
			
			paymentValidator.validate(payment, formBinding);
			
			if(formBinding.hasErrors()) {
				String errorStr = null;
				for(ObjectError objectError : formBinding.getAllErrors()) {
					
					if(StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
						continue;
					}
					errorStr = errorStr + objectError.getDefaultMessage();
				}
				throw new ProvilacException(errorStr);
			}
			payment= paymentService.createPayment(payment, loggedInUser, false, false);
			
			double lastpendingDues = customer.getLastPendingDues();
			lastpendingDues = lastpendingDues - payment.getAmount();
			customer.setLastPendingDues(lastpendingDues);
			/*if(payment.getPointsAmount() >0.0){
				lastpendingDues=lastpendingDues-payment.getPointsAmount();
			}*/
			
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
			
			//Update his pending dues
			if(null!=payment.getInvoice()){
				Invoice invoice = payment.getInvoice();
				invoice.setIsPaid(true);
				invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, true, true);
			}
			apiResponse.addData("payment", new PaymentDTO(payment));
			
			
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/payment/list")
	@ResponseBody
	public ApiResponse getPaymentsByCustomer(@RequestParam(defaultValue="1") Integer pageNumber,
											 @RequestParam(required=true)String customerCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Page<Payment> page=paymentService.getPaymentsByCustomerCode(customerCode, pageNumber, null, false, true);
			//List<Payment> payments =  paymentService.getAllPaymentsByCustomer(customerCode, null, false, false);
			List<PaymentDTO> dtos = new ArrayList<PaymentDTO>();
			for (Payment payment : page) {
				dtos.add(new PaymentDTO(payment));
			}
			int current = page.getNumber() + 1;
			int begin = Math.max(1, current - 5);
			int end = Math.min(begin + 10, page.getTotalPages());

			apiResponse.addData("Payments", dtos)
					   .addData("current", current)
					   .addData("begin", begin)
					   .addData("end", end);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/payment/listById")
	@ResponseBody
	public ApiResponse getPaymentsById(@RequestParam(required=true) String userCode){
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			  List<Payment> paymentLists =  paymentService.getAllPaymentsByCustomer(userCode, null, false, false);
			  List<PaymentDTO> dtos = new ArrayList<PaymentDTO>();
			  for (Payment payment : paymentLists) {
				dtos.add(new PaymentDTO(payment));
			}
			  apiResponse.addData("payments", dtos);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/payment/details")
	@ResponseBody
	public ApiResponse getPaymentDetails(@RequestParam(required=true)String paymentCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Payment payment =  paymentService.getPaymentByCode(paymentCode, false, null, false, false);
			apiResponse.addData("Payment", new PaymentDTO(payment));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/payment/salt")
	@ResponseBody
	public ApiResponse getSalt() {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
			apiResponse.addData("salt", salt);
			}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
		
	}
	
	//USED_FROM_DEVICE
	@RequestMapping(value="/restapi/payment/addPaymentByCollectionBoy", method= RequestMethod.POST)
	@ResponseBody
	public ApiResponse addPaymentByCollectionBoy(@RequestPart(required=true)Double[] amounts,
												 @RequestPart(required=true)String[] customerCodes,
												 @RequestPart(required=true)String[] paymentMethods,
												 @RequestPart(required=true)String[] bankNames,
												 @RequestPart(required=true)String[] chequeNumbers,
												 @RequestPart(required=true)String[] receiptNumbers,
												 @RequestPart(required=true)String[] transactionIds,
												 @RequestPart(required=false)Integer[] pointsRedemeds,
												 @RequestPart(required=false)Double[] ponitsAmounts){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<PaymentDTO> paymentDTOs = new ArrayList<PaymentDTO>();
			
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			
			calendar.add(Calendar.MONTH, -1);
			calendar.set(calendar.DATE, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date fromDate = calendar.getTime();
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.add(Calendar.MONTH, -1);
			calendar2.set(Calendar.DATE, calendar2.getActualMaximum(Calendar.DATE)); 
			calendar2.set(Calendar.HOUR_OF_DAY, 0);
		    calendar2.set(Calendar.MINUTE, 0);
			calendar2.set(Calendar.SECOND, 0);
			calendar2.set(Calendar.MILLISECOND, 0);
			Date toDate = calendar2.getTime();
			
			if(null!=customerCodes){
				for (int i = 0; i < customerCodes.length; i++) {
					Payment payment = new Payment();
					String customerCode = customerCodes[i];
					User loggedInUser = userService.getLoggedInUser();
					User customer = userService.getUserByCode(customerCode, true, loggedInUser, false, false);
					payment.setCustomer(customer);
					if(amounts[i]!=0){
						payment.setAmount(amounts[i]);
					}
					if(null!=paymentMethods[i]){
						if(paymentMethods[i].equals(PaymentMethod.Cheque) || paymentMethods[i].equals(PaymentMethod.NEFT)){
							if(null!=bankNames[i]){
								payment.setBankName(bankNames[i]);
							}
							if(null!=chequeNumbers[i]){
								payment.setChequeNumber(chequeNumbers[i]);
							}
						}else{
							if(null!=receiptNumbers[i]){
								payment.setReceiptNo(receiptNumbers[i]);
							}
						}
						payment.setPaymentMethod(PaymentMethod.valueOf(paymentMethods[i]));
					}
					if(null!=transactionIds[i]){
						payment.setTxnId(transactionIds[i]);
					}
					
					payment.setDate(date);
					
					if(pointsRedemeds[i]>=0){
						payment.setPointsRedemed(pointsRedemeds[i]);
						//AccumulatedPoints
						if(customer.getAccumulatedPoints()>0){
						customer.setAccumulatedPoints(customer.getAccumulatedPoints()-pointsRedemeds[i]);
						}
					}
					
					if(ponitsAmounts[i]>=0){
					payment.setPointsAmount(pointsRedemeds[i]);
					}
					
					payment = paymentService.createPayment(payment, loggedInUser, false, false);
					
					double lastpendingDues = customer.getLastPendingDues();
					
					if (payment.getAdjustmentAmount()!=0){
						lastpendingDues = lastpendingDues - (payment.getAmount()+payment.getAdjustmentAmount());
					}else{
						
						lastpendingDues = lastpendingDues - payment.getAmount();
					}
					
					customer.setLastPendingDues(lastpendingDues);
					userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
					
					Invoice invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate, loggedInUser, false, false);
					if (null != invoice) {
						invoice.setIsPaid(true);
						invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, false, false);
					}
					paymentDTOs.add(new PaymentDTO(payment));
				}
			}
			
			apiResponse.addData("payments", paymentDTOs);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/payment/undepositedChequelist")
	@ResponseBody
	public ApiResponse getUndepositedChequeList(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<Payment> payments =  paymentService.getAllPaymentsByReceivedBy(loggedInUser.getId(),loggedInUser, false,false);
			List<PaymentDTO>dtos = new ArrayList<PaymentDTO>();
			for (Payment payment : payments) {
				if(payment.getPaymentMethod().equals(PaymentMethod.Cheque) && !payment.getIsVerified()){
					dtos.add(new PaymentDTO(payment));
				}
			}
			apiResponse.addData("updepositedCheques", dtos);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
		
	@RequestMapping(value="/payment/chequeDetails/update")
	@ResponseBody
	public ApiResponse updateChequeIsbounced(@RequestParam(required=true)boolean flag,
											@RequestParam(required=true)Long id){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Payment payment = paymentService.getPayment(id, true, null, false, false);
			//boolean x = Boolean.parseBoolean(flag);
			payment.setIsBounced(flag);
			User customer = payment.getCustomer();
			if(flag==true && payment.getPaymentMethod() == PaymentMethod.Cheque){
				
				firebaseWrapper.sendBouncedPaymentNotification(payment);
				plivoSMSHelper.sendSMS(payment.getCustomer().getMobileNumber(), "Dear Provilac Customer your cheque has been dishourned. Please note bank charges will be applied.");
				
				//Add 250 Rs extra charge for bounced payment
				customer.setLastPendingDues((payment.getCustomer().getLastPendingDues() + 250 + payment.getAmount()));
				
				payment = paymentService.changeBounceStatus(id, flag, null, false, false);
				userService.updateUser(customer.getId(), customer, null, false, false);
				apiResponse.addData("payment", new PaymentDTO(payment));
			} else if (payment.getPaymentMethod() == PaymentMethod.Cheque){
				throw new ProvilacException("Can not mark this payment as not bounced, please add separate payment.", "400");
			} else {
				throw new ProvilacException("Only cheque payments can be marked as bounced.", "400");
			}
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}

	@RequestMapping(value="/openapi/payment/getHashAndTxnId")
	@ResponseBody
	public ApiResponse getHashAndTxnIdForWebPayments(@RequestParam String key,
													@RequestParam String productInfo,
													@RequestParam String surl,
													@RequestParam String furl,
													@RequestParam String service_provider,
													@RequestParam String firstName,
													@RequestParam String lastName,
													@RequestParam String address1,
													@RequestParam String address2,
													@RequestParam String city,
													@RequestParam String state,
													@RequestParam String country,
													@RequestParam String zipcode,
													@RequestParam String udf1,
													@RequestParam String udf2,
													@RequestParam String udf3,
													@RequestParam String udf4,
													@RequestParam String udf5,
													@RequestParam String pg,
													@RequestParam String phone,
													@RequestParam String email,
													@RequestParam double amount) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		/**
		 * 1. Validate Fields
		 * 2. Generate TxnId
		 * 3. Create Hash String
		 * 4. Return response
		 */
		Invoice invoice = invoiceService.getInvoiceByCode(productInfo, true, null, false, false);
		String originalKey = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		String successUrl = serverUrl + AppConstants.PAYU_SURL;
		String failureUrl = serverUrl + AppConstants.PAYU_FURL;
		
		if(!originalKey.equals(key) || !successUrl.equals(surl) || !failureUrl.equals(furl) || 
				!invoice.getCustomer().getFirstName().equals(firstName) || !invoice.getCustomer().getMobileNumber().equals(phone)) {
			apiResponse.setError("Something went wrong, please refresh the page.", "400");
			return apiResponse;
		}
		
		String txnId = "WEB-" + invoice.getCode() + "-" + System.currentTimeMillis();
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String amountString = new DecimalFormat("##.##").format(amount);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + firstName + "|" + email + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			apiResponse.setError("Unable to generate secured hash. Please try again later.", "500");
		}
		apiResponse.addData("hash", encodedHash);
		apiResponse.addData("txnId", txnId);
		apiResponse.addData("amount", amountString);
		return apiResponse;
	}
	
	private String hashCal(String type, String str) throws NoSuchAlgorithmException {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(type);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append("0");
            }
            hexString.append(hex);
        }
        return hexString.toString();
	}
	
	@RequestMapping(value="/openapi/paytm/generateChecksum", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse generateChecksum(HttpServletRequest request) {
		
		ApiResponse response = new ApiResponse(true);
		
		Enumeration<String> paramNames = request.getParameterNames();
		Map<String, String[]> mapData = request.getParameterMap();
		TreeMap<String,String> parameters = new TreeMap<String,String>();
		parameters.put("MID", (String) mapData.get("MID")[0]);
		parameters.put("ORDER_ID",(String) mapData.get("ORDER_ID")[0]);
		parameters.put("CUST_ID",(String) mapData.get("CUST_ID")[0]);
		parameters.put("INDUSTRY_TYPE_ID",(String) mapData.get("INDUSTRY_TYPE_ID")[0]);
		parameters.put("CHANNEL_ID",(String) mapData.get("CHANNEL_ID")[0]);
		parameters.put("TXN_AMOUNT",(String) mapData.get("TXN_AMOUNT")[0]);
		parameters.put("WEBSITE",(String) mapData.get("WEBSITE")[0]);
		parameters.put("EMAIL", (String) mapData.get("EMAIL")[0]);
		parameters.put("MOBILE_NO", (String) mapData.get("MOBILE_NO")[0]);
		parameters.put("CALLBACK_URL", (String) mapData.get("CALLBACK_URL")[0]);
		while(paramNames.hasMoreElements()) {
			String paramName = (String)paramNames.nextElement();
			String paramValue = (String)mapData.get(paramName)[0];
			// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
			if(paramValue.toLowerCase().contains("refund")){
				continue;
			}
			//parameters.put(paramName,paramValue);	
		}
		
		SystemProperty systemProperty = systemPropertyHelper.getSystemProperty(SystemProperty.PAYTM_MERCHANT_KEY);
		try {
			String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(systemProperty.getPropValue(), parameters);
			response.addData("checksum", checkSum);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Something went wrong while generating checksum. Please try again.", "500");
		}
		return response;
	}
	
	/** 
	 * End REST API Methods
	 */
}
