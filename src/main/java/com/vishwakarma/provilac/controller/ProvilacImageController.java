package com.vishwakarma.provilac.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.ProvilacImageDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.ProvilacImage;
import com.vishwakarma.provilac.model.ProvilacImage.LinkTo;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.service.ProvilacImageService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class ProvilacImageController {

	@Resource
	private ProvilacImageService provilacImageService;

	@Resource
	private UserService userService;

	@Resource
	private Environment environment;

	@Resource
	private AppFileUtils appFileUtils;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
	}

	private Model populateModelForAdd(Model model, ProvilacImage provilacImage) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("provilacImage", provilacImage);
		model.addAttribute("provilacImages", provilacImageService.getallProvilacImages(loggedInUser, false, false));

		return model;
	}

	@RequestMapping(value = "/admin/provilacimage/add")
	public String addProvilacImage(Model model) {
		model = populateModelForAdd(model, new ProvilacImage());
		return "admin-pages/provilacimage/add";
	}

	@RequestMapping(value = "/admin/provilacimage/add", method = RequestMethod.POST)
	public String provilacImageAdd(ProvilacImage provilacImage, BindingResult formBinding, Model model, @RequestParam(required = true, value = "provilacimage") MultipartFile[] provilacimages, @RequestParam(required = false, value = "linkTo") LinkTo linkTo) {

		for (int i = 0; i < provilacimages.length; i++) {
			MultipartFile provilacimage = provilacimages[i];
			ProvilacImage image = new ProvilacImage();

			if (null != provilacimage && !provilacimage.isEmpty()) {
				String contentType = provilacimage.getContentType();
				if (!contentType.contains("image/")) {
					formBinding.addError(new FieldError("provilacImage", "PicUrl", "Please select valid Provilac Images"));
				} else if (provilacimage.getSize() > 5242880) { // Max Size 5MB
					formBinding.addError(new FieldError("provilacImage", "PicUrl", "Please Select valid Image Size less Than 5MB"));
				}
				try {
					image.setPicData(provilacimage.getBytes());
					image.setTempFileName(provilacimage.getOriginalFilename());
					image.setLinkTo(linkTo);
				} catch (IOException e) {
					e.printStackTrace();

				}
			} else {
				formBinding.addError(new FieldError("provilacImage", "PicUrl", "Please Select Valid Image"));
			}

			if (formBinding.hasErrors()) {
				model = populateModelForAdd(model, image);
				return "admin-pages/provilacimage/add";
			}

			User loggedInUser = userService.getLoggedInUser();
			image = provilacImageService.createProvilacImage(image, loggedInUser, false, false);
		}
		String message = "Provilac Image Add Successfully";
		return "redirect:/admin/provilacimage/show?message=" + message;
	}

	@RequestMapping(value = "/admin/provilacimage/show")
	public String showProvilacImage(Model model) {
		model = populateModelForAdd(model, new ProvilacImage());
		return "admin-pages/provilacimage/show";

	}

	@RequestMapping(value = "/admin/provilacimage/delete", method = RequestMethod.POST)
	public String deleteImage(@RequestParam(required = true) Long id) {
		String message = "";
		try {
			provilacImageService.deleteProvilacImage(id);
			message = "Image deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Image";
		}
		return "redirect:/admin/provilacimage/show?message=" + message;
	}

	@RequestMapping(value = "admin/provilacimage/update", method = RequestMethod.POST)
	public String updateProvilacImage(ProvilacImage provilacImage, BindingResult formBinding, Model model, @RequestParam(required = false, value = "imageid") Long id, @RequestParam(required = false, value = "newprovilacimage") MultipartFile provilacimage, @RequestParam(required = false, value = "linkTo") LinkTo linkTo) {

		String message = "";
		try {
			if (null != provilacimage && !provilacimage.isEmpty()) {

				String contentType = provilacimage.getContentType();
				if (!contentType.contains("image/")) {
					formBinding.addError(new FieldError("provilacImage", "PicUrl", "Please Select valid Image"));
				} else if (provilacimage.getSize() > 5242880) {
					formBinding.addError(new FieldError("provilacImage", "PicUrl", "Please Select valid Image less Than 5MB"));
				}

				try {
					provilacImage.setPicData(provilacimage.getBytes());
					provilacImage.setTempFileName(provilacimage.getOriginalFilename());
					provilacImage.setLinkTo(linkTo);
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				provilacImage.setLinkTo(linkTo);
			}
			if (formBinding.hasErrors()) {
				model = populateModelForAdd(model, provilacImage);
				return "admin-pages/provilacimage/show";
			}

			User logInUser = userService.getLoggedInUser();
			ProvilacImage existingProvilacImage = provilacImageService.updateProvilacImage(id, provilacImage, logInUser, false, false);
			message = "Image Update Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to update Bvg Image";
		}
		return "redirect:/admin/provilacimage/show?message=" + message;
	}

	/**
	 * START API Methods
	 */

	@RequestMapping(value = "/openapi/getProvilacImage", method = RequestMethod.GET)
	@ResponseBody
	public void getProvilacImageById(Long id, HttpServletResponse httpServletResponse) {

		if (Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(AppConstants.PROVILAC_IMAGES_FOLDER, id + "");
				// TODO: Remove this, find some better mechanism for this
				if (redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"), AppConstants.PROVILAC_IMAGES_FOLDER);
			File file = new File(baseDir, id + "");

			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/jpeg");
			httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@RequestMapping(value = "/openapi/provilacimages/list")
	@ResponseBody
	public ApiResponse getAllProvilacImages() {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			List<ProvilacImage> provilacImages = provilacImageService.getallProvilacImages(null, false, false);
			List<ProvilacImageDTO> provilacImageDTOs = new ArrayList<ProvilacImageDTO>();
			for (ProvilacImage provilacImage : provilacImages) {
				provilacImageDTOs.add(new ProvilacImageDTO(provilacImage));
			}
			apiResponse.addData("images", provilacImageDTOs);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * END API Methods
	 */
}
