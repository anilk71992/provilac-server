package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.repository.SystemPropertyRepository;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

@Controller
@SessionAttributes({"id"})
public class SystemPropertyController {

	@Resource
	private SystemPropertyRepository systemPropertyRepository;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	private final static Logger log = LoggerFactory.getLogger(SystemPropertyController.class);
	
	
	@InitBinder(value = "systemProperty")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
	}

	private Model populateModelForAdd(Model model, SystemProperty systemProperty) {
		model.addAttribute("payment", systemProperty);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}
	
	
	@RequestMapping("/admin/systemProperty/list")
	public String systemPropertyList(final Model model,
			@RequestParam(defaultValue="1") Integer pageNumber,
			@RequestParam(required = false) String message) {
		model.addAttribute("message", message);
		
		Page<SystemProperty> page = systemPropertyHelper.getSystemProperties(pageNumber);
	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current-5);
	    int end = Math.min(begin + 10, page.getTotalPages());

	    model.addAttribute("page", page);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("currentIndex", current);
	    model.addAttribute("properties", page.getContent());
	    
		
		return "/admin-pages/systemproperty/list";
	}
	
	@RequestMapping(value = "/admin/systemproperty/add", method = RequestMethod.GET)
	public String systemPropertyAdd(Model model, @RequestParam(required=false) String retUrl) {
		model.addAttribute("property", new SystemProperty());
		if (!StringUtils.isEmpty(retUrl)) {
			model.addAttribute("retUrl", retUrl);
		}
		return "/admin-pages/systemproperty/add";
	}
	
	@RequestMapping(value = { "/admin/systemproperty/add" }, method = RequestMethod.POST)
	public String submitSystemProperty(@Valid SystemProperty systemproperty, BindingResult formBinding) {
		if (formBinding.hasErrors()) {
			return "/admin-pages/systemproperty/add";
		}
		systemPropertyRepository.save(systemproperty);
		String message = "Added Successfully!";
		return "redirect:/admin/systemproperty/list?message=" + message;
	}
	
	@RequestMapping(value = {"/systemproperty/{systempropertyId}","/show/systemproperty/{systempropertyId}"}, method = RequestMethod.GET)
	public String updateSystemProperty(@PathVariable Long systempropertyId,Model model,@RequestParam(required=false) String readOnly) {
		
		SystemProperty property = systemPropertyRepository.findOne(systempropertyId);
		log.info("Property Id --- " + systempropertyId + " --- " + systempropertyId);
		model.addAttribute("readOnly", readOnly);
		model.addAttribute("property", property);
		model.addAttribute("id", property.getId());
		return "/admin-pages/systemproperty/update";
	}
	
	@RequestMapping(value = "/admin/systemproperty/update/{id}")
	public String updatePayment(@PathVariable String id, Model model) {

		SystemProperty systemProperty = systemPropertyRepository.findOne(Long.parseLong(id));
		model = populateModelForAdd(model, systemProperty);
		model.addAttribute("id", systemProperty.getId());
		model.addAttribute("systemProperty", systemProperty);
		return "/admin-pages/systemproperty/update";
	}
	@RequestMapping(value = { "/admin/systemProperty/update" }, method = RequestMethod.POST)
	public String updateSystemProperty( SystemProperty systemProperty, BindingResult formBinding,
			Map<String, Object> model) {
		
		systemProperty.setId(Long.parseLong(model.get("id").toString()));
		systemPropertyRepository.save(systemProperty);
		model.remove("id");
		model.remove("version");
		String message = "Updated Successfully!";
		return "redirect:/admin/systemProperty/list?message=" + message;
	}
	
	@RequestMapping(value = "/systemproperty/delete/{systempropertyId}")
	public String deleteStatus(@PathVariable Long systempropertyId) {
		log.info("Property Id --- " + systempropertyId + " --- " + systempropertyId);
		systemPropertyRepository.delete(systempropertyId);
		String message = "Deleted Successfully!";
		return "redirect:/systemproperty/list?message=" + message;
	}
	
	/*
	 * REST API Methods
	 */

	
	@RequestMapping(value="/restapi/android/version",method=RequestMethod.GET)
	@ResponseBody
	public ApiResponse getAndroidVersion(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			
		List<SystemProperty> systemProperties = systemPropertyHelper.geAlltSystemProperties();
		Map<String,Object> map = new HashMap<String, Object>();
			for (SystemProperty systemProperty : systemProperties) {
				if(systemProperty.getPropName().equals("VERSION_NAME")){
					map.put("versionName", systemProperty.getPropValue());
				}else if(systemProperty.getPropName().equals("VERSION_CODE")){
					map.put("versionCode", Integer.parseInt(systemProperty.getPropValue()));
				}else if(systemProperty.getPropName().equals("IS_MANDATORY_UPDATE")){
					map.put("isMandatoryUpdate", Boolean.parseBoolean(systemProperty.getPropValue()));
				}else if(systemProperty.getPropName().equals("POINT_VALUE")){
					map.put("pointValue", Double.parseDouble(systemProperty.getPropValue()));
				}else if(systemProperty.getPropName().equals("MIN_BALANCE_FOR_CASHBACK")){
					map.put("minBalanceForCashback", Double.parseDouble(systemProperty.getPropValue()));
				}else if(systemProperty.getPropName().equals("CASHBACK_PECENTAGE")){
					map.put("cashbackPercentage", Double.parseDouble(systemProperty.getPropValue()));
				}
				map.put("currentDate", DateHelper.getFormattedTimestamp(new Date()));
			}
			apiResponse.setData(map);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
		
	}
	/*
	 * End REST Api Methods
	 */
}
