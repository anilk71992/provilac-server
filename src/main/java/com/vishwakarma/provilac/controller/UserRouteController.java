package com.vishwakarma.provilac.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.mvc.validator.UserRouteValidator;
import com.vishwakarma.provilac.property.editor.RoutePropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.RouteRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class UserRouteController {

	@Resource
	private UserRouteValidator userRouteValidator;

	@Resource
	private UserRouteService userRouteService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRepository routeRepository;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@InitBinder(value = "userRoute")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Route.class, new RoutePropertyEditor(routeRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(userRouteValidator);
	}

	private Model populateModelForAdd(Model model, UserRoute userRoute) {
		User loggedInUser = userService.getLoggedInUser();
		List<UserRoute> userRoutes= userRouteService.getAllUserRoutes(null, false, false);
		List<Route> routes = routeService.getAllRoutes(null, false, false);
		List<Route>routes1 = new ArrayList<Route>();
		List<Route>routes2 = new ArrayList<Route>();
		for (UserRoute userRoute1 : userRoutes) {
				if(routes.contains(userRoute1.getRoute())){
						routes1.add(userRoute1.getRoute());
				}
		}

		for (Route route : routes) {
			if(routes1.contains(route)){
						continue;
				}else{
					routes2.add(route);
				}
		}
		model.addAttribute("userRoute", userRoute);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_DELIVERY_BOY, loggedInUser, false, false));
		model.addAttribute("collectionBoys", userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser, false, false));
		model.addAttribute("routes", routes2);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/userRoute/add")
	public String addUserRoute(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new UserRoute());
		return "admin-pages/userRoute/add";
	}

	@RequestMapping(value = "/admin/userRoute/add", method = RequestMethod.POST)
	public String userRouteAdd(UserRoute userRoute, 
									BindingResult formBinding, 
									Model model) {
		
		userRouteValidator.validate(userRoute, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, userRoute);
			return "admin-pages/userRoute/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		userRouteService.createUserRoute(userRoute, loggedInUser, true,false);
		String message = "UserRoute added successfully";
		return "redirect:/admin/userRoute/list?message=" + message;
	}

	@RequestMapping(value = "/admin/userRoute/list")
	public String getUserRouteList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<UserRoute> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = userRouteService.searchUserRoute(pageNumber, searchTerm, user,true,false);
		} else {
			page = userRouteService.getUserRoutes(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("userRoutes", page.getContent());

		return "/admin-pages/userRoute/list";
	}

	@RequestMapping(value = "/admin/userRoute/show/{cipher}")
	public String showUserRoute(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		UserRoute userRoute = userRouteService.getUserRoute(id, true, loggedIUser,true,false);
		Route route = routeService.getRoute(userRoute.getRoute().getId(), false, null, false, false);
		model.addAttribute("userRoute", userRoute);
		model.addAttribute("route", route);
		return "/admin-pages/userRoute/show";
	}

	@RequestMapping(value = "/admin/userRoute/update/{cipher}/{routeId}")
	public String updateUserRoute(@PathVariable String cipher, Model model,@PathVariable String routeId) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		UserRoute userRoute = userRouteService.getUserRoute(id, true, loggedInUser,true,false);
		Route route = routeService.getRoute(Long.parseLong(routeId), false);
		model = populateModelForAdd(model, userRoute);
		model.addAttribute("id", userRoute.getId());
		model.addAttribute("route", route);
		model.addAttribute("version", userRoute.getVersion());
		return "/admin-pages/userRoute/update";
	}

	@RequestMapping(value = "/admin/userRoute/update", method = RequestMethod.POST)
	public String userRouteUpdate(UserRoute userRoute, 
										BindingResult formBinding, 
										Model model,
										@RequestParam(required=true, value="route1")String routeId) {

		userRoute.setUpdateOperation(true);
		userRouteValidator.validate(userRoute, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, userRoute);
			return "admin-pages/userRoute/update";
		}
		userRoute.setId(Long.parseLong(model.asMap().get("id") + ""));
		userRoute.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		Route route = routeService.getRoute(Long.parseLong(routeId), false);
		userRoute.setRoute(route);
		User loggedInUser = userService.getLoggedInUser();
		userRouteService.updateUserRoute(userRoute.getId(), userRoute, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "UserRoute updated successfully";
		return "redirect:/admin/userRoute/list?message=" + message;
	}

	@RequestMapping(value = "/admin/userRoute/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			userRouteService.deleteUserRoute(id);
			message = "UserRoute deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete UserRoute";
		}
		return "redirect:/admin/userRoute/list?message=" + message;
	}
	
	@RequestMapping(value="/admin/userRoute/import", method=RequestMethod.GET)
	public String importUserRoutes(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/userRoute/import";
	}
	
	@RequestMapping(value="/admin/userRoute/import", method=RequestMethod.POST)
	public String importUserRoutes(Model model, @RequestParam(required=true) MultipartFile file) {
		
		String message = "Successfully imported UserRoutes";
		if(null == file || file.isEmpty() || !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)) {
			
			message = "Please select valid excel 2007 or later file";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/route/import";
		}
		try {
			importUserRoutes(file);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error uploading routes, please try with valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/userRoute/import";
		}
		return "redirect:/admin/userRoute/list?message=" + message;
	}
	

	private void importUserRoutes(MultipartFile excelFile) throws InvalidFormatException, IOException{
		Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		int rowCount = 0;
		DataFormatter dataFormatter = new DataFormatter();
		UserRoute userRoute = null;
		for (Row row : sheet) {

			if(rowCount == 0){
				rowCount++;
				continue;
			}
			String routeName = null;
			Cell routeCell = row.getCell(0);
			if(routeCell!= null)
				 routeName = routeCell.getStringCellValue();
				
			Cell deliveryBoyCell = row.getCell(1);
			String deliveryBoyNumber= dataFormatter.formatCellValue(deliveryBoyCell);
			
			String collectionBoyNumber = null;
			
			if(null != row.getCell(2)){
				collectionBoyNumber = dataFormatter.formatCellValue(row.getCell(2));
			}
			
			userRoute = userRouteService.getUserRouteByRouteName(routeName, false, null, false, false);
			if(null==userRoute){
				userRoute = new UserRoute();
			}
			
			Route route =  routeService.getRouteByName(routeName, false, userService.getLoggedInUser(), false, true);
			if(null == route){
				continue;
			}
			
			User deliveryBoy = userService.getUserByMobileNumber(deliveryBoyNumber, false, null, false, false);
			if(null == deliveryBoy){
				continue;
			}
			
			if(null != collectionBoyNumber){
				User collectionBoy = userService.getUserByMobileNumber(collectionBoyNumber, false, null, false, false);
				if(null != collectionBoy){
					userRoute.setCollectionBoy(collectionBoy);
				}
			}
			
			userRoute.setRoute(route);
			userRoute.setDeliveryBoy(deliveryBoy);
			 
			BindingResult bindingResult = new DataBinder(userRoute).getBindingResult();
			userRouteValidator.validate(userRoute, bindingResult);
			if(bindingResult.hasErrors()) {
				continue;
			}
	
			userRouteService.createUserRoute(userRoute, userService.getLoggedInUser(), false, false);
			rowCount++;
		}
	}
	
	/*
	 * Ajax Method
	 * 
	 */
	

	/**
	 * REST API Methods
	 */

	/**
	 * End REST API Methods
	 */
	
	/**
	 * @author Ritu and Reshma
	 * @param routeCode
	 * @param collectionBoyCode
	 * @return
	 * 
	 * Used from device - Internal App
	 * USED_FROM_DEVICE
	 * 
	 */
	@RequestMapping(value="/restapi/userRoute/sendNotefication", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse  selectCollectionBoyforRoute(@RequestParam(required=true) List<String> mobileNumbers){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			
		   User collectionBoy =userService.getLoggedInUser();
		   firebaseWrapper.sendOnTheWayCollectionNotification(collectionBoy, mobileNumbers);	
		   for(String mobileNumber:mobileNumbers){
			   User customer = userService.getUserByMobileNumber(mobileNumber, true, null, false, false);
			  
			   plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear Provilac Customer we have scheduled your payment Collection for "+DateHelper.getFormattedDate(new Date()) +". Kindly ask for a reciept");
		   }
		   
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		
		 return apiResponse;
	}
	/**
	 * End REST API Methods
	 */
	
}
