package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.DeliveryScheduleDTO;
import com.vishwakarma.provilac.dto.InvoiceDTO;
import com.vishwakarma.provilac.dto.InvoiceLineItemDTO;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.dto.RouteDTO;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Invoice.CollectionStatus;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionRequest;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;
import com.vishwakarma.provilac.mvc.validator.InvoiceValidator;
import com.vishwakarma.provilac.property.editor.PaymentPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.PaymentRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionRequestService;
import com.vishwakarma.provilac.service.SummarySheetRecordService;
import com.vishwakarma.provilac.service.SummarySheetService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;
import com.vishwakarma.provilac.utils.ScheduledJobs;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class InvoiceController {

	@Resource
	private InvoiceValidator invoiceValidator;

	@Resource
	private InvoiceService invoiceService;

	@Resource
	private UserService userService;

	@Resource
	private UserRepository userRepository;

	@Resource
	private PaymentRepository paymentRepository;

	@Resource
	PaymentService paymentService;

	@Resource
	private RouteService routeService;

	@Resource
	private DeliveryScheduleService deliveryScheduleService;

	@Resource
	private SummarySheetService summarySheetService;

	@Resource
	private SummarySheetRecordService summarySheetRecordService;

	@Resource
	private ScheduledJobs scheduledJobs;

	@Resource
	private NotificationHelper notificationHelper;

	@Resource
	private UserRouteService userRouteService;

	@Resource
	private ProductService productService;

	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;

	@Resource
	private RouteRecordService routeRecordService;

	@Resource
	private ServletContext servletContext;

	@Resource
	private UserSubscriptionService userSubscriptionService;

	@Resource
	private SubscriptionRequestService subscriptionRequestService;

	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private Environment environment;
	
	@Resource
	private AsyncJobs asyncJobs;

	@InitBinder(value = "invoice")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Payment.class, new PaymentPropertyEditor(paymentRepository));
		binder.setValidator(invoiceValidator);
	}

	private Model populateModelForAdd(Model model, Invoice invoice) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("invoice", invoice);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("payments", paymentService.getAllPayments(loggedInUser, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/invoice/add")
	public String addInvoice(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT) || loggedInUser.hasAnyRole(Role.ROLE_INTERN) || loggedInUser.hasAnyRole(Role.ROLE_CCE)) {
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Invoice());
		return "admin-pages/invoice/add";
	}

	@RequestMapping(value = "/admin/invoice/add", method = RequestMethod.POST)
	public String invoiceAdd(Invoice invoice, BindingResult formBinding, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		invoiceValidator.validate(invoice, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, invoice);
			return "admin-pages/invoice/add";
		}
		invoiceService.createInvoice(invoice, loggedInUser, true, false);
		String message = "Invoice added successfully";
		return "redirect:/admin/invoice/list?message=" + message;
	}

	@RequestMapping(value = "/admin/invoice/list")
	public String getInvoiceList(Model model, @RequestParam(defaultValue = "1") Integer pageNumber, @RequestParam(required = false) String message, @RequestParam(required = false) String searchTerm, @RequestParam(required = false) String userCode) {

		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN)) {
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Invoice> page = null;
		User user = userService.getLoggedInUser();
		if (null != userCode) {
			page = invoiceService.getInvoicesByCustomer(userCode, pageNumber, user, false, false);
		} else {

			if (StringUtils.isNotBlank(searchTerm)) {
				page = invoiceService.searchInvoice(pageNumber, searchTerm, user, true, false);
			} else {
				page = invoiceService.getInvoices(pageNumber, user, true, false);
			}
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("invoices", page.getContent());

		return "/admin-pages/invoice/list";
	}

	@RequestMapping(value = "/admin/invoice/show/{cipher}")
	public String showInvoice(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN) || loggedInUser.hasAnyRole(Role.ROLE_CCE)) {
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Invoice invoice = invoiceService.getInvoice(id, true, loggedIUser, true, false);
		model.addAttribute("invoice", invoice);
		return "/admin-pages/invoice/show";
	}

	@RequestMapping(value = "/admin/invoice/update/{cipher}")
	public String updateInvoice(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN) || loggedInUser.hasAnyRole(Role.ROLE_CCE)) {
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Invoice invoice = invoiceService.getInvoice(id, true, loggedInUser, true, false);
		model = populateModelForAdd(model, invoice);
		model.addAttribute("id", invoice.getId());
		model.addAttribute("version", invoice.getVersion());
		return "/admin-pages/invoice/update";
	}

	@RequestMapping(value = "/admin/invoice/update", method = RequestMethod.POST)
	public String invoiceUpdate(Invoice invoice, BindingResult formBinding, Model model) {

		invoice.setUpdateOperation(true);
		invoiceValidator.validate(invoice, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, invoice);
			return "admin-pages/invoice/update";
		}
		invoice.setId(Long.parseLong(model.asMap().get("id") + ""));
		invoice.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, true, false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Invoice updated successfully";
		return "redirect:/admin/invoice/list?message=" + message;
	}

	@RequestMapping(value = "/admin/invoice/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN)) {
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			invoiceService.deleteInvoice(id);
			message = "Invoice deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Invoice";
		}
		return "redirect:/admin/invoice/list?message=" + message;
	}

	@RequestMapping(value = "/admin/invoice/generate")
	public void downloadPDF(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN) || loggedInUser.hasAnyRole(Role.ROLE_CCE)) {
			response.sendRedirect("redirect:/admin/denied");
		}

		Boolean isNotifyCustomers = Boolean.parseBoolean(request.getParameter("isNotify"));
		String selectedDate = request.getParameter("date");
		if (null == selectedDate || selectedDate.isEmpty()) {
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}

		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
		Calendar calendar = Calendar.getInstance();
		LocalDate localDate1 = formatter.parseLocalDate("01-" + selectedDate);
		calendar.setTime(new Date());

		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		Date fromDate = localDate1.toDate();
		calendar.setTime(fromDate);
		Date toDate = null;

		if (month == calendar.get(Calendar.MONTH) && year == calendar.get(Calendar.YEAR) && calendar.getActualMaximum(Calendar.DAY_OF_MONTH) != day) {
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}

		final ServletContext servletContext = request.getSession().getServletContext();
		final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		final String temperotyFilePath = tempDirectory.getAbsolutePath();
		Font TIME_ROMAN_NORMAL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

		String fileName = "Invoice.pdf";
		
		String paymentBaseUrl = environment.getProperty("server.url");
		if(paymentBaseUrl.endsWith("/")) {
			paymentBaseUrl = paymentBaseUrl.substring(0, paymentBaseUrl.length() - 1);
		}
		paymentBaseUrl = paymentBaseUrl + AppConstants.WEB_PAYMENT_URL + "?" + AppConstants.WEB_PAYMENT_PARAM_NAME + "=";

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);

		List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordService.getAllDeletedRouteRecords(null, false, false);
		Map<String, Route> mapRouteRecords = new HashMap<String, Route>();

		for (DeletedRouteRecord deletedRouteRecord : deletedRouteRecords) {
			mapRouteRecords.put(deletedRouteRecord.getCustomer().getMobileNumber(), deletedRouteRecord.getRoute());
		}

		try {
			Document document = null;
			try {
				document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(temperotyFilePath + "\\" + fileName));
				document.open();
				LocalDate newDate = localDate1.withDayOfMonth(localDate1.dayOfMonth().getMaximumValue());
				toDate = newDate.toDate();
				calendar.setTime(fromDate);
				String toMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

				List<LocalDate> localDates = new ArrayList<LocalDate>();
				int days = Days.daysBetween(LocalDate.fromDateFields(fromDate), LocalDate.fromDateFields(toDate)).getDays() + 1;

				for (int i = 0; i < days; i++) {
					LocalDate d = LocalDate.fromDateFields(fromDate).withFieldAdded(DurationFieldType.days(), i);
					localDates.add(d);
				}

				List<Route> routes = routeService.getAllRoutes(loggedInUser, false, true);
				Paragraph preface = new Paragraph();

				Invoice invoice = null;
				Map<Product, Integer> productQuantity = new HashMap<Product, Integer>();
				Map<Product, Double> productTotalAmount = new HashMap<Product, Double>();
				List<PdfPCell> quantityCellList = null;
				Map<Product, List<PdfPCell>> productQuantityMap = new HashMap<Product, List<PdfPCell>>();

				double totalAmount = 0;
				double previousMonthDue = 0;
				PdfPTable table1 = null;
				PdfPTable table11 = null;
				PdfPTable table21 = null;
				PdfPTable tableValue1 = null;
				PdfPTable tableValue11 = null;
				PdfPTable tableValue21 = null;
				PdfPTable productTotalTable = null;
				PdfPCell cell = null;
				boolean isExist = false;
				boolean isAlreadyTableCreated = false;
				boolean isDeliveryScheduleExistForAll = false;
				boolean isInvoiceExist = true;
				User customer = null;

				int count = 1;
				for (LocalDate date : localDates) {
					if (count >= 21) {
						if (count == 21) {
							table21 = new PdfPTable(12 - (31 - days));
							table21 = generateTableIndex(table21, "Days", false);
						}
						table21 = generateTableIndex(table21, date.getDayOfMonth() + "", false);

					} else if (count >= 11 && count <= 20) {
						if (count == 11) {
							table11 = new PdfPTable(11);
							table11 = generateTableIndex(table11, "Days", false);
						}
						table11 = generateTableIndex(table11, date.getDayOfMonth() + "", false);
					} else {
						if (count == 1) {
							table1 = new PdfPTable(11);
							table1 = generateTableIndex(table1, "Days", false);
						}
						table1 = generateTableIndex(table1, date.getDayOfMonth() + "", false);
					}
					count++;
				}

				for (Route route : routes) {
					Set<RouteRecord> routeRecords = route.getRouteRecords();
					for (RouteRecord routeRecord : routeRecords) {
						List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(routeRecord.getCustomer().getId(), fromDate, toDate, SubscriptionType.Postpaid,loggedInUser, false, true);
						customer = userService.getUser(routeRecord.getCustomer().getId(), false, loggedInUser, false, false);
						if (null != deliverySchedules && !deliverySchedules.isEmpty()) {
							isInvoiceExist = true;
							//TODO: Remove SingleInvoicePDF
							Document singleInvoicePDF = new Document();
							String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
							PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
							singleInvoicePDF.open();

							productQuantity = new HashMap<Product, Integer>();
							productTotalAmount = new HashMap<Product, Double>();
							productQuantityMap = new HashMap<Product, List<PdfPCell>>();

							preface = new Paragraph();
							isDeliveryScheduleExistForAll = true;

							invoice = invoiceService.getInvoiceByCustomerAndDateRange(routeRecord.getCustomer().getId(), fromDate, toDate, loggedInUser, false, false);
							if (null == invoice) {
								isInvoiceExist = false;
								invoice = new Invoice();
								invoice.setCustomer(routeRecord.getCustomer());
								invoice.setDeliverySchedule(new HashSet<DeliverySchedule>(deliverySchedules));
								invoice.setFromDate(fromDate);
								invoice.setToDate(toDate);

							}

							document = getPDFHeader(document);
							singleInvoicePDF = getPDFHeader(singleInvoicePDF);

							preface.add(new Paragraph("CUSTOMER ID		" + routeRecord.getCustomer().getCode(), TIME_ROMAN_NORMAL));
							preface.add(new Paragraph("Bill To:			" + routeRecord.getCustomer().getFirstName() + " " + routeRecord.getCustomer().getLastName(), TIME_ROMAN_NORMAL));
							preface.add(new Paragraph("Bill Address:		" + routeRecord.getCustomer().getBuildingAddress(), TIME_ROMAN_NORMAL));

							creteEmptyLine(preface, 1);

							Paragraph routeParagraph = new Paragraph(route.getName(), TIME_ROMAN_NORMAL);
							Chunk alignmentClue = new Chunk(new VerticalPositionMark());
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

							preface.add(routeParagraph);

							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							for (DeliverySchedule deliverySchedule : deliverySchedules) {
								for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {

									if (!productQuantity.containsKey(deliveryLineItem.getProduct())) {
										if (productQuantity.isEmpty()) {
											productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
										} else {
											productQuantity.put(deliveryLineItem.getProduct(), (productQuantity.get(deliveryLineItem.getProduct()) != null ? productQuantity.get(deliveryLineItem.getProduct()) : 0) + deliveryLineItem.getQuantity());
										}
										productTotalAmount.put(deliveryLineItem.getProduct(), deliveryLineItem.getProduct().getPrice());
									} else {
										totalAmount = productTotalAmount.get(deliveryLineItem.getProduct());
										totalAmount += deliveryLineItem.getProduct().getPrice();
										productTotalAmount.put(deliveryLineItem.getProduct(), totalAmount);
										productQuantity.put(deliveryLineItem.getProduct(), productQuantity.get(deliveryLineItem.getProduct()) + deliveryLineItem.getQuantity());
									}
								}
							}

							Paragraph paragraph = new Paragraph();
							creteEmptyLine(paragraph, 1);

							document.add(paragraph);
							singleInvoicePDF.add(paragraph);

							for (Entry<Product, Integer> map : productQuantity.entrySet()) {
								count = 1;
								quantityCellList = new ArrayList<PdfPCell>();
								for (LocalDate localDate : localDates) {
									for (DeliverySchedule deliverySchedule : deliverySchedules) {
										calendar.setTime(deliverySchedule.getDate());
										for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
											if (localDate.getDayOfMonth() == calendar.get(Calendar.DAY_OF_MONTH) && localDate.getMonthOfYear() == calendar.get(Calendar.MONTH) + 1) {
												if (deliveryLineItem.getProduct().equals(map.getKey())) {
													cell = generateTableCell(deliveryLineItem.getQuantity() + "", true);
													quantityCellList.add(cell);
													isExist = true;
												}
											}
										}
									}

									if (!isExist) {
										cell = generateTableCell("-", true);
										quantityCellList.add(cell);
									}
									isExist = false;

									if (count >= 21) {
										if (count == 21) {
											if (!isAlreadyTableCreated) {
												tableValue21 = new PdfPTable(12 - (31 - days));
												isAlreadyTableCreated = true;
											}
										}
									} else if (count >= 11 && count <= 20) {
										if (count == 11) {
											if (!isAlreadyTableCreated) {
												tableValue11 = new PdfPTable(11);
											}
										}
									} else {
										if (count == 1) {
											if (!isAlreadyTableCreated) {
												tableValue1 = new PdfPTable(11);
											}
										}
									}
									count++;
								}

								productQuantityMap.put(map.getKey(), quantityCellList);
							}

							for (Entry<Product, List<PdfPCell>> map : productQuantityMap.entrySet()) {
								count = 1;

								for (PdfPCell tableCell : map.getValue()) {
									if (count == 21) {
										cell = generateTableCell(map.getKey().getName(), false);
										tableValue21.addCell(cell);
									} else if (count == 1) {
										cell = generateTableCell(map.getKey().getName(), false);
										tableValue1.addCell(cell);
									} else if (count == 11) {
										cell = generateTableCell(map.getKey().getName(), false);
										tableValue11.addCell(cell);
									}
									if (count >= 21) {
										tableValue21.addCell(tableCell);
									} else if (count >= 11 && count <= 20) {
										tableValue11.addCell(tableCell);
									} else {
										tableValue1.addCell(tableCell);
									}
									count++;
								}
							}

							table1.setWidthPercentage(100);
							tableValue1.setWidthPercentage(100);
							table11.setWidthPercentage(100);
							tableValue11.setWidthPercentage(100);
							table21.setWidthPercentage(100);
							tableValue21.setWidthPercentage(100);

							document.add(table1);
							document.add(tableValue1);
							document.add(paragraph);
							document.add(table11);
							document.add(tableValue11);
							document.add(paragraph);
							document.add(table21);
							document.add(tableValue21);
							document.add(paragraph);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							singleInvoicePDF.add(table1);
							singleInvoicePDF.add(tableValue1);
							singleInvoicePDF.add(paragraph);
							singleInvoicePDF.add(table11);
							singleInvoicePDF.add(tableValue11);
							singleInvoicePDF.add(paragraph);
							singleInvoicePDF.add(table21);
							singleInvoicePDF.add(tableValue21);
							singleInvoicePDF.add(paragraph);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							isAlreadyTableCreated = false;
							productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
							productTotalTable = generateTableIndex(productTotalTable, "Description", false);
							for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
								productTotalTable = generateTableIndex(productTotalTable, "Total Quantity(" + entry.getKey().getName() + ")", true);
								productTotalTable = generateTableIndex(productTotalTable, "Unit Price(" + entry.getKey().getName() + ")", true);
							}
							productTotalTable = generateTableIndex(productTotalTable, "Total", false);
							productTotalTable.setWidthPercentage(100);

							document.add(productTotalTable);
							singleInvoicePDF.add(productTotalTable);

							productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
							productTotalTable = generateTableIndex(productTotalTable, "Provilac Milk", false);
							totalAmount = 0;
							for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
								productTotalTable = generateTableIndex(productTotalTable, productQuantity.get(entry.getKey()) + "", true);
								productTotalTable = generateTableIndex(productTotalTable, entry.getKey().getPrice() + "", true);
								totalAmount += entry.getKey().getPrice() * productQuantity.get(entry.getKey());
							}

							previousMonthDue = customer.getLastPendingDues();
							if (null == invoice.getCode()) {
								customer.setLastPendingDues(previousMonthDue + totalAmount);
								userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
								invoice.setLastPendingDues(previousMonthDue);
								invoice.setTotalAmount(totalAmount);
								invoice.setCollectionStatus(CollectionStatus.Not_Collected);
								invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);

								/*
								 * fetch all delivery schedule by from date and
								 * to date set thouse deliveryschedule to
								 * genrated invoice
								 */
								List<DeliverySchedule> listOfDeliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(invoice.getCustomer().getId(), invoice.getFromDate(), invoice.getToDate(), loggedInUser, true, true, SubscriptionType.Postpaid, Type.Normal, Type.Paid_Trial);
								for (DeliverySchedule deliverySchedule : listOfDeliverySchedules) {
									if(deliverySchedule.getType() == Type.Free_Trial) {
										continue;
									}
									deliverySchedule.setInvoice(invoice);
									deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser, true, true);
								}

								if (isNotifyCustomers == true) {
									if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
										plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac Milk bill of INR " + (totalAmount + previousMonthDue) + " is due. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
									}
								}
							} else {
								if (isNotifyCustomers == true) {
									if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
										plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac Milk bill of INR " + previousMonthDue + " is due. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
									}
								}
							}

							productTotalTable = generateTableIndex(productTotalTable, totalAmount + "", false);
							productTotalTable.setWidthPercentage(100);
							document.add(productTotalTable);
							singleInvoicePDF.add(productTotalTable);

							preface = new Paragraph();
							creteEmptyLine(preface, 1);

							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Subtotal:	" + totalAmount, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							if (isInvoiceExist) {
								if (previousMonthDue < 0) {
									routeParagraph.add(new Paragraph("Previous Month:	" + (totalAmount + previousMonthDue), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								} else {
									routeParagraph.add(new Paragraph("Previous Month:	" + (Math.abs(totalAmount - previousMonthDue)), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								}
							} else {
								routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							}

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							if (isInvoiceExist) {
								routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							} else {

								// ////////////////////////////Changes made by Vishal//////////////////////////////
								if (previousMonthDue > 0) {
									routeParagraph.add(new Paragraph("Balance Due:	" + (previousMonthDue + totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								} else {
									double negatedPrevious = previousMonthDue * -1;
									if (negatedPrevious >= totalAmount) {
										routeParagraph.add(new Paragraph("Balance Due:	-" + (negatedPrevious - totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
									} else {
										routeParagraph.add(new Paragraph("Balance Due:	" + (totalAmount - negatedPrevious), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
									}
								}
								// ////////////////////////////Changes made by Vishal//////////////////////////////
							}

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							preface = new Paragraph();
							creteEmptyLine(preface, 1);
							routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							preface.add(routeParagraph);

							document.add(preface);
							document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							singleInvoicePDF.close();
							if (isNotifyCustomers == true) {
								notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
							}
							document.newPage();
						} else {
							boolean isPrepaidCustomer = false;
							List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, false);
							for (UserSubscription userSubscription : subscriptions) {
								if(userSubscription.getSubscriptionType() == SubscriptionType.Prepaid) {
									isPrepaidCustomer = true;
								}
							}
							
							if (customer.getLastPendingDues() != 0 && !isPrepaidCustomer) {
								Document singleInvoicePDF = new Document();
								String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
								PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
								singleInvoicePDF.open();

								preface = new Paragraph();
								isDeliveryScheduleExistForAll = true;

								invoice = invoiceService.getInvoiceByCustomerAndDateRange(routeRecord.getCustomer().getId(), fromDate, toDate, loggedInUser, false, false);
								if (null == invoice) {
									invoice = new Invoice();
									invoice.setCustomer(routeRecord.getCustomer());
									invoice.setFromDate(fromDate);
									invoice.setToDate(toDate);
									invoice.setTotalAmount(customer.getLastPendingDues());
									invoice.setLastPendingDues(customer.getLastPendingDues());
									invoice.setCollectionStatus(CollectionStatus.Not_Collected);
									invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);
								}

								document = getPDFHeader(document);
								singleInvoicePDF = getPDFHeader(singleInvoicePDF);

								preface.add(new Paragraph("CUSTOMER ID		" + routeRecord.getCustomer().getCode(), TIME_ROMAN_NORMAL));
								preface.add(new Paragraph("Bill To:			" + routeRecord.getCustomer().getFirstName() + " " + routeRecord.getCustomer().getLastName(), TIME_ROMAN_NORMAL));
								preface.add(new Paragraph("Bill Address:		" + routeRecord.getCustomer().getBuildingAddress(), TIME_ROMAN_NORMAL));

								creteEmptyLine(preface, 1);

								Paragraph routeParagraph = new Paragraph(route.getName(), TIME_ROMAN_NORMAL);
								Chunk alignmentClue = new Chunk(new VerticalPositionMark());
								routeParagraph.add(alignmentClue);
								routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

								preface.add(routeParagraph);

								document.add(preface);
								document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

								singleInvoicePDF.add(preface);
								singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

								previousMonthDue = customer.getLastPendingDues();
								document.add(new Paragraph("No Orders for this month", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL)));
								preface = new Paragraph();
								creteEmptyLine(preface, 1);

								document.add(preface);
								document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
								singleInvoicePDF.add(preface);
								singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

								//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
								routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
								routeParagraph.add(alignmentClue);
								routeParagraph.add(new Paragraph("Subtotal:	0", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

								document.add(routeParagraph);
								singleInvoicePDF.add(routeParagraph);

								//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
								routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
								routeParagraph.add(alignmentClue);
								routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

								document.add(routeParagraph);
								singleInvoicePDF.add(routeParagraph);

								//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
								routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
								routeParagraph.add(alignmentClue);
								routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

								document.add(routeParagraph);
								singleInvoicePDF.add(routeParagraph);

								//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
								//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
								document.add(preface);
								document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
								document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
								document.add(preface);
								document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

								singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
								singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
								singleInvoicePDF.add(preface);
								singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
								singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
								singleInvoicePDF.add(preface);
								singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

								preface = new Paragraph();
								creteEmptyLine(preface, 1);
								routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
								routeParagraph.add(alignmentClue);
								routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								preface.add(routeParagraph);

								document.add(preface);
								document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								singleInvoicePDF.add(preface);
								singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

								singleInvoicePDF.close();
								if (isNotifyCustomers == true) {
									notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
									if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
										plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Provilac Milk bill for " + toMonth + "-" + year + " of Rs." + previousMonthDue + " of " + customer.getFullName() + " is Due on " + toMonth + "-" + year + ".Pay your bill at earliest. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
									}
								}
								document.newPage();
							}
						}
					}
				}

				List<User> users = userService.getAllUsersByIsAssignedRoute(false, false, false);
				for (User user : users) {

					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(user.getId(), fromDate, toDate, SubscriptionType.Postpaid,loggedInUser, false, true);
					customer = user;
					if (null != deliverySchedules && !deliverySchedules.isEmpty()) {
						isInvoiceExist = true;
						Document singleInvoicePDF = new Document();
						String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
						PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
						singleInvoicePDF.open();

						productQuantity = new HashMap<Product, Integer>();
						productTotalAmount = new HashMap<Product, Double>();
						productQuantityMap = new HashMap<Product, List<PdfPCell>>();

						preface = new Paragraph();
						isDeliveryScheduleExistForAll = true;

						invoice = invoiceService.getInvoiceByCustomerAndDateRange(user.getId(), fromDate, toDate, loggedInUser, false, false);
						if (null == invoice) {
							isInvoiceExist = false;
							invoice = new Invoice();
							invoice.setCustomer(user);
							invoice.setDeliverySchedule(new HashSet<DeliverySchedule>(deliverySchedules));
							invoice.setFromDate(fromDate);
							invoice.setToDate(toDate);
						}

						document = getPDFHeader(document);
						singleInvoicePDF = getPDFHeader(singleInvoicePDF);

						preface.add(new Paragraph("CUSTOMER ID		" + user.getCode(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill To:			" + user.getFirstName() + " " + user.getLastName(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill Address:		" + user.getBuildingAddress(), TIME_ROMAN_NORMAL));

						creteEmptyLine(preface, 1);

						Paragraph routeParagraph;
						if (mapRouteRecords.containsKey(user.getMobileNumber())) {
							routeParagraph = new Paragraph(mapRouteRecords.get(user.getMobileNumber()).getName(), TIME_ROMAN_NORMAL);
						} else {
							routeParagraph = new Paragraph("No Route Assign", TIME_ROMAN_NORMAL);
						}
						Chunk alignmentClue = new Chunk(new VerticalPositionMark());
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

						preface.add(routeParagraph);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						for (DeliverySchedule deliverySchedule : deliverySchedules) {
							for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {

								if (!productQuantity.containsKey(deliveryLineItem.getProduct())) {
									if (productQuantity.isEmpty()) {
										productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
									} else {
										productQuantity.put(deliveryLineItem.getProduct(), (productQuantity.get(deliveryLineItem.getProduct()) != null ? productQuantity.get(deliveryLineItem.getProduct()) : 0) + deliveryLineItem.getQuantity());
									}
									productTotalAmount.put(deliveryLineItem.getProduct(), deliveryLineItem.getProduct().getPrice());
								} else {
									totalAmount = productTotalAmount.get(deliveryLineItem.getProduct());
									totalAmount += deliveryLineItem.getProduct().getPrice();
									productTotalAmount.put(deliveryLineItem.getProduct(), totalAmount);
									productQuantity.put(deliveryLineItem.getProduct(), productQuantity.get(deliveryLineItem.getProduct()) + deliveryLineItem.getQuantity());
								}
							}
						}

						Paragraph paragraph = new Paragraph();
						creteEmptyLine(paragraph, 1);

						document.add(paragraph);
						singleInvoicePDF.add(paragraph);

						for (Entry<Product, Integer> map : productQuantity.entrySet()) {
							count = 1;
							quantityCellList = new ArrayList<PdfPCell>();
							for (LocalDate localDate : localDates) {
								for (DeliverySchedule deliverySchedule : deliverySchedules) {
									calendar.setTime(deliverySchedule.getDate());
									for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
										if (localDate.getDayOfMonth() == calendar.get(Calendar.DAY_OF_MONTH) && localDate.getMonthOfYear() == calendar.get(Calendar.MONTH) + 1) {
											if (deliveryLineItem.getProduct().equals(map.getKey())) {
												cell = generateTableCell(deliveryLineItem.getQuantity() + "", true);
												quantityCellList.add(cell);
												isExist = true;
											}
										}
									}
								}

								if (!isExist) {
									cell = generateTableCell("-", true);
									quantityCellList.add(cell);
								}
								isExist = false;

								if (count >= 21) {
									if (count == 21) {
										if (!isAlreadyTableCreated) {
											tableValue21 = new PdfPTable(12 - (31 - days));
											isAlreadyTableCreated = true;
										}
									}
								} else if (count >= 11 && count <= 20) {
									if (count == 11) {
										if (!isAlreadyTableCreated) {
											tableValue11 = new PdfPTable(11);
										}
									}
								} else {
									if (count == 1) {
										if (!isAlreadyTableCreated) {
											tableValue1 = new PdfPTable(11);
										}
									}
								}
								count++;
							}

							productQuantityMap.put(map.getKey(), quantityCellList);
						}

						for (Entry<Product, List<PdfPCell>> map : productQuantityMap.entrySet()) {
							count = 1;

							for (PdfPCell tableCell : map.getValue()) {
								if (count == 21) {
									cell = generateTableCell(map.getKey().getName(), false);
									tableValue21.addCell(cell);
								} else if (count == 1) {
									cell = generateTableCell(map.getKey().getName(), false);
									tableValue1.addCell(cell);
								} else if (count == 11) {
									cell = generateTableCell(map.getKey().getName(), false);
									tableValue11.addCell(cell);
								}
								if (count >= 21) {
									tableValue21.addCell(tableCell);
								} else if (count >= 11 && count <= 20) {
									tableValue11.addCell(tableCell);
								} else {
									tableValue1.addCell(tableCell);
								}
								count++;
							}
						}

						table1.setWidthPercentage(100);
						tableValue1.setWidthPercentage(100);
						table11.setWidthPercentage(100);
						tableValue11.setWidthPercentage(100);
						table21.setWidthPercentage(100);
						tableValue21.setWidthPercentage(100);

						document.add(table1);
						document.add(tableValue1);
						document.add(paragraph);
						document.add(table11);
						document.add(tableValue11);
						document.add(paragraph);
						document.add(table21);
						document.add(tableValue21);
						document.add(paragraph);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(table1);
						singleInvoicePDF.add(tableValue1);
						singleInvoicePDF.add(paragraph);
						singleInvoicePDF.add(table11);
						singleInvoicePDF.add(tableValue11);
						singleInvoicePDF.add(paragraph);
						singleInvoicePDF.add(table21);
						singleInvoicePDF.add(tableValue21);
						singleInvoicePDF.add(paragraph);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						isAlreadyTableCreated = false;
						productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
						productTotalTable = generateTableIndex(productTotalTable, "Description", false);
						for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
							productTotalTable = generateTableIndex(productTotalTable, "Total Quantity(" + entry.getKey().getName() + ")", true);
							productTotalTable = generateTableIndex(productTotalTable, "Unit Price(" + entry.getKey().getName() + ")", true);
						}
						productTotalTable = generateTableIndex(productTotalTable, "Total", false);
						productTotalTable.setWidthPercentage(100);

						document.add(productTotalTable);
						singleInvoicePDF.add(productTotalTable);

						productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
						productTotalTable = generateTableIndex(productTotalTable, "Provilac Milk", false);
						totalAmount = 0;
						for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
							productTotalTable = generateTableIndex(productTotalTable, productQuantity.get(entry.getKey()) + "", true);
							productTotalTable = generateTableIndex(productTotalTable, entry.getKey().getPrice() + "", true);
							totalAmount += entry.getKey().getPrice() * productQuantity.get(entry.getKey());
						}

						previousMonthDue = customer.getLastPendingDues();
						if (null == invoice.getCode()) {
							customer.setLastPendingDues(previousMonthDue + totalAmount);
							userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
							invoice.setLastPendingDues(previousMonthDue);
							invoice.setTotalAmount(totalAmount);
							invoice.setCollectionStatus(CollectionStatus.Not_Collected);
							invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);

							/*
							 * fetch all delivery schedule by from date and to
							 * date set thouse deliveryschedule to genrated
							 * invoice
							 */
							List<DeliverySchedule> listOfDeliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(invoice.getCustomer().getId(), invoice.getFromDate(), invoice.getToDate(), loggedInUser, true, true, SubscriptionType.Postpaid, Type.Normal, Type.Paid_Trial);
							for (DeliverySchedule deliverySchedule : listOfDeliverySchedules) {
								if(deliverySchedule.getType() == Type.Free_Trial) {
									continue;
								}
								deliverySchedule.setInvoice(invoice);
								deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser, true, true);
							}

							if (isNotifyCustomers == true) {
								if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
									plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac Milk bill of INR " + (totalAmount + previousMonthDue) + " is due. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
								}
							}
						} else {
							if (isNotifyCustomers == true) {
								if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
									plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac Milk bill of INR " + previousMonthDue + " is due. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
								}
							}
						}

						productTotalTable = generateTableIndex(productTotalTable, totalAmount + "", false);
						productTotalTable.setWidthPercentage(100);
						document.add(productTotalTable);
						singleInvoicePDF.add(productTotalTable);

						preface = new Paragraph();
						creteEmptyLine(preface, 1);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Subtotal:	" + totalAmount, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						if (isInvoiceExist) {
							if (previousMonthDue < 0) {

								routeParagraph.add(new Paragraph("Previous Month:	" + (totalAmount + previousMonthDue), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							} else {
								routeParagraph.add(new Paragraph("Previous Month:	" + (Math.abs(totalAmount - previousMonthDue)), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							}
						} else {
							routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						}

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						if (isInvoiceExist) {
							routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						} else {

							// ////////////////////////////Changes made by Vishal//////////////////////////////
							if (previousMonthDue > 0) {
								routeParagraph.add(new Paragraph("Balance Due:	" + (previousMonthDue + totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							} else {
								double negatedPrevious = previousMonthDue * -1;
								if (negatedPrevious >= totalAmount) {
									routeParagraph.add(new Paragraph("Balance Due:	-" + (negatedPrevious - totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								} else {
									routeParagraph.add(new Paragraph("Balance Due:	" + (totalAmount - negatedPrevious), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
								}
							}
							// ////////////////////////////Changes made by Vishal//////////////////////////////
						}

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						preface = new Paragraph();
						creteEmptyLine(preface, 1);
						routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						preface.add(routeParagraph);

						document.add(preface);
						document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						singleInvoicePDF.close();
						if (isNotifyCustomers == true) {
							notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
						}
						document.newPage();
					} else {
						boolean isPrepaidCustomer = false;
						List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, false);
						for (UserSubscription userSubscription : subscriptions) {
							if(userSubscription.getSubscriptionType() == SubscriptionType.Prepaid) {
								isPrepaidCustomer = true;
							}
						}
						
						if (customer.getLastPendingDues() != 0 && !isPrepaidCustomer) {
							Document singleInvoicePDF = new Document();
							String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
							PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
							singleInvoicePDF.open();

							preface = new Paragraph();
							isDeliveryScheduleExistForAll = true;

							invoice = invoiceService.getInvoiceByCustomerAndDateRange(user.getId(), fromDate, toDate, loggedInUser, false, false);
							if (null == invoice) {
								invoice = new Invoice();
								invoice.setCustomer(user);
								invoice.setFromDate(fromDate);
								invoice.setToDate(toDate);
								invoice.setTotalAmount(customer.getLastPendingDues());
								invoice.setLastPendingDues(customer.getLastPendingDues());
								invoice.setCollectionStatus(CollectionStatus.Not_Collected);
								invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);
							}

							document = getPDFHeader(document);
							singleInvoicePDF = getPDFHeader(singleInvoicePDF);

							preface.add(new Paragraph("CUSTOMER ID		" + user.getCode(), TIME_ROMAN_NORMAL));
							preface.add(new Paragraph("Bill To:			" + user.getFirstName() + " " + user.getLastName(), TIME_ROMAN_NORMAL));
							preface.add(new Paragraph("Bill Address:		" + user.getBuildingAddress(), TIME_ROMAN_NORMAL));

							creteEmptyLine(preface, 1);

							Paragraph routeParagraph;
							if (mapRouteRecords.containsKey(user.getMobileNumber())) {
								routeParagraph = new Paragraph(mapRouteRecords.get(user.getMobileNumber()).getName(), TIME_ROMAN_NORMAL);
							} else {
								routeParagraph = new Paragraph("No Route Assign", TIME_ROMAN_NORMAL);
							}

							Chunk alignmentClue = new Chunk(new VerticalPositionMark());
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

							preface.add(routeParagraph);

							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							previousMonthDue = customer.getLastPendingDues();
							document.add(new Paragraph("No Orders for this month", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL)));
							preface = new Paragraph();
							creteEmptyLine(preface, 1);

							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Subtotal:	0", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
							routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							document.add(routeParagraph);
							singleInvoicePDF.add(routeParagraph);

							//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
							document.add(preface);
							document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
							singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

							preface = new Paragraph();
							creteEmptyLine(preface, 1);
							routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
							routeParagraph.add(alignmentClue);
							routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							preface.add(routeParagraph);

							document.add(preface);
							document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
							singleInvoicePDF.add(preface);
							singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

							singleInvoicePDF.close();
							if (isNotifyCustomers == true) {
								notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
								if (null != customer.getMobileNumber() || !customer.getMobileNumber().isEmpty()) {
									plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac Milk bill of INR " + previousMonthDue + " is due. To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
								}
							}
							document.newPage();
						}
					}
				}
				if (!isDeliveryScheduleExistForAll) {
					document = getPDFHeader(document);
					preface = new Paragraph();
					creteEmptyLine(preface, 2);
					document.add(new Paragraph("No Invoices Found", new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD)));
				}
				document.close();

			} catch (Exception e) {
				e.printStackTrace();
				response.sendRedirect("redirect:/admin/invoice/list");
				return;
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FileInputStream inputStream = new FileInputStream(temperotyFilePath + "\\" + fileName);
			OutputStream os = response.getOutputStream();
			FileCopyUtils.copy(inputStream, os);
			inputStream.close();
			baos.writeTo(os);
			os.flush();
			asyncJobs.genrateSummarySheet(fromDate, toDate);
		} catch (Exception e1) {
			e1.printStackTrace();
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}
	}

	private PdfPTable generateTableIndex(PdfPTable pdfPTable, String value, boolean isBorderRequired) {
		PdfPCell c1 = new PdfPCell(new Phrase(value));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		if (isBorderRequired) {
			c1.setBorder(Rectangle.BOX);
		} else {
			c1.setBorder(Rectangle.NO_BORDER);
		}
		c1.setBorderWidth(2);
		pdfPTable.addCell(c1);
		return pdfPTable;

	}

	private PdfPCell generateTableCell(String value, boolean isBorderRequired) {
		PdfPCell c1 = new PdfPCell(new Phrase(value));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		if (isBorderRequired) {
			c1.setBorder(Rectangle.BOX);
		} else {
			c1.setBorder(Rectangle.NO_BORDER);
		}
		c1.setBorderWidth(2);
		return c1;

	}

	private static void creteEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private Document getPDFHeader(Document document) {
		try {
			Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
			// Add Meta Data
			document.addTitle("Invoice");
			document.addSubject("Provilac Invoice");
			document.addAuthor("Vishwakarma Technologies Pvt. Ltd.");
			document.addCreator("Harshal Patil");

			// Add Page Title
			Paragraph preface = new Paragraph();
			creteEmptyLine(preface, 1);
			preface.add(new Paragraph("PROVILAC DAIRY FARMS PVT.LTD.", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, BaseColor.CYAN)));

			org.springframework.core.io.Resource resource = new ClassPathResource("provilac_logo.png");
			Image image = Image.getInstance(FileCopyUtils.copyToByteArray(resource.getInputStream()));
			image.setAbsolutePosition(450f, 650f);
			image.scaleAbsolute(100, 131);
			preface.add(image);

			creteEmptyLine(preface, 1);
			preface.add(new Paragraph("Office add:425/75,TMV Colony,Mukundnagar,Pune 411037", TIME_ROMAN_SMALL));
			preface.add(new Paragraph("Phone: 0 8411864646,0 9923075122", TIME_ROMAN_SMALL));
			preface.add(new Paragraph("customercare@provilac.com,www.provilac.com", TIME_ROMAN_SMALL));

			creteEmptyLine(preface, 1);
			Paragraph customerHeader = new Paragraph("CUSTOMER COPY", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.UNDERLINE));
			customerHeader.setAlignment(Element.ALIGN_CENTER);
			customerHeader.setIndentationLeft(50);

			preface.add(customerHeader);
			creteEmptyLine(preface, 1);
			document.add(preface);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return document;
	}

	@RequestMapping(value = "/admin/invoice/customer")
	public void generateInvoiceForCustomer(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User loggedInUser = userService.getLoggedInUser();
		if (loggedInUser.hasAnyRole(Role.ROLE_INTERN) || loggedInUser.hasAnyRole(Role.ROLE_CCE)) {
			response.sendRedirect("redirect:/admin/denied");
		}

		String selectedDate = request.getParameter("date");
		if (null == selectedDate || selectedDate.isEmpty()) {
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}

		String param = request.getParameter("emailNotification");
		Boolean emailNotification = StringUtils.isNotBlank(param) && (Boolean.parseBoolean(param) || param.equalsIgnoreCase("on"));
		param = request.getParameter("smsNotification");
		Boolean smsNotification = StringUtils.isNotBlank(param) && (Boolean.parseBoolean(param) || param.equalsIgnoreCase("on"));

		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
		Calendar calendar = Calendar.getInstance();
		LocalDate localDate1 = formatter.parseLocalDate("01-" + selectedDate);
		calendar.setTime(new Date());

		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		Date fromDate = localDate1.toDate();
		calendar.setTime(fromDate);
		Date toDate = null;

		if (month == calendar.get(Calendar.MONTH) && year == calendar.get(Calendar.YEAR) && calendar.getActualMaximum(Calendar.DAY_OF_MONTH) != day) {
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}

		final ServletContext servletContext = request.getSession().getServletContext();
		final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		final String temperotyFilePath = tempDirectory.getAbsolutePath();
		Font TIME_ROMAN_NORMAL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

		Long customerId = Long.parseLong(request.getParameter("customerId"));
		User customer = userService.getUser(customerId, false, loggedInUser, false, false);
		String fileName = customer.getMobileNumber() + ".pdf";
		
		String paymentBaseUrl = environment.getProperty("server.url");
		if(paymentBaseUrl.endsWith("/")) {
			paymentBaseUrl = paymentBaseUrl.substring(0, paymentBaseUrl.length() - 1);
		}
		paymentBaseUrl = paymentBaseUrl + AppConstants.WEB_PAYMENT_URL + "?" + AppConstants.WEB_PAYMENT_PARAM_NAME + "=";

		RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customer.getCode(), false, loggedInUser, false, false);
		String routeName = null;
		if (null != routeRecord) {
			routeName = routeRecord.getRoute().getName();
		} else {
			DeletedRouteRecord deletedRouteRecord = deletedRouteRecordService.getDeletedRouteRecord(customerId, false);
			if (null != deletedRouteRecord) {
				routeName = deletedRouteRecord.getRoute().getName();
			} else {
				routeName = "No Route Assigned";
			}
		}

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);

		try {
			Document document = null;
			try {
				document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(temperotyFilePath + "\\" + fileName));
				document.open();
				LocalDate newDate = localDate1.withDayOfMonth(localDate1.dayOfMonth().getMaximumValue());
				toDate = newDate.toDate();
				calendar.setTime(fromDate);
				String toMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

				List<LocalDate> localDates = new ArrayList<LocalDate>();
				int days = Days.daysBetween(LocalDate.fromDateFields(fromDate), LocalDate.fromDateFields(toDate)).getDays() + 1;

				for (int i = 0; i < days; i++) {
					LocalDate d = LocalDate.fromDateFields(fromDate).withFieldAdded(DurationFieldType.days(), i);
					localDates.add(d);
				}

				Paragraph preface = new Paragraph();

				Invoice invoice = null;
				Map<Product, Integer> productQuantity = new HashMap<Product, Integer>();
				Map<Product, Double> productTotalAmount = new HashMap<Product, Double>();
				List<PdfPCell> quantityCellList = null;
				Map<Product, List<PdfPCell>> productQuantityMap = new HashMap<Product, List<PdfPCell>>();

				double totalAmount = 0;
				double previousMonthDue = 0;
				PdfPTable table1 = null;
				PdfPTable table11 = null;
				PdfPTable table21 = null;
				PdfPTable tableValue1 = null;
				PdfPTable tableValue11 = null;
				PdfPTable tableValue21 = null;
				PdfPTable productTotalTable = null;
				PdfPCell cell = null;
				boolean isExist = false;
				boolean isAlreadyTableCreated = false;
				boolean isInvoiceExist = true;

				int count = 1;
				for (LocalDate date : localDates) {
					if (count >= 21) {
						if (count == 21) {
							table21 = new PdfPTable(12 - (31 - days));
							table21 = generateTableIndex(table21, "Days", false);
						}
						table21 = generateTableIndex(table21, date.getDayOfMonth() + "", false);

					} else if (count >= 11 && count <= 20) {
						if (count == 11) {
							table11 = new PdfPTable(11);
							table11 = generateTableIndex(table11, "Days", false);
						}
						table11 = generateTableIndex(table11, date.getDayOfMonth() + "", false);
					} else {
						if (count == 1) {
							table1 = new PdfPTable(11);
							table1 = generateTableIndex(table1, "Days", false);
						}
						table1 = generateTableIndex(table1, date.getDayOfMonth() + "", false);
					}
					count++;
				}
				invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate, loggedInUser, true, true);
				Set<DeliverySchedule> deliverySchedules = invoice.getDeliverySchedule();
				if (null != deliverySchedules && !deliverySchedules.isEmpty()) {
					isInvoiceExist = true;
					Document singleInvoicePDF = new Document();
					String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
					PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
					singleInvoicePDF.open();

					productQuantity = new HashMap<Product, Integer>();
					productTotalAmount = new HashMap<Product, Double>();
					productQuantityMap = new HashMap<Product, List<PdfPCell>>();

					preface = new Paragraph();
					document = getPDFHeader(document);
					singleInvoicePDF = getPDFHeader(singleInvoicePDF);

					preface.add(new Paragraph("CUSTOMER ID		" + customer.getCode(), TIME_ROMAN_NORMAL));
					preface.add(new Paragraph("Bill To:			" + customer.getFullName(), TIME_ROMAN_NORMAL));
					preface.add(new Paragraph("Bill Address:		" + customer.getBuildingAddress(), TIME_ROMAN_NORMAL));

					creteEmptyLine(preface, 1);

					Paragraph routeParagraph = new Paragraph(routeName, TIME_ROMAN_NORMAL);
					Chunk alignmentClue = new Chunk(new VerticalPositionMark());
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

					preface.add(routeParagraph);

					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
							if (!productQuantity.containsKey(deliveryLineItem.getProduct())) {
								if (productQuantity.isEmpty()) {
									productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
								} else {
									productQuantity.put(deliveryLineItem.getProduct(), (productQuantity.get(deliveryLineItem.getProduct()) != null ? productQuantity.get(deliveryLineItem.getProduct()) : 0) + deliveryLineItem.getQuantity());
								}
								productTotalAmount.put(deliveryLineItem.getProduct(), deliveryLineItem.getProduct().getPrice());
							} else {
								totalAmount = productTotalAmount.get(deliveryLineItem.getProduct());
								totalAmount += deliveryLineItem.getProduct().getPrice();
								productTotalAmount.put(deliveryLineItem.getProduct(), totalAmount);
								productQuantity.put(deliveryLineItem.getProduct(), productQuantity.get(deliveryLineItem.getProduct()) + deliveryLineItem.getQuantity());
							}
						}
					}

					Paragraph paragraph = new Paragraph();
					creteEmptyLine(paragraph, 1);

					document.add(paragraph);
					singleInvoicePDF.add(paragraph);

					for (Entry<Product, Integer> map : productQuantity.entrySet()) {
						count = 1;
						quantityCellList = new ArrayList<PdfPCell>();
						for (LocalDate localDate : localDates) {
							for (DeliverySchedule deliverySchedule : deliverySchedules) {
								calendar.setTime(deliverySchedule.getDate());
								for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
									if (localDate.getDayOfMonth() == calendar.get(Calendar.DAY_OF_MONTH) && localDate.getMonthOfYear() == calendar.get(Calendar.MONTH) + 1) {
										if (deliveryLineItem.getProduct().equals(map.getKey())) {
											cell = generateTableCell(deliveryLineItem.getQuantity() + "", true);
											quantityCellList.add(cell);
											isExist = true;
										}
									}
								}
							}

							if (!isExist) {
								cell = generateTableCell("-", true);
								quantityCellList.add(cell);
							}
							isExist = false;

							if (count >= 21) {
								if (count == 21) {
									if (!isAlreadyTableCreated) {
										tableValue21 = new PdfPTable(12 - (31 - days));
										isAlreadyTableCreated = true;
									}
								}
							} else if (count >= 11 && count <= 20) {
								if (count == 11) {
									if (!isAlreadyTableCreated) {
										tableValue11 = new PdfPTable(11);
									}
								}
							} else {
								if (count == 1) {
									if (!isAlreadyTableCreated) {
										tableValue1 = new PdfPTable(11);
									}
								}
							}
							count++;
						}
						productQuantityMap.put(map.getKey(), quantityCellList);
					}

					for (Entry<Product, List<PdfPCell>> map : productQuantityMap.entrySet()) {
						count = 1;

						for (PdfPCell tableCell : map.getValue()) {
							if (count == 21) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue21.addCell(cell);
							} else if (count == 1) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue1.addCell(cell);
							} else if (count == 11) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue11.addCell(cell);
							}
							if (count >= 21) {
								tableValue21.addCell(tableCell);
							} else if (count >= 11 && count <= 20) {
								tableValue11.addCell(tableCell);
							} else {
								tableValue1.addCell(tableCell);
							}
							count++;
						}
					}

					table1.setWidthPercentage(100);
					tableValue1.setWidthPercentage(100);
					table11.setWidthPercentage(100);
					tableValue11.setWidthPercentage(100);
					table21.setWidthPercentage(100);
					tableValue21.setWidthPercentage(100);

					document.add(table1);
					document.add(tableValue1);
					document.add(paragraph);
					document.add(table11);
					document.add(tableValue11);
					document.add(paragraph);
					document.add(table21);
					document.add(tableValue21);
					document.add(paragraph);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(table1);
					singleInvoicePDF.add(tableValue1);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(table11);
					singleInvoicePDF.add(tableValue11);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(table21);
					singleInvoicePDF.add(tableValue21);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					isAlreadyTableCreated = false;
					productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
					productTotalTable = generateTableIndex(productTotalTable, "Description", false);
					for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
						productTotalTable = generateTableIndex(productTotalTable, "Total Quantity(" + entry.getKey().getName() + ")", true);
						productTotalTable = generateTableIndex(productTotalTable, "Unit Price(" + entry.getKey().getName() + ")", true);
					}
					productTotalTable = generateTableIndex(productTotalTable, "Total", false);
					productTotalTable.setWidthPercentage(100);

					document.add(productTotalTable);
					singleInvoicePDF.add(productTotalTable);

					productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
					productTotalTable = generateTableIndex(productTotalTable, "Provilac Milk", false);
					totalAmount = 0;
					for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
						productTotalTable = generateTableIndex(productTotalTable, productQuantity.get(entry.getKey()) + "", true);
						productTotalTable = generateTableIndex(productTotalTable, entry.getKey().getPrice() + "", true);
						totalAmount += entry.getKey().getPrice() * productQuantity.get(entry.getKey());
					}

					previousMonthDue = customer.getLastPendingDues();
					if (null == invoice.getCode()) {
						customer.setLastPendingDues(previousMonthDue + totalAmount);
						userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
						invoice.setLastPendingDues(previousMonthDue);
						invoice.setTotalAmount(totalAmount);
						invoice.setCollectionStatus(CollectionStatus.Not_Collected);
						invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);

						/*
						 * fetch all delivery schedule by from date and to date
						 * set thouse deliveryschedule to genrated invoice
						 */
						List<DeliverySchedule> listOfDeliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(invoice.getCustomer().getId(), invoice.getFromDate(), invoice.getToDate(), loggedInUser, true, true, SubscriptionType.Postpaid, Type.Normal, Type.Paid_Trial);
						for (DeliverySchedule deliverySchedule : listOfDeliverySchedules) {
							if(deliverySchedule.getType() == Type.Free_Trial) {
								continue;
							}
							deliverySchedule.setInvoice(invoice);
							deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser, true, true);
						}

						// TODO: Email Integration
						if (smsNotification) {
							if (StringUtils.isNotBlank(customer.getMobileNumber())) {
								// plivoSMSHelper.sendSMS(customer.getMobileNumber(),
								// "Provilac Milk bill for " + toMonth + "-" +
								// year + " of Rs." + (totalAmount +
								// previousMonthDue) + " of " + customer
								// .getFullName() + " is Due on " + toMonth +
								// "-" + year + ".Pay your bill at earliest.");
								plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac milk bill of Rs. " + (totalAmount + previousMonthDue) + " is overdue as of " + DateHelper.getFormattedDate(new Date()) + ". To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
							}
						}
					} else {
						// TODO: Email Integration
						if (smsNotification) {
							if (StringUtils.isNotBlank(customer.getMobileNumber())) {
								// plivoSMSHelper.sendSMS(customer.getMobileNumber(),"Provilac
								// Milk bill for "+ toMonth + "-" + year + " of
								// Rs." + previousMonthDue + " of " +
								// customer.getFullName() + " is Due on " +
								// toMonth + "-" + year + ".Pay your bill at
								// earliest.");
								plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac milk bill of Rs. " + previousMonthDue + " is overdue as of " + DateHelper.getFormattedDate(new Date()) + ". To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
							}
						}
					}

					productTotalTable = generateTableIndex(productTotalTable, totalAmount + "", false);
					productTotalTable.setWidthPercentage(100);
					document.add(productTotalTable);
					singleInvoicePDF.add(productTotalTable);

					preface = new Paragraph();
					creteEmptyLine(preface, 1);

					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("Subtotal:	" + totalAmount, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					if (isInvoiceExist) {
						if (previousMonthDue < 0) {
							routeParagraph.add(new Paragraph("Previous Month:	" + (totalAmount + previousMonthDue), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						} else {
							routeParagraph.add(new Paragraph("Previous Month:	" + (Math.abs(totalAmount - previousMonthDue)), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						}
					} else {
						routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					}

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					if (isInvoiceExist) {
						routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					} else {
						routeParagraph.add(new Paragraph("Balance Due:	" + (previousMonthDue + totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					}

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					preface = new Paragraph();
					creteEmptyLine(preface, 1);
					routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					preface.add(routeParagraph);

					document.add(preface);
					document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

					singleInvoicePDF.close();
					if (emailNotification) {
						notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
					}
					document.newPage();
				} else {
					boolean isPrepaidCustomer = false;
					List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, false);
					for (UserSubscription userSubscription : subscriptions) {
						if(userSubscription.getSubscriptionType() == SubscriptionType.Prepaid) {
							isPrepaidCustomer = true;
						}
					}
					
					if (customer.getLastPendingDues() != 0 && !isPrepaidCustomer) {
						Document singleInvoicePDF = new Document();
						String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
						PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
						singleInvoicePDF.open();

						preface = new Paragraph();

						invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate, loggedInUser, false, false);
						if (null == invoice) {
							invoice = new Invoice();
							invoice.setCustomer(customer);
							invoice.setFromDate(fromDate);
							invoice.setToDate(toDate);
							invoice.setTotalAmount(customer.getLastPendingDues());
							invoice.setLastPendingDues(customer.getLastPendingDues());
							invoice.setCollectionStatus(CollectionStatus.Not_Collected);
							invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);
						}

						document = getPDFHeader(document);
						singleInvoicePDF = getPDFHeader(singleInvoicePDF);

						preface.add(new Paragraph("CUSTOMER ID		" + customer.getCode(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill To:			" + customer.getFullName(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill Address:		" + customer.getBuildingAddress(), TIME_ROMAN_NORMAL));

						creteEmptyLine(preface, 1);

						Paragraph routeParagraph = new Paragraph(routeName, TIME_ROMAN_NORMAL);
						Chunk alignmentClue = new Chunk(new VerticalPositionMark());
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

						preface.add(routeParagraph);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						previousMonthDue = customer.getLastPendingDues();
						document.add(new Paragraph("No Orders for this month", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL)));
						preface = new Paragraph();
						creteEmptyLine(preface, 1);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Subtotal:	0", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						preface = new Paragraph();
						creteEmptyLine(preface, 1);
						routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						preface.add(routeParagraph);

						document.add(preface);
						document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						singleInvoicePDF.close();
						if (emailNotification) {
							notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
						}

						if (StringUtils.isNotBlank(customer.getMobileNumber()) && smsNotification) {
							// plivoSMSHelper.sendSMS(customer.getMobileNumber(),
							// "Provilac Milk bill for " + toMonth + "-" + year
							// + " of Rs." + previousMonthDue + " of " +
							// customer.getFullName() + " is Due on " + toMonth
							// + "-" + year + ".Pay your bill at
							// earliest.Provilac");
							plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Hi " + customer.getFullName() + ". Provilac milk bill of Rs. " + previousMonthDue + " is overdue as of " + DateHelper.getFormattedDate(new Date()) + ". To pay via Provilac App: http://bit.do/provilac and to quickpay: " + paymentBaseUrl + EncryptionUtil.encode(invoice.getId() + "") + ". Customer care: 8411864646");
						}
					}
				}
				document.close();
			} catch (Exception e) {
				e.printStackTrace();
				response.sendRedirect("redirect:/admin/invoice/list");
				return;
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FileInputStream inputStream = new FileInputStream(temperotyFilePath + "\\" + fileName);
			OutputStream os = response.getOutputStream();
			FileCopyUtils.copy(inputStream, os);
			inputStream.close();
			baos.writeTo(os);
			os.flush();
		} catch (Exception e1) {
			e1.printStackTrace();
			response.sendRedirect("redirect:/admin/invoice/list");
			return;
		}
	}

	/*
	 * Ajax Method
	 */

	/**
	 * USED_FROM_DEVICE
	 * REST API Methods
	 */
	@RequestMapping(value = "/restapi/invoice/list")
	@ResponseBody
	public ApiResponse getInvoicesByCustomer(@RequestParam(defaultValue = "1") Integer pageNumber, @RequestParam(required = true) String customerCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Page<Invoice> page = invoiceService.getInvoicesByCustomer(customerCode, pageNumber, null, false, true);
			// List<Invoice> invoices =
			// invoiceService.getAllInvoicesByCustomer(customerCode, null,
			// false, false);
			List<InvoiceDTO> dtos = new ArrayList<InvoiceDTO>();
			User customer = userService.getUserByCode(customerCode, false, userService.getSuperUser(), false, false);
			String invoiceCode = null;
			int flag = 0;
			if (null != page) {
				for (Invoice invoice : page) {
					if (flag == 0) {
						invoiceCode = invoice.getCode();
						flag = 1;
					}
					List<InvoiceLineItemDTO> invoiceLineItemDTOs = new ArrayList<InvoiceLineItemDTO>();
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(customer.getId(), invoice.getFromDate(), invoice.getToDate(), SubscriptionType.Postpaid, null, false, true);
					List<DeliveryLineItem> lineItems = new ArrayList<DeliveryLineItem>();

					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						if(deliverySchedule.getType() == Type.Free_Trial) {
							continue;
						}
						lineItems.addAll(deliverySchedule.getDeliveryLineItems());
					}
					Map<Long, Integer> map = new HashMap<Long, Integer>();

					for (DeliveryLineItem deliveryLineItem : lineItems) {
						Long productId = deliveryLineItem.getProduct().getId();
						Integer quantity = 0;
						if (map.containsKey(productId)) {
							quantity = map.get(productId) + deliveryLineItem.getQuantity();
							map.put(productId, quantity);
						} else {
							map.put(productId, deliveryLineItem.getQuantity());
						}
					}

					List<Entry<Long, Integer>> productWiseQuantity = new ArrayList<Map.Entry<Long, Integer>>(map.entrySet());

					for (Entry<Long, Integer> entry : productWiseQuantity) {
						InvoiceLineItemDTO invoiceLineItemDTO = new InvoiceLineItemDTO();
						Product product = productService.getProduct(entry.getKey(), false, null, false, false);
						invoiceLineItemDTO.setProductDTO(new ProductDTO(product));
						invoiceLineItemDTO.setQuantity(entry.getValue());
						invoiceLineItemDTO.setPrice(entry.getValue() * product.getPrice());
						invoiceLineItemDTOs.add(invoiceLineItemDTO);
					}
					double totalPaid = 0.0;

					List<Payment> payments = paymentService.getPaymentsByInvoiceCode(invoiceCode, userService.getLoggedInUser(), false, true);
					for (Payment payment : payments) {
						totalPaid += payment.getAmount();
					}

					dtos.add(new InvoiceDTO(invoice, invoiceLineItemDTOs, totalPaid));
				}
			}

			int current = page.getNumber() + 1;
			int begin = Math.max(1, current - 5);
			int end = Math.min(begin + 10, page.getTotalPages());

			apiResponse.addData("invoices", dtos).addData("current", current).addData("begin", begin).addData("end", end);

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value = "/restapi/invoice/details")
	@ResponseBody
	public ApiResponse getInvoiceDetails(@RequestParam(required = true) String invoiceCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Invoice invoice = invoiceService.getInvoiceByCode(invoiceCode, false, null, false, false);
			apiResponse.addData("invoice", new InvoiceDTO(invoice));
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	//USED_FROM_WEB payment add & update.jsp
	@RequestMapping(value = "/restapi/invoice/getInvoiceByCustomer")
	@ResponseBody
	public ApiResponse getInvoicesByCustomerId(@RequestParam(required = true) String customerId) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			List<Invoice> invoices = invoiceService.getAllInvoicesByCustomerId(Long.parseLong(customerId), false, null, false, true);
			Set<InvoiceDTO> dtos = new HashSet<InvoiceDTO>();
			for (Invoice invoice : invoices) {
				dtos.add(new InvoiceDTO(invoice));
			}
			apiResponse.addData("invoices", dtos);
			User user = userService.getUser(Long.parseLong(customerId), false, null, false, false);
			apiResponse.addData("due", user.getLastPendingDues());
			apiResponse.addData("accumulatedPoints", user.getAccumulatedPoints());
			
			RouteRecord routeRecord=routeRecordService.getRouteRecordByCustomer(user.getCode(), false, null, false, true);
			Route route=null;
			if(null==routeRecord){
				List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordService.getAllDeletedRouteRecordsByCustomer(user);	
				if(deletedRouteRecords.size()>0){
				DeletedRouteRecord deletedRouteRecord = deletedRouteRecords.get(0);
				route=routeService.getRoute(deletedRouteRecord.getRoute().getId(), false);
				}
			}else{
			 route=routeService.getRoute(routeRecord.getRoute().getId(), false);
			}
			if(null !=route){
			apiResponse.addData("route", new RouteDTO(route));
			}else{
				apiResponse.addData("route", null);
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value = "/restapi/invoice/getInvoiceForCollectionBoy")
	@ResponseBody
	public ApiResponse getInvoicesForCollectionBoy(@RequestParam(required = true) Long collectionBoyId, @RequestParam(required = true) String routeCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, -1);
			calendar.set(Calendar.DATE, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date startDate = calendar.getTime();
			calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date endDate = calendar.getTime();

			calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date startOfTheMonth = calendar.getTime();
			calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date endOfTheMonth = calendar.getTime();
			
			// get userRoute by collection boy and routeId
			UserRoute userRoute = userRouteService.getUserRouteBycollectionBoyAndRoute(collectionBoyId, routeCode, false, null, false, false);
			List<InvoiceDTO> invoiceDTOs = new ArrayList<InvoiceDTO>();
			Route route = routeService.getRoute(userRoute.getRoute().getId(), false, null, false, true);

			// get all invoices of customers of selected route which is assigned
			// to collection Boy
			for (RouteRecord routeRecord : route.getRouteRecords()) {
				Invoice invoice = invoiceService.getInvoiceByCustomerAndDateRange(routeRecord.getCustomer().getId(), startDate, endDate, null, false, false);
				if (null != invoice) {
					List<InvoiceLineItemDTO> invoiceLineItemDTOs = new ArrayList<InvoiceLineItemDTO>();
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(routeRecord.getCustomer().getId(), invoice.getFromDate(), invoice.getToDate(), SubscriptionType.Postpaid, null, false, true);
					List<DeliveryLineItem> lineItems = new ArrayList<DeliveryLineItem>();

					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						lineItems.addAll(deliverySchedule.getDeliveryLineItems());
					}

					Map<Long, Integer> map = new HashMap<Long, Integer>();
					for (DeliveryLineItem deliveryLineItem : lineItems) {
						Long productId = deliveryLineItem.getProduct().getId();
						Integer quantity = 0;
						if (map.containsKey(productId)) {
							quantity = map.get(productId) + deliveryLineItem.getQuantity();
							map.put(productId, quantity);
						} else {
							map.put(productId, deliveryLineItem.getQuantity());
						}
					}

					List<Entry<Long, Integer>> productWiseQuantity = new ArrayList<Map.Entry<Long, Integer>>(map.entrySet());
					for (Entry<Long, Integer> entry : productWiseQuantity) {
						InvoiceLineItemDTO invoiceLineItemDTO = new InvoiceLineItemDTO();
						Product product = productService.getProduct(entry.getKey(), false, null, false, false);
						invoiceLineItemDTO.setProductDTO(new ProductDTO(product));
						;
						invoiceLineItemDTO.setQuantity(entry.getValue());
						invoiceLineItemDTO.setPrice(entry.getValue() * product.getPrice());
						invoiceLineItemDTOs.add(invoiceLineItemDTO);
					}
					
					double totalPaid=0.0;
					List<Payment> payments=paymentService.getPaymentsByCustomerIdAndDateRange(invoice.getCustomer().getId(), startOfTheMonth, endOfTheMonth, null, false, false);
					for(Payment payment:payments){
						totalPaid+=payment.getAmount();
					}
					invoiceDTOs.add(new InvoiceDTO(invoice,invoiceLineItemDTOs, new UserDTO(routeRecord.getCustomer()),totalPaid));
				}
			}

			apiResponse.addData("invoices", invoiceDTOs);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/*
	 * 
	 * If collection boy log in for add reasons of payment not received.
	 * USED_FROM_DEVICE_SIDE
	 * 
	 */
	@RequestMapping(value = "/restapi/invoice/addCollectionBoyNote", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse addCollectionBoyNote(@RequestPart(required = true) String[] status, @RequestPart(required = true) String[] invoiceCodes, @RequestPart(required = true) String[] reasons) {

		ApiResponse apiResponse = new ApiResponse(true);
		User collectionBoy = userService.getLoggedInUser();
		try {
			if (null != invoiceCodes) {
				for (int i = 0; i < invoiceCodes.length; i++) {
					Invoice invoice = invoiceService.getInvoiceByCode(invoiceCodes[i], false, null, false, false);
					if (null != invoice) {
						if (null != status[i]) {
							invoice.setCollectionStatus(CollectionStatus.valueOf(status[i]));
							if (null != reasons[i]) {
								invoice.setReason(reasons[i]);
							}
							invoice = invoiceService.updateInvoice(invoice.getId(), invoice, null, false, false);
						}

						if (invoice.getCollectionStatus().name().equalsIgnoreCase("Not_Collected")) {
							firebaseWrapper.sendPaymentNotReceiveNotification(invoice);
							plivoSMSHelper.sendSMS(invoice.getCustomer().getMobileNumber(), "Dear Provilac Customer unfortunately we could not collect your payment today. Kindly call payment collection executive " + collectionBoy.getFullName() + " " + collectionBoy.getMobileNumber());
						}
					}
				}
			}

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * API to get All the delivery schedules under invoice
	 * 
	 * @param invoiceCode
	 * @return
	 */
	@RequestMapping(value = "/restapi/invoice/getDeliverySchedules", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse getDeliverySchedulesByInvoice(@RequestPart(required = true) String invoiceCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		User loggedInUser = userService.getLoggedInUser();
		try {
			Invoice invoice = invoiceService.getInvoiceByCode(invoiceCode, true, loggedInUser, false, true);
			if (null != invoice) {
				Set<DeliverySchedule> deliverySchedules = invoice.getDeliverySchedule();
				List<DeliveryScheduleDTO> deliveryScheduleDTOs = new ArrayList<DeliveryScheduleDTO>();
				for (DeliverySchedule deliverySchedule : deliverySchedules) {
					deliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
				}
				apiResponse.addData("deliverySchedules", deliveryScheduleDTOs);
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setSuccess(false);
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * USED_FROM_DEVICE_SIDE
	 */
	@RequestMapping(value = "/restapi/invoice/smsInvoiceSummery", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse sendInvoiceSummeryBySMS(@RequestPart(required = true) String invoiceCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		User loggedInUser = userService.getLoggedInUser();
		try {
			Invoice invoice = invoiceService.getInvoiceByCode(invoiceCode, true, loggedInUser, false, true);
			if (null != invoice) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(invoice.getFromDate());
				String toMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
				plivoSMSHelper.sendSMS(invoice.getCustomer().getMobileNumber(), "Provilac Milk bill for " + toMonth + "-" + calendar.get(Calendar.YEAR) + " of Rs." + invoice.getCustomer().getLastPendingDues() + " of " + invoice.getCustomer().getFullName() + " is Due on " + toMonth + "-" + calendar.get(Calendar.YEAR) + ". Pay your bill at earliest.Provilac");
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setSuccess(false);
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value = "/restapi/invoice/emailInvoice", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse sendInvoiceSummeryByEmail(@RequestPart(required = true) String invoiceCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		User loggedInUser = userService.getLoggedInUser();
		try {
			Invoice invoice = invoiceService.getInvoiceByCode(invoiceCode, true, loggedInUser, false, true);
			final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
			final String temperotyFilePath = tempDirectory.getAbsolutePath();
			Font TIME_ROMAN_NORMAL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

			User customer = invoice.getCustomer();
			String fileName = customer.getMobileNumber() + ".pdf";

			RouteRecord routeRecord = routeRecordService.getRouteRecordByCustomer(customer.getCode(), false, loggedInUser, false, false);
			String routeName = null;
			if (null != routeRecord) {
				routeName = routeRecord.getRoute().getName();
			} else {
				DeletedRouteRecord deletedRouteRecord = deletedRouteRecordService.getDeletedRouteRecord(customer.getId(), false);
				if (null != deletedRouteRecord) {
					routeName = deletedRouteRecord.getRoute().getName();
				} else {
					routeName = "No Route Assigned";
				}
			}

			Document document = null;
			Date fromDate = invoice.getFromDate();
			Date toDate = invoice.getToDate();
			try {
				document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(temperotyFilePath + "\\" + fileName));
				document.open();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(fromDate);
				int year = calendar.get(Calendar.YEAR);
				String toMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

				List<LocalDate> localDates = new ArrayList<LocalDate>();
				int days = Days.daysBetween(LocalDate.fromDateFields(fromDate), LocalDate.fromDateFields(toDate)).getDays() + 1;

				for (int i = 0; i < days; i++) {
					LocalDate d = LocalDate.fromDateFields(fromDate).withFieldAdded(DurationFieldType.days(), i);
					localDates.add(d);
				}

				Paragraph preface = new Paragraph();

				Map<Product, Integer> productQuantity = new HashMap<Product, Integer>();
				Map<Product, Double> productTotalAmount = new HashMap<Product, Double>();
				List<PdfPCell> quantityCellList = null;
				Map<Product, List<PdfPCell>> productQuantityMap = new HashMap<Product, List<PdfPCell>>();

				double totalAmount = 0;
				double previousMonthDue = 0;
				PdfPTable table1 = null;
				PdfPTable table11 = null;
				PdfPTable table21 = null;
				PdfPTable tableValue1 = null;
				PdfPTable tableValue11 = null;
				PdfPTable tableValue21 = null;
				PdfPTable productTotalTable = null;
				PdfPCell cell = null;
				boolean isExist = false;
				boolean isAlreadyTableCreated = false;
				boolean isInvoiceExist = true;

				int count = 1;
				for (LocalDate date : localDates) {
					if (count >= 21) {
						if (count == 21) {
							table21 = new PdfPTable(12 - (31 - days));
							table21 = generateTableIndex(table21, "Days", false);
						}
						table21 = generateTableIndex(table21, date.getDayOfMonth() + "", false);

					} else if (count >= 11 && count <= 20) {
						if (count == 11) {
							table11 = new PdfPTable(11);
							table11 = generateTableIndex(table11, "Days", false);
						}
						table11 = generateTableIndex(table11, date.getDayOfMonth() + "", false);
					} else {
						if (count == 1) {
							table1 = new PdfPTable(11);
							table1 = generateTableIndex(table1, "Days", false);
						}
						table1 = generateTableIndex(table1, date.getDayOfMonth() + "", false);
					}
					count++;
				}
				Set<DeliverySchedule> deliverySchedules = invoice.getDeliverySchedule();
				if (null != deliverySchedules && !deliverySchedules.isEmpty()) {
					isInvoiceExist = true;
					Document singleInvoicePDF = new Document();
					String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
					PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
					singleInvoicePDF.open();

					productQuantity = new HashMap<Product, Integer>();
					productTotalAmount = new HashMap<Product, Double>();
					productQuantityMap = new HashMap<Product, List<PdfPCell>>();

					preface = new Paragraph();
					document = getPDFHeader(document);
					singleInvoicePDF = getPDFHeader(singleInvoicePDF);

					preface.add(new Paragraph("CUSTOMER ID		" + customer.getCode(), TIME_ROMAN_NORMAL));
					preface.add(new Paragraph("Bill To:			" + customer.getFullName(), TIME_ROMAN_NORMAL));
					preface.add(new Paragraph("Bill Address:		" + customer.getBuildingAddress(), TIME_ROMAN_NORMAL));

					creteEmptyLine(preface, 1);

					Paragraph routeParagraph = new Paragraph(routeName, TIME_ROMAN_NORMAL);
					Chunk alignmentClue = new Chunk(new VerticalPositionMark());
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

					preface.add(routeParagraph);

					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
							if (!productQuantity.containsKey(deliveryLineItem.getProduct())) {
								if (productQuantity.isEmpty()) {
									productQuantity.put(deliveryLineItem.getProduct(), deliveryLineItem.getQuantity());
								} else {
									productQuantity.put(deliveryLineItem.getProduct(), (productQuantity.get(deliveryLineItem.getProduct()) != null ? productQuantity.get(deliveryLineItem.getProduct()) : 0) + deliveryLineItem.getQuantity());
								}
								productTotalAmount.put(deliveryLineItem.getProduct(), deliveryLineItem.getProduct().getPrice());
							} else {
								totalAmount = productTotalAmount.get(deliveryLineItem.getProduct());
								totalAmount += deliveryLineItem.getProduct().getPrice();
								productTotalAmount.put(deliveryLineItem.getProduct(), totalAmount);
								productQuantity.put(deliveryLineItem.getProduct(), productQuantity.get(deliveryLineItem.getProduct()) + deliveryLineItem.getQuantity());
							}
						}
					}

					Paragraph paragraph = new Paragraph();
					creteEmptyLine(paragraph, 1);

					document.add(paragraph);
					singleInvoicePDF.add(paragraph);

					for (Entry<Product, Integer> map : productQuantity.entrySet()) {
						count = 1;
						quantityCellList = new ArrayList<PdfPCell>();
						for (LocalDate localDate : localDates) {
							for (DeliverySchedule deliverySchedule : deliverySchedules) {
								calendar.setTime(deliverySchedule.getDate());
								for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
									if (localDate.getDayOfMonth() == calendar.get(Calendar.DAY_OF_MONTH) && localDate.getMonthOfYear() == calendar.get(Calendar.MONTH) + 1) {
										if (deliveryLineItem.getProduct().equals(map.getKey())) {
											cell = generateTableCell(deliveryLineItem.getQuantity() + "", true);
											quantityCellList.add(cell);
											isExist = true;
										}
									}
								}
							}

							if (!isExist) {
								cell = generateTableCell("-", true);
								quantityCellList.add(cell);
							}
							isExist = false;

							if (count >= 21) {
								if (count == 21) {
									if (!isAlreadyTableCreated) {
										tableValue21 = new PdfPTable(12 - (31 - days));
										isAlreadyTableCreated = true;
									}
								}
							} else if (count >= 11 && count <= 20) {
								if (count == 11) {
									if (!isAlreadyTableCreated) {
										tableValue11 = new PdfPTable(11);
									}
								}
							} else {
								if (count == 1) {
									if (!isAlreadyTableCreated) {
										tableValue1 = new PdfPTable(11);
									}
								}
							}
							count++;
						}
						productQuantityMap.put(map.getKey(), quantityCellList);
					}

					for (Entry<Product, List<PdfPCell>> map : productQuantityMap.entrySet()) {
						count = 1;

						for (PdfPCell tableCell : map.getValue()) {
							if (count == 21) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue21.addCell(cell);
							} else if (count == 1) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue1.addCell(cell);
							} else if (count == 11) {
								cell = generateTableCell(map.getKey().getName(), false);
								tableValue11.addCell(cell);
							}
							if (count >= 21) {
								tableValue21.addCell(tableCell);
							} else if (count >= 11 && count <= 20) {
								tableValue11.addCell(tableCell);
							} else {
								tableValue1.addCell(tableCell);
							}
							count++;
						}
					}

					table1.setWidthPercentage(100);
					tableValue1.setWidthPercentage(100);
					table11.setWidthPercentage(100);
					tableValue11.setWidthPercentage(100);
					table21.setWidthPercentage(100);
					tableValue21.setWidthPercentage(100);

					document.add(table1);
					document.add(tableValue1);
					document.add(paragraph);
					document.add(table11);
					document.add(tableValue11);
					document.add(paragraph);
					document.add(table21);
					document.add(tableValue21);
					document.add(paragraph);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(table1);
					singleInvoicePDF.add(tableValue1);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(table11);
					singleInvoicePDF.add(tableValue11);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(table21);
					singleInvoicePDF.add(tableValue21);
					singleInvoicePDF.add(paragraph);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					isAlreadyTableCreated = false;
					productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
					productTotalTable = generateTableIndex(productTotalTable, "Description", false);
					for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
						productTotalTable = generateTableIndex(productTotalTable, "Total Quantity(" + entry.getKey().getName() + ")", true);
						productTotalTable = generateTableIndex(productTotalTable, "Unit Price(" + entry.getKey().getName() + ")", true);
					}
					productTotalTable = generateTableIndex(productTotalTable, "Total", false);
					productTotalTable.setWidthPercentage(100);

					document.add(productTotalTable);
					singleInvoicePDF.add(productTotalTable);

					productTotalTable = new PdfPTable((productQuantity.size() * 2) + 2);
					productTotalTable = generateTableIndex(productTotalTable, "Provilac Milk", false);
					totalAmount = 0;
					for (Entry<Product, Double> entry : productTotalAmount.entrySet()) {
						productTotalTable = generateTableIndex(productTotalTable, productQuantity.get(entry.getKey()) + "", true);
						productTotalTable = generateTableIndex(productTotalTable, entry.getKey().getPrice() + "", true);
						totalAmount += entry.getKey().getPrice() * productQuantity.get(entry.getKey());
					}

					previousMonthDue = customer.getLastPendingDues();
					if (null == invoice.getCode()) {
						customer.setLastPendingDues(previousMonthDue + totalAmount);
						userService.updateUser(customer.getId(), customer, loggedInUser, false, false);
						invoice.setLastPendingDues(previousMonthDue);
						invoice.setTotalAmount(totalAmount);
						invoice.setCollectionStatus(CollectionStatus.Not_Collected);
						invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);

						/*
						 * fetch all delivery schedule by from date and to date
						 * set thouse deliveryschedule to genrated invoice
						 */
						List<DeliverySchedule> listOfDeliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(invoice.getCustomer().getId(), invoice.getFromDate(), invoice.getToDate(), loggedInUser, true, true, SubscriptionType.Postpaid, Type.Normal, Type.Paid_Trial);
						for (DeliverySchedule deliverySchedule : listOfDeliverySchedules) {
							deliverySchedule.setInvoice(invoice);
							deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, loggedInUser, true, true);
						}

					}

					productTotalTable = generateTableIndex(productTotalTable, totalAmount + "", false);
					productTotalTable.setWidthPercentage(100);
					document.add(productTotalTable);
					singleInvoicePDF.add(productTotalTable);

					preface = new Paragraph();
					creteEmptyLine(preface, 1);

					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("Subtotal:	" + totalAmount, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					if (isInvoiceExist) {
						if (previousMonthDue < 0) {
							invoice.setIsPaid(true);
							invoice.setCollectionStatus(CollectionStatus.Collected);
							invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, false, false);
							routeParagraph.add(new Paragraph("Previous Month:	" + (totalAmount + previousMonthDue), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						} else {
							routeParagraph.add(new Paragraph("Previous Month:	" + (Math.abs(totalAmount - previousMonthDue)), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						}
					} else {
						routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					}

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
					routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
					routeParagraph.add(alignmentClue);
					if (isInvoiceExist) {
						routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					} else {
						routeParagraph.add(new Paragraph("Balance Due:	" + (previousMonthDue + totalAmount), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					}

					document.add(routeParagraph);
					singleInvoicePDF.add(routeParagraph);

					//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
					document.add(preface);
					document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
					singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

					preface = new Paragraph();
					creteEmptyLine(preface, 1);
					routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
					routeParagraph.add(alignmentClue);
					routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					preface.add(routeParagraph);

					document.add(preface);
					document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
					singleInvoicePDF.add(preface);
					singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

					singleInvoicePDF.close();
					notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
					document.newPage();
				} else {
					boolean isPrepaidCustomer = false;
					List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, loggedInUser, false, false);
					for (UserSubscription userSubscription : subscriptions) {
						if(userSubscription.getSubscriptionType() == SubscriptionType.Prepaid) {
							isPrepaidCustomer = true;
						}
					}
					
					if (customer.getLastPendingDues() != 0 && !isPrepaidCustomer) {
						Document singleInvoicePDF = new Document();
						String invoiceFileName = temperotyFilePath + "\\" + customer.getCode() + "-" + fileName;
						PdfWriter.getInstance(singleInvoicePDF, new FileOutputStream(invoiceFileName));
						singleInvoicePDF.open();

						preface = new Paragraph();

						invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate, loggedInUser, false, false);
						if (null == invoice) {
							invoice = new Invoice();
							invoice.setCustomer(customer);
							invoice.setFromDate(fromDate);
							invoice.setToDate(toDate);
							invoice.setTotalAmount(customer.getLastPendingDues());
							invoice.setLastPendingDues(customer.getLastPendingDues());
							invoice.setCollectionStatus(CollectionStatus.Not_Collected);
							invoice = invoiceService.createInvoice(invoice, loggedInUser, false, false);
						}

						document = getPDFHeader(document);
						singleInvoicePDF = getPDFHeader(singleInvoicePDF);

						preface.add(new Paragraph("CUSTOMER ID		" + customer.getCode(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill To:			" + customer.getFullName(), TIME_ROMAN_NORMAL));
						preface.add(new Paragraph("Bill Address:		" + customer.getBuildingAddress(), TIME_ROMAN_NORMAL));

						creteEmptyLine(preface, 1);

						Paragraph routeParagraph = new Paragraph(routeName, TIME_ROMAN_NORMAL);
						Chunk alignmentClue = new Chunk(new VerticalPositionMark());
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("For month of " + toMonth, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));

						preface.add(routeParagraph);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						previousMonthDue = customer.getLastPendingDues();
						document.add(new Paragraph("No Orders for this month", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL)));
						preface = new Paragraph();
						creteEmptyLine(preface, 1);

						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						//routeParagraph = new Paragraph("For Bank Transfer", TIME_ROMAN_NORMAL);
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Subtotal:	0", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Name: ICICI Bank,Satara Road,Pune", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Previous Month:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//routeParagraph = new Paragraph("Bank Acc No:033705500599(Current Acc)", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD));
						routeParagraph = new Paragraph(" ", TIME_ROMAN_NORMAL);
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Balance Due:	" + previousMonthDue, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						document.add(routeParagraph);
						singleInvoicePDF.add(routeParagraph);

						//document.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						//document.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						document.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						document.add(preface);
						document.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						singleInvoicePDF.add(new Paragraph("Bank Acc Name:Provilac Dairy Farms Pvt Ltd", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(new Paragraph("NEFT IFSC Code: ICIC0000337", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));
						singleInvoicePDF.add(new Paragraph("IMP:Please email/sms us the Transaction ID. Thanks!", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new LineSeparator(1, 100, null, Element.ALIGN_CENTER, 2));

						preface = new Paragraph();
						creteEmptyLine(preface, 1);
						routeParagraph = new Paragraph("Date: " + DateHelper.getFormattedDate(new Date()), new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
						routeParagraph.add(alignmentClue);
						routeParagraph.add(new Paragraph("Customer Signature", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						preface.add(routeParagraph);

						document.add(preface);
						document.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));
						singleInvoicePDF.add(preface);
						singleInvoicePDF.add(new Paragraph("For PDFPL Authorised Signatury:", new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL)));

						singleInvoicePDF.close();
						notificationHelper.sendNotificationForInvoiceGeneration(customer, toMonth + " " + year, new File(invoiceFileName), invoice);
					}
				}
				document.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				apiResponse.setSuccess(false);
			}

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setSuccess(false);
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	//USED_FROM_DEVICE_SIDE
	@RequestMapping(value = "/openapi/invoice/billSummery")
	@ResponseBody
	public ApiResponse getBillSummery(@RequestParam(required = true) String customerId) {

		long id = Long.parseLong(customerId);
		ApiResponse apiResponse = new ApiResponse(true);
		Map<String, Double> billSummery = new HashMap<String, Double>();
		Double totalUnbilledAmount = 0.0;
		try {
			User loggedInUser = userService.getUser(id, true, userService.getSuperUser(), false, false);
			Calendar calendar = Calendar.getInstance();
			Date toDate = calendar.getTime();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.set(Calendar.DATE, 1);
			Date fromDate = calendar.getTime();

			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(id, fromDate, toDate, SubscriptionType.Postpaid, loggedInUser, false, true);
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if (deliverySchedule.getStatus().name().equalsIgnoreCase(DeliveryStatus.Delivered.name())) {
					for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
						totalUnbilledAmount = totalUnbilledAmount + (deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
					}
				}
			}

			Invoice invoice = invoiceService.getLatestInvoiceByCustomer(id, loggedInUser, false, false);
			if (null != invoice) {
				billSummery.put("latestBill", invoice.getTotalAmount());
				apiResponse.addData("invoiceDate", DateHelper.getFormattedDate(invoice.getCreatedDate()));
			} else {
				billSummery.put("latestBill", 0.0);
			}

			Payment payment = paymentService.getLatestPaymentByCustomer(id, loggedInUser, false, false);

			if (null != payment) {
				billSummery.put("latestPayment", payment.getAmount());
				apiResponse.addData("paymentDate", DateHelper.getFormattedDate(payment.getDate()));
			} else {
				billSummery.put("latestPayment", 0.0);
			}

			billSummery.put("unbilledAmount", totalUnbilledAmount);
			billSummery.put("balanceAmount", loggedInUser.getLastPendingDues());

			calendar.setTime(new Date());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			fromDate = calendar.getTime();
			calendar.add(Calendar.DATE, 1);
			toDate = calendar.getTime();

			deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(id, fromDate, toDate, loggedInUser, false, true, null);
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				calendar.setTime(deliverySchedule.getDate());
				if (calendar.getTime().equals(fromDate)) {
					apiResponse.addData("todaysDeliverySchedule", new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
				} else {
					apiResponse.addData("tommorowsDeliverySchedule", new DeliveryScheduleDTO(deliverySchedule, deliverySchedule.getDeliveryLineItems()));
				}
			}

			boolean isExist = false;

			isExist = userSubscriptionService.isUserSubscriptionExistForCustomer(id);

			if (!isExist) {
				apiResponse.addData("isSubscriptioExist", isExist);
				isExist = deliveryScheduleService.isDeliveryScheduleExistforCustomer(id);
				if (isExist) {
					apiResponse.addData("isDeliveryScheduleExist", isExist);
				} else {
					apiResponse.addData("isDeliveryScheduleExist", isExist);
				}
			} else {
				apiResponse.addData("isSubscriptioExist", isExist);
			}
			User customer = userService.getUser(id, false, loggedInUser, false, false);
			SubscriptionRequest request = subscriptionRequestService.getSubscriptionRequestByCustomer(customer.getMobileNumber(), loggedInUser, false, false);
			if (null != request) {
				apiResponse.addData("isSubscriptionRequest", request.getIsSubscriptionRequest());
			} else {
				apiResponse.addData("isSubscriptionRequest", false);
			}
			apiResponse.addData("billSummery", billSummery);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}

	/**
	 * End REST API Methods
	 */
}
