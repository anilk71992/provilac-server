package com.vishwakarma.provilac.controller;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.UserSubscriptionDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.mvc.validator.UserSubscriptionValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.SubscriptionLineItemService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class UserSubscriptionController {

	@Resource
	private UserSubscriptionValidator userSubscriptionValidator;

	@Resource
	private UserSubscriptionService userSubscriptionService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource 
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private SubscriptionLineItemService subscriptionLineItemService;
	
	@Resource AsyncJobs asyncJobs;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private Environment environment;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@Resource ActivityLogService activityLogService;
	
	@InitBinder(value = "userSubscription")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(userSubscriptionValidator);
	}

	private Model populateModelForAdd(Model model, UserSubscription userSubscription) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("userSubscription", userSubscription);
		List<User> newCustomers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.NEW, loggedInUser, false, false);
		model.addAttribute("newCustomers", newCustomers);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/userSubscription/add")
	public String addUserSubscription(Model model, 
										@RequestParam(required=false) boolean createPrepay,
										@RequestParam(required=false) Long selectedCustomerId) {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		UserSubscription userSubscription = new UserSubscription();
		if(null != selectedCustomerId) {
			User customer = userService.getUser(selectedCustomerId, true, loggedInUser, false, false);
			userSubscription.setUser(customer);
		}
		model = populateModelForAdd(model, userSubscription);
		if(createPrepay)
			return "admin-pages/userSubscription/addPrepay";
		return "admin-pages/userSubscription/add";
	}

	@RequestMapping(value = "/admin/userSubscription/add", method = RequestMethod.POST)
	public String userSubscriptionAdd(UserSubscription userSubscription, 
									BindingResult formBinding, 
									Model model,
									@RequestParam(required=false,value="product")String productCodes[],
									@RequestParam(required=false, value="quantity")String quantity[],
									@RequestParam(required=false, value="day") Integer days[],
									@RequestParam(required=false,value="type")String types[]){
		
		
		for (int i = 0; i < productCodes.length; i++) {
			for (int j = 1; j < productCodes.length; j++) {
				if(i!=j){
					if(types[i].equals(types[j])){
						if(productCodes[i].equals(productCodes[j])){
							if(days[i].equals(days[j])){
								String message = "Products should not overlap with same type ";
								return "redirect:/admin/userSubscription/list?message=" + message;
							}
						}
					}
				}
			}
		}
//		UserSubscription userSubscription2 = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(userSubscription.getUser().getCode(), true,userService.getLoggedInUser(),true,true);
//		if (userSubscription2 != null) {
//			userSubscription2.setIsCurrent(false);
//			 userSubscriptionService.updateUserSubscription(userSubscription2.getId(), userSubscription2, userService.getLoggedInUser(), true, true);
//		}
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(userSubscription.getUser().getCode(), true, userService.getLoggedInUser(), true, true);
		
		if(null != userSubscriptions && !userSubscriptions.isEmpty()){
			for (UserSubscription userSubscription2 : userSubscriptions) {
				userSubscription2.setIsCurrent(false);
				userSubscriptionService.updateUserSubscription(userSubscription2.getId(), userSubscription2, userService.getLoggedInUser(), true, true);
			}
		}
		
		for (int i = 0; i < productCodes.length; i++) {
			String productCode = productCodes[i];

			if(types[i].equalsIgnoreCase(SubscriptionType.Custom.name())){
				if(quantity[i] == null){
					continue;
				}
			} else {
				if (Integer.parseInt(quantity[i]) == 0) {
					continue;
				}
			}
			
			Product product = productService.getProductByCode(productCode, false, null, false, false);
			String type = types[i];
			if(SubscriptionType.valueOf(type)==SubscriptionType.Daily){
				SubscriptionLineItem lineItem = new SubscriptionLineItem();
				int quantity1= Integer.parseInt(quantity[i]);
				lineItem.setQuantity(quantity1);
				lineItem.setProduct(product);
				lineItem.setType(SubscriptionType.valueOf(type));
				userSubscription.getSubscriptionLineItems().add(lineItem);
			} else if (SubscriptionType.valueOf(type)==SubscriptionType.Weekly){
					int quantity1= Integer.parseInt(quantity[i]);
					int day = days[i];
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setDay(Day.values()[day]);
					
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.valueOf(type));
					userSubscription.getSubscriptionLineItems().add(lineItem);
			} else{
					SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
					subscriptionLineItem.setProduct(product);
					subscriptionLineItem.setType(SubscriptionType.Custom);
					subscriptionLineItem.setCustomPattern(quantity[i]);
					userSubscription.getSubscriptionLineItems().add(subscriptionLineItem);
			
			}
		}
		userSubscriptionValidator.validate(userSubscription, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, userSubscription);
			return "admin-pages/userSubscription/add";
		}
		User loggedInUser = userService.getLoggedInUser();
	//	DateHelper.setToEndOfDay(userSubscription.getStartDate());
		userSubscription.setIsCurrent(true);
		userSubscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
		userSubscription = userSubscriptionService.createUserSubscription(userSubscription, loggedInUser, false, true);
		
		//update user status to active
		User user = userService.getUserByCode(userSubscription.getUser().getCode(), false, loggedInUser, false, false);
		if(!user.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", user.getStatus()+"_"+"ACTIVE", "deliverySchedule", user.getId());
		user.setStatus(Status.ACTIVE);
		userService.updateUser(user.getId(), user, loggedInUser, true, true);
		
		Date startDate = userSubscription.getStartDate();
		DateHelper.setToStartOfDay(startDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, 2);
		Date endDate = calendar.getTime();
		DateHelper.setToEndOfDay(endDate);
		List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionByCustomerMobileNumber(userSubscription.getUser().getMobileNumber(), loggedInUser, false, true);
		if(subscriptions.size()==1){
			plivoSMSHelper.sendSMS(user.getMobileNumber(), "Welcome to Provilac Milk your subscription has been placed successfully. Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac.  Team Provilac!");
			emailHelper.sendMailForSubscriptionCreationFirstTime(userSubscription);
		} else{
			plivoSMSHelper.sendSMS(user.getMobileNumber(), "Welcome to Provilac Milk your subscription has been placed successfully. Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac.  Team Provilac!");
			firebaseWrapper.sendSubscriptionAddedNotification(userSubscription, endDate);
			emailHelper.sendMailForSubscriptionCreation(userSubscription, endDate);
		}
		asyncJobs.createFirstTimePostpaidDeliverySchedule(userSubscription, userSubscription.getSubscriptionLineItems(), userSubscription.getStartDate(), loggedInUser);
		
		
		String message = "UserSubscription added successfully";
		return "redirect:/admin/userSubscription/list?message=" + message;
	}
	
	@RequestMapping(value = "/admin/prepaySubscription/add", method = RequestMethod.POST)
	public String prepaySubscriptionAdd(UserSubscription userSubscription, 
									BindingResult formBinding, 
									Model model,
									@RequestParam(required=true) Integer duration,
									@RequestParam(required=true, value="product") String productCodes[],
									@RequestParam(required=true, value="quantity") String quantities[],
									@RequestParam(required=true, value="day") Integer days[],
									@RequestParam(required=true, value="type") String types[],
									Authentication authentication){
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		User loggedInUser = userInfo.getUser();
		
		//Lineitem population
		Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
		for (int i = 0; i < productCodes.length; i++) {
			String productCode = productCodes[i];

			if(types[i].equalsIgnoreCase(SubscriptionType.Custom.name())){
				if(quantities[i] == null){
					continue;
				}
			} else {
				if (Integer.parseInt(quantities[i]) == 0) {
					continue;
				}
			}
			
			Product product = productService.getProductByCode(productCode, false, loggedInUser, false, false);
			String type = types[i];
			if(SubscriptionType.valueOf(type)==SubscriptionType.Daily) {
				SubscriptionLineItem lineItem = new SubscriptionLineItem();
				int quantity1= Integer.parseInt(quantities[i]);
				lineItem.setQuantity(quantity1);
				lineItem.setProduct(product);
				lineItem.setType(SubscriptionType.valueOf(type));
				lineItems.add(lineItem);
			} else if (SubscriptionType.valueOf(type)==SubscriptionType.Weekly) {
					int quantity1= Integer.parseInt(quantities[i]);
					int day = days[i];
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setDay(Day.values()[day]);
					
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.valueOf(type));
					lineItems.add(lineItem);
			} else {
					SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
					subscriptionLineItem.setProduct(product);
					subscriptionLineItem.setType(SubscriptionType.Custom);
					subscriptionLineItem.setCustomPattern(quantities[i]);
					lineItems.add(subscriptionLineItem);
			}
		}
		userSubscription.setSubscriptionLineItems(lineItems);
		
		//Set start and end date
		DateHelper.setToStartOfDay(userSubscription.getStartDate());
		Date toDate = DateHelper.addMonths(userSubscription.getStartDate(), duration);
		DateHelper.setToEndOfDay(toDate);
		userSubscription.setEndDate(toDate);
		userSubscription.setSubscriptionType(UserSubscription.SubscriptionType.Prepaid);
		
		//Set final amount = total amount
		userSubscription.setFinalAmount(userSubscription.getTotalAmount());
		
		//Effective price calculations, assumption: keys are consistent across all the products
		List<Product> products = productService.getAllProducts(loggedInUser, false, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = Double.valueOf(userSubscription.getTotalAmount()).intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		Map<Long, Double> effectivePriceMap = new HashMap<Long, Double>();
		Map<Product, Double> effetiveProductPriceMap = new HashMap<Product, Double>();
		if(key == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPrice());
				effetiveProductPriceMap.put(product2, product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPricingMap().get(key) == null ? product2.getPrice() : product2.getPricingMap().get(key));
				effetiveProductPriceMap.put(product2, product2.getPricingMap().get(key) == null ? product2.getPrice() : product2.getPricingMap().get(key));
			}
		}
		userSubscription.setProductPricing(effectivePriceMap);
		
		userSubscriptionValidator.validate(userSubscription, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, userSubscription);
			return "admin-pages/userSubscription/addPrepay";
		}
		
		//Mark all previous subscriptions inactive
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(userSubscription.getUser().getCode(), true, loggedInUser, true, true);
		if(null != userSubscriptions && !userSubscriptions.isEmpty()){
			for (UserSubscription userSubscription2 : userSubscriptions) {
				userSubscription2.setIsCurrent(false);
				userSubscriptionService.updateUserSubscription(userSubscription2.getId(), userSubscription2, loggedInUser, true, true);
			}
		}
		
		//Create subscription
		userSubscription.setIsCurrent(true);
		userSubscription = userSubscriptionService.createUserSubscription(userSubscription, loggedInUser, false, true);
		
		//Update pending dues for customer
		User customer = userSubscription.getUser();
		//customer.getLastPendingDues() > 0, is already managed by validator
		if(customer.getLastPendingDues() < 0) {
			customer.setLastPendingDues(customer.getLastPendingDues() * -1);
		}
		//No need to add pending dues since first time async job runs from subscription start date to end date
		//customer.setLastPendingDues(customer.getLastPendingDues() + userSubscription.getTotalAmount());
		if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"ACTIVE", "user", customer.getId());
		customer.setStatus(Status.ACTIVE);
		customer.setRemainingPrepayBalance(userSubscription.getFinalAmount());
		customer = userService.updateUser(customer.getId(), customer, loggedInUser, false, false);

		//Send welcome email + invoice
		emailHelper.sendMailForPrepaySubscriptionApproval(userSubscription, effetiveProductPriceMap);
//		Send out notifications
//		List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionByCustomerMobileNumber(userSubscription.getUser().getMobileNumber(), loggedInUser, false, true);
//		if(subscriptions.size()==1){//1 since we recently added a subscription, so it was first subscription
//			plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Welcome to Provilac Milk your subscription has been placed successfully. Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac.  Team Provilac!");
//			emailHelper.sendMailForSubscriptionCreationFirstTime(userSubscription);
//		} else{
//			plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Welcome to Provilac Milk your subscription has been placed successfully. Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac.  Team Provilac!");
//			firebaseWrapper.sendSubscriptionAddedNotification(userSubscription, userSubscription.getEndDate());
//			emailHelper.sendMailForSubscriptionCreation(userSubscription, userSubscription.getEndDate());
//		}
		
		asyncJobs.createFirstTimePrepayDeliverySchedules(userSubscription, userSubscription.getSubscriptionLineItems(), loggedInUser);
		
		String message = "UserSubscription added successfully";
		return "redirect:/admin/userSubscription/list?message=" + message;
	}

	@RequestMapping(value = "/admin/userSubscription/list")
	public String getUserSubscriptionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<UserSubscription> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = userSubscriptionService.searchUserSubscription(pageNumber, searchTerm, user,true,false);
		} else {
			page = userSubscriptionService.getUserSubscriptions(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("userSubscriptions", page.getContent());

		return "/admin-pages/userSubscription/list";
	}

	@RequestMapping(value = "/admin/userSubscription/show/{cipher}")
	public String showUserSubscription(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		UserSubscription userSubscription = userSubscriptionService.getUserSubscription(id, true, loggedInUser,true, true);
		//asyncJobs.createFirstTimeDeliverySchedule(userSubscription, userSubscription.getSubscriptionLineItems(),userSubscription.getStartDate(),loggedInUser);
		model.addAttribute("userSubscription", userSubscription);
		return "/admin-pages/userSubscription/show";
	}

	@RequestMapping(value = "/admin/userSubscription/update/{cipher}")
	public String updateUserSubscription(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		UserSubscription userSubscription = userSubscriptionService.getUserSubscription(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, userSubscription);
		model.addAttribute("id", userSubscription.getId());
		model.addAttribute("version", userSubscription.getVersion());
		return "/admin-pages/userSubscription/update";
	}

	@RequestMapping(value = "/admin/userSubscription/update", method = RequestMethod.POST)
	public String userSubscriptionUpdate(UserSubscription userSubscription, 
										BindingResult formBinding, 
										Model model) {

		userSubscription.setUpdateOperation(true);
		userSubscriptionValidator.validate(userSubscription, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, userSubscription);
			return "admin-pages/userSubscription/update";
		}
		userSubscription.setId(Long.parseLong(model.asMap().get("id") + ""));
		userSubscription.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, loggedInUser,true,false);
		
		Date startDate = userSubscription.getStartDate();
		DateHelper.setToStartOfDay(startDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, 2);
		Date endDate = calendar.getTime();
		DateHelper.setToEndOfDay(endDate);
		
		//plivoSMSHelper.sendSMS(userSubscription.getUser().getMobileNumber(), "Dear customer,your subscription has been edited from : "+userSubscription.getStartDate()+" to "+ endDate);
		firebaseWrapper.sendSubscriptionUpdatedNotification(userSubscription, endDate);
		//emailHelper.sendMailForSubscriptionUpdate(userSubscription, endDate);
		
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "UserSubscription updated successfully";
		return "redirect:/admin/userSubscription/list?message=" + message;
	}

	@RequestMapping(value = "/admin/userSubscription/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			userSubscriptionService.deleteUserSubscription(id);
			message = "UserSubscription deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete UserSubscription";
		}
		return "redirect:/admin/userSubscription/list?message=" + message;
	}
	
	/**
	 * Excel Import 
	 */
	@RequestMapping(value="/admin/userSubscription/import", method=RequestMethod.GET)
	public String importUserSubscriptions(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/userSubscription/import";
	}
	
	@RequestMapping(value="/admin/userSubscription/import", method=RequestMethod.POST)
	public String importUserSubscriptions(Model model, @RequestParam(required=true) MultipartFile file) {
		
		String message = "Successfully imported userSubscription";
		if(null == file || file.isEmpty() || !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)) {
			
			message = "Please select valid excel 2007 or later file";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/userSubscription/import";
		}
		try {
			importUserSubscriptions(file);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error uploading userSubscriptions, please try with valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/userSubscription/import";
		}
		return "redirect:/admin/userSubscription/list?message=" + message;
	}
	
	private void importUserSubscriptions(MultipartFile excelFile) throws InvalidFormatException, IOException {
		
		Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();
		int rowCount = 0;
		UserSubscription subscription = null;
		String existingMobileNumber = null;
		List<SubscriptionLineItem> lineItems = new ArrayList<SubscriptionLineItem>();
		for (Row row : sheet) {
			
			if(rowCount == 0) {
								
				rowCount++;
				continue; //Skipping first row 
			}

			Cell mobileNumberCell = row.getCell(0);
			if(rowCount == 1 && row.getCell(0) == null){
				rowCount++;
				continue;
				
			}else{
				if(null == existingMobileNumber && null == row.getCell(0)){
					rowCount++;
					continue;
				}
			}
			
			String mobileNumber = formatter.formatCellValue(mobileNumberCell);
			
			Cell productNameCell = row.getCell(1);
			String productName =  productNameCell.getStringCellValue();
			
			Cell typeCell = row.getCell(2);
			String type = typeCell.getStringCellValue();
			
			Cell mondayCell = row.getCell(3);
			String quantity1 = formatter.formatCellValue(mondayCell);
			
			Cell tuesdayCell = row.getCell(4);
			String quantity2 = formatter.formatCellValue(tuesdayCell);
			
			Cell wednsdayCell = row.getCell(5);
			String quantity3 = formatter.formatCellValue(wednsdayCell);
			
			Cell thursdayCell = row.getCell(6);
			String quantity4 = formatter.formatCellValue(thursdayCell);
			
			Cell fridayCell = row.getCell(7);
			String quantity5 = formatter.formatCellValue(fridayCell);
			
			Cell saturdayCell = row.getCell(8);
			String quantity6 = formatter.formatCellValue(saturdayCell);
			
			Cell sundayCell = row.getCell(9);
			String quantity7 = formatter.formatCellValue(sundayCell);
			
			Cell startDateCell = row.getCell(10);
			String startDate = formatter.formatCellValue(startDateCell);
			
			 List<UserSubscription> subscriptions=  userSubscriptionService.getUserSubscriptionByCustomerMobileNumber(mobileNumber, null, false, false);
			 if(null!=subscriptions){
				 for (UserSubscription userSubscription : subscriptions) {
					userSubscription.setIsCurrent(false);
					userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, null, false, false);
				}
			 }
			User loggedInUser = userService.getLoggedInUser();
			User user = null;
			if(StringUtils.isNotBlank(mobileNumber)){
				user = userService.getUserByMobileNumber(mobileNumber, false, null, false, false);
			}else{
				user = userService.getUserByMobileNumber(existingMobileNumber, false, null, false, false);
			}
			if(null==user ){
				rowCount++;
				continue;
			}
			if(null!=user &&!(user.hasRole(Role.ROLE_CUSTOMER))){
				rowCount++;
				continue;
			}
			
			if(rowCount == 1){
				existingMobileNumber = mobileNumber;
				subscription= new UserSubscription();
				lineItems = new ArrayList<SubscriptionLineItem>();
			}else{
				if(existingMobileNumber != mobileNumber && StringUtils.isNotBlank(mobileNumber)){
					subscription = new UserSubscription();
					existingMobileNumber = mobileNumber;
					lineItems = new ArrayList<SubscriptionLineItem>();
				}
			}
			
			
			subscription.setUser(user);
//			subscription.setType(SubscriptionType.valueOf(type));
			subscription.setStartDate(DateHelper.parseDate(startDate));
			subscription.setIsCurrent(true);
			subscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
			
			subscription = userSubscriptionService.createUserSubscription(subscription, loggedInUser, true, true);
			Product product = productService.getProductByName(productName, false, userService.getLoggedInUser(), false, false);
			 if(!type.toLowerCase().equalsIgnoreCase(SubscriptionType.Custom.toString()) ){
				if(null!= quantity1 && !quantity1.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity1));
					lineItem.setDay(Day.Day_2);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity2 && !quantity2.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity2));
					lineItem.setDay(Day.Day_3);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity3 && !quantity3.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity3));
					lineItem.setDay(Day.Day_4);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity4 && !quantity4.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity4));
					lineItem.setDay(Day.Day_5);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity5 && !quantity5.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity5));
					lineItem.setDay(Day.Day_6);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity6 && !quantity6.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity6));
					lineItem.setDay(Day.Day_7);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
				if(null!= quantity7 && !quantity7.isEmpty()){
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setQuantity(Integer.parseInt(quantity7));
					lineItem.setDay(Day.Day_1);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Daily.toString())){
						lineItem.setType(SubscriptionType.Daily);
					}else{
						lineItem.setType(SubscriptionType.Weekly);
					}
					lineItem.setUserSubscription(subscription);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
				}
			 }else{
				 String pattern = null;

					if(null!= quantity1 && !quantity1.isEmpty()){
						pattern = quantity1;
					}
					if(null!= quantity2 && !quantity2.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity2;
						}else{
							pattern += quantity2;
						}
					}
					if(null!= quantity3 && !quantity3.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity3;
						}else{
							pattern += quantity3;
						}
					}
					if(null!= quantity4 && !quantity4.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity4;
						}else{
							pattern += quantity4;
						}
					}
					if(null!= quantity5 && !quantity5.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity5;
						}else{
							pattern += quantity5;
						}
					}
					if(null!= quantity6 && !quantity6.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity6;
						}else{
							pattern += quantity6;
						}
					}
					if(null!= quantity7 && !quantity7.isEmpty()){
						if(null != pattern){
							pattern += "-"+quantity7;
						}else{
							pattern += quantity7;
						}
					}
				 
				 SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setProduct(product);
					lineItem.setCustomPattern(pattern);
					lineItem.setUserSubscription(subscription);
					if(type.toLowerCase().equalsIgnoreCase(SubscriptionType.Custom.toString()))
						lineItem.setType(SubscriptionType.Custom);
					lineItems.add(lineItem);
//					subscription.getSubscriptionLineItems().add(lineItem);
			 }
			
			BindingResult bindingResult = new DataBinder(subscription).getBindingResult();
			userSubscriptionValidator.validate(subscription, bindingResult);
			
			if(bindingResult.hasErrors()) {
				continue;
			}
//			subscription.getSubscriptionLineItems().clear();
//			subscription.getSubscriptionLineItems().addAll(lineItems);
//			subscription = userSubscriptionService.createUserSubscription(subscription, loggedInUser, true, true);
			lineItems = (List<SubscriptionLineItem>) subscriptionLineItemService.createSubscriptionLineItems(lineItems, loggedInUser, true, true);
			if(!user.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", user.getStatus()+"_"+"ACTIVE", "userSubscription", user.getId());
			user.setStatus(Status.ACTIVE);
			userService.updateUser(user.getId(), user, loggedInUser, false, false);
			subscription = userSubscriptionService.getUserSubscription(subscription.getId(), true, loggedInUser, false, true);
			asyncJobs.createFirstTimePostpaidDeliverySchedule(subscription, subscription.getSubscriptionLineItems(), subscription.getStartDate(), loggedInUser);
			
			rowCount++;
		}
	}
	
	
	// Extend prepaid subscription
	@RequestMapping(value = "/end-user/extend-prepaid/summary-subscription")
	public String extendSubscription(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, true, true);
		if(userSubscription == null) {    
			String message = "userSubscription is not available";
			return "redirect:/index?message=" + message;
		}
		
		if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid) {
			String message = "userSubscription have postpaid";
			return "redirect:/index?message=" + message;
		}
		
		Set<SubscriptionLineItem> existingItems = new HashSet<SubscriptionLineItem>();
		existingItems = userSubscription.getSubscriptionLineItems();
		
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(userSubscription.getEndDate(), 1);
		requestInterceptor.addDataToSession("startDate", startDate);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = 3;
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
	
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		return "/enduser-pages/extend-prepay-subscription/summary-of-existing-subscription";
	}
	
	@RequestMapping(value = "/end-user/extend-prepaid/subscription-duration")
	public String extendSubscriptionDuration(Model model, Authentication authentication) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		requestInterceptor.addDataToSession("prepayDuration", 3);
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		requestInterceptor.addDataToSession("user", userInfo.getUser());
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = (Date) requestInterceptor.getDataFromSession("startDate");
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		//Calculate effective prices
		List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}

		LinkedList<String> effectiveItems = new LinkedList();
		LinkedList<Double> prices = new LinkedList();
		LinkedList<Double> effectivePrices = new LinkedList();
		if(key == null) {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPricingMap().get(key));
			}
		}
		
		model.addAttribute("effectiveItems", effectiveItems);
		model.addAttribute("prices", prices);
		model.addAttribute("effectivePrices", effectivePrices);
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		
		return "/enduser-pages/extend-prepay-subscription/subscription-duration";
	}
	
	@RequestMapping(value = "/end-user/extend-prepaid/average-monthly-bill")
	public String extendAverageMonthlyBill(Model model, HttpServletResponse response) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date startDate = (Date) requestInterceptor.getDataFromSession("startDate");
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("totalSubscriptionAmount", totalSubscriptionAmount);
		model.addAttribute("durationInMonths", durationInMonths);
		requestInterceptor.addDataToSession("totalSubscriptionAmount", totalSubscriptionAmount);
		
		return "/enduser-pages/extend-prepay-subscription/average-monthly-bill";
	}
	
	@RequestMapping(value="/extendSubscription/forwardPrepayPaymentRequest")
	public String extendSubscriptionSetAddressAndForwardPrepayPaymentRequest(Authentication authentication, Model model) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.EXTEND_PREPAY_FRONTEND_PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.PREPAY_FRONTEND_PAYU_FURL);
		model.addAttribute("firstName", userInfo.getFirstName());
		model.addAttribute("lastName", userInfo.getLastName());
		model.addAttribute("fullName", userInfo.getFullName());
		model.addAttribute("phone", userInfo.getMobileNumber());
		model.addAttribute("email", userInfo.getEmail());
		Double totalSubscriptionAmount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		
		String txnId = "WEB-PP" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
		String productInfo = txnId;
		model.addAttribute("productInfo", productInfo);
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		
		String amountString = amountString = new DecimalFormat("##.##").format(totalSubscriptionAmount);
		
		model.addAttribute("amount", amountString);
		String udf1 = "";
		model.addAttribute("udf1", udf1);
		String udf2 = "";
		model.addAttribute("udf2", udf2);
		String udf3 = "";
		model.addAttribute("udf3", udf3);
		String udf4 = "";
		model.addAttribute("udf4", udf4);
		String udf5 = ""; 
		model.addAttribute("udf5", udf5);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + userInfo.getFirstName() + "|" + userInfo.getEmail() + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		model.addAttribute("hash", encodedHash);
		model.addAttribute("txnId", txnId);
		return "/enduser-pages/paymentRequestForwarder";
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.EXTEND_PREPAY_FRONTEND_PAYU_SURL)
	public String handlePayUExtendPrepayPaymentSuccess(Authentication authentication, HttpServletRequest request,
												@RequestParam(required=false) String payuMoneyId, Model model,
												@RequestParam(required=false) String mode) {
		
		String status = "success";
		String firstname = "raj";
		double amount = 1212;
		String txnid = "hdk11";
		String hash = "ddf";
		String productinfo = "rhh";
		String phone = "8720802421";
		String email = "satendra";
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		/*if(!hash.equals(encodedHash)) {
			return "/enduser-pages/payu_success_wrong";
		}*/

		/*if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/enduser-pages/payu_success_wrong";
		}*/
		
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, true, true);
		if(userSubscription == null) {
			String message = "userSubscription is not available";
			return "redirect:/index?message=" + message;
		}
		
		amount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");  
		Date startDate = DateHelper.addDays(userSubscription.getEndDate(), 1);
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		
		userSubscription.setEndDate(endDate);
		userSubscription.setTotalAmount(userSubscription.getTotalAmount() + amount);
		userSubscription.setFinalAmount(userSubscription.getFinalAmount() + amount);
		userSubscription.setTxnId(userSubscription.getTxnId() + " , " + txnid);
		
		UserSubscription updatedUserSubscription = userSubscriptionService.updateUserSubscriptionByCustomer(userSubscription.getId(), userSubscription, loggedInUser, true, true); 
		
		Double lastPendingDues = loggedInUser.getLastPendingDues() + amount + loggedInUser.getRemainingPrepayBalance();
		loggedInUser.setLastPendingDues(lastPendingDues);
		loggedInUser.setRemainingPrepayBalance(0.0);
		
		User updatedLoggedInUser = userService.updateRemmaingPrePaid(loggedInUser.getId(), loggedInUser, true, true);
		
		asyncJobs.createExcessivePrepayDeliveries(updatedLoggedInUser, updatedLoggedInUser, null);
		
		//Prepare prepaySubscriptionRequest OrderLineItems
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("txnid", txnid);
		model.addAttribute("paymentMethod", updatedUserSubscription.getPaymentGateway());
		model.addAttribute("amount", amount);
		//End prepare prepaySubscriptionRequest OrderLineItems
		
		requestInterceptor.addDataToSession("paymentMethod", updatedUserSubscription.getPaymentGateway());
		requestInterceptor.addDataToSession("txnid", txnid);
		requestInterceptor.addDataToSession("finalAmount", amount);
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		
		HttpSession session = request.getSession();
		session.setAttribute("showExtendPrepay", "show"); 
		session.setAttribute("showAmount", updatedLoggedInUser.getRemainingPrepayBalance());
		
		//TODO: Clear session information
		/*requestInterceptor.addDataToSession("prepayItems", null);*/
		
		return "/enduser-pages/extend-prepay-subscription/subscription-payment-success";
	}
	
	@RequestMapping(value = "/end-user/extend-prepaid/mail-prepaid")
	@ResponseBody
	public ApiResponse sendMailPostpaid() {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		
		String txnid = (String) requestInterceptor.getDataFromSession("txnid");
		String paymentMethod = (String) requestInterceptor.getDataFromSession("paymentMethod");
		Double finalAmount = (Double) requestInterceptor.getDataFromSession("finalAmount");
		List<Map<String, Object>> subscriptionItems = (List<Map<String, Object>>) requestInterceptor.getDataFromSession("subscriptionItemsForpostpaidMail");
		
		emailHelper.sendMailForExtendPrePay(userService.getLoggedInUser().getEmail(), subscriptionItems, txnid, paymentMethod, finalAmount, "confirmation mail", null);
		return apiResponse;
	}
	
	private String hashCal(String type, String str) throws NoSuchAlgorithmException {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(type);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append("0");
            }
            hexString.append(hex);
        }
        return hexString.toString();
	}
	
	// End extend prepaid subscription
	
	
	
	/*
	 * Ajax Method
	 * 
	 */
	

	/**
	 * REST API Methods
	 * USED_FROM_WEB showCalendar, newCustomerCalendar
	 */

	@RequestMapping(value = "/restapi/userSubscription/addPermenantNoteToSubscription" )
	@ResponseBody
	public ApiResponse addPermenantNoteToSubscription(  @RequestParam(required=true) String permenantNote,
														@RequestParam(required=true)Integer customerId) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUser(customerId.longValue(), false, null, false, false);
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, null, false, false);
			if(null!=userSubscription){
				if(null!=permenantNote){
					userSubscription.setPermanantNote(permenantNote);
					userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, null, false, false);
					
					Calendar calendar = Calendar.getInstance();
					Date startDate = calendar.getTime();
					DateHelper.setToEndOfDay(startDate);
					Date subscriptionDate = userSubscription.getStartDate();
					calendar.setTime(subscriptionDate);
					calendar.add(Calendar.MONTH, 2);
					Date endDate = calendar.getTime();
					DateHelper.setToEndOfDay(endDate);
					
					List<DeliverySchedule>deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(userSubscription.getUser().getId(), startDate, endDate, null, false, false, null);
					
					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						deliverySchedule.setPermanantNote(permenantNote);
						deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, null, false, false);
					}
					
					apiResponse.setSuccess(true);
				}
				
			}
			
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());

		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE
	@RequestMapping(value = "/restapi/userSubscription/updateUserSubscription/v2" ,method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse addNewUserSubscription(@RequestPart(required = true) String customerCode,
												@RequestPart(required = true) String startDate,
												@RequestPart(required = true) String types[],
												@RequestPart(required = true) String[] productCodes,
												@RequestPart(required = true) String[] quantity) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUserByCode(customerCode, true, userService.getSuperUser(), false, false);
			if(customer.getIsRouteAssigned()==true){
//			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true,userService.getLoggedInUser(),true,true);
//			if (userSubscription != null) {
//				userSubscription.setIsCurrent(false);
//				userSubscription = userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription,userService.getLoggedInUser(), true, true);
//			}
			
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, customer, true, true);
			
			if(null != userSubscriptions && !userSubscriptions.isEmpty()){
				for (UserSubscription userSubscription : userSubscriptions) {
					userSubscription.setIsCurrent(false);
					userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, customer, true, true);
				}
			}
			
			UserSubscription userSubscription = new UserSubscription();
			userSubscription.setUser(customer);
			userSubscription.setStartDate(DateHelper.parseDate(startDate));
			userSubscription.setIsCurrent(true);
			userSubscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
			for (int i = 0; i < productCodes.length; i++) {
				String type= types[i];
				String productCode = productCodes[i];
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				if(type.equalsIgnoreCase(SubscriptionType.Daily.name())){
					int quantity1 = Integer.parseInt(quantity[i]);
					
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.Daily);
					userSubscription.getSubscriptionLineItems().add(lineItem);
				}else if(type.equalsIgnoreCase(SubscriptionType.Weekly.name())){
					String[] daysQuantity = quantity[i].split("-");

					for(int j=0;j<daysQuantity.length;j++){
						SubscriptionLineItem lineItem = new SubscriptionLineItem();
						if (Integer.parseInt(daysQuantity[j]) == 0) {
							continue;
						}
						lineItem.setQuantity(Integer.parseInt(daysQuantity[j]));
						lineItem.setDay(Day.values()[j]);
						lineItem.setProduct(product);
						lineItem.setType(SubscriptionType.Weekly);
						userSubscription.getSubscriptionLineItems().add(lineItem);
					}
				}else{
					SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
					subscriptionLineItem.setProduct(product);
					subscriptionLineItem.setCustomPattern(quantity[i]);
					subscriptionLineItem.setType(SubscriptionType.Custom);
					userSubscription.getSubscriptionLineItems().add(subscriptionLineItem);
				}
			}

			//User loggedInUser = userService.getLoggedInUser();
			userSubscription = userSubscriptionService.createUserSubscription(userSubscription, customer, false, true);
			if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(customer, "statusChange", customer.getStatus()+"_"+"ACTIVE", "user", customer.getId());
			customer.setStatus(Status.ACTIVE);
			userService.updateUser(customer.getId(), customer, customer, true, true);
			apiResponse.addData("userSubscriptionDTO", new UserSubscriptionDTO(userSubscription));
			asyncJobs.createFirstTimePostpaidDeliverySchedule(userSubscription, userSubscription.getSubscriptionLineItems(), userSubscription.getStartDate(), customer);
		}else{
			apiResponse.setSuccess(false);
		}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());

		}
		return apiResponse;
	}

	//USED_FROM_WEB showcalendar.jsp
	@RequestMapping(value = "/restapi/userSubscription/addUserSubscription" )
	@ResponseBody
	public ApiResponse addUserSubscription(@RequestParam(required = true) String customerId,
										   @RequestParam(required = true) String startDate,
										   @RequestParam(required = true) String types[],
										   @RequestParam(required = true) String[] productCodes,
										   @RequestParam(required = true) String[] quantity,
										   @RequestParam(required = false) Integer[] days,
										   @RequestParam(required=false) String permenantNote,
										   @RequestParam(required=false) boolean convertToPostpay) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUser(Long.parseLong(customerId), false, null, false, false);
//			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true,userService.getLoggedInUser(),true,true);
//			if (userSubscription != null) {
//				userSubscription.setIsCurrent(false);
//				userSubscription = userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription,userService.getLoggedInUser(), true, true);
//			}
			
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, userService.getLoggedInUser(), true, true);
			
			boolean isPrepaidCustomer = false;
			if(null != userSubscriptions && !userSubscriptions.isEmpty()){
				for (UserSubscription userSubscription : userSubscriptions) {
					//Running this twice, since user can continue with prepay, and we just have to update the lineitems, and not to change isCurrent value of subscription
					if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
						isPrepaidCustomer = true;
					}
				}
			}
			
			if(isPrepaidCustomer) {
				
				double balanceToAdd = 0;
				List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(customer.getId(), DateHelper.parseDate(startDate), null, false, true);
				for(DeliverySchedule deliverySchedule:deliverySchedules) {
					for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
						balanceToAdd = balanceToAdd + lineItem.getTotalPrice();
					}
					deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
				}
				
				if(!convertToPostpay) {
					//Continue with prepay, so do not negate the available balance
					double availableBalance = customer.getLastPendingDues() + balanceToAdd;
					customer.setLastPendingDues(availableBalance);
					customer = userService.updateUser(customer.getId(), customer, null, false, false);
					//just update the line items
					UserSubscription prepaySubscription = null;
					for (UserSubscription userSubscription : userSubscriptions) {
						if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
							prepaySubscription = userSubscription;
							break;
						}
					}
					//Delete existing lineitem
					for(SubscriptionLineItem lineItem : prepaySubscription.getSubscriptionLineItems()) {
						subscriptionLineItemService.deleteSubscriptionLineItem(lineItem.getId());
					}
					Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
					for (int i = 0; i < productCodes.length; i++) {
						
						String productCode = productCodes[i];
						Product product = productService.getProductByCode(productCode, false, null, false, false);
						String type= types[i];
						if (SubscriptionType.valueOf(type) == SubscriptionType.Daily) {

							int quantity1 = Integer.parseInt(quantity[i]);
							SubscriptionLineItem lineItem = new SubscriptionLineItem();
							lineItem.setQuantity(quantity1);
							lineItem.setProduct(product);
							lineItem.setType(SubscriptionType.Daily);
							lineItem.setUserSubscription(prepaySubscription);
							lineItems.add(lineItem);
						} else if (SubscriptionType.valueOf(type) == SubscriptionType.Weekly) {
							
							int quantity1 = Integer.parseInt(quantity[i]);
							int day = days[i];
							SubscriptionLineItem lineItem = new SubscriptionLineItem();
							lineItem.setQuantity(quantity1);
							lineItem.setDay(Day.values()[day]);
							lineItem.setProduct(product);
							lineItem.setType(SubscriptionType.Weekly);
							lineItem.setUserSubscription(prepaySubscription);
							lineItems.add(lineItem);
						} else {

							SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
							subscriptionLineItem.setProduct(product);
							subscriptionLineItem.setCustomPattern(quantity[i]);
							subscriptionLineItem.setType(SubscriptionType.Custom);
							subscriptionLineItem.setUserSubscription(prepaySubscription);
							lineItems.add(subscriptionLineItem);
						}
					}
					subscriptionLineItemService.createSubscriptionLineItems(lineItems, null, false, false);
					asyncJobs.createExcessivePrepayDeliveries(customer, null, DateHelper.parseDate(startDate));
					return apiResponse;
				}
				//Negate the balance for postpaid
				double availableBalance = 0;
				if(customer.getLastPendingDues() > 0) {
					availableBalance = customer.getLastPendingDues() - balanceToAdd;
				} else {
					availableBalance = (Math.abs(customer.getLastPendingDues()) + balanceToAdd) * -1;
				}
				customer.setLastPendingDues(availableBalance);
				customer.setRemainingPrepayBalance(0);
				customer = userService.updateUser(customer.getId(), customer, null, false, false);
			}
			
			if(null != userSubscriptions && !userSubscriptions.isEmpty()){
				for (UserSubscription userSubscription : userSubscriptions) {
					userSubscription.setIsCurrent(false);
					userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, userService.getLoggedInUser(), true, true);
				}
			}

			UserSubscription userSubscription = new UserSubscription();
			userSubscription.setUser(customer);
			userSubscription.setStartDate(DateHelper.parseDate(startDate));
			userSubscription.setIsCurrent(true);
			userSubscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
			if(null!=permenantNote){
				userSubscription.setPermanantNote(permenantNote);
			}
			for (int i = 0; i < productCodes.length; i++) {
				String productCode = productCodes[i];
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				String type= types[i];
				if (SubscriptionType.valueOf(type) == SubscriptionType.Daily) {

					int quantity1 = Integer.parseInt(quantity[i]);

					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.Daily);

					userSubscription.getSubscriptionLineItems().add(lineItem);

				} else if (SubscriptionType.valueOf(type) == SubscriptionType.Weekly) {
					int quantity1 = Integer.parseInt(quantity[i]);
					
					int day = days[i];

					SubscriptionLineItem lineItem = new SubscriptionLineItem();

					lineItem.setQuantity(quantity1);
					lineItem.setDay(Day.values()[day]);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.Weekly);

					userSubscription.getSubscriptionLineItems().add(lineItem);
				} else {

					SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
					subscriptionLineItem.setProduct(product);
					subscriptionLineItem.setCustomPattern(quantity[i]);
					subscriptionLineItem.setType(SubscriptionType.Custom);
					userSubscription.getSubscriptionLineItems().add(subscriptionLineItem);
				}
			}

			User loggedInUser = userService.getLoggedInUser();
			userSubscription = userSubscriptionService.createUserSubscription(userSubscription, loggedInUser, false, true);
			apiResponse.addData("userSubscriptionDTO", new UserSubscriptionDTO(userSubscription));
			
			//update user status to active
			if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"ACTIVE", "usersuscription", customer.getId());
			customer.setStatus(Status.ACTIVE);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);

			Date fromDate = userSubscription.getStartDate();
			DateHelper.setToStartOfDay(fromDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fromDate);
			calendar.add(Calendar.MONTH, 2);
			Date endDate = calendar.getTime();
			DateHelper.setToEndOfDay(endDate);
			
			//Notifications
			//plivoSMSHelper.sendSMS(userSubscription.getUser().getMobileNumber(), "Dear customer,your provilac milk subscription has been changed from : "+userSubscription.getStartDate()+" to "+endDate);
			firebaseWrapper.sendSubscriptionUpdatedNotification(userSubscription, endDate);
			
			asyncJobs.createFirstTimePostpaidDeliverySchedule(userSubscription, userSubscription.getSubscriptionLineItems(), userSubscription.getStartDate(), loggedInUser);;

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());

		}
		return apiResponse;
	}
	
	//USED_FROM_WEB, showCalendar
	@RequestMapping(value = "/restapi/userSubscription/getCurrentUserSubscription" )
	@ResponseBody
	public ApiResponse getUserSubscription(@RequestParam(required=true)Integer customerId) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUser(customerId.longValue(), false, null, false, false);
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, null, false, true);
			if(null!=userSubscription){
				apiResponse.addData("subscription", new UserSubscriptionDTO(userSubscription));
			}
			
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());

		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/restapi/prepay/calculatePrice")
	@ResponseBody
	public ApiResponse calculatePrepayPricing(@RequestParam(required=false) String productCodes[],
									@RequestParam(required=true) String quantities[],
									@RequestParam(required=true) Integer days[],
									@RequestParam(required=true) String types[],
									@RequestParam(required=true) String startDate,
									@RequestParam(required=true) String customerId,
									@RequestParam(required=true) Integer duration) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
			for (int i = 0; i < productCodes.length; i++) {
				String productCode = productCodes[i];

				if(types[i].equalsIgnoreCase(SubscriptionType.Custom.name())){
					if(quantities[i] == null){
						continue;
					}
				} else {
					if (Integer.parseInt(quantities[i]) == 0) {
						continue;
					}
				}
				
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				String type = types[i];
				if(SubscriptionType.valueOf(type)==SubscriptionType.Daily) {
					SubscriptionLineItem lineItem = new SubscriptionLineItem();
					int quantity1= Integer.parseInt(quantities[i]);
					lineItem.setQuantity(quantity1);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.valueOf(type));
					lineItems.add(lineItem);
				} else if (SubscriptionType.valueOf(type)==SubscriptionType.Weekly) {
						int quantity1= Integer.parseInt(quantities[i]);
						int day = days[i];
						SubscriptionLineItem lineItem = new SubscriptionLineItem();
						lineItem.setQuantity(quantity1);
						lineItem.setDay(Day.values()[day]);
						
						lineItem.setProduct(product);
						lineItem.setType(SubscriptionType.valueOf(type));
						lineItems.add(lineItem);
				} else {
						SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
						subscriptionLineItem.setProduct(product);
						subscriptionLineItem.setType(SubscriptionType.Custom);
						subscriptionLineItem.setCustomPattern(quantities[i]);
						lineItems.add(subscriptionLineItem);
				}
			}
			
			Date fromDate = DateHelper.parseDate(startDate);
			DateHelper.setToStartOfDay(fromDate);
			
			Date toDate = DateHelper.addMonths(fromDate, duration);
			DateHelper.setToEndOfDay(toDate);
			double price = 0.0;
			
			for(SubscriptionLineItem lineItem : lineItems) {
				
				Date runningDate = new Date(fromDate.getTime());
				if(lineItem.getType() == SubscriptionType.Daily) {
					
					while (runningDate.compareTo(toDate) <= 0) {
						price = price + (lineItem.getProduct().getPrice() * lineItem.getQuantity());
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(lineItem.getType() == SubscriptionType.Weekly) {
					
					while(runningDate.compareTo(toDate) <= 0) {
						
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						
						if(currentDay == lineItem.getDay() && lineItem.getQuantity() > 0) {
							price = price + (lineItem.getProduct().getPrice() * lineItem.getQuantity());
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else {
					
					String[] patternArray = lineItem.getCustomPattern().split("-");
					int currentPatternIndex = 0;

					while(runningDate.compareTo(toDate) <= 0) {
						
						if(currentPatternIndex >= patternArray.length) {
							currentPatternIndex = 0;	
						}
						if(StringUtils.isNotEmpty(patternArray[currentPatternIndex])){
							int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
							if(quantity > 0) {
								price = price + (lineItem.getProduct().getPrice() * quantity);
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			
			User customer = userService.getUser(Long.parseLong(customerId), true,	null, false, false);
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, null, false, false);
			for (UserSubscription userSubscription : userSubscriptions) {
				apiResponse.addData("existingSubscriptionType", userSubscription.getSubscriptionType().name());
			}
			apiResponse.addData("pendingDues", customer.getLastPendingDues());
			apiResponse.addData("isActive", !userSubscriptions.isEmpty());
			apiResponse.addData("subscriptionPrice", price);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return apiResponse;
	}
	
	
	@RequestMapping(value = "/end-user/prepay/calculatePrice")
	@ResponseBody
	public ApiResponse addenquiryForm(@RequestParam(required=true) String duration) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		int prepayDuration = Integer.parseInt(duration);
		requestInterceptor.addDataToSession("prepayDuration", prepayDuration);
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		User loggedInUser = (User) requestInterceptor.getDataFromSession("user");
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		/*Date started = (Date) requestInterceptor.getDataFromSession("startDate");*/
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, prepayDuration);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
		}
		
		//Calculate effective prices
		List<Product> products = productService.getAllProducts(loggedInUser, false, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		LinkedList<String> effectiveItems = new LinkedList();
		LinkedList<Double> prices = new LinkedList();
		LinkedList<Double> effectivePrices = new LinkedList();
		if(key == null) {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPricingMap().get(key));
			}
		}
		
		apiResponse.addData("effectiveItems", effectiveItems);
		apiResponse.addData("prices", prices);
		apiResponse.addData("effectivePrices", effectivePrices);
		
		return apiResponse;
	}
	
	
	/**
	 * End REST API Methods
	 */
}
