package com.vishwakarma.provilac.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.repository.UserRepository;

@Controller
public class EmailController {
	
	final static Logger log = LoggerFactory.getLogger(EmailController.class);

	@Resource 
	private UserRepository userRepository;
	
	@Resource
	private EmailHelper emailHelper;
	
	@RequestMapping("/compose")
	public String compose() {
		return "email/form";
	}
	
	@RequestMapping(value="/admin/email/compose", method=RequestMethod.GET)
	public String composeEmail(Model model,
								@RequestParam(required=false)String message,
								@RequestParam(required=false)String errorMessage) {
		
		model.addAttribute("message", message);
		model.addAttribute("errorMessage", errorMessage);
		return "/admin-pages/email/compose";
	}
	
	@RequestMapping(value = "/admin/email/compose", method = RequestMethod.POST)
	public String messageSend(Model model,
								@RequestParam(required=true) MultipartFile emailsExcel,
								@RequestParam(required=false) MultipartFile attachmentFile,
								@RequestParam(required=true) String subject,
								@RequestParam(required=false) String mandrillTemplate,
								@RequestParam(required=false) String emailBody) {
		
		String message = "Emails sent successfully";
		boolean success = true;
		List<String> emailIds = new ArrayList<String>();
		try {
			emailIds = importFile(emailsExcel);
			InputStream attachmedInputStream = null;
			if(attachmentFile != null) {
				attachmedInputStream = attachmentFile.getInputStream();
			}
			if(StringUtils.isNotBlank(mandrillTemplate)) {
				emailHelper.sendMailWithMandrilTemplate(emailIds, subject, mandrillTemplate, attachmedInputStream);
			} else {
				for (String emailId : emailIds) {
					emailHelper.sendMail(emailId, emailBody, subject, attachmedInputStream);
				}
			}
			
		} catch (InvalidFormatException e) {
			e.printStackTrace();
			message = "Error while uploading file, please upload valid data";
			success = false;
		} catch (IOException e) {
			e.printStackTrace();
			message = "Error while uploading file. Check the disk space.";
			success = false;
		}
		model.addAttribute(success ? "message" : "errorMessage", message);
		return "redirect:/admin/email/compose";
	}

	private List<String> importFile(MultipartFile file)
			throws InvalidFormatException, IOException {
	
		Workbook workbook = WorkbookFactory.create(file.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		List<String> emailIds=new ArrayList<String>();
		int rowCount=0;
		for(Row row: sheet){
			
			if(rowCount==0){
				rowCount++;
				continue;
			}
			
			String emailId = null;
			if(null != row.getCell(0)){
				Cell emailCell = row.getCell(0);
				emailId = emailCell.getStringCellValue();
				emailIds.add(emailId);
			}
			rowCount++;
		}
		return emailIds;
	}
}
