package com.vishwakarma.provilac.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.CityDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.model.Country;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.CityValidator;
import com.vishwakarma.provilac.property.editor.CityPropertyEditor;
import com.vishwakarma.provilac.property.editor.CountryPropertyEditor;
import com.vishwakarma.provilac.repository.CityRepository;
import com.vishwakarma.provilac.repository.CountryRepository;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.CountryService;
import com.vishwakarma.provilac.service.LocalityService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.EncryptionUtil;


@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class CityController {

	@Resource
	private CityValidator cityValidator;

	@Resource
	private CityRepository cityRepository;
	
	@Resource
	private CityService cityService;

	@Resource
	private UserService userService;
	
	
	@Resource
	private CountryService countryService;
	
	@Resource
	private LocalityService localityService;
	
	@Resource
	private CountryRepository countryRepository;
	
	
	@InitBinder(value = "city")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(Country.class, new CountryPropertyEditor(countryRepository));
		binder.registerCustomEditor(City.class, new CityPropertyEditor(cityRepository));
		binder.setValidator(cityValidator);
	}

	private Model populateModelForAdd(Model model, City city) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("city", city);
		model.addAttribute("countries", countryService.getAllCountries(loggedInUser, false, false));
		return model;
	}

	@RequestMapping(value = "/admin/city/add")
	public String addCity(Model model) {
		model = populateModelForAdd(model, new City());
		return "admin-pages/city/add";
	}

	@RequestMapping(value = "/admin/city/add", method = RequestMethod.POST)
	public String cityAdd(City city, 
						  BindingResult formBinding, 
						  Model model){
		
		cityValidator.validate(city, formBinding);
		
		if(formBinding.hasErrors()) {
			model=populateModelForAdd(model, city);
			return "admin-pages/city/add";
		}
		User loggedInUser=userService.getLoggedInUser();
		cityService.createCity(city, loggedInUser, false, false);
		
		String message = "City added successfully";
		return "redirect:/admin/city/list?message=" + message;
	}

	@RequestMapping(value = "/admin/city/list")
	public String getCityList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<City> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = cityService.searchCity(pageNumber, searchTerm, user,true,false);
		} else {
			page = cityService.getCities(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("cities", page.getContent());

		return "/admin-pages/city/list";
	}

	@RequestMapping(value = "/admin/city/show/{cipher}")
	public String showCity(@PathVariable String cipher, Model model) {
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		City city = cityService.getCity(id, true, loggedIUser,true,false);
		model.addAttribute("city", city);
		return "/admin-pages/city/show";
	}

	@RequestMapping(value = "/admin/city/update/{cipher}")
	public String updateCity(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		City city = cityService.getCity(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, city);
		model.addAttribute("id", city.getId());
		model.addAttribute("version", city.getVersion());
		
		return "/admin-pages/city/update";
	}

	@RequestMapping(value = "/admin/city/update", method = RequestMethod.POST)
	public String cityUpdate(City city, 
								BindingResult formBinding, 
								Model model){
			
		city.setUpdateOperation(true);
		city.setId(Long.parseLong(model.asMap().get("id") + ""));
		city.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		cityValidator.validate(city, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, city);
			return "admin-pages/city/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		cityService.updateCity(city.getId(), city, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "City updated successfully";
		return "redirect:/admin/city/list?message=" + message;
	}

	@RequestMapping(value = "/admin/city/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			cityService.deleteCity(id);
			message = "City deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete City";
		}
		return "redirect:/admin/city/list?message=" + message;
	}

	/**
	 * REST API Methods
	 */

	@RequestMapping(value="/openapi/city/list")
	@ResponseBody
	public ApiResponse getAllCities(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<City>cities = cityService.getAllCities(null, false, false);
			List<CityDTO> cityDTOs = new ArrayList<CityDTO>();
			for (City city : cities) {
					cityDTOs.add(new CityDTO(city));
				}
					
		apiResponse.addData("cities", cityDTOs);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(),e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*@RequestMapping(value="/restapi/city/byCountry")
	@ResponseBody
	public ApiResponse getAllCitiesByCountry(@RequestParam(required= true)Long id){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser=userService.getLoggedInUser();
			Country country2=countryService.getCountry(id, false, loggedInUser, false, false);
			List<City>cities = cityService.getAllCitiesByCountry(country2.getCode(), loggedInUser, false, true);
			List<CityDTO> cityDTOs = new ArrayList<CityDTO>();
			for (City city : cities) {
					cityDTOs.add(new CityDTO(city));
				
			}
		apiResponse.addData("cities", cityDTOs);
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	*/

	/**
	 * End REST API Methods
	 */
}
