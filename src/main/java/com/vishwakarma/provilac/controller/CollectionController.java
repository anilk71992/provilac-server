package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.CollectionDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Collection;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.CollectionValidator;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CollectionService;
import com.vishwakarma.provilac.service.DepositService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

/**
 * 
 * @author Harshal
 *
 */
@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class CollectionController {

	@Resource
	private CollectionValidator collectionValidator;

	@Resource
	private CollectionService collectionService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private DepositService depositService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@InitBinder(value = "collection")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.setValidator(collectionValidator);
	}

	private Model populateModelForAdd(Model model, Collection collection) {
		model.addAttribute("collection", collection);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/collection/add")
	public String addCollection(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Collection());
		return "admin-pages/collection/add";
	}

	@RequestMapping(value = "/admin/collection/add", method = RequestMethod.POST)
	public String collectionAdd(Collection collection, 
									BindingResult formBinding, 
									Model model){
		
		collectionValidator.validate(collection, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, collection);
			return "admin-pages/collection/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		collection = collectionService.createCollection(collection, loggedInUser, true,true);
		String message = "Collection added successfully";
		return "redirect:/admin/collection/list?message=" + message;
	}

	@RequestMapping(value = "/admin/collection/list")
	public String getCollectionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm,
										@RequestParam(required = false) String userCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Collection> page = null;
		User user = userService.getLoggedInUser();
		if(null!=userCode){
			page = collectionService.getCollectionsByCustomerCode(userCode, pageNumber, user, false, false);
		}else{
			if (StringUtils.isNotBlank(searchTerm)) {
				page = collectionService.searchCollection(pageNumber, searchTerm, user,true,false);
			} else {
				page = collectionService.getCollections(pageNumber, user, true, true);
			}
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("collections", page.getContent());

		return "/admin-pages/collection/collectionLedger";
	}

	@RequestMapping(value = "/admin/collection/show/{cipher}")
	public String showCollection(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Collection collection = collectionService.getCollection(id, true, loggedIUser,true,false);
		model.addAttribute("collection", collection);
		return "/admin-pages/collection/show";
	}

	@RequestMapping(value = "/admin/collection/update/{cipher}")
	public String updateCollection(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Collection collection = collectionService.getCollection(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, collection);
		model.addAttribute("id", collection.getId());
		model.addAttribute("version", collection.getVersion());
		return "/admin-pages/collection/update";
	}

	@RequestMapping(value = "/admin/collection/update", method = RequestMethod.POST)
	public String collectionUpdate(Collection collection, 
										BindingResult formBinding, 
										Model model) {

		collection.setUpdateOperation(true);
		collectionValidator.validate(collection, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, collection);
			return "admin-pages/collection/update";
		}
		collection.setId(Long.parseLong(model.asMap().get("id") + ""));
		collection.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		collectionService.updateCollection(collection.getId(), collection, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Collection updated successfully";
		return "redirect:/admin/collection/list?message=" + message;
	}

	@RequestMapping(value = "/admin/collection/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			collectionService.deleteCollection(id);
			message = "Collection deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Collection";
		}
		return "redirect:/admin/collection/list?message=" + message;
	}
	
	@RequestMapping(value="/restapi/collection/list")
	@ResponseBody
	public ApiResponse getCollectionsByCustomer(@RequestParam(defaultValue = "1") Integer pageNumber,
												@RequestParam(required=true)String customerCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Page<Collection> page =  collectionService.getAllCollectionsByCollectionBoy(pageNumber,customerCode, userService.getLoggedInUser(), false, true);
			List<CollectionDTO>dtos = new ArrayList<CollectionDTO>();
			
			for (Collection collection : page) {
				dtos.add(new CollectionDTO(collection));
			}
			
			int current = page.getNumber() + 1;
			int begin = Math.max(1, current - 5);
			int end = Math.min(begin + 10, page.getTotalPages());

			apiResponse.addData("Collections", dtos).addData("current", current).addData("begin", begin).addData("end", end);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/collection/details")
	@ResponseBody
	public ApiResponse getCollectionDetails(@RequestParam(required=true)String collectionCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Collection collection =  collectionService.getCollectionByCode(collectionCode, false, null, false, false);
			apiResponse.addData("Collection", new CollectionDTO(collection));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE
	@RequestMapping(value="/restapi/collection/addCollection", method= RequestMethod.POST)
	@ResponseBody
	public ApiResponse addCollectionByCollectionBoy(@RequestPart(required=true)Double[] amounts,
												 	@RequestPart(required=true)String[] customerCodes,
												 	@RequestPart(required=true)String[] paymentMethods,
												 	@RequestPart(required=true)String[] bankNames,
												 	@RequestPart(required=true)String[] chequeNumbers,
												 	@RequestPart(required=true)String[] receiptNumbers,
												 	@RequestPart(required=true)String[] transactionIds,
												 	@RequestPart(required=true)String[] invoiceCodes,
												 	@RequestPart(required=true) String routeCode) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Payment> payments = new ArrayList<Payment>();
			User collectionBoy = userService.getLoggedInUser();

			Calendar calendar = Calendar.getInstance();
			
			calendar.add(Calendar.MONTH, -1);
			calendar.set(calendar.DATE, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date fromDate = calendar.getTime();
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.add(Calendar.MONTH, -1);
			calendar2.set(Calendar.DATE, calendar2.getActualMaximum(Calendar.DATE)); 
			calendar2.set(Calendar.HOUR_OF_DAY, 0);
		    calendar2.set(Calendar.MINUTE, 0);
			calendar2.set(Calendar.SECOND, 0);
			calendar2.set(Calendar.MILLISECOND, 0);
			Date toDate = calendar2.getTime();
			
			double chequeAmount = 0.0;
			double cashAmount = 0.0;
			int noOfCheques = 0;			
			if(null!=customerCodes){
				for (int i = 0; i < customerCodes.length; i++) {
					Payment payment = new Payment();
					String customerCode = customerCodes[i];
					User customer = userService.getUserByCode(customerCode, true, collectionBoy, false, false);
					payment.setCustomer(customer);
					payment.setAmount(amounts[i]);
					payment.setReceivedBy(collectionBoy);
					if(null!=paymentMethods[i]){
						if(paymentMethods[i].equals(PaymentMethod.Cheque.name())){
							if(null!=bankNames[i]){
								payment.setBankName(bankNames[i]);
							}
							if(null!=chequeNumbers[i]){
								payment.setChequeNumber(chequeNumbers[i]);
								chequeAmount += amounts[i];
								noOfCheques += 1;
							}
							if(null!=receiptNumbers[i]){
								payment.setReceiptNo(receiptNumbers[i]);
							}
						}else{
							if(null!=receiptNumbers[i]){
								payment.setReceiptNo(receiptNumbers[i]);
							}
							cashAmount += amounts[i];
						}
						
						//cashback logic 
						double cashback = 0.0;
						double dues = customer.getLastPendingDues();
						float minBalanceForCashback = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.MIN_BALANCE_FOR_CASHBACK, "0.0"));
						float cashbackPercentage = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.CASHBACK_PECENTAGE, "0.0")) / 100;
						if(dues > 0 && (payment.getAmount() - dues) >= minBalanceForCashback) {
							//User has paid amount which makes dues zero and supasses min amount required for cashback
							cashback = (payment.getAmount() - dues) * cashbackPercentage;
						} else if(dues <= 0 && payment.getAmount() >= minBalanceForCashback) {
							//If dues are less than zero, and still user is paying more than min amount required for cashback
							cashback = payment.getAmount() * cashbackPercentage;
						}
						
						payment.setCashback(cashback);
						payment.setAmount(payment.getAmount() + payment.getCashback());
						
						payment.setPaymentMethod(PaymentMethod.valueOf(paymentMethods[i]));
					}
					if(null!=transactionIds[i]){
						payment.setTxnId(transactionIds[i]);
					}
					if(null != invoiceCodes[i]){
						Invoice invoice=invoiceService.getInvoiceByCode(invoiceCodes[i], false, userService.getLoggedInUser(), false, false);
						if(null != invoice)
						payment.setInvoice(invoice);
					}
					payment.setDate(new Date());
					
					payment = paymentService.createPayment(payment, collectionBoy, false, false);
					
					double lastpendingDues = customer.getLastPendingDues();
					
					if (payment.getAdjustmentAmount()!=0){
						lastpendingDues = lastpendingDues - (payment.getAmount()+payment.getAdjustmentAmount());
					}else{
						
						lastpendingDues = lastpendingDues - payment.getAmount();
					}
					
					customer.setLastPendingDues(lastpendingDues);
					userService.updateUser(customer.getId(), customer, collectionBoy, true, true);
					
					Invoice invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate,collectionBoy, false, false);
					if (null != invoice) {
						invoice.setIsPaid(true);
						//invoice.setLastPendingDues(lastpendingDues);
						invoiceService.updateInvoice(invoice.getId(), invoice, collectionBoy, false, false);
					}
					payments.add(payment);
				}
			}
			
			Collection collection = new Collection();
			
			collection.setCollectionBoy(collectionBoy);
			collection.setChequeAmount(chequeAmount);
			collection.setCashAmount(cashAmount);
			collection.setTotalAmount(chequeAmount+cashAmount);
			collection.setNoOfCheques(noOfCheques);
			collection.setPayments(payments);
			collection.setDate(new Date());
			Route route = routeService.getRouteByCode(routeCode, false, collectionBoy, false, false);
			collection.setRoute(route);
			
			collection = collectionService.createCollection(collection, collectionBoy, false, false);
			
			//Update CollectionBoy Amount
			collectionBoy.setCollectionOuststanding(collectionBoy.getCollectionOuststanding()+(chequeAmount+cashAmount));
			userService.updateCollectionBoyCollectionOutstanding(collectionBoy.getId(), collectionBoy, collectionBoy, false, false);
			
			for (Payment existingPayment : payments) {
				existingPayment.setCollection(collection);
				paymentService.updatePayment(existingPayment.getId(), existingPayment, collectionBoy, false, false);
			}
			
			apiResponse.addData("collection", new CollectionDTO(collection));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	
//	@RequestMapping(value="/restapi/collection/addCollection", method= RequestMethod.GET)
//	@ResponseBody
//	public ApiResponse addCollectionByCollectionBo(@RequestParam(required=true)String[] amounts,
//			@RequestParam(required=true)String[] customerCodes,
//			@RequestParam(required=true)String[] paymentMethods,
//			@RequestParam(required=true)String[] bankNames,
//			@RequestParam(required=true)String[] chequeNumbers,
//			@RequestParam(required=true)String[] receiptNumbers,
//			@RequestParam(required=true)String[] transactionIds,
//			@RequestParam(required=true)String collectionBoyCode,
//			@RequestParam(required=true)double totalAmount,
//			@RequestParam(required=true)double cashAmount,
//			@RequestParam(required=true)double chequeAmount,
//			@RequestParam(required=true)Integer noOfCheque){
//		
//		ApiResponse apiResponse = new ApiResponse(true);
//		try{
//			//List<PaymentDTO> paymentDTOs = new ArrayList<PaymentDTO>();
//			List<Payment> payments = new ArrayList<Payment>();
//			User loggedInUser = userService.getLoggedInUser();
//
//			Calendar calendar = Calendar.getInstance();
//			
//			calendar.add(Calendar.MONTH, -1);
//			calendar.set(calendar.DATE, 1);
//			calendar.set(Calendar.HOUR_OF_DAY, 0);
//		    calendar.set(Calendar.MINUTE, 0);
//			calendar.set(Calendar.SECOND, 0);
//			calendar.set(Calendar.MILLISECOND, 0);
//			Date fromDate = calendar.getTime();
//			
//			Calendar calendar2 = Calendar.getInstance();
//			calendar2.add(Calendar.MONTH, -1);
//			calendar2.set(Calendar.DATE, calendar2.getActualMaximum(Calendar.DATE)); 
//			calendar2.set(Calendar.HOUR_OF_DAY, 0);
//		    calendar2.set(Calendar.MINUTE, 0);
//			calendar2.set(Calendar.SECOND, 0);
//			calendar2.set(Calendar.MILLISECOND, 0);
//			Date toDate = calendar2.getTime();
//			
//			Collection collection = new Collection();
//			User collectionBoy = userService.getUserByCode(collectionBoyCode, true, loggedInUser);
//			collection.setCollectionBoy(collectionBoy);
//			collection.setChequeAmount(chequeAmount);
//			collection.setCashAmount(cashAmount);
//			collection.setTotalAmount(totalAmount);
//			collection.setNoOfCheque(noOfCheque);
//			
//			if(null!=customerCodes){
//				for (int i = 0; i < customerCodes.length; i++) {
//					Payment payment = new Payment();
//					String customerCode = customerCodes[i];
//					User customer = userService.getUserByCode(customerCode, true, loggedInUser);
//					payment.setCustomer(customer);
//					payment.setAmount(Double.parseDouble(amounts[i]));
//					if(null!=paymentMethods[i]){
//						if(paymentMethods[i].equals(PaymentMethod.Cheque) || paymentMethods[i].equals(PaymentMethod.NEFT)){
//							if(null!=bankNames[i]){
//								payment.setBankName(bankNames[i]);
//							}
//							if(null!=chequeNumbers[i]){
//								payment.setChequeNumber(chequeNumbers[i]);
//							}
//						}else{
//							if(null!=receiptNumbers[i]){
//								payment.setReceiptNo(receiptNumbers[i]);
//							}
//						}
//						payment.setPaymentMethod(PaymentMethod.valueOf(paymentMethods[i]));
//					}
//					if(null!=transactionIds[i]){
//						payment.setTxnId(transactionIds[i]);
//					}
//					
//					payment.setDate(new Date());
//					
//					payment = paymentService.createPayment(payment, loggedInUser, false, false);
//					
//					double lastpendingDues = customer.getLastPendingDues();
//					
//					if (payment.getAdjustmentAmount()!=0){
//						lastpendingDues = lastpendingDues - (payment.getAmount()+payment.getAdjustmentAmount());
//					}else{
//						
//						lastpendingDues = lastpendingDues - payment.getAmount();
//					}
//					
//					customer.setLastPendingDues(lastpendingDues);
//					userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
//					
//					Invoice invoice = invoiceService.getInvoiceByCustomerAndDateRange(customer.getId(), fromDate, toDate, loggedInUser, false, false);
//					if (null != invoice) {
//						invoice.setIsPaid(true);
//						invoice.setLastPendingDues(lastpendingDues);
//						invoiceService.updateInvoice(invoice.getId(), invoice, loggedInUser, false, false);
//					}
//					payments.add(payment);
//					//paymentDTOs.add(new PaymentDTO(payment));
//				}
//			}
//			collection.setPayments(payments);
//			collection = collectionService.createCollection(collection, loggedInUser, false, false);
//			
//			for (Payment existingPayment : payments) {
//				existingPayment.setCollectionId(collection.getId());
//				paymentService.updatePayment(existingPayment.getId(), existingPayment, loggedInUser, false, false);
//			}
//			
//			apiResponse.addData("collection", new CollectionDTO(collection));
//		}catch(ProvilacException e){
//			e.printStackTrace();
//			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
//		}
//		return apiResponse;
//	}
	
	/** 
	 * End REST API Methods
	 */
}
