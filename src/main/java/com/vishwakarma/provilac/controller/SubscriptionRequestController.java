package com.vishwakarma.provilac.controller;

import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.SubscriptionRequestDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.SubscriptionRequest;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.mvc.validator.SubscriptionRequestValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RequestLineItemService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionRequestService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class SubscriptionRequestController {

	@Resource
	private SubscriptionRequestValidator subscriptionRequestValidator;

	@Resource
	private SubscriptionRequestService subscriptionRequestService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource 
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private RequestLineItemService requestLineItemService;
	
	@Resource AsyncJobs asyncJobs;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@InitBinder(value = "subscriptionRequest")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(subscriptionRequestValidator);
	}

	private Model populateModelForAdd(Model model, SubscriptionRequest subscriptionRequest) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("subscriptionRequest", subscriptionRequest);
		List<User> newCustomers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.NEW, loggedInUser, false, false);
		model.addAttribute("newCustomers", newCustomers);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}



	@RequestMapping(value = "/admin/subscriptionRequest/list")
	public String getUserSubscriptionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<SubscriptionRequest> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = subscriptionRequestService.searchSubscriptionRequests(pageNumber, searchTerm, user, true, true);
		} else {
			page = subscriptionRequestService.getSubscriptionRequests(pageNumber, user, true, true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("subscriptionRequest", page.getContent());

		return "/admin-pages/subscriptionRequest/list";
	}

	
	@RequestMapping(value = "/admin/subscriptionRequest/changeList")
	public String getChangeUserSubscriptionList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<SubscriptionRequest> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = subscriptionRequestService.searchChangeSubscriptionRequests(pageNumber, searchTerm, user, true, true);
		} else {
			page = subscriptionRequestService.getChangeSubscriptionRequests(pageNumber, user, true, true);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("subscriptionRequest", page.getContent());

		return "/admin-pages/subscriptionRequest/changeList";
	}

	@RequestMapping(value = "/admin/subscriptionRequest/show/{cipher}")
	public String showSubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		SubscriptionRequest subscriptionRequest =subscriptionRequestService.getSubscriptionRequest(id, true, loggedInUser,true,true);
		//asyncJobs.createFirstTimeDeliverySchedule(userSubscription, userSubscription.getSubscriptionLineItems(),userSubscription.getStartDate(),loggedInUser);
		model.addAttribute("subscriptionRequest", subscriptionRequest);
		return "/admin-pages/subscriptionRequest/show";
	}
	
	@RequestMapping(value = "/admin/subscriptionRequest/deleteNew/{cipher}")
	public String deleteSubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		SubscriptionRequest subscriptionRequest =subscriptionRequestService.getSubscriptionRequest(id, true, loggedInUser,true,true);
		subscriptionRequestService.deleteSubscriptionRequest(subscriptionRequest.getId());
		return "/admin-pages/subscriptionRequest/list";
	}
	
	@RequestMapping(value = "/admin/subscriptionRequest/deleteChange/{cipher}")
	public String deleteChangeSubscriptionRequest(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		SubscriptionRequest subscriptionRequest =subscriptionRequestService.getSubscriptionRequest(id, true, loggedInUser,true,true);
		subscriptionRequestService.deleteSubscriptionRequest(subscriptionRequest.getId());
		return "/admin-pages/subscriptionRequest/changeList";
	}
	
	@RequestMapping(value="/admin/subscriptionRequest/assignRoute/{userCode}/{requestCode}")
	public String changeRoute(Model model, @PathVariable String userCode,@PathVariable String requestCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		User user = userService.getUserByCode(userCode, false, loggedInUser, false, false);
		SubscriptionRequest subscriptionRequest = subscriptionRequestService.getSubscriptionRequestByCode(requestCode, false, loggedInUser, false, false);
		model.addAttribute("request", subscriptionRequest);
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		return "/admin-pages/subscriptionRequest/assignRoute";
	}
	
	@RequestMapping(value="/admin/subscriptionRequest/assignRoute", method=RequestMethod.POST)
	public String assignRoute(@RequestParam(required=false , value="selectedRoute") String selectedRouteCode,
							  @RequestParam(required=false,  value="selectedCustomer") String selectedCustomerCode,
							  @RequestParam(required =true,value="user")String customerCode,
							  @RequestParam(required=false,value="routeName")String routeName,
							  @RequestParam(required=false,value="requestCode")String requestCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		User customer = userService.getUserByCode(customerCode, false, loggedInUser, false, false);
		Route route = routeService.getRouteByCode(selectedRouteCode, false, null, false, true);
		if(null!=selectedCustomerCode){
			RouteRecord routeRecordForSelectedCustomer = routeRecordService.getRouteRecordByCustomer(selectedCustomerCode, false, loggedInUser, false, false);
			List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);
			RouteRecord	routeRecord = new RouteRecord();
			routeRecord.setCustomer(customer);
			routeRecord.setPriority(routeRecordForSelectedCustomer.getPriority());
			routeRecord.setRoute(route);
			route.getRouteRecords().add(routeRecord);
			routeService.updateRoute(route.getId(), route, loggedInUser, false, false);
			routeRecord = routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			
			//Update route of DeliverySchedules of customer whose route is changed
			 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
			
			//is route assign mark true when route is assign 
			customer.setIsRouteAssigned(true);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
			
			Route lastRoute = null;
			for(RouteRecord existingRouteRecord:existingRouteRecords){
				lastRoute = routeRecord.getRoute();
				routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
			}
			
			if(null != lastRoute) {
				deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
				DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
				deletedRouteRecord.setRoute(lastRoute);
				deletedRouteRecord.setCustomer(customer);
				deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			}
		} else {
				List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);					
				RouteRecord routeRecord = new RouteRecord();
				routeRecord.setCustomer(customer);
				routeRecord.setPriority(1);
				routeRecord.setRoute(route);
				routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);

				//Update route of DeliverySchedules of customer whose route is changed
				 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
				
				customer.setIsRouteAssigned(true);
				userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
				
				Route lastRoute = null;
				for(RouteRecord existingRouteRecord:existingRouteRecords){
					lastRoute = routeRecord.getRoute();
					routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
				}
				
				if(null != lastRoute) {
					deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
					DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
					deletedRouteRecord.setRoute(lastRoute);
					deletedRouteRecord.setCustomer(customer);
					deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
				}
		}
		
		SubscriptionRequest request = subscriptionRequestService.getSubscriptionRequestByCode(requestCode, false, null, false, true);
		
		UserSubscription subscription  = new UserSubscription();
		subscription.setUser(request.getUser());
		
		if(request.getStartDate().before(new Date())) {
			request.setStartDate(new Date());
			DateHelper.addDays(request.getStartDate(), 1);
		}
		subscription.setStartDate(request.getStartDate());
		
		boolean isPrepaidCustomer = false;
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customerCode, true, loggedInUser, false, false);
		for (UserSubscription userSubscription : userSubscriptions) {
			if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
				isPrepaidCustomer =true;
			}
			userSubscription.setIsCurrent(false);
			userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, loggedInUser, false, false);
		}
		subscription.setIsCurrent(true);
		subscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
		
		for (RequestLineItem item : request.getRequestLineItems()) {
			SubscriptionLineItem lineItem = new SubscriptionLineItem();
			lineItem.setCurrentIndex(item.getCurrentIndex());
			if(item.getType().equals(SubscriptionType.Daily)){
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
			} else if(item.getType().equals(SubscriptionType.Weekly)) {
				lineItem.setDay(item.getDay());
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
				
			} else {
				lineItem.setCustomPattern(item.getCustomPattern());
				lineItem.setQuantity(item.getQuantity());
				lineItem.setProduct(item.getProduct());
				lineItem.setType(item.getType());
				subscription.getSubscriptionLineItems().add(lineItem);
			}
		}

		subscription = userSubscriptionService.createUserSubscription(subscription, loggedInUser, false, true);
		if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
			activityLogService.createActivityLogForChangeStatus(loggedInUser, "changeStatus", customer.getStatus()+"_"+"ACTIVE", "subscriptionRequest", customer.getId());
		customer.setStatus(Status.ACTIVE);
		
		if(isPrepaidCustomer && customer.getLastPendingDues() > 0) {
			customer.setLastPendingDues(customer.getLastPendingDues() * -1);
		}
		
		userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
		
		//subscription request delete after subscription  create 
		subscriptionRequestService.deleteSubscriptionRequest(request.getId());
		
		asyncJobs.createFirstTimePostpaidDeliverySchedule(subscription, subscription.getSubscriptionLineItems(), subscription.getStartDate(), loggedInUser);
		
		if (customer.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "Route Changed successfully";
			return "redirect:/admin/user/getCustomerlist?message="+message;	
		}
		
		String message = "Route Changed successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/admin/subscriptionRequest/approve/{requestCode}")
	public String approveChangeSubscriptionRequest(@PathVariable String requestCode) {
		
		SubscriptionRequest request = subscriptionRequestService.getSubscriptionRequestByCode(requestCode, false, null, false, true);
		if(null != request){
			User customer=request.getUser();
			
			boolean isPrepaidCustomer = false;
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customer.getCode(), true, customer, true, true);
			for (UserSubscription userSubscription : userSubscriptions) {
				if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
					isPrepaidCustomer = true;
				}
				userSubscription.setIsCurrent(false);
				userSubscriptionService.updateUserSubscription(userSubscription.getId(), userSubscription, customer, true, true);
			}
			
			UserSubscription subscription  = new UserSubscription();
			subscription.setUser(request.getUser());
			
			if(request.getStartDate().before(new Date())) {
				request.setStartDate(new Date());
				DateHelper.addDays(request.getStartDate(), 1);
			}
			subscription.setStartDate(request.getStartDate());
			subscription.setIsCurrent(true);
			subscription.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
			
			for (RequestLineItem item : request.getRequestLineItems()) {
				SubscriptionLineItem lineItem = new SubscriptionLineItem();
				lineItem.setCurrentIndex(item.getCurrentIndex());
				if(item.getType().equals(SubscriptionType.Daily)) {
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				} else if(item.getType().equals(SubscriptionType.Weekly)) {
					lineItem.setDay(item.getDay());
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				} else {
					lineItem.setCustomPattern(item.getCustomPattern());
					lineItem.setQuantity(item.getQuantity());
					lineItem.setProduct(item.getProduct());
					lineItem.setType(item.getType());
					subscription.getSubscriptionLineItems().add(lineItem);
				}
			}
	
			subscription = userSubscriptionService.createUserSubscription(subscription, userService.getLoggedInUser(), false, true);
			if(!customer.getStatus().name().equalsIgnoreCase("ACTIVE"))
				activityLogService.createActivityLogForChangeStatus(userService.getLoggedInUser(), "changeStatus", customer.getStatus() + "_" + "ACTIVE", "subscriptionRequest", customer.getId());
			customer.setStatus(Status.ACTIVE);
			
			if(isPrepaidCustomer && customer.getLastPendingDues() > 0) {
				customer.setLastPendingDues(customer.getLastPendingDues() * -1);
			}
			
			userService.updateUser(customer.getId(), customer, userService.getLoggedInUser(), true, true);
			
			//subscription request delete after subscription  create 
			subscriptionRequestService.deleteSubscriptionRequest(request.getId());
			
			asyncJobs.createFirstTimePostpaidDeliverySchedule(subscription, subscription.getSubscriptionLineItems(), subscription.getStartDate(), userService.getLoggedInUser());
		}
		String message = "Subscription Request Approved successfully";
		return "redirect:/admin/subscriptionRequest/changeList?message="+message;
	}
	
	/*
	 * REST API Methods.
	 * USED_FROM_DEVICE
	 */
	@RequestMapping(value = "/restapi/subscriptionRequest/addSubscriptionRequest" ,method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse addNewSubscriptionRequest(@RequestPart(required = true) String customerCode,
												@RequestPart(required = true) String startDate,
												@RequestPart(required = true) String types[],
												@RequestPart(required = true) String[] productCodes,
												@RequestPart(required = true) String[] quantity,
												@RequestPart(required = false) String addressCode) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUserByCode(customerCode, true, userService.getSuperUser(), false, false);
			
			//delete all existing subscription request..
			List<SubscriptionRequest> subscriptionRequests=subscriptionRequestService.getSubscriptionRequestsByCustomer(customer.getCode(),null, false, false);
			for(SubscriptionRequest subscriptionRequest:subscriptionRequests){
				subscriptionRequestService.deleteSubscriptionRequest(subscriptionRequest.getId());
			}
 			
			
			List<Address> addresses = addressService.getAddressesByCustomer(customer.getCode(), null, false, false);
			for (Address address : addresses) {
				if(address.getCode().equals(addressCode)){
					address.setIsDefault(true);
					customer.setLat(address.getLat());
					customer.setLng(address.getLng());
					userService.updateLocation(customer.getId(), customer, null, false, false);
				}else{
					address.setIsDefault(false);
				}
				addressService.updateAddress(address.getId(), address, null, false, false);
			}
				
			SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
			subscriptionRequest.setUser(customer);
			subscriptionRequest.setStartDate(DateHelper.parseDate(startDate));
			subscriptionRequest.setIsSubscriptionRequest(true);
			for (int i = 0; i < productCodes.length; i++) {
				String type= types[i];
				String productCode = productCodes[i];
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				if(type.equalsIgnoreCase(SubscriptionType.Daily.name())){
					int quantity1 = Integer.parseInt(quantity[i]);
					
					RequestLineItem lineItem = new RequestLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.Daily);
					subscriptionRequest.getRequestLineItems().add(lineItem);
				}else if(type.equalsIgnoreCase(SubscriptionType.Weekly.name())){
					String[] daysQuantity = quantity[i].split("-");

					for(int j=0;j<daysQuantity.length;j++){
						RequestLineItem lineItem = new RequestLineItem();
						if (Integer.parseInt(daysQuantity[j]) == 0) {
							continue;
						}
						lineItem.setQuantity(Integer.parseInt(daysQuantity[j]));
						lineItem.setDay(Day.values()[j]);
						lineItem.setProduct(product);
						lineItem.setType(SubscriptionType.Weekly);
						subscriptionRequest.getRequestLineItems().add(lineItem);
					}
				}else{
					RequestLineItem requestLineItem = new RequestLineItem();
					requestLineItem.setProduct(product);
					requestLineItem.setCustomPattern(quantity[i]);
					requestLineItem.setType(SubscriptionType.Custom);
					subscriptionRequest.getRequestLineItems().add(requestLineItem);
				}

			}

			User loggedInUser = userService.getLoggedInUser();
			subscriptionRequest = subscriptionRequestService.createSubscriptionRequest(subscriptionRequest, loggedInUser, false, true);
			firebaseWrapper.sendNewSubscriptionRequestAddedNotification(subscriptionRequest);
			apiResponse.addData("userSubscriptionRequestDTO", new SubscriptionRequestDTO(subscriptionRequest));
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE
	@RequestMapping(value = "/restapi/subscriptionRequest/changeSubscriptionRequest" ,method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse changeSubscriptionRequest(@RequestPart(required = true) String customerCode,
												@RequestPart(required = true) String startDate,
												@RequestPart(required = true) String types[],
												@RequestPart(required = true) String[] productCodes,
												@RequestPart(required = true) String[] quantity) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User customer = userService.getUserByCode(customerCode, true, userService.getSuperUser(), false, false);
			User loggedInUser = userService.getLoggedInUser();
			
			//delete all existing subscription request..
			subscriptionRequestService.deleteSubscriptionRequestsForUser(customer.getId());
			
			SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
			subscriptionRequest.setUser(customer);
			subscriptionRequest.setStartDate(DateHelper.parseDate(startDate));
			subscriptionRequest.setIsSubscriptionRequest(true);
			
			List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(customerCode, true, loggedInUser, false, false);
			subscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
			
			for (int i = 0; i < productCodes.length; i++) {
				String type= types[i];
				String productCode = productCodes[i];
				Product product = productService.getProductByCode(productCode, false, null, false, false);
				
				if(type.equalsIgnoreCase(SubscriptionType.Daily.name())) {
					int quantity1 = Integer.parseInt(quantity[i]);
					RequestLineItem lineItem = new RequestLineItem();
					lineItem.setQuantity(quantity1);
					lineItem.setProduct(product);
					lineItem.setType(SubscriptionType.Daily);
					subscriptionRequest.getRequestLineItems().add(lineItem);
				} else if(type.equalsIgnoreCase(SubscriptionType.Weekly.name())) {
					String[] daysQuantity = quantity[i].split("-");
					for(int j=0;j<daysQuantity.length;j++){
						RequestLineItem lineItem = new RequestLineItem();
						if (Integer.parseInt(daysQuantity[j]) == 0) {
							continue;
						}
						lineItem.setQuantity(Integer.parseInt(daysQuantity[j]));
						lineItem.setDay(Day.values()[j]);
						lineItem.setProduct(product);
						lineItem.setType(SubscriptionType.Weekly);
						subscriptionRequest.getRequestLineItems().add(lineItem);
					}
				} else {
					RequestLineItem requestLineItem = new RequestLineItem();
					requestLineItem.setProduct(product);
					requestLineItem.setCustomPattern(quantity[i]);
					requestLineItem.setType(SubscriptionType.Custom);
					subscriptionRequest.getRequestLineItems().add(requestLineItem);
				}
			}

			subscriptionRequest = subscriptionRequestService.createSubscriptionRequest(subscriptionRequest, loggedInUser, false, true);
			firebaseWrapper.sendChangeSubscriptionRequestAddedNotification(subscriptionRequest);
			apiResponse.addData("userSubscriptionRequestDTO", new SubscriptionRequestDTO(subscriptionRequest));
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getErrorCode(), e.getExceptionMsg());
		}
		return apiResponse;
	}
	
	/**
	 * End REST API Methods
	 */
	
	@RequestMapping(value = "/postpay-terms-conditions")
	public String postpayTermsConditions(Model model) {
		
		return "/enduser-pages/create-subscription/postpay-subscription/terms-conditions";
	}
	
	@RequestMapping(value = "/postpay-subscription-checkout")
	public String prepaySubscriptionCheckout(Model model, @RequestParam(required=false) String message) {
		
		User loggedInUser = userService.getLoggedInUser();
		List<Address> address = addressService.getAddressesByCustomer(loggedInUser.getCode(), loggedInUser, true, false);
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		Date startDate = new Date();
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, 1);
		DateHelper.setToEndOfDay(endDate);
		Double averageMonthlyBill = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			averageMonthlyBill = averageMonthlyBill + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("averageMonthlyBill", averageMonthlyBill);
		requestInterceptor.addDataToSession("averageMonthlyBill", averageMonthlyBill);
		
		model.addAttribute("addresss", address);
		model.addAttribute("message", message);
		model.addAttribute("today", DateHelper.getFormattedDateForDevice(new Date()));
		return "/enduser-pages/create-subscription/postpay-subscription/postpay-subscription-checkout";
	}
	
	@RequestMapping(value="/postpay/setaddress")
	public String setAddressAndForwardPostpayConfirmation(Authentication authentication, @RequestParam String addressCipher,
					@RequestParam String deliveryFromDate, Model model, HttpServletRequest request) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		Long addressId = Long.valueOf(EncryptionUtil.decode(addressCipher));
		Address address = addressService.getAddress(addressId, true, userInfo.getUser(), false, false);
		List<Address> addresses = addressService.getAddressesByCustomer(userInfo.getUser().getCode(), null, false, false);
		for (Address existingAddress : addresses) {
			existingAddress.setIsDefault(false);
			addressService.updateAddress(existingAddress.getId(), existingAddress, null, false, false);
		}
		address.setIsDefault(true);
		addressService.updateAddress(address.getId(), address, null, false, false);
		
		requestInterceptor.addDataToSession("address", address);
		
		Date startDate = DateHelper.parseDateFormatForDevice(deliveryFromDate);
		DateHelper.setToStartOfDay(startDate);
		String deliveryMode = (String) requestInterceptor.getDataFromSession("deliveryMode");
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setStartDate(startDate);
		subscriptionRequest.setUser(user);
		subscriptionRequest.setIsSubscriptionRequest(true);
		subscriptionRequest.setPermanantNote(deliveryMode);
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, user, false, false);
		subscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null)
			return "redirect:/";
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			subscribedProductCodes.add(subscribedProduct.getCode());
			
			RequestLineItem lineItem = new RequestLineItem();
			lineItem.setType(subscriptionLineItem.getType());
			lineItem.setDay(subscriptionLineItem.getDay());
			lineItem.setQuantity(subscriptionLineItem.getQuantity());
			lineItem.setProduct(subscriptionLineItem.getProduct());
			lineItem.setCustomPattern(subscriptionLineItem.getCustomPattern());
			subscriptionRequest.getRequestLineItems().add(lineItem);
		}
		
		subscriptionRequest = subscriptionRequestService.createSubscriptionRequest(subscriptionRequest, user, false, true);
		
		//Prepare subscriptionRequest OrderLineItems
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCode = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCode.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date endDate = DateHelper.addMonths(startDate, 1);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		model.addAttribute("subscriptionItems", subscriptionItems);
		//End Prepare subscriptionRequest OrderLineItems
		HttpSession session = request.getSession();
		session.setAttribute("showExtendPrepay", null); 
		session.setAttribute("showAmount", user.getLastPendingDues());
		
		/*requestInterceptor.addDataToSession("prepayItems", null);*/
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		requestInterceptor.addDataToSession("startedDateForpostpaidMail", DateHelper.getFormattedDate(subscriptionRequest.getStartDate()));
		
		model.addAttribute("startDate", subscriptionRequest.getStartDate());
		model.addAttribute("averageMonthlyBill", requestInterceptor.getDataFromSession("averageMonthlyBill"));
		return "/enduser-pages/create-subscription/postpay-subscription/subscription-payment-success";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/postpay-terms-conditions")
	public String termsConditions(Model model) {
		
		return "/enduser-pages/change-delivery-pattern/postpay-subscription/terms-conditions";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/postpay-subscription-checkout")
	public String subscriptionCheckout(Model model, @RequestParam(required=false) String message) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		Date startDate = new Date();
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, 1);
		DateHelper.setToEndOfDay(endDate);
		Double averageMonthlyBill = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			averageMonthlyBill = averageMonthlyBill + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("averageMonthlyBill", averageMonthlyBill);
		requestInterceptor.addDataToSession("averageMonthlyBill", averageMonthlyBill);
		
		model.addAttribute("message", message);
		return "/enduser-pages/change-delivery-pattern/postpay-subscription/average-monthly-bill";
	}
	
	@RequestMapping(value="/end-user/change-delivery-pattern/postpay-success")
	public String forwardPostpayConfirmation(Authentication authentication, Model model, HttpServletRequest request) {
		
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		String deliveryMode = (String) requestInterceptor.getDataFromSession("deliveryMode");
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		subscriptionRequest.setStartDate(startDate);
		subscriptionRequest.setUser(user);
		subscriptionRequest.setIsSubscriptionRequest(true);
		subscriptionRequest.setPermanantNote(deliveryMode);
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, user, false, false);
		subscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null)
			return "redirect:/";
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			subscribedProductCodes.add(subscribedProduct.getCode());
			
			RequestLineItem lineItem = new RequestLineItem();
			lineItem.setType(subscriptionLineItem.getType());
			lineItem.setDay(subscriptionLineItem.getDay());
			lineItem.setQuantity(subscriptionLineItem.getQuantity());
			lineItem.setProduct(subscriptionLineItem.getProduct());
			lineItem.setCustomPattern(subscriptionLineItem.getCustomPattern());
			subscriptionRequest.getRequestLineItems().add(lineItem);
		}
		
		subscriptionRequest = subscriptionRequestService.createSubscriptionRequest(subscriptionRequest, user, false, true);
		
		//Prepare subscriptionRequest OrderLineItems
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCode = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCode.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date endDate = DateHelper.addMonths(startDate, 1);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		model.addAttribute("subscriptionItems", subscriptionItems);
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		requestInterceptor.addDataToSession("startedDateForpostpaidMail", DateHelper.getFormattedDate(subscriptionRequest.getStartDate()));
		//End Prepare subscriptionRequest OrderLineItems
		
		HttpSession session = request.getSession();
		session.setAttribute("showExtendPrepay", null); 
		session.setAttribute("showAmount", user.getLastPendingDues());
		
		requestInterceptor.addDataToSession("prepayItems", null);
		
		model.addAttribute("startDate", subscriptionRequest.getStartDate());
		model.addAttribute("averageMonthlyBill", requestInterceptor.getDataFromSession("averageMonthlyBill"));
		return "/enduser-pages/change-delivery-pattern/postpay-subscription/subscription-payment-success";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/mail-postpaid")
	@ResponseBody
	public ApiResponse sendMailPostpaid() {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		
		Double averageMonthlyBill = (Double) requestInterceptor.getDataFromSession("averageMonthlyBill");
		String startedfrom = (String) requestInterceptor.getDataFromSession("startedDateForpostpaidMail");
		List<Map<String, Object>> subscriptionItems = (List<Map<String, Object>>) requestInterceptor.getDataFromSession("subscriptionItemsForpostpaidMail");
		emailHelper.sendMailForPostpaid(userService.getLoggedInUser().getEmail(), subscriptionItems, averageMonthlyBill, startedfrom, "confirmation mail", null);
		return apiResponse;
	}
	
}
