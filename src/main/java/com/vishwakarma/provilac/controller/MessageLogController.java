package com.vishwakarma.provilac.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.MessageLog;
import com.vishwakarma.provilac.model.Notification;
import com.vishwakarma.provilac.model.Notification.NotificationType;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.repository.MessageLogRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.MessageLogService;
import com.vishwakarma.provilac.service.NotificationService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;

@Controller
public class MessageLogController {

	@Resource 
	private MessageLogRepository messageLogRepository;
	
	@Resource
	private MessageLogService messageLogService;

	@Resource 
	private UserRepository userRepository;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private UserService userService;
	
	@Resource AsyncJobs asyncJobs;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	@Resource
	private NotificationService notificationService;
	
	@Resource
	private Environment environment;
	
	private static final int PAGE_SIZE = 50;
	
	@RequestMapping("/messagelog/list")
	public String messageLogList(final Model model,
			@RequestParam(defaultValue="1") Integer pageNumber) {
		Page<MessageLog> page = messageLogService.getScanLogs(pageNumber, PAGE_SIZE);
	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + PAGE_SIZE, page.getTotalPages());

	    model.addAttribute("page", page);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("currentIndex", current);
	    model.addAttribute("messageLogs", page.getContent());
	    return "/messagelog/list";
	}

	@RequestMapping("/admin/message/sendMessage")
	public String sendMessage(Model model) {
		
		return "/admin-pages/message/send";
	}
	
	@RequestMapping("/admin/message/list")
	public String sendMessageList(Model model) {
		
		return "/admin-pages/message/list";
	}
	
	@RequestMapping(value = "/admin/message/sendMessage", method = RequestMethod.POST)
	public String messageSend(Model model,
			@RequestParam(required = true) MultipartFile file,
			 @RequestParam(required=false,value="notificationMessage")String notificationMessage)throws InvalidFormatException, IOException {
		
		String message = "sucessfully Send Message";
		List<String> mobileNumberLists=new ArrayList<String>();
		try {
			mobileNumberLists=importedFile(file,notificationMessage);
		} catch (ProvilacException e) {
			e.printStackTrace();
			message = "Error while uploading file,please upload valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/admin/message/list";
		}
		model.addAttribute("mobileNumberLists", mobileNumberLists);
		model.addAttribute("message", message);
		return "/admin-pages/message/list";
	}

	private List<String> importedFile(MultipartFile file,String notificationMessage)
			throws InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(file.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);

		List<String> mobileNumberList=new ArrayList<String>();
		int rowCount=0;
		for(Row row: sheet){
			
			if(rowCount==0){
				rowCount++;
				continue;
			}
			
			String MobileNumber = null;
			try{
			
			if(null != row.getCell(0)){
				Cell MobileNumberCell = row.getCell(0);
				MobileNumber = MobileNumberCell.getStringCellValue();
				mobileNumberList.add(MobileNumber);
				plivoSMSHelper.sendSMS(MobileNumber, notificationMessage);
			}
			
			}catch(ProvilacException e){
				e.printStackTrace();
				continue;
			}
			
			
			 rowCount++;
		}
		
		return mobileNumberList;
	}
	
	@RequestMapping("/admin/message/sendByRoute")
	public String sendMessageByRouteAndStatus(Model model) {
		
		return "/admin-pages/message/smsAndPush";
	}
	
	
	@RequestMapping(value="/restapi/message/getCustomer")
	@ResponseBody
	public ApiResponse getCustomerList(@RequestParam(required=false)String routeSelect,
								@RequestParam(required=false)String status){
		
		ApiResponse apiResponse=new ApiResponse(true);
		List<User> users=new ArrayList<User>();
		List<UserDTO> userDTOs=new ArrayList<UserDTO>();
	try{
		if(routeSelect.equalsIgnoreCase("All") && !status.equalsIgnoreCase("All")){
		if(Status.ACTIVE.name().equalsIgnoreCase(status)){
			users = userService.getUsersByStatus(Status.ACTIVE);
			for(User user:users){
				userDTOs.add(new UserDTO(user));
			}
		}else if(Status.INACTIVE.name().equalsIgnoreCase(status)){
			users = userService.getUsersByStatus(Status.INACTIVE);
			for(User user:users){
				userDTOs.add(new UserDTO(user));
			}
		}else if(Status.HOLD.name().equalsIgnoreCase(status)){
			users = userService.getUsersByStatus(Status.HOLD);
			for(User user:users){
				userDTOs.add(new UserDTO(user));
			}
		}else if(Status.NEW.name().equalsIgnoreCase(status)){
			users = userService.getUsersByStatus(Status.NEW);
			for(User user:users){
				userDTOs.add(new UserDTO(user));
			}
		}
	}
		
		if(!routeSelect.equalsIgnoreCase("All") && status.equalsIgnoreCase("All")){
			List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(routeSelect, null, false, false);
			for(RouteRecord routeRecord:routeRecords){
				User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					userDTOs.add(new UserDTO(user));
			}
		}
		
		if(!routeSelect.equalsIgnoreCase("All") && !status.equalsIgnoreCase("All")){
			List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(routeSelect, null, false, false);
			for(RouteRecord routeRecord:routeRecords){
				User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
				if(user.getStatus().name().equalsIgnoreCase(status)){
						userDTOs.add(new UserDTO(user));
				}
				
			}
			
		}
		apiResponse.addData("users", userDTOs);
	}catch(ProvilacException e){
		e.printStackTrace();
		apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
	}
		
		return apiResponse;
	}
	
	@RequestMapping(value = "/admin/message/sendByRoute", method = RequestMethod.POST)
	public String sendMessageAndPush(Notification notification,
			BindingResult formBinding, Model model,
			 @RequestParam(required=false,value="message")String message,
			 @RequestParam(required=false,value="title")String title,
			 @RequestParam(required=false,value="sms")boolean sms,
			 @RequestParam(required=false,value="push")boolean push,
			 @RequestParam(required=false,value="email")boolean email,
			 @RequestParam(required=false,value="route")String route,
			 @RequestParam(required=false,value="status")String status,
			 @RequestParam(required=false,value="customers[]")String[] customers,
			 @RequestParam(required = false, value = "notificationPic") MultipartFile notificationPic,
			 @RequestParam(required=false) String mandrillTemplate){
		
		try {
				notification.setSmsMessage(message);
				notification.setPushTitle(title);
				notification.setDate(new Date());
				Set<NotificationType> notificationTypes =new HashSet<NotificationType>();
				if(sms){
					notificationTypes.add(NotificationType.SMS);
				}else if(push){
					notificationTypes.add(NotificationType.PUSH);
				}else{
					notificationTypes.add(NotificationType.EMAIL);
				}
				notification.setNotificationTypes(notificationTypes);
				notification=notificationService.createNotification(notification, userService.getLoggedInUser(), false, false);
				Long id=notification.getId();
		
				InputStream picInputStream = null;
				if (null != notificationPic && !notificationPic.isEmpty()) {
					
					try {
						picInputStream = notificationPic.getInputStream();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					String contentType = notificationPic.getContentType();
					if (!contentType.contains("image/")) {
						formBinding.addError(new FieldError("notification","notificationPic", "Please select valid notification pic"));
					} else if (notificationPic.getSize() > 5242880) { // Maximum size =
																	// 5MB
						formBinding.addError(new FieldError("notification","notificationPic","Please select service image Picture less than 5MB"));
					}
					try {
						appFileUtils.saveData(AppConstants.NOTIFICATION_IMAGES_FOLDER, notification.getId()+"", notificationPic.getBytes());
	
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					id=null;
				}
				
				if(customers[0].equalsIgnoreCase("All")) {
					
					if(route.equalsIgnoreCase("All") && status.equalsIgnoreCase("All")) {
						asyncJobs.sendMessageToAllRoutes(sms, push, email, title, message, route, status, customers[0],id, picInputStream, mandrillTemplate);//AllCustomers
					} else if(!route.equalsIgnoreCase("All") && status.equalsIgnoreCase("All")) {
						asyncJobs.sendMessageByRoute(sms, push, email, title, message, route, status, customers[0],id, picInputStream, mandrillTemplate);//byRoute
					} else if(!route.equalsIgnoreCase("All") && !status.equalsIgnoreCase("All")) {
						asyncJobs.sendMessageByRouteAndStatus(sms, push, email, title, message, route, status, customers[0],id, picInputStream, mandrillTemplate);
					} else if(route.equalsIgnoreCase("All") && !status.equalsIgnoreCase("All")) {
						//asyncJobs.sendMessageToAllRoutesWithStatus(sms, push, email, title, message, route, status, customers[0],id, picInputStream, mandrillTemplate);
						asyncJobs.sendMessageToAllCustomersWithStatus(sms, push, email, title, message, id, status, picInputStream, mandrillTemplate);
					}
				} else {
					asyncJobs.sendMessageToCustomers(sms, push, email, title,message, customers,id, picInputStream, mandrillTemplate);
				}
				
		} catch(ProvilacException e) {
			e.printStackTrace();
			message = "Error while Sending Message,please Check Data";
			model.addAttribute("message", message);
			return "/admin-pages/message/smsAndPush";
		}
		message ="Successfully Sent Message";
		model.addAttribute("message", message);
		return "/admin-pages/message/smsAndPush";
	}
	
	@RequestMapping(value = "/restapi/getNotificationPicture", method = RequestMethod.GET)
	@ResponseBody
	public void getNotificatonPicture(long id,
			HttpServletResponse httpServletResponse) {

		if (Boolean.parseBoolean(environment.getProperty("aws.s3.enabled"))) {
			try {
				String redirectUrl = appFileUtils.getS3Url(	AppConstants.NOTIFICATION_IMAGES_FOLDER, id + "");
				// TODO: Remove this, find some better mechanism for this
				if (redirectUrl.startsWith("https")) {
					redirectUrl = redirectUrl.replaceFirst("https", "http");
				}
				httpServletResponse.sendRedirect(redirectUrl);
			} catch (IOException e) {
				e.printStackTrace();
				// TODO: Decide on how to handle this
			}
		} else {
			File baseDir = new File(System.getProperty("catalina.base"),
					AppConstants.NOTIFICATION_IMAGES_FOLDER);
			File file = new File(baseDir, id + "");

			httpServletResponse.setHeader("Expires", "0");
			httpServletResponse.setHeader("Cache-Control",
					"must-revalidate, post-check=0, pre-check=0");
			httpServletResponse.setHeader("Pragma", "public");
			httpServletResponse.setHeader("Content-Type", "image/*");
			httpServletResponse.setHeader("Content-Length",
					String.valueOf(file.length()));
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				FileCopyUtils.copy(fileInputStream,
						httpServletResponse.getOutputStream());
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
