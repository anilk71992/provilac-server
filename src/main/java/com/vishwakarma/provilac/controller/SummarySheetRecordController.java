package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.SummarySheetRecordValidator;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.SummarySheetRecordService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class SummarySheetRecordController {

	@Resource
	private SummarySheetRecordValidator summarySheetRecordValidator;

	@Resource
	private SummarySheetRecordService summarySheetRecordService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	
	@InitBinder(value = "summarySheetRecord")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(summarySheetRecordValidator);
	}

	private Model populateModelForAdd(Model model, SummarySheetRecord summarySheetRecord) {
		model.addAttribute("summarySheetRecord", summarySheetRecord);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/summarySheetRecord/add")
	public String addSummarySheetRecord(Model model) {
		model = populateModelForAdd(model, new SummarySheetRecord());
		return "admin-pages/summarySheetRecord/add";
	}

	@RequestMapping(value = "/admin/summarySheetRecord/add", method = RequestMethod.POST)
	public String summarySheetRecordAdd(SummarySheetRecord summarySheetRecord, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		summarySheetRecordValidator.validate(summarySheetRecord, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, summarySheetRecord);
			return "admin-pages/summarySheetRecord/add";
		}
		summarySheetRecordService.createSummarySheetRecord(summarySheetRecord, loggedInUser, true,false);
		String message = "SummarySheetRecord added successfully";
		return "redirect:/admin/summarySheetRecord/list?message=" + message;
	}

	@RequestMapping(value = "/admin/summarySheetRecord/list")
	public String getSummarySheetRecordList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<SummarySheetRecord> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = summarySheetRecordService.searchSummarySheetRecord(pageNumber, searchTerm, user,true,false);
		} else {
			page = summarySheetRecordService.getSummarySheetRecords(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("summarySheetRecords", page.getContent());

		return "/admin-pages/summarySheetRecord/list";
	}

	@RequestMapping(value = "/admin/summarySheetRecord/show/{cipher}")
	public String showSummarySheetRecord(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		SummarySheetRecord summarySheetRecord = summarySheetRecordService.getSummarySheetRecord(id, true, loggedIUser,true,false);
		model.addAttribute("summarySheetRecord", summarySheetRecord);
		return "/admin-pages/summarySheetRecord/show";
	}

	@RequestMapping(value = "/admin/summarySheetRecord/update/{cipher}")
	public String updateSummarySheetRecord(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		SummarySheetRecord summarySheetRecord = summarySheetRecordService.getSummarySheetRecord(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, summarySheetRecord);
		model.addAttribute("id", summarySheetRecord.getId());
		model.addAttribute("version", summarySheetRecord.getVersion());
		return "/admin-pages/summarySheetRecord/update";
	}

	@RequestMapping(value = "/admin/summarySheetRecord/update", method = RequestMethod.POST)
	public String summarySheetRecordUpdate(SummarySheetRecord summarySheetRecord, 
										BindingResult formBinding, 
										Model model) {

		summarySheetRecord.setUpdateOperation(true);
		summarySheetRecordValidator.validate(summarySheetRecord, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, summarySheetRecord);
			return "admin-pages/summarySheetRecord/update";
		}
		summarySheetRecord.setId(Long.parseLong(model.asMap().get("id") + ""));
		summarySheetRecord.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		summarySheetRecordService.updateSummarySheetRecord(summarySheetRecord.getId(), summarySheetRecord, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "SummarySheetRecord updated successfully";
		return "redirect:/admin/summarySheetRecord/list?message=" + message;
	}

	@RequestMapping(value = "/admin/summarySheetRecord/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			summarySheetRecordService.deleteSummarySheetRecord(id);
			message = "SummarySheetRecord deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete SummarySheetRecord";
		}
		return "redirect:/admin/summarySheetRecord/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
	

	/**
	 * REST API Methods
	 */

	

	/**
	 * End REST API Methods
	 */
}
