package com.vishwakarma.provilac.controller;


import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.model.Country;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.CountryValidator;
import com.vishwakarma.provilac.service.CountryService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.EncryptionUtil;
/**
 * 
 * @author Rohit
 *
 */
@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class CountryController {

	@Resource
	private CountryValidator countryValidator;

	@Resource
	private CountryService countryService;

	@Resource
	private UserService userService;
	
	@InitBinder(value = "country")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.setValidator(countryValidator);
	}

	private Model populateModelForAdd(Model model, Country country) {
		model.addAttribute("country", country);
		return model;
	}

	@RequestMapping(value = "/admin/country/add")
	public String addCountry(Model model) {
		model = populateModelForAdd(model, new Country());
		return "admin-pages/country/add";
	}

	@RequestMapping(value = "/admin/country/add", method = RequestMethod.POST)
	public String countryAdd(Country country, 
									BindingResult formBinding, 
									Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		countryValidator.validate(country, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, country);
			return "admin-pages/country/add";
		}
		countryService.createCountry(country, loggedInUser, true,false);
		String message = "Country added successfully";
		return "redirect:/admin/country/list?message=" + message;
	}

	@RequestMapping(value = "/admin/country/list")
	public String getCountryList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<Country> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = countryService.searchCountry(pageNumber, searchTerm, user,true,false);
		} else {
			page = countryService.getCountries(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("countries", page.getContent());

		return "/admin-pages/country/list";
	}

	@RequestMapping(value = "/admin/country/show/{cipher}")
	public String showCountry(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Country country = countryService.getCountry(id, true, loggedIUser,true,false);
		model.addAttribute("country", country);
		return "/admin-pages/country/show";
	}

	@RequestMapping(value = "/admin/country/update/{cipher}")
	public String updateCountry(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Country country = countryService.getCountry(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, country);
		model.addAttribute("id", country.getId());
		model.addAttribute("version", country.getVersion());
		return "/admin-pages/country/update";
	}

	@RequestMapping(value = "/admin/country/update", method = RequestMethod.POST)
	public String countryUpdate(Country country, 
										BindingResult formBinding, 
										Model model) {

		country.setUpdateOperation(true);
		country.setId(Long.parseLong(model.asMap().get("id") + ""));
		country.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		countryValidator.validate(country, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, country);
			return "admin-pages/country/update";
		}
		
		User loggedInUser = userService.getLoggedInUser();
		countryService.updateCountry(country.getId(), country, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Country updated successfully";
		return "redirect:/admin/country/list?message=" + message;
	}

	@RequestMapping(value = "/admin/country/delete/{cipher}")
	public String deleteCountry(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			countryService.deleteCountry(id);
			message = "Country deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Country";
		}
		return "redirect:/admin/country/list?message=" + message;
	}
	
	/*
	 * Ajax Method
	 * 
	 */
}
