package com.vishwakarma.provilac.controller;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.CartItem;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.mvc.validator.UserValidator;
import com.vishwakarma.provilac.service.AccessService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.OneTimeOrderService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.PrepaySubscriptionRequestService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.Md5Util;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;
import com.vishwakarma.provilac.utils.RoleCache;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.web.RequestInterceptor;

@Controller
public class AccessController {

	@Resource
	private AccessService accessService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private ProductService productService;
	
	@Resource	
	private RequestInterceptor requestInterceptor;
	
	@Resource
	private AuthenticationManager authenticationManager;
	
	@Resource
	private UserValidator userValidator;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private Environment environment;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private AddressService addressService;     
	
	@Resource
	private OneTimeOrderService oneTimeOrderService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private PrepaySubscriptionRequestService prepaySubscriptionRequestService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private RoleCache roleCache;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	PlivoSMSHelper plivoSMSHelper;
	
	@RequestMapping("/")
	public String welcome() {		
		return "redirect:/index";
	}

	@RequestMapping(value= "/index")
	public String index(Authentication authentication, Model model, @RequestParam(defaultValue="1") Integer pageNumber, @RequestParam(required=false) String message, HttpServletResponse response, HttpServletRequest  request, @RequestParam(required=false) String retUrl) {
		model.addAttribute("message", message);		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);	
		List<Product>products = productService.getAllProducts(null, false, false);
		List<ProductDTO> dtos = new ArrayList<ProductDTO>();
		for (Product product : products) {
			if(product.isAvailableOneTimeOrder())
				dtos.add(new ProductDTO(product));
		}
		model.addAttribute("products", dtos);		

		List<CartItem> cartItems = requestInterceptor.getCartItems();
		Map<String, Integer> productQtyMap = new HashMap<String, Integer>();
		for (CartItem cartItem : cartItems) {
			productQtyMap.put(cartItem.getProduct().getCode(), cartItem.getQuantity());
		}
		model.addAttribute("productQtyMap", productQtyMap);
		try {
			UserInfo info = (UserInfo) authentication.getPrincipal();
			model.addAttribute("lastPendingDues", info.getUser().getLastPendingDues());
			List<PrepaySubscriptionRequest> prepaySubscriptionRequests = prepaySubscriptionRequestService.getPrepaySubscriptionRequestsByCustomer(info.getUser().getCode(), info.getUser(), false, false);
			model.addAttribute("requestedForPrepay", !prepaySubscriptionRequests.isEmpty());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/enduser-pages/index";		
	}
	
	@RequestMapping(value = "/signUp")
	public String signUp(Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "enduser-pages/signUp";
	}
	
	
	@RequestMapping(value = "/enduser/creatCustomer")
	@ResponseBody
	public ApiResponse creatCustomer(@RequestParam(required=false) String mobile) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		String message = "";
		if (!StringUtils.isEmpty(mobile)) {
			User existingUser = userService.getUserByMobileNumber(mobile, false, null, false, false);
			if(existingUser == null) {
				 String numbers = "0123456789";
				 Random rnd = new Random();
				 char[] otp = new char[6];
				 for (int i = 0; i < 6; i++)
					otp[i] = numbers.charAt(rnd.nextInt(numbers.length()));
				 String verificationCode = String.valueOf(otp);
				 User user = new User();
				 user.setUserRole(roleCache.getRole(Role.ROLE_CUSTOMER));
				 user.setMobileNumber(mobile);
				 user.setFirstName(mobile);
				 user.setVerificationCode(verificationCode);
				 user.setStatus(Status.NEW);
				 user.setJoiningDate(new Date());
				 User addedUser = userService.createUser(user, null, false, false);
				 boolean sentSMS = plivoSMSHelper.sendSMS(addedUser.getMobileNumber(), "OTP for mobile number verification is " + verificationCode);
				 apiResponse.addData("addedMobileNumber", addedUser.getMobileNumber());
				 apiResponse.addData("flag", true);
			} else {
				 message = "Mobile number already exist !";
				 apiResponse.addData("message", message);
				 apiResponse.addData("flag", false);
			}
		} else {
			 message = "Mobile number should not blank !";
			 apiResponse.addData("message", message);
			 apiResponse.addData("flag", false);
		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/enduser/verifyOtp")
	@ResponseBody
	public ApiResponse verificationOtp(@RequestParam(required=false) String mobile, @RequestParam(required=false) String verificationCode) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		String message = "";
		if (!StringUtils.isEmpty(mobile) && !StringUtils.isEmpty(verificationCode)) {
			User existingUser = userService.getUserByMobileNumber(mobile, false, null, false, false);
			if(existingUser != null) {
				if(existingUser.getVerificationCode().equalsIgnoreCase(verificationCode)) {
					UserInfo principal = new UserInfo(existingUser, true, true, true, true);
					SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(principal, null, principal.getAuthorities()));
					apiResponse.addData("flag", true);
				} else {
					message = "Incorrect OTP. Please enter correct OTP !";
					apiResponse.addData("message", message);
					apiResponse.addData("flag", false);
				}
			} else {
				message = "Mobile number not exist !";
				apiResponse.addData("message", message);
				apiResponse.addData("flag", false);
			}
		} else {
			message = "OTP should not blank !";
			apiResponse.addData("message", message);
			apiResponse.addData("flag", false);
		}
		return apiResponse;
	}
	
	
	@RequestMapping(value = "/enduser/checkCustomer")
	@ResponseBody
	public ApiResponse checkCustomer(@RequestParam(required=false) String mobile) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		String message = "";
		if (!StringUtils.isEmpty(mobile)) {
			User existingUser = userService.getUserByMobileNumber(mobile, false, null, false, false);
			if(existingUser != null) {
				 String numbers = "0123456789";
				 Random rnd = new Random();
				 char[] otp = new char[6];
				 for (int i = 0; i < 6; i++)
					otp[i] = numbers.charAt(rnd.nextInt(numbers.length()));
				 String verificationCode = String.valueOf(otp);
				 existingUser.setUpdateOperation(true);
				 existingUser.setVerificationCode(verificationCode);
				 User user = userService.updateCustomer(existingUser, null, false, true);
				 boolean sentSMS = plivoSMSHelper.sendSMS(user.getMobileNumber(), "OTP for mobile number verification is " + verificationCode);
				 apiResponse.addData("addedMobileNumber", user.getMobileNumber());
				 apiResponse.addData("flag", true);
			} else {
				 message = "Still you are not member Please signUp first !";
				 apiResponse.addData("message", message);
				 apiResponse.addData("flag", false);
			}
		} else {
			 message = "Mobile number should not blank !";
			 apiResponse.addData("message", message);
			 apiResponse.addData("flag", false);
		}
		return apiResponse;
	}
	
	@RequestMapping(value = "/end-user/welcome")
	public String opneDashboard(Model model, @RequestParam(required=false) String message, HttpServletRequest request) {
		
		model.addAttribute("message", message);
		User loggedInUser = userService.getLoggedInUser();
		HttpSession session = request.getSession();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, false, true);
		if(userSubscription == null) {
			session.setAttribute("showExtendPrepay", null);
			session.setAttribute("isSubsriptionUser", null);
			return "/enduser-pages/dashboard";
		}
		session.setAttribute("isSubsriptionUser", "yes");	
		boolean subscriptionTypeFlag = false;
		boolean tomorrowOrder = false;
		Double totalUnbilledAmount = 0.0;
		String tomorrowDeliveryScheduleCipher = "";
		if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid) {
			session.setAttribute("subscriptionType", "Postpaid");
			subscriptionTypeFlag = true;
			Double lastPendingDues = loggedInUser.getLastPendingDues();
			session.setAttribute("showAmount", lastPendingDues);
			model.addAttribute("lastPendingDues", lastPendingDues);
			Date startDate = DateHelper.addDays(new Date(), 1);
			requestInterceptor.addDataToSession("startDate", startDate);
			
			// Calculate unbilled Amount
			Calendar calendar = Calendar.getInstance();
			Date toDate = calendar.getTime();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.set(Calendar.DATE, 1);
			Date fromDate = calendar.getTime();
			
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(loggedInUser.getId(), fromDate, toDate, com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid, loggedInUser, false, true);
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if (deliverySchedule.getStatus().name().equalsIgnoreCase(DeliveryStatus.Delivered.name())) {
					for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
						totalUnbilledAmount = totalUnbilledAmount + (deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
					}
				}
			}
			// End Calculate unbilled Amount
			
		} else if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
			session.setAttribute("subscriptionType", "Prepaid");
			Double remainingPrepayBalance = loggedInUser.getRemainingPrepayBalance();
			model.addAttribute("remainingPrepayBalance", remainingPrepayBalance);
			Date startDate = DateHelper.addDays(userSubscription.getEndDate(), 1);
			requestInterceptor.addDataToSession("startDate", startDate);
			session.setAttribute("showExtendPrepay", "show");
			session.setAttribute("showAmount", remainingPrepayBalance);
		}
		
		DeliverySchedule deliveryScheduleForToday = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(loggedInUser.getCode(), new Date(), false, loggedInUser, false, true);
		Set<DeliveryLineItem> setOfdeliveryLineItemsForToday = null;
		if(deliveryScheduleForToday != null) {
			setOfdeliveryLineItemsForToday = new HashSet<DeliveryLineItem>();
			setOfdeliveryLineItemsForToday = deliveryScheduleForToday.getDeliveryLineItems();
		}
			
		Date tomorrow = DateHelper.addDays(new Date(), 1);
		DeliverySchedule deliveryScheduleForTomorrow = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(loggedInUser.getCode(), tomorrow, false, loggedInUser, true, true);
		Set<DeliveryLineItem> setOfdeliveryLineItemsForTomorrow = null;
		if(deliveryScheduleForTomorrow != null) {
			setOfdeliveryLineItemsForTomorrow = new HashSet<DeliveryLineItem>();
			setOfdeliveryLineItemsForTomorrow = deliveryScheduleForTomorrow.getDeliveryLineItems();
			tomorrowOrder = true;
			tomorrowDeliveryScheduleCipher = deliveryScheduleForTomorrow.getCipher();
		} 
			
		model.addAttribute("subscriptionTypeFlag", subscriptionTypeFlag);
		model.addAttribute("customerId", loggedInUser.getId());
		model.addAttribute("today", new Date());
		model.addAttribute("deliveryScheduleForToday", deliveryScheduleForToday);
		model.addAttribute("setOfdeliveryLineItemsForToday", setOfdeliveryLineItemsForToday);
		model.addAttribute("deliveryScheduleForTomorrow", deliveryScheduleForTomorrow);
		model.addAttribute("setOfdeliveryLineItemsForTomorrow", setOfdeliveryLineItemsForTomorrow);
		model.addAttribute("totalUnbilledAmount", totalUnbilledAmount);
		model.addAttribute("lastPendingDues", loggedInUser.getLastPendingDues());
		model.addAttribute("tomorrowOrder", tomorrowOrder);
		model.addAttribute("deliveryScheduleCipher", tomorrowDeliveryScheduleCipher);
		return "/enduser-pages/calendar";
	}
	
	@RequestMapping(value = "/termAndCondition")
	public String termAndCondition(Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "/enduser-pages/termAndConditions";
	}
	
	
	@RequestMapping(value = "/dashboardSubmitRequest", method=RequestMethod.POST)
	public String dashboradSubmitRequest(Model model, @RequestParam(required=false) String message, 
			@RequestParam(required=true) String optradio, HttpServletRequest request) {
		
		HttpSession session = request.getSession();	
		if(optradio.equalsIgnoreCase("subscribe")) {
			session.setAttribute("proceedSelected", "subscribe");
		} else if(optradio.equalsIgnoreCase("orderOnce")) {
			session.setAttribute("proceedSelected", null);
			return "redirect:/end-user/onetimeorder/categories";
		}
		model.addAttribute("message", message);
		
		return "redirect:/allCategories";
	}
	
	@RequestMapping(value = "/pincode")
	public String pincode(Model model, @RequestParam(required=false) String message, HttpServletResponse response,
			@RequestParam(required=false) String retUrl) {
		if (!StringUtils.isEmpty(retUrl)) {
			model.addAttribute("retUrl", retUrl);
		}
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "/enduser-pages/pincode";
	}
	
	@RequestMapping(value = "/pincode", method=RequestMethod.POST)
	public String submitPincode(Model model, @RequestParam(required=false) String message, HttpServletResponse response,
			@RequestParam(required=false) String retUrl) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);
		if (!StringUtils.isEmpty(retUrl)) {
			if(retUrl.equalsIgnoreCase("/signIn")) {
				return "enduser-pages/signIn";
			} else if(retUrl.equalsIgnoreCase("/signUp")) {
				return "enduser-pages/signUp";
			} 
		}
		return "/enduser-pages/pincode";
	}
	
	@RequestMapping(value = "/end-user/profile")
	public String profile(Model model, @RequestParam(required=false) String message) {
		model.addAttribute("message", message);
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("user", loggedInUser);
		model.addAttribute("id", loggedInUser.getId());
		model.addAttribute("version", loggedInUser.getVersion());
		return "/enduser-pages/profile";
	}
	
	@RequestMapping(value = "/end-user/profile", method=RequestMethod.POST)
	public String addProfile(Model model, @RequestParam(required=false) String message, @RequestParam(required=false) String firstName,
			@RequestParam(required=false) String email) {
		model.addAttribute("message", message);
		User loggedInUser = userService.getLoggedInUser();
		User user = loggedInUser;
		user.setUpdateOperation(true);
		user.setFirstName(firstName);
		user.setEmail(email);
		userService.updateCustomer(user, loggedInUser, false, true);
		return "/enduser-pages/dashboard";
	}
	
	
	@RequestMapping(value = "/signIn")
	public String signIn(Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "enduser-pages/signIn";
	}
	
	@RequestMapping(value = "/login")
	public String login(Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "access/login";
	}
	
	@RequestMapping(value = "/cart")
	public String cart(Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0); 
		return "enduser-pages/cart";
	}
	
	@RequestMapping(value = "/checkout")
	public String checkout(Model model, @RequestParam(required=false) String message) {
		
		List<CartItem> cartItems = requestInterceptor.getCartItems();
		if(cartItems.isEmpty())
			return "redirect:/allCategories";
		
		User loggedInUser = userService.getLoggedInUser();
		List<Address> address = addressService.getAddressesByCustomer(loggedInUser.getCode(), loggedInUser, false, false);
		
		model.addAttribute("message", message);
		model.addAttribute("addresss", address);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return "enduser-pages/checkout";
	}
	
	
	@RequestMapping(value = "/endUser-page/addToCard")
	@ResponseBody
	public ApiResponse addToCard(Model model, @RequestParam(required=false) String message, @RequestParam(required = true) String productCode, @RequestParam(required=false) Integer qty) {
		ApiResponse apiResponse = new ApiResponse(true);		
		try {
			Product product = productService.getProductByCode(productCode, true, null, false, false);
			List<CartItem> cartItems;
			if(qty == null) {
				cartItems = requestInterceptor.addItemToCart(product);					
			} else {
				cartItems = requestInterceptor.addItemToCart(product, qty);
			}
			apiResponse.addData("cart", cartItems);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());			
		}
		
		return apiResponse;
	}
	
	@RequestMapping(value = "/end-user/deleteItemFromCart")
	@ResponseBody
	public ApiResponse deleteItemFromCart(
		@RequestParam(required = true) String productCode) {
		ApiResponse apiResponse = new ApiResponse(true);		
		try {
			List<CartItem> cartItems = requestInterceptor.deleteItemFromCart(productCode);			
			apiResponse.addData("cart", cartItems);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}
	
	@RequestMapping(value = "/end-user/shippingOrdersAddress")
	@ResponseBody
	public ApiResponse shippingOrdersAddress(HttpServletRequest request,
			@RequestParam(required = true) String shippingOrdersAddress) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		HttpSession session = request.getSession();		
		try {
			session.setAttribute("shippingDeliveryAddressCode", shippingOrdersAddress);
			apiResponse.setSuccess(true);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());			
		}
		return apiResponse;
	}	
	
	@Deprecated	
	@RequestMapping(value="/end-user/verifyOTP")
	public String verifyOTP(Model model, HttpServletRequest request,
							@RequestParam(required=true)String OTP,
							@RequestParam(required=true)String mobileNumber,
							@RequestParam(required=false)String retUrl){
		String message = "";
		User existingUser = userService.getUserByMobileNumber(mobileNumber, false, null, false, false);
		if(OTP.equals(existingUser.getVerificationCode())){
			existingUser.setIsVerified(true);
			userService.updateUser(existingUser.getId(), existingUser, null, false, false);
			existingUser.setPassword(Md5Util.md5(existingUser.getVerificationCode()));
			existingUser = userService.updatePasswordForUser(existingUser.getId(), existingUser, existingUser, false, false);
			
			UserInfo principal = new UserInfo(existingUser, true, true, true, true);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal, existingUser.getVerificationCode(), principal.getAuthorities());
			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContext context = SecurityContextHolder.getContext();
			context.setAuthentication(authentication);
			
			HttpSession httpSession = request.getSession(false);
			httpSession.setAttribute("SPRING_SECURITY_CONTEXT", context);
			
			if (!StringUtils.isEmpty(retUrl)) {
				return "redirect:"+retUrl;
			}			
			return "redirect:/firstTimeProfile";		   	   
		}else{
			model.addAttribute("mobileNumber", mobileNumber);
			message = "Incorrect OTP. Please enter correct OTP !";
			model.addAttribute("message", message);
			if(!StringUtils.isEmpty(retUrl)) {				
				model.addAttribute("retUrl", retUrl);
			}
			return "/endUser-page/loginRegister";
			//TODO: Add mobile number to model
			//TODO: Add OTP to model
		}		
	}
	
	@Deprecated	
	@RequestMapping(value="/end-user/updateUserInfo", method=RequestMethod.POST)
	public String userInfoUpdate(Authentication authentication, @RequestParam String name, @RequestParam String email, Model model) {

		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		String[] nameArr = name.split(" ");
		String firstName = nameArr[0];
		String lastName = "";
		if(nameArr.length > 1) {
			for(int i=1; i < nameArr.length; i++) {
				lastName = lastName + nameArr[i];
			}
		}
		userInfo.getUser().setFirstName(firstName);
		userInfo.getUser().setLastName(lastName);
		userInfo.getUser().setEmail(email);
		User user = userInfo.getUser();
		user.setUpdateOperation(true);
		BindingResult formBinding = new DataBinder(user).getBindingResult();
		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			return "/endUser-page/firstTimeProfile?message=Please enter valid information.";
		}
		user = userService.updateUser(user.getId(), user, user, true, false);
		String message = "User updated successfully";
		return "redirect:/index?message="+message;		
	}
	
	@RequestMapping(value= "/new-prepay-subscription")
	public String subsribe(Authentication authentication, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);
		List<Product>products = productService.getAllProducts(userInfo.getUser(), true, false);
		model.addAttribute("products", products);
		
		return "/enduser-pages/create-subscription/subscribe";
	}
	
	@RequestMapping(value= "/select-subscription-pattern")
	public String patternForCreateSubscription(Authentication authentication, Long productId, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Product product = productService.getProduct(productId, true, userInfo.getUser(), true, false);

		model.addAttribute("product", product);
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);		
		return "/enduser-pages/create-subscription/subscriptionProductDetails";
	}
	
	
	/*@RequestMapping(value= "/select-subscription-pattern")
	public String patternForPrePay(Authentication authentication, String productCipher, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Long id = Long.parseLong(EncryptionUtil.decode(productCipher));
		Product product = productService.getProduct(id, true, userInfo.getUser(), true, false);

		model.addAttribute("product", product);
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);		
		return "/enduser-pages/create-subscription/subscriptionProductDetails";
	}*/
	
	@RequestMapping(value= "/confirm-subscription")
	public String orderConfirm(Authentication authentication, String productCipher, SubscriptionType subscriptionType, Integer[] quantities, Integer[] days, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Long id = Long.parseLong(EncryptionUtil.decode(productCipher));
		Product product = productService.getProduct(id, true, userInfo.getUser(), true, false);
		
		Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
		
		switch (subscriptionType) {
		case Daily:
			SubscriptionLineItem lineItem = new SubscriptionLineItem();
			lineItem.setProduct(product);
			lineItem.setType(subscriptionType);
			lineItem.setQuantity(quantities[0]);
			lineItems.add(lineItem);
			break;

		case Weekly:
			for (int i = 0; i < days.length; i++) {
				int day = days[i];

				SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
				subscriptionLineItem.setProduct(product);
				subscriptionLineItem.setType(subscriptionType);
				subscriptionLineItem.setQuantity(quantities[0]);
				subscriptionLineItem.setDay(Day.values()[day-1]);
				lineItems.add(subscriptionLineItem);
			}
			break;

		case Custom:
			SubscriptionLineItem item = new SubscriptionLineItem();
			item.setProduct(product);
			item.setType(subscriptionType);
			item.setCustomPattern(quantities[0] + "-" + quantities[1]);
			lineItems.add(item);
			break;
		}
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null) {
			existingItems = new HashSet<SubscriptionLineItem>();
		}
		//Remove existing line items from session
		for (Iterator iterator = existingItems.iterator(); iterator.hasNext();) {
			SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) iterator.next();
			if(subscriptionLineItem.getProduct().getId().equals(product.getId())) {
				iterator.remove();
			}
		}
		existingItems.addAll(lineItems);
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		//Calculate effective prices
		List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		Map<Product, Double> effectivePriceMap = new HashMap<Product, Double>();
		if(key == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2, product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2, product2.getPricingMap().get(key));
			}
		}
		
		model.addAttribute("effectivePricing", effectivePriceMap);
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);	
		return "/enduser-pages/create-subscription/confirm-subscription";
	}
	
	@RequestMapping(value = "/choose-subscription-type")
	public String chooseSubscriptionType(Model model, @RequestParam String deliveryMode) {
		requestInterceptor.addDataToSession("deliveryMode", deliveryMode);
		return "/enduser-pages/create-subscription/choose-subscription-type";
	}
	
	@RequestMapping(value = "/prepay-subscription-duration")
	public String prepaySubscriptionDuration(Authentication authentication, Model model) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		requestInterceptor.addDataToSession("prepayDuration", 3);
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		requestInterceptor.addDataToSession("user", userInfo.getUser());
		requestInterceptor.addDataToSession("startDate", new Date());
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		//Calculate effective prices
		List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		
		LinkedList<String> effectiveItems = new LinkedList();
		LinkedList<Double> prices = new LinkedList();
		LinkedList<Double> effectivePrices = new LinkedList();
		if(key == null) {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPricingMap().get(key));
			}
		}
		
		model.addAttribute("effectiveItems", effectiveItems);
		model.addAttribute("prices", prices);
		model.addAttribute("effectivePrices", effectivePrices);
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		
		model.addAttribute("today", DateHelper.getFormattedDateForDevice(new Date()));
		return "/enduser-pages/create-subscription/prepay-subscription/subscription-duration";
	}
	
	@RequestMapping(value = "/prepay-terms-conditions")
	public String prepayTermsConditions(Model model, @RequestParam String date) {
		requestInterceptor.addDataToSession("startDate", date);
		return "/enduser-pages/create-subscription/prepay-subscription/terms-conditions";
	}
	
	@RequestMapping(value = "/average-monthly-bill")
	public String averageMonthlyBill(Model model, HttpServletResponse response) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		String fromDate = (String) requestInterceptor.getDataFromSession("startDate");
		
		Date startDate = DateHelper.parseDateFormatForDevice(fromDate);
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("totalSubscriptionAmount", totalSubscriptionAmount);
		model.addAttribute("durationInMonths", durationInMonths);
		requestInterceptor.addDataToSession("totalSubscriptionAmount", totalSubscriptionAmount);
		
		User user = userService.getLoggedInUser();
		Double lastPendingdues = user.getLastPendingDues();
		boolean flag = false;
		if(lastPendingdues > 0 || lastPendingdues > 0.0) {
			flag = true;
			model.addAttribute("lastPendingdues", lastPendingdues);
		}
		requestInterceptor.addDataToSession("lastPendingdues", lastPendingdues);
		model.addAttribute("flag", flag);
		
		
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);
		return "/enduser-pages/create-subscription/prepay-subscription/average-monthly-bill";
	}
	
	@RequestMapping(value = "/prepay-subscription-checkout")
	public String prepaySubscriptionCheckout(Model model, @RequestParam(required=false) String message) {
		
		User loggedInUser = userService.getLoggedInUser();
		List<Address> address = addressService.getAddressesByCustomer(loggedInUser.getCode(), loggedInUser, true, false);
		model.addAttribute("addresss", address);
		model.addAttribute("message", message);
		return "/enduser-pages/create-subscription/prepay-subscription/prepay-subscription-checkout";
	}
	
	@RequestMapping(value="/remove-prepay-lineitem")
	@ResponseBody
	public ApiResponse removePrepayLineItem(String productCipher) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Long id = Long.parseLong(EncryptionUtil.decode(productCipher));
			Product product = productService.getProduct(id, true);
			Set<SubscriptionLineItem> subscriptionLineItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
			for (Iterator iterator = subscriptionLineItems.iterator(); iterator.hasNext();) {
				SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) iterator.next();
				if(subscriptionLineItem.getProduct().getCode().equalsIgnoreCase(product.getCode())) {
					iterator.remove();
				}
			}
			requestInterceptor.addDataToSession("prepayItems", subscriptionLineItems);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/remove-lineitem")
	@ResponseBody
	public ApiResponse removeLineItem(Long productId) {
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Product product = productService.getProduct(productId, true);
			Set<SubscriptionLineItem> subscriptionLineItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
			for (Iterator iterator = subscriptionLineItems.iterator(); iterator.hasNext();) {
				SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) iterator.next();
				if(subscriptionLineItem.getProduct().getCode().equalsIgnoreCase(product.getCode())) {
					iterator.remove();
				}
			}
			requestInterceptor.addDataToSession("prepayItems", subscriptionLineItems);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/prepay/setaddress")
	public String setAddressAndForwardPrepayPaymentRequest(Authentication authentication, @RequestParam String addressCipher, Model model) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		Long addressId = Long.valueOf(EncryptionUtil.decode(addressCipher));
		Address address = addressService.getAddress(addressId, true, userInfo.getUser(), false, false);
		
		List<Address> addresses = addressService.getAddressesByCustomer(userInfo.getUser().getCode(), null, false, false);
		for (Address existingAddress : addresses) {
			existingAddress.setIsDefault(false);
			addressService.updateAddress(existingAddress.getId(), existingAddress, null, false, false);
		}
		address.setIsDefault(true);
		addressService.updateAddress(address.getId(), address, null, false, false);
		
		requestInterceptor.addDataToSession("address", address);
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.PREPAY_FRONTEND_PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.PREPAY_FRONTEND_PAYU_FURL);
		model.addAttribute("firstName", userInfo.getFirstName());
		model.addAttribute("lastName", userInfo.getLastName());
		model.addAttribute("fullName", userInfo.getFullName());
		model.addAttribute("phone", userInfo.getMobileNumber());
		model.addAttribute("email", userInfo.getEmail());
		Double totalSubscriptionAmount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		
		String txnId = "WEB-PP" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
		String productInfo = txnId;
		model.addAttribute("productInfo", productInfo);
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		String amountString = "";
		if(lastPendingdues > 0 || lastPendingdues > 0.0) {
			amountString = new DecimalFormat("##.##").format(totalSubscriptionAmount + lastPendingdues);
		} else {
			amountString = new DecimalFormat("##.##").format(totalSubscriptionAmount);
		}
		
		model.addAttribute("amount", amountString);
		String udf1 = "";
		model.addAttribute("udf1", udf1);
		String udf2 = "";
		model.addAttribute("udf2", udf2);
		String udf3 = "";
		model.addAttribute("udf3", udf3);
		String udf4 = "";
		model.addAttribute("udf4", udf4);
		String udf5 = ""; 
		model.addAttribute("udf5", udf5);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + userInfo.getFirstName() + "|" + userInfo.getEmail() + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		model.addAttribute("hash", encodedHash);
		model.addAttribute("txnId", txnId);
		return "/enduser-pages/paymentRequestForwarder";
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.PREPAY_FRONTEND_PAYU_SURL)
	public String handlePayUPrepayPaymentSuccess(Authentication authentication, HttpServletRequest request,
												@RequestParam(required=false) String payuMoneyId, Model model,
												@RequestParam(required=false) String mode) {
		
		String status = "success";
		String firstname = "raj";
		double amount = 1212;
		String txnid = "hdk11";
		String hash = "ddf";
		String productinfo = "rhh";
		String phone = "8720802421";
		String email = "satendra";
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		/*if(!hash.equals(encodedHash)) {
			return "/enduser-pages/payu_success_wrong";
		}*/

		/*if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/enduser-pages/payu_success_wrong";
		}*/
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null || existingItems.isEmpty())
			return "redirect:/";
		amount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		String fromDate = (String) requestInterceptor.getDataFromSession("startDate");
		
		Date startDate = DateHelper.parseDateFormatForDevice(fromDate);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");  
		String deliveryMode = (String) requestInterceptor.getDataFromSession("deliveryMode");
		
		PrepaySubscriptionRequest prepaySubscriptionRequest = new PrepaySubscriptionRequest();
		prepaySubscriptionRequest.setStartDate(startDate);
		prepaySubscriptionRequest.setDuration(durationInMonths);
		prepaySubscriptionRequest.setUser(user);
		prepaySubscriptionRequest.setPermanantNote(deliveryMode);
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, user, false, false);
		prepaySubscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
		
		//Effective price calculation & Lineitem population
		
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			subscribedProductCodes.add(subscribedProduct.getCode());
			
			PrepayRequestLineItem lineItem = new PrepayRequestLineItem();
			lineItem.setType(subscriptionLineItem.getType());
			lineItem.setDay(subscriptionLineItem.getDay());
			lineItem.setQuantity(subscriptionLineItem.getQuantity());
			lineItem.setProduct(subscriptionLineItem.getProduct());
			lineItem.setCustomPattern(subscriptionLineItem.getCustomPattern());
			prepaySubscriptionRequest.getPrepayRequestLineItems().add(lineItem);
		}
		List <Product> products = productService.getAllProducts(user, true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = new Double(amount).intValue();
		Integer mapKey = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			mapKey = integer;
		}
		Map<Long, Double> effectivePriceMap = new HashMap<Long, Double>();
		if(mapKey == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPricingMap().get(mapKey));
			}
		}
		prepaySubscriptionRequest.setProductPricing(effectivePriceMap);
		//End Effective price calculation
		
		prepaySubscriptionRequest.setTotalAmount(amount);
		prepaySubscriptionRequest.setFinalAmount(amount);
		prepaySubscriptionRequest.setTxnId(txnid);
		prepaySubscriptionRequest.setPaymentGateway("PayU");
		
		prepaySubscriptionRequest = prepaySubscriptionRequestService.createPrepaySubscriptionRequest(prepaySubscriptionRequest, user, false, true);
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		boolean flag = false;
		if(lastPendingdues > 0 || lastPendingdues > 0.0) {
			Payment payment = new Payment();
			payment.setCustomer(user);
			payment.setAmount(lastPendingdues);
			payment.setTxnId(txnid);
			payment.setDate(new Date());
			payment.setPaymentMethod(PaymentMethod.PAYU);
			Payment addedPayment = paymentService.createPayment(payment, user, false, true);
			
			// Update loggedInUser with lastPendingdues
			User updatedLoggedInUser = userService.updateLastPendingDuesForUser(user.getId(), user, false, true);
			flag = true;
		}
		
		//Prepare prepaySubscriptionRequest OrderLineItems
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("flag", flag);
		//End prepare prepaySubscriptionRequest OrderLineItems
		requestInterceptor.addDataToSession("prepaySubscriptionRequest", prepaySubscriptionRequest);
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		HttpSession session = request.getSession();
		session.setAttribute("showExtendPrepay", "show"); 
		session.setAttribute("showAmount", user.getRemainingPrepayBalance());
		
		//TODO: Clear session information
		requestInterceptor.addDataToSession("prepayItems", null);
		model.addAttribute("prepaySubscriptionRequest", prepaySubscriptionRequest);
		model.addAttribute("lastPendingdues", lastPendingdues);
		return "/enduser-pages/create-subscription/prepay-subscription/subscription-payment-success";
	}
	
	@RequestMapping(value = "/end-user/create-prepay-subscription/mail-prepaid")
	@ResponseBody
	public ApiResponse sendMailPrepay() {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) requestInterceptor.getDataFromSession("prepaySubscriptionRequest");
		List<Map<String, Object>> subscriptionItems = (List<Map<String, Object>>) requestInterceptor.getDataFromSession("subscriptionItemsForpostpaidMail");
		
		emailHelper.sendMailForCreatePrePay(userService.getLoggedInUser().getEmail(), subscriptionItems, prepaySubscriptionRequest,  lastPendingdues, (prepaySubscriptionRequest.getFinalAmount() + lastPendingdues), "confirmation mail", null);
		return apiResponse;
	}
	
	@RequestMapping(value=AppConstants.PREPAY_FRONTEND_PAYU_FURL)
	public String handlePayUPrepayPaymentFailure(@RequestParam String status,
												@RequestParam String firstname,
												@RequestParam double amount,
												@RequestParam String txnid,
												@RequestParam String hash,
												@RequestParam String productinfo,
												@RequestParam String phone,
												@RequestParam String email,
												@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		return "/enduser-pages/payu_failure";
	}
	
	
	@RequestMapping(value= "/subscription-change-pattern")
	public String patternForPrePay(Authentication authentication, Long productId, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Product product = productService.getProduct(productId, true, userInfo.getUser(), true, false);

		model.addAttribute("product", product);
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);		
		return "/enduser-pages/extend-prepay-subscription/subscriptionProductDetails";
	}
	
	@RequestMapping(value= "/end-user/confirm-subscription")
	public String orderConfirmation(Authentication authentication, String productCipher, SubscriptionType subscriptionType, Integer[] quantities, Integer[] days, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Long id = Long.parseLong(EncryptionUtil.decode(productCipher));
		Product product = productService.getProduct(id, true, userInfo.getUser(), true, false);
		
		Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
		
		switch (subscriptionType) {
		case Daily:
			SubscriptionLineItem lineItem = new SubscriptionLineItem();
			lineItem.setProduct(product);
			lineItem.setType(subscriptionType);
			lineItem.setQuantity(quantities[0]);
			lineItems.add(lineItem);
			break;

		case Weekly:
			for (int i = 0; i < days.length; i++) {
				int day = days[i];

				SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
				subscriptionLineItem.setProduct(product);
				subscriptionLineItem.setType(subscriptionType);
				subscriptionLineItem.setQuantity(quantities[0]);
				subscriptionLineItem.setDay(Day.values()[day-1]);
				lineItems.add(subscriptionLineItem);
			}
			break;

		case Custom:
			SubscriptionLineItem item = new SubscriptionLineItem();
			item.setProduct(product);
			item.setType(subscriptionType);
			item.setCustomPattern(quantities[0] + "-" + quantities[1]);
			lineItems.add(item);
			break;
		}
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null) {
			existingItems = new HashSet<SubscriptionLineItem>();
		}
		//Remove existing line items from session
		for (Iterator iterator = existingItems.iterator(); iterator.hasNext();) {
			SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) iterator.next();
			if(subscriptionLineItem.getProduct().getId().equals(product.getId())) {
				iterator.remove();
			}
		}
		existingItems.addAll(lineItems);
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		return "redirect:/end-user/change-pattern-subscription";
	}
	
	@RequestMapping(value = "/change-delivery-pattern/by-customer")
	public String changeDeliveryPattern(Model model, HttpServletRequest request) {
		
		User loggedInUser = userService.getLoggedInUser();
		UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(loggedInUser.getCode(), true, loggedInUser, true, true);
		if(userSubscription == null) {
			String message = "userSubscription is not available";
			return "redirect:/index?message=" + message;
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("changeDeliveryPattern", "changeDeliveryPattern");
		
		Set<SubscriptionLineItem> existingItems = new HashSet<SubscriptionLineItem>();
		existingItems = userSubscription.getSubscriptionLineItems();
		
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		
		return "redirect:/end-user/change-delivery-pattern/summary";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/summary")
	public String subscribedSummary(Model model, HttpServletRequest request) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = 3;
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
	
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		return "/enduser-pages/change-delivery-pattern/change-pattern";
	}
	
	@RequestMapping(value= "/change-delivery-pattern")
	public String deliveryPatternForPrePay(Authentication authentication, Long productId, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Product product = productService.getProduct(productId, true, userInfo.getUser(), true, false);

		model.addAttribute("product", product);
		model.addAttribute("message", message);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache"); 
		response.setDateHeader("Expires", 0);		
		return "/enduser-pages/change-delivery-pattern/subscriptionProductDetails";
	}
	
	@RequestMapping(value= "/end-user/change-delivery-pattern/confirm-subscription")
	public String changeDeliveryPatternConfirm(Authentication authentication, String productCipher, SubscriptionType subscriptionType, Integer[] quantities, Integer[] days, Model model, @RequestParam(required=false) String message, HttpServletResponse response) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Long id = Long.parseLong(EncryptionUtil.decode(productCipher));
		Product product = productService.getProduct(id, true, userInfo.getUser(), true, false);
		
		Set<SubscriptionLineItem> lineItems = new HashSet<SubscriptionLineItem>();
		
		switch (subscriptionType) {
		case Daily:
			SubscriptionLineItem lineItem = new SubscriptionLineItem();
			lineItem.setProduct(product);
			lineItem.setType(subscriptionType);
			lineItem.setQuantity(quantities[0]);
			lineItems.add(lineItem);
			break;

		case Weekly:
			for (int i = 0; i < days.length; i++) {
				int day = days[i];

				SubscriptionLineItem subscriptionLineItem = new SubscriptionLineItem();
				subscriptionLineItem.setProduct(product);
				subscriptionLineItem.setType(subscriptionType);
				subscriptionLineItem.setQuantity(quantities[0]);
				subscriptionLineItem.setDay(Day.values()[day-1]);
				lineItems.add(subscriptionLineItem);
			}
			break;

		case Custom:
			SubscriptionLineItem item = new SubscriptionLineItem();
			item.setProduct(product);
			item.setType(subscriptionType);
			item.setCustomPattern(quantities[0] + "-" + quantities[1]);
			lineItems.add(item);
			break;
		}
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null) {
			existingItems = new HashSet<SubscriptionLineItem>();
		}
		//Remove existing line items from session
		for (Iterator iterator = existingItems.iterator(); iterator.hasNext();) {
			SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) iterator.next();
			if(subscriptionLineItem.getProduct().getId().equals(product.getId())) {
				iterator.remove();
			}
		}
		existingItems.addAll(lineItems);
		requestInterceptor.addDataToSession("prepayItems", existingItems);
		return "redirect:/end-user/change-delivery-pattern/summary";
	}
	
	@RequestMapping(value= "/end-user/change-delivery-pattern-subscription")
	public String changeDeliveryPatternSubscription(Authentication authentication, Model model, HttpServletRequest request){
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		Date startDate = DateHelper.addDays(new Date(), 1);
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = 3;
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("changePattern", "changePattern");
		session.setAttribute("changeDeliveryPattern", null);
		model.addAttribute("subscriptionItems", subscriptionItems);
		return "/enduser-pages/change-delivery-pattern/change-pattern";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/choose-type")
	public String takeSubscriptionType(Model model, @RequestParam String deliveryMode) {
		requestInterceptor.addDataToSession("deliveryMode", deliveryMode);
		return "/enduser-pages/change-delivery-pattern/choose-subscription-type";
	}
	
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/prepay-subscription-duration")
	public String subscriptionDuration(Authentication authentication, Model model) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		requestInterceptor.addDataToSession("prepayDuration", 3);
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		requestInterceptor.addDataToSession("user", userInfo.getUser());
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		/*requestInterceptor.addDataToSession("startDate", new Date());*/
		Date startDate = (Date) requestInterceptor.getDataFromSession("startDate");
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			//Calculate Total Quantity
			int totalQty = 0;
			Set<String> quantityStrings = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					if(subscriptionLineItem.getQuantity() > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
					}
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						subscriptionItemDetails.put("type", "Weekly");
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							switch (dayOfWeek) {
							case Calendar.SUNDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
								}
								break;

							case Calendar.MONDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.TUESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.WEDNESDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.THURSDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.FRIDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
								
							case Calendar.SATURDAY:
								if(subscriptionLineItem.getQuantity() > 1) {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
								} else {
									quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
								}
								break;
							}
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
					} else {
						subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
					}
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			if(!quantityStrings.isEmpty()) {
				String quantity = "";
				for (String string : quantityStrings) {
					if(quantity.isEmpty()) {
						quantity = string;
						continue;
					}
					quantity = quantity + ", " + string;
				}
				subscriptionItemDetails.put("quantity", quantity);
			}
			subscriptionItemDetails.put("totalQuantity", totalQty);
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItems.add(subscriptionItemDetails);
		}
		
		//Calculate effective prices
		List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = totalSubscriptionAmount.intValue();
		Integer key = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			key = integer;
		}
		/*Map<Product, Double> effectivePriceMap = new HashMap<Product, Double>();
		if(key == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2, product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2, product2.getPricingMap().get(key));
			}
		}*/
		
		//
		
		LinkedList<String> effectiveItems = new LinkedList();
		LinkedList<Double> prices = new LinkedList();
		LinkedList<Double> effectivePrices = new LinkedList();
		if(key == null) {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectiveItems.add(product2.getName());
				prices.add(product2.getPrice());
				effectivePrices.add(product2.getPricingMap().get(key));
			}
		}
		
		model.addAttribute("effectiveItems", effectiveItems);
		model.addAttribute("prices", prices);
		model.addAttribute("effectivePrices", effectivePrices);
		
		//
		
		/*model.addAttribute("effectivePricing", effectivePriceMap);*/
		model.addAttribute("subscriptionItems", subscriptionItems);
		
		return "/enduser-pages/change-delivery-pattern/prepay-subscription/subscription-duration";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/prepay-terms-conditions")
	public String termsConditions(Model model) {
		return "/enduser-pages/change-delivery-pattern/prepay-subscription/terms-conditions";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/average-monthly-bill")
	public String monthlyAverageBill(Model model, HttpServletResponse response, HttpServletRequest request) {
		
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
			subscribedProductCodes.add(subscribedProduct.getCode());
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
		
		Date startDate = (Date) requestInterceptor.getDataFromSession("startDate");
		DateHelper.setToStartOfDay(startDate);
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		Double totalSubscriptionAmount = 0.0;
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		User loggedInUser = userService.getLoggedInUser();
		
		
		Double totalUnbilledAmount = 0.0;
		Double lastPendingdues = 0.0;
		HttpSession session = request.getSession();
		boolean flag = false;
		String subscriptionType = (String) session.getAttribute("subscriptionType");   
		if(subscriptionType.equalsIgnoreCase("Postpaid")) {
			// Calculate unbilled Amount
			Calendar calendar = Calendar.getInstance();
			Date toDate = calendar.getTime();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.set(Calendar.DATE, 1);
			Date fromDate = calendar.getTime();

			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(loggedInUser.getId(), fromDate, toDate, com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid, loggedInUser, false, true);
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if (deliverySchedule.getStatus().name().equalsIgnoreCase(DeliveryStatus.Delivered.name())) {
					for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
						totalUnbilledAmount = totalUnbilledAmount + (deliveryLineItem.getProduct().getPrice() * deliveryLineItem.getQuantity());
					}
				}
			}
			// End Calculate unbilled Amount
			flag = true;
			lastPendingdues = loggedInUser.getLastPendingDues();
		}
		
		requestInterceptor.addDataToSession("totalSubscriptionAmount", totalSubscriptionAmount);
		requestInterceptor.addDataToSession("lastPendingdues", lastPendingdues);
		requestInterceptor.addDataToSession("totalUnbilledAmount", totalUnbilledAmount);
		requestInterceptor.addDataToSession("amountToPay", (totalSubscriptionAmount + lastPendingdues + totalUnbilledAmount));
		
		model.addAttribute("totalSubscriptionAmount", totalSubscriptionAmount);
		model.addAttribute("lastPendingdues", lastPendingdues);
		model.addAttribute("totalUnbilledAmount", totalUnbilledAmount);
		model.addAttribute("amountToPay", (totalSubscriptionAmount + lastPendingdues + totalUnbilledAmount));
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("durationInMonths", durationInMonths);
		model.addAttribute("flag", flag);
		return "/enduser-pages/change-delivery-pattern/prepay-subscription/average-monthly-bill";
	}
	
	@RequestMapping(value="/end-user/change-delivery-pattern/forwardPaymanetRequest")
	public String forwardPrepayPaymentRequest(Authentication authentication, Model model) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.CHANGE_DELIVERY_PATTERN_FRONTEND_PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.PREPAY_FRONTEND_PAYU_FURL);
		model.addAttribute("firstName", userInfo.getFirstName());
		model.addAttribute("lastName", userInfo.getLastName());
		model.addAttribute("fullName", userInfo.getFullName());
		model.addAttribute("phone", userInfo.getMobileNumber());
		model.addAttribute("email", userInfo.getEmail());
		Double amountToPay = (Double) requestInterceptor.getDataFromSession("amountToPay");
		
		String txnId = "WEB-PP" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
		String productInfo = txnId;
		model.addAttribute("productInfo", productInfo);
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		
		String amountString = new DecimalFormat("##.##").format(amountToPay);
		
		model.addAttribute("amount", amountString);
		String udf1 = "";
		model.addAttribute("udf1", udf1);
		String udf2 = "";
		model.addAttribute("udf2", udf2);
		String udf3 = "";
		model.addAttribute("udf3", udf3);
		String udf4 = "";
		model.addAttribute("udf4", udf4);
		String udf5 = ""; 
		model.addAttribute("udf5", udf5);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + userInfo.getFirstName() + "|" + userInfo.getEmail() + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		model.addAttribute("hash", encodedHash);
		model.addAttribute("txnId", txnId);
		return "/enduser-pages/paymentRequestForwarder";
	}
	
	/*@RequestParam String status,
	@RequestParam String firstname,
	@RequestParam double amount,
	@RequestParam String txnid,
	@RequestParam String hash,
	@RequestParam String productinfo,
	@RequestParam String phone,
	@RequestParam String email,*/
	
	@RequestMapping(value=AppConstants.CHANGE_DELIVERY_PATTERN_FRONTEND_PAYU_SURL)
	public String changeDeliveryPatternHandlePayUPrepayPaymentSuccess(Authentication authentication, HttpServletRequest request,
												@RequestParam(required=false) String payuMoneyId, Model model,
												@RequestParam(required=false) String mode) {
		
		String status = "success";
		String firstname = "raj";
		double amount = 1212;
		String txnid = "hdk11";
		String hash = "ddf";
		String productinfo = "rhh";
		String phone = "8720802421";
		String email = "satendra";
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		/*if(!hash.equals(encodedHash)) {
			return "/enduser-pages/payu_success_wrong";
		}*/

		/*if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/enduser-pages/payu_success_wrong";
		}*/
		
		amount = (Double) requestInterceptor.getDataFromSession("totalSubscriptionAmount");
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		
		Date startDate = (Date) requestInterceptor.getDataFromSession("startDate");
		DateHelper.setToStartOfDay(startDate);
		int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");  
		String deliveryMode = (String) requestInterceptor.getDataFromSession("deliveryMode");
		
		PrepaySubscriptionRequest prepaySubscriptionRequest = new PrepaySubscriptionRequest();
		prepaySubscriptionRequest.setStartDate(startDate);
		prepaySubscriptionRequest.setDuration(durationInMonths);
		prepaySubscriptionRequest.setUser(user);
		prepaySubscriptionRequest.setPermanantNote(deliveryMode);
		
		List<UserSubscription> userSubscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, user, false, false);
		prepaySubscriptionRequest.setIsChangeRequest(!userSubscriptions.isEmpty());
		
		//Effective price calculation & Lineitem population
		Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
		if(existingItems == null)
			return "redirect:/";
		
		Set<String> subscribedProductCodes = new HashSet<String>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			subscribedProductCodes.add(subscribedProduct.getCode());
			
			PrepayRequestLineItem lineItem = new PrepayRequestLineItem();
			lineItem.setType(subscriptionLineItem.getType());
			lineItem.setDay(subscriptionLineItem.getDay());
			lineItem.setQuantity(subscriptionLineItem.getQuantity());
			lineItem.setProduct(subscriptionLineItem.getProduct());
			lineItem.setCustomPattern(subscriptionLineItem.getCustomPattern());
			prepaySubscriptionRequest.getPrepayRequestLineItems().add(lineItem);
		}
		List <Product> products = productService.getAllProducts(user, true, true);
		Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
		int desiredValue = new Double(amount).intValue();
		Integer mapKey = null;
		List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
		Collections.sort(keySet);
		for (Integer integer : keySet) {
			if(desiredValue < integer) {
				break;
			}
			mapKey = integer;
		}
		Map<Long, Double> effectivePriceMap = new HashMap<Long, Double>();
		if(mapKey == null) {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPrice());
			}
		} else {
			for(Product product2: products) {
				effectivePriceMap.put(product2.getId(), product2.getPricingMap().get(mapKey));
			}
		}
		prepaySubscriptionRequest.setProductPricing(effectivePriceMap);
		//End Effective price calculation
		
		prepaySubscriptionRequest.setTotalAmount(amount);
		prepaySubscriptionRequest.setFinalAmount(amount);
		prepaySubscriptionRequest.setTxnId(txnid);
		prepaySubscriptionRequest.setPaymentGateway("PayU");
		
		prepaySubscriptionRequest = prepaySubscriptionRequestService.createPrepaySubscriptionRequest(prepaySubscriptionRequest, user, false, true);
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		Double totalUnbilledAmount = (Double) requestInterceptor.getDataFromSession("totalUnbilledAmount");
		Double totalPayment = lastPendingdues + totalUnbilledAmount;
		HttpSession session = request.getSession();
		boolean flag = false;
		String subscriptionType = (String) session.getAttribute("subscriptionType");   
		if(subscriptionType.equalsIgnoreCase("Postpaid")) {
			if(totalPayment > 0 || totalPayment > 0.0) {
				Payment payment = new Payment();
				payment.setCustomer(user);
				payment.setAmount(totalPayment);
				payment.setTxnId(txnid);
				payment.setDate(new Date());
				payment.setPaymentMethod(PaymentMethod.PAYU);
				Payment addedPayment = paymentService.createPayment(payment, user, false, true);
				
				// Update loggedInUser with lastPendingdues
				User updatedLoggedInUser = userService.updateLastPendingDuesForUser(user.getId(), user, false, true);
				flag = true;
			}
		}
		
		//Prepare prepaySubscriptionRequest OrderLineItems
		Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
		for (SubscriptionLineItem subscriptionLineItem : existingItems) {
			Product subscribedProduct = subscriptionLineItem.getProduct();
			if(!productSubscriptionMap.containsKey(subscribedProduct)) {
				productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
			}
			productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
		}
		
		List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
		
		Date endDate = DateHelper.addMonths(startDate, durationInMonths);
		DateHelper.setToEndOfDay(endDate);
		for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
			Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
			Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
			int totalQty = 0;
			for (SubscriptionLineItem subscriptionLineItem : entryValue) {
				Date runningDate = new Date(startDate.getTime());
				if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
					subscriptionItemDetails.put("type", "Daily");
					while (runningDate.before(endDate)) {
						totalQty = totalQty + subscriptionLineItem.getQuantity();
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
					subscriptionItemDetails.put("type", "Weekly");
					while (runningDate.before(endDate)) {
						int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
						Day currentDay = Day.values()[dayOfWeek - 1];
						if(subscriptionLineItem.getDay() == currentDay) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
					int currentIndex = 0;
					subscriptionItemDetails.put("type", "Alternate");
					String arr[] = subscriptionLineItem.getCustomPattern().split("-");
					//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
					int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
					while (runningDate.before(endDate)) {
						totalQty = totalQty + quantity[currentIndex];
						currentIndex = currentIndex + 1;
						if(currentIndex > 1) {
							currentIndex = 0;
						}
						runningDate = DateHelper.addDays(runningDate, 1);
					}
				}
			}
			subscriptionItemDetails.put("product", subscriptionEntry.getKey());
			subscriptionItemDetails.put("totalQuantity", totalQty);
			subscriptionItemDetails.put("productPrice", subscriptionEntry.getKey().getPrice());
			subscriptionItems.add(subscriptionItemDetails);
		}
		model.addAttribute("subscriptionItems", subscriptionItems);
		model.addAttribute("flag", flag);
		//End prepare prepaySubscriptionRequest OrderLineItems
		
		requestInterceptor.addDataToSession("prepaySubscriptionRequest", prepaySubscriptionRequest);
		requestInterceptor.addDataToSession("subscriptionItemsForpostpaidMail", subscriptionItems);
		
		session.setAttribute("showExtendPrepay", "show"); 
		session.setAttribute("showAmount", user.getRemainingPrepayBalance());
		
		//TODO: Clear session information
		requestInterceptor.addDataToSession("prepayItems", null);
		model.addAttribute("prepaySubscriptionRequest", prepaySubscriptionRequest);
		model.addAttribute("lastPendingdues", lastPendingdues);
		model.addAttribute("totalUnbilledAmount", totalUnbilledAmount);
		return "/enduser-pages/change-delivery-pattern/prepay-subscription/subscription-payment-success";
	}
	
	@RequestMapping(value = "/end-user/change-delivery-pattern/mail-prepaid")
	@ResponseBody
	public ApiResponse sendMailPostpaid() {
		
		ApiResponse apiResponse = new ApiResponse(true);		
		
		Double lastPendingdues = (Double) requestInterceptor.getDataFromSession("lastPendingdues");
		Double totalUnbilledAmount = (Double) requestInterceptor.getDataFromSession("totalUnbilledAmount");
		PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) requestInterceptor.getDataFromSession("prepaySubscriptionRequest");
		List<Map<String, Object>> subscriptionItems = (List<Map<String, Object>>) requestInterceptor.getDataFromSession("subscriptionItemsForpostpaidMail");
		
		emailHelper.sendMailForPrePay(userService.getLoggedInUser().getEmail(), subscriptionItems, prepaySubscriptionRequest,  lastPendingdues, totalUnbilledAmount, (prepaySubscriptionRequest.getFinalAmount() + lastPendingdues + totalUnbilledAmount), "confirmation mail", null);
		return apiResponse;
	}
	
	
	@Deprecated
	@RequestMapping(value = "/end-user/confirm-delivery-pattern")
	public String confirmDeliveryPattern(Model model, @RequestParam String deliveryMode, HttpServletRequest request, Authentication authentication) {
		requestInterceptor.addDataToSession("deliveryMode", deliveryMode);
		HttpSession session = request.getSession();
		String userSubscriptionType = (String) session.getAttribute("subscriptionType");
		if(userSubscriptionType.equalsIgnoreCase("Prepaid")) {
			Set<SubscriptionLineItem> existingItems = (Set<SubscriptionLineItem>) requestInterceptor.getDataFromSession("prepayItems");
			requestInterceptor.addDataToSession("prepayDuration", 3);
			
			UserInfo userInfo = (UserInfo) authentication.getPrincipal();
			requestInterceptor.addDataToSession("user", userInfo.getUser());
			Map<Product, Set<SubscriptionLineItem>> productSubscriptionMap = new HashMap<Product, Set<SubscriptionLineItem>>();
			Set<String> subscribedProductCodes = new HashSet<String>();
			for (SubscriptionLineItem subscriptionLineItem : existingItems) {
				Product subscribedProduct = subscriptionLineItem.getProduct();
				if(!productSubscriptionMap.containsKey(subscribedProduct)) {
					productSubscriptionMap.put(subscribedProduct, new HashSet<SubscriptionLineItem>());
				}
				productSubscriptionMap.get(subscribedProduct).add(subscriptionLineItem);
				subscribedProductCodes.add(subscribedProduct.getCode());
			}
			
			List<Map<String, Object>> subscriptionItems = new ArrayList<Map<String,Object>>();
			Date startDate = DateHelper.addDays(new Date(), 1);
			DateHelper.setToStartOfDay(startDate);
			int durationInMonths = (Integer) requestInterceptor.getDataFromSession("prepayDuration");
			Date endDate = DateHelper.addMonths(startDate, durationInMonths);
			DateHelper.setToEndOfDay(endDate);
			Double totalSubscriptionAmount = 0.0;
			for (Entry<Product, Set<SubscriptionLineItem>> subscriptionEntry : productSubscriptionMap.entrySet()) {
				
				Map<String, Object> subscriptionItemDetails = new HashMap<String, Object>();
				Set<SubscriptionLineItem> entryValue = subscriptionEntry.getValue();
				//Calculate Total Quantity
				int totalQty = 0;
				Set<String> quantityStrings = new HashSet<String>();
				for (SubscriptionLineItem subscriptionLineItem : entryValue) {
					Date runningDate = new Date(startDate.getTime());
					if(subscriptionLineItem.getType() == SubscriptionType.Daily) {
						if(subscriptionLineItem.getQuantity() > 1) {
							subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottles");
						} else {
							subscriptionItemDetails.put("quantity", subscriptionLineItem.getQuantity() + " bottle");
						}
						subscriptionItemDetails.put("type", "Daily");
						while (runningDate.before(endDate)) {
							totalQty = totalQty + subscriptionLineItem.getQuantity();
							runningDate = DateHelper.addDays(runningDate, 1);
						}
					} else if (subscriptionLineItem.getType() == SubscriptionType.Weekly) {
						while (runningDate.before(endDate)) {
							int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
							subscriptionItemDetails.put("type", "Weekly");
							Day currentDay = Day.values()[dayOfWeek - 1];
							if(subscriptionLineItem.getDay() == currentDay) {
								totalQty = totalQty + subscriptionLineItem.getQuantity();
								switch (dayOfWeek) {
								case Calendar.SUNDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Sun-" + subscriptionLineItem.getQuantity()+ " bottle");
									}
									break;

								case Calendar.MONDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Mon-" + subscriptionLineItem.getQuantity() + " bottle");
									}
									break;
									
								case Calendar.TUESDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Tue-" + subscriptionLineItem.getQuantity() + " bottle");
									}
									break;
									
								case Calendar.WEDNESDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Wed-" + subscriptionLineItem.getQuantity() + " bottle");
									}
									break;
									
								case Calendar.THURSDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Thu-" + subscriptionLineItem.getQuantity() + " bottle");
									}
									break;
									
								case Calendar.FRIDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Fri-" + subscriptionLineItem.getQuantity() + " bottles");
									}
									break;
									
								case Calendar.SATURDAY:
									if(subscriptionLineItem.getQuantity() > 1) {
										quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottles");
									} else {
										quantityStrings.add("Sat-" + subscriptionLineItem.getQuantity() + " bottle");
									}
									break;
								}
							}
							runningDate = DateHelper.addDays(runningDate, 1);
						}
					} else if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
						int currentIndex = 0;
						subscriptionItemDetails.put("type", "Alternate");
						String arr[] = subscriptionLineItem.getCustomPattern().split("-");
						//TODO: As of now assuming only alternate subscriptions will be created, tweak to take custom
						int quantity[] = new int[] {Integer.parseInt(arr[0]), Integer.parseInt(arr[1])};
						if(Integer.parseInt(arr[0]) > 1 || Integer.parseInt(arr[1]) > 1) {
							subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottles");
						} else {
							subscriptionItemDetails.put("quantity", subscriptionLineItem.getCustomPattern() + " bottle");
						}
						while (runningDate.before(endDate)) {
							totalQty = totalQty + quantity[currentIndex];
							currentIndex = currentIndex + 1;
							if(currentIndex > 1) {
								currentIndex = 0;
							}
							runningDate = DateHelper.addDays(runningDate, 1);
						}
					}
				}
				if(!quantityStrings.isEmpty()) {
					String quantity = "";
					for (String string : quantityStrings) {
						if(quantity.isEmpty()) {
							quantity = string;
							continue;
						}
						quantity = quantity + ", " + string;
					}
					subscriptionItemDetails.put("quantity", quantity);
				}
				subscriptionItemDetails.put("totalQuantity", totalQty);
				totalSubscriptionAmount = totalSubscriptionAmount + (totalQty * subscriptionEntry.getKey().getPrice());
				subscriptionItemDetails.put("product", subscriptionEntry.getKey());
				subscriptionItems.add(subscriptionItemDetails);
			}
			
			//Calculate effective prices
			List <Product> products = productService.getAllProducts(userInfo.getUser(), true, true);
			Map<Integer, Double> pricingMap = products.get(0).getPricingMap();
			int desiredValue = totalSubscriptionAmount.intValue();
			Integer key = null;
			List<Integer> keySet = new ArrayList<Integer>(pricingMap.keySet());
			Collections.sort(keySet);
			for (Integer integer : keySet) {
				if(desiredValue < integer) {
					break;
				}
				key = integer;
			}
			Map<Product, Double> effectivePriceMap = new HashMap<Product, Double>();
			if(key == null) {
				for(Product product2: products) {
					effectivePriceMap.put(product2, product2.getPrice());
				}
			} else {
				for(Product product2: products) {
					effectivePriceMap.put(product2, product2.getPricingMap().get(key));
				}
			}
			
			model.addAttribute("effectivePricing", effectivePriceMap);
			model.addAttribute("subscriptionItems", subscriptionItems);
			
			
			model.addAttribute("today", DateHelper.getFormattedDateForDevice(new Date()));
			return "/enduser-pages/change-delivery-pattern/subscription-duration";
		}
	
		return "redirect:/end-user/extend-subscription-duration";
	}
	
	@RequestMapping(value = "/end-user/invoice-billing")
	public String accountAndInvoice(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		List<Payment> billingList = paymentService.getAllPaymentsByCustomer(loggedInUser.getCode(), loggedInUser, false, true);
		List<Invoice> invoceList = invoiceService.getAllInvoicesByCustomer(loggedInUser.getCode(), loggedInUser, false, true);  
		model.addAttribute("billingList", billingList);
		model.addAttribute("invoceList", invoceList);
		return "/enduser-pages/accountsAndBilling";
	}
	
	
	@RequestMapping(value="/end-user/logout")
	public String logoutUser(Model model, HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){    
		   new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		SecurityContextHolder.getContext().setAuthentication(null);
		return "redirect:/index?message=You have been logged out successfully";
	}
	
	@RequestMapping(value= "/end-user/pay-now")
	public String payNowOrders(Model model) {
		
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		model.addAttribute("key", key);
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		model.addAttribute("surl", serverUrl + AppConstants.FRONTEND_PAYU_SURL);
		model.addAttribute("furl", serverUrl + AppConstants.FRONTEND_PAYU_FURL);
		return "/endUser-page/payNow";
	}
	
	@RequestMapping(value="/restapi/payment/getHashAndTxnId")
	@ResponseBody
	public ApiResponse getHashAndTxnIdForWebPayments(Authentication authentication,
													@RequestParam String key,
													@RequestParam String productInfo,
													@RequestParam String surl,
													@RequestParam String furl,
													@RequestParam String service_provider,
													@RequestParam String firstName,
													@RequestParam String lastName,
													@RequestParam String address1,
													@RequestParam String address2,
													@RequestParam String city,
													@RequestParam String state,
													@RequestParam String country,
													@RequestParam String zipcode,
													@RequestParam String udf1,
													@RequestParam String udf2,
													@RequestParam String udf3,
													@RequestParam String udf4,
													@RequestParam String udf5,
													@RequestParam String pg,
													@RequestParam String phone,
													@RequestParam String email,
													@RequestParam double amount) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		ApiResponse apiResponse = new ApiResponse(true);
		/**
		 * 1. Validate Fields
		 * 2. Generate TxnId
		 * 3. Create Hash String
		 * 4. Return response
		 */
		String originalKey = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String serverUrl = environment.getProperty("server.url");
		if(serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		String successUrl = serverUrl + AppConstants.FRONTEND_PAYU_SURL;
		String failureUrl = serverUrl + AppConstants.FRONTEND_PAYU_FURL;
		
		if(!originalKey.equals(key) || !successUrl.equals(surl) || !failureUrl.equals(furl) || 
				!userInfo.getFirstName().equals(firstName) || !userInfo.getMobileNumber().equals(phone)) {
			apiResponse.setError("Something went wrong, please refresh the page.", "400");
			return apiResponse;
		}
		
		String txnId = "WEB-" + userInfo.getUser().getCode() + "-" + System.currentTimeMillis();
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String amountString = new DecimalFormat("##.##").format(amount);
		String plainHash = key + "|" + txnId + "|" + amountString + "|" + productInfo + "|" + firstName + "|" + email + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;
		//String encodedHash = DigestUtils.sha512Hex(plainHash.getBytes());
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			apiResponse.setError("Unable to generate secured hash. Please try again later.", "500");
		}
		apiResponse.addData("hash", encodedHash);
		apiResponse.addData("txnId", txnId);
		apiResponse.addData("amount", amountString);
		return apiResponse;
	}
	
	private String hashCal(String type, String str) throws NoSuchAlgorithmException {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        MessageDigest algorithm = MessageDigest.getInstance(type);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append("0");
            }
            hexString.append(hex);
        }
        return hexString.toString();
	}
	
	@RequestMapping(value=AppConstants.FRONTEND_PAYU_SURL)
	public String handlePayUWebPaymentSuccess(Authentication authentication,
												@RequestParam String status,
												@RequestParam String firstname,
												@RequestParam double amount,
												@RequestParam String txnid,
												@RequestParam String hash,
												@RequestParam String productinfo,
												@RequestParam String phone,
												@RequestParam String email,
												@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		
		String salt = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_SALT).getPropValue();
		String key = systemPropertyHelper.getSystemProperty(SystemProperty.PAYUMONEY_KEY).getPropValue();
		String plainHash = salt + "|" + status + "|||||||||||" + email + "|" + firstname + "|" + productinfo + "|" + amount + "|" + txnid + "|" + key;
		String encodedHash = null;
		try {
			encodedHash = hashCal("SHA-512", plainHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		if(!hash.equals(encodedHash)) {
			return "/endUser-page/payu_success_wrong";
		}

		if(paymentService.getPaymentByTxnId(txnid, null, false, false) != null) {
			return "/endUser-page/payu_success_wrong";
		}
		
		User user = ((UserInfo) authentication.getPrincipal()).getUser();
		
		Payment payment = new Payment();
		
		payment.setAmount(amount);
		double cashback = 0.0;
		double dues = user.getLastPendingDues();
		float minBalanceForCashback = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.MIN_BALANCE_FOR_CASHBACK, "0.0"));
		float cashbackPercentage = Float.parseFloat(systemPropertyHelper.getSystemProperty(SystemProperty.CASHBACK_PECENTAGE, "0.0")) / 100;
		if(dues > 0 && (payment.getAmount() - dues) >= minBalanceForCashback) {
			//User has paid amount which makes dues zero and supasses min amount required for cashback
			cashback = (payment.getAmount() - dues) * cashbackPercentage;
		} else if(dues <= 0 && payment.getAmount() >= minBalanceForCashback) {
			//If dues are less than zero, and still user is paying more than min amount required for cashback
			cashback = payment.getAmount() * cashbackPercentage;
		}
		payment.setCashback(cashback);
		payment.setAmount(payment.getAmount() + payment.getCashback());
		
		payment.setTxnId(txnid);
		payment.setPaymentMethod(PaymentMethod.PAYU);
		payment.setCustomer(user);
		payment.setDate(new Date());
		paymentService.createPayment(payment, null, false, false);

		double lastpendingDues = user.getLastPendingDues();
		lastpendingDues = lastpendingDues - payment.getAmount();
		user.setLastPendingDues(lastpendingDues);
		userService.updateUser(user.getId(), user, null, false, false);
		
		return "/endUser-page/payu_success";
	}
	
	@RequestMapping(value=AppConstants.FRONTEND_PAYU_FURL)
	public String handlePayUWebPaymentFailure(@RequestParam String status,
												@RequestParam String firstname,
												@RequestParam double amount,
												@RequestParam String txnid,
												@RequestParam String hash,
												@RequestParam String productinfo,
												@RequestParam String phone,
												@RequestParam String email,
												@RequestParam(required=false) String payuMoneyId,
												@RequestParam(required=false) String mode) {
		return "/endUser-page/payu_failure";
	}
	
	@RequestMapping(value = "/admin/denied")
 	public String denied() {
		return "/access/denied";
	}
	
	@RequestMapping(value = "/login/failure")
 	public String loginFailure() {
		String message = "Login Failure!";
		return "redirect:/login?message="+message;
	}
	
	@RequestMapping(value = "/logout/success")
 	public String logoutSuccess() {
		String message = "Logout Success!";
		return "redirect:/index?message="+message;
	}
	
	@RequestMapping(value="/admin/access/forgotPassword", method=RequestMethod.GET)
	public String recoverPassword(){
		return "/access/forgotpassword";
	}

	@RequestMapping(value="/access/forgotPassword", method=RequestMethod.POST)
	public String recoverPassword(@RequestParam(required=true) String email) {
		boolean result;
		String message = "";
		result = accessService.recoverUserPassword(email);
		if(result) {
			message = "Successfully changed password, please check your email!";
			return "redirect:/login?message="+message;
		}
		message = "Unable to change the password, please try later!";
		return "redirect:/login?message="+message;
	}
	
	@RequestMapping(value="/admin/access/changePassword", method=RequestMethod.GET)
	public String changePassword(@RequestParam(required=false)String message) {
		return "/changePassword";
	}
	
	@RequestMapping(value="/access/changePassword", method=RequestMethod.POST)
	public String changePassword(@RequestParam(required=true) String oldPassword, @RequestParam(required=true) String newPassword) {
		
		String message = "";
		if(null == oldPassword || StringUtils.isBlank(oldPassword)) {
			message = "Incorrect current password!";
			return "redirect:/admin/access/changePassword?message=" + message;
		}
		if(null == newPassword || StringUtils.isBlank(newPassword)) {
			message = "Invalid new passwords!";
			return "redirect:/admin/access/changePassword?message=" + message;
		}
		UserInfo userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean isCorrectPass = userService.checkPassword(userInfo.getId(), oldPassword);
		if(isCorrectPass) {
			try {
				userService.changePassword(newPassword);
				message = "Password changed successfully";
				return "redirect:/home?message=" + message;
			} catch(Exception e) {
				e.printStackTrace();
				message = "Unable to change password..Please try again later";
			}
		} else {
			message = "Incorrect old password";
		}
		return "redirect:/admin/access/changePassword?message=" + message;
	}
	
	@RequestMapping(value="/non-secured/downloadApp")
	public String downloadApp() {
		return "/non-secured/downloadApp";
	}
	
	/*
	 * 
	 * REST API Methods
	 * 
	 */
	
	/**
	 * Actual login is done by spring, we will populate the response here
	 * @return
	 */
	@RequestMapping(value="/restapi/login", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse apiLogin() {
		ApiResponse apiResponse = new ApiResponse(true);
		UserDTO userDTO = new UserDTO(userService.getLoggedInUser());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("user", userDTO);
		apiResponse.setData(map);
		return apiResponse;
	}
	
	@RequestMapping(value = "/restapi/denied")
 	@ResponseBody 
 	public ApiResponse apiAccessDenied() {
		return new ApiResponse(false).setError("Invalid Email/Password or User is not enabled", "AccessDenied");
	}
	
}