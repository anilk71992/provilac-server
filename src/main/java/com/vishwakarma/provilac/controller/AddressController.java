package com.vishwakarma.provilac.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.AddressDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.model.Locality;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.mvc.validator.AddressValidator;
import com.vishwakarma.provilac.property.editor.CityPropertyEditor;
import com.vishwakarma.provilac.property.editor.LocalityPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.CityRepository;
import com.vishwakarma.provilac.repository.LocalityRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.LocalityService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;


@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class AddressController {

	@Resource
	private AddressValidator addressValidator;

	@Resource
	private AddressService addressService;

	@Resource
	private UserService userService;

	@Resource
	private UserRepository userRepository;

	@Resource
	private LocalityService localityService;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private CityService cityService;
	
	@Resource LocalityRepository localityRepository;
	
	@Resource
	private CityRepository cityRepository;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "address")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(City.class, new CityPropertyEditor(cityRepository));
		binder.registerCustomEditor(Locality.class, new LocalityPropertyEditor(localityRepository));
		binder.setValidator(addressValidator);
	}

	private Model populateModelForAdd(Model model, Address address) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("address", address);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("cities", cityService.getAllCities(loggedInUser, false, false));
		model.addAttribute("localities",localityService.getAllLocalities(loggedInUser, false, false));
		return model;

	}

	@RequestMapping(value = "/admin/address/add")
	public String addAddress(Model model) {
		model = populateModelForAdd(model, new Address());
		return "admin-pages/address/add";
	}

	@RequestMapping(value = "/admin/address/add", method = RequestMethod.POST)
	public String addressAdd(
			Address address,
			BindingResult formBinding,
			Model model,
			@RequestParam(required = false, value = "selectedCustomer") String userId) {

		if (StringUtils.isNotBlank(userId) && StringUtils.isNotEmpty(userId)) {
			User customer = userService.getUser(Long.parseLong(userId), true, userService.getLoggedInUser(), false, false);
			address.setCustomer(customer);
		}
		if (address.getIsDefault()) {
			List<Address> addresses = addressService.getAddressesByCustomer(
					address.getCustomer().getCode(), null, false, false);
			for (Address existingAddress : addresses) {
				existingAddress.setIsDefault(false);
				addressService.updateAddress(existingAddress.getId(),
						existingAddress, null, false, false);
			}
			address.setIsDefault(true);
		}
		addressValidator.validate(address, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, address);
			return "admin-pages/address/add";
		}
		String message="";
		User loggedInUser = userService.getLoggedInUser();
		address =addressService.createAddress(address, loggedInUser, true,false);
		
		if (StringUtils.isNotBlank(userId) && StringUtils.isNotEmpty(userId)) {
			message = "Address added successfully";
		}
		return "redirect:/admin/address/list?message=" + message ;
	}
	
	private Model populateModelForAddCustomer(Model model, User loggedInUser, Address address) {				
		model.addAttribute("address", address);
		Long id = loggedInUser.getId();
		model.addAttribute("CustomerId", id);
		model.addAttribute("users", userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false));
		model.addAttribute("cities", cityService.getAllCities(loggedInUser, false, false));
		model.addAttribute("localities",localityService.getAllLocalities(loggedInUser, false, false));
		return model;

	}
	
	@RequestMapping(value= "/createAddress")
	public String addCustomerAddress(Authentication authentication, Model model, HttpServletRequest request, 
				@RequestParam(required=false) String retUrl, @RequestParam(required=false) String message) {
		if (!StringUtils.isEmpty(retUrl)) {
			model.addAttribute("retUrl", retUrl);
		}
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		model = populateModelForAddCustomer(model, userInfo.getUser(), new Address());
		model.addAttribute("message", message);
		return "enduser-pages/createAddress";
	}
	
	@RequestMapping(value = "/createAddress", method = RequestMethod.POST)
	public String addCustomerAddress(Authentication authentication, Address address, BindingResult formBinding, Model model, 
					HttpServletRequest request, @RequestParam(required=false) String retUrl) {
		
		User loggedInUser = userService.getLoggedInUser();
		String pinecodes = systemPropertyHelper.getSystemProperty("PineCode").getPropValue();
		List<String> pinecodeList = Arrays.asList(pinecodes.split(","));
		boolean flag = false;
		for(String pinecode : pinecodeList) {
			if(address.getPincode().equalsIgnoreCase(pinecode)) {
				flag = true;
				break;
			}
		}
		if(flag == false) {
			if (!StringUtils.isEmpty(retUrl)) {
				model.addAttribute("retUrl", retUrl);
			}
			model = populateModelForAddCustomer(model, loggedInUser, new Address());
			model.addAttribute("message", "We are not deliver in this area");
			return "enduser-pages/createAddress";
		}
		
		List<City> cities = cityService.getAllCities(loggedInUser, true, true); 
		if(null == cities || cities.isEmpty()) {
			model.addAttribute("message", "Please add city first !");
			return "enduser-pages/createAddress";
		}
		address.setCity(cities.get(0));
		address.setCustomer(loggedInUser);
		address.setName(address.getAddressLine1());
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, address);
			return "enduser-pages/createAddress";
		}
		String message="";
		address = addressService.createAddress(address, loggedInUser, false,false);
		message = "Address added successfully";
		if (!StringUtils.isEmpty(retUrl)) {
			return "redirect:"+retUrl+"?message="+message;
		}
		return "redirect:/checkout?message=" + message ;
	}
	
	@RequestMapping(value = "/updateAddress/{id}")
	public String updateAddress(@PathVariable long id, Model model, @RequestParam(required=false) String retUrl) {
		
		if (!StringUtils.isEmpty(retUrl)) {
			model.addAttribute("retUrl", retUrl);
		}
		User loggedInUser = userService.getLoggedInUser();
		Address address = addressService.getAddress(id, true, loggedInUser, false, true);		
		model = populateModelForAdd(model, address);
		model.addAttribute("id", address.getId());
		model.addAttribute("version", address.getVersion());
		model.addAttribute("customer", loggedInUser);
		model.addAttribute("address",address);
		return "enduser-pages/editAddress";
	}
	
	@RequestMapping(value="/address/update", method=RequestMethod.POST)
	public String UpdateCustomeraddress(Authentication authentication, Address address, BindingResult formBinding, Model model, 
				@RequestParam(required=false) String retUrl, @RequestParam(required=false) Long addressId) {
		
		User loggedInUser = userService.getLoggedInUser();
		String pinecodes = systemPropertyHelper.getSystemProperty("PineCode").getPropValue();
		List<String> pinecodeList = Arrays.asList(pinecodes.split(","));
		boolean flag = false;
		for(String pinecode : pinecodeList) {
			if(address.getPincode().equalsIgnoreCase(pinecode)) {
				flag = true;
				break;
			}
		}
		if(flag == false) {
			if (!StringUtils.isEmpty(retUrl)) {
				model.addAttribute("retUrl", retUrl);
			}
			Address address1 = addressService.getAddress(addressId, true, loggedInUser, false, true);		
			model = populateModelForAdd(model, address1);
			model.addAttribute("id", address1.getId());
			model.addAttribute("version", address1.getVersion());
			model.addAttribute("customer", loggedInUser);
			model.addAttribute("address",address1);
			model.addAttribute("message", "We are not deliver which given area");
			return "enduser-pages/editAddress";
		}
		
		address.setUpdateOperation(true);
		address.setId(Long.parseLong(model.asMap().get("id") + ""));
		address.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		address.setCustomer(userInfo.getUser());
		address.setName(address.getAddressLine1());
		/*addressValidator.validate(address, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, address);
			return "enduser-pages/editAddress";
		}*/
		
		addressService.updateAddress(address.getId(), address, loggedInUser, true, false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		model.asMap().remove("password");
		
		String message = "Address updated successfully";
		if (!StringUtils.isEmpty(retUrl)) {
			return "redirect:"+retUrl+"?message="+message;
		}
		return "redirect:/checkout?message=" + message ;
	}
	

	@RequestMapping(value = "/admin/address/list")
	public String getAddressList(Model model,
			@RequestParam(defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false) String message,
			@RequestParam(required = false) String searchTerm) {

		model.addAttribute("message", message);
		Page<Address> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = addressService.searchAddress(pageNumber, searchTerm, user,
					true, false);
		} else {
			page = addressService.getAddresses(pageNumber, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("addresses", page.getContent());

		return "/admin-pages/address/list";
	}
	
	@RequestMapping(value= "/end-user/checkout")
	public String checkout(Authentication authentication, Model model, HttpServletRequest request) {		
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		List<Address> list = addressService.getAddressesByCustomer(userInfo.getUser().getCode(), userInfo.getUser(), true, false);
		model.addAttribute("addresss",list);
		return "/endUser-page/checkout";
	}
	
	@RequestMapping(value= "/prepay-checkout")
	public String prepayCheckout(Authentication authentication, Model model, HttpServletRequest request) {		
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		List<Address> list = addressService.getAddressesByCustomer(userInfo.getUser().getCode(), userInfo.getUser(), true, false);
		model.addAttribute("addresss",list);
		return "/endUser-page/create-prepay-subscription/prepay-checkout";
	}

	@RequestMapping(value = "/admin/address/show/{cipher}")
	public String showAddress(@PathVariable String cipher, Model model) {
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Address address = addressService.getAddress(id, true, loggedIUser, true, false);
		model.addAttribute("address", address);
		return "/admin-pages/address/show";
	}

	@RequestMapping(value = "/admin/address/update/{cipher}")
	public String updateAddress(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		Address address = addressService.getAddress(id, true, loggedInUser, true, false);
		model = populateModelForAdd(model, address);
		model.addAttribute("id", address.getId());
		model.addAttribute("version", address.getVersion());
		return "/admin-pages/address/update";
	}

	@RequestMapping(value = "/admin/address/update", method = RequestMethod.POST)
	public String addressUpdate(Address address, BindingResult formBinding,
			Model model,
			@RequestParam(required = false, value = "selectedCustomer") String userId) {

		if (StringUtils.isNotBlank(userId) && StringUtils.isNotEmpty(userId)) {
			User customer = userService.getUser(Long.parseLong(userId), true, userService.getLoggedInUser(), false, false);
			address.setCustomer(customer);
		}
		address.setId(Long.parseLong(model.asMap().get("id") + ""));
		if (address.getIsDefault()) {
			List<Address> addresses = addressService.getAddressesByCustomer(
					address.getCustomer().getCode(), null, false, false);
			for (Address existingAddress : addresses) {
				existingAddress.setIsDefault(false);
				addressService.updateAddress(existingAddress.getId(),
						existingAddress, null, false, false);
			}
			address.setIsDefault(true);
		}
		User loggedInUser = userService.getLoggedInUser();
		address.setUpdateOperation(true);
		addressValidator.validate(address, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, address);
			return "admin-pages/address/update";
		}

		address.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		addressService.updateAddress(address.getId(), address, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Address updated successfully";
		return "redirect:/admin/address/list?message=" + message;
	}
	
	@RequestMapping(value = "/end-user/address/update/{cipher}")
	public String updateAddressOfCustomer(Authentication authentication, @PathVariable String cipher, Model model, HttpServletRequest request) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		long id = Long.parseLong(EncryptionUtil.decode(cipher));
		Address address = addressService.getAddress(id, true, userInfo.getUser(), true, false);
		model = populateModelForAddCustomer(model, userInfo.getUser(), address);		
		model.addAttribute("id", address.getId());
		model.addAttribute("version", address.getVersion());
		return "/endUser-page/editAddress";
	}
	
	@RequestMapping(value = "/end-user/address/update", method = RequestMethod.POST)
	public String updateAddressOfCustomer(Authentication authentication, Address address, BindingResult formBinding, Model model, HttpServletRequest request) {
		
		UserInfo userInfo = (UserInfo) authentication.getPrincipal();
		address.setId(Long.parseLong(model.asMap().get("id") + ""));
		address.setCustomer(userInfo.getUser());
		if (address.getIsDefault()) {
			List<Address> addresses = addressService.getAddressesByCustomer(userInfo.getUser().getCode(), null, false, false);
			for (Address existingAddress : addresses) {
				existingAddress.setIsDefault(false);
				addressService.updateAddress(existingAddress.getId(), existingAddress, null, false, false);
			}
			address.setIsDefault(true);
		}
		address.setUpdateOperation(true);		

		if (formBinding.hasErrors()) {
			model = populateModelForAddCustomer(model, userInfo.getUser(), address);
			return "/endUser-page/editAddress";
		}

		address.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		addressService.updateAddress(address.getId(), address, userInfo.getUser(), true, false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Address updated successfully";
		model.addAttribute("message", message);
		return "redirect:/end-user/checkout?message=" + message;
	}

	@RequestMapping(value = "/admin/address/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			addressService.deleteAddress(id);
			message = "Address deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Address";
		}
		return "redirect:/admin/address/list?message=" + message;
	}
	
	@RequestMapping(value = "/address/delete/{id}")
	public String deleteCutomerAddress(Model model, @PathVariable long id, @RequestParam(required=false) String retUrl) {
		String message = "";
		try {
			addressService.deleteAddress(id);
			message = "Address deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Address";
		}
		if (!StringUtils.isEmpty(retUrl)) {
			return "redirect:"+retUrl+"?message="+message;
		}
		return "redirect:/checkout?message=" + message;
	}

	/**
	 * REST API Methods
	 */
	@RequestMapping(value = "/restapi/address/add", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse addAddress(
			@RequestBody(required = true) AddressDTO addressDTO) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User loggedInUser = userService.getLoggedInUser();
			
			Address address = new Address();
			address.setCustomer(loggedInUser);
			
			City city = cityService.getCityByCode(addressDTO.getCityCode(), false, null, false, false);
			address.setCity(city);
			if(StringUtils.isNotBlank(addressDTO.getLocalityName())&& StringUtils.isNotEmpty(addressDTO.getLocalityName())){
				address.setLocality(addressDTO.getLocalityName());
			}
			
			if(StringUtils.isNotBlank(addressDTO.getPincode())&& StringUtils.isNotEmpty(addressDTO.getPincode())){
				address.setPincode(addressDTO.getPincode());
			}
			
			address.setAddressLine1(addressDTO.getAddressLine1());
			
			if (StringUtils.isNotBlank(addressDTO.getAddressLine2())&& StringUtils.isNotEmpty(addressDTO.getAddressLine2())) {
				address.setAddressLine2(addressDTO.getAddressLine2().trim());
			}
			
			if(StringUtils.isNotBlank(addressDTO.getLandmark()) && StringUtils.isNotEmpty(addressDTO.getLandmark())){
			address.setLandmark(addressDTO.getLandmark());
			}
			
			if(StringUtils.isNotBlank(addressDTO.getName().trim()) && StringUtils.isNotEmpty(addressDTO.getName().trim())){
			address.setName(addressDTO.getName());
			}
			if (addressDTO.getLat() != null && addressDTO.getLng() != null) {
				address.setLat(addressDTO.getLat());
				address.setLng(addressDTO.getLng());
			}

			BindingResult bindingResult = new DataBinder(address)
					.getBindingResult();
			addressValidator.validate(address, bindingResult);
			if (bindingResult.hasErrors()) {
				String errorStr = null;
				for (ObjectError objectError : bindingResult.getAllErrors()) {
					if (StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
					} else {
						errorStr = errorStr + ","
								+ objectError.getDefaultMessage();
					}
				}
				throw new ProvilacException(errorStr, "400");
			}
			address = addressService.createAddress(address, loggedInUser, false, false);
			address = addressService.createAddress(address, loggedInUser,
					false, false);

			apiResponse.addData("address", new AddressDTO(address));

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;

	}

	@RequestMapping(value = "/restapi/address/list")
	@ResponseBody
	public ApiResponse getAddressesByUser(
			@RequestParam(required = true) String customerCode) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			List<Address> addresses = addressService.getAddressesByCustomer(customerCode, null, false, false);
			List<AddressDTO> addressDTOs = new ArrayList<AddressDTO>();
			for (Address address : addresses) {
						addressDTOs.add(new AddressDTO(address));
					}
			
			apiResponse.addData("addresses", addressDTOs);
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;

	}

	@RequestMapping(value = "/restapi/address/update", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateAddress(@RequestBody AddressDTO addressDTO) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User loggedInUser = userService.getLoggedInUser();
			Address address = addressService.getAddress(addressDTO.getId(),true, loggedInUser, false, false);
			address.setAddressLine1(addressDTO.getAddressLine1().trim());
			if (StringUtils.isNotBlank(addressDTO.getAddressLine2())&& StringUtils.isNotEmpty(addressDTO.getAddressLine2())) {
				address.setAddressLine2(addressDTO.getAddressLine2().trim());
			}
			City city = cityService.getCityByCode(addressDTO.getCityCode(), false, null, false, false);
			address.setCity(city);
			
				address.setLocality(addressDTO.getLocalityName());
				
				address.setPincode(addressDTO.getPincode());
				
				if(StringUtils.isNotBlank(addressDTO.getAddressLine1().trim()) && StringUtils.isNotEmpty(addressDTO.getAddressLine1().trim())){
					address.setAddressLine1(addressDTO.getAddressLine1());
				}
			
				address.setAddressLine2(addressDTO.getAddressLine2());
			
				address.setLandmark(addressDTO.getLandmark());
			
			if(StringUtils.isNotBlank(addressDTO.getName().trim()) && StringUtils.isNotEmpty(addressDTO.getName().trim())){
				address.setName(addressDTO.getName().trim());
			}
			
			if (addressDTO.getLat() != null && addressDTO.getLng() != null) {
				address.setLat(addressDTO.getLat());
				address.setLng(addressDTO.getLng());
			}
			
			if (addressDTO.isDefault()) {
				List<Address> addresses = addressService.getAddressesByCustomer(address.getCustomer().getCode(), loggedInUser, false,
								false);
				for (Address existingAddress : addresses) {
					existingAddress.setIsDefault(false);
					addressService.updateAddress(existingAddress.getId(),existingAddress, loggedInUser, false, false);
				}
				address.setIsDefault(true);
			}

			address.setUpdateOperation(true);
			BindingResult bindingResult = new DataBinder(address)
					.getBindingResult();
			addressValidator.validate(address, bindingResult);
			if (bindingResult.hasErrors()) {
				String errorStr = null;
				for (ObjectError objectError : bindingResult.getAllErrors()) {
					if (StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
					} else {
						errorStr = errorStr + ","
								+ objectError.getDefaultMessage();
					}
				}
				throw new ProvilacException(errorStr, "400");
			}
			
			address = addressService.updateAddress(address.getId(), address, loggedInUser, false,false);

			address = addressService.updateAddress(address.getId(), address,
					loggedInUser, false, false);

			apiResponse.addData("address", new AddressDTO(address));

		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value = "/restapi/address/delete")
	@ResponseBody
	public ApiResponse deleteAddress(
			@RequestParam(required = true) String addressCode) {
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Address address = addressService.getAddressByCode(addressCode,	false, null, false, false);
				addressService.deleteAddress(address.getId());
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * End REST API Methods
	 */
}
