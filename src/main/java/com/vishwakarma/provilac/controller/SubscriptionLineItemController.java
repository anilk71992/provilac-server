package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.SubscriptionLineItemValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.SubscriptionLineItemService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class SubscriptionLineItemController {

	@Resource
	private SubscriptionLineItemValidator subscriptionLineItemValidator;

	@Resource
	private SubscriptionLineItemService subscriptionLineItemService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "subscriptionLineItem")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(subscriptionLineItemValidator);
	}

	private Model populateModelForAdd(Model model, SubscriptionLineItem subscriptionLineItem) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("subscriptionLineItem", subscriptionLineItem);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/add")
	public String addSubscriptionLineItem(Model model) {
		model = populateModelForAdd(model, new SubscriptionLineItem());
		return "admin-pages/subscriptionLineItem/add";
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/add", method = RequestMethod.POST)
	public String subscriptionLineItemAdd(SubscriptionLineItem subscriptionLineItem, 
									BindingResult formBinding, 
									Model model){
		
		subscriptionLineItemValidator.validate(subscriptionLineItem, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, subscriptionLineItem);
			return "admin-pages/subscriptionLineItem/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		subscriptionLineItemService.createSubscriptionLineItem(subscriptionLineItem, loggedInUser, true,false);
		String message = "SubscriptionLineItem added successfully";
		return "redirect:/admin/subscriptionLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/list")
	public String getSubscriptionLineItemList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<SubscriptionLineItem> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = subscriptionLineItemService.searchSubscriptionLineItem(pageNumber, searchTerm, user,true,false);
		} else {
			page = subscriptionLineItemService.getSubscriptionLineItems(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("subscriptionLineItems", page.getContent());

		return "/admin-pages/subscriptionLineItem/list";
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/show/{cipher}")
	public String showSubscriptionLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		SubscriptionLineItem subscriptionLineItem = subscriptionLineItemService.getSubscriptionLineItem(id, true, loggedIUser,true,false);
		model.addAttribute("subscriptionLineItem", subscriptionLineItem);
		return "/admin-pages/subscriptionLineItem/show";
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/update/{cipher}")
	public String updateSubscriptionLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		SubscriptionLineItem subscriptionLineItem = subscriptionLineItemService.getSubscriptionLineItem(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, subscriptionLineItem);
		model.addAttribute("id", subscriptionLineItem.getId());
		model.addAttribute("version", subscriptionLineItem.getVersion());
		return "/admin-pages/subscriptionLineItem/update";
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/update", method = RequestMethod.POST)
	public String subscriptionLineItemUpdate(SubscriptionLineItem subscriptionLineItem, 
										BindingResult formBinding, 
										Model model) {

		subscriptionLineItem.setUpdateOperation(true);
		subscriptionLineItemValidator.validate(subscriptionLineItem, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, subscriptionLineItem);
			return "admin-pages/subscriptionLineItem/update";
		}
		subscriptionLineItem.setId(Long.parseLong(model.asMap().get("id") + ""));
		subscriptionLineItem.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "SubscriptionLineItem updated successfully";
		return "redirect:/admin/subscriptionLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/subscriptionLineItem/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			subscriptionLineItemService.deleteSubscriptionLineItem(id);
			message = "SubscriptionLineItem deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete SubscriptionLineItem";
		}
		return "redirect:/admin/subscriptionLineItem/list?message=" + message;
	}
	
	
	/*
	 * Ajax Method
	 * 
	 */
	
	@RequestMapping(value="/subscriptionLineItem/getAllProducts")
	@ResponseBody
	public ApiResponse getAllProducts(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<Product> products = productService.getAllProducts(loggedInUser, false, false);
			List<ProductDTO> dtos = new ArrayList<ProductDTO>();
			for (Product product : products) {
				dtos.add(new ProductDTO(product));
			}
			apiResponse.addData("products", dtos);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value="/subscriptionLineItem/getProductsForSubscription")
	@ResponseBody
	public ApiResponse getProductsForSubscription(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<Product> products = productService.getProductsForSubscription(loggedInUser, false, false);
			List<ProductDTO> dtos = new ArrayList<ProductDTO>();
			for (Product product : products) {
				dtos.add(new ProductDTO(product));
			}
			apiResponse.addData("products", dtos);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

}
