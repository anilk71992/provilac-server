package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.DepositDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Collection;
import com.vishwakarma.provilac.model.Deposit;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.DepositValidator;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.CollectionService;
import com.vishwakarma.provilac.service.DepositService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;

/**
 * 
 * @author Harshal
 *
 */
@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class DepositController {

	@Resource
	private DepositValidator depositValidator;

	@Resource
	private DepositService depositService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private CollectionService collectionService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@InitBinder(value = "deposit")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.setValidator(depositValidator);
	}

	private Model populateModelForAdd(Model model, Deposit deposit) {
		model.addAttribute("deposit", deposit);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/deposit/add")
	public String addDeposit(Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new Deposit());
		return "admin-pages/deposit/add";
	}

	@RequestMapping(value = "/admin/deposit/add", method = RequestMethod.POST)
	public String depositAdd(Deposit deposit, 
									BindingResult formBinding, 
									Model model){
		
		depositValidator.validate(deposit, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deposit);
			return "admin-pages/deposit/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		deposit = depositService.createDeposit(deposit, loggedInUser, true,true);
		String message = "Deposit added successfully";
		return "redirect:/admin/deposit/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deposit/list")
	public String getDepositList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm,
										@RequestParam(required = false) String userCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<Deposit> page = null;
		User user = userService.getLoggedInUser();
		if(null!=userCode){
			page = depositService.getDepositsByCustomerCode(userCode, pageNumber, user, false, false);
		}else{
			if (StringUtils.isNotBlank(searchTerm)) {
				page = depositService.searchDeposit(pageNumber, searchTerm, user,true,false);
			} else {
				page = depositService.getDeposits(pageNumber, user, true, true);
			}
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("deposits", page.getContent());

		return "/admin-pages/deposit/list";
	}

	@RequestMapping(value = "/admin/deposit/show/{cipher}")
	public String showDeposit(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		Deposit deposit = depositService.getDeposit(id, true, loggedIUser,true,false);
		model.addAttribute("deposit", deposit);
		return "/admin-pages/deposit/show";
	}

	@RequestMapping(value = "/admin/deposit/update/{cipher}")
	public String updateDeposit(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		Deposit deposit = depositService.getDeposit(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, deposit);
		model.addAttribute("id", deposit.getId());
		model.addAttribute("version", deposit.getVersion());
		return "/admin-pages/deposit/update";
	}

	@RequestMapping(value = "/admin/deposit/update", method = RequestMethod.POST)
	public String depositUpdate(Deposit deposit, 
										BindingResult formBinding, 
										Model model) {

		deposit.setUpdateOperation(true);
		depositValidator.validate(deposit, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deposit);
			return "admin-pages/deposit/update";
		}
		deposit.setId(Long.parseLong(model.asMap().get("id") + ""));
		deposit.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		depositService.updateDeposit(deposit.getId(), deposit, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "Deposit updated successfully";
		return "redirect:/admin/deposit/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deposit/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			depositService.deleteDeposit(id);
			message = "Deposit deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete Deposit";
		}
		return "redirect:/admin/deposit/list?message=" + message;
	}
	
	@RequestMapping(value="/admin/deposit/accountProfile")
	public String getAccountProfile(Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		List<User> collectionBoys = userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser, false, false);
		model.addAttribute("collectionBoys",collectionBoys);
		return "admin-pages/deposit/accountProfile";
	}
	
	@RequestMapping(value="/admin/deposit/accountProfile",method=RequestMethod.POST)
	public String getUserAccountProfile(@RequestParam String code,Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		List<User> collectionBoys = userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser, false, false);
		if(code.equals("none")){
			model.addAttribute("collectionBoys",collectionBoys);
			return "admin-pages/deposit/accountProfile";
		}
		
		User collectionBoy = userService.getUserByCode(code, false, loggedInUser, true, false);

		List<Deposit> deposits = depositService.getAllDepositsByCollectionBoy(code, loggedInUser, true, true);
		List<Collection> collections =  collectionService.getAllCollectionsByCollectionBoy(code, loggedInUser, true, true);

		model.addAttribute("totalDues",collectionBoy.getCollectionOuststanding());
		model.addAttribute("collectionBoyCode",collectionBoy.getCode());
		model.addAttribute("deposits", deposits);
		model.addAttribute("collections", collections);
		model.addAttribute("collectionBoys",collectionBoys);
		return "admin-pages/deposit/accountProfile";
	}
	
	@RequestMapping(value="/restapi/deposit/list")
	@ResponseBody
	public ApiResponse getDepositsByCustomer(@RequestParam(defaultValue = "1") Integer pageNumber,
											 @RequestParam(required=true)String customerCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Page<Deposit> page =  depositService.getAllDepositsByCollectionBoy(pageNumber,customerCode, null, false, true);
			List<DepositDTO>dtos = new ArrayList<DepositDTO>();
			for (Deposit deposit : page) {
				dtos.add(new DepositDTO(deposit));
			}
			apiResponse.addData("Deposits", dtos);
			
			int current = page.getNumber() + 1;
			int begin = Math.max(1, current - 5);
			int end = Math.min(begin + 10, page.getTotalPages());

			apiResponse.addData("Deposits", dtos).addData("current", current).addData("begin", begin).addData("end", end);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/deposit/details")
	@ResponseBody
	public ApiResponse getDepositDetails(@RequestParam(required=true)String depositCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Deposit deposit =  depositService.getDepositByCode(depositCode, false, null, false, false);
			apiResponse.addData("Deposit", new DepositDTO(deposit));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/deposit/addDepositByCollectionBoy", method= RequestMethod.POST)
	@ResponseBody
	public ApiResponse addDepositByCollectionBoy(@RequestPart(required=true)String[] paymentCodes,
												 @RequestPart(required=false)String cashTransactionId,
												 @RequestPart(required=false)Double cashAmount){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User collectionBoy = userService.getLoggedInUser();
			
			Deposit deposit = new Deposit();
			
			deposit.setCollectionBoy(collectionBoy);
			deposit.setCashAmount(cashAmount);
			deposit.setDate(new Date());
			deposit.setCashTransactionId(cashTransactionId);
			
			deposit = depositService.createDeposit(deposit, collectionBoy, false, true);
			
			double chequeAmount = 0.0;
			int chequeNumbers = 0;
			
			if(null != paymentCodes){
				for (int i = 0; i < paymentCodes.length; i++) {
					Payment payment = paymentService.getPaymentByCode(paymentCodes[i], false, collectionBoy, false, false);
					if(null != payment){
						payment.setDeposit(deposit);
						payment.setHasDeposited(true);
						chequeAmount += payment.getAmount();
						chequeNumbers += 1;
						paymentService.updatePayment(payment.getId(), payment, collectionBoy, false, false);
					}
				}
			}
			
			deposit.setChequeAmount(chequeAmount);
			deposit.setNoOfCheques(chequeNumbers);
			deposit.setTotalAmount(chequeAmount+cashAmount);
			deposit = depositService.updateDeposit(deposit.getId(), deposit, collectionBoy, false, true);
			
			firebaseWrapper.sendCollectionBoyAmountDepositeNotification(deposit);
			
			//Updated collection Boy OutStanding
			collectionBoy.setCollectionOuststanding(collectionBoy.getCollectionOuststanding() - cashAmount);
			collectionBoy = userService.updateCollectionBoyCollectionOutstanding(collectionBoy.getId(), collectionBoy, collectionBoy, false, false);
			apiResponse.addData("deposit", new DepositDTO(deposit));
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/** 
	 * End REST API Methods
	 */
}
