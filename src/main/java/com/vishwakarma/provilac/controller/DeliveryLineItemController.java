package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.DeliveryLineItemValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.DeliveryLineItemService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class DeliveryLineItemController {

	@Resource
	private DeliveryLineItemValidator deliveryLineItemValidator;

	@Resource
	private DeliveryLineItemService deliveryLineItemService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "deliveryLineItem")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(deliveryLineItemValidator);
	}

	private Model populateModelForAdd(Model model, DeliveryLineItem deliveryLineItem) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("deliveryLineItem", deliveryLineItem);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/deliveryLineItem/add")
	public String addDeliveryLineItem(Model model) {
		model = populateModelForAdd(model, new DeliveryLineItem());
		return "admin-pages/deliveryLineItem/add";
	}

	//MAYBE_UNUSED
	@Deprecated
	@RequestMapping(value = "/admin/deliveryLineItem/add", method = RequestMethod.POST)
	public String deliveryLineItemAdd(DeliveryLineItem deliveryLineItem, 
									BindingResult formBinding, 
									Model model){
		
		deliveryLineItemValidator.validate(deliveryLineItem, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deliveryLineItem);
			return "admin-pages/deliveryLineItem/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		deliveryLineItemService.createDeliveryLineItem(deliveryLineItem, loggedInUser, true,false);
		String message = "DeliveryLineItem added successfully";
		return "redirect:/admin/deliveryLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deliveryLineItem/list")
	public String getDeliveryLineItemList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<DeliveryLineItem> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = deliveryLineItemService.searchDeliveryLineItem(pageNumber, searchTerm, user,true,false);
		} else {
			page = deliveryLineItemService.getDeliveryLineItems(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("deliveryLineItems", page.getContent());

		return "/admin-pages/deliveryLineItem/list";
	}

	@RequestMapping(value = "/admin/deliveryLineItem/show/{cipher}")
	public String showDeliveryLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		DeliveryLineItem deliveryLineItem = deliveryLineItemService.getDeliveryLineItem(id, true, loggedIUser,true,false);
		model.addAttribute("deliveryLineItem", deliveryLineItem);
		return "/admin-pages/deliveryLineItem/show";
	}

	@RequestMapping(value = "/admin/deliveryLineItem/update/{cipher}")
	public String updateDeliveryLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		DeliveryLineItem deliveryLineItem = deliveryLineItemService.getDeliveryLineItem(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, deliveryLineItem);
		model.addAttribute("id", deliveryLineItem.getId());
		model.addAttribute("version", deliveryLineItem.getVersion());
		return "/admin-pages/deliveryLineItem/update";
	}

	//MAYBE_UNUSED
	@Deprecated
	@RequestMapping(value = "/admin/deliveryLineItem/update", method = RequestMethod.POST)
	public String deliveryLineItemUpdate(DeliveryLineItem deliveryLineItem, 
										BindingResult formBinding, 
										Model model) {

		deliveryLineItem.setUpdateOperation(true);
		deliveryLineItemValidator.validate(deliveryLineItem, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, deliveryLineItem);
			return "admin-pages/deliveryLineItem/update";
		}
		deliveryLineItem.setId(Long.parseLong(model.asMap().get("id") + ""));
		deliveryLineItem.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		deliveryLineItemService.updateDeliveryLineItem(deliveryLineItem.getId(), deliveryLineItem, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "DeliveryLineItem updated successfully";
		return "redirect:/admin/deliveryLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/deliveryLineItem/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			deliveryLineItemService.deleteDeliveryLineItem(id);
			message = "DeliveryLineItem deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete DeliveryLineItem";
		}
		return "redirect:/admin/deliveryLineItem/list?message=" + message;
	}
	
	
	/*
	 * Ajax Method
	 * 
	 */
	
	/*
	 * End Ajax Method
	 */

	/**
	 * REST API Methods
	 */
	
	@RequestMapping(value="/restapi/deliveryLineItem/populateLineItemPrices")
	@ResponseBody
	public ApiResponse populateLineItemPrices() {
		asyncJobs.populateDeliverylineItemPrice();
		return new ApiResponse(true);
	}

	/**
	 * End REST API Methods
	 */
}
