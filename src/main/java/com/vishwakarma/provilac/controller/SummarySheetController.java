package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.SummarySheetDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SummarySheet;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.SummarySheetRecordValidator;
import com.vishwakarma.provilac.mvc.validator.SummarySheetValidator;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.SummarySheetRecordService;
import com.vishwakarma.provilac.service.SummarySheetService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.ScheduledJobs;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class SummarySheetController {

	@Resource
	private SummarySheetValidator summarySheetValidator;

	@Resource
	private SummarySheetService summarySheetService;

	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ScheduledJobs scheduledJobs;
	
	@Resource
	private SummarySheetRecordService summarySheetRecordService;
	
	@Resource
	private SummarySheetRecordValidator summarySheetRecordValidator;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@InitBinder(value = "summarySheet")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(summarySheetValidator);
	}

	private Model populateModelForAdd(Model model, SummarySheet summarySheet) {
		model.addAttribute("summarySheet", summarySheet);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}


	@RequestMapping(value = "/admin/summarySheet/list")
	public String getSummarySheetList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<SummarySheet> page = null;
		User user = userService.getLoggedInUser();
	/*	scheduledJobs.genrateSummarySheet();*/
		if (StringUtils.isNotBlank(searchTerm)) {
			page = summarySheetService.searchSummarySheet(pageNumber, searchTerm, user,true,false);
		} else {
			page = summarySheetService.getSummarySheets(pageNumber, user,true,true);
			
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("summarySheets", page.getContent());

		return "/admin-pages/summarySheet/list";
	}

	@RequestMapping(value = "/admin/summarySheet/show/{cipher}")
	public String showSummarySheet(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		SummarySheet summarySheet = summarySheetService.getSummarySheet(id, true, loggedIUser,true,false);
		model.addAttribute("summarySheet", summarySheet);
		return "/admin-pages/summarySheet/show";
	}
	
	
	
	@RequestMapping(value = "/admin/summarySheet/update/{cipher}")
	public String updateSummarySheet(@PathVariable String cipher, Model model) {

		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		SummarySheet summarySheet = summarySheetService.getSummarySheet(id, false, loggedInUser, false, true);
		List<User> users = userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser, false, false);
		model = populateModelForAdd(model, summarySheet);
		model.addAttribute("id", summarySheet.getId());
		model.addAttribute("collectionBoys", users);
		model.addAttribute("version", summarySheet.getVersion());
		return "/admin-pages/summarySheet/update";
	}

	@RequestMapping(value = "/admin/summarySheet/update", method = RequestMethod.POST)
	public String summarySheetUpdate(SummarySheet summarySheet, 
										BindingResult formBinding, 
										Model model,
										@RequestParam(required=false,value="record")String[] summarySheetRecordCodes,
										@RequestParam(required=false,value="adjustment")Double[] adjustments,
										@RequestParam(required=false,value="paymentMethod")String[] payentMethods,
										@RequestParam(required=false,value="chequeNumber")String[] chequeNumbers,
										@RequestParam(required=false,value="chequeDate")String[] chequeDates,
										@RequestParam(required=false,value="receivedDate")String[] receivedDates,
										@RequestParam(required=false,value="receiptNumber")String[] receiptNumbers,
										@RequestParam(required=false,value="receivedBy")Long[] customrtIds) {
		
		
		for(int i=0;i<summarySheetRecordCodes.length;i++){
			 String summarySheetRecordCode = summarySheetRecordCodes[i];
			 SummarySheetRecord sheetRecord = summarySheetRecordService.getSummarySheetRecordByCode(summarySheetRecordCode, false, null, false, false);
			 
			  long receivedById = customrtIds[i];
			  
			  if(adjustments.length!=0){
				  double adjustment = adjustments[i];
				  sheetRecord.setAdjustment(adjustment);
			  }
			  if(payentMethods.length!=0){
				  String paymentMethod = payentMethods[i];
				  sheetRecord.setPaymentMethod(PaymentMethod.valueOf(paymentMethod));
			  }
			  if(chequeNumbers.length!=0){
				  String chequeNumber = chequeNumbers[i];
				  sheetRecord.setChequeNumber(chequeNumber);
			  }
			  if(chequeDates.length!=0){
				  String chequeDate = chequeDates[i];
				  sheetRecord.setChequeDate(DateHelper.parseDate(chequeDate));
			  }
			 if(receivedDates.length!=0){
				  String receivedDate = receivedDates[i];
				 sheetRecord.setReceivedDate(DateHelper.parseDate(receivedDate));
			  
			 }if( receiptNumbers.length!=0){
				 String receiptNumer = receiptNumbers[i];
				  sheetRecord.setReceiptNumber(receiptNumer);
			 }
			  User receivedBy = userService.getUser(receivedById, false, null, false, false);
			  if(null!=receivedBy)
				  sheetRecord.setReceivedBy(receivedBy);
			  
			  sheetRecord.setUpdateOperation(true);
			  summarySheetRecordValidator.validate(sheetRecord, formBinding);
			
			  if (formBinding.hasErrors()) {
					model = populateModelForAdd(model, summarySheet);
					return "admin-pages/summarySheet/update";
				}
			  summarySheetRecordService.updateSummarySheetRecord(sheetRecord.getId(), sheetRecord, null, false, false);
		}
		String message = "SummarySheet updated successfully";
		return "redirect:/admin/summarySheet/list?message=" + message;
	}

	
	
	
	@RequestMapping("/admin/summarySheet/export/{summarySheetCode}")
	public ResponseEntity<byte []> exportToExcel(HttpServletResponse httpServletResponse, @PathVariable String summarySheetCode) throws IOException {
		
		org.springframework.core.io.Resource  tp = new ClassPathResource("/excelSheet/SummarySheet.xls");
		File file = tp.getFile();
		String fName = file.getName();
		SummarySheet summarySheet = summarySheetService.getSummarySheetByCode(summarySheetCode, false, null, false, true);
		Set<SummarySheetRecord> records= new HashSet<SummarySheetRecord>(summarySheet.getSummarySheetRecords());	
		List<SummarySheetRecord> sheetRecords = new ArrayList<SummarySheetRecord>();
		for (SummarySheetRecord summarySheetRecord : records) {
			sheetRecords.add(summarySheetRecord);
		}
		
		ByteArrayOutputStream byteArrayOutputStream = exportSummarySheetToExcel(file, sheetRecords,summarySheet.getFromDate(),summarySheet.getToDate());
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/xls");
		httpServletResponse.setHeader("Content-Disposition", "inline;filename=SummarySheet.xls");
		httpServletResponse.setHeader("Content-Length", String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(), httpServletResponse.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return null;
	}
	
private ByteArrayOutputStream exportSummarySheetToExcel(File file, List<SummarySheetRecord> sheetRecords,Date fromDate,Date toDate) throws IOException {
		
		int rowIndex=1;
		double totalAmountOfAllCustomers=0;
		double totalLastPendingDuesOfAllCustomers=0;
		double totalPaidAmountOfAllCustomers=0;
		double totalCashbackOfAllCustomers=0;
		double totalAdjustmentsOfAllCustomers=0;
		double totalOutStandingsOfAllCustomers=0;
		
		//Start: This variables for Individual Customer
		double adjustmentAmountOfCustomer = 0;
		double paidAmountOfCustomer = 0, cashback = 0;
		String paymentMethodOfCustomer = "";
		String chequeNumberOfCustomer = "";
		String paymentDateOfCustomer ="";
		String receiptNumberOfCustomer = "";
		String paidForNameOfCustomer = "";
		String paymentByOfCustomer = "";
		//End: This variables for Individual Customer
		
		SimpleDateFormat sdf = new SimpleDateFormat(DateHelper.DATE_FORMAT);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH,1);
		Date currentMonthFromDate = calendar.getTime();
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date currentMonthToDate = calendar.getTime();
		
		DateHelper.setToStartOfDay(currentMonthFromDate);
		DateHelper.setToEndOfDay(currentMonthToDate);
		
		DateHelper.setToStartOfDay(fromDate);
		DateHelper.setToEndOfDay(toDate);
		
		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
		HSSFRow hssfRow = null;
		HSSFCell hssfCell = null;
		
		List<String> headers = new ArrayList<String>();
		List<String> data = null;
		List<List> records = new ArrayList<List>();
		
		List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordService.getAllDeletedRouteRecords(null, false, false);
		Map<String, Route> map = new HashMap<String, Route>();
		
		for (DeletedRouteRecord deletedRouteRecord : deletedRouteRecords) {
			map.put(deletedRouteRecord.getCustomer().getMobileNumber(), deletedRouteRecord.getRoute());
		}
		
		headers.add("Sr No.");
		headers.add("Customer Name");
		headers.add("Address");
		headers.add("Phone Number");
		headers.add("Route Name");
		headers.add("Present Month");
		headers.add("Past Dues");
		headers.add("Advance");
		headers.add("Adjustment");
		headers.add("Total OutStanding");
		headers.add("Paid Amount");
		headers.add("Cashback");
		headers.add("Mode Of Payment");
		headers.add("Cheque Number");
		headers.add("Cheque Date");
		headers.add("Received Date");
		headers.add("Receipt Number");
		headers.add("Received By");
		headers.add("Date Of Data Entry");
		headers.add("Data Entry Name");
		
		records.add(headers); 
		
		for (SummarySheetRecord	 sheetRecord : sheetRecords) {
			//Start:Initialise customers variables
			adjustmentAmountOfCustomer = 0;
			paidAmountOfCustomer = 0; 
			cashback = 0;
			paymentMethodOfCustomer = "";
			chequeNumberOfCustomer = "";
			paymentDateOfCustomer ="";
			receiptNumberOfCustomer = "";
			paidForNameOfCustomer = "";
			paymentByOfCustomer = "";
			Double presentMonthAmount = 0.0, pastDues = 0.0, advance = 0.0, totalOutstanding = 0.0;
			
			//End:Initialise customers variables
			
			data = new ArrayList<String>();
			data.add(rowIndex+"");
			data.add(sheetRecord.getCustomer().getFirstName()+" " + sheetRecord.getCustomer().getLastName());
			data.add(sheetRecord.getCustomer().getBuildingAddress()); 
			data.add(sheetRecord.getCustomer().getMobileNumber());

			List<RouteRecord> routeRecord = routeRecordService.getAllRouteRecordsByCustomer(sheetRecord.getCustomer());
			if(routeRecord.isEmpty()){
				if(map.containsKey(sheetRecord.getCustomer().getMobileNumber())){
					data.add(map.get(sheetRecord.getCustomer().getMobileNumber()).getName());
				}else{
					data.add("No Route Assign");
				}
			}else{
				data.add(routeRecord.get(0).getRoute().getName());
			}
			
			pastDues = sheetRecord.getInvoice().getLastPendingDues();
			if(pastDues < 0) {
				advance = pastDues * -1;
				pastDues = 0.0;
			}
			
			presentMonthAmount = (sheetRecord.getInvoice().getTotalAmount());
			if(sheetRecord.getInvoice().getTotalAmount() == sheetRecord.getInvoice().getLastPendingDues()) {
				//If no any delivery schedule exists, no deliveries were made to the customer
				List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(sheetRecord.getCustomer().getId(), fromDate, toDate, SubscriptionType.Postpaid, null, false, false);
				if(deliverySchedules.isEmpty()) {
					presentMonthAmount = 0.0;
				}
			}
			
			data.add(presentMonthAmount + "");
			data.add(pastDues + "");
			data.add(advance + "");

			totalLastPendingDuesOfAllCustomers +=sheetRecord.getInvoice().getLastPendingDues();
			
			List<Payment> payments = paymentService.getPaymentsByCustomerIdAndDateRange(sheetRecord.getCustomer().getId(), currentMonthFromDate, currentMonthToDate, null, false, false);
			
			for (Payment payment : payments) {
				adjustmentAmountOfCustomer += payment.getAdjustmentAmount();
				paidAmountOfCustomer += payment.getAmount() - payment.getCashback();
				cashback += payment.getCashback();
				paymentMethodOfCustomer += payment.getPaymentMethod().toString()+",";
				
				if(null!=payment.getChequeNumber() && !payment.getChequeNumber().isEmpty()){
					chequeNumberOfCustomer += payment.getChequeNumber()+",";
				}else{
					if(null != payment.getTxnId()){
						chequeNumberOfCustomer += payment.getTxnId()+",";
					}else{
						chequeNumberOfCustomer += "-,";
					}
				}
				
				paymentDateOfCustomer +=sdf.format(payment.getDate())+",";
				
				if(null == payment.getReceiptNo()){
					receiptNumberOfCustomer += payment.getTxnId()+",";
				}else{
					receiptNumberOfCustomer += payment.getReceiptNo()+",";
				}
				if(null != payment.getReceivedBy()){
					paidForNameOfCustomer += payment.getReceivedBy().getFullName()+",";
				}else{
					paidForNameOfCustomer += "-,";
				}
				
				paymentByOfCustomer += payment.getCreatedBy()+",";
				
				totalPaidAmountOfAllCustomers += payment.getAmount() - payment.getCashback();
				totalCashbackOfAllCustomers += payment.getCashback();
				totalAdjustmentsOfAllCustomers += payment.getAdjustmentAmount();
			}
			
			data.add(adjustmentAmountOfCustomer+"");
			//TotalOutstanding amount
			totalOutstanding = presentMonthAmount + pastDues - advance - adjustmentAmountOfCustomer;
			data.add(totalOutstanding + "");
			data.add(paidAmountOfCustomer+"");
			data.add(cashback + "");
			
			totalOutStandingsOfAllCustomers += totalOutstanding;
			
			if(payments.isEmpty()){
				data.add("-");
				data.add("-");
				data.add("-");
				data.add("-");
				data.add("-");
				data.add("-");
				data.add("-");
				data.add("-");
			}else{
				data.add(paymentMethodOfCustomer.substring(0, paymentMethodOfCustomer.length()-1));
				data.add(chequeNumberOfCustomer.substring(0, chequeNumberOfCustomer.length()-1));
				data.add(paymentDateOfCustomer.substring(0, paymentDateOfCustomer.length()-1));
				data.add(paymentDateOfCustomer.substring(0, paymentDateOfCustomer.length()-1));
				data.add(receiptNumberOfCustomer.substring(0, receiptNumberOfCustomer.length()-1));
				data.add(paidForNameOfCustomer.substring(0, paidForNameOfCustomer.length()-1));
				data.add(paymentDateOfCustomer.substring(0, paymentDateOfCustomer.length()-1));
				data.add(paymentByOfCustomer.substring(0, paymentByOfCustomer.length()-1));
			}
			
			records.add(data);
			rowIndex++;
		}
		
		rowIndex = 0;
		HSSFCellStyle hssfCellStyleForContent = hssfWorkbook.createCellStyle();
		
		hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);

		for (int j = 0; j < records.size(); j++) {
			hssfRow = hssfSheet.createRow(rowIndex);
			List<String> l2= records.get(j);
			for(int k=0; k<l2.size(); k++){
				hssfCell = hssfRow.createCell(k);
				hssfCell.setCellStyle(hssfCellStyleForContent);
				hssfCell.setCellValue(l2.get(k));
			}
			rowIndex++;
		}

		hssfRow = hssfSheet.createRow(rowIndex+1);
		hssfCell = hssfRow.createCell(1);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue("Total ");
		
		hssfCell = hssfRow.createCell(5);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalAmountOfAllCustomers));
		
		hssfCell = hssfRow.createCell(6);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalLastPendingDuesOfAllCustomers));

		hssfCell = hssfRow.createCell(8);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalAdjustmentsOfAllCustomers));

		hssfCell = hssfRow.createCell(9);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalOutStandingsOfAllCustomers));
		
		hssfCell = hssfRow.createCell(10);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalPaidAmountOfAllCustomers));
		
		hssfCell = hssfRow.createCell(11);
		hssfCell.setCellStyle(hssfCellStyleForContent);
		hssfCell.setCellValue(String.valueOf(totalCashbackOfAllCustomers));

		for(int colNum = 0; colNum<hssfSheet.getRow(0).getLastCellNum();colNum++){
			hssfSheet.autoSizeColumn(colNum);
		}

		hssfSheet.setFitToPage(true);
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}

	/*
	 * Ajax Method
	 */ 
	 
	

	/**
	 * REST API Methods
	 */
	@RequestMapping(value="/restapi/summarySheet/update")
	@ResponseBody
	public ApiResponse updateSummarySheet(@RequestParam(required=true)String summarySheetCode,
										  @RequestParam(required=true)String[] summarySheetRecords,
										  @RequestParam(required=true)Double[] adjustMents,
										  @RequestParam(required=false)String[] paymentMethods,
										  @RequestParam(required=false)String[] chequeNumbers,
										  @RequestParam(required=false)String[] chequeDates,
										  @RequestParam(required=false)String[] receivedDates,
										  @RequestParam(required=false)String[] receiptNumbers,
										  @RequestParam(required=false)Long[] receivedBy){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			SummarySheet summarySheet = summarySheetService.getSummarySheetByCode(summarySheetCode, false, null, false, true);
			List<SummarySheetRecord> sheetRecords = new ArrayList<SummarySheetRecord>(summarySheet.getSummarySheetRecords());
			Set<SummarySheetRecord> records = new HashSet<SummarySheetRecord>();
			if(summarySheetRecords.length!=0){
				for (int i = 0; i < summarySheetRecords.length; i++) {
					for (SummarySheetRecord summarySheetRecord : sheetRecords) {
						if(summarySheetRecords[i].equals(summarySheetRecord.getCode())){
							if(null!=adjustMents){
						     double	adjustment= adjustMents[i];
								summarySheetRecord.setAdjustment(adjustment);
							}
							if(null!=paymentMethods){
								String	paymentMethod = paymentMethods[i];
								summarySheetRecord.setPaymentMethod(PaymentMethod.valueOf(paymentMethod));
							}
							if(null!=chequeNumbers){
								String chequeNumber= chequeNumbers[i];
								summarySheetRecord.setChequeNumber(chequeNumber);
							}
							if(null!=chequeDates){
								 String chequeDate= chequeDates[i];
								 summarySheetRecord.setChequeDate(DateHelper.parseDate(chequeDate));
							}
							if(null!=receivedDates){
								String receivedDate= receivedDates[i];
								summarySheetRecord.setReceivedDate(DateHelper.parseDate(receivedDate));
							}
							if(null!=receiptNumbers){
								String receiptNumber= receiptNumbers[i];
								summarySheetRecord.setReceiptNumber(receiptNumber);
							}
							if(null!=receivedBy){
								long paymentReceivedByUserCode= receivedBy[i];
								User paymentReceivedByUser = userService.getUser(paymentReceivedByUserCode, false, userService.getLoggedInUser(), false, false);
								summarySheetRecord.setReceivedBy(paymentReceivedByUser);
							}
						}
						 summarySheetRecordService.updateSummarySheetRecord(summarySheetRecord.getId(), summarySheetRecord, null, false, false);
					}
				}
			}
			summarySheet = summarySheetService.getSummarySheet(summarySheet.getId(), false, null, false, true);
			
			apiResponse.addData("summarySheet", new SummarySheetDTO(summarySheet));
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
//	@RequestMapping(value="/restapi/payment/details")
//	@ResponseBody
//	public ApiResponse getPaymentDetails(@RequestParam(required=true)String paymentCode){
//		
//		ApiResponse apiResponse = new ApiResponse(true);
//		try{
//			Payment payment =  paymentService.getPaymentByCode(paymentCode, false, null, false, false);
//			apiResponse.addData("Payment", new PaymentDTO(payment));
//		}catch(ProvilacException e){
//			e.printStackTrace();
//			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
//		}
//		return apiResponse;
//	}
	
	

	/**
	 * End REST API Methods
	 */
}
