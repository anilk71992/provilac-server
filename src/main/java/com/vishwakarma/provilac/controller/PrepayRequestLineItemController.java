package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.PrepayRequestLineItemValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.PrepayRequestLineItemService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class PrepayRequestLineItemController {

	@Resource
	private PrepayRequestLineItemValidator prepayRequestLineItemValidator;

	@Resource
	private PrepayRequestLineItemService prepayRequestLineItemService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "prepayRequestLineItem")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(prepayRequestLineItemValidator);
	}

	private Model populateModelForAdd(Model model, PrepayRequestLineItem prepayRequestLineItem) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("prepayRequestLineItem", prepayRequestLineItem);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/add")
	public String addPrepayRequestLineItem(Model model) {
		model = populateModelForAdd(model, new PrepayRequestLineItem());
		return "admin-pages/prepayRequestLineItem/add";
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/add", method = RequestMethod.POST)
	public String prepayRequestLineItemAdd(PrepayRequestLineItem prepayRequestLineItem, 
									BindingResult formBinding, 
									Model model){
		
		prepayRequestLineItemValidator.validate(prepayRequestLineItem, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, prepayRequestLineItem);
			return "admin-pages/prepayRequestLineItem/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		prepayRequestLineItemService.createPrepayRequestLineItem(prepayRequestLineItem, loggedInUser, true,false);
		String message = "prepayRequestLineItem added successfully";
		return "redirect:/admin/prepayRequestLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/list")
	public String getPrepayRequestLineItemList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<PrepayRequestLineItem> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = prepayRequestLineItemService.searchPrepayRequestLineItem(pageNumber, searchTerm, user, false, false);
		} else {
			page = prepayRequestLineItemService.getPrepayRequestLineItems(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("prepayRequestLineItems", page.getContent());

		return "/admin-pages/prepayRequestLineItem/list";
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/show/{cipher}")
	public String showSubscriptionLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		PrepayRequestLineItem prepayRequestLineItem = prepayRequestLineItemService.getPrepayRequestLineItem(id, true, loggedIUser,true,false);
		model.addAttribute("prepayRequestLineItem", prepayRequestLineItem);
		return "/admin-pages/prepayRequestLineItem/show";
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/update/{cipher}")
	public String updatePrepayRequestLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		PrepayRequestLineItem prepayRequestLineItem = prepayRequestLineItemService.getPrepayRequestLineItem(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, prepayRequestLineItem);
		model.addAttribute("id", prepayRequestLineItem.getId());
		model.addAttribute("version", prepayRequestLineItem.getVersion());
		return "/admin-pages/prepayRequestLineItem/update";
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/update", method = RequestMethod.POST)
	public String prepayRequestLineItemUpdate(PrepayRequestLineItem prepayRequestLineItem, 
										BindingResult formBinding, 
										Model model) {

		prepayRequestLineItem.setUpdateOperation(true);
		prepayRequestLineItemValidator.validate(prepayRequestLineItem, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, prepayRequestLineItem);
			return "admin-pages/prepayRequestLineItem/update";
		}
		prepayRequestLineItem.setId(Long.parseLong(model.asMap().get("id") + ""));
		prepayRequestLineItem.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		prepayRequestLineItemService.updatePrepayRequestLineItem(prepayRequestLineItem.getId(), prepayRequestLineItem, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "prepayRequestLineItem updated successfully";
		return "redirect:/admin/prepayRequestLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/prepayRequestLineItem/delete/{cipher}")
	public String deletePrepayRequestLineItem(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			prepayRequestLineItemService.deletePrepayRequestLineItems(id);
			message = "prepayRequestLineItem deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete prepayRequestLineItem";
		}
		return "redirect:/admin/prepayRequestLineItem/list?message=" + message;
	}
	
	
	/*
	 * Ajax Method
	 * 
	 */
	
}
