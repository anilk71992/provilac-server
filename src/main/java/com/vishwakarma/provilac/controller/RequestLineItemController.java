package com.vishwakarma.provilac.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.dto.SubscriptionRequestDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.SubscriptionRequest;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.mvc.validator.RequestLineItemValidator;
import com.vishwakarma.provilac.property.editor.ProductPropertyEditor;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RequestLineItemService;
import com.vishwakarma.provilac.service.SubscriptionRequestService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.AppConstants.Day;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class RequestLineItemController {

	@Resource
	private RequestLineItemValidator requestLineItemValidator;

	@Resource
	private RequestLineItemService requestLineItemService;

	@Resource
	private UserService userService;
	
	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private SubscriptionRequestService subscriptionRequestService;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@InitBinder(value = "requestLineItem")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), true);
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Product.class, new ProductPropertyEditor(productRepository));
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.setValidator(requestLineItemValidator);
	}

	private Model populateModelForAdd(Model model, RequestLineItem requestLineItem) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("requestLineItem", requestLineItem);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		model.addAttribute("products", productService.getAllProducts(null, false, false));
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		return model;
	}

	@RequestMapping(value = "/admin/requestLineItem/add")
	public String addRequestLineItem(Model model) {
		model = populateModelForAdd(model, new RequestLineItem());
		return "admin-pages/requestLineItem/add";
	}

	@RequestMapping(value = "/admin/requestLineItem/add", method = RequestMethod.POST)
	public String requestLineItemAdd(RequestLineItem requestLineItem, 
									BindingResult formBinding, 
									Model model){
		
		requestLineItemValidator.validate(requestLineItem, formBinding);
		
		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, requestLineItem);
			return "admin-pages/requestLineItem/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		requestLineItemService.createRequestLineItem(requestLineItem, loggedInUser, true,false);
		String message = "requestLineItem added successfully";
		return "redirect:/admin/requestLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/requestLineItem/list")
	public String getRequestLineItemList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<RequestLineItem> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = requestLineItemService.searchRequestLineItem(pageNumber, searchTerm, user, false, false);
		} else {
			page = requestLineItemService.getRequestLineItems(pageNumber, user,true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("requestLineItems", page.getContent());

		return "/admin-pages/requestLineItem/list";
	}

	@RequestMapping(value = "/admin/requestLineItem/show/{cipher}")
	public String showSubscriptionLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		RequestLineItem requestLineItem = requestLineItemService.getRequestLineItem(id, true, loggedIUser,true,false);
		model.addAttribute("requestLineItem", requestLineItem);
		return "/admin-pages/requestLineItem/show";
	}

	@RequestMapping(value = "/admin/requestLineItem/update/{cipher}")
	public String updateRequestLineItem(@PathVariable String cipher, Model model) {

		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		RequestLineItem requestLineItem = requestLineItemService.getRequestLineItem(id, true, loggedInUser,true,false);
		model = populateModelForAdd(model, requestLineItem);
		model.addAttribute("id", requestLineItem.getId());
		model.addAttribute("version", requestLineItem.getVersion());
		return "/admin-pages/requestLineItem/update";
	}

	@RequestMapping(value = "/admin/requestLineItem/update", method = RequestMethod.POST)
	public String requestLineItemUpdate(RequestLineItem requestLineItem, 
										BindingResult formBinding, 
										Model model) {

		requestLineItem.setUpdateOperation(true);
		requestLineItemValidator.validate(requestLineItem, formBinding);

		if (formBinding.hasErrors()) {
			model = populateModelForAdd(model, requestLineItem);
			return "admin-pages/requestLineItem/update";
		}
		requestLineItem.setId(Long.parseLong(model.asMap().get("id") + ""));
		requestLineItem.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		requestLineItemService.updateRequestLineItem(requestLineItem.getId(), requestLineItem, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		String message = "requestLineItem updated successfully";
		return "redirect:/admin/requestLineItem/list?message=" + message;
	}

	@RequestMapping(value = "/admin/requestLineItem/delete/{cipher}")
	public String deleteRequestLineItem(@PathVariable String cipher) {

		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			requestLineItemService.deleteRequestLineItems(id);
			message = "requestLineItem deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete requestLineItem";
		}
		return "redirect:/admin/requestLineItem/list?message=" + message;
	}
	
	
	/*
	 * Ajax Method
	 * 
	 */
	
	@RequestMapping(value="/requestLineItem/getAllProducts")
	@ResponseBody
	public ApiResponse getAllProductsRequestLineItem(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<Product> products = productService.getAllProducts(loggedInUser, false, false);
			List<ProductDTO> dtos = new ArrayList<ProductDTO>();
			for (Product product : products) {
				dtos.add(new ProductDTO(product));
			}
			apiResponse.addData("products", dtos);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value = "/restapi/requestLineItem/addNewrequestLineItem",method =RequestMethod.POST)
	@ResponseBody
	public ApiResponse createNewrequestLineItem(@RequestPart(required = true) String[] productCodes,
													 @RequestPart(required = true) String[] quantity,
													 @RequestPart(required = true) String[] types,
													 @RequestPart(required = true) String startDate,
													 @RequestPart(required = true) String customerCode) {

		ApiResponse apiResponse = new ApiResponse(true);

		try {
			User loggedInUser  =userService.getLoggedInUser();
			SubscriptionRequest existingSubscriptionRequest = subscriptionRequestService.getSubscriptionRequestByCustomerAndIsCurrent(customerCode, true, loggedInUser, true, true);

			if (existingSubscriptionRequest != null) {

				Set<RequestLineItem> newlyAddedItems = new HashSet<RequestLineItem>();
				
				//TODO: Check for same products in newly added items as well as existing items
			
					for (int i = 0; i < productCodes.length; i++) {
						String productCode = productCodes[i];
						Product product = productService.getProductByCode(productCode, false, null, false, false);
						String type = types[i];
						if (SubscriptionType.valueOf(type) == SubscriptionType.Daily) {
							int quantity1= Integer.parseInt(quantity[i]);
							if(quantity1 > 0) {
								RequestLineItem lineItem = new RequestLineItem();
								lineItem.setQuantity(quantity1);
								lineItem.setProduct(product);
								lineItem.setType(SubscriptionType.Daily);
								lineItem.setSubscriptionRequest(existingSubscriptionRequest);
								
								lineItem = requestLineItemService.createRequestLineItem(lineItem, loggedInUser, false, true);
								newlyAddedItems.add(lineItem);
							}
					
				} else if (SubscriptionType.valueOf(type) == SubscriptionType.Weekly) {

						String[] arr = quantity[i].split("-");
						for (int j = 0; j < arr.length; j++) {
							int quantity2 = Integer.parseInt(arr[j]);
							if(quantity2 > 0) {
								RequestLineItem lineItem = new RequestLineItem();
								lineItem.setProduct(product);
								lineItem.setQuantity(quantity2);
								lineItem.setDay(Day.values()[j]);
								lineItem.setType(SubscriptionType.Weekly);
								lineItem.setSubscriptionRequest(existingSubscriptionRequest);
							
								lineItem = requestLineItemService.createRequestLineItem(lineItem, loggedInUser, false, true);
								newlyAddedItems.add(lineItem);
							}
						}
					
				} else {
						RequestLineItem lineItem = new RequestLineItem();	
						lineItem.setProduct(product);
						lineItem.setCustomPattern(quantity[i]);
						lineItem.setType(SubscriptionType.Custom);
						lineItem.setSubscriptionRequest(existingSubscriptionRequest);
						
						lineItem = requestLineItemService.createRequestLineItem(lineItem, loggedInUser, false, true);		
						newlyAddedItems.add(lineItem);
					}
				}
					existingSubscriptionRequest = subscriptionRequestService.getSubscriptionRequest(existingSubscriptionRequest.getId(), true, null, false, true);
				apiResponse.addData("subscriptionRequestDTO", new SubscriptionRequestDTO(existingSubscriptionRequest));
				//asyncJobs.generateScheduleForLineItems(existingSubscriptionRequest, newlyAddedItems, DateHelper.parseDate(startDate), loggedInUser);
			} else {
				apiResponse.setError("No current subscription found for this user. Please add one before proceeding.", "400");
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}

		return apiResponse;
	}
}
