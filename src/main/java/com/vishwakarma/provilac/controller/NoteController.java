package com.vishwakarma.provilac.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.NoteDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Note;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.service.NoteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.utils.EncryptionUtil;


@Controller
@SessionAttributes(value = { "id", "version", "retUrl", "retPage" })
public class NoteController {


	@Resource
	private UserService userService;
	
	@Resource
	private NoteService noteService;
	
	@InitBinder(value = "note")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
	}

	private Model populateModelForAdd(Model model, Note note) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("note", "");
		return model;
	}

	@RequestMapping(value = "/admin/note/add")
	public String addCity(Model model) {
		model = populateModelForAdd(model, new Note());
		return "admin-pages/note/add";
	}

	//USED_FROM_WEB
	@RequestMapping(value = "/admin/note/add", method = RequestMethod.POST)
	public String noteAdd(@RequestParam(value="callDetails") String callDetails,
			@RequestParam(value="customerCode")String customerCode){
		
		//Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		Note note=new Note();
		User customer =userService.getUserByCode(customerCode, false, userService.getLoggedInUser(), false, false);
		if(null !=customer){
			note.setCustomer(customer);
			note.setDate(new Date());
			note.setCallDetails(callDetails);
		}
		noteService.createNote(note, userService.getLoggedInUser(), false, false);
		note.setDate(new Date());
		
		String message = "Note added successfully";
		return "redirect:/admin/user/getCustomerlist?message="+message;
	}

	@RequestMapping(value = "/admin/note/list")
	public String getNoteList(Model model, 
										@RequestParam(defaultValue = "1") Integer pageNumber,
										@RequestParam(required = false) String message, 
										@RequestParam(required = false) String searchTerm) {
		
		model.addAttribute("message", message);
		Page<Note> page = null;
		User user = userService.getLoggedInUser();
		if (StringUtils.isNotBlank(searchTerm)) {
			page = noteService.searchNote(pageNumber, searchTerm, user,true,false);
		} else {
			page = noteService.getAllNotes(user,pageNumber, true,false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("notes", page.getContent());

		return "/admin-pages/note/list";
	}

	@RequestMapping(value = "/admin/note/show/{cipher}")
	public String showCity(@PathVariable String cipher, Model model) {
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedInUser = userService.getLoggedInUser();
		Note note=noteService.getNoteById(id, loggedInUser, false, true);
		model.addAttribute("note", note);
		return "/admin-pages/note/show";
	}

	@RequestMapping(value = "/admin/note/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		String message = "";
		try {
			noteService.deleteNote(id);
			message = "Note deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete note";
		}
		return "redirect:/admin/note/list?message=" + message;
	}

	//USED_FROM_WEB
	@RequestMapping(value = "/restapi/note/getNotes")
	@ResponseBody
	public ApiResponse getNotesByCustomerId(@RequestParam(value="id",required=false) Long id){
		
		ApiResponse apiResponse=new ApiResponse(true);
		try{
			
			User customer =userService.getUser(id, false, null, false, false);
			List<Note> notes=noteService.getAllNotesByCustomer(customer.getCode(), userService.getLoggedInUser(), false, false); 
		
			List<NoteDTO> noteDTOs=new ArrayList<NoteDTO>();
			for(Note note:notes){
				noteDTOs.add(new NoteDTO(note));
			}
			apiResponse.addData("notes", noteDTOs);
		}catch(ProvilacException e){
			apiResponse.setSuccess(false);
		}
		
		return apiResponse;
	}
}
