package com.vishwakarma.provilac.controller;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.ReasonToStop;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.service.CommonService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.PickListItemService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionRequestService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

@Controller
public class DashboardController {

	@Resource 
	private CommonService commonService;
	
	@Autowired 
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private PickListItemService pickListItemService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private UserRouteService userRouteService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private SubscriptionRequestService subscriptionRequestService;
	
	@RequestMapping("/home")
	public String dashboard(final Model model, 
							HttpSession httpSession,
							@RequestParam(required = false) String message,
							HttpServletResponse response) {

		String orgName = systemPropertyHelper.getSystemProperty(SystemProperty.ORG_NAME, null);
		if (null!=orgName) {
			model.addAttribute("orgName", orgName);
			httpSession.setAttribute("orgName", orgName);
		}
		User user = userService.getLoggedInUser();
		if(user.hasRole(Role.ROLE_CUSTOMER)) {
			return "redirect:/index";
		} else {
			if(StringUtils.isBlank(message)){
				return "redirect:/admin/home";
			}else{
				return "redirect:/admin/home?message"+ message; 
			}
		}
	}
	
	@RequestMapping("/admin/home")
	public String adminHome(Model model) {

		User loggedInUser = userService.getLoggedInUser();
		List<User> customers  = userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false);
		List<User>newCustomers= new ArrayList<User>();
		List<User> activeCustomers = new ArrayList<User>();
		List<User> onHoldCustomers = new ArrayList<User>();
		List<User> inActiveCustomers = new ArrayList<User>();

		Map<String,Integer> customersByStatus = new HashMap<String, Integer>();
		for (User customer : customers) {
			if(customer.getStatus().equals(Status.NEW)){
				newCustomers.add(customer);
			}else if(customer.getStatus().equals(Status.ACTIVE)){
				activeCustomers.add(customer);
			}else if(customer.getStatus().equals(Status.HOLD)){
				onHoldCustomers.add(customer);
			}else{
				inActiveCustomers.add(customer);
			}
		}

		customersByStatus.put(Status.NEW.toString(), newCustomers.size());
		customersByStatus.put(Status.ACTIVE.toString(), activeCustomers.size());
		customersByStatus.put(Status.HOLD.toString(), onHoldCustomers.size());
		customersByStatus.put(Status.INACTIVE.toString(), inActiveCustomers.size());

		List<String> status = new ArrayList<String>();
		List<Integer>customerCount = new ArrayList<Integer>();
		for (Map.Entry<String, Integer> entry : customersByStatus.entrySet()) {
			status.add(entry.getKey());
			customerCount.add(entry.getValue());
		}

		JSONArray  report = new JSONArray();
		report.add(status);
		report.add(customerCount);
		report.add(newCustomers);

		model.addAttribute("status", status);
		model.addAttribute("customerCount", customerCount);
		model.addAttribute("newCustomerCount", newCustomers.size());
		model.addAttribute("newSubscriptionRequestCount", subscriptionRequestService.getCountByRequestType(false));
		model.addAttribute("changeSubscriptionRequestCount", subscriptionRequestService.getCountByRequestType(true));
		return "/admin-pages/home";
	}

	@RequestMapping(value="/home/getDeliveredProductsForRange")
	@ResponseBody
	public ApiResponse GetProductReportInDateRange(@RequestParam(required=true)String startDate,
												   @RequestParam(required=true)String endDate){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			Date fromDate = null;
			Date toDate = null;
			Calendar calendar = Calendar.getInstance();
			if(null!=startDate && null!=endDate){
				
				calendar.setTime(DateHelper.parseDate(startDate));
				 fromDate = calendar.getTime();
				calendar.setTime(DateHelper.parseDate(endDate));
				 toDate = calendar.getTime();
			}else{
				fromDate = calendar.getTime();
				toDate = calendar.getTime();
			}
			
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByStatusAndDateRange(fromDate, toDate, null, false, true);
			Map<Date,List<DeliverySchedule>> dateWiseSchedules = new HashMap<Date, List<DeliverySchedule>>();
			
			Date scheduleDate;
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				List<DeliverySchedule>schedules = null;
				scheduleDate = deliverySchedule.getDate();
				DateHelper.setToStartOfDay(scheduleDate);
				if(dateWiseSchedules.containsKey(scheduleDate)){
					schedules = dateWiseSchedules.get(deliverySchedule.getDate());
				}else{
					schedules = new ArrayList<DeliverySchedule>();
				}
				schedules.add(deliverySchedule);
				dateWiseSchedules.put(scheduleDate, schedules);
			}
			Map<Date,Map<String,Integer>> map1 = new HashMap<Date, Map<String,Integer>>();
			for (Map.Entry<Date, List<DeliverySchedule>> entry : dateWiseSchedules.entrySet()) {
				List<DeliverySchedule> schedules = entry.getValue();
				Map<String,Integer> productQuantity = new HashMap<String,Integer>();
				for (DeliverySchedule deliverySchedule : schedules) {
					for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {

						if(productQuantity.containsKey(deliveryLineItem.getProduct().getName())){
							if(productQuantity.isEmpty()){
								productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
							}else{
								productQuantity.put(deliveryLineItem.getProduct().getName(), (productQuantity.get(deliveryLineItem.getProduct().getName()) != null?productQuantity.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
							}
						}else{
							productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
						}
					}
					
				}
				map1.put(entry.getKey(), productQuantity);
			}
			
			Map<String,List<Integer>> productWiseQuantity = new HashMap<String, List<Integer>>();
			for (Map.Entry<Date, Map<String,Integer>> entry : map1.entrySet()) {
				
				Map<String,Integer> map = entry.getValue();
				 for (Map.Entry<String,Integer> entry1 : map.entrySet()) {
					 if(productWiseQuantity.containsKey(entry1.getKey())){
							if(productWiseQuantity.isEmpty()){
								List<Integer> quantity = new ArrayList<Integer>();
								quantity.add(entry1.getValue());
								productWiseQuantity.put(entry1.getKey(),quantity);
							}else{
								List<Integer> quantity = productWiseQuantity.get(entry1.getKey());
								if(null!=quantity){
									quantity.add(entry1.getValue());
								}else{
									quantity.add(0);
								}
								productWiseQuantity.put(entry1.getKey(), quantity);
							}
						}else{
							List<Integer> quantity = new ArrayList<Integer>();
							quantity.add(entry1.getValue());
							productWiseQuantity.put(entry1.getKey(),quantity);
						}
				}
			}
			
			JSONArray productJson = new JSONArray();
			JSONArray tempArray = null;
			int totalProductsCount=0;
			JSONArray productJson1 = new JSONArray();
			for ( Map.Entry<String,List<Integer>> entry : productWiseQuantity.entrySet()) {
				tempArray = new JSONArray();
				tempArray.add(entry.getKey());
				
				tempArray.add(entry.getValue());
				productJson.add(tempArray);
			}
			
			fromDate = calendar.getTime();
			toDate = calendar.getTime();
			List<DeliverySchedule> deliverySchedules1 = deliveryScheduleService.getDeliverySchedulesByStatusAndDateRange(fromDate, toDate, null, false, true);
			Map<String,Integer> productQuantity = new HashMap<String,Integer>();
			for (DeliverySchedule deliverySchedule : deliverySchedules1) {
				for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {

					if(productQuantity.containsKey(deliveryLineItem.getProduct().getName())){
						if(productQuantity.isEmpty()){
							productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
						}else{
							productQuantity.put(deliveryLineItem.getProduct().getName(), (productQuantity.get(deliveryLineItem.getProduct().getName()) != null?productQuantity.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
						}
					}else{
						productQuantity.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
					}
				}
				
			}
			
			for (Map.Entry<String, Integer> entry : productQuantity.entrySet()) {
				tempArray = new JSONArray();
				
				tempArray.add(entry.getKey()+ "- "+ entry.getValue());
				tempArray.add(entry.getValue());
				productJson1.add(tempArray);
			}
			
			JSONArray report = new JSONArray();
			report.add(productJson);
			report.add(productJson1);
			//productJson.add(totalProductsCount);
			apiResponse.addData("products", report);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value="/home/getCustomersByAssingedToUser")
	@ResponseBody
	public ApiResponse GetProductReportInDateRange(@RequestParam(required=true)String userId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User user = userService.getUser(Long.parseLong(userId), false, null, false, false);
			List<User> customers = userService.getUsersByAssignedToUser(user.getCode());
			List<User>newCustomers= new ArrayList<User>();
			List<User> activeCustomers = new ArrayList<User>();
			List<User> onHoldCustomers = new ArrayList<User>();
			List<User> inActiveCustomers = new ArrayList<User>();
			Map<String,Integer> customersByStatus = new HashMap<String, Integer>();
			for (User customer : customers) {
				if(customer.getStatus().equals(Status.NEW)){
					newCustomers.add(customer);
				}else if(customer.getStatus().equals(Status.ACTIVE)){
					activeCustomers.add(customer);
				}else if(customer.getStatus().equals(Status.HOLD)){
					onHoldCustomers.add(customer);
				}else{
					inActiveCustomers.add(customer);
				}
			}
			customersByStatus.put(Status.NEW.toString(), newCustomers.size());
			customersByStatus.put(Status.ACTIVE.toString(), activeCustomers.size());
			customersByStatus.put(Status.HOLD.toString(), onHoldCustomers.size());
			customersByStatus.put(Status.INACTIVE.toString(), inActiveCustomers.size());

			List<String> status = new ArrayList<String>();
			List<Integer>customerCount = new ArrayList<Integer>();
			for (Map.Entry<String, Integer> entry : customersByStatus.entrySet()) {
				status.add(entry.getKey());
				customerCount.add(entry.getValue());
			}
			JSONArray status1 = new JSONArray();
			status1.add(status);
			status1.add(customerCount);
			apiResponse.addData("customers",status1);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value="/home/getAllProductsForRange")
	@ResponseBody
	public ApiResponse GetTotalProductReportInDateRange(@RequestParam(required=true)String fromDate,
												   @RequestParam(required=true)String toDate){
		
		ApiResponse apiResponse = new ApiResponse(true);

		try{
			Calendar calendar = Calendar.getInstance();
			   Date today = new Date();
			   calendar.setTime(DateHelper.parseDate(fromDate));  
			   calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
			   Date startDate= calendar.getTime();
			   calendar.setTime(DateHelper.parseDate(toDate));
			   calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
			   Date endDate = calendar.getTime();
			JSONArray productReport = getTotalProductCount(startDate,endDate);
			apiResponse.addData("allProducts", productReport);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	
	@RequestMapping(value="/home/getReportForRange")
	@ResponseBody
	public ApiResponse GetReportInDateRange(@RequestParam(required=true)String fromDate,
												   @RequestParam(required=true)String toDate){
		
		ApiResponse apiResponse = new ApiResponse(true);

		try{
			Date startDate = DateHelper.parseDate(fromDate);
			Date endDate = DateHelper.parseDate(toDate);
			Calendar calendar = Calendar.getInstance();
			Date today = new Date();
			calendar.setTime(today);  
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date startDate1= calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(calendar.DAY_OF_MONTH));
			Date endDate1 = calendar.getTime();


			JSONArray reportValueJson = new JSONArray();
			JSONArray reportNameJson = new JSONArray();

			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getUsersByStatusAndCreatedDate(Status.NEW, startDate, endDate, loggedInUser, false, false);
			reportValueJson.add(customers.size());
			reportNameJson.add("New Customers");

			List<UserSubscription> userSubscriptions = userSubscriptionService.getCurrentUserSubscriptionByStartDate(startDate, endDate, loggedInUser, false, false);
			reportValueJson.add(userSubscriptions.size());
			reportNameJson.add("New Subscriptions");

			List<User>onHoldCustomers = userService.getUsersByStatus(Status.HOLD);
			reportValueJson.add(onHoldCustomers.size());
			reportNameJson.add("Hold Customers");

			List<User> stoppedCustomers = userService.getUsersByStatus(Status.INACTIVE);
			reportValueJson.add(stoppedCustomers.size());
			reportNameJson.add("Stopped Customers");

			List<DeliverySchedule>freeTrialDeliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Free_Trial,startDate,endDate, null, false, false); 
			reportValueJson.add(freeTrialDeliverySchedules.size());
			reportNameJson.add("Free Trial DeliverySchedules");

			List<DeliverySchedule>paidTrialDeliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Paid_Trial,startDate,endDate, null, false, false); 
			reportValueJson.add(paidTrialDeliverySchedules.size());
			reportNameJson.add("Paid Trial DeliverySchedules");

			JSONArray reportJson = new JSONArray();
			reportJson.add(reportNameJson);
			reportJson.add(reportValueJson);

			apiResponse.addData("reports", reportJson);

		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/home/getCollectionBoyReport")
	@ResponseBody
	public ApiResponse GetCollectionBoyReport(@RequestParam(required=true)String userId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<UserRoute>userRoutes = userRouteService.getUserRoutesForColeectionBoy(Long.parseLong(userId), null, false, false);
			JSONArray routeNameJson = new JSONArray();
			JSONArray pendingDuesJson = new JSONArray();
			for (UserRoute userRoute : userRoutes) {
				Route route = routeService.getRoute(userRoute.getRoute().getId(), false, null, false, true);
				List<RouteRecord> routeRecords = new ArrayList<RouteRecord>(route.getRouteRecords());
				Double pendingDues = 0.0;
				User loggedInUser = userService.getLoggedInUser();
				for (RouteRecord routeRecord : routeRecords) {
					User user = userService.getUser(routeRecord.getCustomer().getId(), false, loggedInUser, false, false);
					pendingDues = pendingDues+user.getLastPendingDues();
				}
				routeNameJson.add(route.getName());
				pendingDuesJson.add(pendingDues);
			}
			JSONArray collectionBoyReport= new JSONArray();
			collectionBoyReport.add(routeNameJson);
			collectionBoyReport.add(pendingDuesJson);
			apiResponse.addData("collectionBoyReport", collectionBoyReport);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/home/getCollectionReport")
	@ResponseBody
	public ApiResponse GetCollectionReport(@RequestParam(required=true)String userId){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Payment>payments = paymentService.getAllPaymentsByReceivedBy(Long.parseLong(userId), null, false, false);
			JSONArray amountJson = new JSONArray();
			JSONArray paymenTypeJson = new JSONArray();
			JSONArray collectionReport = new JSONArray();
			double segregateCash = 0.0;
			double chequeAmount = 0.0;
			for (Payment payment : payments) {
				if(payment.getPaymentMethod().equals(PaymentMethod.Cash)){
					segregateCash=segregateCash+payment.getAmount();
				}else if(payment.getPaymentMethod().equals(PaymentMethod.Cheque)){
					chequeAmount=chequeAmount+payment.getAmount();
				}
			}
			amountJson.add(segregateCash);
			amountJson.add(chequeAmount);
			paymenTypeJson.add("Cash");
			paymenTypeJson.add("cheque");
			collectionReport.add(amountJson);
			collectionReport.add(paymenTypeJson);
			apiResponse.addData("collectionReport", collectionReport);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	@RequestMapping(value="/home/getDelivererySchedulesForPastEightDays")
	@ResponseBody
	public ApiResponse getDeliveryReportOfPastDays(@RequestParam(required = false)String fromDate,
			  @RequestParam(required = false)String toDate){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			Date startDate;
			Date endDate;
			Calendar calendar = Calendar.getInstance();
			if(null == fromDate || null == toDate){
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				startDate = calendar.getTime();
				DateHelper.setToStartOfDay(startDate);
				calendar.setTime(startDate);
				calendar.add(Calendar.DAY_OF_YEAR, -6);
				endDate = calendar.getTime();
				DateHelper.setToStartOfDay(endDate);
			}else{
				startDate = DateHelper.parseDate(fromDate);
				endDate = DateHelper.parseDate(toDate);
			}
			
			JSONArray deliveredScheduleJson = new JSONArray();
			JSONArray notDeliveredScheduleJson = new JSONArray();
			JSONArray dateJson =  new JSONArray();
	 		
			User loggedInUser = userService.getLoggedInUser();
			Integer count=0;
			for (;endDate.before(startDate)||endDate.equals(startDate);){
				List<DeliverySchedule>deliverySchedules = deliveryScheduleService.getDeliverySchedulesByDateAndStatus(endDate, DeliveryStatus.Delivered, loggedInUser, false, true);
				List<DeliverySchedule>schedules = deliveryScheduleService.getDeliverySchedulesByDateAndStatus(endDate, DeliveryStatus.NotDelivered, loggedInUser, false, true);
				calendar.setTime(endDate);
				String dayName = new DateFormatSymbols().getWeekdays()[calendar.get(Calendar.DAY_OF_WEEK)];
				String formattedaDate = dayName  + "- " +DateHelper.getFormattedDate(endDate) ;
				dateJson.add(formattedaDate);
				count=0;
				for(DeliverySchedule deliverySchedule:deliverySchedules){
					for(DeliveryLineItem deliveryLineItem:deliverySchedule.getDeliveryLineItems()){
						count+=deliveryLineItem.getQuantity();
					}
				}
				deliveredScheduleJson.add(count);
				count=0;
				for(DeliverySchedule deliverySchedule:schedules){
					for(DeliveryLineItem deliveryLineItem:deliverySchedule.getDeliveryLineItems()){
						count+=deliveryLineItem.getQuantity();
					}
				}
//				deliveredScheduleJson.add(deliverySchedules.size());
				notDeliveredScheduleJson.add(count);
				endDate = DateHelper.addDays(endDate, 1);
			}
	 		
			JSONArray reports = new JSONArray();
			reports.add(deliveredScheduleJson);
			reports.add(notDeliveredScheduleJson);
			reports.add(dateJson);
			
			apiResponse.addData("reports", reports);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/home/getReasonWiseCustomers")
	@ResponseBody
	public ApiResponse getStoppedUserReport(){	
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> stoppedUsers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.INACTIVE, loggedInUser, false, false);
			Map<String, Integer> reasonWiseUsers = new HashMap<String, Integer>();
			List<User>shiftedCity = new ArrayList<User>();
			List<User>purchesFromComparator = new ArrayList<User>();
			List<User>milkQualityOrTaste = new ArrayList<User>();
			List<User>bottleQuality = new ArrayList<User>();
			List<User>expensive = new ArrayList<User>();
			List<User>variantsNotAvailable = new ArrayList<User>();
			List<User>other = new ArrayList<User>();
			for (User user : stoppedUsers) {
				if(null!=user.getReasonToStop()){
					if(user.getReasonToStop().equals(ReasonToStop.Bottle_Quality)){
						bottleQuality.add(user);
					}else if(user.getReasonToStop().equals(ReasonToStop.Expensive)){
						expensive.add(user);
					}else if(user.getReasonToStop().equals(ReasonToStop.Milk_Quality_Or_Taste)){
						milkQualityOrTaste.add(user);
					}else if(user.getReasonToStop().equals(ReasonToStop.Purchase_From_Competitor)){
						purchesFromComparator.add(user);
					}else if(user.getReasonToStop().equals(ReasonToStop.Shifted_City)){
						shiftedCity.add(user);
					}else if(user.getReasonToStop().equals(ReasonToStop.Variants_Not_Available)){
						variantsNotAvailable.add(user);
					}else{
						other.add(user);
					}
				}
			}
			
			reasonWiseUsers.put(ReasonToStop.Shifted_City.toString(), shiftedCity.size());
			reasonWiseUsers.put(ReasonToStop.Bottle_Quality.toString(), bottleQuality.size());
			reasonWiseUsers.put(ReasonToStop.Expensive.toString(), expensive.size());
			reasonWiseUsers.put(ReasonToStop.Milk_Quality_Or_Taste.toString(), milkQualityOrTaste.size());
			reasonWiseUsers.put(ReasonToStop.Purchase_From_Competitor.toString(), purchesFromComparator.size());
			reasonWiseUsers.put(ReasonToStop.Variants_Not_Available.toString(), variantsNotAvailable.size());
			reasonWiseUsers.put(ReasonToStop.Others.toString(), other.size());
			
			
			List<String> reasons = new ArrayList<String>();
			List<Integer> reasonCount = new ArrayList<Integer>();
			for (Map.Entry<String, Integer> entry : reasonWiseUsers.entrySet()) {
				reasons.add(entry.getKey());
				reasonCount.add(entry.getValue());
			}
		     JSONArray reasons1 = new JSONArray();
		     reasons1.add(reasons);
		     reasons1.add(reasonCount);
		apiResponse.addData("reports", reasons1);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	

	@RequestMapping(value="/home/getCashFlowReport")
	@ResponseBody
	public ApiResponse getCashFlowReport(){	
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			Calendar calendar = Calendar.getInstance();
			Date currentDate = new Date();
			calendar.setTime(currentDate);  
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			Date invoiceFrom= calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(calendar.DAY_OF_MONTH));
			Date invoiceTo = calendar.getTime();

			Map<String,Double>cashFlowChart = new HashMap<String, Double>();
			List<Invoice>invoices = invoiceService.getInvoiceByDateRange(invoiceFrom, invoiceTo, null, false, false);

			double currentMonthPendingDue =0.0;
			for (Invoice invoice : invoices) {
				currentMonthPendingDue =currentMonthPendingDue+invoice.getLastPendingDues();
			}
			
			List<Payment> payments = paymentService.getAllPayments(loggedInUser, false, false);
			double totalCollection = 0.0;
			double totalAdjustments=0.0;
			
			for (Payment payment : payments) {
				totalCollection = totalCollection+payment.getAmount();
				totalAdjustments= totalAdjustments+payment.getAdjustmentAmount();
			}

			List<User> customers  = userService.getUsersForRole(Role.ROLE_CUSTOMER, loggedInUser, false, false);
			double pendingDues = 0.0;
			
			for (User customer : customers) {
				pendingDues = (pendingDues+customer.getLastPendingDues());
			}

			cashFlowChart.put("Pending Dues", pendingDues);
			cashFlowChart.put("Total Collection", totalCollection);
			cashFlowChart.put("Total Adjustments", totalAdjustments);
			cashFlowChart.put("Current Month Dues", Math.abs(currentMonthPendingDue));

			JSONArray cashFlowValue = new JSONArray();
			JSONArray cashFlowParameter = new JSONArray();
			
			for (Map.Entry<String, Double> entry : cashFlowChart.entrySet()) {
				cashFlowParameter.add(entry.getKey());
				cashFlowValue.add(Math.abs(entry.getValue()));
			}

			JSONArray report = new JSONArray();
			report.add(cashFlowValue);
			report.add(cashFlowParameter);
			apiResponse.addData("report", report);

		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());

		}
		return apiResponse;
	}
	

	@RequestMapping(value="/home/getSalesPerformanceOfLoggedInUser")
	@ResponseBody
	public ApiResponse getSalesPerformanceOfLoggedInUser(){	
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			JSONArray loggedInUserNewCustomersAndSubscriptions= new JSONArray();
			List<User>cusomersofLoggedInUser = userService.getUsersAssignedToUserAndStatus(loggedInUser.getCode(), Status.NEW);
			loggedInUserNewCustomersAndSubscriptions.add(cusomersofLoggedInUser.size());

			JSONArray loggedInUserSubscriptions= new JSONArray();
			List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionByIsCurrentAndAssignedToUser(loggedInUser.getCode(), loggedInUser, false, false);
			loggedInUserNewCustomersAndSubscriptions.add(subscriptions.size());
			JSONArray reportName = new JSONArray();
			reportName.add(" New Customers");
			reportName.add("New Subsriptions");

			JSONArray report = new JSONArray();
			report.add(reportName);
			report.add(loggedInUserNewCustomersAndSubscriptions);
			apiResponse.addData("report", report);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/home/getCustomerAndSubscriptionReport")
	@ResponseBody
	public ApiResponse getCustomerAndSubscriptionReport(){
	 ApiResponse apiResponse = new ApiResponse(true);
		try{
			Calendar calendar = Calendar.getInstance();
			   Date today = new Date();
			   calendar.setTime(today);  
			   calendar.set(Calendar.DAY_OF_MONTH, 1);
			   Date startDate1= calendar.getTime();
			   calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(calendar.DAY_OF_MONTH));
			   Date endDate1 = calendar.getTime();

			   JSONArray reportValueJson = new JSONArray();
			   JSONArray reportNameJson = new JSONArray();
			   User loggedInUser = userService.getLoggedInUser();
			   List<User> customers1 = userService.getUsersByStatusAndCreatedDate(Status.NEW, startDate1, endDate1, loggedInUser, false, false);
			   reportValueJson.add(customers1.size());
			   reportNameJson.add("New Customers");

			   List<UserSubscription> userSubscriptions = userSubscriptionService.getCurrentUserSubscriptionByStartDate(startDate1, endDate1, loggedInUser, false, false);
			   reportValueJson.add(userSubscriptions.size());
			   reportNameJson.add("New Subscriptions");

			   List<User>onHoldCustomers1 = userService.getUsersByStatus(Status.HOLD);
			   reportValueJson.add(onHoldCustomers1.size());
			   reportNameJson.add("Hold Customers");

			   List<User> stoppedCustomers = userService.getUsersByStatus(Status.INACTIVE);
			   reportValueJson.add(stoppedCustomers.size());
			   reportNameJson.add("Stopped Customers");

			   List<DeliverySchedule>freeTrialDeliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Free_Trial,startDate1,endDate1, null, false, false); 
			   reportValueJson.add(freeTrialDeliverySchedules.size());
			   reportNameJson.add("Free Trial DeliverySchedules");

			   List<DeliverySchedule>paidTrialDeliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Paid_Trial,startDate1,endDate1, null, false, false); 
			   reportValueJson.add(paidTrialDeliverySchedules.size());
			   reportNameJson.add("Paid Trial DeliverySchedules");
			   JSONArray report = new JSONArray();
			   report.add(reportNameJson);
			   report.add(reportValueJson);
			   apiResponse.addData("report", report);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
	     
	   return apiResponse;
   }
   
	@RequestMapping(value="/home/getTotalProducts")
	@ResponseBody
	public ApiResponse getTotalProducts(){

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			Calendar calendar = Calendar.getInstance();
			Date today = new Date();
			calendar.setTime(today);  
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date startDate= calendar.getTime();
			calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(calendar.DAY_OF_MONTH));
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date endDate = calendar.getTime();

		   JSONArray productReport = getTotalProductCount(startDate,endDate);
    	   	apiResponse.addData("allProducts", productReport);			   
		} catch (ProvilacException e) {
		}
	   
	   return apiResponse;
   }
   private JSONArray getTotalProductCount (Date startDate,Date endDate){
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByDateRange(startDate, endDate, null, false, true);
		List<DeliverySchedule>startDateDeliverySchedules = new ArrayList<DeliverySchedule>();
		List<DeliverySchedule>endDateDeliverySchedules = new ArrayList<DeliverySchedule>();
		
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(deliverySchedule.getDate());
			Date date = calendar.getTime();
			if(date.equals(startDate)){
				startDateDeliverySchedules.add(deliverySchedule);
			}else{
				endDateDeliverySchedules.add(deliverySchedule);
			}
		}
		
		Map<String, Integer> endDayProducts = new HashMap<String, Integer>();
		Map<String,Integer> productsOfStartDate = new HashMap<String, Integer>();
		
		for (DeliverySchedule deliverySchedule : endDateDeliverySchedules) {
			
			for(DeliveryLineItem deliveryLineItem:deliverySchedule.getDeliveryLineItems()){

				if(endDayProducts.containsKey(deliveryLineItem.getProduct().getName())){
					if(endDayProducts.isEmpty()){
						endDayProducts.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
					}else{
						endDayProducts.put(deliveryLineItem.getProduct().getName(), (endDayProducts.get(deliveryLineItem.getProduct().getName()) != null?endDayProducts.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
					}
				}else{
					endDayProducts.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
				}
			}
		}
		for (DeliverySchedule deliverySchedule : startDateDeliverySchedules) {

			for(DeliveryLineItem deliveryLineItem:deliverySchedule.getDeliveryLineItems()){

				if(productsOfStartDate.containsKey(deliveryLineItem.getProduct().getName())){
					if(productsOfStartDate.isEmpty()){
						productsOfStartDate.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
					}else{
						productsOfStartDate.put(deliveryLineItem.getProduct().getName(), (endDayProducts.get(deliveryLineItem.getProduct().getName()) != null?productsOfStartDate.get(deliveryLineItem.getProduct().getName()):0)+deliveryLineItem.getQuantity());
					}
				}else{
					productsOfStartDate.put(deliveryLineItem.getProduct().getName(), deliveryLineItem.getQuantity());
				}
			}
		}
		JSONArray tempArray = null;
		Map<String,JSONArray>map  = new HashMap<String, JSONArray>();
		int totalProductsCount=0;
		JSONArray array = new JSONArray();
		if(productsOfStartDate.entrySet().size()!=0){
			int totalProductsCount1=0;
			for (Map.Entry<String, Integer> entry : productsOfStartDate.entrySet()) {
				if(map.containsKey(entry.getKey())){
					if(map.isEmpty()){
						tempArray = new JSONArray();
						tempArray.add(entry.getValue());
						map.put(entry.getKey(), tempArray);
					}else{
						tempArray = map.get(entry.getKey());
						tempArray.add(entry.getValue());
						map.put(entry.getKey(), tempArray);
					}
				}else{
					tempArray = new JSONArray();
					tempArray.add(entry.getValue());
					map.put(entry.getKey(),tempArray);
				}

				totalProductsCount1=totalProductsCount1+entry.getValue();
				
			}
			totalProductsCount=totalProductsCount+totalProductsCount1;
			array.add(totalProductsCount1);
		}
		if(totalProductsCount==0){
			array.add(0);
		}
		if(endDayProducts.entrySet().size()!=0){
			int totalProductsCount3=0;
			for (Map.Entry<String, Integer> entry : endDayProducts.entrySet()) {
				if(map.containsKey(entry.getKey())){
					if(map.isEmpty()){
						tempArray = new JSONArray();
						tempArray.add(entry.getValue());
						map.put(entry.getKey(), tempArray);
					}else{
						tempArray = map.get(entry.getKey());
						tempArray.add(entry.getValue());
						map.put(entry.getKey(), tempArray);
					}
				}else{
					tempArray = new JSONArray();
					tempArray.add(entry.getValue());
					map.put(entry.getKey(),tempArray);
				}
				totalProductsCount3=totalProductsCount3+entry.getValue();
				
			}
			totalProductsCount=totalProductsCount+totalProductsCount3;
		}
		
		if(startDateDeliverySchedules.size()==0){
			for (Map.Entry<String, JSONArray> entry : map.entrySet()){
				tempArray = new JSONArray();
				tempArray = map.get(entry.getKey());
				tempArray.add(0, 0);
				map.put(entry.getKey(), tempArray);
			}
		}else{
			for (Map.Entry<String, JSONArray> entry : map.entrySet()){
				if(entry.getValue().size()==1){
					tempArray = new JSONArray();
					tempArray = map.get(entry.getKey());
					tempArray.add(0, 0);
					map.put(entry.getKey(), tempArray);
				}
			}
		}
		JSONArray productJson = new JSONArray();
		for (Map.Entry<String, JSONArray> entry : map.entrySet()) {
			JSONArray array1 = new JSONArray();
			array1.add(entry.getKey());
			array1.add(entry.getValue());
			productJson.add(array1);
		}
		
		tempArray = new JSONArray();
		tempArray.add("Total");
		array.add(totalProductsCount);
		tempArray.add(array);
		productJson.add(tempArray);
		
		JSONArray dates= new JSONArray();
		dates.add(DateHelper.getFormattedDate(startDate));
		dates.add(DateHelper.getFormattedDate(endDate));
		JSONArray productReport = new JSONArray();
		productReport.add(productJson);
		productReport.add(dates);
	   
		return productReport;
   }
   
}
