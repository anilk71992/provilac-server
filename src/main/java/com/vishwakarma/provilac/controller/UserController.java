package com.vishwakarma.provilac.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.propertyeditors.CharacterEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.dto.AccountHistoryDTO;
import com.vishwakarma.provilac.dto.InvoiceLineItemDTO;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.dto.UserDTO;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.ActivityLog;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.ReasonToStop;
import com.vishwakarma.provilac.model.User.ReferredMedia;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.mvc.validator.UserValidator;
import com.vishwakarma.provilac.property.editor.UserPropertyEditor;
import com.vishwakarma.provilac.repository.RoleRepository;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.service.ActivityLogService;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionRequestService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AsyncJobs;
import com.vishwakarma.provilac.utils.CollectionEditor;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.Md5Util;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;
import com.vishwakarma.provilac.utils.RoleCache;



/**
 * @author Vishal
 *
 */
@Controller
@SessionAttributes(value={"id", "version", "retUrl", "retPage"})
public class UserController {
	
	@Resource
	private UserValidator userValidator;
	
	@Resource
	private UserService userService;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private RoleCache roleCache;
	
	@Resource
	private RoleRepository roleRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private ProductService productService;
	
	@Resource
	private UserRouteService  userRouteService;
	
	@Resource 
	private AsyncJobs asyncJobs;
	
	@Resource
	private CityService cityService;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private ActivityLogService activityLogService; 
	
	@Resource
	private SubscriptionRequestService subscriptionRequestService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@InitBinder(value="user")
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id", "version");
		CustomDateEditor customDateEditor = new CustomDateEditor(new SimpleDateFormat(DateHelper.DATE_FORMAT), false);
		binder.registerCustomEditor(Date.class, customDateEditor);
		binder.registerCustomEditor(Character.class, new CharacterEditor(false));
		binder.registerCustomEditor(User.class, new UserPropertyEditor(userRepository));
		binder.registerCustomEditor(Set.class, new CollectionEditor(Set.class, roleRepository));
		
		binder.setValidator(userValidator);
	}
	
	private Model populateModelForAdd(Model model, User user) {
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("user", user);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		List<Role> roles =  (List<Role>) roleRepository.findAll();
		List<Role> roles1 = new ArrayList<Role>();
		List<Role> roles2 = new ArrayList<Role>();
		List<Role> rolesForAdmin = new ArrayList<Role>();
		for (Role role : roles) {
			if(!role.getId().equals(roleCache.getRole(role.ROLE_CUSTOMER).getId())){
				roles1.add(role);
			}else{
				roles2.add(role);
			}
			
			if(!role.getId().equals(roleCache.getRole(role.ROLE_CUSTOMER).getId())&&!role.getId().equals(roleCache.getRole(role.ROLE_ADMIN).getId())&&!role.getId().equals(roleCache.getRole(role.ROLE_SUPER_ADMIN).getId())){
				rolesForAdmin.add(role);
			}
		}
		model.addAttribute("roles",roles1);
		model.addAttribute("roles2",roles2);
		model.addAttribute("rolesForAdmin",rolesForAdmin);
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		List<User> assignedToUsers = userService.getListOfUsersExceptUserRoleAsCustomer(Role.ROLE_CUSTOMER, loggedInUser, false, false);
		model.addAttribute("assignedToUsers", assignedToUsers);
		model.addAttribute("cities", cityService.getAllCities(loggedInUser, false, false));
		return model;
	}
	
	@RequestMapping(value="/admin/user/addUser")
	public String addUser(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new User());
		return "admin-pages/user/addUser";
	}
	
	@RequestMapping(value="/admin/user/addUser", method=RequestMethod.POST)
	public String userAdd(User user, BindingResult formBinding, Model model) {

		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "/admin-pages/user/addUser";
		}
		User loggedInUser = userService.getLoggedInUser();
		userService.createUser(user, loggedInUser, false, false);
		
		String message = "User added successfully";
		return "redirect:/admin/user/list?message="+message;
	}

	
	@RequestMapping(value="/admin/user/add")
	public String addCustomer(Model model) {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)){
			return "redirect:/admin/denied";
		}
		model = populateModelForAdd(model, new User());
		return "admin-pages/user/add";
	}

	@RequestMapping(value="/admin/user/add", method=RequestMethod.POST)
	public String userAdd(User user, BindingResult formBinding, Model model,
			@RequestParam(required=true, value="route")String routeCode,
			@RequestParam(required=false, value="customer")String customerCode,
			@RequestParam(required=false,value="routeName")String routeName,
			@RequestParam(required = true, value = "city")Long cityId) {

		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "admin-pages/user/add";
		}
		User loggedInUser = userService.getLoggedInUser();
		user.getUserRoles().add(roleCache.getRole(Role.ROLE_CUSTOMER));
		userService.createUser(user, loggedInUser, false, false);
		
		//Add Address of customers
		Address address = new Address();
		
		City city = cityService.getCity(cityId, false, loggedInUser, false, false);
		address.setCity(city);
		address.setIsDefault(true);
		address.setName("defaultAddress");
		address.setCustomer(user);
		address.setLat(user.getLat());
		address.setLng(user.getLng());
		
		if(StringUtils.isNotBlank(user.getBuildingAddress())){
			address.setAddressLine1(user.getBuildingAddress());
			if(StringUtils.isNotBlank(user.getAddress())){
				address.setAddressLine2(user.getAddress());
			}
			addressService.createAddress(address, loggedInUser, false, false);
		}else{
			if(StringUtils.isNotBlank(user.getAddress())){
				address.setAddressLine1(user.getAddress());
				addressService.createAddress(address, loggedInUser, false, false);
			}
		}
		Route route = routeService.getRouteByCode(routeCode, false, null, false, true);
		if(null!=customerCode){
		
			List<RouteRecord> records= new ArrayList<RouteRecord>(route.getRouteRecords());
			Map<Long,Integer> map = new HashMap<Long, Integer>();
			
			for (RouteRecord routeRecord : records) {
				map.put(routeRecord.getId(), routeRecord.getPriority());
			}
			
			Set<Entry<Long, Integer>> routeRecords = map.entrySet();
			List<Entry<Long,Integer>> sortedRecords = new ArrayList(routeRecords);
			
			Collections.sort(sortedRecords,  new Comparator<Map.Entry<Long, Integer>>(){
				public int compare( Map.Entry<Long, Integer> o1, Map.Entry<Long, Integer> o2 )
				{
					return (o1.getValue()).compareTo( o2.getValue() );
				}
			} );
			
			int i = 0;
			List<Entry<Long,Integer>> results = new  ArrayList();
			
			for(int j = 0;j<=sortedRecords.size()-1;j++){
				Long recordId= sortedRecords.get(j).getKey();
				RouteRecord routeRecord1 = routeRecordService.getRouteRecord(recordId, false);
				if(routeRecord1.getCustomer().getCode().equals(customerCode)){
					RouteRecord routeRecord2 = new RouteRecord();
					routeRecord2.setCustomer(user);
					routeRecord2.setPriority(routeRecord1.getPriority()+1);
					routeRecord2.setRoute(route);
					routeRecordService.createRouteRecordForChangeRoute(routeRecord2, loggedInUser, false, false);
					results = sortedRecords.subList(i, sortedRecords.size());
					break;
				}
				i++;
			}
			
			user.setIsRouteAssigned(true);
			userService.updateUser(user.getId(), user, loggedInUser, true, true);
		}else{
			RouteRecord routeRecord = new RouteRecord();
			routeRecord.setCustomer(user);
			routeRecord.setPriority(1);
			routeRecord.setRoute(route);
			routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			user.setIsRouteAssigned(true);
			userService.updateUser(user.getId(), user, loggedInUser, true, true);
		}

		if (user.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "Customer added successfully";
			return "redirect:/admin/user/getCustomerlist?message="+message;	
		}
		String message = "User added successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	//get all user except customer
	@RequestMapping(value="/admin/user/list")
	public String getUserList(Model model,
										@RequestParam(defaultValue="1") Integer pageNumber,
										@RequestParam(required=false) String message,
										@RequestParam(required=false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<User> page = null;
		
		User user = userService.getLoggedInUser();
		if(StringUtils.isNotBlank(searchTerm)) {
			page = userService.searchUsersExceptUserRoleAsCustomer(pageNumber, searchTerm, Role.ROLE_CUSTOMER, user, true, false);
		} else {
			page = userService.searchUsersExceptUserRoleAsCustomer(pageNumber, null, Role.ROLE_CUSTOMER, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("users", page.getContent());

		return "/admin-pages/user/list";
	}
	
	// it is used only to shows customer seperately
	@RequestMapping(value="/admin/user/getCustomerlist")
	public String getCustomerList(Model model,
										@RequestParam(defaultValue="1") Integer pageNumber,
										@RequestParam(required=false) String message,
										@RequestParam(required=false) String searchTerm) {
		model.addAttribute("message", message);
		Page<User> page = null;
		User user = userService.getLoggedInUser();
		if(StringUtils.isNotBlank(searchTerm)) {
			page = userService.searchUsers(pageNumber, searchTerm,Role.ROLE_CUSTOMER, user, true, false);
		} else {
			page = userService.getUsersByStatusNotEqualToNew(pageNumber, Status.NEW, Role.ROLE_CUSTOMER, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("users", page.getContent());
		return "/admin-pages/user/customerList";
	}
	
	//it is used only to shows newly added user and whose route is not assigned
	
	@RequestMapping(value="/admin/user/getNewlyCustomerlist")
	public String getNewlyAddedCustomerList(Model model,
										@RequestParam(defaultValue="1") Integer pageNumber,
										@RequestParam(required=false) String message,
										@RequestParam(required=false) String searchTerm) {
		
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("message", message);
		Page<User> page = null;
		User user = userService.getLoggedInUser();
		if(StringUtils.isNotBlank(searchTerm)) {
			page = userService.getUsersByIsRouteAssignedAndStatus(pageNumber,searchTerm, Role.ROLE_CUSTOMER, Status.NEW, false, user, true, false);
		} else {
			page = userService.getUsersByRoleAndStatus(pageNumber, Role.ROLE_CUSTOMER, Status.NEW, user, true, false);
		}
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
		
		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("users", page.getContent());
		model.addAttribute("pageNumber", pageNumber);
		return "/admin-pages/user/NewlyCustomerList";
	}
	
	@RequestMapping(value="/admin/user/show/{cipher}")
	public String showUser(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		User user = userService.getUser(id, true, loggedIUser, true, false);
		model.addAttribute("user", user);
		return "/admin-pages/user/usershow";
	}
	
	@RequestMapping(value="/admin/customer/show/{cipher}")
	public String showCustomer(@PathVariable String cipher, Model model) {
		
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User loggedIUser = userService.getLoggedInUser();
		User user = userService.getUser(id, true, loggedIUser, true, false);
		model.addAttribute("user", user);
		return "/admin-pages/user/show";
	}
	
	@RequestMapping(value="/admin/customer/show")
	public String showCustomerBySearch(@RequestParam String customerCode,Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		User user = userService.getUserByCode(customerCode, false, loggedInUser, true, false);
		
		model.addAttribute("user", user);
		return "/admin-pages/user/show";
		/*
		String	cipher=EncryptionUtil.encode(user.getId()+"");
		return "redirect:/admin/customer/show/"+cipher;*/
	}
	
	/**
	 * Update User Without Role Customer
	*/
	
	@RequestMapping(value="/admin/user/updateUser/{cipher}")
	public String updateUser(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User user = userService.getUser(id, true, loggedInUser, true, false);
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		List<Role> roles =  (List<Role>) roleRepository.findAll();
		List<Role> roles1 = new ArrayList<Role>();
		for (Role role : roles) {
			if(!role.getId().equals(roleCache.getRole(role.ROLE_CUSTOMER).getId())){
				roles1.add(role);
			}
		}
		model.addAttribute("roles",roles1);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		return "/admin-pages/user/updateuser1";
	}
	
	@RequestMapping(value="/admin/user/updateUser1", method=RequestMethod.POST)
	public String userUpdate(User user, BindingResult formBinding, Model model) {
		
		user.setUpdateOperation(true);
		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "/admin-pages/user/update";
		}
		user.setId(Long.parseLong(model.asMap().get("id") + ""));
		user.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		userService.updateUser(user.getId(), user, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");
		
		if (user.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "User updated successfully";
			return "redirect:/admin/user/getCustomerlist?message="+message;	
		}
		String message = "User updated successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/admin/user/update/{cipher}")
	public String updateCustomer(@PathVariable String cipher, Model model) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		Long id = Long.valueOf(EncryptionUtil.decode(cipher));
		User user = userService.getUser(id, true, loggedInUser, true, false);
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		List<Role> roles =  (List<Role>) roleRepository.findAll();
		List<Role> roles1= new ArrayList<Role>();
		for (Role role : roles) { 
			if(role.getId().equals(roleCache.getRole(role.ROLE_CUSTOMER).getId())){
				roles1.add(role);
			}
		}
		model.addAttribute("roles",roles1);
		model.addAttribute("today", DateHelper.getFormattedDate(new Date()));
		model.addAttribute("users", userService.getAllUsers(loggedInUser, false, false));
		return "/admin-pages/user/update";
	}

	
	@RequestMapping(value="/admin/user/update", method=RequestMethod.POST)
	public String userCustomer(User user, BindingResult formBinding, Model model) {

		user.setUpdateOperation(true);
		userValidator.validate(user, formBinding);
		if(formBinding.hasErrors()) {
			model = populateModelForAdd(model, user);
			return "/admin-pages/user/update";
		}
		
		user.setId(Long.parseLong(model.asMap().get("id") + ""));
		user.setVersion(Long.valueOf(model.asMap().get("version") + ""));
		User loggedInUser = userService.getLoggedInUser();
		userService.updateUser(user.getId(), user, loggedInUser,true,false);
		model.asMap().remove("id");
		model.asMap().remove("version");

		if (user.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "Customer updated successfully";
			return "redirect:/admin/user/getCustomerlist?message="+message;	
		}
		if (user.getStatus().name().equals(Status.NEW.name())) {
			String message = "User updated successfully";
			return "redirect:/admin/user/getNewlyCustomerlist?message="+message;	
		}
		String message = "User updated successfully";
		return "redirect:/admin/user/getCustomerlist?message="+message;	
	}
	
	@RequestMapping(value="/admin/user/delete/{cipher}")
	public String deleteUser(@PathVariable String cipher) {
		
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		boolean flag = false;
		User user = userService.getUser(id, false, null, false, false);
		if (user.hasRole(Role.ROLE_CUSTOMER)) {
			flag = true;
		}
		String message = "";
		try {
			userService.deleteUser(id, userService.getLoggedInUser());
			message = "User deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete user";
		}
		if (flag) {
			return "redirect:/admin/user/getCustomerlist?message=" + message;
		}
		return "redirect:/admin/user/list?message="+message;
	}
	
	@RequestMapping(value="/admin/newCustomer/delete/{cipher}")
	public String deleteNewCustomer(@PathVariable String cipher,
									@RequestParam(defaultValue="1") Integer pageNumber) {
		
		Long id = Long.parseLong(EncryptionUtil.decode(cipher));
		User user = userService.getUser(id, false, null, false, false);
		if (!user.hasRole(Role.ROLE_CUSTOMER) || user.getIsRouteAssigned() || user.getStatus() != Status.NEW) {
			return "redirect:/admin/user/getNewlyCustomerlist?pageNumber=" + pageNumber + "&message=Only new customers with no route assigned can be deleted.";
		}
		String message = "";
		try {
			activityLogService.deleteAllActivitiesOfUser(id);
			addressService.deleteAllAddressesForCustomer(id);
			subscriptionRequestService.deleteSubscriptionRequestsForUser(id);
			userService.deleteUser(id, userService.getLoggedInUser());
			message = "User deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Unable to delete user";
		}
		return "redirect:/admin/user/getNewlyCustomerlist?message=" + message + "&pageNumber=" + pageNumber;
	}
	
	@RequestMapping(value="/restapi/getProfilePicture", method=RequestMethod.GET)
	@ResponseBody
	public void getProfilePic( String email, HttpServletResponse httpServletResponse){
		
		User user = userService.getUserByEmail(email, false, null, false, false);
		File file = new File(user.getProfilePicUrl());
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "image/*");
		httpServletResponse.setHeader("Content-Length", String.valueOf(file.length()));
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			FileCopyUtils.copy(fileInputStream, httpServletResponse.getOutputStream());
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 *Show Page TO track Delivery Boys 
	 */
	
	@RequestMapping(value="/admin/user/show")
	public String showDeliveryBoys() {
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/deliveryBoys/show";
	}
	
	/*
	 *Change Route  
	 */
	
	@RequestMapping(value="/admin/user/changeRoute/{userCode}")
	public String changeRoute(Model model,
							 @PathVariable String userCode) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		
		User user = userService.getUserByCode(userCode, false, loggedInUser, true, false);
		Address address = addressService.getDefaultAddressByCustomer(user.getCode(), false, null, false, false);
		if(null != address){
			model.addAttribute("addressLine1", address.getAddressLine1());
			model.addAttribute("addressLine2", address.getAddressLine2());
			model.addAttribute("addressCode", address.getCode());
			model.addAttribute("cityCode", address.getCity().getCode());
		}
		model.addAttribute("cities", cityService.getAllCities(loggedInUser, false, false));
		model.addAttribute("user", user);
		model.addAttribute("id", user.getId());
		model.addAttribute("version", user.getVersion());
		return "/admin-pages/changeRoute/show";
	}
	
	@RequestMapping(value="/admin/user/changeRoute", method=RequestMethod.POST)
	public String routeChange(@RequestParam(required=false , value="selectedRoute") String selectedRouteCode,
							  @RequestParam(required=false,  value="selectedCustomer") String selectedCustomerCode,
							  @RequestParam(required =true,value="user")String customerCode,
							  @RequestParam(required=false,value="lat")Double lat,
							  @RequestParam(required=false,value="lng")Double lng,
							  @RequestParam(required=false,value="address")String address,
							  @RequestParam(required=false,value="routeName")String routeName,
							  @RequestParam(required=false,value="locality")String locality,
							  @RequestParam(required=false,value="city")Long cityId,
							  @RequestParam(required=false,value="pincode")String pincode,
							  @RequestParam(required=false,value="buildingAddress")String buildingAddress) {
		
		User loggedInUser = userService.getLoggedInUser();
		User customer = userService.getUserByCode(customerCode, false, loggedInUser, true, false);
		Route route = routeService.getRouteByCode(selectedRouteCode, false, null, false, true);
		if(null!=selectedCustomerCode){
			RouteRecord routeRecordForSelectedCustomer = routeRecordService.getRouteRecordByCustomer(selectedCustomerCode, false, loggedInUser, false, false);
			List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);
			RouteRecord	routeRecord = new RouteRecord();
			routeRecord.setCustomer(customer);
			routeRecord.setPriority(routeRecordForSelectedCustomer.getPriority());
			routeRecord.setRoute(route);
			route.getRouteRecords().add(routeRecord);
			routeService.updateRoute(route.getId(), route, loggedInUser, false, false);
			routeRecord = routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);
			
			//Update route of DeliverySchedules of customer whose route is changed
			 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
			
			//is route assign mark true when route is assign 
			customer.setIsRouteAssigned(true);
			customer.setLat(lat);
			customer.setLng(lng);
			customer.setAddress(address);
			customer.setBuildingAddress(buildingAddress);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
			userService.updateLocation(customer.getId(), customer, loggedInUser, false, false);
			
			Route lastRoute = null;
			for(RouteRecord existingRouteRecord:existingRouteRecords){
				lastRoute = routeRecord.getRoute();
				routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
			}
			
			if(null != lastRoute) {
				deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
				DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
				deletedRouteRecord.setRoute(lastRoute);
				deletedRouteRecord.setCustomer(customer);
				deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			}
		} else {
			List<RouteRecord> existingRouteRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);					
			RouteRecord routeRecord = new RouteRecord();
			routeRecord.setCustomer(customer);
			routeRecord.setPriority(1);
			routeRecord.setRoute(route);
			routeRecordService.createRouteRecordForChangeRoute(routeRecord, loggedInUser, false, false);

			//Update route of DeliverySchedules of customer whose route is changed
			 asyncJobs.updateDeliveryScheduleRoute(routeRecord,loggedInUser);
			
			customer.setIsRouteAssigned(true);
			customer.setLat(lat);
			customer.setLng(lng);
			customer.setAddress(address);
			customer.setBuildingAddress(buildingAddress);
			userService.updateUser(customer.getId(), customer, loggedInUser, true, true);
			
			Route lastRoute = null;
			for(RouteRecord existingRouteRecord:existingRouteRecords){
				lastRoute = routeRecord.getRoute();
				routeRecordService.deleteRouteRecord(existingRouteRecord.getId(),existingRouteRecord.getRoute().getId());
			}
			
			if(null != lastRoute) {
				deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
				DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
				deletedRouteRecord.setRoute(lastRoute);
				deletedRouteRecord.setCustomer(customer);
				deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			}
		}
		
		Address existingDefaultaddress = addressService.getDefaultAddressByCustomer(customer.getCode(), false, loggedInUser, false, false);
		City city = cityService.getCity(cityId, false, loggedInUser, false, false);
		if(null != existingDefaultaddress){
			existingDefaultaddress.setIsDefault(false);
			addressService.updateAddress(existingDefaultaddress.getId(), existingDefaultaddress, loggedInUser, false, false);
		}
		Address addressToSave = new Address();
		addressToSave.setAddressLine2(address);
		addressToSave.setAddressLine1(buildingAddress);
		addressToSave.setCity(city);
		if(StringUtils.isNotBlank(locality)){
			addressToSave.setLocality(locality);
		}else{
			addressToSave.setLocality(city.getName());
		}
		if(StringUtils.isNotBlank(pincode)){
			addressToSave.setPincode(pincode);
		}
		addressToSave.setLat(lat);
		addressToSave.setLng(lng);
		addressToSave.setCustomer(customer);
		addressToSave.setName("defaultAddress");
		addressToSave.setIsDefault(true);
		
		addressService.createAddress(addressToSave, loggedInUser, false, false);
		
		
		if (customer.hasRole(Role.ROLE_CUSTOMER)) {
			String message = "Route Changed successfully";
			return "redirect:/admin/user/getCustomerlist?message="+message;	
		}
		
		String message = "Route Changed successfully";
		return "redirect:/admin/user/list?message="+message;
	}
	
	/* 
	 *Import User 
	 */
	
	@RequestMapping(value="/admin/user/import",method=RequestMethod.GET)
	public String importUsers(Model model){
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/user/import";
	}
	
	@RequestMapping(value="/admin/user/import",method=RequestMethod.POST)
	public String importUsers(Model model,@RequestParam(required=true)MultipartFile file) throws InvalidFormatException, IOException{
		String message ="sucessfully imported users";
		
		if(null==file||file.isEmpty()|| !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)){
			message="please select valid excel 2007 or excel file";
			model.addAttribute("erroeMessage", message);
			return  "/admin-pages/user/import";
		}
		
		try{
			importUsers(file);
		}catch(ProvilacException e){
			e.printStackTrace();
			message="Error while uploading users,please upload valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/user/import";
		}
		return "redirect:/admin/user/getCustomerlist?message="+message;	
	}
	
	private void importUsers(MultipartFile file) throws InvalidFormatException, IOException{
		
		Workbook workbook = WorkbookFactory.create(file.getInputStream());
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();
		
		int rowCount=0;
		for(Row row: sheet){
			
			if(rowCount==0){
				rowCount++;
				continue;
			}
			
			if(row.getPhysicalNumberOfCells() <= 12){
				continue;
			}
			
			String firstName = null;
			if(null != row.getCell(0)){
				Cell firstNameCell = row.getCell(0);
				firstName = firstNameCell.getStringCellValue();
			}
			
			String laseName = null;
			if(null != row.getCell(1)){
				Cell lastNameCell = row.getCell(1);
				laseName = lastNameCell.getStringCellValue();
			}
			String userName = null;
			if(null != row.getCell(2)){
				Cell userNameCell = row.getCell(2);
				userName = formatter.formatCellValue(userNameCell);
			}
			
			Cell emailCell = row.getCell(3);
			String email = emailCell.getStringCellValue();
			
			Cell mobileNumberCell = row.getCell(4);
			String mobileNumber = formatter.formatCellValue(mobileNumberCell);
			
			Cell alternateMobileNumberCell = row.getCell(5);
			String alternateMobileNumber = formatter.formatCellValue(alternateMobileNumberCell);
			
			String password = null;
			if(null != row.getCell(6)){
				Cell passwordCell = row.getCell(6);
				password = passwordCell.getStringCellValue();
			}
			
			Cell addressCell = row.getCell(7);
			String address = addressCell.getStringCellValue();
			
			String lat = null;
			if(null != row.getCell(8)){
				Cell latCell = row.getCell(8);
				lat = formatter.formatCellValue(latCell);
			}
			
			String lng = null;
			if(null != row.getCell(9)){
				Cell lngCell = row.getCell(9);
				lng = formatter.formatCellValue(lngCell);
			}
			
			String dob = null;
			if(null != row.getCell(10)){
				Cell dobCell = row.getCell(10);
				dob = dobCell.getStringCellValue();
			}
			
			Cell genderCell= row.getCell(11);
			String gender = formatter.formatCellValue(genderCell);
			
			String userRole= null;
			if(null != row.getCell(12)){
				Cell rollCell = row.getCell(12);
				userRole= rollCell.getStringCellValue();
			}
			
			String joiningDate  = null;
			if(null != row.getCell(13)){
				Cell joiningDateCell = row.getCell(13);
				joiningDate = joiningDateCell.getStringCellValue();
			}
			
			Cell statusCell = row.getCell(14);
			String status = statusCell.getStringCellValue();
			
			String referredMedia = null;
			if(null != row.getCell(15)){
				Cell referredMediaCell = row.getCell(15);
				referredMedia = referredMediaCell.getStringCellValue();
			}
			
			String otherBrand = null;
			if(null != row.getCell(16)){
				Cell otherBrandCell = row.getCell(16);
				otherBrand = otherBrandCell.getStringCellValue();
			}
			
			String consumptionQty = null;
			if(null != row.getCell(17)){
				Cell consumptionQtyCell = row.getCell(17);
				consumptionQty = formatter.formatCellValue(consumptionQtyCell);
			}
			
			String notes = null;
			if(null != row.getCell(18)){
				Cell notesCell = row.getCell(18);
				notes = notesCell.getStringCellValue();
			}
			
			String paymentMethod = null;
			if(null != row.getCell(19)){
				Cell paymentMethodCell = row.getCell(19);
				paymentMethod = paymentMethodCell.getStringCellValue();
			}
			
			String pendingDues = null;
			if(null != row.getCell(20)){
				Cell pendingDuesCell = row.getCell(20);
				pendingDues = formatter.formatCellValue(pendingDuesCell);
			}
			
			String referredBy = null;
			if(null != row.getCell(21)){
				Cell referredByCell = row.getCell(21);
				referredBy = formatter.formatCellValue(referredByCell);
			}
			String assignedTo = null;
			if(null!=row.getCell(22)){
				Cell assignedToCell = row.getCell(22);
				 assignedTo = formatter.formatCellValue(assignedToCell);
			}
			
			User user = null;
			boolean isUpdate = true;
			user = userService.getUserByUserNameOrMobileNumber(mobileNumber, false, null, false, false);
			if(null == user){
				user = new User();
				isUpdate = false;
			}
			
			user.setFirstName(firstName);
			user.setLastName(laseName);
			user.setEmail(email);
			user.setMobileNumber(mobileNumber);
			user.setUsername(userName);
			if(null!= alternateMobileNumber){
				user.setAlterNateMobileNumber(alternateMobileNumber);
			}
			
			if(!isUpdate){
				user.setPassword(password);
			}
			user.setBuildingAddress(address);
			if(null != lat && !lat.isEmpty() && !lat.equalsIgnoreCase("NULL")){
				user.setLat(Double.parseDouble(lat));
			}
			if(null != lng && !lng.isEmpty() && !lng.equalsIgnoreCase("NULL")){
				user.setLng(Double.parseDouble(lng));
			}
			user.setGender(Boolean.parseBoolean(gender));
			if(null != dob && !dob.isEmpty()){
				user.setDob(DateHelper.parseDate(dob));
			}
			
			if(null != joiningDate && !joiningDate.isEmpty()){
				user.setJoiningDate(DateHelper.parseDate(joiningDate));
			}
			if(null!= status){
				if(status.toLowerCase().equalsIgnoreCase(Status.ACTIVE.toString())){
					if(!user.getStatus().name().equalsIgnoreCase("ACTIVE"))
					activityLogService.createActivityLogForChangeStatus(user, "changeStatus", user.getStatus()+"_"+"ACTIVE", "user", user.getId());
					user.setStatus(Status.ACTIVE);
				}else if(status.toLowerCase().equalsIgnoreCase(Status.NEW.toString())){
					if(!user.getStatus().name().equalsIgnoreCase("NEW"))
					activityLogService.createActivityLogForChangeStatus(user, "changeStatus", user.getStatus()+"_"+"NEW", "user", user.getId());
					user.setStatus(Status.NEW);
				} else if(status.toLowerCase().equalsIgnoreCase(Status.INACTIVE.toString())){
					if(!user.getStatus().name().equalsIgnoreCase("INACTIVE"))
					activityLogService.createActivityLogForChangeStatus(user, "changeStatus", user.getStatus()+"_"+"INACTIVE", "user", user.getId());
					user.setStatus(Status.INACTIVE);
					
					List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(user);
					
					if(!routeRecords.isEmpty()){
						asyncJobs.addDeletedRouteRecordForStopCustomer(user, routeRecords.get(0).getRoute());
					}
				}else{
					if(!user.getStatus().name().equalsIgnoreCase("HOLD"))
					activityLogService.createActivityLogForChangeStatus(user, "changeStatus", user.getStatus()+"_"+"HOLD", "user", user.getId());
					user.setStatus(Status.HOLD);
				}
			}
			if(null != referredMedia){
				if(referredMedia.toLowerCase().equalsIgnoreCase(ReferredMedia.Event.toString())){
					user.setReferredMedia(ReferredMedia.Event);
				}else if(referredMedia.toLowerCase().equalsIgnoreCase(ReferredMedia.Friend.toString())){
					if(StringUtils.isNotEmpty(referredBy)){
						User referredBy1 = userService.getUserByMobileNumber(referredBy, false, null, false, false);
						if(null != referredBy1){
							user.setReferredMedia(ReferredMedia.Friend);
							user.setReferredBy(referredBy1);
						}
					}
				} else if(referredMedia.toLowerCase().equalsIgnoreCase(ReferredMedia.Newspaper.toString())){
					user.setReferredMedia(ReferredMedia.Newspaper);
				}else if(referredMedia.toLowerCase().equalsIgnoreCase(ReferredMedia.Radio.toString())){
					user.setReferredMedia(ReferredMedia.Radio);
				} else if(referredMedia.toLowerCase().equalsIgnoreCase(ReferredMedia.Social_Media.toString())){
					user.setReferredMedia(ReferredMedia.Social_Media);
				}else 
					user.setReferredMedia(ReferredMedia.Website);
				}	
			if(null != userRole && !userRole.isEmpty()){
				user.getUserRoles().add(roleCache.getRole(userRole));
			}
			if(null != notes && !notes.isEmpty()){
				user.setNotes(notes);
			}
			if(null != consumptionQty && !consumptionQty.isEmpty()){
				user.setConsumptionQty(Double.parseDouble(consumptionQty));
			}
			if(null != otherBrand && !otherBrand.isEmpty()){
				user.setOtherBrand(otherBrand);
			}
			
			if(null != paymentMethod && !otherBrand.isEmpty()){
				if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Cash.toString())){
					user.setPaymentMethod(PaymentMethod.Cash);
				}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Cheque.toString())){
					user.setPaymentMethod(PaymentMethod.Cheque);
				}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Online.toString())){
					user.setPaymentMethod(PaymentMethod.Online);
				}
			}
			
			if(null == pendingDues){
				user.setLastPendingDues(0);
			}else{
				user.setLastPendingDues(Double.parseDouble(pendingDues));
			}
			
			if(StringUtils.isNotEmpty(assignedTo)){
				User assignedToUser = userService.getUserByMobileNumber(assignedTo, false, null, false, false);
				if(null!=assignedToUser){
					user.setAssignedTo(assignedToUser);
				}
			}
			if(isUpdate){
				user.setUpdateOperation(true);
			}
			
			BindingResult bindingResult = new DataBinder(user).getBindingResult();
			userValidator.validate(user, bindingResult);
			if(bindingResult.hasErrors()){
				continue;
			}
			User loggedInUser = userService.getLoggedInUser();
			if(isUpdate){
				user = userService.updateUser(user.getId(), user, loggedInUser, false, false);
			}else{
				user = userService.createUser(user, loggedInUser, false, false);
			}
			 
			 rowCount++;
		}
	}
	
	
	/*
	 * End Import User
	 */
	
	/*
	 * Summary Sheet Generation 
	 */
	@RequestMapping(value="/admin/user/summarySheet",method=RequestMethod.GET)
	public String importUsers() throws IOException{
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		String message ="sucessfully generated sheets";
		
		String filename = "D:/NewExcelFile.xls";
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		HSSFSheet sheet =  workbook.createSheet("SummarySheet");
		HSSFRow  rowHead = sheet.createRow((short)0);
		rowHead.createCell(0).setCellValue("Code");
		rowHead.createCell(1).setCellValue("User Name");
		
		HSSFRow row = sheet.createRow((short)1);
        row.createCell(0).setCellValue("1");
        row.createCell(1).setCellValue("Sankumarsingh");
        
        FileOutputStream fileOut = new FileOutputStream(filename);
        workbook.write(fileOut);
        fileOut.close();
						
        return "redirect:/admin/user/list?message=" + message;
	}
	
	/*
	 * End Summary Sheet Generation
	 */
	
	@RequestMapping(value="/admin/user/pendingDuesList")
	public String pendingDuesUserList(Model model,
			@RequestParam(defaultValue="1") Integer pageNumber,
			@RequestParam(required=false) String message,
			@RequestParam(required=false) String searchTerm) {

			User loggedInUser = userService.getLoggedInUser();
			if(loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
				return "redirect:/admin/denied";
			}
			model.addAttribute("message", message);
			Page<User> page = null;
			
			User user = userService.getLoggedInUser();
			if(StringUtils.isNotBlank(searchTerm)) {
				page = userService.getUsersByIsRouteAssignedFalseAndDuesPending(pageNumber, searchTerm,Role.ROLE_CUSTOMER,false, user, true, false);
			} else {
				page = userService.getUsersByIsRouteAssignedFalseAndDuesPending(pageNumber, null,Role.ROLE_CUSTOMER ,false, user, true, false);
			}
			int currentIndex = page.getNumber() + 1;
			int beginIndex = Math.max(1, currentIndex - 5);
			int endIndex = Math.min(beginIndex + 10, page.getTotalPages());
			
			model.addAttribute("page", page);
			model.addAttribute("currentIndex", currentIndex);
			model.addAttribute("beginIndex", beginIndex);
			model.addAttribute("endIndex", endIndex);
			model.addAttribute("searchTerm", searchTerm);
			model.addAttribute("users", page.getContent());
			
			return "/admin-pages/user/pendingDuesList";
	}
	
	@RequestMapping("/admin/user/currentCustomer/export/{status}")
	public ResponseEntity<byte []> exportToExcel(HttpServletResponse httpServletResponse, @PathVariable("status") String status) {
		List<User> customers = null;
    if(Status.ACTIVE.name().equalsIgnoreCase(status))
	 customers = userService.getUserByCustomerRoleAndStatus(Role.ROLE_CUSTOMER, Status.ACTIVE);
    else if(Status.INACTIVE.name().equalsIgnoreCase(status))
     customers = userService.getUserByCustomerRoleAndStatus(Role.ROLE_CUSTOMER, Status.INACTIVE);
    else if(Status.HOLD.name().equalsIgnoreCase(status))
        customers = userService.getUserByCustomerRoleAndStatus(Role.ROLE_CUSTOMER, Status.HOLD);
    else if(Status.NEW.name().equalsIgnoreCase(status))
        customers = userService.getUserByCustomerRoleAndStatus(Role.ROLE_CUSTOMER, Status.NEW);
    
		ByteArrayOutputStream byteArrayOutputStream = exportCustomersToExcel(customers);
		httpServletResponse.setHeader("Expires", "0");
		httpServletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		httpServletResponse.setHeader("Pragma", "public");
		httpServletResponse.setHeader("Content-Type", "application/xls");
		httpServletResponse.setHeader("Content-Disposition", "inline;filename="+status+" Customers_details.xls");
		httpServletResponse.setHeader("Content-Length", String.valueOf(byteArrayOutputStream.toByteArray().length));
		try {
			FileCopyUtils.copy(byteArrayOutputStream.toByteArray(), httpServletResponse.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return null;
	}
	
	private ByteArrayOutputStream exportCustomersToExcel( List<User> customers){
		
		int rowIndex=1;

		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		HSSFSheet hssfSheet = hssfWorkbook.createSheet();
		HSSFRow hssfRow = null;
		HSSFCell hssfCell = null;

		HSSFCellStyle hssfCellStyleForContent = hssfWorkbook.createCellStyle();

		hssfCellStyleForContent.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderTop(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderRight(HSSFCellStyle.BORDER_THIN);
		hssfCellStyleForContent.setBorderLeft(HSSFCellStyle.BORDER_THIN);

		List<String> headers = new ArrayList<String>();
		List<String> data = null;
		List<List> records = new ArrayList<List>();

		headers.add("Customer Code");
		headers.add("Name");
		headers.add("Email");
		headers.add("Number");
		headers.add("Adderss");
		headers.add("Referral Code");
		headers.add("Date Registered");
		headers.add("Status");

		records.add(headers); 

		for (User customer : customers) {

			data = new ArrayList<String>();
			data.add(customer.getCode());
			data.add(customer.getFullName());
			data.add(customer.getEmail());
			data.add(customer.getMobileNumber());
			data.add(customer.getAddress());
			data.add(customer.getReferralCode());
			data.add(DateHelper.getFormattedDate(customer.getCreatedDate()));
			data.add(customer.getStatus().name());
			records.add(data);
		}
		rowIndex = 0;


		for (int j = 0; j < records.size(); j++) {
			hssfRow = hssfSheet.createRow(rowIndex);
			List<String> l2 = records.get(j);
			for(int k=0; k<l2.size(); k++){
				hssfCell = hssfRow.createCell(k);
				hssfCell.setCellStyle(hssfCellStyleForContent);
				hssfCell.setCellValue(l2.get(k));
			}
			rowIndex++;
		}

		for(int colNum = 0; colNum<hssfSheet.getRow(0).getLastCellNum(); colNum++){
			hssfSheet.autoSizeColumn(colNum);
		}

		hssfSheet.setFitToPage(true);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ProvilacException("Unable to write output to excel sheet, please try again");
		}
		return byteArrayOutputStream;
	}
	
	
	@RequestMapping(value="/admin/customerStatusReport/list")
	public String getCustomerStatusReportList(Model model,
										@RequestParam(defaultValue="1") Integer pageNumber,
										@RequestParam(required=false) String message,
										@RequestParam(required=false, value="startDate") String startDate,
										@RequestParam(required=false, value="endDate") String endDate,
										@RequestParam(required=false, value="status1") String status1,
										@RequestParam(required=false, value="status2") String status2) {
		
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		model.addAttribute("message", message);
		Page<User> page = null;
		
		User user = userService.getLoggedInUser();
		if(StringUtils.isNotBlank(startDate) && StringUtils.isNotEmpty(startDate)) {
			
				String finalStatus=status1.concat("_").concat(status2);
							Date fromDate = null;
							Date toDate = null;
							Calendar calendar = Calendar.getInstance();
							if(null!=startDate && null!=endDate){
								
								calendar.setTime(DateHelper.parseDate(startDate));
								 fromDate = calendar.getTime();
								calendar.setTime(DateHelper.parseDate(endDate));
								 toDate = calendar.getTime();
							}else{
								fromDate = calendar.getTime();
								toDate = calendar.getTime();
							}
						
				List<ActivityLog> activityLogs = activityLogService.getActivityLogsByDateAndStatus(finalStatus, fromDate, toDate, pageNumber, loggedInUser, false, false);
				List<User> users=new ArrayList<User>();
				Set<Long> userIds=new HashSet<Long>();
				for(ActivityLog activityLog:activityLogs){
					userIds.add(activityLog.getEntityId());
					
				}
				for(Long id:userIds){
					User customer=userService.getUser(id, false, null, false, false);
					users.add(customer);

				}
				page=new PageImpl<User>(users);
				
				
		} 
		if(page !=null){
		int currentIndex = page.getNumber() + 1;
		int beginIndex = Math.max(1, currentIndex - 5);
		int endIndex = Math.min(beginIndex + 10, page.getTotalPages());

		model.addAttribute("page", page);
		model.addAttribute("currentIndex", currentIndex);
		model.addAttribute("beginIndex", beginIndex);
		model.addAttribute("endIndex", endIndex);
		model.addAttribute("searchTerm", status1);
		model.addAttribute("users", page.getContent());
		}
		return "/admin-pages/user/customerStatusReportList";
	}
	
	@RequestMapping(value="/admin/customerStatus/show/{id}")
	public String showCustomerStatus(@PathVariable Long id, Model model) {
		
		List<ActivityLog> activityLogs=activityLogService.getActivityLogsByEntityIdAndStatus(id, null, null, false, false);
		User loggedIUser = userService.getLoggedInUser();
		User user = userService.getUser(id, true, loggedIUser, true, false);
		model.addAttribute("activityLogs", activityLogs);
		model.addAttribute("user",user);
		return "/admin-pages/user/statusShow";
	}
	
	@RequestMapping(value="/admin/user/paymentModeImport",method=RequestMethod.GET)
	public String paymentModeImport(Model model){
		User loggedInUser = userService.getLoggedInUser();
		if(loggedInUser.hasAnyRole(Role.ROLE_ACCOUNTANT)||loggedInUser.hasAnyRole(Role.ROLE_INTERN)||loggedInUser.hasAnyRole(Role.ROLE_CCE)){
			return "redirect:/admin/denied";
		}
		return "/admin-pages/user/paymentModeImport";
	}
	
	@RequestMapping(value="/admin/user/paymentModeImport",method=RequestMethod.POST)
	public String importPaymentMode(Model model,@RequestParam(required=true)MultipartFile file) throws InvalidFormatException, IOException{
		String message ="sucessfully imported Payment Modes";
		
		if(null==file||file.isEmpty()|| !file.getContentType().equals(AppConstants.IMPORT_EXCEL_FORMAT)){
			message="please select valid excel 2007 or excel file";
			model.addAttribute("erroeMessage", message);
			return  "/admin-pages/user/import";
		}
		
		try{
			paymentModeImport(file);
		}catch(ProvilacException e){
			e.printStackTrace();
			message="Error while uploading payment modes,please upload valid data";
			model.addAttribute("errorMessage", message);
			return "/admin-pages/user/paymentModeImport";
		}
		return "redirect:/admin/user/getCustomerlist?message="+message;	
	}
	
	public void paymentModeImport(MultipartFile file)throws InvalidFormatException,IOException {
		
			Workbook workbook = WorkbookFactory.create(file.getInputStream());
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter formatter=new DataFormatter();
			
			int rowCount=0;
			for(Row row: sheet){
				
			try{
				if(rowCount==0){
					rowCount++;
					continue;
				}
				
				String mobileNumber = null;
				if(null != row.getCell(0)){
					Cell mobileNumberCell = row.getCell(0);
				mobileNumber = formatter.formatCellValue(mobileNumberCell);
				}
				
				String paymentMethod = null;
				if(null != row.getCell(1)){
					Cell paymentMethodCell = row.getCell(1);
					paymentMethod = paymentMethodCell.getStringCellValue();
				}
				
				User user=userService.getUserByMobileNumber(mobileNumber, true, null, false, false);
				if(null != user){
					
					if(null != paymentMethod){
						if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Cash.toString())){
							user.setPaymentMethod(PaymentMethod.Cash);
						}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Cheque.toString())){
							user.setPaymentMethod(PaymentMethod.Cheque);
						}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.Online.toString())){
							user.setPaymentMethod(PaymentMethod.Online);
						}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.NEFT.toString())){
							user.setPaymentMethod(PaymentMethod.NEFT);
						}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.PAYTM.toString())){
							user.setPaymentMethod(PaymentMethod.PAYTM);
						}else if(paymentMethod.toLowerCase().equalsIgnoreCase(PaymentMethod.PAYU.toString())){
							user.setPaymentMethod(PaymentMethod.PAYU);
						}
						
					}
					userService.updateUser(user.getId(), user, userService.getLoggedInUser(), false, false);
				}
				
			}catch(ProvilacException e){
				e.printStackTrace();
				rowCount++;
				continue;
			}
			rowCount++;
			}
	}
	
	@RequestMapping(value="/end-user/my-profile")
	public String myProfile(Model model){
		
		User loggedInUser = userService.getLoggedInUser();
		model.addAttribute("loggedInUser", loggedInUser);
		return "/enduser-pages/my-profile";	
	}
/*
 * Ajax Call Methods
 */

	//MAYBE_UNUSED
	@RequestMapping(value="/restapi/user/deliveryBoys")
	@ResponseBody
	public ApiResponse getAllDeliveryBoys(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> users = userService.getAllUsers(loggedInUser, false, false);
			List<UserDTO> deliveryBoys = new ArrayList<UserDTO>();
			for (User user : users) {
				if(user.hasAnyRole(Role.ROLE_DELIVERY_BOY)){
					deliveryBoys.add(new UserDTO(user));
				}
			}
			apiResponse.addData("deliveryBoys", deliveryBoys);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*
	 * Ajax method to get Users except Role_Customer.
	 */
	//TODO: Optimize
	@RequestMapping(value="/user/assignedToUsers")
	@ResponseBody
	public ApiResponse getAssignedToUsers(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> users = userService.getListOfUsersExceptUserRoleAsCustomer(Role.ROLE_CUSTOMER, loggedInUser, false, false);
			List<UserDTO> assignedToUsers = new ArrayList<UserDTO>();
			for (User user : users) {
					assignedToUsers.add(new UserDTO(user));
			}
			apiResponse.addData("assignedToUsers",assignedToUsers);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*
	 * Ajax method to get Collection Boy
	 */

	//USED_FROM_WEB home.jsp
	@RequestMapping(value="/user/collectionBoys")
	@ResponseBody
	public ApiResponse getAllCollectionBoys(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> users = userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, loggedInUser, false, false);
			List<UserDTO> collectionBoys = new ArrayList<UserDTO>();
			for (User user : users) {
					collectionBoys.add(new UserDTO(user));
			}
			apiResponse.addData("collectionBoys", collectionBoys);
		} catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/*
	 * Ajax method to stop user and set reason to stop.
	 * USED_FROM_WEB
	 */
	@RequestMapping(value="/restapi/user/stopCustomer",method = RequestMethod.GET)
	@ResponseBody
	public ApiResponse StopDeliveryScheduleOfCustomer(@RequestParam(required=true)Long customerId,
													  @RequestParam(required=true)String reasonToStop,
													  @RequestParam(required=true)String fromDate,
													  Authentication authentication){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			UserInfo userInfo = (UserInfo) authentication.getPrincipal();
			User loggedInUser = userInfo.getUser();
			
			User customer = userService.getUser(customerId, true, loggedInUser, false, false); 
			UserSubscription subscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(customer.getCode(), true, null, false, false);
			if(null != subscription){
				Date dateToStop = DateHelper.parseDate(fromDate);
				DateHelper.setToStartOfDay(dateToStop);
				if(subscription.getSubscriptionType() == SubscriptionType.Postpaid) {

					//Unbilled amount
					Date startOfMonth=new Date();
					DateHelper.getStartOfMonth(startOfMonth);
					DateHelper.setToStartOfDay(startOfMonth);
					
					double totalUnbilled=0.0;
					List<DeliverySchedule> deliverySchedules=deliveryScheduleService.getunbilledDeliverySchedule(startOfMonth, DateHelper.addDays(dateToStop, -1), customer.getCode(), loggedInUser, false, true);
					for(DeliverySchedule deliverySchedule:deliverySchedules) {
						Set<DeliveryLineItem> deliveryLineItems=deliverySchedule.getDeliveryLineItems();
						for(DeliveryLineItem deliveryLineItem:deliveryLineItems){
							Product product=deliveryLineItem.getProduct();
							long quantity=deliveryLineItem.getQuantity();
							double singleProductPrice = 0.0;
							if (product.getTax() > 0) {
								singleProductPrice = product.getPrice() + ((product.getPrice() * product.getTax())/100);
							} else {
								singleProductPrice = product.getPrice();
							}    
							
							totalUnbilled+=singleProductPrice*quantity;
						}
					}
					
					plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear "+customer.getFullName()+". This is to confirm that your milk subscription with Provilac is stopped from "+DateHelper.getFormattedDate(dateToStop)+". Your outstanding amount is Rs. "+(totalUnbilled + customer.getLastPendingDues())+" to pay.  To pay via Provilac App: http://bit.do/provilac. Customer care: 8411864646");
					asyncJobs.deleteAllDeliverySchedulesFromDate(customer, dateToStop);
				} else {
					
					double balanceToAdd = 0;
					List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(customer.getId(), dateToStop, null, false, true);
					for(DeliverySchedule deliverySchedule:deliverySchedules) {
						for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
							balanceToAdd = balanceToAdd + lineItem.getTotalPrice();
						}
						deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
					}

					//Update last pending dues
					customer.setLastPendingDues((customer.getLastPendingDues() + balanceToAdd) * -1);
					plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Dear "+customer.getFullName()+". This is to confirm that your milk subscription with Provilac is stopped from "+DateHelper.getFormattedDate(dateToStop)+". Customer care: 8411864646");
				}
				
				firebaseWrapper.sendSubscriptionStopNotification(subscription);
				emailHelper.sendMailForSubscriptionStop(subscription);
				subscription.setIsCurrent(false);
				userSubscriptionService.updateUserSubscription(subscription.getId(), subscription, loggedInUser, false, false);
			}
			
			List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(customer);
			Route lastRoute = null;
			for(RouteRecord routeRecord:routeRecords){
				lastRoute = routeRecord.getRoute();
				routeRecordService.deleteRouteRecord(routeRecord.getId(), routeRecord.getRoute().getId());
			}
			
			if(null != lastRoute) {
				deletedRouteRecordService.deleteAllRecordsForCustomer(customer.getId());
				DeletedRouteRecord deletedRouteRecord = new DeletedRouteRecord();
				deletedRouteRecord.setRoute(lastRoute);
				deletedRouteRecord.setCustomer(customer);
				deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
			}
			
			if(!customer.getStatus().name().equalsIgnoreCase("INACTIVE"))
				activityLogService.createActivityLogForChangeStatus(customer, "changeStatus", customer.getStatus()+"_"+"INACTIVE", "user", customer.getId());
			
			customer.setStatus(Status.INACTIVE);
			customer.setIsRouteAssigned(false);
			customer.setReasonToStop(ReasonToStop.valueOf(reasonToStop));
			userService.updateUser(customer.getId(), customer, null, false, false);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		} 
		return apiResponse;
		
	}

	/*
	 * End Ajax Call Methods
	 */

	/*
	 * REST APIS
	 * 
	 */
	
	/**
	 * Update user
	 * USED_FROM_DEVICE
	 */
	
	@RequestMapping(value="/restapi/user/update",method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateUser(@RequestPart(required=true)UserDTO userDTO,
			@RequestPart(required = false, value = "profilePic") MultipartFile profilePic) {
		
		ApiResponse apiResponse= new ApiResponse(true);
		
		try{
			
			User loggedInUser= userService.getSuperUser();
			User user=userService.getUserByCode(userDTO.getCode(), true, loggedInUser, false, false);
			
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			if(null != userDTO.getDob() && !userDTO.getDob().isEmpty()){
				user.setDob(DateHelper.parseDate(userDTO.getDob()));
			}
			user.setFbId(userDTO.getFbId());
			user.setGender(userDTO.getGender());
			user.setgPlusId(userDTO.getgPlusId());
			user.setIsEnabled(userDTO.getIsEnabled());
			user.setIsVerified(userDTO.getIsVerified());
			user.setMobileNumber(userDTO.getMobileNumber());
			user.setAlterNateMobileNumber(userDTO.getAlterNateMobileNumber());
			user.setTwitterId(userDTO.getTwitterId());
			user.setEmail(userDTO.getEmail());
			user.setVerificationCode(userDTO.getVerificationCode());
			
			if(userDTO.getLat()!=0)
				user.setLat(userDTO.getLat());
			
			if(userDTO.getLng()!=0)
				user.setLng(userDTO.getLng());
			
			if(null!=userDTO.getAddress())
				user.setAddress(userDTO.getAddress());
			
			if(null!=userDTO.getStatus())
				user.setStatus(Status.valueOf(userDTO.getStatus()));
			
			if(null!=userDTO.getReferredMedia())
				user.setReferredMedia(ReferredMedia.valueOf(userDTO.getReferredMedia()));
			
			if(null!=userDTO.getOtherBrand())
				user.setOtherBrand(userDTO.getOtherBrand());
			
			if(userDTO.getConsumptionQty()!=0)
				user.setConsumptionQty(userDTO.getConsumptionQty());
			
			if(userDTO.getLastPendingDues()!=0)
				user.setLastPendingDues(userDTO.getLastPendingDues());
			
			if (null != profilePic) {
				try {
					user.setProfilePicData(profilePic.getBytes());
					user.setFileName(profilePic.getOriginalFilename());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			user.setUpdateOperation(true);
						
			BindingResult bindingResult = new DataBinder(user).getBindingResult();
			userValidator.validate(user, bindingResult);
			if(bindingResult.hasErrors()) {
				String errorStr= null;
				for (ObjectError objectError : bindingResult.getAllErrors()) {
					if(StringUtils.isBlank(errorStr)) {
						errorStr = objectError.getDefaultMessage();
					}else{
						errorStr = errorStr+ ", "+ objectError.getDefaultMessage();
					}
					
				}
					throw new ProvilacException(errorStr, "400");
				
			}
			user = userService.updateUser(user.getId(), user, loggedInUser,false,false);
			UserSubscription userSubscription =userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(user.getCode(), true, loggedInUser, true, true);
			if (userSubscription != null) {
				apiResponse.addData("user", new UserDTO(user,userSubscription));
			}else{
				apiResponse.addData("user", new UserDTO(user));
			}
			
		}catch(ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		
		return apiResponse;
	}
	
	/**
	 * 
	 * @param mobileNumber
	 * @return sends OTP to given mobileNumber
	 * @author Reshma
	 */
	
	
	@RequestMapping(value="/restapi/customer/sendOTP/v2", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse sendOTP(Model model, @RequestPart(required=true) String mobileNumber,
								@RequestPart(required=false) String referralCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		model.addAttribute("mobileNo", mobileNumber);
		try{
			String randomNo = new BigInteger(24, new SecureRandom()).toString().substring(0, 6);

			User existingUser = userService.getUserByMobileNumber(mobileNumber, false, null, false, false);
			
			if(null == existingUser){
				User user = new User();
				user.setIsVerified(false);
				user.setIsEnabled(true);
				user.setMobileNumber(mobileNumber);
				user.setVerificationCode(randomNo);
				user.addRole(roleCache.getRole(Role.ROLE_CUSTOMER));
				//activityLogService.createActivityLogForChangeStatus(user, "changeStatus", user.getStatus()+"_"+"NEW", "deliverySchedule", user.getId());
				user.setStatus(Status.NEW);
				
				if(StringUtils.isNotBlank(referralCode)) {
					User referredBy = userService.getUserByReferralCode(referralCode, false, null, false, false);
					if(referredBy!=null){
						user.setReferredBy(referredBy);
					}else{
						apiResponse.setSuccess(false);
						throw new ProvilacException("Invalid Referral Code.");
					}
					
				}
				
				user = userService.createUser(user, null, false, false);
			}else{
				if(existingUser.hasRole(Role.ROLE_CUSTOMER)){
					existingUser.setVerificationCode(randomNo);
					String password = userService.generateHashWithRandomString(existingUser.getMobileNumber());
					existingUser.setPassword(password);
					
					//Update referredBy only if he has not used any referral code before
					if(StringUtils.isNotBlank(referralCode) && existingUser.getReferredBy() == null) {
						User referredBy = userService.getUserByReferralCode(referralCode, false, null, false, false);
						if(referredBy!=null){
							existingUser.setReferredBy(referredBy);
							existingUser.setReferredMedia(ReferredMedia.Friend);
						}else{
							apiResponse.setSuccess(false);
							throw new ProvilacException("Invalid Referral Code.");
						}
						
					}else if(StringUtils.isNotBlank(referralCode) && existingUser.getReferredBy() != null){
						apiResponse.setSuccess(false);
						throw new ProvilacException("Referral Code Already applied by this user");
					}
					
					userService.updateUser(existingUser.getId(), existingUser, null, false, true);
				}else{
					apiResponse.setSuccess(false);
					throw new ProvilacException("Invalid OTP.", "400");
				}
			}
			String messageContent = randomNo + " is your OTP to login into Provilac app.";
			//Mobile Number Must with country code ex +918234567890
			plivoSMSHelper.sendSMS(mobileNumber, messageContent);
			apiResponse.setSuccess(true);

		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * 
	 * @param OTP
	 * @param mobileNumber
	 * @return Verifies OTP of given mobileNumber and returns userDTO.
	 * @author Reshma
	 */
	
	@RequestMapping(value="/restapi/user/verifyOTP/v2",method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse verifyOTP(@RequestPart(required=true)String OTP,
								 @RequestPart(required=true)String mobileNumber,
								 @RequestPart(required=false)String device){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User existingUser = userService.getUserByMobileNumber(mobileNumber, false, null, false, false);
			String password="";
			if(OTP.equals(existingUser.getVerificationCode())){
				existingUser.setIsVerified(true);
				userService.updateUser(existingUser.getId(), existingUser, null, false, false);
				if(device != null) {
					if(device.equalsIgnoreCase("ios")){
						password = userService.generateRandomString(mobileNumber);
					}
				} else {
					 password = userService.generateHashWithRandomString(mobileNumber);
				}
				existingUser.setPassword(Md5Util.md5(password));
				userService.updatePasswordForUser(existingUser.getId(), existingUser, null, false, false);
				apiResponse.addData("user", new UserDTO(existingUser));
			}else{
				apiResponse.setSuccess(false);
				throw new ProvilacException("Invalid OTP.", "400");
			}
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE/MAYBE_UNUSED
	@RequestMapping(value="/restapi/user/verifyOTP",method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse verifyOTPOld(@RequestParam(required=true)String OTP,
								    @RequestParam(required=true)String mobileNumber,
								    @RequestParam(required=false)String device){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try {
			String password="";
			User existingUser = userService.getUserByMobileNumber(mobileNumber, false, null, false, false);
			if(OTP.equals(existingUser.getVerificationCode())){
				existingUser.setIsVerified(true);
				userService.updateUser(existingUser.getId(), existingUser, null, false, false);
				if(device != null) {
					if(device.equalsIgnoreCase("ios")) {
						password = userService.generateRandomString(mobileNumber);
					}
				} else {
					 password = userService.generateHashWithRandomString(mobileNumber);
				}
				existingUser.setPassword(Md5Util.md5(password));
				userService.updatePasswordForUser(existingUser.getId(), existingUser, null, false, false);
				apiResponse.addData("user", new UserDTO(existingUser));
			} else {
				apiResponse.setSuccess(false);
				throw new ProvilacException("Invalid OTP.", "400");
			}
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}

	/**
	 * USED_FROM_WEB
	 * USED_FROM_DEVICE 
	 * @param userId
	 * @return userDTO object with details of given userId
	 */
	@RequestMapping(value = "/restapi/user/getDetails", method = RequestMethod.GET)
	@ResponseBody
	public ApiResponse getUserDetails(@RequestParam(required = true) Long userId) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User loggedInUser = userService.getLoggedInUser();
			User user = userService.getUser(userId, true, loggedInUser, false, false);
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(user.getCode(), true, loggedInUser, true, true);
			if (userSubscription != null) {
				apiResponse.addData("user", new UserDTO(user,userSubscription));
			} else {
				apiResponse.addData("user", new UserDTO(user));
			}
			Address defaultAddress = addressService.getDefaultAddressByCustomer(user.getCode(), false, loggedInUser, false, false);
			if(null != defaultAddress){
				apiResponse.addData("defaultAddress", true);
			}else{
				apiResponse.addData("defaultAddress", false);
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	//USED_FROM_DEVICE iOS
	@RequestMapping(value = "/openapi/user/getDetails", method = RequestMethod.GET)
	@ResponseBody
	public ApiResponse getUserDetailsOpenApi(@RequestParam(required = true) String userCode) {

		ApiResponse apiResponse = new ApiResponse(true);
		try {
			User loggedInUser = userService.getSuperUser();
			User user = userService.getUserByCode(userCode, true, loggedInUser, false, false);
			UserSubscription userSubscription = userSubscriptionService.getUserSubscriptionByCustomerAndIsCurrent(user.getCode(), true, loggedInUser, true, true);
			if (userSubscription != null) {
				apiResponse.addData("user", new UserDTO(user,userSubscription));
			} else {
				apiResponse.addData("user", new UserDTO(user));
			}
			Address defaultAddress = addressService.getDefaultAddressByCustomer(user.getCode(), false, loggedInUser, false, false);
			if(null != defaultAddress){
				apiResponse.addData("defaultAddress", true);
			}else{
				apiResponse.addData("defaultAddress", false);
			}
		} catch (ProvilacException e) {
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/*
	 * USED_FROM_DEVICE/MAYBE_UNUSED
	 * REST Api to get account history(Invoices,Payments of current month) of perticular Customer. 
	 */
	@RequestMapping(value="/restapi/user/accountHistory",method=RequestMethod.GET)
	@ResponseBody
	public ApiResponse getAccountHistory(@RequestParam(required=true)String customerCode){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<Invoice> invoices = invoiceService.getAllInvoicesByCustomer(customerCode, null, false, true);
			List<Payment> payments = paymentService.getAllPaymentsByCustomer(customerCode, null, false, false);
			List<AccountHistoryDTO> history = new ArrayList<AccountHistoryDTO>();
			
			User loggedInUser =userService.getLoggedInUser();
			User customer = userService.getUserByCode(customerCode, false, loggedInUser, false, false);
			
			if(null!=invoices){
				for (Invoice invoice : invoices) {
					List<InvoiceLineItemDTO> invoiceLineItemDTOs = new  ArrayList<InvoiceLineItemDTO>();
					List<DeliverySchedule>deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(customer.getId(), invoice.getFromDate(), invoice.getToDate(), SubscriptionType.Postpaid, null, false, true);
					List<DeliveryLineItem> lineItems= new ArrayList<DeliveryLineItem>();
					
					for (DeliverySchedule deliverySchedule : deliverySchedules) {
						lineItems.addAll(deliverySchedule.getDeliveryLineItems());
					}
					
					Map<Long,Integer> map = new HashMap<Long, Integer>();
					
					for (DeliveryLineItem deliveryLineItem : lineItems) {
						Long productId = deliveryLineItem.getProduct().getId();
						Integer quantity = 0;
						if(map.containsKey(productId) ) {
							quantity = map.get(productId)+deliveryLineItem.getQuantity();
							map.put(productId, quantity);
						}else{
							map.put(productId, deliveryLineItem.getQuantity());
						}
					}
					
					List<Entry<Long,Integer>> productWiseQuantity = new ArrayList<Map.Entry<Long,Integer>>(map.entrySet());
					
					for (Entry<Long, Integer> entry : productWiseQuantity) {
						InvoiceLineItemDTO invoiceLineItemDTO = new InvoiceLineItemDTO();
						Product product= productService.getProduct(entry.getKey(), false, null, false, false);
						invoiceLineItemDTO.setProductDTO(new ProductDTO(product));
						invoiceLineItemDTO.setQuantity(entry.getValue());
						invoiceLineItemDTO.setPrice(entry.getValue()*product.getPrice());
						invoiceLineItemDTOs.add(invoiceLineItemDTO);
					}
					history.add(new AccountHistoryDTO(invoice, invoiceLineItemDTOs));
				}
			}
			if(null!= payments){
				for (Payment payment : payments) {
					history.add(new AccountHistoryDTO(payment));
				}
			}
			Map<String, Double>accountSummary = new HashMap<String, Double>();
			
			Calendar c = Calendar.getInstance();
			Date currentDate = c.getTime();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = 1;
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.set(year, month, day);
			Date firstDayOfMonth = c.getTime();
			
			List<DeliverySchedule>deliverySchedules= deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(customer.getId(), firstDayOfMonth, currentDate, SubscriptionType.Postpaid, null, false, true);
			double quantity = 0.0;
			double amount = 0.0;
			List<DeliveryLineItem> lineItems = new ArrayList<DeliveryLineItem>();
			
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				lineItems.addAll(deliverySchedule.getDeliveryLineItems());
			}
			
			for (DeliveryLineItem deliveryLineItem : lineItems) {
				quantity+= deliveryLineItem.getQuantity();
				amount+= deliveryLineItem.getQuantity()*deliveryLineItem.getProduct().getPrice();
			}
			
			c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
			Date currentMonthToDate = c.getTime();
			
			amount = 0.0; //Reuse amount for paid amount
			List<Payment> currentMonthPayments = paymentService.getPaymentsByCustomerIdAndDateRange(customer.getId(), firstDayOfMonth, currentMonthToDate, null, false, false);
			for(Payment payment:currentMonthPayments){
				amount += payment.getAmount();
			}

			accountSummary.put("quantity", quantity);
			accountSummary.put("amount", amount);
			accountSummary.put("totalOutstanding", customer.getLastPendingDues());
			accountSummary.put("paid", amount);
			
			apiResponse.addData("startDate", DateHelper.getFormattedDate(firstDayOfMonth));
			apiResponse.addData("endDate", DateHelper.getFormattedDate(currentMonthToDate));
			apiResponse.addData("accountHistory", history);
			apiResponse.addData("accountSummary", accountSummary);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		
		return apiResponse;
	}
	
	/*
	 * USED_FROM_DEVICE/MAYBE_UNUSED
	 * Update lattitude and longitude of deliveryBoy's current location. 
	 */
	
	@RequestMapping(value="/restapi/user/updateLatLng", method=RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateLatLng(@RequestParam(required=true) Long deliveryBoyId,
									@RequestParam(required=true)Double lat,
									@RequestParam(required=true)Double lng){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User deliveryBoy = userService.getUser(deliveryBoyId, true, null, false, false);
			deliveryBoy.setLat(lat);
			deliveryBoy.setLng(lng);
			deliveryBoy= userService.updateUser(deliveryBoy.getId(), deliveryBoy, null, false, false);
			apiResponse.addData("deliveryBoy", new UserDTO(deliveryBoy));
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(),e.getErrorCode());
		}
		
		return apiResponse; 
	}
	
	@RequestMapping(value="/restapi/user/deletescheduleforInactive")
	@ResponseBody
	public ApiResponse getallInactive(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			int count = 0;
			List<User> customers = userService.getUsersByStatus(Status.INACTIVE);
			for (User customer : customers) {
				List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerAndDate(customer.getId(), new Date(), null, false, false);
				for (DeliverySchedule deliverySchedule : deliverySchedules) {
					deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
				}
			}
			apiResponse.addData("Count",count);
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		} 
		return apiResponse;
	}
	
	
	/**
	 * Method to get new Customers
	 * USED_FROM_WEB New customer calendar and newly customer nav
	 */
	@RequestMapping(value="/restapi/user/getNewCustomers")
	@ResponseBody
	public ApiResponse getNewCustomers(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.NEW, loggedInUser, false, false);
			List<UserDTO>dtos = new ArrayList<UserDTO>();
			for (User user : customers) {
				if(user.getIsRouteAssigned()==true){
					dtos.add(new UserDTO(user));
				}
			}
			apiResponse.addData("customers", dtos);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	/**
	 * Get Customers whoes status is STOP
	 * USED_FROM_WEB stopCustomerCalendar
	 */
	@RequestMapping(value="/restapi/user/getStopCustomers")
	@ResponseBody
	public ApiResponse getStopCustomers(){
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			User loggedInUser = userService.getLoggedInUser();
			List<User> customers = userService.getUsersByRoleAndStatus(Role.ROLE_CUSTOMER, Status.INACTIVE, loggedInUser, false, false);
			List<UserDTO>dtos = new ArrayList<UserDTO>();
			for (User user : customers) {
				dtos.add(new UserDTO(user));
			}
			apiResponse.addData("customers", dtos);
			
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		}
		return apiResponse;
	}
	
	@RequestMapping(value="/restapi/user/deleteNewCustomers")
	@ResponseBody
	public ApiResponse deleteNewCustomer(){
		
		ApiResponse apiResponse = new ApiResponse(true);
		try{
			List<User> customers = userService.getUsersByStatus(Status.NEW);
			for (User customer : customers) {
				List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomer(customer.getId(), null, false, false);
				for (DeliverySchedule deliverySchedule : deliverySchedules) {
					deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
				}
				userService.deleteUser(customer.getId(), userService.getLoggedInUser());
			}	
		}catch(ProvilacException e){
			e.printStackTrace();
			apiResponse.setError(e.getExceptionMsg(), e.getErrorCode());
		} 
		return apiResponse;
	}
}
