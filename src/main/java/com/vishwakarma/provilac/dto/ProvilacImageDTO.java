package com.vishwakarma.provilac.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.ProvilacImage;
import com.vishwakarma.provilac.model.ProvilacImage.LinkTo;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_NULL)
public class ProvilacImageDTO implements Serializable{

	private long id;
	
	private String imagePath;
	
	private LinkTo linkTo;

	public ProvilacImageDTO(){
		
	}
	
	public ProvilacImageDTO(ProvilacImage provilacImage){
		setId(provilacImage.getId());
		setImagePath("/openapi/getProvilacImage?id="+provilacImage.getId());
		if(null !=provilacImage.getLinkTo())
			setLinkTo(provilacImage.getLinkTo());
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public LinkTo getLinkTo() {
		return linkTo;
	}

	public void setLinkTo(LinkTo linkTo) {
		this.linkTo = linkTo;
	}
	
	
}
