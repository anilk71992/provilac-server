package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.UserRoute;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown= true)
public class UserRouteDTO {
	
	private String code, userCode, routeCode;
	
	public UserRouteDTO(){
		
	}
	
	public UserRouteDTO(UserRoute userRoute){
		setCode(userRoute.getCode());
		setUserCode(userRoute.getDeliveryBoy().getCode());
		setRouteCode(userRoute.getRoute().getCode());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	
}
