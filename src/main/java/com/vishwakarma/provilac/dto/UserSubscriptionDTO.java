package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.UserSubscription;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserSubscriptionDTO {
	
	private String code, userCode, permenantNote, subscriptionType, cipher;
	
	private Set<SubscriptionLineItemDTO> lineItemDTOs= new HashSet<SubscriptionLineItemDTO>();
	
	public UserSubscriptionDTO(){
		
	}
	
	public UserSubscriptionDTO(UserSubscription subscription){
		setCode(subscription.getCode());
		setUserCode(subscription.getUser().getCode());
		setPermenantNote(subscription.getPermanantNote());
		for (SubscriptionLineItem lineItem : subscription.getSubscriptionLineItems()) {
			lineItemDTOs.add(new SubscriptionLineItemDTO(lineItem));
		}
		setSubscriptionType(subscription.getSubscriptionType().name());
		setCipher(subscription.getCipher());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Set<SubscriptionLineItemDTO> getLineItemDTOs() {
		return lineItemDTOs;
	}

	public void setLineItemDTOs(Set<SubscriptionLineItemDTO> lineItemDTOs) {
		this.lineItemDTOs = lineItemDTOs;
	}

	public String getPermenantNote() {
		return permenantNote;
	}

	public void setPermenantNote(String permenantNote) {
		this.permenantNote = permenantNote;
	}

	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getCipher() {
		return cipher;
	}

	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
}
