package com.vishwakarma.provilac.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Note;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class NoteDTO implements Serializable  {

	private String code,customerCode,date;
	
	private String callDetails;
	
	public NoteDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public NoteDTO(Note note) {
		// TODO Auto-generated constructor stub
		setDate(DateHelper.getFormattedTimestamp(note.getDate()));
		setCallDetails(note.getCallDetails());
		setCode(note.getCode());
		setCustomerCode(note.getCustomer().getCode());
	}
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getCallDetails() {
		return callDetails;
	}


	public void setCallDetails(String callDetails) {
		this.callDetails = callDetails;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
}
