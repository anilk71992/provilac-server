package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.utils.AppConstants.Day;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RequestLineItemDTO {
	
	private String code,productCode,userSubscriptionCode,customPattern;
	
	private SubscriptionType type;
	
	private int quantity;
	
	private Day day;
	
	public RequestLineItemDTO(){
		
	}
	
	public RequestLineItemDTO(RequestLineItem lineItem){
			setCode(lineItem.getCode());
			setProductCode(lineItem.getProduct().getCode());
			setUserSubscriptionCode(lineItem.getSubscriptionRequest().getCode());
			setQuantity(lineItem.getQuantity());
			setDay(lineItem.getDay());
			setCustomPattern(lineItem.getCustomPattern());
			setType(lineItem.getType());
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getUserSubscriptionCode() {
		return userSubscriptionCode;
	}

	public void setUserSubscriptionCode(String userSubscriptionCode) {
		this.userSubscriptionCode = userSubscriptionCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public String getCustomPattern() {
		return customPattern;
	}

	public void setCustomPattern(String customPattern) {
		this.customPattern = customPattern;
	}

	public SubscriptionType getType() {
		return type;
	}

	public void setType(SubscriptionType type) {
		this.type = type;
	}
	


	

}
