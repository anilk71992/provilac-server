package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PaymentDTO {
	
	private String code, date,txnId,customerCode,chequeNumber, bankName,receivedByUserCode,invoiceCode,collectionCode,depositCode,customerName,paytmOrderId;
	
	private double amount, ponitsAmount,adjustmentAmount,cashback;
	
	private int pointsRedemed;

	private String paymentMethod;
	
	private boolean isBounced,hasDeposited, isVerified;

	public PaymentDTO(){
		
	}
	
	public PaymentDTO(Payment payment){
		setCode(payment.getCode());
		if(null!=payment.getInvoice()){
			setInvoiceCode(payment.getInvoice().getCode());
		}
		if(null!=payment.getDate()){
			setDate(DateHelper.getFormattedDate(payment.getDate()));
		}
		setTxnId(payment.getTxnId());
		setCustomerCode(payment.getCustomer().getCode());
		setCustomerName(payment.getCustomer().getFirstName() + " "+ payment.getCustomer().getLastName());
		if(null != payment.getChequeNumber()){
			setChequeNumber(payment.getChequeNumber());
		}
		if(null != payment.getBankName()){
			setBankName(payment.getBankName());
		}
		if(null != payment.getReceivedBy()){
			setReceivedByUserCode(payment.getReceivedBy().getCode());
		}
		setAmount(payment.getAmount());
		setPaymentMethod(payment.getPaymentMethod().name());
		setIsBounced(payment.getIsBounced());
		setHasDeposited(payment.getHasDeposited());
		setIsVerified(payment.getIsVerified());
		if(null != payment.getCollection())
			setCollectionCode(payment.getCollection().getCode());
		if(null!=payment.getDeposit())
			setDepositCode(payment.getDeposit().getCode());
		setPonitsAmount(payment.getPointsAmount());
		setPointsRedemed(payment.getPointsRedemed());
		setAdjustmentAmount(payment.getAdjustmentAmount());
		setCashback(payment.getCashback());
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getReceivedByUserCode() {
		return receivedByUserCode;
	}

	public void setReceivedByUserCode(String receivedByUserCode) {
		this.receivedByUserCode = receivedByUserCode;
	}

	
	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public boolean getIsBounced() {
		return isBounced;
	}

	public void setIsBounced(boolean isBounced) {
		this.isBounced = isBounced;
	}

	public String getCollectionCode() {
		return collectionCode;
	}

	public void setCollectionCode(String collectionCode) {
		this.collectionCode = collectionCode;
	}

	public String getDepositCode() {
		return depositCode;
	}

	public void setDepositCode(String depositCode) {
		this.depositCode = depositCode;
	}

	public boolean getHasDeposited() {
		return hasDeposited;
	}

	public void setHasDeposited(boolean hasDeposited) {
		this.hasDeposited = hasDeposited;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getPonitsAmount() {
		return ponitsAmount;
	}

	public void setPonitsAmount(double ponitsAmount) {
		this.ponitsAmount = ponitsAmount;
	}

	public int getPointsRedemed() {
		return pointsRedemed;
	}

	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}

	public String getPaytmOrderId() {
		return paytmOrderId;
	}

	public void setPaytmOrderId(String paytmOrderId) {
		this.paytmOrderId = paytmOrderId;
	}

	public double getAdjustmentAmount() {
		return adjustmentAmount;
	}

	public void setAdjustmentAmount(double adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	public double getCashback() {
		return cashback;
	}

	public void setCashback(double cashback) {
		this.cashback = cashback;
	}

}
