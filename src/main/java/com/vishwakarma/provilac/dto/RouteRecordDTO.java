package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.RouteRecord;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RouteRecordDTO {
	
	private String customerCode, code, routeCode;
	
	private int priority;

	private UserDTO customerDTO;
	
	public RouteRecordDTO(){
		
	}
	
	public RouteRecordDTO(RouteRecord routeRecord){
		setCode(routeRecord.getCode());
		setCustomerCode(routeRecord.getCustomer().getCode());
		setRouteCode(routeRecord.getRoute().getCode());
		setPriority(routeRecord.getPriority());
		setCustomerDTO(new UserDTO(routeRecord.getCustomer()));
	}
	
	public RouteRecordDTO(RouteRecord routeRecord,UserDTO userDTO ){
		setCode(routeRecord.getCode());
		setRouteCode(routeRecord.getRoute().getCode());
		setPriority(routeRecord.getPriority());
		setCustomerDTO(userDTO);
	}
	
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRouteCode() {
		return routeCode;
	}
	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public UserDTO getCustomerDTO() {
		return customerDTO;
	}
	public void setCustomerDTO(UserDTO customerDTO) {
		this.customerDTO = customerDTO;
	}
	
	
	
	
}
