package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Category;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CategoryDTO {
	
	private String code;
	
	private String name, description, categoryPicUrl;
	
	
	
	public CategoryDTO(){
		
	}
	
	public CategoryDTO(Category category){
		setCode(category.getCode());
		setName(category.getName());
		setDescription(category.getDescription());
		setCategoryPicUrl("/restapi/getCategoryPicture?id=" + category.getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public String getCategoryPicUrl() {
		return categoryPicUrl;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCategoryPicUrl(String categoryPicUrl) {
		this.categoryPicUrl = categoryPicUrl;
	}



}
