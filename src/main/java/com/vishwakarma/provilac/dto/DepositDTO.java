package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Deposit;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.utils.DateHelper;

/**
 * 
 * @author Harshal
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class DepositDTO {
	
	private String code,date,collectionBoyCode,cashTransactionId;
	
	private double totalAmount,cashAmount,chequeAmount;
	
	private Integer noOfCheque;
	
	private Set<PaymentDTO> chequePaymantsDTOs = new HashSet<PaymentDTO>();
	
	public DepositDTO(){
		
	}
	
	public DepositDTO(Deposit deposit){
		setCode(deposit.getCode());
		setCollectionBoyCode(deposit.getCollectionBoy().getCode());
		setCashAmount(deposit.getCashAmount());
		setTotalAmount(deposit.getTotalAmount());
		setChequeAmount(deposit.getChequeAmount());
		setNoOfCheque(deposit.getNoOfCheques());
		setDate(DateHelper.getFormattedTimestamp(deposit.getDate()));
		
		if(deposit.getChequePayments().size() != 0){
			for (Payment payment : deposit.getChequePayments()) {
				chequePaymantsDTOs.add(new PaymentDTO(payment));
			}
		}
		if(StringUtils.isNotBlank(deposit.getCashTransactionId()))
			setCashTransactionId(deposit.getCashTransactionId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCollectionBoyCode() {
		return collectionBoyCode;
	}

	public void setCollectionBoyCode(String collectionBoyCode) {
		this.collectionBoyCode = collectionBoyCode;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public Integer getNoOfCheque() {
		return noOfCheque;
	}

	public void setNoOfCheque(Integer noOfCheque) {
		this.noOfCheque = noOfCheque;
	}

	public Set<PaymentDTO> getChequePaymantsDTOs() {
		return chequePaymantsDTOs;
	}

	public void setChequePaymantsDTOs(Set<PaymentDTO> chequePaymantsDTOs) {
		this.chequePaymantsDTOs = chequePaymantsDTOs;
	}

	public String getCashTransactionId() {
		return cashTransactionId;
	}

	public void setCashTransactionId(String cashTransactionId) {
		this.cashTransactionId = cashTransactionId;
	}
	
}
