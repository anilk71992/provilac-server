package com.vishwakarma.provilac.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OneTimeOrder.OrderStatus;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class OneTimeOrderDTO implements Serializable{

	private String code,customerCode,addressCode,date;
	
	
	private String instructions,txnId,paymentGateway,paymentOrderId;
	
	private double totalAmount = 0, pointsDiscount = 0, finalAmount = 0;
	
	private int pointsRedemed;
	
	private int priority;
	
	private OrderStatus status;
	
	private Double lat, lng;
	
	Set<OrderLineItemDTO> orderLineItemDTOs=new HashSet<OrderLineItemDTO>();
	
	public OneTimeOrderDTO(){
		
	}
	public OneTimeOrderDTO(OneTimeOrder oneTimeOrder){
		
		setCode(oneTimeOrder.getCode());
		setInstructions(oneTimeOrder.getInstructions());
		setTxnId(oneTimeOrder.getTxnId());
		setPaymentGateway(oneTimeOrder.getPaymentGateway());
		setTotalAmount(oneTimeOrder.getTotalAmount());
		setPointsDiscount(oneTimeOrder.getPointsDiscount());
		setFinalAmount(oneTimeOrder.getFinalAmount());
		setCustomerCode(oneTimeOrder.getCustomer().getCode());
		setAddressCode(oneTimeOrder.getAddress().getCode());
		setDate(DateHelper.getFormattedDate(oneTimeOrder.getDate()));
		setPointsRedemed(oneTimeOrder.getPointsRedemed());
		setPriority(oneTimeOrder.getPriority());
		setStatus(oneTimeOrder.getStatus());
		setLat(oneTimeOrder.getAddress().getLat());
		setLng(oneTimeOrder.getAddress().getLng());
		setPaymentOrderId(oneTimeOrder.getPaymentOrderId());
	}
	
	public OneTimeOrderDTO(OneTimeOrder oneTimeOrder,Set<OrderLineItem> orderLineItems){
		
		this(oneTimeOrder);
		if(orderLineItems.size()!=0){
			for(OrderLineItem orderLineItem:orderLineItems){
				orderLineItemDTOs.add(new OrderLineItemDTO(orderLineItem));
			}
		}
		
	}
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getPointsDiscount() {
		return pointsDiscount;
	}

	public void setPointsDiscount(double pointsDiscount) {
		this.pointsDiscount = pointsDiscount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getAddressCode() {
		return addressCode;
	}
	public void setAddressCode(String addressCode) {
		this.addressCode = addressCode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getPointsRedemed() {
		return pointsRedemed;
	}
	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public Set<OrderLineItemDTO> getOrderLineItemDTOs() {
		return orderLineItemDTOs;
	}
	public void setOrderLineItemDTOs(Set<OrderLineItemDTO> orderLineItemDTOs) {
		this.orderLineItemDTOs = orderLineItemDTOs;
	}
	public String getPaymentOrderId() {
		return paymentOrderId;
	}
	public void setPaymentOrderId(String paymentOrderId) {
		this.paymentOrderId = paymentOrderId;
	}
	
	
}
