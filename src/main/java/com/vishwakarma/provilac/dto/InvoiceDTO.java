package com.vishwakarma.provilac.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class InvoiceDTO {
	
	private Long id;
	
	private String code,fromDate,toDate,customerCode,preferedPaymentMethod,collectionBoyNote,collectionStatus,reason;
	
	private double totalAmount, lastPendingDues, provilacOutStanding,totalPaid;
	
	private Set<DeliveryScheduleDTO> deliveryScheduleDTOs ;
	
	private List<InvoiceLineItemDTO> invoiceLineItems = new ArrayList<InvoiceLineItemDTO>();
	
	private UserDTO userDTO;
	
	private boolean isPaid;
	
	public InvoiceDTO(){
		
	}
	public InvoiceDTO(Invoice invoice){
		setId(invoice.getId());
		setCode(invoice.getCode());
		setFromDate(DateHelper.getFormattedDate(invoice.getFromDate()));
		setToDate(DateHelper.getFormattedDate(invoice.getToDate()));
		setCustomerCode(invoice.getCustomer().getCode());
		setTotalAmount(invoice.getTotalAmount());
		setLastPendingDues(invoice.getLastPendingDues());
		setProvilacOutStanding(invoice.getProvilacOutStanding());
		setIsPaid(invoice.getIsPaid());
		setCollectionBoyNote(invoice.getCollectionBoyNote());
		if(invoice.getCustomer().getPaymentMethod()!=null){
			setPreferedPaymentMethod(invoice.getCustomer().getPaymentMethod().toString());
		}
		if(null!=invoice.getCollectionStatus()){
			setCollectionStatus(invoice.getCollectionStatus().toString());
		}
		if(null!=invoice.getReason()){
			setReason(invoice.getReason());
		}
	}
	
	public InvoiceDTO(Invoice invoice , Double pendingDue){
		this(invoice);
		setLastPendingDues(pendingDue);
	}
	
	public InvoiceDTO(Invoice invoice ,List<InvoiceLineItemDTO> invoiceLineItemDTOs){
		this(invoice);
		for (InvoiceLineItemDTO invoiceLineItemDTO : invoiceLineItemDTOs) {
			invoiceLineItems.add(invoiceLineItemDTO);
		}
	}
	
	public InvoiceDTO(Invoice invoice ,List<InvoiceLineItemDTO> invoiceLineItemDTOs,UserDTO userDTO,double totalPaid){
		this(invoice,invoiceLineItemDTOs);
		setUserDTO(userDTO);
		setTotalPaid(totalPaid);
		
	}
	
	public InvoiceDTO(Invoice invoice,List<InvoiceLineItemDTO> invoiceLineItemDTOs,double totalPaid){
		this(invoice,invoiceLineItemDTOs);
		setTotalPaid(totalPaid);
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	public String getToDate() {
		return toDate;
	}
	
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}
	
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getLastPendingDues() {
		return lastPendingDues;
	}
	
	public void setLastPendingDues(double lastPendingDues) {
		this.lastPendingDues = lastPendingDues;
	}
	
	
	public double getProvilacOutStanding() {
		return provilacOutStanding;
	}
	
	public void setProvilacOutStanding(double provilacOutStanding) {
		this.provilacOutStanding = provilacOutStanding;
	}
	
	public Set<DeliveryScheduleDTO> getDeliveryScheduleDTOs() {
		return deliveryScheduleDTOs;
	}
	
	public void setDeliveryScheduleDTOs(
			Set<DeliveryScheduleDTO> deliveryScheduleDTOs) {
		this.deliveryScheduleDTOs = deliveryScheduleDTOs;
	}
	public List<InvoiceLineItemDTO> getInvoiceLineItems() {
		return invoiceLineItems;
	}
	public void setInvoiceLineItems(List<InvoiceLineItemDTO> invoiceLineItems) {
		this.invoiceLineItems = invoiceLineItems;
	}
	public String getPreferedPaymentMethod() {
		return preferedPaymentMethod;
	}
	public void setPreferedPaymentMethod(String preferedPaymentMethod) {
		this.preferedPaymentMethod = preferedPaymentMethod;
	}
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public boolean getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}
	public String getCollectionBoyNote() {
		return collectionBoyNote;
	}
	public void setCollectionBoyNote(String collectionBoyNote) {
		this.collectionBoyNote = collectionBoyNote;
	}
	
	public String getCollectionStatus() {
		return collectionStatus;
	}
	public void setCollectionStatus(String collectionStatus) {
		this.collectionStatus = collectionStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public double getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(double totalPaid) {
		this.totalPaid = totalPaid;
	}
	
	
	
}
