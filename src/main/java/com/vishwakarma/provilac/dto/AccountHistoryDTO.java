package com.vishwakarma.provilac.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class AccountHistoryDTO {
	
	private String type;
	
	private InvoiceDTO invoiceDTO;
	
	private PaymentDTO paymentDTO;
	
	private String date;
	
	private Map<String , Double> accountSummary;
	
	public AccountHistoryDTO(){
		
	}
	
	public AccountHistoryDTO(Invoice invoice,List<InvoiceLineItemDTO> invoiceLineItemDTOs){
		setType("invoice");
		setDate(DateHelper.getFormattedDate(invoice.getToDate()));
		setInvoiceDTO(new InvoiceDTO(invoice, invoiceLineItemDTOs));
	}
	
	public AccountHistoryDTO(Payment payment){
		setType("payment");
		setDate(DateHelper.getFormattedDate(payment.getDate()));
		setPaymentDTO(new PaymentDTO(payment));
	}
	
	
	public AccountHistoryDTO(Map<String, Double> accountSummary){
		setAccountSummary(accountSummary);
	}

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public InvoiceDTO getInvoiceDTO() {
		return invoiceDTO;
	}

	public void setInvoiceDTO(InvoiceDTO invoiceDTO) {
		this.invoiceDTO = invoiceDTO;
	}

	public PaymentDTO getPaymentDTO() {
		return paymentDTO;
	}

	public void setPaymentDTO(PaymentDTO paymentDTO) {
		this.paymentDTO = paymentDTO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Map<String, Double> getAccountSummary() {
		return accountSummary;
	}

	public void setAccountSummary(Map<String, Double> accountSummary) {
		this.accountSummary = accountSummary;
	}

	

}
