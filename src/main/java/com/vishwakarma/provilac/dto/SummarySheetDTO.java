package com.vishwakarma.provilac.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.SummarySheet;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SummarySheetDTO {
	
	private String fromDate,toDate;
	
	private List<SummarySheetRecordDTO>sheetRecordDTOs = new ArrayList<SummarySheetRecordDTO>();
	
	public SummarySheetDTO(){
		
	}
	
	public SummarySheetDTO(SummarySheet summarySheet){
	      setFromDate(DateHelper.getFormattedDate(summarySheet.getFromDate()));
	      setToDate(DateHelper.getFormattedDate(summarySheet.getToDate()));
	      
	      if(null!=summarySheet.getSummarySheetRecords()){
	    	  for (SummarySheetRecord sheetRecord : summarySheet.getSummarySheetRecords()) {
				sheetRecordDTOs.add(new SummarySheetRecordDTO(sheetRecord));
			}
	      }
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<SummarySheetRecordDTO> getSheetRecordDTOs() {
		return sheetRecordDTOs;
	}

	public void setSheetRecordDTOs(List<SummarySheetRecordDTO> sheetRecordDTOs) {
		this.sheetRecordDTOs = sheetRecordDTOs;
	}

}
