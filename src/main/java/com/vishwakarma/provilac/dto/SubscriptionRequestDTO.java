package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.SubscriptionRequest;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SubscriptionRequestDTO {
	
	private String code,userCode,permenantNote;
	
	private Set<RequestLineItemDTO> lineItemDTOs= new HashSet<RequestLineItemDTO>();
	
	public SubscriptionRequestDTO(){
		
	}
	
	public SubscriptionRequestDTO(SubscriptionRequest subscription){
		setCode(subscription.getCode());
		setUserCode(subscription.getUser().getCode());
		setPermenantNote(subscription.getPermanantNote());
		for (RequestLineItem lineItem : subscription.getRequestLineItems()) {
			lineItemDTOs.add(new RequestLineItemDTO(lineItem));
			}
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	
	public Set<RequestLineItemDTO> getLineItemDTOs() {
		return lineItemDTOs;
	}

	public void setLineItemDTOs(Set<RequestLineItemDTO> lineItemDTOs) {
		this.lineItemDTOs = lineItemDTOs;
	}

	public String getPermenantNote() {
		return permenantNote;
	}

	public void setPermenantNote(String permenantNote) {
		this.permenantNote = permenantNote;
	}
	
	

}
