package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class DeliveryLineItemDTO {
	
	private String code,productCode,deliveryScheduleCode,productName;
	private int quantity;
	private String date;
	private long deliveryScheduleId;
	private String productColorCode,lastModifiedBy;
	
	public DeliveryLineItemDTO(){
		
	}
	
	public DeliveryLineItemDTO(DeliveryLineItem deliveryLineItem){
		setCode(deliveryLineItem.getCode());
		setProductCode(deliveryLineItem.getProduct().getCode());
		setDeliveryScheduleCode(deliveryLineItem.getDeliverySchedule().getCode());
		setProductName(deliveryLineItem.getProduct().getName());
		setQuantity(deliveryLineItem.getQuantity());
		setDate(DateHelper.getFormattedDate(deliveryLineItem.getDeliverySchedule().getDate()));
		setDeliveryScheduleId(deliveryLineItem.getDeliverySchedule().getId());
		setProductColorCode(deliveryLineItem.getProduct().getColorCode());
		
		setLastModifiedBy(deliveryLineItem.getLastModifiedBy());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getDeliveryScheduleCode() {
		return deliveryScheduleCode;
	}

	public void setDeliveryScheduleCode(String deliveryScheduleCode) {
		this.deliveryScheduleCode = deliveryScheduleCode;
	}
	
	
	public long getDeliveryScheduleId() {
		return deliveryScheduleId;
	}

	public void setDeliveryScheduleId(long deliveryScheduleId) {
		this.deliveryScheduleId = deliveryScheduleId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getProductColorCode() {
		return productColorCode;
	}

	public void setProductColorCode(String productColorCode) {
		this.productColorCode = productColorCode;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	

}
