package com.vishwakarma.provilac.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.OrderLineItem;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class OrderLineItemDTO implements Serializable{

	private String code,productCode;

	private int quantity = 0;
	
	private double amount;
	
	private long oneTimeOrderId,productId;
	
	private ProductDTO productDTO;

	public OrderLineItemDTO(){
		
	}
	public OrderLineItemDTO(OrderLineItem orderLineItem){
		setCode(orderLineItem.getCode());
		setQuantity(orderLineItem.getQuantity());
		setAmount(orderLineItem.getAmount());
		setOneTimeOrderId(orderLineItem.getId());
		setProductId(orderLineItem.getProduct().getId());
		setProductCode(orderLineItem.getProduct().getCode());
		setProductDTO(new ProductDTO(orderLineItem.getProduct()));
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getOneTimeOrderId() {
		return oneTimeOrderId;
	}

	public void setOneTimeOrderId(long oneTimeOrderId) {
		this.oneTimeOrderId = oneTimeOrderId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}
	

	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public ProductDTO getProductDTO() {
		return productDTO;
	}
	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}
	
	
}
