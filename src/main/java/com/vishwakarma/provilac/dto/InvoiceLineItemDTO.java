package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Product;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class InvoiceLineItemDTO {

	private String productName;
	
	private int Quantity;
	
	private double price;
	
	private ProductDTO productDTO = new ProductDTO();
	
	
	public InvoiceLineItemDTO(){
		
	}
	
	public InvoiceLineItemDTO(Product product,int quantity,double price){
		setQuantity(quantity);
		setPrice(price);
		setProductDTO(new ProductDTO(product));
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}
	
	
}
