package com.vishwakarma.provilac.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Locality;

/**
 * 
 * @author Rohit
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_NULL)
public class LocalityDTO implements Serializable{

	private Long id,pincode;
	
	private String name, code , cityCode;

	
	public LocalityDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public LocalityDTO(Locality locality){
		setId(locality.getId());
		setCode(locality.getCode());
		setName(locality.getName());
		setCityCode(locality.getCity().getCode());
		setPincode(locality.getPincode());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	
}
