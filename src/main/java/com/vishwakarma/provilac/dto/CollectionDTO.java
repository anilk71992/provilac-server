package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Collection;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.utils.DateHelper;

/**
 * 
 * @author Harshal
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CollectionDTO {
	
	private String code,date,collectionBoyCode,routCode;
	
	private double totalAmount,cashAmount,chequeAmount;
	
	private Integer noOfCheque;
	
	private Set<PaymentDTO> paymentDTOs = new HashSet<PaymentDTO>();

	public CollectionDTO(){
		
	}
	
	public CollectionDTO(Collection collection){
		setCode(collection.getCode());
		setCollectionBoyCode(collection.getCollectionBoy().getCode());
		setCashAmount(collection.getCashAmount());
		setTotalAmount(collection.getTotalAmount());
		setChequeAmount(collection.getChequeAmount());
		setNoOfCheque(collection.getNoOfCheques());
		setDate(DateHelper.getFormattedTimestamp(collection.getDate()));
		setRoutCode(collection.getRoute().getCode());
		if(null != collection.getPayments()){
			for(Payment payment:collection.getPayments()){
				paymentDTOs.add(new PaymentDTO(payment));
			}
		}
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCollectionBoyCode() {
		return collectionBoyCode;
	}

	public void setCollectionBoyCode(String collectionBoyCode) {
		this.collectionBoyCode = collectionBoyCode;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public Integer getNoOfCheque() {
		return noOfCheque;
	}

	public void setNoOfCheque(Integer noOfCheque) {
		this.noOfCheque = noOfCheque;
	}

	public Set<PaymentDTO> getPaymentDTOs() {
		return paymentDTOs;
	}

	public void setPaymentDTOs(Set<PaymentDTO> paymentDTOs) {
		this.paymentDTOs = paymentDTOs;
	}

	public String getRoutCode() {
		return routCode;
	}

	public void setRoutCode(String routCode) {
		this.routCode = routCode;
	}
	
}
