package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RouteDTO {
	
	private String name,code;
	
	private Long id;
	
	private Set<RouteRecordDTO> routeRecordDTOs = new HashSet<RouteRecordDTO>();

	public RouteDTO(){
		
	}
	
	public RouteDTO(Route route) {
		setCode(route.getCode());
		setName(route.getName());
		setId(route.getId());
	}
	
	public RouteDTO(Route route, List<RouteRecord> routeRecords) {
		this(route);
		for (RouteRecord routeRecord : route.getRouteRecords()) {
			routeRecordDTOs.add(new RouteRecordDTO(routeRecord));
		}
	}
	public RouteDTO(Route route, Set<RouteRecordDTO> routeRecordDtos) {
		this(route);
		if(routeRecordDtos.size()!=0){
			routeRecordDTOs.addAll(routeRecordDtos);
		}
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<RouteRecordDTO> getRouteRecordDTOs() {
		return routeRecordDTOs;
	}

	public void setRouteRecordDTOs(Set<RouteRecordDTO> routeRecordDTOs) {
		this.routeRecordDTOs = routeRecordDTOs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
