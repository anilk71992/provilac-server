package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class DeliveryScheduleDTO {
	
	private long id;
	
	private int priority;
	
	private Double lat,lng;
	
	private DeliveryStatus status;
	
	private Type type;
	
	private String reason,date, deliveryTime,code,customerCode,invoiceCode,permanantNote,tempararyNote,routeCode;
	
	private Set<DeliveryLineItemDTO> deliveryLineItemDTOs = new HashSet<DeliveryLineItemDTO>();

	private UserDTO customerDTO ;
	
	private boolean isUpdatedFromWeb;
	
	private boolean isUpdatedFromMobile;
	
	public DeliveryScheduleDTO(){
		
	}
	
	public DeliveryScheduleDTO(DeliverySchedule deliverySchedule){
		setId(deliverySchedule.getId());
		setCode(deliverySchedule.getCode());
		setStatus(deliverySchedule.getStatus());
		if(null!=deliverySchedule.getCustomer()){
			setCustomerCode(deliverySchedule.getCustomer().getCode());
			setCustomerDTO(new UserDTO(deliverySchedule.getCustomer()));
		}
		setType(deliverySchedule.getType());
		setLat(deliverySchedule.getLat());
		setLng(deliverySchedule.getLng());
		setReason(deliverySchedule.getReason());
		setDate(DateHelper.getFormattedDate(deliverySchedule.getDate()));
		if(null != deliverySchedule.getDeliveryTime())
			setDeliveryTime(DateHelper.getFormattedDate(deliverySchedule.getDeliveryTime()));
		if(null!=deliverySchedule.getCustomer()){
			setCustomerCode(deliverySchedule.getCustomer().getCode());
			setCustomerDTO(new UserDTO(deliverySchedule.getCustomer()));
		}
		if(null != deliverySchedule.getPermanantNote()){
			setPermanantNote(deliverySchedule.getPermanantNote());
		}
		if(null != deliverySchedule.getTempararyNote()){
			setTempararyNote(deliverySchedule.getTempararyNote());
		}
		setRouteCode(deliverySchedule.getRoute().getCode());
		setPriority(deliverySchedule.getPriority());
		
		if(null!=deliverySchedule.getInvoice()){
			setInvoiceCode(deliverySchedule.getInvoice().getCode());
		}
		
		setUpdatedFromMobile(deliverySchedule.isUpdatedFromMobile());
		setUpdatedFromWeb(deliverySchedule.isUpdatedFromWeb());
	}
	
	public DeliveryScheduleDTO(DeliverySchedule deliverySchedule,List<DeliveryLineItem> deliveryLineItems){
		this(deliverySchedule);
		for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
			deliveryLineItemDTOs.add(new DeliveryLineItemDTO(deliveryLineItem));
		}
	}

	public DeliveryScheduleDTO(DeliverySchedule deliverySchedule,Set<DeliveryLineItem> deliveryLineItems){
		this(deliverySchedule);
		if(deliveryLineItems.size()!=0){
			for (DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
				deliveryLineItemDTOs.add(new DeliveryLineItemDTO(deliveryLineItem));
			}
		}
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	
	public DeliveryStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatus status) {
		this.status = status;
	}

	public Type getType() {
		return type;
	}

	public String getPermanantNote() {
		return permanantNote;
	}

	public void setPermanantNote(String permanantNote) {
		this.permanantNote = permanantNote;
	}

	public String getTempararyNote() {
		return tempararyNote;
	}

	public void setTempararyNote(String tempararyNote) {
		this.tempararyNote = tempararyNote;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}
	
	public Set<DeliveryLineItemDTO> getDeliveryLineItemDTOs() {
		return deliveryLineItemDTOs;
	}

	public void setDeliveryLineItemDTOs(
			Set<DeliveryLineItemDTO> deliveryLineItemDTOs) {
		this.deliveryLineItemDTOs = deliveryLineItemDTOs;
	}

	public UserDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(UserDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public boolean isUpdatedFromWeb() {
		return isUpdatedFromWeb;
	}

	public void setUpdatedFromWeb(boolean isUpdatedFromWeb) {
		this.isUpdatedFromWeb = isUpdatedFromWeb;
	}

	public boolean isUpdatedFromMobile() {
		return isUpdatedFromMobile;
	}

	public void setUpdatedFromMobile(boolean isUpdatedFromMobile) {
		this.isUpdatedFromMobile = isUpdatedFromMobile;
	}
	
	
}
