package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Product;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductDTO {
	
	private Long id;
	
	private String code,categoryCode;
	
	private String name,desciption,productPicUrl,productBannerUrl,productCartPicUrl,productListPicUrl,productDetailsPicUrl;
	
	private double price,tax;
	
	private boolean isAvailableSubscription, isAvailableOneTimeOrder;
	
	public ProductDTO(){
		
	}
	
	public ProductDTO(Product product){
		setId(product.getId());
		setCode(product.getCode());
		setName(product.getName());
		setPrice(product.getPrice());
		setCategoryCode(product.getCategory().getCode());
		setDesciption(product.getDescription());
		setTax(product.getTax());
		setIsAvailableSubscription(product.isAvailableSubscription());
		setIsAvailableOneTimeOrder(product.isAvailableOneTimeOrder());
		setProductPicUrl("restapi/getProductPicture?id=" + product.getId());
		setProductBannerUrl("restapi/getProductBanner?id="+product.getId());
		setProductCartPicUrl("restapi/getProductCartPicture?id="+product.getId());
		setProductListPicUrl("restapi/getProductListPicture?id="+product.getId());
		setProductDetailsPicUrl("restapi/getProductDetailsPicture?id="+product.getId());
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getProductPicUrl() {
		return productPicUrl;
	}

	public void setProductPicUrl(String productPicUrl) {
		this.productPicUrl = productPicUrl;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public boolean isAvailableSubscription() {
		return isAvailableSubscription;
	}

	public void setIsAvailableSubscription(boolean isAvailableSubscription) {
		this.isAvailableSubscription = isAvailableSubscription;
	}

	public boolean isAvailableOneTimeOrder() {
		return isAvailableOneTimeOrder;
	}

	public void setIsAvailableOneTimeOrder(boolean isAvailableOneTimeOrder) {
		this.isAvailableOneTimeOrder = isAvailableOneTimeOrder;
	}

	public String getProductBannerUrl() {
		return productBannerUrl;
	}

	public void setProductBannerUrl(String productBannerUrl) {
		this.productBannerUrl = productBannerUrl;
	}

	public String getProductCartPicUrl() {
		return productCartPicUrl;
	}

	public String getProductListPicUrl() {
		return productListPicUrl;
	}

	public String getProductDetailsPicUrl() {
		return productDetailsPicUrl;
	}

	public void setProductCartPicUrl(String productCartPicUrl) {
		this.productCartPicUrl = productCartPicUrl;
	}

	public void setProductListPicUrl(String productListPicUrl) {
		this.productListPicUrl = productListPicUrl;
	}

	public void setProductDetailsPicUrl(String productDetailsPicUrl) {
		this.productDetailsPicUrl = productDetailsPicUrl;
	}
    
}
