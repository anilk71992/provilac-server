package com.vishwakarma.provilac.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.utils.DateHelper;

/**
 * @author Vishal
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserDTO implements Serializable {

	private long id;

	private String paymentMethod;

	private String status;

	private String referredMedia;

	private String firstName, lastName, email, mobileNumber, username, profilePicUrl,dob, fbId, gPlusId, twitterId, password, verificationCode,alterNateMobileNumber;

	private String code, referredBy, referralCode, notes, address, otherBrand;

	private double consumptionQty, lastPendingDues, collectionOuststanding, remainingPrepayBalance;

	private double lat, lng;

	private boolean gender, isEnabled;

	private List<String> userRoles = new ArrayList<String>();

	private boolean isVerified = false;
	private boolean isRouteAssigned = false;
	
	private int accumulatedPoints = 0;
	private boolean allocatedFirstTimeReferralPoints;

	private UserSubscriptionDTO userSubscriptionDTO;
	
	private Set<DeliveryLineItemDTO> deliveryLineDTOs = new HashSet<DeliveryLineItemDTO>();

	public UserDTO() {

	}

	public UserDTO(User user) {
		setId(user.getId());
		setCode(user.getCode());
		setFirstName(user.getFirstName());
		setLastName(user.getLastName());
		setEmail(user.getEmail());
		setMobileNumber(user.getMobileNumber());
		setUsername(user.getUsername());
		setPassword(user.getPassword());
		setProfilePicUrl("/restapi/getProfilePicture?email=" + user.getEmail());
		
		if (null != user.getPaymentMethod())
			setPaymentMethod(user.getPaymentMethod().name());
		if (null != user.getStatus())
			setStatus(user.getStatus().name());
		if (null != user.getReferredMedia())
			setReferredMedia(user.getReferredMedia().name());
		if (null != user.getReferredBy())
			setReferredBy(user.getReferredBy().getCode());
		
		setReferralCode(user.getReferralCode());
		setNotes(user.getNotes());
		setOtherBrand(user.getOtherBrand());
		setAddress(user.getBuildingAddress());
		setConsumptionQty(user.getConsumptionQty());
		setLastPendingDues(user.getLastPendingDues());
		setCollectionOuststanding(user.getCollectionOuststanding());
		setRemainingPrepayBalance(user.getRemainingPrepayBalance());
		setAlterNateMobileNumber(user.getAlterNateMobileNumber());
		setIsRouteAssigned(user.getIsRouteAssigned());
		setAccumulatedPoints(user.getAccumulatedPoints());
		setAllocatedFirstTimeReferralPoints(user.getAllocatedFirstTimeReferralPoints());
		
		if (null != user.getLat())
			setLat(user.getLat());

		if (null != user.getLng())
			setLng(user.getLng());

		if (null != user.getDob())
			setDob(DateHelper.getFormattedDate(user.getDob()));
		setGender(user.getGender());
		setIsEnabled(user.getIsEnabled());
		setIsVerified(user.getIsVerified());
		for (Role role : user.getUserRoles()) {
			userRoles.add(role.getRole());
		}
		setFbId(user.getFbId());
		setgPlusId(user.getgPlusId());
		setTwitterId(user.getTwitterId());
		if(null!=user.getPaymentMethod()){
			setPaymentMethod(user.getPaymentMethod().toString());
		}
	}

	public UserDTO(User user, UserSubscription userSubscription) {
		this(user);
		if (userSubscription.getIsCurrent()) {
			setUserSubscriptionDTO(new UserSubscriptionDTO(userSubscription));
		}
	}
	
	public UserDTO(User user, Set<DeliveryLineItem> deliveryLineItems) {
		this(user);
		if(deliveryLineItems.size()!=0){
			for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				deliveryLineDTOs.add(new DeliveryLineItemDTO(deliveryLineItem));
			}


		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReferredMedia() {
		return referredMedia;
	}

	public void setReferredMedia(String referredMedia) {
		this.referredMedia = referredMedia;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getgPlusId() {
		return gPlusId;
	}

	public void setgPlusId(String gPlusId) {
		this.gPlusId = gPlusId;
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOtherBrand() {
		return otherBrand;
	}

	public void setOtherBrand(String otherBrand) {
		this.otherBrand = otherBrand;
	}

	public double getConsumptionQty() {
		return consumptionQty;
	}

	public void setConsumptionQty(double consumptionQty) {
		this.consumptionQty = consumptionQty;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public List<String> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<String> userRoles) {
		this.userRoles = userRoles;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public double getLastPendingDues() {
		return lastPendingDues;
	}

	public void setLastPendingDues(double lastPendingDues) {
		this.lastPendingDues = lastPendingDues;
	}

	public UserSubscriptionDTO getUserSubscriptionDTO() {
		return userSubscriptionDTO;
	}

	public void setUserSubscriptionDTO(UserSubscriptionDTO userSubscriptionDTO) {
		this.userSubscriptionDTO = userSubscriptionDTO;
	}

	public String getAlterNateMobileNumber() {
		return alterNateMobileNumber;
	}

	public void setAlterNateMobileNumber(String alterNateMobileNumber) {
		this.alterNateMobileNumber = alterNateMobileNumber;
	}

	public boolean getIsRouteAssigned() {
		return isRouteAssigned;
	}

	public void setIsRouteAssigned(boolean isRouteAssigned) {
		this.isRouteAssigned = isRouteAssigned;
	}

	public int getAccumulatedPoints() {
		return accumulatedPoints;
	}

	public void setAccumulatedPoints(int accumulatedPoints) {
		this.accumulatedPoints = accumulatedPoints;
	}

	public boolean getAllocatedFirstTimeReferralPoints() {
		return allocatedFirstTimeReferralPoints;
	}

	public void setAllocatedFirstTimeReferralPoints(boolean allocatedFirstTimeReferralPoints) {
		this.allocatedFirstTimeReferralPoints = allocatedFirstTimeReferralPoints;
	}

	public Set<DeliveryLineItemDTO> getDeliveryLineDTOs() {
		return deliveryLineDTOs;
	}

	public void setDeliveryLineDTOs(Set<DeliveryLineItemDTO> deliveryLineDTOs) {
		this.deliveryLineDTOs = deliveryLineDTOs;
	}

	public double getCollectionOuststanding() {
		return collectionOuststanding;
	}

	public void setCollectionOuststanding(double collectionOuststanding) {
		this.collectionOuststanding = collectionOuststanding;
	}

	public double getRemainingPrepayBalance() {
		return remainingPrepayBalance;
	}

	public void setRemainingPrepayBalance(double remainingPrepayBalance) {
		this.remainingPrepayBalance = remainingPrepayBalance;
	}
	
	
	
}
