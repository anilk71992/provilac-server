package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PaymentDetailsDTO {

	private long id;
	
	private String routeName;
	
	private double totalAmount,totalPendingDues,totalPaidAmount;
	
	public  PaymentDetailsDTO(){
		
	}
	
	public PaymentDetailsDTO(Long id,String route,Double totalAmount,Double totalPendingDues,Double TotalPaidAmount){
		
		setId(id);
		setRouteName(route);
		setTotalAmount(totalAmount);
		setTotalPaidAmount(totalPaidAmount);
		setTotalPendingDues(totalPendingDues);
	}

	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalPendingDues() {
		return totalPendingDues;
	}

	public void setTotalPendingDues(double totalPendingDues) {
		this.totalPendingDues = totalPendingDues;
	}

	public double getTotalPaidAmount() {
		return totalPaidAmount;
	}

	public void setTotalPaidAmount(double totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}
	
	
	
}
