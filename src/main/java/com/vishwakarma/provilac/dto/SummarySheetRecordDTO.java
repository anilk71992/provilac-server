package com.vishwakarma.provilac.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SummarySheetRecordDTO {
	
	private String code,summarySheetCode,customerCode,chequeNumber,receivedByUserCode,invoiceCode,chequeDate,receivedDate,receiptNumber;
	
	private double amount,adjustment,previousMonth,balance_amount,totalAMount;

	private PaymentMethod paymentMethod;

	public SummarySheetRecordDTO(){
		
	}
	
	public SummarySheetRecordDTO(SummarySheetRecord sheetRecord){
         setCode(sheetRecord.getCode());
         setSummarySheetCode(sheetRecord.getSummarySheet().getCode());
         setCustomerCode(sheetRecord.getCustomer().getCode());
         if(null!=sheetRecord.getPaymentMethod())
        	 setPaymentMethod(sheetRecord.getPaymentMethod());
         if(null!=sheetRecord.getChequeNumber())
        	 setChequeNumber(sheetRecord.getChequeNumber());
         if(null!=getChequeDate())
        	 setChequeDate(DateHelper.getFormattedDate(sheetRecord.getChequeDate()));
         if(null!=sheetRecord.getReceivedBy())
        	 setReceivedByUserCode(sheetRecord.getReceivedBy().getCode());
         setInvoiceCode(sheetRecord.getInvoice().getCode());
        if(null!=sheetRecord.getReceiptNumber())
        	setReceiptNumber(sheetRecord.getReceiptNumber());
        setTotalAMount(sheetRecord.getTotalAmount());
        setAdjustment(sheetRecord.getAdjustment());
        setPreviousMonth(sheetRecord.getPreviousMonth());
        setBalance_amount(sheetRecord.getBalanceAmount());
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}


	public String getReceivedByUserCode() {
		return receivedByUserCode;
	}

	public void setReceivedByUserCode(String receivedByUserCode) {
		this.receivedByUserCode = receivedByUserCode;
	}

	
	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getSummarySheetCode() {
		return summarySheetCode;
	}

	public void setSummarySheetCode(String summarySheetCode) {
		this.summarySheetCode = summarySheetCode;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public double getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(double adjustment) {
		this.adjustment = adjustment;
	}

	public double getPreviousMonth() {
		return previousMonth;
	}

	public void setPreviousMonth(double previousMonth) {
		this.previousMonth = previousMonth;
	}

	public double getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(double balance_amount) {
		this.balance_amount = balance_amount;
	}

	public double getTotalAMount() {
		return totalAMount;
	}

	public void setTotalAMount(double totalAMount) {
		this.totalAMount = totalAMount;
	}

}
