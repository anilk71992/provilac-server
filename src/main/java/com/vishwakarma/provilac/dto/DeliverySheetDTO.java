package com.vishwakarma.provilac.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySheet;
import com.vishwakarma.provilac.utils.DateHelper;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class DeliverySheetDTO {
	
	private long id;
	
	private UserRouteDTO userRouteDTO;
	
	private boolean isCompleted;
	
	private String code,date;
	
	private Set<DeliveryScheduleDTO> deliveryScheduleDTOs = new HashSet<DeliveryScheduleDTO>();

	public DeliverySheetDTO(){
		
	}
	
	public DeliverySheetDTO(DeliverySheet deliverySheet){
		setId(deliverySheet.getId());
		setCode(deliverySheet.getCode());
		setDate(DateHelper.getFormattedDate(deliverySheet.getDate()));
		setIsCompleted(deliverySheet.getIsCompleted());
		setUserRouteDTO(new UserRouteDTO(deliverySheet.getUserRoute()));
		if(null != deliverySheet.getDeliverySchedules()){
			for(DeliverySchedule deliverySchedule:deliverySheet.getDeliverySchedules()){
				deliveryScheduleDTOs.add(new DeliveryScheduleDTO(deliverySchedule));
			}
		}
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserRouteDTO getUserRouteDTO() {
		return userRouteDTO;
	}

	public void setUserRouteDTO(UserRouteDTO userRouteDTO) {
		this.userRouteDTO = userRouteDTO;
	}

	public boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<DeliveryScheduleDTO> getDeliveryScheduleDTOs() {
		return deliveryScheduleDTOs;
	}

	public void setDeliveryScheduleDTOs(Set<DeliveryScheduleDTO> deliveryScheduleDTOs) {
		this.deliveryScheduleDTOs = deliveryScheduleDTOs;
	}
}
