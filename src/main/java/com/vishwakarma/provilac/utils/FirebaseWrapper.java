package com.vishwakarma.provilac.utils;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.common.base.CharMatcher;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Deposit;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.SubscriptionRequest;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription;

@Component
public class FirebaseWrapper {

	private Environment environment;
	
	private static enum NotificationType {
		CHANGE_ROUTE,
		ASSIGN_ROUTE,
		PAYMENT_CONFIRMATION,
		INVOICE_GENERATED,
		MISC,
		
		UNDELIVERED_ORDER,
		BOUNCED_PAYMENT,
		PAYMENT_RECEIVED,
		//INVOICE_GENERATED,
		ROUTERECORD_CREATED,
		ROUTERECORD_UPDATED,
		ROUTE_SELECTION_FOR_COLLECTION_BOY,
		COLLECTION_BOY_AMOUNT_DEPOSITE,
		COLLECTION_BOY_OUTSTANDING_AMOUNT,
		DELIVERY_BOY_REMINDER,
		ONE_DAY_BEFORE_TRIAL_REMINDER,
		TODAY_TRIAL_REMINDER,
		SUBSCRIPTION_ADDED,
		SUBSCRIPTION_CHANGED,
		SUBSCRIPTION_UPDATED,
		DELIVERED_ORDER,
		SUBSCRIPTION_STOP,
		SUBSCRIPTION_HOLD,
		ON_THE_WAY_TO_COLLECTION,
		REFERRAL_BALANCE_ADDED,
		ONE_TIME_ORDER_NOTIFICATION,
		DELIVERY_SCHEDULE_CHANGE_NOTIFICATION,
		PAYMENT_NOT_RECEIVED,
		DO_LOGOUT,
		NEW_SUBSCRIPTION_REQUEST_PLACED,
		CHANGE_SUBSCRIPTION_REQUEST_PLACED,
		SUBSCRIPTION_REQUEST_APPROVED
	}

	private String firebaseUrl = "firebase.url";
	//private static String FIREBASE_URL = "https://testfirebase-c2685.firebaseio.com";
	private static String APP_ID = "C3X5ZumXLUCJGdd1WYcMzZlBYtHpRQstLkPb0Bz2";
	private static String REST_API_KEY = "EkX5Hikg1c5MUuaFKOFZAzWSuceCPrCXhoGSbCnt";
	private static String PARSE_URL ="https://api.parse.com/1/push"; 
	

	private Logger logger = LoggerFactory.getLogger(FirebaseWrapper.class);

	public void sendSimpleMessage(String message) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("message", message);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		sendFirebaseRequest(jsonObject, "U-1", "U-2");
	}
	
	private void sendFirebaseRequest(JSONObject dataObject, String... channels) {

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("data", dataObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		for(int i = 0; i < channels.length; i++) {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(firebaseUrl + "/provilac/users/" + channels[i] + ".json");
	
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
	
			StringEntity stringEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
			httpPost.setEntity(stringEntity);
	
			try {
				HttpResponse httpResponse = httpClient.execute(httpPost);
				logger.debug("================== Firebase Response Code ================== " + httpResponse.getStatusLine().getStatusCode());
				String response = IOUtils.toString(httpResponse.getEntity().getContent());
				logger.debug("================== Firebase Response ================== " + response);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//ParsePushWrapper Class Methods......
	
	/*private static enum NotificationType{
		UNDELIVERED_ORDER,
		BOUNCED_PAYMENT,
		PAYMENT_RECEIVED,
		INVOICE_GENERATED,
		ROUTERECORD_CREATED,
		ROUTERECORD_UPDATED,
		ROUTE_SELECTION_FOR_COLLECTION_BOY,
		COLLECTION_BOY_AMOUNT_DEPOSITE,
		COLLECTION_BOY_OUTSTANDING_AMOUNT,
		DELIVERY_BOY_REMINDER,
		ONE_DAY_BEFORE_TRIAL_REMINDER,
		TODAY_TRIAL_REMINDER,
		SUBSCRIPTION_ADDED,
		SUBSCRIPTION_CHANGED,
		SUBSCRIPTION_UPDATED,
		DELIVERED_ORDER,
		SUBSCRIPTION_STOP,
		SUBSCRIPTION_HOLD,
		ON_THE_WAY_TO_COLLECTION,
		REFERRAL_BALANCE_ADDED,
		ONE_TIME_ORDER_NOTIFICATION,
		DELIVERY_SCHEDULE_CHANGE_NOTIFICATION
	}*/
	
	//private static Logger logger = LoggerFactory.getLogger(ParsePushWrapper.class);

	public void sendUndeliveredOrderNotification(DeliverySchedule deliverySchedule){
		try{
			String alert = "Undelivered Order.";
			String title = "Provilac";
			String notificationType = NotificationType.UNDELIVERED_ORDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("deliveryScheduleCode", deliverySchedule.getCode());
			jsonObject.put("actorMobileNumber", deliverySchedule.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, deliverySchedule.getCustomer().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void sendBouncedPaymentNotification(Payment payment){
		try{
			String alert = "Your Cheque is Bounced.";
			String title = "Provilac";
			String notificationType = NotificationType.BOUNCED_PAYMENT.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("paymentCode", payment.getCode());
			jsonObject.put("actorMobileNumber", payment.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, payment.getCustomer().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void sendPaymentReceivedNotification(Payment payment){
		try{
			String alert = "Payment Received.";
			String alert1 = "Thank you for the Payment. Your Check has been Received. We will update you once Check gets clear.";
			String title = "Provilac";
			String notificationType = NotificationType.PAYMENT_RECEIVED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			if (payment.getPaymentMethod() == PaymentMethod.Cheque) {
				jsonObject.put("alert", alert1);	
			} else {
				jsonObject.put("alert", alert);
			}
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("paymentCode", payment.getCode());
			jsonObject.put("actorMobileNumber", payment.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, payment.getCustomer().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendInvoiceGenerationNotification(Invoice invoice){
		try{
			String alert = "Dear customer, your invoice for previous month has generated.";
			String title = "Provilac";
			String notificationType = NotificationType.INVOICE_GENERATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("actorMobileNumber", invoice.getCustomer().getMobileNumber());
			jsonObject.put("sound", "Default");
			jsonObject.put("invoiceCode", invoice.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, invoice.getCustomer().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void sendRouteSelectionByCollectionBoyNotification(User user,Route route){
		try{
			String alert = route.getName()+" is selected for collecting Payment By "+user.getFullName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTE_SELECTION_FOR_COLLECTION_BOY.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("actorMobileNumber", user.getMobileNumber());
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, user.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendRouteRecordCreateNotification(User user,Route route){
		try{
			String alert = "Dear Customer, you are added in "+route.getName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTERECORD_CREATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("actorMobileNumber", user.getMobileNumber());
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, user.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendRouteRecordChangeNotification(User user,Route route){
		try{
			String alert = "Dear Customer, your route is changed to : "+route.getName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTERECORD_UPDATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("actorMobileNumber", user.getMobileNumber());
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, user.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendSubscriptionAddedNotification(UserSubscription subscription,Date endDate){
		try{
			String alert = "Dear customer,you have subscribed provilac milk subscription from : "+subscription.getStartDate()+" to "+ endDate ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_ADDED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", subscription.getUser().getMobileNumber());
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscription.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendDeliveryScheduleChangeNotification(DeliverySchedule deliverySchedule){
		try{
			String alert = "Dear customer, your Delivery Schedule has been Change : "+"Date:-"+deliverySchedule.getDate();
			String title = "Provilac";
			String notificationType = NotificationType.DELIVERY_SCHEDULE_CHANGE_NOTIFICATION.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", deliverySchedule.getCustomer().getMobileNumber());
			jsonObject.put("deliveryScheduleCode", deliverySchedule.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, deliverySchedule.getCustomer().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendSubscriptionUpdatedNotification(UserSubscription subscription,Date endDate){
		try{
			String alert = "Dear customer, your subscription has been edited from : "+subscription.getStartDate()+" to "+ endDate ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_UPDATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", subscription.getUser().getMobileNumber());
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscription.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void sendSubscriptionPutOnHoldNotification(UserSubscription subscription, String startDate, String endDate){
		try{
			String alert = "Dear customer, your subscription has been put on hold." ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_HOLD.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("startDate", startDate);
			jsonObject.put("endDate", endDate);
			jsonObject.put("actorMobileNumber", subscription.getUser().getMobileNumber());
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscription.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendSubscriptionStopNotification(UserSubscription subscription){
		try{
			String alert = "Thank you to be a part of Provilac family. Sorry to see you go. Your total outstanding till date is: Rs."+subscription.getUser().getLastPendingDues() ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_STOP.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", subscription.getUser().getMobileNumber());
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscription.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendOnTheWayCollectionNotification(User collectionBoy, List<String> mobileNumbers){
		try{

				for (String mobileNumber : mobileNumbers) {
					String alert = "Dear Customer, collection boy : "+ collectionBoy.getFirstName() + " " + collectionBoy.getLastName() + " is on the way to collect payment amount.";
					String title = "Provilac";
					String notificationType = NotificationType.ON_THE_WAY_TO_COLLECTION.name();
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("alert", alert);
					jsonObject.put("title", title);
					jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
					jsonObject.put("sound", "Default");
					jsonObject.put("actorMobileNumber", mobileNumber);
					jsonObject.put("notificationType", notificationType);
					sendFirebaseRequest(jsonObject, mobileNumber);
				}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendReferralBalanceAddedNotification(User customer){
		try{
				
				String alert = "Dear Customer, you have got referral points";
				String title = "Provilac";
				String notificationType = NotificationType.REFERRAL_BALANCE_ADDED.name();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("alert", alert);
				jsonObject.put("title", title);
				jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
				jsonObject.put("sound", "Default");
				jsonObject.put("actorMobileNumber", customer.getMobileNumber());
				jsonObject.put("notificationType", notificationType);
				sendFirebaseRequest(jsonObject,customer.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendTrialDayRememberNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk trial delivery is arriving today.";
			String title = "Provilac";
			String notificationType = NotificationType.TODAY_TRIAL_REMINDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", user.getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, user.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendOneDayBeforeTrialDayRememberNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk trial delivery is arriving tomorrow.";
			String title = "Provilac";
			String notificationType = NotificationType.ONE_DAY_BEFORE_TRIAL_REMINDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", user.getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, user.getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public void sendOneTimeOrderNotification(OneTimeOrder oneTimeOrder){
		try{
			String alert = "Dear Customer, Your provilac milk one time order is placed succesfully.";
			String title = "Provilac";
			String notificationType= NotificationType.ONE_TIME_ORDER_NOTIFICATION.name();
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("oneTimeOrderCode", oneTimeOrder.getCode());
			jsonObject.put("actorMobileNumber", oneTimeOrder.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, oneTimeOrder.getCustomer().getMobileNumber());	
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendOneTimeOrderDeliveredNotification(OneTimeOrder oneTimeOrder){
		try{
			String alert = "Dear Customer, Your provilac milk one time order delivered successfully.";
			String title = "Provilac";
			String notificationType = NotificationType.DELIVERED_ORDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("oneTimeOrderCode", oneTimeOrder.getCode());
			jsonObject.put("actorMobileNumber", oneTimeOrder.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, oneTimeOrder.getCustomer().getMobileNumber());	
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendOneTimeOrderOnDayNotification(OneTimeOrder oneTimeOrder){
		try{
			String alert = "Dear Customer, Your provilac one time order delivery is Today.";
			String title = "Provilac";
			String notificationType = NotificationType.ONE_TIME_ORDER_NOTIFICATION.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("oneTimeOrderCode", oneTimeOrder.getCode());
			jsonObject.put("actorMobileNumber", oneTimeOrder.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, oneTimeOrder.getCustomer().getMobileNumber());	
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendCollectionBoyAmountDepositeNotification(Deposit deposit){
		try{
			String alert = "Thank you for deposit, please submit cheque receipts to the accountant.";
			String title = "Provilac";
			String notificationType = NotificationType.COLLECTION_BOY_AMOUNT_DEPOSITE.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("depositCode", deposit.getCode());
			jsonObject.put("actorMobileNumber", deposit.getCollectionBoy().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, deposit.getCollectionBoy().getMobileNumber());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendCollectionBoyOutstandingAmountNotification(User collectionBoy){
		try{
			String alert = "Please deposit collected amount to the nearest bank.";
			String title = "Provilac";
			String notificationType = NotificationType.COLLECTION_BOY_OUTSTANDING_AMOUNT.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("collectionBoyCode", collectionBoy.getCode());
			jsonObject.put("actorMobileNumber", collectionBoy.getMobileNumber());
			jsonObject.put(notificationType, notificationType);
			sendFirebaseRequest(jsonObject, collectionBoy.getMobileNumber());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendDeliveryBoyRemainderNotification(User deliveryBoy){
		try{
			String alert = "Your delivery sheet is ready, please collect milk from provilac office and start delivery.";
			String title = "Provilac";
			String notificationType = NotificationType.DELIVERY_BOY_REMINDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("deliveryCode", deliveryBoy.getCode());
			jsonObject.put("actorMobileNumber", deliveryBoy.getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, deliveryBoy.getMobileNumber());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendPaymentNotReceiveNotification(Invoice invoice){
		try{
			String name = invoice.getCustomer().getFirstName() + (null != invoice.getCustomer().getLastName() ? " " + invoice.getCustomer().getLastName() : "" );
			String alert = "Hello " + name + " your payment has not been received beacause of you were " + invoice.getReason() + ".";
			
			if (invoice.getReason() != null && invoice.getReason().toLowerCase().contains("reschedule")) {
				String str=(CharMatcher.DIGIT).retainFrom(invoice.getReason());
				Calendar today=Calendar.getInstance();
				
				if(StringUtils.isNotBlank(str))
				today.add(Calendar.DAY_OF_YEAR, Integer.parseInt(str));
				
				alert = "Hello " + name + " your collection has been reschedule for " + DateHelper.getFormattedDateSimple(today.getTime()) + "." ;
			}
			
			String title = "Provilac";
			String notificationType = NotificationType.PAYMENT_NOT_RECEIVED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("reason", invoice.getReason());
			jsonObject.put("invoiceCode", invoice.getCode());
			jsonObject.put("customerCode", invoice.getCustomer().getCode());
			jsonObject.put("actorMobileNumber", invoice.getCustomer().getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, invoice.getCustomer().getMobileNumber());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendNotificationFromWeb(User customer,String title,String message,Long id) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("actorName", customer.getFirstName() + customer.getLastName());
			jsonObject.put("actorMobileNumber", customer.getMobileNumber());
			jsonObject.put("alert", message);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			String notificationType = NotificationType.MISC.name();
			jsonObject.put("notificationType", notificationType);
			if(null !=id){
				jsonObject.put("notificationImage","/restapi/getNotificationPicture?id=" + id);
			}else{
				jsonObject.put("notificationImage","null");
			}
			
			jsonObject.put("customerCode", customer.getCode());
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		sendFirebaseRequest(jsonObject, customer.getMobileNumber());
	}
	
	public void sendNotificationForDoLogout(User customer){
		try {
			String alert = "Verification faield login again";
			String title = "Provilac";
			String notificationType = NotificationType.DO_LOGOUT.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", customer.getMobileNumber());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, customer.getMobileNumber());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendNewSubscriptionRequestAddedNotification(SubscriptionRequest subscriptionRequest){
		try{
			String alert = "We’ve received your subscription request. We will inform you once it gets approved.";
			String title = "Provilac";
			String notificationType = NotificationType.NEW_SUBSCRIPTION_REQUEST_PLACED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", subscriptionRequest.getUser().getMobileNumber());
			jsonObject.put("subscriptionRequestCode", subscriptionRequest.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscriptionRequest.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void sendChangeSubscriptionRequestAddedNotification(SubscriptionRequest subscriptionRequest){
		try{
			String alert = "We’ve received your request for change in subscription. We will inform you once it gets approved from our customer care team.";
			String title = "Provilac";
			String notificationType = NotificationType.CHANGE_SUBSCRIPTION_REQUEST_PLACED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", subscriptionRequest.getUser().getMobileNumber());
			jsonObject.put("subscriptionRequestCode", subscriptionRequest.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, subscriptionRequest.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void sendSubscriptionRequestApproveNotification(UserSubscription userSubscription){
		try{
			String alert = "Hello, Your subscription request has been approved. Please check calendar section to manage your milk deliveries.";
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_REQUEST_APPROVED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", userSubscription.getUser().getMobileNumber());
			jsonObject.put("subscriptionRequestCode", userSubscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, userSubscription.getUser().getMobileNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private void sendParseRequest(JSONObject dataObject, String... channels) {
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("data", dataObject);
			JSONArray channelArray = new JSONArray();
			for (int i = 0; i < channels.length; i++) {
				channelArray.put(channels[i]);
			}
			jsonObject.put("channels", channelArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(PARSE_URL);
		
		httpPost.setHeader("X-Parse-Application-Id", APP_ID);
		httpPost.setHeader("X-Parse-REST-API-Key", REST_API_KEY);
		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		
		StringEntity stringEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
		httpPost.setEntity(stringEntity);
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpPost);
			logger.debug("================== Parse Response Code ================== " + httpResponse.getStatusLine().getStatusCode());
			String response = IOUtils.toString(httpResponse.getEntity().getContent());
			logger.debug("================== Parse Response ================== " + response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendOTP(){
		try{
			String alert = "Hello, Your subscription request has been approved. Please check calendar section to manage your milk deliveries.";
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_REQUEST_APPROVED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("dateTime", DateHelper.getFormattedTimestamp(new Date()));
			jsonObject.put("sound", "Default");
			jsonObject.put("actorMobileNumber", "7411284260");
			jsonObject.put("subscriptionRequestCode", "123");
			jsonObject.put("notificationType", notificationType);
			sendFirebaseRequest(jsonObject, "7411284260");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
