/**
 * 
 */
package com.vishwakarma.provilac.utils;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.vishwakarma.provilac.api.ApiResponse;
import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.SummarySheet;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryLineItemService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.DeliverySheetService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.OneTimeOrderService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.ProductService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionLineItemService;
import com.vishwakarma.provilac.service.SummarySheetRecordService;
import com.vishwakarma.provilac.service.SummarySheetService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants.Day;

/**
 * @author Vishal
 *
 */
@Component
public class AsyncJobs {

	private final static Logger log = LoggerFactory.getLogger(AsyncJobs.class);
	
	@Resource
	private ProductService productService;
	
	@Resource
	private DeliveryScheduleService  deliveryScheduleService;
	
	@Resource
	private SubscriptionLineItemService subscriptionLineItemService;
	
	@Resource
	private UserService userService;
	
	@Resource 
	private RouteService routeService;
	
	@Resource
	private PaymentService paymentService;
 	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private DeliveryLineItemService deliveryLineItemService;
	
	@Resource
	private UserSubscriptionService userSubscriptionService;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService; 
	
	@Resource
	private CityService cityService;
	
	@Resource
	private AddressService addressService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private NotificationHelper  notificationHelper;
	
	@Resource
	private DeliverySheetService deliverySheetService;
	
	@Resource
	private UserRouteService userRouteService;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Resource
	private OneTimeOrderService oneTimeOrderService;
	
	@Resource
	private SummarySheetService summarySheetService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource
	private SummarySheetRecordService summarySheetRecordService;
	
	@Resource
	FirebaseWrapper firebaseWrapper;
	
	@Async
	public void createFirstTimePostpaidDeliverySchedule(UserSubscription userSubscription, Set<SubscriptionLineItem> subscriptionLineItems, Date date, User loggedInUser){
		
		if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
			return;
		}
		
		Date startDate = userSubscription.getStartDate();
		DateHelper.setToStartOfDay(startDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.DATE, 60);
		Date endDate = calendar.getTime();
		DateHelper.setToEndOfDay(endDate);
		
		DeliverySchedule deliverySchedule = null;
		List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(userSubscription.getUser());
		if(routeRecords.isEmpty()){
			return;
		}
		
		RouteRecord routeRecord = routeRecords.get(0);
		
		for (;startDate.before(endDate);) {
			
			deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(userSubscription.getUser().getCode(), startDate, false, loggedInUser, true, true);
			if (deliverySchedule != null) {
				deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
			}
			deliverySchedule = new DeliverySchedule();
			deliverySchedule.setCustomer(userSubscription.getUser());
			deliverySchedule.setStatus(null);
			deliverySchedule.setType(Type.Normal);
			deliverySchedule.setDate(startDate);
			deliverySchedule.setDeliveryTime(startDate);
			deliverySchedule.setRoute(routeRecord.getRoute());
			if(null!=userSubscription.getPermanantNote()){
				deliverySchedule.setPermanantNote(userSubscription.getPermanantNote());
			}
			//TODO: IMPORTANT!! Need to update priority for other schedules?
			
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(deliverySchedule.getRoute().getId(), startDate, routeRecord.getPriority(), null, false, false);

			if(!deliverySchedules.isEmpty()){
				List<DeliverySchedule> schedules=  new ArrayList<DeliverySchedule>();
				schedules= deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), startDate, routeRecord.getPriority(), null, false, false);
				if(schedules.size()!=0){
					for (DeliverySchedule deliverySchedule2 : schedules) {
						deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
						deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
					}
				 }
			}
			 
			deliverySchedule.setPriority(routeRecord.getPriority());
				 
			for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
				
				if(subscriptionLineItem.getType()==SubscriptionType.Daily){
					if (subscriptionLineItem.getQuantity() == 0) {
						continue;
					}
					DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
					deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
					
					deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
					deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * subscriptionLineItem.getQuantity());
					
					deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
					deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
				} else if( subscriptionLineItem.getType()== SubscriptionType.Weekly){
					if (subscriptionLineItem.getQuantity() == 0) {
						continue;
					}
					int dayOfWeek = DateHelper.getDateField(startDate, Calendar.DAY_OF_WEEK);
					Day currentDay = Day.values()[dayOfWeek - 1];
					if(subscriptionLineItem.getDay() == currentDay && subscriptionLineItem.getQuantity() > 0) {
						DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
						deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
						
						deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
						deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * subscriptionLineItem.getQuantity());
						
						deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
						deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
					}
				} else{
					if (subscriptionLineItem.getCustomPattern() == null) {
						continue;
					}
					String[] patternArray = subscriptionLineItem.getCustomPattern().split("-");
					int currentPatternIndex = subscriptionLineItem.getCurrentIndex();

					if(currentPatternIndex >= patternArray.length) {
						currentPatternIndex = 0;	
					}
					if(StringUtils.isNotEmpty(patternArray[currentPatternIndex])){
						int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
						if(quantity > 0) {
							DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
							deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
							
							deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
							deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * quantity);
							
							deliveryLineItem.setQuantity(quantity);
							deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
						}
					}
					subscriptionLineItem.setCurrentIndex(currentPatternIndex + 1);
				}
			}
			
			if(!deliverySchedule.getDeliveryLineItems().isEmpty())  {
				deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
				deliveryScheduleService.createDeliverySchedule(deliverySchedule, loggedInUser, true, true);
			}
				
			startDate = DateHelper.addDays(startDate, 1);
		}
		
		// Update updated indices for all the subscription lineitems
		for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
			subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, loggedInUser, false, false);
		}
		
		firebaseWrapper.sendSubscriptionRequestApproveNotification(userSubscription);
	}

	@Async
	public void createFirstTimePrepayDeliverySchedules(UserSubscription userSubscription, Set<SubscriptionLineItem> subscriptionLineItems, User loggedInUser){
		
		if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid) {
			return;
		}
		
		Date startDate = userSubscription.getStartDate();
		Date endDate = userSubscription.getEndDate();
		DateHelper.setToStartOfDay(startDate);
		DateHelper.setToEndOfDay(endDate);
		
		User user = userSubscription.getUser();
		List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(user);
		if(routeRecords.isEmpty()){
			return;
		}

		RouteRecord routeRecord = routeRecords.get(0);
		List<DeliverySchedule> existingSchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(user.getId(), startDate, loggedInUser, false, true);
		for (DeliverySchedule deliverySchedule : existingSchedules) {
			deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
		}
		Map<Long, Double> productPricing = userSubscription.getProductPricing();
		
		for (;startDate.before(endDate);) {
			
			DeliverySchedule deliverySchedule  = new DeliverySchedule();
			deliverySchedule.setCustomer(user);
			deliverySchedule.setStatus(null);
			deliverySchedule.setType(Type.Normal);
			deliverySchedule.setDate(startDate);
			deliverySchedule.setRoute(routeRecord.getRoute());
			if(null!=userSubscription.getPermanantNote()){
				deliverySchedule.setPermanantNote(userSubscription.getPermanantNote());
			}
			deliverySchedule.setPriority(routeRecord.getPriority());
			
			for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
				
				Product product = subscriptionLineItem.getProduct();
				DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
				deliveryLineItem.setProduct(product);
				deliveryLineItem.setPricePerUnit(product.getPrice());
				double lineItemCost = 0;
				
				if(subscriptionLineItem.getType()==SubscriptionType.Daily){
					if (subscriptionLineItem.getQuantity() == 0) {
						continue;
					}
					
					deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
					if(productPricing.containsKey(product.getId())) {
						lineItemCost = productPricing.get(product.getId()) * subscriptionLineItem.getQuantity();
					} else {
						lineItemCost = product.getPrice() * subscriptionLineItem.getQuantity();
					}

					deliveryLineItem.setTotalPrice(lineItemCost);
					deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
				} else if( subscriptionLineItem.getType()== SubscriptionType.Weekly){
					if (subscriptionLineItem.getQuantity() == 0) {
						continue;
					}
					int dayOfWeek = DateHelper.getDateField(startDate, Calendar.DAY_OF_WEEK);
					Day currentDay = Day.values()[dayOfWeek - 1];
					if(subscriptionLineItem.getDay() == currentDay && subscriptionLineItem.getQuantity() > 0) {
						
						if(productPricing.containsKey(product.getId())) {
							lineItemCost = productPricing.get(product.getId()) * subscriptionLineItem.getQuantity();
						} else {
							lineItemCost = product.getPrice() * subscriptionLineItem.getQuantity();
						}
						deliveryLineItem.setTotalPrice(lineItemCost);
						
						deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
						deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
					}
				} else{
					if (subscriptionLineItem.getCustomPattern() == null) {
						continue;
					}
					String[] patternArray = subscriptionLineItem.getCustomPattern().split("-");
					int currentPatternIndex = subscriptionLineItem.getCurrentIndex();

					if(currentPatternIndex >= patternArray.length) {
						currentPatternIndex = 0;	
					}
					if(StringUtils.isNotEmpty(patternArray[currentPatternIndex])){
						int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
						if(quantity > 0) {
							
							deliveryLineItem.setQuantity(quantity);
							if(productPricing.containsKey(product.getId())) {
								lineItemCost = productPricing.get(product.getId()) * quantity;
							} else {
								lineItemCost = product.getPrice() * quantity;
							}

							deliveryLineItem.setTotalPrice(lineItemCost);
							deliverySchedule.getDeliveryLineItems().add(deliveryLineItem);
						}
					}
					subscriptionLineItem.setCurrentIndex(currentPatternIndex + 1);
				}
			}
			
				
			//TODO: IMPORTANT!! Need to update priority for other schedules?
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getScheduleByRouteAndDateAndPriority(deliverySchedule.getRoute().getId(), startDate, routeRecord.getPriority(), null, false, false);
			if(!deliverySchedules.isEmpty()){
				List<DeliverySchedule> schedules=  new ArrayList<DeliverySchedule>();
				schedules= deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), startDate, routeRecord.getPriority(), null, false, false);
				if(schedules.size()!=0){
					for (DeliverySchedule deliverySchedule2 : schedules) {
						deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
						deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
					}
				 }
			}
			
			deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
			deliveryScheduleService.createDeliverySchedule(deliverySchedule, loggedInUser, true, true);
				
			startDate = DateHelper.addDays(startDate, 1);
		}
		
		// Update updated indices for all the subscription lineitems
		for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
			subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, loggedInUser, false, false);
		}
		
		firebaseWrapper.sendSubscriptionRequestApproveNotification(userSubscription);
		createExcessivePrepayDeliveries(user, loggedInUser, null);
	}
	
	@Async
	public void createExcessivePrepayDeliveries(User user, User loggedInUser, Date fromDate) {
		
		double availableBalance = user.getLastPendingDues();
		if(availableBalance <= 0) {
			return;
		}
		
		List<DeliverySchedule> deliverySchedulesToCreate = new ArrayList<DeliverySchedule>();
		
		List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(user);
		if(routeRecords.isEmpty()){
			return;
		}
		RouteRecord routeRecord = routeRecords.get(0);
		
		List<UserSubscription> subscriptions = userSubscriptionService.getUserSubscriptionsByCustomerAndIsCurrent(user.getCode(), true, loggedInUser, false, true);
		if(subscriptions.get(0).getSubscriptionType() != com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
			return;
		}
		UserSubscription activeSubscription = subscriptions.get(0);
		Set<SubscriptionLineItem> subscriptionLineItems = activeSubscription.getSubscriptionLineItems();
		
		//Get the last date of delivery && find the next earliest date to deliver
		DeliverySchedule lastDeliverySchedule = deliveryScheduleService.getLastDeliverySchedule(user.getCode(), false, loggedInUser, false, false);
		Date lastDeliveryDate;
		if(lastDeliverySchedule != null) {
			lastDeliveryDate = lastDeliverySchedule.getDate();
		} else {
			lastDeliveryDate = new Date();
		}
		Date today = new Date();
		DateHelper.setToEndOfDay(today);
		if(fromDate == null) {
			//Normal scenario, where user changed a delivery for a date(s)
			if(lastDeliveryDate.before(today)) {
				lastDeliveryDate = today;
			}
			fromDate = DateHelper.addDays(lastDeliveryDate, 1);
			DateHelper.setToStartOfDay(fromDate);
		} else {
			DateHelper.setToStartOfDay(fromDate);
			if(fromDate.compareTo(lastDeliveryDate) <= 0) { //from date before or equal
				//Scenario where user puts particular in-between date window on hold
				fromDate = DateHelper.addDays(lastDeliveryDate, 1);
			} else {
				//Customer has put on hold deliveries for a time period beyond the last date of delivery
			}
		}
		Date deliveryDate = fromDate;
		Map<Long, Double> productPricing =  activeSubscription.getProductPricing();
		
		while (availableBalance > 0) {

			double deliveryScheduleCost = 0;
			List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>();
			for(SubscriptionLineItem lineItem : subscriptionLineItems) {
				
				DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
				Product product = lineItem.getProduct();
				deliveryLineItem.setProduct(product);
				deliveryLineItem.setPricePerUnit(product.getPrice());
				
				double lineItemCost = 0;
				
				if(lineItem.getType() == SubscriptionType.Daily && lineItem.getQuantity() > 0) {
					deliveryLineItem.setQuantity(lineItem.getQuantity());
					if(productPricing.containsKey(product.getId())) {
						lineItemCost = productPricing.get(product.getId()) * lineItem.getQuantity();
					} else {
						lineItemCost = product.getPrice() * lineItem.getQuantity();
					}
					
					deliveryLineItem.setTotalPrice(lineItemCost);
					deliveryLineItems.add(deliveryLineItem);
				} else if(lineItem.getType() == SubscriptionType.Weekly && lineItem.getQuantity() > 0) {
					
					int dayOfWeek = DateHelper.getDateField(deliveryDate, Calendar.DAY_OF_WEEK);
					Day currentDay = Day.values()[dayOfWeek - 1];
					if(currentDay == lineItem.getDay()) {
						deliveryLineItem.setQuantity(lineItem.getQuantity());
						if(productPricing.containsKey(product.getId())) {
							lineItemCost = productPricing.get(product.getId()) * lineItem.getQuantity();
						} else {
							lineItemCost = product.getPrice() * lineItem.getQuantity();
						}
						
						deliveryLineItem.setTotalPrice(lineItemCost);
						deliveryLineItems.add(deliveryLineItem);
					}
				} else {
					
					String[] patternArray = lineItem.getCustomPattern().split("-");
					int currentPatternIndex = lineItem.getCurrentIndex();

					if(currentPatternIndex >= patternArray.length) {
						currentPatternIndex = 0;	
					}
					
					int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
					if(quantity > 0) {
						deliveryLineItem.setQuantity(quantity);
						if(productPricing.containsKey(product.getId())) {
							lineItemCost = productPricing.get(product.getId()) * quantity;
						} else {
							lineItemCost = product.getPrice() * quantity;
						}
						
						deliveryLineItem.setTotalPrice(lineItemCost);
						deliveryLineItems.add(deliveryLineItem);
					}
				}
				deliveryScheduleCost = deliveryScheduleCost + lineItemCost;
			}
			
			if(availableBalance < deliveryScheduleCost) {
				break;
			}
			
			DeliverySchedule deliverySchedule = new DeliverySchedule();
			deliverySchedule = new DeliverySchedule();
			deliverySchedule.setCustomer(user);
			deliverySchedule.setDate(deliveryDate);
			deliverySchedule.getDeliveryLineItems().addAll(deliveryLineItems);
			deliverySchedule.setRoute(routeRecord.getRoute());
			deliverySchedule.setPriority(routeRecord.getPriority());
			if(null!=activeSubscription.getPermanantNote()){
				deliverySchedule.setPermanantNote(activeSubscription.getPermanantNote());
			}
			deliverySchedule.setType(Type.Normal);
			deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
			deliverySchedulesToCreate.add(deliverySchedule);
			
			availableBalance = availableBalance - deliveryScheduleCost;
			deliveryDate = DateHelper.addDays(deliveryDate, 1);
		}
		
		for (DeliverySchedule deliverySchedule : deliverySchedulesToCreate) {
			deliveryScheduleService.createDeliverySchedule(deliverySchedule, loggedInUser, false, false);
		}
		
		//Update the indices
		for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
			if(subscriptionLineItem.getType() == SubscriptionType.Custom) {
				subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, null, false, false);
			}
		}
		
		user.setLastPendingDues(availableBalance);
		userService.updateUser(user.getId(), user, loggedInUser, false, false);
	}
	
	@Async
	public void genrateSummarySheet(Date fromDate,Date toDate) {
		try{
			
			List<Invoice> invoices = invoiceService.getInvoiceByDateRange(fromDate, toDate, null, false, false);
			SummarySheet summarySheet = new SummarySheet();
			summarySheet.setFromDate(fromDate);
			summarySheet.setToDate(toDate);
			summarySheetService.createSummarySheet(summarySheet, null, false, true);
			Set<SummarySheetRecord> listofSheetRecords = new HashSet<SummarySheetRecord>();
			
			for (Invoice invoice : invoices) {
				SummarySheetRecord summarySheetRecord = new SummarySheetRecord();
				summarySheetRecord.setCustomer(invoice.getCustomer());
				summarySheetRecord.setInvoice(invoice);
				summarySheetRecord.setTotalAmount(invoice.getTotalAmount());
				summarySheetRecord.setSummarySheet(summarySheet);
				summarySheetRecord =  summarySheetRecordService.createSummarySheetRecord(summarySheetRecord, null, false, true);
				listofSheetRecords.add(summarySheetRecord);
			}
			summarySheet.setSummarySheetRecords(listofSheetRecords);
			summarySheetService.updateSummarySheet(summarySheet.getId(), summarySheet, null, false, false);
		} catch(ProvilacException e){
			e.printStackTrace();
		}
	}
	
	@Async
	public void updateDeliveryScheduleRoute(RouteRecord routeRecord,User loggedInUser) {
		
		Date date = new Date();
		DateHelper.setToStartOfDay(date);
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveredDeliverySchedulesByCustomerAndDate(routeRecord.getCustomer().getId(), date, null, false, false);
		List<DeliverySchedule> schedules1 = new ArrayList<DeliverySchedule>(); 
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			
			List<DeliverySchedule> schedules = new ArrayList<DeliverySchedule>();
			List<DeliverySchedule>schedules3 = deliveryScheduleService.getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule.getPriority()+1, null, false, false);
			if(schedules3.size()!=0){
				for (DeliverySchedule deliverySchedule2 : schedules3) {
					if(!deliverySchedule.getCode().equals(deliverySchedule2.getCode()) && !(deliverySchedule2.getPriority()<=1)){
						deliverySchedule2.setPriority(deliverySchedule2.getPriority()-1);
						schedules.add(deliverySchedule2);
						//deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, loggedInUser, false, false);
					}

				}
			}
			//TODO: Call update method
			deliveryScheduleService.createDeliverySchedules(schedules, false, null, false, false);
			
			deliverySchedule.setRoute(routeRecord.getRoute());
			deliverySchedule.setPriority(routeRecord.getPriority());
			List<DeliverySchedule>deliverySchedules2 = new ArrayList<DeliverySchedule>();
			List<DeliverySchedule>schedules2 = deliveryScheduleService.getScheduleByRouteAndDate(routeRecord.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule.getPriority(), null, false, false);
			if(schedules2.size()!=0){
				for (DeliverySchedule deliverySchedule2 : schedules2) {
					if(!deliverySchedule.getCode().equals(deliverySchedule2.getCode())){
						deliverySchedule2.setPriority(deliverySchedule2.getPriority()+1);
						deliverySchedules2.add(deliverySchedule2);
						//deliveryScheduleService.updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, loggedInUser, false, false);
					}
				}
			}
			//TODO: Call update method
			deliveryScheduleService.createDeliverySchedules(deliverySchedules2, false, null, false, false);
			schedules1.add(deliverySchedule);
			deliverySchedule = deliveryScheduleService.updateDeliverySchedule(deliverySchedule.getId(), deliverySchedule, null, false, false);
		}
		//deliveryScheduleService.createDeliverySchedules(schedules1, null, false, false);
	}
	
	@Async
	public void deletePostpaidDeliverySchedules(User customer,Date fromDate,Date toDate){
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(customer.getId(), fromDate, toDate, null, false, false, UserSubscription.SubscriptionType.Postpaid);
		for(DeliverySchedule deliverySchedule:deliverySchedules) {
			deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
		}
	}
	
	@Async
	public void deleteAllDeliverySchedulesFromDate(User customer, Date fromDate) {
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getAllDeliverySchedulesForCustomerFromDate(customer.getId(), fromDate, null, false, false);
		for(DeliverySchedule deliverySchedule:deliverySchedules) {
			deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
		}
	}
	
	@Async
	public void deleteDeliverySchedulesInRange(String customerCode,String[] dates){
		for (String date : dates) {
			DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(customerCode, DateHelper.parseDate(date), false, null, false,true);
			if (deliverySchedule != null) {
				deliveryScheduleService.deleteDeliverySchedule(deliverySchedule.getId());
			}
		}
	}
	
	@Async
	public void addDeletedRouteRecordForStopCustomer(User customer,Route route){
		
		List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordService.getAllDeletedRouteRecordsByCustomer(customer);
		DeletedRouteRecord deletedRouteRecord = null;
		if(deletedRouteRecords.isEmpty()){
			deletedRouteRecord = new DeletedRouteRecord();
			deletedRouteRecord.setRoute(route);
			deletedRouteRecord.setCustomer(customer);
		}else{
			deletedRouteRecord = deletedRouteRecords.get(0);
			deletedRouteRecord.setRoute(route);
			deletedRouteRecord.setCustomer(customer);
		}
		deletedRouteRecordService.createDeletedRouteRecordForChangeRoute(deletedRouteRecord, null, false, false);
	}

	@Async
	public void sendMessageToCustomers(Boolean sms, Boolean push, Boolean email, String title, String message, String[] customers, Long id, InputStream picIpStream, String templateName) {
		
		if(sms) {
			for(String customer:customers) {
				User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
				plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
			}
		}
		
		if(push) {
			for(String customer:customers) {
				User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
				firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
			}
		}
		
		if(email) {
			
			if(StringUtils.isNotBlank(templateName)) {
				
				List<String> mailIds = new ArrayList<String>();
				for(String customer:customers) {
					User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
					if(null != user.getEmail()) {
						mailIds.add(user.getEmail());
					}
				}
				emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
				
			} else {
				for(String customer:customers) {
					User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
					if(null != user.getEmail()) {
						emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
					}
				}
			}
		}
	}
	
	@Async
	public void sendMessageToAllRoutes(Boolean sms, Boolean push, Boolean email, String title, String message, String route, String status, String customer, Long id, InputStream picIpStream, String templateName) {
		
		List<User> users = userService.getUsersForRole(Role.ROLE_CUSTOMER, null, false, false);
		
		if(sms) {
			for(User user : users) {
				if(!user.getMobileNumber().startsWith("+")) {
					plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
				}
			}
		}
		
		if(push) {
			for(User user : users) {
				firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
			}
		}
		
		if(email) {
			
			if(StringUtils.isNotBlank(templateName)) {
				
				List<String> mailIds = new ArrayList<String>();
				for(User user : users) {
					if(StringUtils.isNotBlank(user.getEmail())) {
						mailIds.add(user.getEmail());
					}
				}
				emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
			} else {
			
				for (User user : users) {
					if(StringUtils.isNotBlank(user.getEmail())) {
						emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
					}
				}
			}
		}
	}
	
	@Async
	public void sendMessageByRoute(Boolean sms, Boolean push, Boolean email, String title, String message, String route, String status, String customer, Long id, InputStream picIpStream, String templateName) {
		
		if(sms) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
				}
			}
		}
		
		if(push) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
				}
			}
		}
		
		if(email) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				
				if(StringUtils.isNotBlank(templateName)) {
					
					List<String> mailIds = new ArrayList<String>();
					for(RouteRecord routeRecord:routeRecords) {
						User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
						if(null != user.getEmail()) {
							mailIds.add(user.getEmail());
						}
					}
					emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
				} else {
					
					for(RouteRecord routeRecord:routeRecords) {
						User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
						if(null != user.getEmail()) {
							emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
						}
					}
				}
			}
		}
	}
	
	@Async
	public void sendMessageByRouteAndStatus(Boolean sms, Boolean push, Boolean email, String title, String message, String route, String status, String customer, Long id, InputStream picIpStream, String templateName) {
		
		if(sms) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					if(user.getStatus().name().equalsIgnoreCase(status)) {
						plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
					}
				}
			}
		}
		
		if(push) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					if(user.getStatus().name().equalsIgnoreCase(status)) {
						firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
					}
				}
			}
		}
		
		if(email) {
			Route route2=routeService.getRouteByCode(route, false, null, false, false);
			if(null !=route2) {
				List<RouteRecord> routeRecords=routeRecordService.getRouteRecordsByRouteCode(route2.getCode(), null, false, false);
				
				if(StringUtils.isNotBlank(templateName)) {
					
					List<String> mailIds = new ArrayList<String>();
					for(RouteRecord routeRecord:routeRecords) {
						User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
						if(null != user.getEmail()) {
							if(user.getStatus().name().equalsIgnoreCase(status)){
								mailIds.add(user.getEmail());
							}
						}
					}
					emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
				} else {
					
					for(RouteRecord routeRecord:routeRecords) {
						User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
						if(null != user.getEmail()) {
							if(user.getStatus().name().equalsIgnoreCase(status)){
								emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
							}
						}
					}
				}
				
			}
		}
	}
	
	@Async
	public void sendMessageToAllRoutesWithStatus(Boolean sms, Boolean push, Boolean email, String title, String message, String route, String status, String customer, Long id, InputStream picIpStream, String templateName) {
		
		if(sms) {
			List<RouteRecord> routeRecords=routeRecordService.getAllRouteRecords(null, false, false);
			for(RouteRecord routeRecord:routeRecords) {
				User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
				if(user.getStatus().name().equalsIgnoreCase(status)) {
					plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
				}
			}
		}
		
		if(push){
			List<RouteRecord> routeRecords=routeRecordService.getAllRouteRecords(null, false, false);
			for(RouteRecord routeRecord:routeRecords) {
				User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
				if(user.getStatus().name().equalsIgnoreCase(status)) {
					firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
				}
			}
		}
		
		if(email){
			List<RouteRecord> routeRecords=routeRecordService.getAllRouteRecords(null, false, false);
			
			if(StringUtils.isNotBlank(templateName)) {

				List<String> mailIds = new ArrayList<String>();
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					if(null != user.getEmail()) {
						if(user.getStatus().name().equalsIgnoreCase(status)) {
							mailIds.add(user.getEmail());
						}
					}
				}
				emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
			} else {
				
				for(RouteRecord routeRecord:routeRecords) {
					User user=userService.getUser(routeRecord.getCustomer().getId(), false, null, false, false);
					if(null != user.getEmail()) {
						if(user.getStatus().name().equalsIgnoreCase(status)) {
							emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
						}
					}
				}
			}
			
		}
	}
	
	@Async
	public void sendMessageToCustomersWithStatus(Boolean sms, Boolean push, Boolean email, String title, String message, String[] customers, Long id, String status, InputStream picIpStream, String templateName) {
		
		if(sms) {
			for(String customer:customers) {
				User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
				if(user.getStatus().name().equalsIgnoreCase(status)) {
					plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
				}
			}
		}
		
		if(push) {
			for(String customer:customers) {
				User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
				if(user.getStatus().name().equalsIgnoreCase(status)) {
					firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
				}
			}
		}
		
		if(email) {
			
			if(StringUtils.isNotBlank(templateName)) {

				List<String> mailIds = new ArrayList<String>();
				for(String customer:customers) {
					User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
					if(user.getStatus().name().equalsIgnoreCase(status)) {
						if(null != user.getEmail()) {
							mailIds.add(user.getEmail());
						}
					}
				}
				emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
			} else {
				
				for(String customer:customers) {
					User user=userService.getUser(Long.parseLong(customer), false, null, false, false);
					if(user.getStatus().name().equalsIgnoreCase(status)) {
						if(null != user.getEmail()) {
							emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
						}
					}
				}
			}
		}
	}
	
	@Async
	public void sendMessageToAllCustomersWithStatus(Boolean sms, Boolean push, Boolean email, String title, String message, Long id, String status, InputStream picIpStream, String templateName) {
		
		List<User> users = userService.getUsersByStatus(Status.valueOf(status));
		if(sms) {
			for(User user:users) {
				plivoSMSHelper.sendSMS(user.getMobileNumber(), message);
			}
		}
		
		if(push) {
			for(User user : users) {
				firebaseWrapper.sendNotificationFromWeb(user, title, message,id);
			}
		}
		
		if(email) {
			if(StringUtils.isNotBlank(templateName)) {
				List<String> mailIds = new ArrayList<String>();
				for(User user : users) {
					if(null != user.getEmail()) {
						mailIds.add(user.getEmail());
					}
				}
				emailHelper.sendMailWithMandrilTemplate(mailIds, title, templateName, picIpStream);
			} else {
				for(User user : users) {
					if(null != user.getEmail()) {
						emailHelper.sendMailForSelectedCustomer(user, message, title, picIpStream);
					}
				}
			}
		}
	}
	
	@Async
	public void checkStatusWithPaytm(String txnId,String orderId) {
		SystemProperty systemProperty=systemPropertyHelper.getSystemProperty(SystemProperty.PAYTM_MID);
		
		String paramString = "{\"MID\":\"" +systemProperty.getPropValue() + "\",\"ORDERID\":\"" + orderId + "\"}";
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("JsonData", paramString);
		try {
			ApiResponse apiResponse = HttpUtils.sendSecuredHttpGet("secure.paytm.in", "/oltp/HANDLER_INTERNAL/TXNSTATUS", params);
			if(apiResponse.isSuccess()) {
				Map<String, Object> dataMap = apiResponse.getData();
				String status = dataMap.get("STATUS") + "";
				if(status != null && status.equalsIgnoreCase("TXN_SUCCESS")) {
					return;
				}
			}
			
			//Mark payment status as not verified by paytm
			paymentService.updatePGStatus(txnId, false, false, null, false, false);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void checkStatusWithPaytmForOneTimeOrder(String txnId) {
		SystemProperty systemProperty=systemPropertyHelper.getSystemProperty(SystemProperty.PAYTM_MID);
		
		String paramString = "{\"MID\":\"" +systemProperty.getPropValue() + "\",\"ORDERID\":\"" + txnId + "\"}";
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("JsonData", paramString);
		try {
			ApiResponse apiResponse = HttpUtils.sendSecuredHttpGet("secure.paytm.in", "/oltp/HANDLER_INTERNAL/TXNSTATUS", params);
			if(apiResponse.isSuccess()) {
				Map<String, Object> dataMap = apiResponse.getData();
				String status = dataMap.get("STATUS") + "";
				if(status != null && status.equalsIgnoreCase("TXN_SUCCESS")) {
					return;
				}
			}
			
			//Mark payment status as not verified by paytm
			oneTimeOrderService.updatePGStatus(txnId, false, false, null, false, false);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	public void populateDeliveryScheduleSubscriptionType() {
		
		List<Route> routes = routeService.getAllRoutes(null, false, false);
		for (Route route : routes) {
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByRoute(route.getId(), null, false, false);
			List<DeliverySchedule> deliverySchedulesToSave = new ArrayList<DeliverySchedule>();
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(deliverySchedule.getType() != Type.Normal) {
					continue;
				}
				deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
				deliverySchedulesToSave.add(deliverySchedule);
			}
			deliveryScheduleService.updateSubscriptionType(deliverySchedulesToSave, null, false, false);
		}
	}
	
	@Async
	public void populateDeliverylineItemPrice() {
		
		List<Route> routes = routeService.getAllRoutes(null, false, false);
		for (Route route : routes) {
			
			List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliverySchedulesByRoute(route.getId(), null, false, false);
			List<Long> scheduleIds = new ArrayList<Long>();
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				scheduleIds.add(deliverySchedule.getId());
			}
			
			List<DeliveryLineItem> deliveryLineItems = deliveryLineItemService.getAllDeliveryLineItemsByDeliverySchedule(scheduleIds, null, false, false);
			List<DeliveryLineItem> deliveryLineItemsToSave = new ArrayList<DeliveryLineItem>();
			for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				deliveryLineItem.setPricePerUnit(deliveryLineItem.getProduct().getPrice());
				deliveryLineItem.setTotalPrice(deliveryLineItem.getPricePerUnit() * deliveryLineItem.getQuantity());
				deliveryLineItemsToSave.add(deliveryLineItem);
			}
			deliveryLineItemService.updatePriceAndTotalPrice(deliveryLineItemsToSave, null, false, false);
		}
	}
}
