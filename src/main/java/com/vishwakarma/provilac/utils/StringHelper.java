package com.vishwakarma.provilac.utils;

public class StringHelper {

	public static String arrayToCsv (Object[] elems) {
		
		if (null==elems || 0==elems.length) {
			return "";
		}
		
		StringBuffer buf = new StringBuffer();
		for (Object obj : elems) {
			buf.append(obj.toString()).append(",");
		}
		if (buf.length()>0) {
			return buf.substring(0, buf.length()-1);
		}
		else {
			return buf.toString();
		}
	}

	public static String generateHashWithRandomString(String email) {
		StringBuilder builder = new StringBuilder();
		builder.append(email);
		builder.append("aZbwAcdR");
		return String.valueOf(builder.toString().hashCode());
		}
}
