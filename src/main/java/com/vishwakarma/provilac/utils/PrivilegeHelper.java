package com.vishwakarma.provilac.utils;

import javax.annotation.Resource;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.service.PrivilegeService;
import com.vishwakarma.provilac.service.UserService;

@Component
public class PrivilegeHelper {
	
	@Resource 
	private PrivilegeService privilegeService;
	
	@Resource 
	private UserService userService;
	
	public boolean hasPrivilege(Long userId, String privilege) {
		User user = userService.getUser(userId, true, null, false, false);
		for(Role role : user.getUserRoles()) {
			if(role.getPrivileges().toString().contains(privilege)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean loggedInUserHasPrivilege(String privilege) {
		UserInfo userInfo = (UserInfo)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		for(Role role : userInfo.getUser().getUserRoles()) {
			if(role.getPrivileges().toString().contains(privilege)) {
				return true;
			}
		}
		return false;
	}
	
}
