package com.vishwakarma.provilac.utils;

public class AppConstants {

	public static final int PAGE_SIZE = 10;
	public static final int PASSWORD_LENGTH = 6;
	public static final String IMPORT_EXCEL_FORMAT = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String CLONED_SUBSCRIPTION_NOTE = "SUBSCRIPTION_CLONED";
	public static final String PROVILAC_IMAGES_FOLDER = "provilacImages";
	public static final String CATEGORY_PICS_FOLDER = "categoryPics";
	public static final String PRODUCT_PICS_FOLDER = "productPics";
	public static final String PRODUCT_CART_PICS_FOLDER = "productCartPics";
	public static final String PRODUCT_LIST_PICS_FOLDER = "productListPics";
	public static final String PRODUCT_DETAILS_PICS_FOLDER = "productDelailsPics";
	public static final String NOTIFICATION_IMAGES_FOLDER ="notificationImages";
	public static final String PRODUCT_BANNERS_FOLDER = "productBanners";
	public static final String PAYU_SURL = "/paynow/payu/success";
	public static final String PAYU_FURL = "/paynow/payu/failure";
	public static final String WEB_PAYMENT_URL = "/paynow";
	public static final String WEB_PAYMENT_PARAM_NAME = "cipher";
	public static final String FRONTEND_PAYU_SURL = "/frontend/paynow/payu/success";
	public static final String FRONTEND_PAYU_FURL = "/frontend/paynow/payu/failure";
	public static final String OT_FRONTEND_PAYU_SURL = "/onetimeorder/payu/success";
	public static final String OT_FRONTEND_PAYU_FURL = "/onetimeorder/payu/failure";
	public static final String PREPAY_FRONTEND_PAYU_SURL = "/prepay/payu/success";
	public static final String PREPAY_FRONTEND_PAYU_FURL = "/prepay/payu/failure";
	public static final String EXTEND_PREPAY_FRONTEND_PAYU_SURL = "/extend/prepay/payu/success";
	public static final String SHIFT_TO_PREPAY_FRONTEND_PAYU_SURL = "/shift/prepay/payu/success";
	public static final String CHANGE_DELIVERY_PATTERN_FRONTEND_PAYU_SURL = "/change-delivery-pattern/prepay/payu/success";
	
	//starting from sunday 
	public static enum Day {
		Day_1,
		Day_2,
		Day_3,
		Day_4,
		Day_5,
		Day_6,
		Day_7
	}
}
