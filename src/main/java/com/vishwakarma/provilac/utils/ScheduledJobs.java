package com.vishwakarma.provilac.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.DeliverySheet;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OneTimeOrder.OrderStatus;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.service.AddressService;
import com.vishwakarma.provilac.service.CityService;
import com.vishwakarma.provilac.service.CollectionService;
import com.vishwakarma.provilac.service.DeletedRouteRecordService;
import com.vishwakarma.provilac.service.DeliveryLineItemService;
import com.vishwakarma.provilac.service.DeliveryScheduleService;
import com.vishwakarma.provilac.service.DeliverySheetService;
import com.vishwakarma.provilac.service.DepositService;
import com.vishwakarma.provilac.service.InvoiceService;
import com.vishwakarma.provilac.service.OneTimeOrderService;
import com.vishwakarma.provilac.service.PaymentService;
import com.vishwakarma.provilac.service.RouteRecordService;
import com.vishwakarma.provilac.service.RouteService;
import com.vishwakarma.provilac.service.SubscriptionLineItemService;
import com.vishwakarma.provilac.service.SummarySheetRecordService;
import com.vishwakarma.provilac.service.SummarySheetService;
import com.vishwakarma.provilac.service.UserRouteService;
import com.vishwakarma.provilac.service.UserService;
import com.vishwakarma.provilac.service.UserSubscriptionService;
import com.vishwakarma.provilac.utils.AppConstants.Day;

@Component
public class ScheduledJobs {
	
	private final static Logger log = LoggerFactory.getLogger(ScheduledJobs.class);

	@Resource
	private UserSubscriptionService userSubscriptionService;

	@Resource
	private DeliveryScheduleService deliveryScheduleService;

	@Resource 
	private  UserService userService;
	
	@Resource
	private InvoiceService invoiceService;
	
	@Resource 
	private SummarySheetService summarySheetService;
	
	@Resource
	private SummarySheetRecordService summarySheetRecordService;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private DeliverySheetService deliverySheetService;
	
	@Resource
	private UserRouteService userRouteService;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource 
	private DeliveryLineItemService deliveryLineItemService;
	
	@Resource SubscriptionLineItemService subscriptionLineItemService;
	
	@Resource
	private NotificationHelper  notificationHelper;
	
	@Resource
	private EmailHelper emailHelper;
	
	@Resource
	private CityService cityService;
	
	@Resource
	private AddressService addressService;
	
	@Resource  
	private OneTimeOrderService oneTimeOrderService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private AsyncJobs asyncJobs;
	
	@Resource
	private PaymentService paymentService;
	
	@Resource
	private DeletedRouteRecordService deletedRouteRecordService;
	
	@Resource
	private CollectionService collectionService;
	
	@Resource
	private DepositService depositService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Async
	@Scheduled(cron = "0 30 2 * * ?")
	public void runDailyCreateDeliveryScheduler() {
		
		Date date= new Date();
		DateHelper.setToStartOfDay(date);
		
		Date prepaidDeliveryDate = DateHelper.addDays(date, 2);
		int dayOfWeek = DateHelper.getDateField(prepaidDeliveryDate, Calendar.DAY_OF_WEEK);
		Day currentDay = Day.values()[dayOfWeek - 1];
		
		String startTimeMessageBody = "<p>Cron Job (runDailyCreateDeliveryScheduler) is started At: "+ date +".</p>";
		notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", startTimeMessageBody, "Cron Job Status");
		notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", startTimeMessageBody, "Cron Job Status");
		List<UserSubscription> userSubscriptions = userSubscriptionService.getAllCurrentSubscriptions(null, false, true);

		for (UserSubscription userSubscription : userSubscriptions) {
			if((userSubscription.getUser().getStatus().equals(Status.NEW)||userSubscription.getUser().getStatus().equals(Status.ACTIVE)) && userSubscription.getUser().getIsRouteAssigned()){
				
				if (!date.after(userSubscription.getStartDate()) || userSubscription.getUser().getStatus().toString().equalsIgnoreCase(Status.HOLD.name())) {
					continue;
				}
				
				if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid) {
					generatePostpaidDeliverySchedule(userSubscription);
				}
//				} else {
//					/**
//					 * 0. Check subscription end date
//					 * 1. Create delivery schedule according to the line items
//					 * 2. Check user's pending dues
//					 * 3. Update user's pending dues 
//					 */
//					User user = userSubscription.getUser();
//					double availableBalance = user.getLastPendingDues();
//					//Month's extension and 10 is the required minimum balance
//					if(DateHelper.getDifferenceInDays(userSubscription.getEndDate(), date) > 30 && availableBalance > 10) {
//						continue;
//					}
//					
//					List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(userSubscription.getUser());
//					if(routeRecords.isEmpty()){
//						continue;
//					}
//					RouteRecord routeRecord = routeRecords.get(0);
//					
//					DeliverySchedule deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(userSubscription.getUser().getCode(), prepaidDeliveryDate, false, null, false, true);
//					if(deliverySchedule != null) {
//						continue;
//					}
//					
//					List<DeliveryLineItem> deliveryLineItems = new ArrayList<DeliveryLineItem>();
//					double deliveryScheduleCost = 0;
//					
//					Set<SubscriptionLineItem> lineItemToSave = new HashSet<SubscriptionLineItem>();
//					for(SubscriptionLineItem subscriptionLineItem : userSubscription.getSubscriptionLineItems()) {
//						
//						Product product = subscriptionLineItem.getProduct();
//						DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
//						deliveryLineItem.setProduct(product);
//						deliveryLineItem.setPricePerUnit(product.getPrice());
//						double lineItemCost = 0;
//						
//						if((subscriptionLineItem.getType() == SubscriptionType.Daily || (subscriptionLineItem.getType() == SubscriptionType.Weekly && currentDay == subscriptionLineItem.getDay())) && subscriptionLineItem.getQuantity() > 0) {
//
//							deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
//							lineItemCost = product.getPrice() * subscriptionLineItem.getQuantity();
//							
//							deliveryLineItem.setTotalPrice(lineItemCost);
//							deliveryLineItems.add(deliveryLineItem);
//						} else {
//							//Custom Pattern
//							String[] patternArray = subscriptionLineItem.getCustomPattern().split("-");
//							int currentPatternIndex = subscriptionLineItem.getCurrentIndex();
//
//							if(currentPatternIndex >= patternArray.length) {
//								currentPatternIndex = 0;	
//							}
//							
//							int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
//							if(quantity > 0) {
//							
//								deliveryLineItem.setQuantity(quantity);
//								lineItemCost = subscriptionLineItem.getProduct().getPrice() * quantity;
//
//								deliveryLineItem.setTotalPrice(lineItemCost);
//								deliveryLineItems.add(deliveryLineItem);
//							}
//							subscriptionLineItem.setCurrentIndex(currentPatternIndex + 1);
//							lineItemToSave.add(subscriptionLineItem);
//						}
//						deliveryScheduleCost = deliveryScheduleCost + lineItemCost;
//					}
//					
//					//update the indices, before the next block because of alternate quantity
//					for (SubscriptionLineItem subscriptionLineItem : lineItemToSave) {
//						subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, null, false, false);
//					}
//
//					//deliveryScheuleCost == 0 --> No delivery available
//					if(deliveryLineItems.isEmpty() || deliveryScheduleCost <= 0 || availableBalance < deliveryScheduleCost) {
//						continue;
//					}
//					
//					deliverySchedule = new DeliverySchedule();
//					deliverySchedule.setCustomer(user);
//					deliverySchedule.setDate(prepaidDeliveryDate);
//					deliverySchedule.getDeliveryLineItems().addAll(deliveryLineItems);
//					deliverySchedule.setRoute(routeRecord.getRoute());
//					deliverySchedule.setPriority(routeRecord.getPriority());
//					if(null!=userSubscription.getPermanantNote()){
//						deliverySchedule.setPermanantNote(userSubscription.getPermanantNote());
//					}
//					deliverySchedule.setType(Type.Normal);
//					deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid);
//					deliveryScheduleService.createDeliverySchedule(deliverySchedule, null, false, false);
//					
//					user.setLastPendingDues(availableBalance - deliveryScheduleCost);
//					userService.updateUser(user.getId(), user, null, false, false);
//				}
			}
		}
		String endTimeMessageBody = "<p>Cron Job (runDailyCreateDeliveryScheduler) is Completed at: "+ new Date() +".</p>";
		notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", endTimeMessageBody, "Cron Job Status");
		notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", endTimeMessageBody, "Cron Job Status");
	}
	
	private void generatePostpaidDeliverySchedule(UserSubscription userSubscription){
		
		if(userSubscription.getSubscriptionType() == com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Prepaid) {
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 60);
		Date startDate = calendar.getTime();
		DateHelper.setToEndOfDay(startDate);

		DeliverySchedule deliverySchedule = null;
		List<RouteRecord> routeRecords = routeRecordService.getAllRouteRecordsByCustomer(userSubscription.getUser());
		if(routeRecords.isEmpty()){
			return;
		}
		
		RouteRecord routeRecord = routeRecords.get(0);
		
		Set<DeliveryLineItem> lineItemsToSave = new HashSet<DeliveryLineItem>();
		for (SubscriptionLineItem subscriptionLineItem : userSubscription.getSubscriptionLineItems()) {
			if(subscriptionLineItem.getType().equals(SubscriptionType.Daily)){
			if(subscriptionLineItem.getQuantity() > 0) {
				DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
				deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
				deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
				
				deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
				deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * subscriptionLineItem.getQuantity());
				
				lineItemsToSave.add(deliveryLineItem);
			}
			}else if(subscriptionLineItem.getType().equals(SubscriptionType.Weekly)) {
				int dayOfWeek = DateHelper.getDateField(startDate, Calendar.DAY_OF_WEEK);
				Day currentDay = Day.values()[dayOfWeek - 1];
				if(subscriptionLineItem.getDay() == currentDay && subscriptionLineItem.getQuantity() > 0) {
					
					DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
					deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
					deliveryLineItem.setQuantity(subscriptionLineItem.getQuantity());
					
					deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
					deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * subscriptionLineItem.getQuantity());
					
					lineItemsToSave.add(deliveryLineItem);
				}
			}else{
				String[] patternArray = subscriptionLineItem.getCustomPattern().split("-");
				int currentPatternIndex = subscriptionLineItem.getCurrentIndex();

				if(currentPatternIndex >= patternArray.length) {
					currentPatternIndex = 0;	
				}
				
				int quantity = Integer.parseInt(patternArray[currentPatternIndex].trim());
				if(quantity > 0) {
				
					DeliveryLineItem deliveryLineItem = new DeliveryLineItem();
					deliveryLineItem.setProduct(subscriptionLineItem.getProduct());
					deliveryLineItem.setQuantity(quantity);
					
					deliveryLineItem.setPricePerUnit(subscriptionLineItem.getProduct().getPrice());
					deliveryLineItem.setTotalPrice(subscriptionLineItem.getProduct().getPrice() * quantity);
					
					lineItemsToSave.add(deliveryLineItem);
				}
				subscriptionLineItem.setCurrentIndex(currentPatternIndex + 1);
				subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, null, false, false);
			}
		}
		
		// Update updated indices for all the subscription lineitems
		for (SubscriptionLineItem subscriptionLineItem : userSubscription.getSubscriptionLineItems()) {
			subscriptionLineItemService.updateSubscriptionLineItem(subscriptionLineItem.getId(), subscriptionLineItem, null, false, false);
		}
		
		if(!lineItemsToSave.isEmpty()) {
			deliverySchedule = deliveryScheduleService.getDeliveryScheduleCustomerAndDate(userSubscription.getUser().getCode(), startDate, false, null, false, true);
			if (deliverySchedule == null) {
				
				deliverySchedule = new DeliverySchedule();
				deliverySchedule.setCustomer(userSubscription.getUser());
				deliverySchedule.setStatus(null);
				deliverySchedule.setType(Type.Normal);
				deliverySchedule.setDate(startDate);
				deliverySchedule.setRoute(routeRecord.getRoute());
				if(null!=userSubscription.getPermanantNote()){
					deliverySchedule.setPermanantNote(userSubscription.getPermanantNote());
				}
				deliverySchedule.setPriority(routeRecord.getPriority());
				deliverySchedule.setSubscriptionType(com.vishwakarma.provilac.model.UserSubscription.SubscriptionType.Postpaid);
				deliverySchedule = deliveryScheduleService.createDeliverySchedule(deliverySchedule, null, false, true);

				for (DeliveryLineItem deliveryLineItem : lineItemsToSave) {
					deliveryLineItem.setDeliverySchedule(deliverySchedule);
				}
				
				deliveryLineItemService.createDeliveryLineItems(lineItemsToSave, null, false, false);
				
			}
		}
	}
	
	@Async
	@Scheduled(cron = "0 15 3 * * ?")
	public void createDeliverySheet(){
		try {
			
			String startTimeMessageBody = "<p>Cron Job (createDeliverySheet) is started At: "+ new Date() +".</p>";
			notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", startTimeMessageBody, "Cron Job Status");
			notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", startTimeMessageBody, "Cron Job Status");
			Set<DeliverySchedule> deliverySchedules = null;
			List<DeliverySchedule> list = null;
			DeliverySheet deliverySheet = null;
			UserRoute userRoute = null;
			
			Date fromDate = new Date();
			Date toDate = new Date();
			DateHelper.setToStartOfDay(fromDate);
			DateHelper.setToEndOfDay(toDate);
			
			List<Route> routes = routeService.getAllRoutes(null, false, true);
			for (Route route : routes) {
				deliverySchedules = new HashSet<DeliverySchedule>();
				userRoute = userRouteService.getUserRouteByRouteName(route.getName(), false, null, false, true);
				if(null == userRoute){
					continue;
				}
				list = deliveryScheduleService.getScheduleByRouteAndDate(route.getId(), fromDate, null, false, false);
				if(list.isEmpty()){
					continue;
				}
					
				deliverySheet = new DeliverySheet();
				deliverySheet.setDate(toDate);
				if(!list.isEmpty()){
					deliverySheet.getDeliverySchedules().addAll(list);
				}
				
				deliverySheet.setUserRoute(userRoute);
				deliverySheet = deliverySheetService.createDeliverySheet(deliverySheet, null, false, false);
				for(DeliverySchedule schedule:list){
					schedule.setDeliverySheet(deliverySheet);
					//TODO: Call update method
					schedule = deliveryScheduleService.createDeliverySchedule(schedule, null, false, false);
				}
			}
			String endTimeMessageBody = "<p>Cron Job (createDeliverySheet) is Completed at: "+ new Date() +".</p>";
			notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", endTimeMessageBody, "Cron Job Status");
			notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", endTimeMessageBody, "Cron Job Status");
		} catch (Exception e){
			log.warn(e.getLocalizedMessage(), e);
		}
	}
	
	public void sendNotificationforNonDeliveredOrder(){
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE,-1);
		Date date = calendar.getTime();
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getScheduleByStatusAndDate(DeliveryStatus.NotDelivered,date, null, false, false);
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			firebaseWrapper.sendUndeliveredOrderNotification(deliverySchedule);
		}
		
	}
	

	@Async
	@Scheduled(cron = "0 55 23 * * ?")
	public void updateDeliverySchedulesForPastDay(){
		try {
			
			String startTimeMessageBody = "<p>Cron Job (updateDeliverySchedulesForPastDay) is started At: "+ new Date() +".</p>";
			notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", startTimeMessageBody, "Cron Job Status");
			notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", startTimeMessageBody, "Cron Job Status");
			List<DeliverySchedule> list = null;
			
			Date fromDate = new Date();
			Date toDate = new Date();
			Date date = new Date();
			DateHelper.setToStartOfDay(fromDate);
			DateHelper.setToEndOfDay(toDate);
			list = deliveryScheduleService.getDeliverySchedulesByDate(date, null, false, true);
			if(list.size()!=0){
				for (DeliverySchedule deliverySchedule : list) {
					
					if(deliverySchedule.getSubscriptionType() == UserSubscription.SubscriptionType.Postpaid && deliverySchedule.getStatus() != null)
						continue;
					
					if(deliverySchedule.getStatus() == null)
						deliverySchedule.setStatus(DeliveryStatus.Delivered);
					
					if(deliverySchedule.getSubscriptionType() == UserSubscription.SubscriptionType.Prepaid && deliverySchedule.getStatus() == DeliveryStatus.Delivered) {
						double deliveryScheduleAmount = 0;
						for(DeliveryLineItem deliveryLineItem : deliverySchedule.getDeliveryLineItems()) {
							deliveryScheduleAmount = deliveryScheduleAmount + deliveryLineItem.getTotalPrice();
						}
						User customer = deliverySchedule.getCustomer();
						customer.setRemainingPrepayBalance(customer.getRemainingPrepayBalance() - deliveryScheduleAmount);
						customer = userService.updateUser(customer.getId(), customer, null, false, false);
						//TODO: Send email and sms for low prepay balance
						if(customer.getRemainingPrepayBalance() < 50) {
							plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Your milk subscription has been stopped. Please choose the plan or call customer care: +91-8411864646. https://bit.do/provilac");
							emailHelper.sendMailOnPrepayStopped(customer);
						} else if(customer.getRemainingPrepayBalance() < 199 && customer.getRemainingPrepayBalance() >=50) {
							//Critically low
							plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Your Provilac prepay milk subscription is coming to an end. Please choose a plan to continue further. https://bit.do/provilac");
							emailHelper.sendMailForLowPrepayBalance(customer);
						} else if(customer.getRemainingPrepayBalance() < 499 && customer.getRemainingPrepayBalance() >= 200) {
							//Running low
							plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Your Provilac prepay milk subscription is coming to an end. Please choose a plan to continue further. https://bit.do/provilac");
							emailHelper.sendMailForLowPrepayBalance(customer);
						} else if(customer.getRemainingPrepayBalance() < 1000) {
							//About to expire
							plivoSMSHelper.sendSMS(customer.getMobileNumber(), "Your Provilac prepay milk subscription is coming to an end. Please choose a plan to continue further. https://bit.do/provilac");
							emailHelper.sendMailForLowPrepayBalance(customer);
						}
					}
					
					deliveryScheduleService.updateDeliveryScheduleStatusAndReason(deliverySchedule.getId(), deliverySchedule, null, false, false);
				}
			}
			
			String	endTimeMessageBody = "<p>Cron Job (runDailyCreateDeliveryScheduler) is Completed at: "+ new Date() +".</p>";
			notificationHelper.sendNotificationForCronJobCompletion("vishalpawale123@gmail.com", endTimeMessageBody, "Cron Job Status");
			notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", endTimeMessageBody, "Cron Job Status");
			
		} catch (Exception e){
			log.warn(e.getLocalizedMessage(), e);
			notificationHelper.sendNotificationForCronJobCompletion("rohitpotangale@gmail.com", e.getLocalizedMessage(), "Cron Job Exception");
		}
	}
	
	@Scheduled(cron = "0 0 23 * * ?")
	public void updateOneTimeOrderStatus() {
		
		Date today = new Date();
		DateHelper.setToStartOfDay(today);
		List<OneTimeOrder> orders = oneTimeOrderService.getOneTimeOrderByDate(today, null, false, true);
		for (OneTimeOrder oneTimeOrder : orders) {
			
			if (null == oneTimeOrder.getDeliveryBoy())
				continue;
			
			oneTimeOrder.setDeliveryTime(today);
			oneTimeOrder.setStatus(OrderStatus.Delivered);
			oneTimeOrderService.updateOneTimeOrder(oneTimeOrder.getId(), oneTimeOrder, null, false, false);
		}
	}
	
	@Async
	//@Scheduled(cron = "0 15 1 * * ?")
	@Deprecated
	public void sendNotificationOnTrialDay(){
		
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Normal, date, date, null, false, false);
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			firebaseWrapper.sendTrialDayRememberNotification(deliverySchedule.getCustomer());
			emailHelper.sendMailForTrialDeliveryToday(deliverySchedule.getCustomer());
			plivoSMSHelper.sendSMS(deliverySchedule.getCustomer().getMobileNumber(), "Hello "+deliverySchedule.getCustomer().getFullName()+". We have delivered you the a bottle of Provilac Milk this morning. Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac. Team Provilac!");
		}
		
	}
	
	@Async
	@Scheduled(cron = "0 15 1 * * ?")
	public void sendNotificationOneDayBeforeTrialReminder(){
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		Date date = calendar.getTime();
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleService.getDeliveryScheduleByType(Type.Free_Trial, date, date, null, false, false);
		deliverySchedules.addAll(deliveryScheduleService.getDeliveryScheduleByType(Type.Paid_Trial, date, date, null, false, false));
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			firebaseWrapper.sendOneDayBeforeTrialDayRememberNotification(deliverySchedule.getCustomer());
			emailHelper.sendMailForTrialDeliveryTommorow(deliverySchedule.getCustomer());
			plivoSMSHelper.sendSMS(deliverySchedule.getCustomer().getMobileNumber(), "Hello "+deliverySchedule.getCustomer().getFullName()+". A 1-day delivery of Provilac Milk have been scheduled for you tommorow morning.Customer care no. 8411864646 for any queries. Provilac App: http://bit.do/provilac. Team Provilac!");
		}
	}
	
	@Async
	//@Scheduled(cron = "0 5 0 * * ?")
	public void sendNotificationOneTimeOrderOnDeliveryDay(){
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		
		List<OneTimeOrder> oneTimeOrders = oneTimeOrderService.getTodayOneTimeOrdersByStatus(OrderStatus.Delivered, null,false, false);
		for (OneTimeOrder oneTimeOrder : oneTimeOrders) {
			firebaseWrapper.sendOneTimeOrderDeliveredNotification(oneTimeOrder);
			plivoSMSHelper.sendSMS(oneTimeOrder.getCustomer().getMobileNumber(), "Dear Customer, Your One Time Order is Delivered.");
		}
	}
	
	
	@Async
	@Scheduled(cron = "0 15 1 * * ?")
	public void sendNotificationOutstandingCollectionBoy(){
		
		List<User> collectionBoys=userService.getUsersForRole(Role.ROLE_COLLECTION_BOY, null, false, false);
		for (User collectionBoy :collectionBoys) {
			firebaseWrapper.sendCollectionBoyOutstandingAmountNotification(collectionBoy);
		}
	}
	
	@Async
	@Scheduled(cron = "0 30 12 * * ?")
	public void sendNotificationDeliveryBoyRemainder(){
		
		List<User> deliveryBoys=userService.getUsersForRole(Role.ROLE_DELIVERY_BOY, null, false, false);
		for (User deliveryBoy :deliveryBoys) {
			firebaseWrapper.sendDeliveryBoyRemainderNotification(deliveryBoy);
		}
	}
}
