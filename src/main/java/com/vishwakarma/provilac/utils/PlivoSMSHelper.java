package com.vishwakarma.provilac.utils;

import java.util.LinkedHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

@Component
public class PlivoSMSHelper {
	
	@Resource
	private Environment environment;
	
	private boolean isEnabled;
	private String authId;
	private String authToken;
	private String sender;
	
	@PostConstruct
	public void doInit() {
		
		isEnabled = Boolean.parseBoolean(environment.getProperty("sms.enabled", "false"));
		if(isEnabled) {
			authId = environment.getRequiredProperty("sms.plivo.authid");
	        authToken = environment.getRequiredProperty("sms.plivo.authtoken");
	        sender = environment.getRequiredProperty("sms.plivo.sender");
		}
	}
	
	/**
	 * 
	 * @param number followed by Country Code i.e +91 for India
	 * @param messageContent 
	 * @return True if message send Otherwise false
	 */
	public Boolean sendSMS(String number,String messageContent){

		if(!isEnabled)
			return false;
		
		RestAPI api = new RestAPI(authId, authToken, "v1");
        
        LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
        parameters.put("src", sender);
        parameters.put("dst", "+91"+number.trim());
        parameters.put("text", messageContent);
        parameters.put("url", "http://server/message/notification/");
        parameters.put("method", "GET");
            
        try {
            MessageResponse msgResponse = api.sendMessage(parameters);

            System.out.println(msgResponse);
            System.out.println("Api ID : " + msgResponse.apiId);
            System.out.println("Message : " + msgResponse.message);

            if (msgResponse.serverCode == 202) {
                System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
                return true;
            } else {
                System.out.println(msgResponse.error);
                return false;
            }
        } catch (PlivoException e) {
            System.out.println(e.getLocalizedMessage());
            return false;
        }
	}

}
