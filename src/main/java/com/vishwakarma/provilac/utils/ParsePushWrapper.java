package com.vishwakarma.provilac.utils;

/**
 * @author Vishal
 * 
 */
public class ParsePushWrapper {
	
/*	private static enum NotificationType{
		UNDELIVERED_ORDER,
		BOUNCED_PAYMENT,
		PAYMENT_RECEIVED,
		INVOICE_GENERATED,
		ROUTERECORD_CREATED,
		ROUTERECORD_UPDATED,
		ROUTE_SELECTION_FOR_COLLECTION_BOY,
		COLLECTION_BOY_AMOUNT_DEPOSITE,
		COLLECTION_BOY_OUTSTANDING_AMOUNT,
		DELIVERY_BOY_REMINDER,
		ONE_DAY_BEFORE_TRIAL_REMINDER,
		TODAY_TRIAL_REMINDER,
		SUBSCRIPTION_ADDED,
		SUBSCRIPTION_CHANGED,
		SUBSCRIPTION_UPDATED,
		DELIVERED_ORDER,
		SUBSCRIPTION_STOP,
		SUBSCRIPTION_HOLD,
		ON_THE_WAY_TO_COLLECTION,
		REFERRAL_BALANCE_ADDED,
		ONE_TIME_ORDER_NOTIFICATION,
		DELIVERY_SCHEDULE_CHANGE_NOTIFICATION
	}
	
	private static String APP_ID = "C3X5ZumXLUCJGdd1WYcMzZlBYtHpRQstLkPb0Bz2";
	private static String REST_API_KEY = "EkX5Hikg1c5MUuaFKOFZAzWSuceCPrCXhoGSbCnt";
	private static String PARSE_URL ="https://api.parse.com/1/push"; 
	
	private static Logger logger = LoggerFactory.getLogger(ParsePushWrapper.class);

	public static void sendUndeliveredOrderNotification(User user){
		try{
			String alert = "Undelivered Order.";
			String title = "Provilac";
			String notificationType = NotificationType.UNDELIVERED_ORDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void sendBouncedPaymentNotification(Payment payment){
		try{
			String alert = "Your Cheque is Bounced.";
			String title = "Provilac";
			String notificationType = NotificationType.BOUNCED_PAYMENT.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, payment.getCustomer().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void sendPaymentReceivedNotification(Payment payment){
		try{
			String alert = "Payment Received.";
			String alert1 = "Thank You For the Payment.Your Check has been Received. We will update you once Check gets clear";
			String title = "Provilac";
			String notificationType = NotificationType.PAYMENT_RECEIVED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			if (payment.getPaymentMethod() == PaymentMethod.Cash || payment.getPaymentMethod() == PaymentMethod.Online) {
				jsonObject.put("alert", alert);	
			} else {
				jsonObject.put("alert", alert1);
			}
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, payment.getCustomer().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendInvoiceGenerationNotification(User user){
		try{
			String alert = "Dear customer,your invoice for previous month has generated.";
			String title = "Provilac";
			String notificationType = NotificationType.INVOICE_GENERATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void sendRouteSelectionByCollectionBoyNotification(User user,Route route){
		try{
			String alert = route.getName()+" is selected for collecting Payment By"+user.getFullName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTE_SELECTION_FOR_COLLECTION_BOY.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendRouteRecordCreateNotification(User user,Route route){
		try{
			String alert = "Dear Customer, you are added in "+route.getName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTERECORD_CREATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendRouteRecordChangeNotification(User user,Route route){
		try{
			String alert = "Dear Customer, your route is changed to :"+route.getName();
			String title = "Provilac";
			String notificationType = NotificationType.ROUTERECORD_UPDATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendSubscriptionAddedNotification(UserSubscription subscription,Date endDate){
		try{
			String alert = "Dear customer,you have subscribed provilac milk subscription from : "+subscription.getStartDate()+" to "+ endDate ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_ADDED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, subscription.getUser().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendDeliveryScheduleChangeNotification(DeliverySchedule deliverySchedule){
		try{
			String alert = "Dear customer,your Delivery Schedule has been Change : "+"Date:-"+deliverySchedule.getDate();
			String title = "Provilac";
			String notificationType = NotificationType.DELIVERY_SCHEDULE_CHANGE_NOTIFICATION.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("deliverySchedule", deliverySchedule.getCode());
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, deliverySchedule.getCustomer().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendSubscriptionUpdatedNotification(UserSubscription subscription,Date endDate){
		try{
			String alert = "Dear customer,your subscription has been edited from : "+subscription.getStartDate()+" to "+ endDate ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_UPDATED.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, subscription.getUser().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static void sendSubscriptionPutOnHoldNotification(UserSubscription subscription){
		try{
			String alert = "Dear customer,your subscription has been put on hold." ;
			String title = "Provilac";
			String notificationType = NotificationType.SUBSCRIPTION_HOLD.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("subscriptionCode", subscription.getCode());
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, subscription.getUser().getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendOnTheWayCollectionNotification(User collectionBoy,List<String> codes){
		try{
			for (String code : codes) {
				String alert = "Dear Customer,collection boy :"+ collectionBoy.getFirstName() +collectionBoy.getLastName()+ " is on the way to collect payment amount.";
				String title = "Provilac";
				String notificationType = NotificationType.ROUTERECORD_UPDATED.name();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("alert", alert);
				jsonObject.put("title", title);
				jsonObject.put("sound", "Default");
				jsonObject.put("notificationType", notificationType);
				sendParseRequest(jsonObject,code);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void sendReferralBalanceAddedNotification(User customer){
		try{
				
				String alert = "Dear Customer,you have got referral points";
				String title = "Provilac";
				String notificationType = NotificationType.REFERRAL_BALANCE_ADDED.name();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("alert", alert);
				jsonObject.put("title", title);
				jsonObject.put("sound", "Default");
				jsonObject.put("notificationType", notificationType);
				sendParseRequest(jsonObject,customer.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendTrialDayRememberNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk trial delivery is arriving today";
			String title = "Provilac";
			String notificationType = NotificationType.TODAY_TRIAL_REMINDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendOneDayBeforeTrialDayRememberNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk trial delivery is arriving tomorrow";
			String title = "Provilac";
			String notificationType = NotificationType.ONE_DAY_BEFORE_TRIAL_REMINDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public static void sendOneTimeOrderNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk one time order is placed succesfully.";
			String title = "Provilac";
			String notificationType= NotificationType.ONE_TIME_ORDER_NOTIFICATION.name();
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());	
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendOneTimeOrderDeliveredNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac milk one time order delivered successfully.";
			String title = "Provilac";
			String notificationType = NotificationType.DELIVERED_ORDER.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put("notificationType", notificationType);
			sendParseRequest(jsonObject, user.getCode());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void sendOneTimeOrderOnDayNotification(User user){
		try{
			String alert = "Dear Customer, Your provilac one time order delivery is Today.";
			String title = "Provilac";
			String notificationType = NotificationType.ONE_TIME_ORDER_NOTIFICATION.name();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("alert", alert);
			jsonObject.put("title", title);
			jsonObject.put("sound", "Default");
			jsonObject.put(notificationType, notificationType);
			sendParseRequest(jsonObject, user.getCode());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static void sendParseRequest(JSONObject dataObject, String... channels) {
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("data", dataObject);
			JSONArray channelArray = new JSONArray();
			for (int i = 0; i < channels.length; i++) {
				channelArray.put(channels[i]);
			}
			jsonObject.put("channels", channelArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(PARSE_URL);
		
		httpPost.setHeader("X-Parse-Application-Id", APP_ID);
		httpPost.setHeader("X-Parse-REST-API-Key", REST_API_KEY);
		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		
		StringEntity stringEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
		httpPost.setEntity(stringEntity);
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpPost);
			logger.debug("================== Parse Response Code ================== " + httpResponse.getStatusLine().getStatusCode());
			String response = IOUtils.toString(httpResponse.getEntity().getContent());
			logger.debug("================== Parse Response ================== " + response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
}
