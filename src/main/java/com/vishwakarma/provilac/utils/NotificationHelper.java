package com.vishwakarma.provilac.utils;

import java.io.File;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription;



@Component
public class NotificationHelper {
	
	@Resource 
	private EmailHelper emailHelper;
	
	@Deprecated
	public void sendNotificationForNewAccount(User user, String password) {
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailHelper.sendSimpleEmail(user.getEmail(), "Welcome to provilac", getEmailBodyForNewAccount(user, password));
		}
	}	
	
	public void sendNotificationForNewPaymentAdd(User user, Payment payment ,Double lastPendingAmount ) {
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailHelper.sendMailForPaymentReceived(user, payment, lastPendingAmount);
		}
	}
	
	@Deprecated
	public void sendNotificationForPaymentBounced(User user, Payment payment) {
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailHelper.sendMailForPaymentBounced(user, payment);
		}
	}
	
	public void sendNotificationForNonDeliveredOrder(DeliverySchedule deliverySchedule) {
		if(null != deliverySchedule.getCustomer().getEmail() && !deliverySchedule.getCustomer().getEmail().isEmpty()){
			emailHelper.sendMailForNonDeliverdOrder(deliverySchedule);
		}
	}
		
	public void sendNotificationForInvoiceGeneration(User user, String monthAndYear, File file, Invoice invoice) {
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailHelper.sendMailForInvoiceGeneration(user, monthAndYear, file, invoice);
		}
	}
	
	@Deprecated
	public void sendNotificationForStopSubscription(UserSubscription userSubscription,File file){
		if(null !=userSubscription.getUser().getEmail() && !userSubscription.getUser().getEmail().isEmpty()){
			emailHelper.sendMailForSubscriptionStop(userSubscription);
		}
	}
	
	public void sendNotificationForOneTimeOrder(User customer,OneTimeOrder oneTimeOrder){
		if(null != customer.getEmail() && !customer.getEmail().isEmpty()){
			emailHelper.sendMailForOneTimeOrderNotification(customer,oneTimeOrder);
		}
	}
	
	@Deprecated
	public void sendNotificationForOneTimeOrderOnDelivered(User customer,OneTimeOrder oneTimeOrder){
		if(null != customer.getEmail() && !customer.getEmail().isEmpty()){
			emailHelper.sendMailForOneTimeOrderOnDeliveredNotification(customer, oneTimeOrder);
		}
	}
	
	
	@Async
	public void sendNotificationForCronJobCompletion(String email,String messageBody,String subject) {
		emailHelper.sendMailForCronJobCompletion(email, subject, messageBody);
	}
	
	@Async
	public void sendNotificationForNewPrepaySubscription(UserSubscription subscription, Map<Product, Double> effectivePrice) {
		emailHelper.sendMailForPrepaySubscriptionApproval(subscription, effectivePrice);
	}
	
	@Async
	@Deprecated
	public void sendPromotionalEmail(User user) {
		emailHelper.sendPromotionalEmail(user);
	}
	
	public String getSmsBodyForNewAccount(User user, String password) {
		String smsText = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			smsText = "Your account is successfully created on provilac. Your Username is " +  user.getEmail() +
					"and Password is " + password;
		}
		return smsText;
	}
	
	public String getSmsBodyForPasswordReset(User user, String password) {
		String smsText = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			smsText = "Your password is successfully changed for provilac Account. Your Username is " +  user.getEmail() +
					"and new Password is " + password;
		}
		return smsText;
	}
	
	public String getEmailBodyForNewAccount(User user, String password) {
		String emailBody = "No email found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailBody = "Hello " + user.getFullName() + ",\n\n" + 
					"Thanks for signing up with provilac. We're really excited to have you onboard!." + "\n\n" + 
					"Your account details are as follows:-" + "\n\n" + 
					"Username- " + user.getEmail() + "\n" +
					"Password- " + password + "\n\n" +
					"You can use provilac as an Individual user to connect with businesses"+
					"worldwide as well as use provilac as a business user to connect with your"+
					"existing and new customers. You will see that provilac is an extremely easy to use and very useful Mobile App."+"\n\n"+
					"As an individual user you can seek customer support from various businesses"+
					"easily, find and order products and services from various small and large"+
					"businesses, order food from Restaurants, find doctors, florists, handymen,"+
					"architects and millions of other small and local businesses."+"\n\n"+
					"As a business user, your existing customers will be able to chat with you"+
					"for customer service and new customers will be able to find you to order"+
					"your products and services. You will be able to create happier customers as"+
					"well as get many new customers. provilac will help your Business grow."+"\n\n"+
					"Best Regards,"+"\n"+
					"Team provilac";
		}
		return emailBody;
	}
	
	public String getEmailBodyForPasswordReset(User user, String password) {
		String emailBody = "No Email Found";
		if(null != user.getEmail() && !user.getEmail().isEmpty()){
			emailBody = "Hi " + user.getFullName() + ",\n" + 
					"Your password has been successfully changed for your provilac account." + "\n" + 
					"Your new account credentials are as follows-" + "\n" +
					"Username- " + user.getEmail() + "\n" +
					"Password- " + password;
		}
		return emailBody;
	}
}
