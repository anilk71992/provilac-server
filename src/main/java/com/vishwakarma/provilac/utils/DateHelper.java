package com.vishwakarma.provilac.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;

public class DateHelper {
	
	
	public static final String DATE_FORMAT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_SIMPLE = "dd MMM yyyy";
	public static final String TIME_FORMAT = "HH:mm";
	public static final String TIME_FORMAT_WITH_MARKER = "HH:mm a";
	public static final String DATE_TIME_FORMAT = DATE_FORMAT+" "+TIME_FORMAT;
	public static final String DATE_TIME_FORMAT_WITH_MARKER = DATE_FORMAT+" HH:mm:ss a";
	public static final String DATE_FORMAT_FOR_DEVICE = "yyyy-MM-dd";

	public static final String DATE_TIME_FORMAT_TIME_PERIOD = DATE_FORMAT+" "+TIME_FORMAT +" "+TIME_FORMAT_WITH_MARKER;
	
	
	public static String getFormattedDateWithMarker(Date d) {
		return new SimpleDateFormat(DATE_TIME_FORMAT_WITH_MARKER).format(d);		
	}
	
	public static String getFormattedTime(Date d) {
		return new SimpleDateFormat(TIME_FORMAT).format(d);
	}
	
	public static String getFormattedTimeWithMarker(Date d) {
		return new SimpleDateFormat(TIME_FORMAT_WITH_MARKER).format(d);
	}
	
	public static String getFormattedDate(Date d) {
		return new SimpleDateFormat(DATE_FORMAT).format(d);		
	}
	
	public static String getFormattedDateSimple(Date d) {
		return new SimpleDateFormat(DATE_FORMAT_SIMPLE).format(d);		
	}

	public static String getFormattedDateForDevice(Date d) {
		return new SimpleDateFormat(DATE_FORMAT_FOR_DEVICE).format(d);		
	}

	public static String getFormattedTimestamp(Date d) {
		return new SimpleDateFormat(DATE_TIME_FORMAT).format(d);		
	}
	
	//"dd-MM-yyyy HH:mm aa" eg 10-04-2015 10:10 AM
	public static String parseDateTimeWithTimePeriod(Date dateTime){
		return new SimpleDateFormat(DATE_TIME_FORMAT_TIME_PERIOD).format(dateTime);
	}
	
	public static Date parseDateTime(String dateTime) {
		try {
			return new SimpleDateFormat(DATE_TIME_FORMAT).parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date parseDate(String date) {
		try {
			return new SimpleDateFormat(DATE_FORMAT).parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Date parseDateFormatForDevice(String date) {
		try {
			return new SimpleDateFormat(DATE_FORMAT_FOR_DEVICE).parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Date getCurrDateWithTimeZone(String timeZone) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
		cal.setTime(date);
		return cal.getTime();
	}
	
	public static void setToStartOfDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		date.setTime(cal.getTimeInMillis());
	}
	
	public static void setToEndOfDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		date.setTime(cal.getTimeInMillis());
	}
	
	public static Date getLeftTimeWindow(Date date, int window) {
		Calendar leftTimeWindow = Calendar.getInstance();
		leftTimeWindow.setTimeInMillis(date.getTime() - window);
		
		return leftTimeWindow.getTime();
	}
	
	public static Date getRightTimeWindow(Date date, int window) {
		Calendar rightTimeWindow = Calendar.getInstance();
		rightTimeWindow.setTimeInMillis(date.getTime() + window);
		
		return rightTimeWindow.getTime();
	}
	
	public static int calculateScanWindow(long sessionWidth, String windowValue) {
		if (windowValue.contains("PER:")) {
			windowValue = StringUtils.substringAfter(windowValue, "PER:");
			return (int) (sessionWidth*(Double.parseDouble(windowValue))/100); //returns window in milliseconds
		}
		else if (windowValue.contains("ABS:")) {
			windowValue = StringUtils.substringAfter(windowValue, "ABS:");
			return Integer.parseInt(windowValue)*60*1000;  //returns window in milliseconds
		}
		
		return 20*60*1000; //returns window in milliseconds
	}
	
	public static void setToFirstDayOfPreviousWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		date.setTime(cal.getTimeInMillis());
	}
	
	public static void setToLastDayOfPreviousWeek() {
		
	}
	
	public static boolean validateTime(String str) throws NumberFormatException, PatternSyntaxException {
		try {
			String[] hrsAndMinsStrings = str.split(":");
			if(hrsAndMinsStrings.length != 2) {
				return false;
			}
			if(Integer.parseInt(hrsAndMinsStrings[0]) > 23 || Integer.parseInt(hrsAndMinsStrings[1]) > 59) {
				return false;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new NumberFormatException("Invalid hrs./mins. values");
		} catch (PatternSyntaxException e) {
			e.printStackTrace();
			throw new PatternSyntaxException("Invalid time format", ":", -1);
		}
		return true;		
	}
	
	/**
	 * Compares fromDateString with toDateString
	 * @param fromDateString fromDate which is supposed to be initial/earliest date
	 * @param toDateString toDate which is supposed to be final/later date
	 * @return returns true if fromDate is less than toDate otherwise false
	 * @throws NullPointerException if fromDateString or toDateString is not parsable to {@link Date} object 
	 * @author Vishal Pawale
	 */
	public static boolean compareDateStrings(String fromDateString, String toDateString) {
		Date fromDate = parseDate(fromDateString);
		Date toDate = parseDate(toDateString);
		if(toDate.compareTo(fromDate) >= 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Compares fromDate with toDate
	 * @param fromDate fromDate which is supposed to be initial/earliest date
	 * @param toDate toDate which is supposed to be final/later date
	 * @return returns true if fromDate is less than toDate otherwise false
	 * @throws NullPointerException if any of the date is null
	 * @author Vishal Pawale
	 */
	public static boolean compareDates(Date fromDate, Date toDate) {
		if(toDate.compareTo(fromDate) >= 0) {
			return true;
		}
		return false;
	}
	
	public static int getMinsFromHHMM(String hhMMString) {
		
		String[] tokenArray = hhMMString.split(":");
		int hours = Integer.valueOf(tokenArray[0]);
		int mins = Integer.valueOf(tokenArray[1]);
		return (hours * 60) + mins;
	}
	
	public static String getHHMMFromMins(int mins) {
		
		int hours = mins / 60;
		int remainMinute = mins % 60;
		String result = String.format("%02d", hours) + ":" + String.format("%02d", remainMinute);
		return result;
	}
	
	public static int getDateField(Date date, int calendarField) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar.get(calendarField);
	}
	
	public static long getDifferenceInMinutes(Date afterDate, Date pastDate) {
		long diff = afterDate.getTime() - pastDate.getTime();
		return diff/(60 * 1000);
	}
	
	public static long getDifferenceInDays(Date firstDate, Date secondDate) {

		long diffInMillies = secondDate.getTime() - firstDate.getTime();
		TimeUnit timeUnit = TimeUnit.DAYS;
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
	
	public static Date getFirstDatefromDateInPerticularMonth(Date date, int noOfMonths) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, noOfMonths);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = 1;
		c.set(year, month, day);
		int numOfDaysInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		// First Day of month
		Date firstDayOfMonth = c.getTime();
		return firstDayOfMonth;
	}

	public static Date getLastDatefromDateInPerticularMonth(Date date, int noOfMonths) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, noOfMonths);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = 1;
		c.set(year, month, day);
		int numOfDaysInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		c.add(Calendar.DAY_OF_MONTH, numOfDaysInMonth - 1);
		// Last Day of month
		Date lastDayOfMonth = c.getTime();
		return lastDayOfMonth;

	}
	
	/**
	 * -ve days will decreament
	 * @param date
	 * @param days
	 * @return
	 *
	 * @author Vishal
	 */
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); //minus number would decrement the days
		return cal.getTime();
	}
	
	public static Date getStartOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		date.setTime(cal.getTimeInMillis());
		return date;
	}
	
	public static Date addMonths(Date date, int months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months); //minus number would decrement the days
		return cal.getTime();
	}
}
