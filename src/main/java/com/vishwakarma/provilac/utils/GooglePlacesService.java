package com.vishwakarma.provilac.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GooglePlacesService {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/geocode/json";

    private static final String API_KEY = "AIzaSyC4PfR6tAaUZgkUoHgPsUhcSZ8jJvh1HvM";


    public static String getLocality( double lat, double lng) {
        String locality = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append("?latlng="+String.valueOf(lat)+","+String.valueOf(lng));
            sb.append("&sensor=false");
            sb.append("&key=" + API_KEY);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return locality;
        } catch (IOException e) {
            return locality;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
            Map<String, List<String>> typeMap = new HashMap<String,List<String>>();
            for (int i = 0; i < predsJsonArray.length(); i++) {
            	JSONObject object = predsJsonArray.getJSONObject(i);
            	JSONArray types = object.getJSONArray("types"); 
            	
            	List<String> typesToAdd = new ArrayList<String>();
            	for (int j = 0; j < types.length(); j++) {
            		typesToAdd.add(types.getString(j));
				}
            	typeMap.put( object.get("long_name").toString(), typesToAdd);
			}
            
            for (Map.Entry<String, List<String>> entry: typeMap.entrySet()) {
				List<String> types  = entry.getValue();
				if(types.contains("sublocality") && types.contains("sublocality_level_1")){
					locality = entry.getKey();
					break;
				}else if(types.contains("sublocality") && types.contains("sublocality_level_2")){
					locality = entry.getKey();
					break;
				}else if(types.contains("sublocality") && types.contains("sublocality_level_3")){
					locality = entry.getKey();
					break;
				}
			}
            
        } catch (JSONException e) {
           return  e.getMessage();
        }

        return locality;
    }

    public static String getPincode( double lat, double lng) {
        String pincode = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append("?latlng="+String.valueOf(lat)+","+String.valueOf(lng));
            sb.append("&sensor=false");
            sb.append("&key=" + API_KEY);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return pincode;
        } catch (IOException e) {
            return pincode;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
            Map<String, List<String>> typeMap = new HashMap<String,List<String>>();
            for (int i = 0; i < predsJsonArray.length(); i++) {
            	JSONObject object = predsJsonArray.getJSONObject(i);
            	JSONArray types = object.getJSONArray("types"); 
            	
            	List<String> typesToAdd = new ArrayList<String>();
            	for (int j = 0; j < types.length(); j++) {
            		typesToAdd.add(types.getString(j));
				}
            	typeMap.put( object.get("long_name").toString(), typesToAdd);
			}
            
            for (Map.Entry<String, List<String>> entry: typeMap.entrySet()) {
				List<String> types  = entry.getValue();
				if(types.contains("postal_code")){
					pincode = entry.getKey();
					break;
				}
			}
            
        } catch (JSONException e) {
           return  e.getMessage();
        }

        return pincode;
    }
    
	public static Boolean verifycheckSum(String merchantKey,TreeMap <String,String>parameters,String checksum){
		
		boolean isValidChecksum = false;
		try {
			com.paytm.merchant.CheckSumServiceHelper checkSumServiceHelper =  com.paytm.merchant.CheckSumServiceHelper.getCheckSumServiceHelper();
			isValidChecksum = checkSumServiceHelper.verifycheckSum(merchantKey, parameters, checksum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return isValidChecksum;
	}
	
}
