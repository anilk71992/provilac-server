package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.model.specifications.UserRouteSpecifications;
import com.vishwakarma.provilac.repository.UserRouteRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class UserRouteService {

	@Resource
	private UserRouteRepository UserRouteRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Transactional
	public UserRoute createUserRoute(UserRoute userRoute, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == userRoute) {
			throw new ProvilacException("Invalid UserRoute");
		}
		userRoute = UserRouteRepository.save(userRoute);
		activityLogService.createActivityLog(loggedInUser, "add", "UserRoute", userRoute.getId());
		firebaseWrapper.sendRouteSelectionByCollectionBoyNotification(userRoute.getDeliveryBoy(), userRoute.getRoute());
		
		if (populateTransientFields) {
			userRoute = populateTransientFields(userRoute, loggedInUser);
		}
		
		if(fetchEagerly) {
			userRoute= fetchEagerly(userRoute);
		}
		return userRoute;
	}
	
	@Transactional
	public List<UserRoute> getAllUserRoutes(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserRoute> userRoutes = UserRouteRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (UserRoute userRoute : userRoutes) {
			if(populateTransientFields)
				userRoute = populateTransientFields(userRoute, loggedInUser);
			if (fetchEagerly)
				userRoute = fetchEagerly(userRoute);
			}
		}
		return userRoutes;
	}
	
	@Transactional
	public List<UserRoute> getUserRoutesForDeliveryBoy(String deliveryBoyCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserRoute> userRoutes = UserRouteRepository.findByDeliveryBoy_Code(deliveryBoyCode);
		if(populateTransientFields || fetchEagerly){
		for (UserRoute userRoute : userRoutes) {
			if(populateTransientFields)
				userRoute = populateTransientFields(userRoute, loggedInUser);
			if (fetchEagerly)
				userRoute = fetchEagerly(userRoute);
			}
		}
		return userRoutes;
	}
	
	@Transactional
	public List<UserRoute> getUserRoutesForColeectionBoy(Long collectionBoyId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserRoute> userRoutes = UserRouteRepository.findByCollectionBoy_Id(collectionBoyId);
		if(populateTransientFields || fetchEagerly){
		for (UserRoute userRoute : userRoutes) {
			if(populateTransientFields)
				userRoute = populateTransientFields(userRoute, loggedInUser);
			if (fetchEagerly)
				userRoute = fetchEagerly(userRoute);
			}
		}
		return userRoutes;
	}
	
	@Transactional
	public Page<UserRoute> getUserRoutes(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<UserRoute> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = UserRouteRepository.findAll(pageRequest);
		Iterator<UserRoute> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			UserRoute userRoute = (UserRoute) iterator.next();
			if(populateTransientFields)
				userRoute = populateTransientFields(userRoute, loggedInUser);
			if(fetchEagerly)
				userRoute = fetchEagerly(userRoute);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<UserRoute> searchUserRoute(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<UserRoute> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = UserRouteRepository.findAll(UserRouteSpecifications.search(searchTerm), pageRequest);
		Iterator<UserRoute> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			UserRoute userRoute = (UserRoute) iterator.next();
			if(populateTransientFields)
				userRoute = populateTransientFields(userRoute, loggedInUser);
			if(fetchEagerly)
				userRoute = fetchEagerly(userRoute);
			}
		}
		return page;
	}
	
	public UserRoute getUserRoute(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserRoute id");
		}
		UserRoute userRoute = getUserRoute(id, false);
		if(doPostValidation && null == userRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		if(populateTransientFields)
			userRoute= populateTransientFields(userRoute, loggedInUser);
		if(fetchEagerly)
			userRoute= fetchEagerly(userRoute);
		
		return userRoute;
	}

  /**
   * 
   * @param collectionBoyId
   * @param routeCode
   * @param doPostValidation
   * @param loggedInUser
   * @param populateTransientFields
   * @param fetchEagerly
   * @return UserRoute Object otherwise null 
   */
	public UserRoute getUserRouteBycollectionBoyAndRoute(Long collectionBoyId, String routeCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == collectionBoyId) {
			throw new ProvilacException("Invalid collection boy id");
		}
		UserRoute userRoute = UserRouteRepository.findByCollectionBoy_IdAndRoute_Code(collectionBoyId, routeCode);
		if(doPostValidation && null == userRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		if(populateTransientFields)
			userRoute= populateTransientFields(userRoute, loggedInUser);
		if(fetchEagerly)
			userRoute= fetchEagerly(userRoute);
		
		return userRoute;
	}
	
	/**
	 * Get UserRoute by routeName
	 * @param routeName
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return UserRoute Object otherwise null
	 */
	public UserRoute getUserRouteByRouteName(String routeName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == routeName) {
			throw new ProvilacException("Invalid RouteName");
		}
		UserRoute userRoute = UserRouteRepository.findByRoute_Name(routeName);
		if(doPostValidation && null == userRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		if(populateTransientFields)
			userRoute= populateTransientFields(userRoute, loggedInUser);
		if(fetchEagerly)
			userRoute= fetchEagerly(userRoute);
		
		return userRoute;
	}
	
	/**
	 * Get UserRoute By UserCode
	 * @param userCode
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return UserRoute Object otherwise null
	 */
	public UserRoute getUserRouteByUserCodeAndRouteName(String userCode,String routeName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == userCode) {
			throw new ProvilacException("Invalid User Code");
		}
		UserRoute userRoute = UserRouteRepository.findByDeliveryBoy_CodeAndRoute_name(userCode,routeName);
		if(doPostValidation && null == userRoute) {
			throw new ProvilacException("UserRoute with this User Code does not exists");
		}
		if(populateTransientFields)
			userRoute= populateTransientFields(userRoute, loggedInUser);
		if(fetchEagerly)
			userRoute= fetchEagerly(userRoute);
		
		return userRoute;
	}
	

	/**
	 * returns @UserRoute without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public UserRoute getUserRoute(Long id, boolean doPostValidation) {
		
		UserRoute userRoute;
		if(null == id) {
			throw new ProvilacException("Invalid UserRoute id");
		}
		userRoute = UserRouteRepository.findOne(id);
		if(doPostValidation && null == userRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		return userRoute;
	}
	
	/**
	 * 
	 * Updates the UserRoute. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing UserRoute
	 * @param UserRoute - Updated UserRoute
	 * @param loggedInUserRoute - LoggedIn UserRoute
	 * @return Updated UserRoute
	 * 
	 * @author Vishal
	 */
	public UserRoute updateUserRoute(Long id, UserRoute userRoute, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserRoute id");
		}
		UserRoute existingUserRoute = UserRouteRepository.findOne(id);
		if(null == existingUserRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		existingUserRoute.setDeliveryBoy(userRoute.getDeliveryBoy());
		existingUserRoute.setRoute(userRoute.getRoute());
		if(null != userRoute.getCollectionBoy()){
			existingUserRoute.setCollectionBoy(userRoute.getCollectionBoy());
		}
		existingUserRoute = UserRouteRepository.save(existingUserRoute);
		activityLogService.createActivityLog(loggedInUser, "update", "UserRoute", id);
		existingUserRoute.setCipher(EncryptionUtil.encode(userRoute.getId()+""));
		if(populateTransientFields)
			existingUserRoute =populateTransientFields(existingUserRoute, loggedInUser);
		if(fetchEagerly)
			existingUserRoute=fetchEagerly(existingUserRoute);
		return existingUserRoute;
	}
	
	public void deleteUserRoute(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserRoute id");
		}
		UserRoute userRoute = UserRouteRepository.findOne(id);
		if(null == userRoute) {
			throw new ProvilacException("UserRoute with this Id does not exists");
		}
		UserRouteRepository.delete(id);
		activityLogService.createActivityLog(null,"delete", "UserRoute", id);
	}
	
	
	
	
	private UserRoute populateTransientFields(UserRoute userRoute, User loggedInUser) {
		
		if(null == userRoute) {
			return userRoute;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid UserRoute");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			userRoute.setIsEditAllowed(true);
			userRoute.setIsDeleteAllowed(true);
		} else {
			userRoute.setIsEditAllowed(false);
			userRoute.setIsDeleteAllowed(false);
		}
		userRoute.setCipher(EncryptionUtil.encode(userRoute.getId()+""));
		return userRoute;
	}
	
	private UserRoute fetchEagerly(UserRoute userRoute) {
		
		return userRoute;
	}
	
}

