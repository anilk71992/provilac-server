package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Invoice.CollectionStatus;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.InvoiceSpecifications;
import com.vishwakarma.provilac.repository.InvoiceRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class InvoiceService {

	@Resource
	private InvoiceRepository invoiceRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Transactional
	public Invoice createInvoice(Invoice invoice, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == invoice) {
			throw new ProvilacException("Invalid invoice");
		}
		invoice = invoiceRepository.save(invoice);
		activityLogService.createActivityLog(userService.getLoggedInUser(), "add", "Invoice", invoice.getId());
		firebaseWrapper.sendInvoiceGenerationNotification(invoice);
		
		if (populateTransientFields) {
			invoice = populateTransientFields(invoice, loggedInUser);
		}
		
		if(fetchEagerly) {
			invoice= fetchEagerly(invoice);
		}
		return invoice;
	}
	
	@Transactional
	public List<Invoice> getAllInvoices(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Invoice> invoices = invoiceRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Invoice invoice : invoices) {
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if (fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return invoices;
	}
	
	@Transactional
	public List<Invoice> getAllInvoicesByCustomer(String customerCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Invoice> invoices = invoiceRepository.findByCustomer_Code(customerCode);
		if(populateTransientFields || fetchEagerly){
		for (Invoice invoice : invoices) {
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if (fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return invoices;
	}
	

	@Transactional
	public List<Invoice> getAllInvoicesByCustomerId(Long customerId,boolean isPaid, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		List<Invoice> invoices = invoiceRepository.findByCustomerIdAndIsPaid(customerId,isPaid);
		if (populateTransientFields || fetchEagerly) {
			for (Invoice invoice : invoices) {
				if (populateTransientFields)
					invoice = populateTransientFields(invoice, loggedInUser);
				if (fetchEagerly)
					invoice = fetchEagerly(invoice);
			}
		}
		return invoices;
	}
	
	@Transactional
	public Page<Invoice> getInvoices(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = invoiceRepository.findAll(pageRequest);
		Iterator<Invoice> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Invoice invoice = (Invoice) iterator.next();
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if(fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Invoice> getInvoicesByCustomer(String customerCode,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = invoiceRepository.findByCustomer_Code(customerCode, pageRequest);
		Iterator<Invoice> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Invoice invoice = (Invoice) iterator.next();
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if(fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Invoice> searchInvoice(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = invoiceRepository.findAll(InvoiceSpecifications.search(searchTerm), pageRequest);
		Iterator<Invoice> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Invoice invoice = (Invoice) iterator.next();
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if(fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Invoice> searchInvoices(Integer pageNumber, String searchTerm,CollectionStatus status, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = invoiceRepository.findAll(InvoiceSpecifications.searchForNotCollectedInvoices(searchTerm, status),pageRequest);
		Iterator<Invoice> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Invoice invoice = (Invoice) iterator.next();
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if(fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Invoice> searchInvoicesByCollectionStatus(Integer pageNumber,Long routeId,CollectionStatus status,Date fromDate,Date toDate, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = invoiceRepository.findAll(InvoiceSpecifications.searchByRouteAndDateRangeAndCollectionStatus(routeId, status, fromDate, toDate),pageRequest);
		Iterator<Invoice> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Invoice invoice = (Invoice) iterator.next();
			if(populateTransientFields)
				invoice = populateTransientFields(invoice, loggedInUser);
			if(fetchEagerly)
				invoice = fetchEagerly(invoice);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Invoice getInvoiceByCustomerAndDateRange(Long customrId,Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Invoice> invoices = invoiceRepository.findAll(InvoiceSpecifications.searchByCustomerIdAndDateRange(customrId, fromDate, toDate));
		if(invoices.isEmpty()) {
			return null;
		}
		Invoice invoice = invoices.get(0);
		if(populateTransientFields)
			invoice = populateTransientFields(invoice, loggedInUser);
		if (fetchEagerly)
			invoice = fetchEagerly(invoice);
		return invoice;
	}
	
	

	@Transactional
	public List<Invoice> getInvoiceByDateRange(Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Invoice> invoices = invoiceRepository.findAll(InvoiceSpecifications.searchByDateRange(fromDate, toDate),new Sort(Sort.Direction.DESC, "id"));
		if(populateTransientFields || fetchEagerly){
			for (Invoice invoice : invoices) {
				if(populateTransientFields)
					invoice = populateTransientFields(invoice, loggedInUser);
				if (fetchEagerly)
					invoice = fetchEagerly(invoice);
				}
			}
		return invoices;
	}
	
	@Transactional
	public Invoice getInvoice(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Invoice id");
		}
		Invoice invoice = getInvoice(id, false);
		if(doPostValidation && null == invoice) {
			throw new ProvilacException("Invoice with this Id does not exists");
		}
		if(populateTransientFields)
			invoice= populateTransientFields(invoice, loggedInUser);
		if(fetchEagerly)
			invoice= fetchEagerly(invoice);
		
		return invoice;
	}
	@Transactional
	public Invoice getInvoiceByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Invoice code");
		}
		Invoice invoice = invoiceRepository.findByCode(code);
		if(doPostValidation && null == invoice) {
			throw new ProvilacException("Invoice with this Code does not exists");
		}
		if(populateTransientFields)
			invoice= populateTransientFields(invoice, loggedInUser);
		if(fetchEagerly)
			invoice= fetchEagerly(invoice);

		return invoice;
	}

	/**
	 * returns @Invoice without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	@Transactional
	public Invoice getInvoice(Long id, boolean doPostValidation) {
		
		Invoice invoice;
		if(null == id) {
			throw new ProvilacException("Invalid invoice id");
		}
		invoice = invoiceRepository.findOne(id);
		if(doPostValidation && null == invoice) {
			throw new ProvilacException("invoice with this Id does not exists");
		}
		return invoice;
	}
	
	@Transactional
	public Invoice getLatestInvoiceByCustomer(Long customerId, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Invoice> page = null;
		PageRequest pageRequest = new PageRequest(0, 1, Direction.DESC, "id");
		page = invoiceRepository.findAll(InvoiceSpecifications.searchByCustomerId(customerId),pageRequest);
		
		if(page.hasContent()){
			Invoice invoice = page.getContent().get(0);
			if(populateTransientFields){
				invoice = populateTransientFields(invoice, loggedInUser);
			}
			if(fetchEagerly){
				invoice = fetchEagerly(invoice);
			}
			
			return invoice;
		}else{
			return null;
		}
	}
	
	/**
	 * 
	 * Updates the Invoice. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Invoice
	 * @param Invoice - Updated Invoice
	 * @param loggedInInvoice - LoggedIn Invoice
	 * @return Updated Invoice
	 * 
	 * @author Vishal
	 */
	@Transactional
	public Invoice updateInvoice(Long id, Invoice invoice, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Invoice id");
		}
		Invoice existingInvoice = invoiceRepository.findOne(id);
		if(null == existingInvoice) {
			throw new ProvilacException("Invoice with this Id does not exists");
		}
		existingInvoice.setCustomer(invoice.getCustomer());
		existingInvoice.setFromDate(invoice.getFromDate());
		existingInvoice.setLastPendingDues(invoice.getLastPendingDues());
		existingInvoice.setProvilacOutStanding(invoice.getProvilacOutStanding());
		existingInvoice.setToDate(invoice.getToDate());
		existingInvoice.setTotalAmount(invoice.getTotalAmount());
		existingInvoice.setIsPaid(invoice.getIsPaid());
		existingInvoice.setCollectionBoyNote(invoice.getCollectionBoyNote());
		if (null != invoice.getCollectionStatus()) {
			existingInvoice.setCollectionStatus(invoice.getCollectionStatus());
		}
		if (null != invoice.getReason()) {
			existingInvoice.setReason(invoice.getReason());
		}
		
		existingInvoice = invoiceRepository.save(existingInvoice);
		activityLogService.createActivityLog(loggedInUser, "update", "Invoice",id);
		existingInvoice.setCipher(EncryptionUtil.encode(invoice.getId()+""));
		if(populateTransientFields)
			existingInvoice =populateTransientFields(existingInvoice, loggedInUser);
		if(fetchEagerly)
			existingInvoice=fetchEagerly(existingInvoice);
		return existingInvoice;
	}
	
	public void deleteInvoice(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Invoice id");
		}
		Invoice invoice = invoiceRepository.findOne(id);
		if(null == invoice) {
			throw new ProvilacException("Invoice with this Id does not exists");
		}
		invoiceRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Invoice",id);
	}
	
	private Invoice populateTransientFields(Invoice invoice, User loggedInUser) {
		
		if(null == invoice) {
			return invoice;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Invoice");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			invoice.setIsEditAllowed(true);
			invoice.setIsDeleteAllowed(true);
		} else {
			invoice.setIsEditAllowed(false);
			invoice.setIsDeleteAllowed(false);
		}
		invoice.setCipher(EncryptionUtil.encode(invoice.getId()+""));
		return invoice;
	}
	
	private Invoice fetchEagerly(Invoice invoice) {
		if(null == invoice){
			return invoice;
		}
		invoice.getDeliverySchedule().size();
		for(DeliverySchedule deliverySchedule:invoice.getDeliverySchedule()){
			deliverySchedule.getDeliveryLineItems().size();
		}
		return invoice;
	}
	
}

