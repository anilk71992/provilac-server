package com.vishwakarma.provilac.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OneTimeOrder.OrderStatus;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.OneTimeOrderSpecification;
import com.vishwakarma.provilac.repository.OneTimeOrderRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;

@Service
public class OneTimeOrderService {

	@Resource
	private OneTimeOrderRepository oneTimeOrderRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
	private OrderLineItemService orderLineItemService;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Transactional
	public OneTimeOrder createOneTimeOrder(OneTimeOrder oneTimeOrder,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null==oneTimeOrder){
			throw new ProvilacException("Invalid Order");
		}

		oneTimeOrder=oneTimeOrderRepository.save(oneTimeOrder);
		activityLogService.createActivityLog(loggedInUser, "add", "OneTimeOrder", oneTimeOrder.getId());
		if(oneTimeOrder.getOrderLineItems().size()!=0){
			for(OrderLineItem orderLineItem:oneTimeOrder.getOrderLineItems()){
				orderLineItem.setOneTimeOrder(oneTimeOrder);
			}
		}
		 
		 orderLineItemService.createOrderLineItems(oneTimeOrder.getOrderLineItems(), loggedInUser,false, false);
		
		 firebaseWrapper.sendOneTimeOrderNotification(oneTimeOrder);
		 plivoSMSHelper.sendSMS(oneTimeOrder.getCustomer().getMobileNumber(), "Your order "+ oneTimeOrder.getId() +" of INR "+ oneTimeOrder.getFinalAmount() +"  has been placed. It is due for delivery on "+DateHelper.getFormattedDate(oneTimeOrder.getDate()) +". Feel free to contact us on 8411864646 for any queries.");
		 notificationHelper.sendNotificationForOneTimeOrder(oneTimeOrder.getCustomer(),oneTimeOrder);
		
		if(populateTransientFields||fetchEagerly){
			if(populateTransientFields){
				oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
			}
			if(fetchEagerly){
				oneTimeOrder=fetchEagerly(oneTimeOrder);
			}
		}
		return oneTimeOrder;
	}
	
	@Transactional
	public OneTimeOrder createOneTimeOrders(OneTimeOrder oneTimeOrder,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null==oneTimeOrder){
			throw new ProvilacException("Invalid Order");
		}

		oneTimeOrder=oneTimeOrderRepository.save(oneTimeOrder);
		activityLogService.createActivityLog(loggedInUser, "add", "OneTimeOrder", oneTimeOrder.getId());
		if(oneTimeOrder.getOrderLineItems().size()!=0){
			for(OrderLineItem orderLineItem:oneTimeOrder.getOrderLineItems()){
				orderLineItem.setOneTimeOrder(oneTimeOrder);
			}
		}
		 
		 orderLineItemService.createOrderLineItems(oneTimeOrder.getOrderLineItems(), loggedInUser,false, false);
		
		if(populateTransientFields||fetchEagerly){
			if(populateTransientFields){
				oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
			}
			if(fetchEagerly){
				oneTimeOrder=fetchEagerly(oneTimeOrder);
			}
		}
		return oneTimeOrder;
	}
	
	@Transactional
	public List<OneTimeOrder> getAllOneTimeOrders(User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		List<OneTimeOrder> oneTimeOrders=oneTimeOrderRepository.findAll();
		if(populateTransientFields||fetchEagerly){

			for(OneTimeOrder oneTimeOrder:oneTimeOrders){
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
			
		}
		return oneTimeOrders;
	}
	
	@Transactional
	public List<OneTimeOrder> getOneTimeOrderByDate(Date date,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
	List<OneTimeOrder> oneTimeOrders = oneTimeOrderRepository.findByDate(date);	
	if (populateTransientFields || fetchEagerly) {
		for (OneTimeOrder oneTimeOrder : oneTimeOrders ) {
			if(populateTransientFields)
				oneTimeOrder = populateTransientFields(oneTimeOrder, loggedInUser);
			else
				oneTimeOrder = fetchEagerly(oneTimeOrder);
		}
		
	 }
	return oneTimeOrders;
	}
	
	/**
	 * This method will not check for status if status param is null. Do not use this method if you want to 
	 * get orders with null status.
	 * @return
	 */
	@Transactional
	public List<OneTimeOrder> getTodayOneTimeOrders(OrderStatus status, Long deliveryBoyId, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly){
		
		
		
		Date date = new Date();
		DateHelper.setToStartOfDay(date);
		
		List<OneTimeOrder> orders = oneTimeOrderRepository.findByDeliveryBoyIdAndDate(deliveryBoyId,date);
		List<OneTimeOrder> ordersDate = oneTimeOrderRepository.findByDeliveryBoyIdAndDate(null,date);
		orders.addAll(ordersDate);
		Set<OneTimeOrder> oneTimeOrderSet=new HashSet<OneTimeOrder>(orders);
		List<OneTimeOrder> ordersList=new ArrayList<OneTimeOrder>(oneTimeOrderSet);
		
		if(populateTransientFields||fetchEagerly){

			for(OneTimeOrder oneTimeOrder:ordersList){
				
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
			
		}
		return ordersList;
	}
	
	@Transactional
	public List<OneTimeOrder> getTodayOneTimeOrdersByStatus(OrderStatus status,User loggedInUser, boolean populateTransientFields,boolean fetchEagerly){
		
		Date date = new Date();
		DateHelper.setToStartOfDay(date);
		
		List<OneTimeOrder> orders = oneTimeOrderRepository.findByStatusAndDate(status.name(), date);
		
		if(populateTransientFields||fetchEagerly){

			for(OneTimeOrder oneTimeOrder:orders){
				
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
			
		}
		return orders;
	}
	
	@Transactional
	public List<OneTimeOrder> getTodayOneTimeOrdersByDeliveryBoy(Long deliveryBoyId, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly){
		
		
		
		Date date = new Date();
		DateHelper.setToStartOfDay(date);
		
		List<OneTimeOrder> orders = oneTimeOrderRepository.findByDeliveryBoyIdAndDate(deliveryBoyId,date);
		
		if(populateTransientFields||fetchEagerly){

			for(OneTimeOrder oneTimeOrder:orders){
				
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
			
		}
		return orders;
	}
	
	@Transactional
	public Page<OneTimeOrder> getTodayOneTimeOrders(Integer pageNumber,OrderStatus status, Long deliveryBoyId,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Page<OneTimeOrder> page=null;
		PageRequest pageRequest=new PageRequest(pageNumber-1,AppConstants.PAGE_SIZE,Direction.DESC,"id");
		
		Date date = new Date();
		DateHelper.setToStartOfDay(date);
		
		page=oneTimeOrderRepository.findByDate(date, pageRequest);
		Iterator<OneTimeOrder> iterator=page.iterator();
		if(populateTransientFields||fetchEagerly){
			while(iterator.hasNext()){
				OneTimeOrder oneTimeOrder=(OneTimeOrder)iterator.next();
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
		}
		
		return page;
	}
	@Transactional
	public OneTimeOrder getOneTimeOrder(Long id,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		OneTimeOrder oneTimeOrder=oneTimeOrderRepository.findOne(id);
		if(populateTransientFields||fetchEagerly){
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			
		}
		return oneTimeOrder;
	}
	
	@Transactional
	public OneTimeOrder getOneTimeOrderByCode(String code,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		OneTimeOrder oneTimeOrder=oneTimeOrderRepository.findByCode(code);
		if(populateTransientFields||fetchEagerly){
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			
		}
		return oneTimeOrder;
	}
	
	@Transactional
	public OneTimeOrder getOneTimeOrderByTxnId(String txnId,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		OneTimeOrder oneTimeOrder=oneTimeOrderRepository.findByTxnId(txnId);
		if(populateTransientFields||fetchEagerly){
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			
		}
		return oneTimeOrder;
	}
	
	@Transactional
	public List<OneTimeOrder> getOneTimeOrderByCustomer(Long id,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		List<OneTimeOrder> oneTimeOrders=oneTimeOrderRepository.findByCustomer_Id(id);
		if(populateTransientFields||fetchEagerly){

			for(OneTimeOrder oneTimeOrder:oneTimeOrders){
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
			
		}
		return oneTimeOrders;
	}
	
	
	@Transactional
	public Page<OneTimeOrder> searchOneTimeOrder(Integer pageNumber,String searchTerm,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Page<OneTimeOrder> page=null;
		PageRequest pageRequest=new PageRequest(pageNumber-1,AppConstants.PAGE_SIZE,Direction.DESC,"date");
		page=oneTimeOrderRepository.findAll(OneTimeOrderSpecification.search(searchTerm),pageRequest);
		Iterator<OneTimeOrder> iterator=page.iterator();
		if(populateTransientFields||fetchEagerly){
			while(iterator.hasNext()){
				OneTimeOrder oneTimeOrder=(OneTimeOrder)iterator.next();
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
		}
		
		return page;
	}
	
	@Transactional
	public Page<OneTimeOrder> getOneTimeOrders(Integer pageNumber,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Page<OneTimeOrder> page=null;
		PageRequest pageRequest=new PageRequest(pageNumber-1,AppConstants.PAGE_SIZE,Direction.DESC,"date");
		page=oneTimeOrderRepository.findAll(pageRequest);
		Iterator<OneTimeOrder> iterator=page.iterator();
		if(populateTransientFields||fetchEagerly){
			while(iterator.hasNext()){
				OneTimeOrder oneTimeOrder=(OneTimeOrder)iterator.next();
				if(populateTransientFields){
					oneTimeOrder=populateTransientFields(oneTimeOrder, loggedInUser);
				}
				if(fetchEagerly){
					oneTimeOrder=fetchEagerly(oneTimeOrder);
				}
			}
		}
		
		return page;
	}
	
	@Transactional
	public OneTimeOrder updateOneTimeOrder(Long id,OneTimeOrder oneTimeOrder,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(id==null){
			throw new ProvilacException("Invalid Id");
		}
		
		OneTimeOrder existingOneTimeOrder=oneTimeOrderRepository.findOne(id);
		
		if(existingOneTimeOrder==null){
			throw new ProvilacException("Order With This Id Does Not Exist");
		}
		existingOneTimeOrder.setPriority(oneTimeOrder.getPriority());
		existingOneTimeOrder.setStatus(oneTimeOrder.getStatus());
		existingOneTimeOrder.setDeliveryBoy(oneTimeOrder.getDeliveryBoy());
		existingOneTimeOrder.setDeliveryTime(oneTimeOrder.getDeliveryTime());
		
		existingOneTimeOrder=oneTimeOrderRepository.save(existingOneTimeOrder);
		activityLogService.createActivityLog(loggedInUser, "update", "OneTimeOrder",id);
		
		if(populateTransientFields||fetchEagerly){
			if(populateTransientFields){
				existingOneTimeOrder=populateTransientFields(existingOneTimeOrder, loggedInUser);
			}
			if(fetchEagerly){
				existingOneTimeOrder=fetchEagerly(existingOneTimeOrder);
			}
		}
		return existingOneTimeOrder;
	}
	
	
	@Transactional
	public OneTimeOrder updatePGStatus(String txnId, boolean isApproved, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		OneTimeOrder oneTimeOrder = oneTimeOrderRepository.findByPaymentOrderId(txnId);
		if(null != oneTimeOrder){
			
			oneTimeOrder.setApprovedByPG(isApproved);
			oneTimeOrder = oneTimeOrderRepository.save(oneTimeOrder);
		}
		
		return oneTimeOrder;
	}
	
	private OneTimeOrder populateTransientFields(OneTimeOrder oneTimeOrder,User loggedInUser){
		
		if(loggedInUser==null){
			throw new ProvilacException("Invalid Log-in User");
		}
		
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)){
			oneTimeOrder.setIsEditAllowed(true);
			oneTimeOrder.setIsDeleteAllowed(true);
		}else{
			oneTimeOrder.setIsEditAllowed(false);
			oneTimeOrder.setIsDeleteAllowed(false);
		}
		
		oneTimeOrder.setCipher(EncryptionUtil.encode(oneTimeOrder.getId()+""));
		
		return oneTimeOrder;
	}
	
	private OneTimeOrder fetchEagerly(OneTimeOrder oneTimeOrder){
		if(oneTimeOrder!=null){
		oneTimeOrder.getOrderLineItems().size();
		}
		return oneTimeOrder;
	}
}
