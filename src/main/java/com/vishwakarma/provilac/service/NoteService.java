package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Note;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.NoteSpecifications;
import com.vishwakarma.provilac.repository.NoteRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class NoteService {

	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private NoteRepository noteRepository;
	@Transactional
	public Note createNote(Note note, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == note) {
			throw new ProvilacException("Invalid note");
		}
		note = noteRepository.save(note);
		activityLogService.createActivityLog(loggedInUser, "add", "Note", note.getId());
		
		if (populateTransientFields) {
			note = populateTransientFields(note, loggedInUser);
		}
		
		if(fetchEagerly) {
			note= fetchEagerly(note);
		}
		return note;
	}
	
	@Transactional
	public List<Note> getAllNotes(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<Note> notes = noteRepository.findAll();
		if (populateTransientFields || fetchEagerly) {
			for (Note note : notes) {
				if (populateTransientFields)
					note = populateTransientFields(note, loggedInUser);
				if (fetchEagerly)
					note = fetchEagerly(note);
			}
		}
		return notes;
	}
	
	@Transactional
	public List<Note> getAllNotesByCustomer(String customerCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<Note> notes = noteRepository.findByCustomerCode(customerCode);
		if (populateTransientFields || fetchEagerly) {
			for (Note note : notes) {
				if (populateTransientFields)
					note = populateTransientFields(note, loggedInUser);
				if (fetchEagerly)
					note = fetchEagerly(note);
			}
		}
		return notes;
	}
	
	
	@Transactional
	public Page<Note> getAllNotes(User loggedInUser,Integer pageNumber,boolean populateTransientFields, boolean fetchEagerly) {

		Page<Note> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = noteRepository.findAll(pageRequest);
		if (populateTransientFields || fetchEagerly) {
			for (Note note : page) {
				if (populateTransientFields)
					note = populateTransientFields(note, loggedInUser);
				if (fetchEagerly)
					note = fetchEagerly(note);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Note> searchNote(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Note> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = noteRepository.findAll(NoteSpecifications.search(searchTerm), pageRequest);
		Iterator<Note> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Note note = (Note) iterator.next();
			if(populateTransientFields)
				note = populateTransientFields(note, loggedInUser);
			if(fetchEagerly)
				note = fetchEagerly(note);
			}
		}
		return page;
	}
	@Transactional
	public Note getNoteById(Long id, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		Note note=noteRepository.findOne(id);
		if(null != note){
			if(populateTransientFields){
				note=populateTransientFields(note, loggedInUser);
			if(fetchEagerly){
				note=fetchEagerly(note);
			}
			}
		}
		return note;
	}
	
	public void deleteNote(Long id) {
		if(null == id) {
			throw new ProvilacException("Invalid note id");
		}
		Note note = noteRepository.findOne(id);
		if(null == note) {
			throw new ProvilacException("Note with this id does not exists");
		}
		noteRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Note",id);
	}
	
	private Note populateTransientFields(Note note, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			note.setIsEditAllowed(true);
			note.setIsDeleteAllowed(true);
		} else {
			note.setIsEditAllowed(false);
			note.setIsDeleteAllowed(false);
		}
		note.setCipher(EncryptionUtil.encode(note.getId()+""));
		return note;
	}
	
	private Note fetchEagerly(Note note) {
		return note;
	}
	
}

