
package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;
import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.model.specifications.UserSpecifications;
import com.vishwakarma.provilac.repository.UserRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.CommonUtils;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.Md5Util;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;



@Service
public class UserService {

	@Resource
	private UserRepository userRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public User createUser(User user, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == user) {
			throw new ProvilacException("Invalid user");
		}
		if(StringUtils.isBlank(user.getMobileNumber())) {
			throw new ProvilacException("Invalid mobile number.");
		}
		String password= user.getPassword();
		if(StringUtils.isBlank(password)){
			password = generateHashWithRandomString(user.getMobileNumber());
		}
		user.setPassword(Md5Util.md5(password));
		user.setIsEnabled(true);
		User existingUser = getUserByMobileNumber(user.getMobileNumber(), false, null, false, false);
		if(null != existingUser) {
			throw new ProvilacException("User with same mobile number already exists");
		}
		user.setReferralCode(getReferralCode(user));
		user = userRepository.save(user);
		
		activityLogService.createActivityLog(user, "register", "User", user.getId());
				
		if(populateTransientFields) {
			user = populateTransientFields(user, loggedInUser);
		}
		if(fetchEagerly) {
			user = fetchEagerly(user);
		}
		return user;
	}
	
	@Transactional
	public List<User> getAllUsers(User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<User> users = userRepository.findAll();
		if(populateTransientFields || fetchEagerly) {
			for (User user : users) {
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}
	
	@Transactional
	public List<User> getAllUsersByIsAssignedRoute(boolean isRouteAssigned, boolean populateTransientFields, boolean fetchEagerly) {
		List<User> users = userRepository.findByIsRouteAssigned(isRouteAssigned);
		return users;
	}
	
	@Transactional
	@Deprecated
	public Page<User> getUsers(Integer pageNumber,String role, User loggedInUser) {
		
		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(pageRequest);
		} else {
			page = userRepository.findUserByRole(roleCache.getRole(role).getId(), pageRequest);
		}
		Iterator<User> iterator = page.iterator();
		while (iterator.hasNext()) {
			User user = (User) iterator.next();
			user = populateTransientFields(user, loggedInUser);
		}
		return page;
	}
	
	@Transactional
	public List<User> getUsersForRole(String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		List<User> users = userRepository.findByRole(roleCache.getRole(role).getId());
		if(populateTransientFields || fetchEagerly) {
			for (User user : users) {
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}
	
	@Transactional
	public List<User> getUsersByStatus(User.Status status) {
		List<User> users = userRepository.findByStatus(status);
		return users;
	}
	
	@Transactional
	public List<User>  getUserByCustomerRoleAndStatus(String role, Status status){
		List<User> customer = userRepository.findUserByRoleAndStatus(roleCache.getRole(role).getId(), status);
		return customer;
	}
	
	@Transactional
	public List<User> getInactiveAndIsRouteAssignedCustomers(User.Status status,boolean isRouteAssigned) {
		List<User> users = userRepository.findByStatusAndIsRouteAssigned(status,isRouteAssigned);
		return users;
	}
	
	@Transactional
	public List<User> getUsersByAssignedToUser(String assigedToUserCode) {
		List<User> users = userRepository.findByAssignedTo_Code(assigedToUserCode);
		return users;
	}
	
	@Transactional
	public List<User> getUsersAssignedToUserAndStatus(String assigedToUserCode,Status status) {
		List<User> users = userRepository.findByAssignedTo_CodeAndStatus(assigedToUserCode, status);
		return users;
	}
	
	@Transactional
	public Page<User> getUsersByRoleAndStatus(Integer pageNumber, String role, Status status, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){

		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(pageRequest);
		} else {
			page = userRepository.findUserByRoleAndStatus(roleCache.getRole(role).getId(), status, pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<User> getUsersByStatusNotEqualToNew(Integer pageNumber, Status status, String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){

		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(pageRequest);
		} else {
			page= userRepository.findUserByRoleAndStatusNotEqualToNew(roleCache.getRole(role).getId(), status, pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}
	
	@Transactional
	public List<User> getUsersByRoleAndStatusNotEqualToActive(Status status, String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){

		List<User>	users= userRepository.findAll(UserSpecifications.getPredicateForRoleAndStatusNotEqual(role, status));
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}

	@Transactional
	public List<User> getActiveAndOnHoldUsers(String role, Status activeStatus, Status holdStatus, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		List<User> users = userRepository.findAll(UserSpecifications.searchCustomersByStatusActiveAndHold(activeStatus, holdStatus, role));
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}
	
	@Transactional
	public List<User> getUsersByStatusAndCreatedDate(Status status, Date fromDate, Date toDate, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		List<User> users = userRepository.findAll(UserSpecifications.searchByStatusAndCreatedDate(status, fromDate, toDate));
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}
	
	@Transactional
	public List<User> getUsersByRoleAndStatus(String role, Status status, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		List<User> users = userRepository.findUserByRoleAndStatus(roleCache.getRole(role).getId(), status);
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}

	@Transactional
	public Page<User> getUsersByIsRouteAssignedAndStatus(Integer pageNumber, String searchTerm, String role, Status status, Boolean isRouteAssigned, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){

		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (searchTerm != null) {
			page = userRepository.findAll(UserSpecifications.searchBySearchTermAndRoleAndIsRouteAssignedAndStatus(searchTerm, role, isRouteAssigned, status), pageRequest);
		} else {
			page = userRepository.findUserByRoleIsRouteAssignAndStatus(roleCache.getRole(role).getId(), status,null, pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<User> searchUsersExceptUserRoleAsCustomer(Integer pageNumber, String searchTerm,String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (searchTerm != null) {
			page = userRepository.findAll(UserSpecifications.searchBySearchTermAndNotCustomerRole(searchTerm, role), pageRequest);
		} else {
			page = userRepository.findUserByRoleNotEqual(roleCache.getRole(role).getId(), pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}

	/**
	 * Method to get Customers whoes pending dues are greater than zero
	 * @param pageNumber
	 * @param searchTerm
	 * @param role
	 * @param isRouteAssigned
	 * @param loggedInUser
	 * @return
	 */
	@Transactional
	public Page<User> getUsersByIsRouteAssignedFalseAndDuesPending(Integer pageNumber, String searchTerm, String role, Boolean isRouteAssigned, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){

		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (searchTerm != null) {
			page = userRepository.findAll(UserSpecifications.searchBySearchTermAndRoleAndIsRouteAssignedAndPendingDues(searchTerm, role, isRouteAssigned, 0), pageRequest);
		} else {
			page = userRepository.findUserByRoleIsRouteAssignAndLastPendingDues(roleCache.getRole(role).getId(), 0,false, pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}

	@Transactional
	public List<User> getListOfUsersExceptUserRoleAsCustomer(String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		List<User> users = userRepository.findUserByRoleNotEqualToCustomer(roleCache.getRole(role).getId());
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return users;
	}
	
	@Transactional
	public Page<User> searchUsers(Integer pageNumber, String searchTerm, String role, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<User> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		if (role == null) {
			page = userRepository.findAll(UserSpecifications.search(searchTerm), pageRequest);
		}else{
			page = userRepository.findAll(UserSpecifications.searchBySearchTermAndRole(searchTerm, role),pageRequest);
		}
		if(populateTransientFields || fetchEagerly) {
			Iterator<User> iterator = page.iterator();
			while (iterator.hasNext()) {
				User user = (User) iterator.next();
				if(populateTransientFields)
					user = populateTransientFields(user, loggedInUser);
				if(fetchEagerly)
					user = fetchEagerly(user);
			}
		}
		return page;
	}
	
	@Transactional
	public User getUser(Long id, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User user = userRepository.findOne(id);
		if(doPostValidation && null == user) {
			throw new ProvilacException("User with this Id does not exists");
		}
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}

	@Transactional
	public User getUserByCode(String code, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(StringUtils.isBlank(code)) {
			throw new ProvilacException("Invalid user code");
		}
		User user = userRepository.findByCode(code);
		if(doPostValidation && null == user) {
			throw new ProvilacException("User with this Id does not exists");
		}
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}
	
	public User getSuperUser() {
		User user = userRepository.findByRole(roleCache.getRole(Role.ROLE_SUPER_ADMIN).getId()).get(0);
		return user;
	}

	/**
	 * 
	 * @param email/mobileNumber
	 * @param doPostValidation
	 * @return User otherwise null
	 */
	@Transactional
	public User getUserByUserNameOrMobileNumber(String email, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		User user = null;
		if(StringUtils.isBlank(email)) {
			throw new ProvilacException("Invalid username");
		}
		List<User> users = userRepository.findAll(Specifications.where(UserSpecifications.getPredicateByMobileOrUserNameOrEmail(email)));
		if(users.isEmpty()){
			return user;
		}
		user = users.get(0);
		if(doPostValidation && null == user) {
			throw new ProvilacException("User with this email does not exists");
		}
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}
	
	/**
	 * returns @User without Post validation & without populating transient fields
	 * @param email
	 * @return
	 */
	@Transactional
	public User getUserByEmail(String email, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		User user;
		if(StringUtils.isBlank(email)) {
			throw new ProvilacException("Invalid email");
		}
		user = userRepository.findByEmail(email);
		if(doPostValidation && null == user) {
			throw new ProvilacException("User with this email does not exists");
		}
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}
	
	/**
	 * returns @User without Post validation & without populating transient fields
	 * @param mobileNumber
	 * @return
	 */
	@Transactional
	public User getUserByMobileNumber(String mobileNumber, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		User user;
		if(StringUtils.isBlank(mobileNumber)) {
			throw new ProvilacException("Invalid mobile number.");
		}
		user = userRepository.findByMobileNumber(mobileNumber);
		if(doPostValidation && null == user) {
			throw new ProvilacException("User with this mobile number does not exists.");
		}
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}
	
	/**
	 * @return <b>FIRST</b> user with matching referralCode, otherwise returns null
	 */
	@Transactional
	public User getUserByReferralCode(String referralCode, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<User> users = userRepository.findByReferralCode(referralCode);
		if(users.isEmpty() && doPostValidation) {
			throw new ProvilacException("User with this referral code does not exists");
		} else if(users.isEmpty()) {
			return null;
		}
		
		User user = users.get(0);
		if(populateTransientFields)
			user = populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		
		return user;	
	}
	
	/**
	 * 
	 * Updates the user. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing user
	 * @param user - Updated User
	 * @param loggedInUser - LoggedIn User
	 * @return Updated User
	 * 
	 * @author Vishal
	 */
	@Transactional
	public User updateUser(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		
		if(StringUtils.isNotBlank(user.getEmail())){
			existingUser.setEmail(user.getEmail());
		}
		if(StringUtils.isNotBlank(user.getVerificationCode())){
			existingUser.setVerificationCode(user.getVerificationCode());
		}
		existingUser.setAlterNateMobileNumber(user.getAlterNateMobileNumber());
		existingUser.setDob(user.getDob());
		existingUser.setGender(user.getGender());
		if(null != user.getReferredBy()){
			existingUser.setReferredBy(user.getReferredBy());
			existingUser.setReferredMedia(user.getReferredMedia());
		}
		if(StringUtils.isNotBlank(user.getReferralCode())){
			existingUser.setReferralCode(user.getReferralCode());
		}
		existingUser.setAllocatedFirstTimeReferralPoints(user.getAllocatedFirstTimeReferralPoints());
		existingUser.setAccumulatedPoints(user.getAccumulatedPoints());
		existingUser.setLastPendingDues(user.getLastPendingDues());
		existingUser.setRemainingPrepayBalance(user.getRemainingPrepayBalance());
		if(null != user.getProfilePicData()){
			existingUser.setProfilePicData(user.getProfilePicData());
			existingUser.setFileName(user.getFileName());
		}
		existingUser.setStatus(user.getStatus());
		existingUser.setIsRouteAssigned(user.getIsRouteAssigned());
		existingUser.setReasonToStop(user.getReasonToStop());
		existingUser.createProfilePicFile();
		
		//assuming customer not giving blank address and building address
		if(StringUtils.isNotBlank(user.getAddress()))
			existingUser.setAddress(user.getAddress());
		if(StringUtils.isNotBlank(user.getBuildingAddress()))
			existingUser.setBuildingAddress(user.getBuildingAddress());
		
		if(user.getUserRoles().size()>0)
		existingUser.getUserRoles().clear();
		for (Role role : user.getUserRoles()) {
			existingUser.getUserRoles().add(role);
		}
		existingUser.setEnabled(user.getIsEnabled());
		if(null !=user.getPaymentMethod())
			existingUser.setPaymentMethod(user.getPaymentMethod());
		existingUser = userRepository.save(existingUser);
		if(existingUser.hasAnyRole(Role.ROLE_CUSTOMER)){
			activityLogService.createActivityLog(existingUser, "Update", "Customer", user.getId());
		}else{
			activityLogService.createActivityLog(existingUser, "Update", "User", user.getId());
		}
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public User updateCustomer(User user, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		user = userRepository.save(user);
		if(populateTransientFields)
			user =populateTransientFields(user, loggedInUser);
		if(fetchEagerly)
			user = fetchEagerly(user);
		return user;
	}
	
	@Transactional
	public User updateLocation(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		existingUser.setLat(user.getLat());
		existingUser.setLng(user.getLng());
	
		existingUser = userRepository.save(existingUser);
		activityLogService.createActivityLog(existingUser, "update location", "User", existingUser.getId());
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public User updateReferralPoints(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		existingUser.setAccumulatedPoints(user.getAccumulatedPoints());
		existingUser.setAllocatedFirstTimeReferralPoints(user.getAllocatedFirstTimeReferralPoints());
	
		existingUser = userRepository.save(existingUser);
		activityLogService.createActivityLog(existingUser, "update referral points", "customer.", existingUser.getId());
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public User updatePasswordForUser(Long id, User user, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		
		existingUser.setPassword(user.getPassword());
		
		existingUser = userRepository.save(existingUser);
		activityLogService.createActivityLog(existingUser, "update password", "User", existingUser.getId());
		existingUser.setCipher(EncryptionUtil.encode(user.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public User updateLastPendingDuesForUser(Long id, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		
		existingUser.setLastPendingDues(0.0);
		
		existingUser = userRepository.save(existingUser);
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public User updateRemmaingPrePaid(Long id, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("User with this Id does not exists");
		}
		
		existingUser.setRemainingPrepayBalance(loggedInUser.getRemainingPrepayBalance());
		existingUser.setLastPendingDues(loggedInUser.getLastPendingDues());
		
		existingUser = userRepository.save(existingUser);
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	/**
	 * Update Collection Boy OutStanding amount
	 * @param id
	 * @param collectionBoy
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	@Transactional
	public User updateCollectionBoyCollectionOutstanding(Long id, User collectionBoy, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid CollectionBoy id");
		}
		User existingUser = userRepository.findOne(id);
		if(null == existingUser) {
			throw new ProvilacException("CollectionBoy with this Id does not exists");
		}
		existingUser.setCollectionOuststanding(collectionBoy.getCollectionOuststanding());
		
		existingUser = userRepository.save(existingUser);
		activityLogService.createActivityLog(existingUser, "update", "Collection Boy",existingUser.getId());
		existingUser.setCipher(EncryptionUtil.encode(collectionBoy.getId()+""));
		if(populateTransientFields)
		  existingUser =populateTransientFields(existingUser, loggedInUser);
		if(fetchEagerly)
			existingUser = fetchEagerly(existingUser);
		return existingUser;
	}
	
	@Transactional
	public void deleteUser(Long id, User loggedInUser) {
		
		if(null == id) {
			throw new ProvilacException("Invalid user id");
		}
		User user = userRepository.findOne(id);
		if(null == user) {
			throw new ProvilacException("User with this Id does not exists");
		}
		userRepository.delete(id);
		activityLogService.createActivityLog(loggedInUser, "deleted", "User", id);
	}
	
	private User populateTransientFields(User user, User loggedInUser) {
		
		if(null == user) {
			return user;
		}
	/*	if(null == loggedInUser) {
			throw new ProvilacException("Invalid user");
		}*/
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			user.setIsEditAllowed(true);
			user.setIsDeleteAllowed(true);
		} else {
			user.setIsEditAllowed(false);
			user.setIsDeleteAllowed(false);
		}
		user.setCipher(EncryptionUtil.encode(user.getId()+""));
		return user;
	}
	
	
	public boolean checkPassword(Long userId, String password) {
		User user = userRepository.findOne(userId);
		return Md5Util.md5(password).equals(user.getPassword());
	}
	
	public void changePassword(String newPassword) {
		
		UserInfo userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userRepository.findOne(userInfo.getId());
		if(null == user) {
			throw new ProvilacException("User not found");
		}
		user.setPassword(Md5Util.md5(newPassword));
		userRepository.save(user);
		activityLogService.createActivityLog(user, "change Password", "User", user.getId());
	}
	
	public String recoverPassword(Long userId) {
		
		User user = getUser(userId, true, null, false, false);
		String password = RandomStringUtils.randomAlphanumeric(AppConstants.PASSWORD_LENGTH);
		user.setPassword(Md5Util.md5(password));
		userRepository.save(user);
		return password;
	}
	
	public User getLoggedInUser() {
		
		UserInfo userInfo = getLoggedInUserInfo();
		if(null == userInfo) {
			throw new ProvilacException("Invalid logged-in user information");
		}
		User user = getUser(userInfo.getId(), true, null, false, false);
		return user;
	}

	/**
	 * @return
	 */
	public UserInfo getLoggedInUserInfo() {
		UserInfo userInfo;
		try {
			userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ProvilacException("Error retrieving logged in user information");
		}
		return userInfo;
	}
	
	private User fetchEagerly(User user) {
		user.getUserRoles().size();
		
		return user;
		
	}
	
	@Transactional
	public String generateHashWithRandomString(String mobileNumber) {

	StringBuilder builder = new StringBuilder();
	builder.append(mobileNumber);
	builder.append("aZbwAcdR");
	return String.valueOf(builder.toString().hashCode());
	}
	
	
	@Transactional
	public List<User> searchUsersByKeyword(String searchTerm, User loggedInUser) {
		
			List<User> users = userRepository.findAll(UserSpecifications.search(searchTerm));
		for(User user:users){
			user=fetchEagerly(user);
		}
		return users;
	}
	
	@Transactional
	public String generateRandomString(String email) {
		StringBuilder builder = new StringBuilder();
		builder.append(email);
		builder.append("-aZbwAcdR");
		return String.valueOf(builder.toString());
		}
	
	public String getReferralCode(User user){
		
		String referralCode=CommonUtils.getReferralCodeForUser(user);
		if(StringUtils.isNotBlank(referralCode)){
			List<User> existingUsers=userRepository.findByReferralCode(referralCode);
			if(existingUsers.size()>0){
				return getReferralCode(user);
			}
		}
		
		return referralCode;
	}
}
