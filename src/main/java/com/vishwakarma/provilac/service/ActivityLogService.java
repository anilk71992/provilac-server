package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.ActivityLog;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.ActivityLogSpecifications;
import com.vishwakarma.provilac.repository.ActivityLogRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

/**
 * 
 * @author Harshal
 *
 */

@Service
public class ActivityLogService {

	@Resource
	private ActivityLogRepository activityLogRepository;
	
	/**
	 * Add Activity log
	 * @param user
	 * @param activity
	 * @param entityType
	 * @param entityId
	 */
	
	public void createActivityLog(User user,String activity,String entityType,Long entityId) {
		ActivityLog activityLog = new ActivityLog();
		activityLog.setUser(user);
		activityLog.setActivity(activity);
		activityLog.setEntityType(entityType);
		activityLog.setEntityId(entityId);
		activityLog.setDate(new Date());
		activityLog = activityLogRepository.save(activityLog);
	}
	
	public void createActivityLogForChangeStatus(User user,String activity, String changeStatus, String entityType,Long entityId) {
		ActivityLog activityLog = new ActivityLog();
		activityLog.setUser(user);
		activityLog.setActivity(activity);
		activityLog.setEntityType(entityType);
		activityLog.setEntityId(entityId);
		activityLog.setChangeStatus(changeStatus);
		activityLog.setDate(new Date());
		activityLog = activityLogRepository.save(activityLog);
	}
	
	/**
	 * 
	 * Method to get all activity logs
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return activityLogs
	 */
	@Transactional
	public List<ActivityLog> getActivityLogs(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<ActivityLog> activityLoges = activityLogRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (ActivityLog activityLog : activityLoges) {
			if(populateTransientFields)
				activityLog = populateTransientFields(activityLog, loggedInUser);
			if (fetchEagerly)
				activityLog = fetchEagerly(activityLog);
			}
		}
		return activityLoges;
	}
	
	
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return page of activityLoges with specified pageNumber
	 */
	@Transactional
	public Page<ActivityLog> getActivityLogs(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<ActivityLog> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = activityLogRepository.findAll(pageRequest);
		Iterator<ActivityLog> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			ActivityLog activityLog = (ActivityLog) iterator.next();
			if(populateTransientFields)
				activityLog = populateTransientFields(activityLog, loggedInUser);
			if(fetchEagerly)
				activityLog = fetchEagerly(activityLog);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param searchTerm
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return activityLog or list of activityLoges according to search term 
	 */
	@Transactional
	public Page<ActivityLog> searchActivityLogs(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<ActivityLog> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = activityLogRepository.findAll(ActivityLogSpecifications.search(searchTerm), pageRequest);
		Iterator<ActivityLog> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				ActivityLog activityLog = (ActivityLog) iterator.next();
				if(populateTransientFields)
					activityLog = populateTransientFields(activityLog, loggedInUser);
				if(fetchEagerly)
					activityLog = fetchEagerly(activityLog);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param id
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return activityLog
	 */
	@Transactional
	public ActivityLog getActivityLog(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid ActivityLog id");
		}
		ActivityLog activityLog = activityLogRepository.findOne(id);
		if(doPostValidation && null == activityLog) {
			throw new ProvilacException("ActivityLog with this Id does not exists");
		}
		if(null == activityLog){
			return null;
		}
		if(populateTransientFields)
			activityLog= populateTransientFields(activityLog, loggedInUser);
		if(fetchEagerly)
			activityLog= fetchEagerly(activityLog);
		
		return activityLog;
	}
	
	@Transactional
	public List<ActivityLog> getActivityLogsByDateAndStatus(String status,Date fromDate,Date toDate,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<ActivityLog> activityLogs = null;
		activityLogs = activityLogRepository.findAll(ActivityLogSpecifications.searchByDateRangeAndChangeStatus(status, fromDate, toDate));
		Iterator<ActivityLog> iterator = activityLogs.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			ActivityLog activityLog = (ActivityLog) iterator.next();
			if(populateTransientFields)
				activityLog = populateTransientFields(activityLog, loggedInUser);
			if(fetchEagerly)
				activityLog = fetchEagerly(activityLog);
			}
		}
		return activityLogs;
	}
	
	@Transactional
	public List<ActivityLog> getActivityLogsByEntityIdAndStatus(Long entityId,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<ActivityLog> activityLogs = null;
		activityLogs = activityLogRepository.findByEntityIdAndChangeStatusNotNull(entityId);
		Iterator<ActivityLog> iterator = activityLogs.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			ActivityLog activityLog = (ActivityLog) iterator.next();
			if(populateTransientFields)
				activityLog = populateTransientFields(activityLog, loggedInUser);
			if(fetchEagerly)
				activityLog = fetchEagerly(activityLog);
			}
		}
		return activityLogs;
	}
	
	@Transactional
	public void deleteAllActivitiesOfUser(Long userId) {
		activityLogRepository.deleteByUser(userId);
	}
	
	private ActivityLog populateTransientFields(ActivityLog activityLog, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid ActivityLog");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			activityLog.setIsEditAllowed(true);
			activityLog.setIsDeleteAllowed(true);
		} else {
			activityLog.setIsEditAllowed(false);
			activityLog.setIsDeleteAllowed(false);
		}
		activityLog.setCipher(EncryptionUtil.encode(activityLog.getId()+""));
		return activityLog;
	}
	
	private ActivityLog fetchEagerly(ActivityLog activityLog) {
		
		return activityLog;
	}

	
}

