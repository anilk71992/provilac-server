package com.vishwakarma.provilac.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.ProductSpecifications;
import com.vishwakarma.provilac.repository.ProductRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class ProductService {

	@Resource
	private ProductRepository productRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	public Product createProduct(Product product, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == product) {
			throw new ProvilacException("Invalid product");
		}
		product = productRepository.save(product);
		activityLogService.createActivityLog(loggedInUser, "add", "Product", product.getId());
		
		//update service Pic
		if(null != product.getPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_PICS_FOLDER,product.getId()+"", product.getPicData());
		}
		
		//update service Banner
		if(null != product.getBannerData()){
				appFileUtils.saveData(AppConstants.PRODUCT_BANNERS_FOLDER,product.getId()+"", product.getBannerData());
		}
		
		//update service product cart pic
		if(null != product.getProductCartPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_CART_PICS_FOLDER,product.getId()+"", product.getProductCartPicData());
		}
		//update service product list Pic
		if(null != product.getProductListPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_LIST_PICS_FOLDER,product.getId()+"", product.getProductListPicData());
		}
		//update service product details Pic
		if(null != product.getProductDetailsPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_DETAILS_PICS_FOLDER,product.getId()+"", product.getProductDetailsPicData());
		}
				
		if (populateTransientFields) {
			product = populateTransientFields(product, loggedInUser);
		}
		
		if(fetchEagerly) {
			product= fetchEagerly(product);
		}
		return product;
	}
	
	@Transactional
	public List<Product> getAllProducts(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Product> products = productRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Product product : products) {
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if (fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return products;
	}
	
	@Deprecated
	@Transactional
	public List<Product> getAllProductsAccordingAlphaOrder(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Product> products = productRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Product product : products) {
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if (fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		
		Collections.sort(products,new Comparator<Product>() {
			@Override
			public int compare(Product p1, Product p2) {
				// TODO Auto-generated method stub
				return p1.getName().compareToIgnoreCase(p2.getName());
			}
	    });
		
		return products;
	}
	
	@Transactional
	public List<Product> getProductsForSubscription(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Product> products = productRepository.findByIsAvailableSubscription(true);
		if(populateTransientFields || fetchEagerly){
		for (Product product : products) {
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if (fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return products;
	}
	
	@Transactional
	public List<Product> getAllProductsByCategoryForOnetimeorder(User loggedInUser,Category category,boolean isAvailableOneTimeOrder, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Product> products = productRepository.findByIsAvailableOneTimeOrderAndCategory_Code(isAvailableOneTimeOrder, category.getCode());
		if(populateTransientFields || fetchEagerly){
		for (Product product : products) {
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if (fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return products;
	}
	
	@Transactional
	public Page<Product> getProducts(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Product> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = productRepository.findAll(pageRequest);
		Iterator<Product> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Product product = (Product) iterator.next();
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if(fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Product> searchProduct(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Product> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = productRepository.findAll(ProductSpecifications.search(searchTerm), pageRequest);
		Iterator<Product> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Product product = (Product) iterator.next();
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if(fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return page;
	}
	
	@Transactional
	public Product getProduct(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Product id");
		}
		Product product = getProduct(id, false);
		if(doPostValidation && null == product) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);
		
		return product;
	}
	
	@Transactional
	public Product getProductByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Product code");
		}
		Product product = productRepository.findByCode(code);
		if(doPostValidation && null == product) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);

		return product;
	}
	
	/**
	 * 
	 * @param code
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	@Transactional
	public Product getProductByName(String name, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == name) {
			throw new ProvilacException("Invalid Product name");
		}
		Product product = productRepository.findByName(name);
		if(doPostValidation && null == product) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);

		return product;
	}
	
	@Transactional
	public Product getProductByNameAndCategory(String name,Category category, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == name) {
			throw new ProvilacException("Invalid Product name");
		}
		Product product = productRepository.findByNameAndCategory(name, category);
		if(doPostValidation && null == product) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		if(populateTransientFields)
			product= populateTransientFields(product, loggedInUser);
		if(fetchEagerly)
			product= fetchEagerly(product);

		return product;
	}
	
	@Transactional
	public Page<Product> getProductByCategoryCode(Integer pageNumber, String categoryCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		Page<Product> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		
		page = productRepository.findByCategory_Code(categoryCode, pageRequest);
		Iterator<Product> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Product product = (Product) iterator.next();
			if(populateTransientFields)
				product = populateTransientFields(product, loggedInUser);
			if(fetchEagerly)
				product = fetchEagerly(product);
			}
		}
		return page;
	}

	/**
	 * returns @Product without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Product getProduct(Long id, boolean doPostValidation) {
		
		Product product;
		if(null == id) {
			throw new ProvilacException("Invalid product id");
		}
		product = productRepository.findOne(id);
		if(doPostValidation && null == product) {
			throw new ProvilacException("product with this Id does not exists");
		}
		return product;
	}
	
	
	
	/**
	 * 
	 * Updates the Product. Do not updates pricing map
	 * 
	 * @param id - Id of existing Product
	 * @param Product - Updated Product
	 * @param loggedInProduct - LoggedIn Product
	 * @return Updated Product
	 * 
	 * @author Vishal
	 */
	@Transactional
	public Product updateProduct(Long id, Product product, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Product id");
		}
		Product existingProduct = productRepository.findOne(id);
		if(null == existingProduct) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		existingProduct.setName(product.getName());
		existingProduct.setPrice(product.getPrice());
		existingProduct.setColorCode(product.getColorCode());
		existingProduct.setCategory(product.getCategory());
		existingProduct.setDescription(product.getDescription());
		existingProduct.setTax(product.getTax());
		existingProduct.setAvailableOneTimeOrder(product.isAvailableOneTimeOrder());
		existingProduct.setAvailableSubscription(product.isAvailableSubscription());
		//TODO update profilePic.
		if(null != product.getPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_PICS_FOLDER,product.getId()+"", product.getPicData());
		}
		
		//TODO update profilePic.
		if(null != product.getBannerData()){
			appFileUtils.saveData(AppConstants.PRODUCT_BANNERS_FOLDER,product.getId()+"", product.getBannerData());
		}
		
		//update service product cart pic
		if(null != product.getProductCartPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_CART_PICS_FOLDER,product.getId()+"", product.getProductCartPicData());
		}
		//update service product list Pic
		if(null != product.getProductListPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_LIST_PICS_FOLDER,product.getId()+"", product.getProductListPicData());
		}
		//update service product details Pic
		if(null != product.getProductDetailsPicData()){
			appFileUtils.saveData(AppConstants.PRODUCT_DETAILS_PICS_FOLDER,product.getId()+"", product.getProductDetailsPicData());
		}
		
		existingProduct = productRepository.save(existingProduct);
		activityLogService.createActivityLog(loggedInUser, "update", "Product",id);
		existingProduct.setCipher(EncryptionUtil.encode(product.getId()+""));
		if(populateTransientFields)
			existingProduct =populateTransientFields(existingProduct, loggedInUser);
		if(fetchEagerly)
			existingProduct=fetchEagerly(existingProduct);
		return existingProduct;
	}
	
	@Transactional
	public Collection<Product> updatePrepayPricing(Collection<Product> products, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		products = productRepository.save(products);
		if(populateTransientFields || fetchEagerly) {
			for (Product product : products) {
				if(populateTransientFields)
					product = populateTransientFields(product, loggedInUser);
				if (fetchEagerly)
					product = fetchEagerly(product);
			}
		}
		return products;
	}
	
	public void deleteProduct(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Product id");
		}
		Product product = productRepository.findOne(id);
		if(null == product) {
			throw new ProvilacException("Product with this Id does not exists");
		}
		productRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Product", id);
	}
	
	private Product populateTransientFields(Product product, User loggedInUser) {
		
		if(null == product) {
			return product;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Logged In User");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			product.setIsEditAllowed(true);
			product.setIsDeleteAllowed(true);
		} else {
			product.setIsEditAllowed(false);
			product.setIsDeleteAllowed(false);
		}
		product.setCipher(EncryptionUtil.encode(product.getId()+""));
		return product;
	}
	
	private Product fetchEagerly(Product product) {
		
		product.getPricingMap().size();
		return product;
	}
	
}

