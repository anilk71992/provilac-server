package com.vishwakarma.provilac.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.repository.OrderLineItemRepository;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class OrderLineItemService {

	@Resource
	private OrderLineItemRepository orderLineItemRepository;

	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;

	@Transactional
	public List<OrderLineItem> createOrderLineItems(Set<OrderLineItem> orderLineItems,User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null==loggedInUser){
			throw new ProvilacException("Invalid Log-in User");
		}
		 List<OrderLineItem> orderLineItemsave=orderLineItemRepository.save(orderLineItems);
		 for(OrderLineItem ordersLineItem:orderLineItems){
		 activityLogService.createActivityLog(loggedInUser, "add", "OrderLineItem",ordersLineItem.getId());
		 }
		 if(populateTransientFields||fetchEagerly){
			 for(OrderLineItem orderLineItem:orderLineItems){
			if(populateTransientFields){
				orderLineItem=populateTransientFields(orderLineItem, loggedInUser);
			}
			if(fetchEagerly){
				orderLineItem=fetchEagerly(orderLineItem);
			}
		}
		}
		return orderLineItemsave;
	}
	
	@Transactional
	public List<OrderLineItem> getOrderLineItems(String orderCode,User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null==loggedInUser){
			throw new ProvilacException("Invalid Log-in User");
		}
		 List<OrderLineItem> orderLineItems=orderLineItemRepository.findByOneTimeOrder_Code(orderCode);
		
		 if(populateTransientFields||fetchEagerly){
			 for(OrderLineItem orderLineItem:orderLineItems){
			if(populateTransientFields){
				orderLineItem=populateTransientFields(orderLineItem, loggedInUser);
			}
			if(fetchEagerly){
				orderLineItem=fetchEagerly(orderLineItem);
			}
		}
		}
		return orderLineItems;
	}
	
	private OrderLineItem populateTransientFields(OrderLineItem orderLineItem,User loggedInUser){
		if(null==loggedInUser){
			throw new ProvilacException("Invalid User");
		}
		
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)){
			orderLineItem.setIsEditAllowed(true);
			orderLineItem.setIsDeleteAllowed(true);
		}else{
			orderLineItem.setIsEditAllowed(false);
			orderLineItem.setIsDeleteAllowed(false);
		}
		
		orderLineItem.setCipher(EncryptionUtil.decode(orderLineItem.getId()+""));
		return orderLineItem;
	}
	
	private OrderLineItem fetchEagerly(OrderLineItem orderLineItem){
		return orderLineItem;
	}
}
