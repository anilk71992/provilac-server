package com.vishwakarma.provilac.service;

import javax.annotation.Resource;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.model.UserInfo;
import com.vishwakarma.provilac.repository.UserRepository;

/**
 * A custom {@link UserDetailsService} where user information
 * is retrieved from a JPA repository
 */
@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
	
	
	@Resource 
	private UserRepository userDao;

	/**
	 * Returns a populated {@link UserDetails} object. 
	 * The email is first retrieved from the database and then mapped to 
	 * a {@link UserDetails} object.
	 */
	public UserDetails loadUserByUsername(String mobileNumber) throws UsernameNotFoundException {
		
			com.vishwakarma.provilac.model.User domainUser = userDao.findByMobileNumber(mobileNumber);
			
			boolean enabled = domainUser.getIsEnabled();
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			
			return new UserInfo(
					domainUser,
					enabled,
					accountNonExpired,
					credentialsNonExpired,
					accountNonLocked
					);
		
	}
	
}
