package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Deposit;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.DepositSpecifications;
import com.vishwakarma.provilac.repository.DepositRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class DepositService {

	@Resource
	private DepositRepository depositRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource 
	private UserService userService;
	
	
	@Transactional
	public Deposit createDeposit(Deposit deposit, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == deposit) {
			throw new ProvilacException("Invalid deposit");
		}
		
		deposit = depositRepository.save(deposit);
		activityLogService.createActivityLog(loggedInUser, "add", "Deposit", deposit.getId());
		
		if (populateTransientFields) {
			deposit = populateTransientFields(deposit, loggedInUser);
		}
		
		if(fetchEagerly) {
			deposit= fetchEagerly(deposit);
		}
		return deposit;
	}
	
	@Transactional
	public List<Deposit> getAllDeposits(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Deposit> deposits = depositRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Deposit deposit : deposits) {
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if (fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return deposits;
	}
/**
 * 	
 * @param userCode
 * @param loggedInUser
 * @param populateTransientFields
 * @param fetchEagerly
 * @return List of Deposits for particular customer
 */
	@Transactional
	public List<Deposit> getAllDepositsByCollectionBoy(String userCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Deposit> deposits = depositRepository.findByCollectionBoy_Code(userCode);
		if(populateTransientFields || fetchEagerly){
		for (Deposit deposit : deposits) {
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if (fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return deposits;
	}
	
	@Transactional
	public Page<Deposit> getAllDepositsByCollectionBoy(Integer pageNumber, String userCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Deposit> page =null;
		PageRequest pageRequest= new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page =depositRepository.findByCollectionBoy_Code(userCode,pageRequest);
		if(populateTransientFields || fetchEagerly){
			Iterator<Deposit> iterator=page.iterator();
		while (iterator.hasNext()) {
			Deposit deposit=iterator.next();
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if (fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Page of Deposits
	 */
	@Transactional
	public Page<Deposit> getDeposits(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Deposit> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = depositRepository.findAll(pageRequest);
		Iterator<Deposit> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Deposit deposit = (Deposit) iterator.next();
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if(fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return page;
	}
	

	@Transactional
	public Page<Deposit> getDepositsByCustomerCode(String customerCode,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Deposit> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = depositRepository.findByCollectionBoy_Code(customerCode, pageRequest);
		Iterator<Deposit> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Deposit deposit = (Deposit) iterator.next();
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if(fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return page;
	}
	
	/**
	 * Get Deposits by CustomerId and date Range
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 * 
	 * @author Harshal
	 */
	@Transactional
	public List<Deposit> getDepositsByCustomerIdAndDateRange(Long customerId,Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Deposit> deposits = depositRepository.findAll(DepositSpecifications.searchByCollectionBoyAndDateRange(customerId, fromDate, toDate));
		if(populateTransientFields || fetchEagerly){
			for (Deposit deposit : deposits) {
				if(populateTransientFields){
					deposit = populateTransientFields(deposit, loggedInUser);
				}
				if (fetchEagerly){
					deposit = fetchEagerly(deposit);
				}
			}
		}
		return deposits;
	}
	
	@Transactional
	public Page<Deposit> searchDeposit(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Deposit> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = depositRepository.findAll(DepositSpecifications.search(searchTerm), pageRequest);
		Iterator<Deposit> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Deposit deposit = (Deposit) iterator.next();
			if(populateTransientFields)
				deposit = populateTransientFields(deposit, loggedInUser);
			if(fetchEagerly)
				deposit = fetchEagerly(deposit);
			}
		}
		return page;
	}
	
	@Transactional
	public Deposit getDeposit(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Deposit id");
		}
		Deposit deposit = getDeposit(id, false);
		if(doPostValidation && null == deposit) {
			throw new ProvilacException("Deposit with this Id does not exists");
		}
		if(populateTransientFields)
			deposit= populateTransientFields(deposit, loggedInUser);
		if(fetchEagerly)
			deposit= fetchEagerly(deposit);
		
		return deposit;
	}
	
	public Deposit getDepositByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Deposit code");
		}
		Deposit deposit = depositRepository.findByCode(code);
		if(doPostValidation && null == deposit) {
			throw new ProvilacException("Deposit with this Code does not exists");
		}
		if(populateTransientFields)
			deposit= populateTransientFields(deposit, loggedInUser);
		if(fetchEagerly)
			deposit= fetchEagerly(deposit);

		return deposit;
	}

	/**
	 * returns @Deposit without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Deposit getDeposit(Long id, boolean doPostValidation) {
		
		Deposit deposit;
		if(null == id) {
			throw new ProvilacException("Invalid deposit id");
		}
		deposit = depositRepository.findOne(id);
		if(doPostValidation && null == deposit) {
			throw new ProvilacException("deposit with this Id does not exists");
		}
		return deposit;
	}
	
	/**
	 * 
	 * Updates the Deposit. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Deposit
	 * @param Deposit - Updated Deposit
	 * @param loggedInDeposit - LoggedIn Deposit
	 * @return Updated Deposit
	 * 
	 * @author Vishal
	 */
	@Transactional
	public Deposit updateDeposit(Long id, Deposit deposit, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Deposit id");
		}
		Deposit existingDeposit = depositRepository.findOne(id);
		if(null == existingDeposit) {
			throw new ProvilacException("Deposit with this Id does not exists");
		}

		existingDeposit.setCashAmount(deposit.getCashAmount());
		existingDeposit.setTotalAmount(deposit.getTotalAmount());
		existingDeposit.setChequeAmount(deposit.getChequeAmount());
		existingDeposit.setNoOfCheques(deposit.getNoOfCheques());
		existingDeposit.setChequePayments(deposit.getChequePayments());
		existingDeposit = depositRepository.save(existingDeposit);
		activityLogService.createActivityLog(loggedInUser, "update", "Deposit",id);
		
		existingDeposit.setCipher(EncryptionUtil.encode(deposit.getId() + ""));
		if (populateTransientFields){
			existingDeposit = populateTransientFields(existingDeposit, loggedInUser);
		}

		if (fetchEagerly){
			existingDeposit = fetchEagerly(existingDeposit);
		}
		
		return existingDeposit;
	}
	
	public void deleteDeposit(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Deposit id");
		}
		Deposit deposit = depositRepository.findOne(id);
		if(null == deposit) {
			throw new ProvilacException("Deposit with this Id does not exists");
		}
		depositRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Deposit",id);
		
	}
	
	private Deposit populateTransientFields(Deposit deposit, User loggedInUser) {
		
		if(null == deposit) {
			return deposit;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Deposit");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			deposit.setIsEditAllowed(true);
			deposit.setIsDeleteAllowed(true);
		} else {
			deposit.setIsEditAllowed(false);
			deposit.setIsDeleteAllowed(false);
		}
		deposit.setCipher(EncryptionUtil.encode(deposit.getId()+""));
		return deposit;
	}
	
	private Deposit fetchEagerly(Deposit deposit) {
		deposit.getChequePayments().size();
		return deposit;
	}
	
}

