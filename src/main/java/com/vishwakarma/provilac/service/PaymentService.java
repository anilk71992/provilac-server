package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.PaymentSpecifications;
import com.vishwakarma.provilac.repository.PaymentRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.PlivoSMSHelper;


@Service
public class PaymentService {

	@Resource
	private PaymentRepository paymentRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private PlivoSMSHelper plivoSMSHelper;
	
	@Resource
	private DeliveryScheduleService deliveryScheduleService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Transactional
	public Payment createPayment(Payment payment, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == payment) {
			throw new ProvilacException("Invalid payment");
		}
		payment = paymentRepository.save(payment);
		activityLogService.createActivityLog(loggedInUser, "add", "Payment", payment.getId());

		//Don't send SMS for waiver payments
		if(payment.getAmount() > 0) {
			double lastPending = payment.getCustomer().getLastPendingDues();
			if(payment.getAdjustmentAmount()!=0){
				lastPending = lastPending - (payment.getAmount()+payment.getAdjustmentAmount());
			}else{
				lastPending = lastPending - payment.getAmount();
			}
			notificationHelper.sendNotificationForNewPaymentAdd(payment.getCustomer(), payment ,lastPending);
			firebaseWrapper.sendPaymentReceivedNotification(payment);
			Date startOfMonth = new Date();
			DateHelper.getStartOfMonth(startOfMonth);
			plivoSMSHelper.sendSMS(payment.getCustomer().getMobileNumber(), "Hi "+payment.getCustomer().getFullName()+". We have received INR "+ String.format("%.0f", payment.getAmount()+payment.getAdjustmentAmount()) +" against your Provilac milk purchase. Your balance due for billed invoice is INR "+ String.format("%.0f", lastPending));
		}
		
		if (populateTransientFields) {
			payment = populateTransientFields(payment, loggedInUser);
		}
		
		if(fetchEagerly) {
			payment= fetchEagerly(payment);
		}
		return payment;
	}
	
	@Transactional
	public List<Payment> getAllPayments(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Payment> payments = paymentRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Payment payment : payments) {
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if (fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return payments;
	}
/**
 * 	
 * @param userCode
 * @param loggedInUser
 * @param populateTransientFields
 * @param fetchEagerly
 * @return List of Payments for perticular customer
 */
	@Transactional
	public List<Payment> getAllPaymentsByCustomer(String userCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Payment> payments = paymentRepository.findByCustomer_Code(userCode);
		if(populateTransientFields || fetchEagerly){
		for (Payment payment : payments) {
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if (fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return payments;
	}
	
	@Transactional
	public List<Payment> getPaymentsByInvoiceCode(String invoiceCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		List<Payment> payments = paymentRepository.findByInvoice_Code(invoiceCode);
		for(Payment payment:payments){
		if(populateTransientFields || fetchEagerly){
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if (fetchEagerly)
				payment = fetchEagerly(payment);
		}
		}
		return payments;
	}
	
	/**
	 * 
	 * @param isBounced
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Bounced Payments
	 */
	@Transactional
	public Page<Payment> getBouncedPayments(boolean isBounced,Integer pageNumber, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = paymentRepository.findAll(PaymentSpecifications.searchByIsBounced(isBounced), pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Page of Payments
	 */
	@Transactional
	public Page<Payment> getPayments(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = paymentRepository.findAll(pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return un deposited ChequePayments
	 */
			
	
	@Transactional
	public Page<Payment> getUndepositedChequePaymants(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = paymentRepository.findAll(PaymentSpecifications.searchUndepositedCheques(false, PaymentMethod.Cheque), pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Un verifiedDeposited Cheques
	 */
	@Transactional
	public Page<Payment> getUnVerifieddepositedChequePaymants(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = paymentRepository.findAll(PaymentSpecifications.searchUnVerifieddepositedCheques(true, PaymentMethod.Cheque,false), pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	
	

	@Transactional
	public Page<Payment> getPaymentsByCustomerCode(String customerCode,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = paymentRepository.findByCustomer_Code(customerCode, pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	
	/**
	 * Get Payments by CustomerId and date Range
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 * 
	 * @author Harshal
	 */
	@Transactional
	public List<Payment> getPaymentsByCustomerIdAndDateRange(Long customerId,Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Payment> payments = paymentRepository.findAll(PaymentSpecifications.searchByCustomerAndDateRange(customerId, fromDate, toDate));
		if(populateTransientFields || fetchEagerly){
			for (Payment payment : payments) {
				if(populateTransientFields){
					payment = populateTransientFields(payment, loggedInUser);
				}
				if (fetchEagerly){
					payment = fetchEagerly(payment);
				}
			}
		}
		return payments;
	}
	
	@Transactional
	public Page<Payment> searchPayment(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = paymentRepository.findAll(PaymentSpecifications.search(searchTerm), pageRequest);
		Iterator<Payment> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Payment payment = (Payment) iterator.next();
			if(populateTransientFields)
				payment = populateTransientFields(payment, loggedInUser);
			if(fetchEagerly)
				payment = fetchEagerly(payment);
			}
		}
		return page;
	}
	
	@Transactional
	public List<Payment> getAllPaymentsByReceivedBy(Long customerId, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		List<Payment> payments = paymentRepository.findByreceivedBy_Id(customerId);
		if (populateTransientFields || fetchEagerly) {
			for (Payment payment : payments) {
				if (populateTransientFields)
					payment = populateTransientFields(payment, loggedInUser);
				if (fetchEagerly)
					payment = fetchEagerly(payment);
			}
		}
		return payments;
	}
	
	public Payment getPayment(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Payment id");
		}
		Payment payment = getPayment(id, false);
		if(doPostValidation && null == payment) {
			throw new ProvilacException("Payment with this Id does not exists");
		}
		if(populateTransientFields)
			payment= populateTransientFields(payment, loggedInUser);
		if(fetchEagerly)
			payment= fetchEagerly(payment);
		
		return payment;
	}
	
	public Payment getPaymentByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Payment code");
		}
		Payment payment = paymentRepository.findByCode(code);
		if(doPostValidation && null == payment) {
			throw new ProvilacException("Payment with this Code does not exists");
		}
		if(populateTransientFields)
			payment= populateTransientFields(payment, loggedInUser);
		if(fetchEagerly)
			payment= fetchEagerly(payment);

		return payment;
	}
	
	/**
	 * 
	 * Do not use this method, there can be multiple records with same cheque number
	 * 
	 */
	@Transactional
	@Deprecated
	public Payment getPaymentByChequeNumber(String chequeNumber, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == chequeNumber) {
			throw new ProvilacException("Invalid Payment chequeNumber");
		}
		Payment payment = paymentRepository.findByChequeNumber(chequeNumber);
		if(doPostValidation && null == payment) {
			throw new ProvilacException("Payment with this chequeNumber does not exists");
		}
		if(populateTransientFields)
			payment= populateTransientFields(payment, loggedInUser);
		if(fetchEagerly)
			payment= fetchEagerly(payment);

		return payment;
	}

	@Transactional
	public Payment getPaymentByTxnId(String txnId,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		Payment payment = paymentRepository.findBytxnId(txnId);
		if(populateTransientFields)
			payment = populateTransientFields(payment, loggedInUser);
		if(fetchEagerly)
			payment = fetchEagerly(payment);
		
		return payment;
	}
	
	@Transactional
	public Payment getLatestPaymentByCustomer(Long customerId, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		Page<Payment> page = null;
		PageRequest pageRequest = new PageRequest(0, 1, Direction.DESC, "id");
		page = paymentRepository.findAll(PaymentSpecifications.searchByCustomer(customerId), pageRequest);
		if(page.hasContent()){
			Payment payment = page.getContent().get(0);
			return payment;
		}else{
			return null;
		}
	}
	
	/**
	 * returns @Payment without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Payment getPayment(Long id, boolean doPostValidation) {
		
		Payment payment;
		if(null == id) {
			throw new ProvilacException("Invalid payment id");
		}
		payment = paymentRepository.findOne(id);
		if(doPostValidation && null == payment) {
			throw new ProvilacException("payment with this Id does not exists");
		}
		return payment;
	}
	
	@Transactional
	public List<Payment> getAllPaymentsByDateAndMethodAndRecievedByAndAddFrom(Long receivedById,Date date,PaymentMethod paymentMethod1,PaymentMethod paymentMethod2,Long collectionId, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		List<Payment> payments = paymentRepository.findAll(PaymentSpecifications.getPaymentsFromDateToDateAndCollectionBoyIdAndPaymentMethodAndAddFrom(receivedById, date, paymentMethod1,paymentMethod2, collectionId));
		if (populateTransientFields || fetchEagerly) {
			for (Payment payment : payments) {
				if (populateTransientFields)
					payment = populateTransientFields(payment, loggedInUser);
				if (fetchEagerly)
					payment = fetchEagerly(payment);
			}
		}
		return payments;
	}
	
	/**
	 * 
	 * Updates the Payment. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Payment
	 * @param Payment - Updated Payment
	 * @param loggedInPayment - LoggedIn Payment
	 * @return Updated Payment
	 * 
	 * @author Vishal
	 */
	@Transactional
	public Payment updatePayment(Long id, Payment payment, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Payment id");
		}
		Payment existingPayment = paymentRepository.findOne(id);
		if(null == existingPayment) {
			throw new ProvilacException("Payment with this Id does not exists");
		}

		existingPayment.setAmount(payment.getAmount());
		existingPayment.setBankName(payment.getBankName());
		existingPayment.setChequeNumber(payment.getChequeNumber());
		existingPayment.setCustomer(payment.getCustomer());
		existingPayment.setDate(payment.getDate());
		existingPayment.setIsBounced(existingPayment.getIsBounced());
		existingPayment.setPaymentMethod(payment.getPaymentMethod());
		existingPayment.setReceivedBy(payment.getReceivedBy());
		existingPayment.setTxnId(payment.getTxnId());
		existingPayment.setReceiptNo(payment.getReceiptNo());
		existingPayment.setDeposit(payment.getDeposit());
		existingPayment.setHasDeposited(payment.getHasDeposited());
		existingPayment.setPointsRedemed(payment.getPointsRedemed());
		existingPayment.setPointsAmount(payment.getPointsAmount());
		existingPayment.setCashback(payment.getCashback());
		
		if(null != payment.getCollection()){
			existingPayment.setCollection(payment.getCollection());
		}
		if(null != payment.getDeposit()){
			existingPayment.setDeposit(payment.getDeposit());
		}
		existingPayment = paymentRepository.save(existingPayment);
		activityLogService.createActivityLog(loggedInUser, "update", "Payment", id);
		/*if(payment.getIsBounced()==true){
			FirebaseWrapper.sendBouncedPaymentNotification(payment);
			plivoSMSHelper.sendSMS(payment.getCustomer().getMobileNumber(), "Dear Provilac Customer your cheque has been dishourned. Please note bank charges will be applied.");
			notificationHelper.sendNotificationForPaymentBounced(payment.getCustomer(), payment);
		}*/
		
		existingPayment.setCipher(EncryptionUtil.encode(payment.getId() + ""));
		if (populateTransientFields)
			existingPayment = populateTransientFields(existingPayment, loggedInUser);

		if (fetchEagerly)
			existingPayment = fetchEagerly(existingPayment);
		
		return existingPayment;
	}
	
	@Transactional
	public Payment changeBounceStatus(Long id, boolean isBounced, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Payment id");
		}
		Payment existingPayment = paymentRepository.findOne(id);
		if(null == existingPayment) {
			throw new ProvilacException("Payment with this Id does not exists");
		}

		existingPayment.setIsBounced(isBounced);
		existingPayment = paymentRepository.save(existingPayment);
		existingPayment.setCipher(EncryptionUtil.encode(existingPayment.getId() + ""));
		
		if (populateTransientFields)
			existingPayment = populateTransientFields(existingPayment, loggedInUser);

		if (fetchEagerly)
			existingPayment = fetchEagerly(existingPayment);
		
		return existingPayment;
	}
	
	@Transactional
	public Payment verifyChequePayment(Long id, Payment payment, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Payment id");
		}
		Payment existingPayment = paymentRepository.findOne(id);
		if(null == existingPayment) {
			throw new ProvilacException("Payment with this Id does not exists");
		}

		existingPayment.setIsVerified(true);
		
		existingPayment = paymentRepository.save(existingPayment);
		
		existingPayment.setCipher(EncryptionUtil.encode(payment.getId() + ""));
		if (populateTransientFields)
			existingPayment = populateTransientFields(existingPayment, loggedInUser);

		if (fetchEagerly)
			existingPayment = fetchEagerly(existingPayment);
		
		return existingPayment;
	}
	
	
	public void deletePayment(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Payment id");
		}
		Payment payment = paymentRepository.findOne(id);
		if(null == payment) {
			throw new ProvilacException("Payment with this Id does not exists");
		}
		paymentRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Payment", id);
	}
	

	@Transactional
	public Payment updatePGStatus(String txnId, boolean isApproved, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Payment payment = paymentRepository.findBytxnId(txnId);
		if(null != payment){
			
			payment.setApprovedByPG(isApproved);
			payment = paymentRepository.save(payment);
		}
		
		return payment;
	}
	
	
	private Payment populateTransientFields(Payment payment, User loggedInUser) {
		
		if(null == payment) {
			return payment;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Payment");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			payment.setIsEditAllowed(true);
			payment.setIsDeleteAllowed(true);
		} else {
			payment.setIsEditAllowed(false);
			payment.setIsDeleteAllowed(false);
		}
		payment.setCipher(EncryptionUtil.encode(payment.getId()+""));
		return payment;
	}
	
	private Payment fetchEagerly(Payment payment) {
		return payment;
	}
	
	
	public static Boolean verifycheckSum(String merchantKey,TreeMap <String,String>parameters,String checksum){
		
		boolean isValidChecksum = false;
		try {
			com.paytm.merchant.CheckSumServiceHelper checkSumServiceHelper =  com.paytm.merchant.CheckSumServiceHelper.getCheckSumServiceHelper();
			isValidChecksum = checkSumServiceHelper.verifycheckSum(merchantKey, parameters, checksum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return isValidChecksum;
	}
}

