package com.vishwakarma.provilac.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.RequestLineItemSpecifications;
import com.vishwakarma.provilac.repository.PrepayRequestLineItemRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class PrepayRequestLineItemService {

	@Resource
	private  PrepayRequestLineItemRepository prepayRequestLineItemRepository;
	
	@Transactional
	public  PrepayRequestLineItem createPrepayRequestLineItem(PrepayRequestLineItem  prepayRequestLineItem, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		if (null ==  prepayRequestLineItem) {
			throw new ProvilacException("Invalid  prepay request lineitem");
		}
		prepayRequestLineItem =  prepayRequestLineItemRepository.save(prepayRequestLineItem);
		
		if (populateTransientFields) {
			prepayRequestLineItem = populateTransientFields( prepayRequestLineItem, loggedInUser);
		}
		
		if(fetchEagerly) {
			prepayRequestLineItem= fetchEagerly( prepayRequestLineItem);
		}
		return  prepayRequestLineItem;
	}
	
	@Transactional
	public Collection<PrepayRequestLineItem> createPrepayRequestLineItems(Collection<PrepayRequestLineItem> lineItems, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		if(null == lineItems){
			throw new ProvilacException("Invalid prepay request lineitem");
		}
		lineItems = prepayRequestLineItemRepository.save(lineItems);
		if(populateTransientFields||fetchEagerly){
			for (PrepayRequestLineItem prepayRequestLineItem : lineItems) {
				if(populateTransientFields)
					prepayRequestLineItem = populateTransientFields(prepayRequestLineItem, loggedInUser);
				if(fetchEagerly)
					prepayRequestLineItem = fetchEagerly(prepayRequestLineItem);
			}
		}
		return lineItems;
	}
	
	@Transactional
	public List<PrepayRequestLineItem> getAllPrepayRequestLineItems(User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List< PrepayRequestLineItem>  requestLineItems =  prepayRequestLineItemRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for ( PrepayRequestLineItem  prepayRequestLineItem :  requestLineItems) {
			if(populateTransientFields)
				prepayRequestLineItem = populateTransientFields( prepayRequestLineItem, loggedInUser);
			if (fetchEagerly)
				prepayRequestLineItem = fetchEagerly( prepayRequestLineItem);
			}
		}
		return  requestLineItems;
	}
	
	@Transactional
	public Page<PrepayRequestLineItem> getPrepayRequestLineItems(Integer pageNumber, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<PrepayRequestLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  prepayRequestLineItemRepository.findAll(pageRequest);
		Iterator< PrepayRequestLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 PrepayRequestLineItem  prepayRequestLineItem = (PrepayRequestLineItem) iterator.next();
			if(populateTransientFields)
				prepayRequestLineItem = populateTransientFields(prepayRequestLineItem, loggedInUser);
			if(fetchEagerly)
				prepayRequestLineItem = fetchEagerly(prepayRequestLineItem);
			}
		}
		return page;
	}
	
	@Transactional
	public Page< PrepayRequestLineItem> searchPrepayRequestLineItem(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page< PrepayRequestLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  prepayRequestLineItemRepository.findAll( RequestLineItemSpecifications.search(searchTerm), pageRequest);
		Iterator< PrepayRequestLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 PrepayRequestLineItem  prepayRequestLineItem = ( PrepayRequestLineItem) iterator.next();
			if(populateTransientFields)
				prepayRequestLineItem = populateTransientFields( prepayRequestLineItem, loggedInUser);
			if(fetchEagerly)
				prepayRequestLineItem = fetchEagerly( prepayRequestLineItem);
			}
		}
		return page;
	}
	
	public PrepayRequestLineItem getPrepayRequestLineItem(Long id, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid prepay request lineitem id");
		}
		 PrepayRequestLineItem  prepayRequestLineItem = getPrepayRequestLineItem(id, false);
		if(doPostValidation && null ==  prepayRequestLineItem) {
			throw new ProvilacException("prepay request lineitem with this id does not exists");
		}
		if(populateTransientFields)
			prepayRequestLineItem= populateTransientFields(prepayRequestLineItem, loggedInUser);
		if(fetchEagerly)
			prepayRequestLineItem= fetchEagerly( prepayRequestLineItem);
		
		return  prepayRequestLineItem;
	}
		
	/**
	 * 
	 * @param id
	 * @param doPostValidation
	 * @return
	 */
	public PrepayRequestLineItem getPrepayRequestLineItem(Long id, boolean doPostValidation) {
		
		 PrepayRequestLineItem  prepayRequestLineItem;
		if(null == id) {
			throw new ProvilacException("Invalid  prepay request lineitem id");
		}
		prepayRequestLineItem =  prepayRequestLineItemRepository.findOne(id);
		if(doPostValidation && null ==  prepayRequestLineItem) {
			throw new ProvilacException("Prepay request lineitem with this id does not exists");
		}
		return  prepayRequestLineItem;
	}
	/**
	 * 
	 * @param id
	 * @param prepayRequestLineItem
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	public  PrepayRequestLineItem updatePrepayRequestLineItem(Long id, PrepayRequestLineItem  prepayRequestLineItem, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid prepay request lineitem id");
		}
		 PrepayRequestLineItem existingRequestLineItem =  prepayRequestLineItemRepository.findOne(id);
		if(null == existingRequestLineItem) {
			throw new ProvilacException("Prepay request lineitem with this Id does not exists");
		}
		existingRequestLineItem.setDay(prepayRequestLineItem.getDay());
		existingRequestLineItem.setProduct(prepayRequestLineItem.getProduct());
		existingRequestLineItem.setQuantity(prepayRequestLineItem.getQuantity());
		existingRequestLineItem = prepayRequestLineItemRepository.save(existingRequestLineItem);
		existingRequestLineItem.setCipher(EncryptionUtil.encode( prepayRequestLineItem.getId()+""));
		if(populateTransientFields)
			existingRequestLineItem =populateTransientFields(existingRequestLineItem, loggedInUser);
		if(fetchEagerly)
			existingRequestLineItem=fetchEagerly(existingRequestLineItem);
		return existingRequestLineItem;
	}
	
	@Transactional
	public Collection<PrepayRequestLineItem> updatePrepayRequestLineItems(Collection<PrepayRequestLineItem> requestLineItems, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		if(null == requestLineItems){
			throw new ProvilacException("Invalid prepay request lineitem");
		}
		requestLineItems = prepayRequestLineItemRepository.save(requestLineItems);
		if(populateTransientFields||fetchEagerly){
			for (PrepayRequestLineItem prepayRequestLineItem : requestLineItems) {
				if(populateTransientFields)
					prepayRequestLineItem = populateTransientFields(prepayRequestLineItem, loggedInUser);
				if(fetchEagerly)
					prepayRequestLineItem = fetchEagerly(prepayRequestLineItem);
			}
		}
		return requestLineItems;
	}
	
	public void deletePrepayRequestLineItems(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid prepay request lineitem id");
		}
		 PrepayRequestLineItem  prepayRequestLineItem =  prepayRequestLineItemRepository.findOne(id);
		if(null ==  prepayRequestLineItem) {
			throw new ProvilacException("Prepay request lineitem with this id does not exists");
		}
		 prepayRequestLineItemRepository.delete(id);
	}
	
	private  PrepayRequestLineItem populateTransientFields( PrepayRequestLineItem  prepayRequestLineItem, User loggedInUser) {
		
		if(null ==  prepayRequestLineItem) {
			return  prepayRequestLineItem;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid prepay request lineitem");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			prepayRequestLineItem.setIsEditAllowed(true);
			prepayRequestLineItem.setIsDeleteAllowed(true);
		} else {
			prepayRequestLineItem.setIsEditAllowed(false);
			prepayRequestLineItem.setIsDeleteAllowed(false);
		}
		prepayRequestLineItem.setCipher(EncryptionUtil.encode( prepayRequestLineItem.getId()+""));
		return  prepayRequestLineItem;
	}
	
	private  PrepayRequestLineItem fetchEagerly(PrepayRequestLineItem prepayRequestLineItem) {
		
		return  prepayRequestLineItem;
	}
	
}

