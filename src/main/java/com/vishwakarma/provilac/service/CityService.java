package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.CitySpecifications;
import com.vishwakarma.provilac.repository.CityRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class CityService {

	@Resource
	private CityRepository cityRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
	ActivityLogService activityLogService;
	
	@Transactional
	public City createCity(City city, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == city) {
			throw new ProvilacException("Invalid city");
		}
		city = cityRepository.save(city);
		activityLogService.createActivityLog(loggedInUser, "add", "City", city.getId());
		
		if (populateTransientFields) {
			city = populateTransientFields(city, loggedInUser);
		}
		
		if(fetchEagerly) {
			city= fetchEagerly(city);
		}
		return city;
	}
	
	@Transactional
	public List<City> getAllCities(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<City> cities = cityRepository.findAll();
		if (populateTransientFields || fetchEagerly) {
			for (City city : cities) {
				if (populateTransientFields)
					city = populateTransientFields(city, loggedInUser);
				if (fetchEagerly)
					city = fetchEagerly(city);
			}
		}
		return cities;
	}
	
	@Transactional
	public Page<City> getCities(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		Page<City> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = cityRepository.findAll(pageRequest);
		Iterator<City> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				City city = (City) iterator.next();
				if(populateTransientFields)
					city = populateTransientFields(city, loggedInUser);
				if(fetchEagerly)
					city = fetchEagerly(city);
			}
		}
		return page;
	}
	
	@Transactional
	public List<City> getAllCitiesByCountry(String countryCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<City> cities = cityRepository.findByCountry_Code(countryCode);
		if(populateTransientFields || fetchEagerly){
		for (City city : cities) {
			if(populateTransientFields)
				city = populateTransientFields(city, loggedInUser);
			if (fetchEagerly)
				city = fetchEagerly(city);
			}
		}
		return cities;
	}
	
	@Transactional
	public Page<City> searchCity(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<City> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = cityRepository.findAll(CitySpecifications.search(searchTerm), pageRequest);
		Iterator<City> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			City city = (City) iterator.next();
			if(populateTransientFields)
				city = populateTransientFields(city, loggedInUser);
			if(fetchEagerly)
				city = fetchEagerly(city);
			}
		}
		return page;
	}
	
	@Transactional
	public City getCity(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid city id");
		}
		City city = cityRepository.findOne(id);
		if(doPostValidation && null == city) {
			throw new ProvilacException("City with this id does not exists");
		}
		if(null == city){
			return null;
		}
		if(populateTransientFields)
			city= populateTransientFields(city, loggedInUser);
		if(fetchEagerly)
			city= fetchEagerly(city);
		
		return city;
	}
	
	@Transactional
	public City getCityByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(StringUtils.isBlank(code)) {
			throw new ProvilacException("Invalid city code");
		}
		City city = cityRepository.findByCode(code);
		if(doPostValidation && null == city) {
			throw new ProvilacException("City with this id does not exists");
		}
		if(city == null){
			return null;
		}
		if(populateTransientFields)
			city= populateTransientFields(city, loggedInUser);
		if(fetchEagerly)
			city= fetchEagerly(city);

		return city;
	}
	
	@Transactional
	public City getCityByName(String name, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(StringUtils.isBlank(name)) {
			throw new ProvilacException("Invalid city code");
		}
		City city = cityRepository.findByName(name);
		if(doPostValidation && null == city) {
			throw new ProvilacException("City with this Name does not exists");
		}
		if(city == null){
			return null;
		}
		if(populateTransientFields)
			city= populateTransientFields(city, loggedInUser);
		if(fetchEagerly)
			city= fetchEagerly(city);

		return city;
	}
	/**
	 * 
	 * @param code
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	@Transactional
	public City getCityByNameAndCountry(String name,String countryName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(StringUtils.isBlank(name)) {
			throw new ProvilacException("Invalid City name");
		}
		City city = cityRepository.findByNameAndCountryName(name, countryName);
		if(doPostValidation && null == city) {
			throw new ProvilacException("City with this name & country does not exists");
		}
		if(city == null){
			return null;
		}
		if(populateTransientFields)
			city= populateTransientFields(city, loggedInUser);
		if(fetchEagerly)
			city= fetchEagerly(city);

		return city;
	}

	
	/**
	 * 
	 * Updates the City.
	 * 
	 * @param id - Id of existing City
	 * @param City - Updated City
	 * @param loggedInCity - LoggedIn City
	 * @return Updated City
	 * 
	 * @author Rohit
	 */
	@Transactional
	public City updateCity(Long id, City city, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid city id");
		}
		City existingCity = cityRepository.findOne(id);
		if(null == existingCity) {
			throw new ProvilacException("City with this id does not exists");
		}
		existingCity.setName(city.getName());
		existingCity.setCountry(city.getCountry());
		existingCity.setState(city.getState());
		
		existingCity = cityRepository.save(existingCity);
		activityLogService.createActivityLog(loggedInUser, "update", "City",id);
		existingCity.setCipher(EncryptionUtil.encode(city.getId()+""));
		if(populateTransientFields)
			existingCity =populateTransientFields(existingCity, loggedInUser);
		if(fetchEagerly)
			existingCity=fetchEagerly(existingCity);
		return existingCity;
	}
	
	public void deleteCity(Long id) {
		if(null == id) {
			throw new ProvilacException("Invalid city id");
		}
		City city = cityRepository.findOne(id);
		if(null == city) {
			throw new ProvilacException("City with this id does not exists");
		}
		cityRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "City",id);
	}
	
	private City populateTransientFields(City city, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			city.setIsEditAllowed(true);
			city.setIsDeleteAllowed(true);
		} else {
			city.setIsEditAllowed(false);
			city.setIsDeleteAllowed(false);
		}
		city.setCipher(EncryptionUtil.encode(city.getId()+""));
		return city;
	}
	
	private City fetchEagerly(City city) {
		return city;
	}
	
}

