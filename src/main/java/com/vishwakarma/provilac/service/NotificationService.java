package com.vishwakarma.provilac.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Notification;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.repository.NotificationRepository;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class NotificationService {

	@Resource 
	private UserService userService;
	
	@Resource
	private NotificationRepository notificationRepository;
	
	
	@Transactional
	public Notification createNotification(Notification notification, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == notification) {
			throw new ProvilacException("Invalid notification");
		}
		notification =  notificationRepository.save(notification);
		if (populateTransientFields) {
			notification = populateTransientFields(notification, loggedInUser);
		}
		if(fetchEagerly) {
			notification = fetchEagerly(notification);
		}
		return notification;
	}
	
	
	/*@Transactional
	public Page<Notification> searchNotification(Integer pageNumber,String searchTerm,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Page<Notification>page=null;
		PageRequest pageRequest=new PageRequest(pageNumber-1 , AppConstants.PAGE_SIZE,Direction.DESC,"id");
		page=notificationRepository.findAll(NotificationSpecification.search(searchTerm),pageRequest);
		Iterator<Notification> iterator=page.iterator();
		if(populateTransientFields||fetchEagerly){
			while(iterator.hasNext()){
				Notification notification=(Notification)iterator.next();
				if(populateTransientFields){
					notification=populateTransientFields(notification, loggedInUser);
				}
				if(fetchEagerly){
					notification=fetchEagerly(notification);
				}
			}
		}
		return page;
	}
	
	@Transactional
	public Page<Notification> getNotifications(Integer pageNumber,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Page<Notification>page=null;
		PageRequest pageRequest=new PageRequest(pageNumber-1 , AppConstants.PAGE_SIZE,Direction.DESC,"id");
		page=notificationRepository.findAll(pageRequest);
		Iterator<Notification> iterator=page.iterator();
		if(populateTransientFields||fetchEagerly){
			while(iterator.hasNext()){
				Notification notification=(Notification)iterator.next();
				if(populateTransientFields){
					notification=populateTransientFields(notification, loggedInUser);
				}
				if(fetchEagerly){
					notification=fetchEagerly(notification);
				}
			}
		}
		return page;
	}
	
	@Transactional
	public Notification getNotification(Long id,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		Notification notification=notificationRepository.findOne(id);
		if(populateTransientFields||fetchEagerly){
				if(populateTransientFields){
					notification=populateTransientFields(notification, loggedInUser);
				}
				if(fetchEagerly){
					notification=fetchEagerly(notification);
				}
			}
		return notification;
	}
	
	@Transactional
	public Notification updateNotificaion(Long id, Notification notification, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new HelloDhobiException("Invalid notificaion id");
		}
		Notification existingNotification = notificationRepository.findOne(id);
		if(null == existingNotification) {
			throw new HelloDhobiException("Bvg Service with this Id does not exists");
		}
		
		existingNotification.setCities(notification.getCities());
		existingNotification =populateTransientFields(existingNotification, loggedInUser);
		if(fetchEagerly)
			existingNotification=fetchEagerly(existingNotification);
		return existingNotification;
	}
	
	*/
	
	private Notification populateTransientFields(Notification notification,User loggedInUser){
		if(null==loggedInUser){
			throw new ProvilacException("Invalid Notification");
		}
		/*if(loggedInUser.hasAnyRole(Role.ROLE_SUPER_ADMIN)){
		}*/
		
		notification.setCipher(EncryptionUtil.encode(notification.getId()+""));
		return notification;
	}
	
	private Notification fetchEagerly(Notification notification){
		return notification;
	}
	
}
