package com.vishwakarma.provilac.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.repository.DeletedRouteRecordRepository;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class DeletedRouteRecordService {

	@Resource
	private DeletedRouteRecordRepository deletedRouteRecordRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;

	@Transactional
	public DeletedRouteRecord createDeletedRouteRecordForChangeRoute(DeletedRouteRecord deletedRouteRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == deletedRouteRecord) {
			throw new ProvilacException("Invalid deletedRouteRecord");
		}
		deletedRouteRecord = deletedRouteRecordRepository.save(deletedRouteRecord);
		activityLogService.createActivityLog(loggedInUser, "add", "deletedRouteRecord", deletedRouteRecord.getId());
		if (populateTransientFields) {
			deletedRouteRecord = populateTransientFields(deletedRouteRecord, loggedInUser);
		}

		if(fetchEagerly) {
			deletedRouteRecord= fetchEagerly(deletedRouteRecord);
		}
		return deletedRouteRecord;
	}
	
	@Transactional
	public List<DeletedRouteRecord> getAllDeletedRouteRecords(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (DeletedRouteRecord deletedRouteRecord : deletedRouteRecords) {
			if(populateTransientFields)
				deletedRouteRecord = populateTransientFields(deletedRouteRecord, loggedInUser);
			if (fetchEagerly)
				deletedRouteRecord = fetchEagerly(deletedRouteRecord);
			}
		}
		return deletedRouteRecords;
	}
	
	@Transactional
	public List<DeletedRouteRecord> getAllDeletedRouteRecordsByCustomer(User customer) {
		
		List<DeletedRouteRecord> deletedRouteRecords = deletedRouteRecordRepository.findByCustomer(customer);
		return deletedRouteRecords;
	}
	
	public DeletedRouteRecord getDeletedRouteRecord(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeletedRouteRecord id");
		}
		DeletedRouteRecord deletedRouteRecord = getDeletedRouteRecord(id, false);
		if(doPostValidation && null == deletedRouteRecord) {
			throw new ProvilacException("DeletedRouteRecord with this Id does not exists");
		}
		if(populateTransientFields)
			deletedRouteRecord= populateTransientFields(deletedRouteRecord, loggedInUser);
		if(fetchEagerly)
			deletedRouteRecord= fetchEagerly(deletedRouteRecord);
		
		return deletedRouteRecord;
	}
	

	/**
	 * 
	 * @param customerCode
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	public DeletedRouteRecord getDeletedRouteRecordByCustomer(String customerCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == customerCode) {
			throw new ProvilacException("Invalid customerCode");
		}
		DeletedRouteRecord deletedRouteRecord = deletedRouteRecordRepository.findByCustomer_Code(customerCode);
		if(doPostValidation && null == deletedRouteRecord) {
			throw new ProvilacException("DeletedRouteRecord with this Id does not exists");
		}
		if(populateTransientFields)
			deletedRouteRecord= populateTransientFields(deletedRouteRecord, loggedInUser);
		if(fetchEagerly)
			deletedRouteRecord= fetchEagerly(deletedRouteRecord);
		
		return deletedRouteRecord;
	}
	/**
	 * returns @DeletedRouteRecord without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public DeletedRouteRecord getDeletedRouteRecord(Long id, boolean doPostValidation) {
		
		DeletedRouteRecord deletedRouteRecord;
		if(null == id) {
			throw new ProvilacException("Invalid deletedRouteRecord id");
		}
		deletedRouteRecord = deletedRouteRecordRepository.findOne(id);
		if(doPostValidation && null == deletedRouteRecord) {
			throw new ProvilacException("deletedRouteRecord with this Id does not exists");
		}
		return deletedRouteRecord;
	}
	
	/**
	 * 
	 * Updates the DeletedRouteRecord. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing DeletedRouteRecord
	 * @param DeletedRouteRecord - Updated DeletedRouteRecord
	 * @param loggedInDeletedRouteRecord - LoggedIn DeletedRouteRecord
	 * @return Updated DeletedRouteRecord
	 * 
	 * @author Vishal
	 */
	public DeletedRouteRecord updateDeletedRouteRecord(Long id, DeletedRouteRecord deletedRouteRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeletedRouteRecord id");
		}
		DeletedRouteRecord existingDeletedRouteRecord = deletedRouteRecordRepository.findOne(id);
		if(null == existingDeletedRouteRecord) {
			throw new ProvilacException("DeletedRouteRecord with this Id does not exists");
		}
		existingDeletedRouteRecord.setCustomer(deletedRouteRecord.getCustomer());
		existingDeletedRouteRecord.setRoute(deletedRouteRecord.getRoute());
		existingDeletedRouteRecord = deletedRouteRecordRepository.save(existingDeletedRouteRecord);
		activityLogService.createActivityLog(loggedInUser, "update", "deletedRouteRecord", id);
		existingDeletedRouteRecord.setCipher(EncryptionUtil.encode(deletedRouteRecord.getId()+""));
		if(populateTransientFields)
			existingDeletedRouteRecord =populateTransientFields(existingDeletedRouteRecord, loggedInUser);
		if(fetchEagerly)
			existingDeletedRouteRecord=fetchEagerly(existingDeletedRouteRecord);
		return existingDeletedRouteRecord;
	}
	
	@Transactional
	public void deleteAllRecordsForCustomer(Long customerId) {
		deletedRouteRecordRepository.deleteAllRecordsForCustomer(customerId);
	}
	
	private DeletedRouteRecord populateTransientFields(DeletedRouteRecord deletedRouteRecord, User loggedInUser) {
		
		if(null == deletedRouteRecord) {
			return deletedRouteRecord;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid DeletedRouteRecord");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			deletedRouteRecord.setIsEditAllowed(true);
			deletedRouteRecord.setIsDeleteAllowed(true);
		} else {
			deletedRouteRecord.setIsEditAllowed(false);
			deletedRouteRecord.setIsDeleteAllowed(false);
		}
		deletedRouteRecord.setCipher(EncryptionUtil.encode(deletedRouteRecord.getId()+""));
		return deletedRouteRecord;
	}
	
	private DeletedRouteRecord fetchEagerly(DeletedRouteRecord deletedRouteRecord) {
		
		return deletedRouteRecord;
	}
	
}

