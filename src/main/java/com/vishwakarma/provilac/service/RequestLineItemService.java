package com.vishwakarma.provilac.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.RequestLineItemSpecifications;
import com.vishwakarma.provilac.repository.RequestLineItemRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class RequestLineItemService {

	@Resource
	private  RequestLineItemRepository requestLineItemRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService; 
	
	@Transactional
	public  RequestLineItem createRequestLineItem( RequestLineItem  requestLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null ==  requestLineItem) {
			throw new ProvilacException("Invalid  requestLineItem");
		}
		requestLineItem =  requestLineItemRepository.save(requestLineItem);
		 activityLogService.createActivityLog(loggedInUser, "add", "requestLineItem", requestLineItem.getId());
		
		if (populateTransientFields) {
			requestLineItem = populateTransientFields( requestLineItem, loggedInUser);
		}
		
		if(fetchEagerly) {
			requestLineItem= fetchEagerly( requestLineItem);
		}
		return  requestLineItem;
	}
	
	@Transactional
	public Collection<RequestLineItem> createRequestLineItems(Collection<RequestLineItem> lineItems,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null == lineItems){
			throw new ProvilacException("Invalid requestLineItem");
		}
		lineItems = requestLineItemRepository.save(lineItems);
		for(RequestLineItem requestLineItem:lineItems){
			activityLogService.createActivityLog(loggedInUser, "add", "requestLineItem",requestLineItem.getId());
		}
		if(populateTransientFields||fetchEagerly){
			for (RequestLineItem requestLineItem : lineItems) {
				if(populateTransientFields)
					requestLineItem = populateTransientFields(requestLineItem, loggedInUser);
				if(fetchEagerly)
					requestLineItem = fetchEagerly(requestLineItem);
			}
		}
		return lineItems;
	}
	
	@Transactional
	public List<RequestLineItem> getAllRequestLineItems(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List< RequestLineItem>  requestLineItems =  requestLineItemRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for ( RequestLineItem  requestLineItem :  requestLineItems) {
			if(populateTransientFields)
				requestLineItem = populateTransientFields( requestLineItem, loggedInUser);
			if (fetchEagerly)
				requestLineItem = fetchEagerly( requestLineItem);
			}
		}
		return  requestLineItems;
	}
	
	@Transactional
	public Page<RequestLineItem> getRequestLineItems(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<RequestLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  requestLineItemRepository.findAll(pageRequest);
		Iterator< RequestLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 RequestLineItem  requestLineItem = ( RequestLineItem) iterator.next();
			if(populateTransientFields)
				requestLineItem = populateTransientFields( requestLineItem, loggedInUser);
			if(fetchEagerly)
				requestLineItem = fetchEagerly( requestLineItem);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page< RequestLineItem> searchRequestLineItem(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page< RequestLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  requestLineItemRepository.findAll( RequestLineItemSpecifications.search(searchTerm), pageRequest);
		Iterator< RequestLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 RequestLineItem  requestLineItem = ( RequestLineItem) iterator.next();
			if(populateTransientFields)
				requestLineItem = populateTransientFields( requestLineItem, loggedInUser);
			if(fetchEagerly)
				requestLineItem = fetchEagerly( requestLineItem);
			}
		}
		return page;
	}
	
	public  RequestLineItem getRequestLineItem(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  requestLineItem id");
		}
		 RequestLineItem  requestLineItem = getRequestLineItem(id, false);
		if(doPostValidation && null ==  requestLineItem) {
			throw new ProvilacException("requestLineItem with this Id does not exists");
		}
		if(populateTransientFields)
			requestLineItem= populateTransientFields(requestLineItem, loggedInUser);
		if(fetchEagerly)
			requestLineItem= fetchEagerly( requestLineItem);
		
		return  requestLineItem;
	}
	
/**
 * 
 * @param id
 * @param doPostValidation
 * @return
 */
	public  RequestLineItem getRequestLineItem(Long id, boolean doPostValidation) {
		
		 RequestLineItem  requestLineItem;
		if(null == id) {
			throw new ProvilacException("Invalid  requestLineItem id");
		}
		requestLineItem =  requestLineItemRepository.findOne(id);
		if(doPostValidation && null ==  requestLineItem) {
			throw new ProvilacException(" requestLineItem with this Id does not exists");
		}
		return  requestLineItem;
	}
	/**
	 * 
	 * @param id
	 * @param requestLineItem
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	public  RequestLineItem updateRequestLineItem(Long id,  RequestLineItem  requestLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  requestLineItem id");
		}
		 RequestLineItem existingRequestLineItem =  requestLineItemRepository.findOne(id);
		if(null == existingRequestLineItem) {
			throw new ProvilacException(" RequestLineItem with this Id does not exists");
		}
		existingRequestLineItem.setDay(requestLineItem.getDay());
		existingRequestLineItem.setProduct(requestLineItem.getProduct());
		existingRequestLineItem.setQuantity(requestLineItem.getQuantity());
		existingRequestLineItem.setCurrentIndex(requestLineItem.getCurrentIndex());
		existingRequestLineItem = requestLineItemRepository.save(existingRequestLineItem);
		activityLogService.createActivityLog(loggedInUser, "update", "requestLineItem", id);
		existingRequestLineItem.setCipher(EncryptionUtil.encode( requestLineItem.getId()+""));
		if(populateTransientFields)
			existingRequestLineItem =populateTransientFields(existingRequestLineItem, loggedInUser);
		if(fetchEagerly)
			existingRequestLineItem=fetchEagerly(existingRequestLineItem);
		return existingRequestLineItem;
	}
	
	@Transactional
	public Collection<RequestLineItem> updateRequestLineItems(Collection<RequestLineItem> requestLineItems,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null == requestLineItems){
			throw new ProvilacException("Invalid requestLineItem");
		}
		requestLineItems = requestLineItemRepository.save(requestLineItems);
		for(RequestLineItem requestLineItem:requestLineItems){
			activityLogService.createActivityLog(loggedInUser, "add", "requestLineItem",requestLineItem.getId());
		}
		if(populateTransientFields||fetchEagerly){
			for (RequestLineItem requestLineItem : requestLineItems) {
				if(populateTransientFields)
					requestLineItem = populateTransientFields(requestLineItem, loggedInUser);
				if(fetchEagerly)
					requestLineItem = fetchEagerly(requestLineItem);
			}
		}
		return requestLineItems;
	}
	public void deleteRequestLineItems(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  requestLineItems id");
		}
		 RequestLineItem  requestLineItem =  requestLineItemRepository.findOne(id);
		if(null ==  requestLineItem) {
			throw new ProvilacException(" requestLineItem with this Id does not exists");
		}
		 requestLineItemRepository.delete(id);
		 activityLogService.createActivityLog(null, "delete", "requestLineItems", id);
	}
	
	private  RequestLineItem populateTransientFields( RequestLineItem  requestLineItem, User loggedInUser) {
		
		if(null ==  requestLineItem) {
			return  requestLineItem;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid  requestLineItem");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			requestLineItem.setIsEditAllowed(true);
			requestLineItem.setIsDeleteAllowed(true);
		} else {
			requestLineItem.setIsEditAllowed(false);
			requestLineItem.setIsDeleteAllowed(false);
		}
		requestLineItem.setCipher(EncryptionUtil.encode( requestLineItem.getId()+""));
		return  requestLineItem;
	}
	
	private  RequestLineItem fetchEagerly(RequestLineItem requestLineItem) {
		
		return  requestLineItem;
	}
	
}

