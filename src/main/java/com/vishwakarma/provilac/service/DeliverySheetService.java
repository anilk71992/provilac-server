package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySheet;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.DeliverySheetSpecifications;
import com.vishwakarma.provilac.repository.DeliverySheetRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class DeliverySheetService {

	@Resource
	DeliverySheetRepository deliverySheetRepository;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	public DeliverySheet createDeliverySheet(DeliverySheet deliverySheet, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == deliverySheet) {
			throw new ProvilacException("Invalid deliverySheet");
		}
		deliverySheet = deliverySheetRepository.save(deliverySheet);
		activityLogService.createActivityLog(loggedInUser, "add", "DeliverySheet", deliverySheet.getId());
		
		if(fetchEagerly) {
			deliverySheet= fetchEagerly(deliverySheet);
		}
		return deliverySheet;
	}
	
	@Transactional
	public List<DeliverySheet> getAllDeliverySheets(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySheet> deliverySheets = deliverySheetRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (DeliverySheet deliverySheet : deliverySheets) {
			if(populateTransientFields)
				deliverySheet = populateTransientFields(deliverySheet, loggedInUser);
			if (fetchEagerly)
				deliverySheet = fetchEagerly(deliverySheet);
			}
		}
		return deliverySheets;
	}
	
	@Transactional
	public Page<DeliverySheet> getDeliverySheets(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<DeliverySheet> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliverySheetRepository.findAll(pageRequest);
		Iterator<DeliverySheet> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			DeliverySheet deliverySheet = (DeliverySheet) iterator.next();
			if(populateTransientFields)
				deliverySheet = populateTransientFields(deliverySheet, loggedInUser);
			if(fetchEagerly)
				deliverySheet = fetchEagerly(deliverySheet);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<DeliverySheet> searchDeliverySheet(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<DeliverySheet> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliverySheetRepository.findAll(DeliverySheetSpecifications.search(searchTerm), pageRequest);
		Iterator<DeliverySheet> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			DeliverySheet deliverySheet = (DeliverySheet) iterator.next();
			if(populateTransientFields)
				deliverySheet = populateTransientFields(deliverySheet, loggedInUser);
			if(fetchEagerly)
				deliverySheet = fetchEagerly(deliverySheet);
			}
		}
		return page;
	}
	
	@Transactional
	public List<DeliverySheet> getDeliverySheetsByDate(Date date,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		List<DeliverySheet> deliverySheets = deliverySheetRepository.findAll(DeliverySheetSpecifications.searchByDate(date));
		
		if(populateTransientFields||fetchEagerly) {
			for(DeliverySheet deliverySheet:deliverySheets){
				if(populateTransientFields){
					deliverySheet = populateTransientFields(deliverySheet, loggedInUser);
				}
				if(fetchEagerly){
					deliverySheet = fetchEagerly(deliverySheet);
				}
			}
		}
		return deliverySheets;
	}
	
	@Transactional
	public DeliverySheet getDeliverySheet(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == id) {
			throw new ProvilacException("Invalid DeliverySheet id");
		}
		DeliverySheet deliverySheet = getDeliverySheet(id, false);
		if(doPostValidation && null == deliverySheet) {
			throw new ProvilacException("DeliverySheet with this Id does not exists");
		}
		if(populateTransientFields)
			deliverySheet= populateTransientFields(deliverySheet, loggedInUser);
		if(fetchEagerly)
			deliverySheet= fetchEagerly(deliverySheet);

		return deliverySheet;
	}
	
	@Transactional
	public DeliverySheet getDeliverySheetByRoute(String userRouteCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		DeliverySheet deliverySheet = deliverySheetRepository.findByUserRoute_Code(userRouteCode);
		if(doPostValidation && null == deliverySheet) {
			throw new ProvilacException("DeliverySheet with this Id does not exists");
		}
		if(populateTransientFields)
			deliverySheet= populateTransientFields(deliverySheet, loggedInUser);
		if(fetchEagerly)
			deliverySheet= fetchEagerly(deliverySheet);

		return deliverySheet;
	}
	
	@Transactional
	public DeliverySheet getDeliverySheetByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid DeliverySheet id");
		}
		DeliverySheet deliverySheet = deliverySheetRepository.findByCode(code);
		if(doPostValidation && null == deliverySheet) {
			throw new ProvilacException("DeliverySheet with this Id does not exists");
		}
		if(populateTransientFields)
			deliverySheet= populateTransientFields(deliverySheet, loggedInUser);
		if(fetchEagerly)
			deliverySheet= fetchEagerly(deliverySheet);

		return deliverySheet;
	}	
	
	@Transactional
	public DeliverySheet getDeliverySheet(Long id, boolean doPostValidation) {
		
		DeliverySheet deliverySheet;
		if(null == id) {
			throw new ProvilacException("Invalid deliverySheet id");
		}
		deliverySheet = deliverySheetRepository.findOne(id);
		if(doPostValidation && null == deliverySheet) {
			throw new ProvilacException("deliverySheet with this Id does not exists");
		}
		return deliverySheet;
	}
	
	public void deleteDeliverySheet(Long id) {
		if(null == id) {
			throw new ProvilacException("Invalid DeliverySheet id");
		}
		DeliverySheet deliverySheet = deliverySheetRepository.findOne(id);
		if(null == deliverySheet) {
			throw new ProvilacException("DeliverySheet with this Id does not exists");
		}
		deliverySheetRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "DeliverySheet",id);
	}
	
	private DeliverySheet populateTransientFields(DeliverySheet deliverySheet, User loggedInUser) {
		
		if(null == deliverySheet) {
			return deliverySheet;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid DeliverySheet");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			deliverySheet.setIsEditAllowed(true);
			deliverySheet.setIsDeleteAllowed(true);
		} else {
			deliverySheet.setIsEditAllowed(false);
			deliverySheet.setIsDeleteAllowed(false);
		}
		deliverySheet.setCipher(EncryptionUtil.encode(deliverySheet.getId()+""));
		return deliverySheet;
	}
	
	private DeliverySheet fetchEagerly(DeliverySheet deliverySheet) {
		if(null != deliverySheet.getDeliverySchedules() && !deliverySheet.getDeliverySchedules().isEmpty()){
			deliverySheet.getDeliverySchedules().size();
			
			for(DeliverySchedule deliverySchedule:deliverySheet.getDeliverySchedules()){
				if(null != deliverySchedule.getDeliveryLineItems() && !deliverySchedule.getDeliveryLineItems().isEmpty()){
					deliverySchedule.getDeliveryLineItems().size();
				}
			}
		}
		return deliverySheet;
	}
}
