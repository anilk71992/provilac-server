package com.vishwakarma.provilac.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;
import com.vishwakarma.provilac.model.specifications.DeliveryScheduleSpecifications;
import com.vishwakarma.provilac.repository.DeliveryScheduleRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class DeliveryScheduleService {

	@Resource
	private DeliveryScheduleRepository deliveryScheduleRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private DeliveryLineItemService deliveryLineItemService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	public DeliverySchedule createDeliverySchedule(DeliverySchedule deliverySchedule, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == deliverySchedule) {
			throw new ProvilacException("Invalid deliverySchedule");
		}
		deliverySchedule = deliveryScheduleRepository.save(deliverySchedule);
		activityLogService.createActivityLog(loggedInUser, "add", "deliverySchedule", deliverySchedule.getId());
		for (DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
			lineItem.setDeliverySchedule(deliverySchedule);
		}
		deliveryLineItemService.createDeliveryLineItems(deliverySchedule.getDeliveryLineItems(), loggedInUser, false, false);
		
		if (populateTransientFields) {
			deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
		}
		
		if(fetchEagerly) {
			deliverySchedule= fetchEagerly(deliverySchedule);
		}
		return deliverySchedule;
	}
	
	@Transactional
	public Collection<DeliverySchedule> createDeliverySchedules(Collection<DeliverySchedule> deliverySchedules, boolean saveDeliveryLineItems, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly){
		
		if(null == deliverySchedules){
			throw new ProvilacException("Invalid deliverySchedule");
		}
		deliverySchedules = deliveryScheduleRepository.save(deliverySchedules);
		for(DeliverySchedule deliverySchedule:deliverySchedules){
			activityLogService.createActivityLog(loggedInUser, "add", "deliverySchedule", deliverySchedule.getId());
			if(saveDeliveryLineItems) {
				for(DeliveryLineItem lineItem : deliverySchedule.getDeliveryLineItems()) {
					lineItem.setDeliverySchedule(deliverySchedule);
				}
				deliveryLineItemService.createDeliveryLineItems(deliverySchedule.getDeliveryLineItems(), loggedInUser, populateTransientFields, fetchEagerly);
			}
		}
		if(populateTransientFields||fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if(fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getAllDeliverySchedules(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getAllDeliverySchedulesByDeliverySheet(Long DeliverySheetId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	/**
	 * 
	 * @param CustomerId
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return list of DeliverySchedules for perticular customer;
	 */
	@Transactional
	public List<DeliverySchedule> getAllDeliverySchedulesForCustomer(Long customrId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findByCustomer_Id(customrId);
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getAllDeliverySchedulesForCustomerFromDate(Long customrId, Date fromDate, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		DateHelper.setToStartOfDay(fromDate);
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findByCustomer_IdAndDateGreaterThanEqual(customrId, fromDate);
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliveredDeliverySchedulesByCustomerDateRangeAndDeliveredStatusAndSubscriptionType(Long customrId, Date fromDate, Date toDate, SubscriptionType subscriptiontype, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByCustomerIdAndDateRangeAndTypeNot(customrId, fromDate, toDate, DeliverySchedule.DeliveryStatus.Delivered, Type.Free_Trial),new Sort(Sort.Direction.ASC, "deliveryTime"));
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	

	@Transactional
	public List<DeliverySchedule> getDeliveredDeliverySchedulesByCustomerAndDate(Long customrId,Date date,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByCustomerAndDate(customrId, date),new Sort(Sort.Direction.ASC, "date"));
		if(populateTransientFields || fetchEagerly){
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByCustomerAndDateRangeAndSubscriptionTypeAndStatus(Long customrId, Date fromDate, Date toDate, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly, SubscriptionType subscriptionType, Type... types) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByCustomerIdAndDateRange(customrId, fromDate, toDate, subscriptionType, types));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByCustomerAndDateRange(String customerCode, Date fromDate, Date toDate, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDateRangeAndCustomer(fromDate, toDate, customerCode));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByStatusAndDateRange(Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByStatusAndDateRange(fromDate, toDate, DeliveryStatus.Delivered));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByDateRange(Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDateRange(fromDate, toDate));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByRoute(Long routeId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findByRoute_Id(routeId);
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	
	@Transactional
	public Page<DeliverySchedule> getDeliverySchedules(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<DeliverySchedule> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliveryScheduleRepository.findAll(pageRequest);
		Iterator<DeliverySchedule> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			DeliverySchedule deliverySchedule = (DeliverySchedule) iterator.next();
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if(fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<DeliverySchedule> searchDeliverySchedule(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<DeliverySchedule> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.search(searchTerm), pageRequest);
		Iterator<DeliverySchedule> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			DeliverySchedule deliverySchedule = (DeliverySchedule) iterator.next();
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if(fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return page;
	}
	/**
	 * 
	 * @param pageNumber
	 * @param searchTerm
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return DeliverySchedules By DeliveryStatus
	 */
	@Transactional
	public Page<DeliverySchedule> searchDeliveryScheduleByStatus(Integer pageNumber, DeliveryStatus searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		Page<DeliverySchedule> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDeliveryStatus(searchTerm), pageRequest);
		Iterator<DeliverySchedule> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				DeliverySchedule deliverySchedule = (DeliverySchedule) iterator.next();
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if(fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<DeliverySchedule> searchDeliveryScheduleByType(Integer pageNumber, Type type, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		Page<DeliverySchedule> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDeliveryType(type), pageRequest);
		Iterator<DeliverySchedule> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				DeliverySchedule deliverySchedule = (DeliverySchedule) iterator.next();
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if(fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return page;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliveryScheduleByType(Type type,Date startDate,Date endDate, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDeliveryTypeAndDateRange(type, startDate, endDate));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getScheduleByStatusAndDate( DeliveryStatus searchTerm,Date date, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDeliveryStatusAndDate(searchTerm,date));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getScheduleByRouteAndDate( Long routeId,Date date,int priority, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByRouteIdAndDate(routeId, date,priority),new Sort(Sort.Direction.ASC, "priority"));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getScheduleByRouteAndDate( Long routeId,Date date, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByRouteIdAndDate(routeId, date),new Sort(Sort.Direction.ASC, "priority"));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getScheduleByRouteAndDateAndPriority( Long routeId,Date date,int priority, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByRouteAndDateAndPriority(routeId, date, priority));
		for(DeliverySchedule deliverySchedule:deliverySchedules){
			if(populateTransientFields)
				deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
			if (fetchEagerly)
				deliverySchedule = fetchEagerly(deliverySchedule);
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByDate(Date date,User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByDate(date));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> getDeliverySchedulesByDateAndStatus(Date date,DeliveryStatus status,User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {

		List<DeliverySchedule>deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchDeliveryScheduleByDateAndStatus(date, status));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	@Transactional
	public DeliverySchedule getDeliverySchedule(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == id) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule deliverySchedule = getDeliverySchedule(id, false);
		if(doPostValidation && null == deliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		if(populateTransientFields)
			deliverySchedule= populateTransientFields(deliverySchedule, loggedInUser);
		if(fetchEagerly)
			deliverySchedule= fetchEagerly(deliverySchedule);

		return deliverySchedule;
	}
	
	@Transactional
	public DeliverySchedule getLastDeliverySchedule(String customerCode, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		if(StringUtils.isBlank(customerCode)) {
			throw new ProvilacException("Invalid customer code");
		}
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findByCustomer_Code(customerCode, new PageRequest(0, 1, Direction.DESC, "date"));
		if(doPostValidation && deliverySchedules.isEmpty()) {
			throw new ProvilacException("DeliverySchedule with this criteria does not exists");
		} else if(deliverySchedules.isEmpty()) {
			return null;
		}
		DeliverySchedule deliverySchedule = deliverySchedules.get(0);
		if(populateTransientFields)
			deliverySchedule= populateTransientFields(deliverySchedule, loggedInUser);
		if(fetchEagerly)
			deliverySchedule= fetchEagerly(deliverySchedule);

		return deliverySchedule;
	}

	@Transactional
	public DeliverySchedule getDeliveryScheduleByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule deliverySchedule = deliveryScheduleRepository.findByCode(code);
		if(doPostValidation && null == deliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		if(populateTransientFields)
			deliverySchedule= populateTransientFields(deliverySchedule, loggedInUser);
		if(fetchEagerly)
			deliverySchedule= fetchEagerly(deliverySchedule);

		return deliverySchedule;
	}
	
	/**
	 * Get all delivery schedules from given date
	 * @param fromDate
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	@Transactional
	public List<DeliverySchedule> getInvalidSchedulesFromDate(Date date, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == date) {
			throw new ProvilacException("Invalid date.");
		}
		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.getDeliverySchedulesFromDate(date));
		if(populateTransientFields || fetchEagerly) {
			for(DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule= populateTransientFields(deliverySchedule, loggedInUser);
				if(fetchEagerly)
					deliverySchedule= fetchEagerly(deliverySchedule);
			}
		}

		return deliverySchedules;
	}
	
	/*
	 * Returns all the delivery schedule for particular customer in whole day
	 */
	@Transactional
	public DeliverySchedule getDeliveryScheduleCustomerAndDate(String code, Date date, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid customer code.");
		}
		DeliverySchedule deliverySchedule = deliveryScheduleRepository.findOne(DeliveryScheduleSpecifications.searchDeliveryScheduleByCustomerIdAndDate(code, date));
		if(doPostValidation && null == deliverySchedule) {
			throw new ProvilacException("DeliverySchedule for selected customer and date does not exists.");
		}
		if (deliverySchedule == null) {
			return null;
		}
		if(populateTransientFields)
			deliverySchedule= populateTransientFields(deliverySchedule, loggedInUser);
		if(fetchEagerly)
			deliverySchedule= fetchEagerly(deliverySchedule);

		return deliverySchedule;
	}

	/**
	 * returns @DeliverySchedule without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	@Transactional
	public DeliverySchedule getDeliverySchedule(Long id, boolean doPostValidation) {
		
		DeliverySchedule deliverySchedule;
		if(null == id) {
			throw new ProvilacException("Invalid deliverySchedule id");
		}
		deliverySchedule = deliveryScheduleRepository.findOne(id);
		if(doPostValidation && null == deliverySchedule) {
			throw new ProvilacException("deliverySchedule with this Id does not exists");
		}
		return deliverySchedule;
	}
	
	@Transactional
	public boolean isDeliveryScheduleExistforCustomer(Long customerId) {

		Page<DeliverySchedule> page = null;
		PageRequest pageRequest = new PageRequest(0, 1, Direction.ASC, "id");
		page = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByCustomerId(customerId), pageRequest);
		
		if(page.hasContent()){
			return true;
		}else{
			return false;
		}
	}
	
	@Transactional
	public List<DeliverySchedule> getunbilledDeliverySchedule(Date fromDate,Date toDate,String customerCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<DeliverySchedule> deliverySchedules = deliveryScheduleRepository.findAll(DeliveryScheduleSpecifications.searchByDateRangeAndCustomer(fromDate, toDate,customerCode));
		if(populateTransientFields || fetchEagerly){
			for (DeliverySchedule deliverySchedule : deliverySchedules) {
				if(populateTransientFields)
					deliverySchedule = populateTransientFields(deliverySchedule, loggedInUser);
				if (fetchEagerly)
					deliverySchedule = fetchEagerly(deliverySchedule);
			}
		}
		return deliverySchedules;
	}
	
	/**
	 * 
	 * Updates the DeliverySchedule. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing DeliverySchedule
	 * @param DeliverySchedule - Updated DeliverySchedule
	 * @param loggedInDeliverySchedule - LoggedIn DeliverySchedule
	 * @return Updated DeliverySchedule
	 * 
	 * @author Vishal
	 */
	@Transactional
	public DeliverySchedule updateDeliverySchedule(Long id, DeliverySchedule deliverySchedule, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(id);
		if(null == existingDeliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		existingDeliverySchedule.setType(deliverySchedule.getType());
		existingDeliverySchedule.setCustomer(deliverySchedule.getCustomer());
		existingDeliverySchedule.setLat(deliverySchedule.getLat());
		existingDeliverySchedule.setLng(deliverySchedule.getLng());
		existingDeliverySchedule.setDeliveryTime(deliverySchedule.getDeliveryTime());
		existingDeliverySchedule.setDate(deliverySchedule.getDate()); 
		existingDeliverySchedule.setInvoice(deliverySchedule.getInvoice());
		existingDeliverySchedule.setReason(deliverySchedule.getReason());
		existingDeliverySchedule.setUpdatedFromMobile(deliverySchedule.isUpdatedFromMobile());
		existingDeliverySchedule.setUpdatedFromWeb(deliverySchedule.isUpdatedFromWeb());
		if(null != deliverySchedule.getPermanantNote()){
			existingDeliverySchedule.setPermanantNote(deliverySchedule.getPermanantNote());
		}
		if(null != deliverySchedule.getTempararyNote()){
			existingDeliverySchedule.setTempararyNote(deliverySchedule.getTempararyNote());
		}
		
		existingDeliverySchedule.setRoute(deliverySchedule.getRoute());
		existingDeliverySchedule.setPriority(deliverySchedule.getPriority());
		existingDeliverySchedule.setDeliverySheet(deliverySchedule.getDeliverySheet());
		existingDeliverySchedule = deliveryScheduleRepository.save(existingDeliverySchedule);
		activityLogService.createActivityLog(loggedInUser, "update", "DeliverySchedule", id);
		
		existingDeliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule.getId()+""));
		if(populateTransientFields)
			existingDeliverySchedule =populateTransientFields(existingDeliverySchedule, loggedInUser);
		if(fetchEagerly)
			existingDeliverySchedule=fetchEagerly(existingDeliverySchedule);
		return existingDeliverySchedule;
	}
	
	@Transactional
	public DeliverySchedule updateDeliveryScheduleByCustomer(Long id, DeliverySchedule deliverySchedule, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(id);
		if(null == existingDeliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		
		existingDeliverySchedule = deliveryScheduleRepository.save(existingDeliverySchedule);
		activityLogService.createActivityLog(loggedInUser, "update", "DeliverySchedule", id);
		
		existingDeliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule.getId()+""));
		if(populateTransientFields)
			existingDeliverySchedule =populateTransientFields(existingDeliverySchedule, loggedInUser);
		if(fetchEagerly)
			existingDeliverySchedule=fetchEagerly(existingDeliverySchedule);
		return existingDeliverySchedule;
	}
	
	/**
	 * 
	 * Update DeliverySchedule status and reason
	 * 
	 * @param id
	 * @param deliverySchedule
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	@Transactional
	public DeliverySchedule updateDeliveryScheduleStatusAndReason(Long id,DeliverySchedule deliverySchedule, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(id);
		if(null == existingDeliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		
		existingDeliverySchedule.setStatus(deliverySchedule.getStatus());
		existingDeliverySchedule.setReason(deliverySchedule.getReason());
		existingDeliverySchedule = deliveryScheduleRepository.save(existingDeliverySchedule);
		activityLogService.createActivityLog(loggedInUser, "update", "deliverySchedule",id);
				
		if (deliverySchedule.getStatus() == DeliveryStatus.NotDelivered) {
			notificationHelper.sendNotificationForNonDeliveredOrder(deliverySchedule);
		}
		
		existingDeliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule.getId()+""));
		if(populateTransientFields)
			existingDeliverySchedule =populateTransientFields(existingDeliverySchedule, loggedInUser);
		if(fetchEagerly)
			existingDeliverySchedule=fetchEagerly(existingDeliverySchedule);
		return existingDeliverySchedule;
	}
	
	@Transactional
	public List<DeliverySchedule> updateDeliverySchedules(List<DeliverySchedule>deliverySchedules, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == deliverySchedules) {
			throw new ProvilacException("Invalid DeliverySchedules");
		}
		List<DeliverySchedule> existingDeliverySchedules = new ArrayList<DeliverySchedule>();
		for (DeliverySchedule deliverySchedule2 : deliverySchedules) {
			DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(deliverySchedule2.getId());
			if(null == existingDeliverySchedule) {
				throw new ProvilacException("DeliverySchedule with this Id does not exists");
			}
			
			existingDeliverySchedule.setRoute(deliverySchedule2.getRoute());
			existingDeliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule2.getId()+""));
			existingDeliverySchedules.add(existingDeliverySchedule);
		}
		
		deliveryScheduleRepository.save(existingDeliverySchedules);
		
		for(DeliverySchedule deliverySchedule:deliverySchedules){
			activityLogService.createActivityLog(loggedInUser, "update", "deliverySchedule", deliverySchedule.getId());
		}
		return existingDeliverySchedules;
	}
	
	@Transactional
	public List<DeliverySchedule> updateSubscriptionType(List<DeliverySchedule> deliverySchedules, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliverySchedule> existingDeliverySchedules = new ArrayList<DeliverySchedule>();
		for (DeliverySchedule deliverySchedule : deliverySchedules) {
			
			DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(deliverySchedule.getId());
			existingDeliverySchedule.setSubscriptionType(deliverySchedule.getSubscriptionType());
			existingDeliverySchedules.add(existingDeliverySchedule);
		}
		existingDeliverySchedules = deliveryScheduleRepository.save(existingDeliverySchedules);
		
		if(populateTransientFields || fetchEagerly) {
			for (DeliverySchedule existingDeliverySchedule : existingDeliverySchedules) {
				existingDeliverySchedule.setCipher(EncryptionUtil.encode(existingDeliverySchedule.getId()+""));
				if(populateTransientFields) {
					existingDeliverySchedule = populateTransientFields(existingDeliverySchedule, loggedInUser);
				}
				if(fetchEagerly) {
					existingDeliverySchedule = fetchEagerly(existingDeliverySchedule);
				}
			}
		}
		return existingDeliverySchedules;
	}
	
	@Transactional
	public DeliverySchedule updateSubscriptionType(DeliverySchedule deliverySchedule, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == deliverySchedule) {
			throw new ProvilacException("Invalid delivery schedule");
		}
		DeliverySchedule existingDeliverySchedule = deliveryScheduleRepository.findOne(deliverySchedule.getId());
		existingDeliverySchedule.setSubscriptionType(deliverySchedule.getSubscriptionType());
		existingDeliverySchedule = deliveryScheduleRepository.save(existingDeliverySchedule);
		existingDeliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule.getId()+""));
		
		if(populateTransientFields) {
			existingDeliverySchedule = populateTransientFields(existingDeliverySchedule, loggedInUser);
		}
		if(fetchEagerly) {
			existingDeliverySchedule = fetchEagerly(existingDeliverySchedule);
		}
		return existingDeliverySchedule;
	}
	
	public void deleteDeliverySchedule(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid DeliverySchedule id");
		}
		DeliverySchedule deliverySchedule = deliveryScheduleRepository.findOne(id);
		if(null == deliverySchedule) {
			throw new ProvilacException("DeliverySchedule with this Id does not exists");
		}
		List<DeliverySchedule> deliverySchedules1 = new ArrayList<DeliverySchedule>();
		List<DeliverySchedule>deliverySchedules = getScheduleByRouteAndDate(deliverySchedule.getRoute().getId(), deliverySchedule.getDate(), deliverySchedule.getPriority(), null, false, false);
		for (DeliverySchedule deliverySchedule2 : deliverySchedules) {
			if (deliverySchedule2.getPriority()>1){
				deliverySchedule2.setPriority(deliverySchedule2.getPriority()-1);
				deliverySchedules1.add(deliverySchedule2);
			}
		//	updateDeliverySchedule(deliverySchedule2.getId(), deliverySchedule2, null, false, false);
		}
		//TODO: Call update method
		Collection<DeliverySchedule>schedules = createDeliverySchedules(deliverySchedules1, false, null, false, false);
		deliveryScheduleRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "deliverySchedule", id);
	}
	
	private DeliverySchedule populateTransientFields(DeliverySchedule deliverySchedule, User loggedInUser) {
		
		if(null == deliverySchedule) {
			return deliverySchedule;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid DeliverySchedule");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			deliverySchedule.setIsEditAllowed(true);
			deliverySchedule.setIsDeleteAllowed(true);
		} else {
			deliverySchedule.setIsEditAllowed(false);
			deliverySchedule.setIsDeleteAllowed(false);
		}
		deliverySchedule.setCipher(EncryptionUtil.encode(deliverySchedule.getId()+""));
		return deliverySchedule;
	}
	
	private DeliverySchedule fetchEagerly(DeliverySchedule deliverySchedule) {
		if( deliverySchedule.getDeliveryLineItems().size()!=0 && !deliverySchedule.getDeliveryLineItems().isEmpty()){
			deliverySchedule.getDeliveryLineItems().size();
//			if(null != deliverySchedule.getCustomer().getReferredBy()){
//				deliverySchedule.getCustomer().getReferredBy().getUserRoles().size();
//			}
		}
		return deliverySchedule;
	}
	
}

