package com.vishwakarma.provilac.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.RouteRecordSpecifications;
import com.vishwakarma.provilac.repository.RouteRecordRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.FirebaseWrapper;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class RouteRecordService {

	@Resource
	private RouteRecordRepository routeRecordRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private RouteService routeService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private FirebaseWrapper firebaseWrapper;
	
	@Transactional
	public RouteRecord createRouteRecordForChangeRoute(RouteRecord routeRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == routeRecord) {
			throw new ProvilacException("Invalid routeRecord");
		}
		routeRecord = routeRecordRepository.save(routeRecord);
		activityLogService.createActivityLog(loggedInUser, "add", "RouteRecord", routeRecord.getId());
		
		firebaseWrapper.sendRouteRecordChangeNotification(routeRecord.getCustomer(), routeRecord.getRoute());
		
		List<RouteRecord> routeRecords= new ArrayList<RouteRecord>(routeRecord.getRoute().getRouteRecords());
		Collections.sort(routeRecords, new Comparator<RouteRecord>() {
			@Override
			public int compare(RouteRecord arg0, RouteRecord arg1) {
				return arg0.getPriority() - arg1.getPriority();
			}
		});
		List<RouteRecord>records = new ArrayList<RouteRecord>();
		if(routeRecords.size()!=0 && routeRecords.size()!=1){
			if(routeRecords.contains(routeRecord)){
				List<RouteRecord> existngRecords = new ArrayList<RouteRecord>(routeRecords.subList(routeRecords.indexOf(routeRecord), routeRecords.size()));
				for (RouteRecord routeRecord2 : existngRecords) {
					routeRecord2.setPriority(routeRecord2.getPriority()+1);
					records.add(routeRecord2);
					//updateRouteRecord(routeRecord2.getId(), routeRecord2, loggedInUser, false, false);
				}
			}else{
				List<RouteRecord> existngRecords = new ArrayList<RouteRecord>(routeRecords.subList(routeRecord.getPriority()-1, routeRecords.size()));
				for (RouteRecord routeRecord2 : existngRecords) {
					routeRecord2.setPriority(routeRecord2.getPriority()+1);
					records.add(routeRecord2);
					//updateRouteRecord(routeRecord2.getId(), routeRecord2, loggedInUser, false, false);
				}
			}
		}
		updateRouteRecords(records, loggedInUser, false, false);
		
		if (populateTransientFields) {
			routeRecord = populateTransientFields(routeRecord, loggedInUser);
		}

		if(fetchEagerly) {
			routeRecord= fetchEagerly(routeRecord);
		}
		return routeRecord;
	}
	
	@Transactional
	public Collection<RouteRecord> createRouteRecords(Collection<RouteRecord> routeRecords, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		
		routeRecords = routeRecordRepository.save(routeRecords);
		for(RouteRecord routesRecord:routeRecords){
			activityLogService.createActivityLog(loggedInUser, "createRouteRecord", "RouteRecord", routesRecord.getId());
		}
		if(populateTransientFields || fetchEagerly)
			for (RouteRecord routeRecord : routeRecords) {
				
				if(populateTransientFields)
					routeRecord = populateTransientFields(routeRecord, loggedInUser);
				
				if(fetchEagerly)
					routeRecord = fetchEagerly(routeRecord);
			}
		return routeRecords;
	}
	
	@Transactional
	public List<RouteRecord> getAllRouteRecords(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<RouteRecord> routeRecords = routeRecordRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (RouteRecord routeRecord : routeRecords) {
			if(populateTransientFields)
				routeRecord = populateTransientFields(routeRecord, loggedInUser);
			if (fetchEagerly)
				routeRecord = fetchEagerly(routeRecord);
			}
		}
		return routeRecords;
	}
	
	@Transactional
	public List<RouteRecord> getAllRouteRecordsByCustomer(User customer) {
		
		List<RouteRecord> routeRecords = routeRecordRepository.findByCustomer(customer);
		return routeRecords;
	}
	
	@Transactional
	public List<RouteRecord> getRouteRecordsByRouteCode(String code,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<RouteRecord> routeRecords = routeRecordRepository.findByRouteCode(code);
		if(populateTransientFields || fetchEagerly){
		for (RouteRecord routeRecord : routeRecords) {
			if(populateTransientFields)
				routeRecord = populateTransientFields(routeRecord, loggedInUser);
			if (fetchEagerly)
				routeRecord = fetchEagerly(routeRecord);
			}
		}
		return routeRecords;
	}
	
	@Transactional
	public Page<RouteRecord> getRouteRecords(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<RouteRecord> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = routeRecordRepository.findAll(pageRequest);
		Iterator<RouteRecord> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			RouteRecord routeRecord = (RouteRecord) iterator.next();
			if(populateTransientFields)
				routeRecord = populateTransientFields(routeRecord, loggedInUser);
			if(fetchEagerly)
				routeRecord = fetchEagerly(routeRecord);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<RouteRecord> searchRouteRecord(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<RouteRecord> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = routeRecordRepository.findAll(RouteRecordSpecifications.search(searchTerm), pageRequest);
		Iterator<RouteRecord> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			RouteRecord routeRecord = (RouteRecord) iterator.next();
			if(populateTransientFields)
				routeRecord = populateTransientFields(routeRecord, loggedInUser);
			if(fetchEagerly)
				routeRecord = fetchEagerly(routeRecord);
			}
		}
		return page;
	}
	
	public RouteRecord getRouteRecord(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid RouteRecord id");
		}
		RouteRecord routeRecord = getRouteRecord(id, false);
		if(doPostValidation && null == routeRecord) {
			throw new ProvilacException("RouteRecord with this Id does not exists");
		}
		if(populateTransientFields)
			routeRecord= populateTransientFields(routeRecord, loggedInUser);
		if(fetchEagerly)
			routeRecord= fetchEagerly(routeRecord);
		
		return routeRecord;
	}
	

	/**
	 * 
	 * @param customerCode
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	public RouteRecord getRouteRecordByCustomer(String customerCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == customerCode) {
			throw new ProvilacException("Invalid customerCode");
		}
		RouteRecord routeRecord = routeRecordRepository.findByCustomer_Code(customerCode);
		if(doPostValidation && null == routeRecord) {
			throw new ProvilacException("RouteRecord with this Id does not exists");
		}
		if(populateTransientFields)
			routeRecord= populateTransientFields(routeRecord, loggedInUser);
		if(fetchEagerly)
			routeRecord= fetchEagerly(routeRecord);
		
		return routeRecord;
	}
	/**
	 * returns @RouteRecord without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public RouteRecord getRouteRecord(Long id, boolean doPostValidation) {
		
		RouteRecord routeRecord;
		if(null == id) {
			throw new ProvilacException("Invalid routeRecord id");
		}
		routeRecord = routeRecordRepository.findOne(id);
		if(doPostValidation && null == routeRecord) {
			throw new ProvilacException("routeRecord with this Id does not exists");
		}
		return routeRecord;
	}
	
	/**
	 * 
	 * Updates the RouteRecord. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing RouteRecord
	 * @param RouteRecord - Updated RouteRecord
	 * @param loggedInRouteRecord - LoggedIn RouteRecord
	 * @return Updated RouteRecord
	 * 
	 * @author Vishal
	 */
	public RouteRecord updateRouteRecord(Long id, RouteRecord routeRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid RouteRecord id");
		}
		RouteRecord existingRouteRecord = routeRecordRepository.findOne(id);
		if(null == existingRouteRecord) {
			throw new ProvilacException("RouteRecord with this Id does not exists");
		}
		existingRouteRecord.setCustomer(routeRecord.getCustomer());
		existingRouteRecord.setRoute(routeRecord.getRoute());
		existingRouteRecord.setPriority(routeRecord.getPriority());
		existingRouteRecord = routeRecordRepository.save(existingRouteRecord);
		activityLogService.createActivityLog(loggedInUser, "update", "RouteRecord", id );
		existingRouteRecord.setCipher(EncryptionUtil.encode(routeRecord.getId()+""));
		if(populateTransientFields)
			existingRouteRecord =populateTransientFields(existingRouteRecord, loggedInUser);
		if(fetchEagerly)
			existingRouteRecord=fetchEagerly(existingRouteRecord);
		return existingRouteRecord;
	}
	
	@Transactional
	public Collection<RouteRecord> updateRouteRecords(Collection<RouteRecord> routeRecords, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		
		routeRecords = routeRecordRepository.save(routeRecords);
		for(RouteRecord routesRecord:routeRecords){
			activityLogService.createActivityLog(loggedInUser, "update", "RouteRecord", routesRecord.getId());
		}
		if(populateTransientFields || fetchEagerly)
			for (RouteRecord routeRecord : routeRecords) {
				
				if(populateTransientFields)
					routeRecord = populateTransientFields(routeRecord, loggedInUser);
				
				if(fetchEagerly)
					routeRecord = fetchEagerly(routeRecord);
			}
		return routeRecords;
	}
	
	@Transactional
	public void deleteRouteRecord(Long id,Long routeId) {

		if(null == id) {
			throw new ProvilacException("Invalid RouteRecord id");
		}
		RouteRecord routeRecord = routeRecordRepository.findOne(id);
		
		if(null == routeRecord) {
			throw new ProvilacException("RouteRecord with this Id does not exists");
		}
		
		Route route = routeService.getRoute(routeId, false, null, false, true);
		List<RouteRecord> routeRecords = new ArrayList<RouteRecord>(route.getRouteRecords());
		Collections.sort(routeRecords, new Comparator<RouteRecord>() {
			@Override
			public int compare(RouteRecord arg0, RouteRecord arg1) {
				return arg0.getPriority() - arg1.getPriority();
			}
		});
		List<RouteRecord>recordsList = new ArrayList<RouteRecord>();
		List<RouteRecord> records = new ArrayList<RouteRecord>(routeRecords.subList(routeRecords.indexOf(routeRecord),routeRecords.size()));
		for (RouteRecord record : records) {
			record.setPriority(record.getPriority()-1);
			recordsList.add(record);
			//updateRouteRecord(record.getId(), record, null, false, false);
		}
		updateRouteRecords(recordsList, null, false, false);
		routeRecordRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "RouteRecord", id);
		
	}
	private RouteRecord populateTransientFields(RouteRecord routeRecord, User loggedInUser) {
		
		if(null == routeRecord) {
			return routeRecord;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid RouteRecord");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			routeRecord.setIsEditAllowed(true);
			routeRecord.setIsDeleteAllowed(true);
		} else {
			routeRecord.setIsEditAllowed(false);
			routeRecord.setIsDeleteAllowed(false);
		}
		routeRecord.setCipher(EncryptionUtil.encode(routeRecord.getId()+""));
		return routeRecord;
	}
	
	private RouteRecord fetchEagerly(RouteRecord routeRecord) {
		
		return routeRecord;
	}
	
}

