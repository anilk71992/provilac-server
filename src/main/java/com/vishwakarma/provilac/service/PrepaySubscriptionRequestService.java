package com.vishwakarma.provilac.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.PrepayRequestLineItem;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.PrepaySubscriptionRequestSpecifications;
import com.vishwakarma.provilac.repository.PrepaySubscriptionRequestRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

 
@Service
public class PrepaySubscriptionRequestService {

	@Resource
	private PrepaySubscriptionRequestRepository prepaySubscriptionRequestRepository;
	
	@Resource
	private PrepayRequestLineItemService prepayRequestLineItemService;
	
	@Transactional
	public PrepaySubscriptionRequest createPrepaySubscriptionRequest(PrepaySubscriptionRequest prepaySubscriptionRequest, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		if (null == prepaySubscriptionRequest) {
			throw new ProvilacException("Invalid prepaySubscriptionRequest");
		}
		prepaySubscriptionRequest = prepaySubscriptionRequestRepository.save(prepaySubscriptionRequest);
		Set<PrepayRequestLineItem> lineItems= new HashSet<PrepayRequestLineItem>();
		for (PrepayRequestLineItem lineItem : prepaySubscriptionRequest.getPrepayRequestLineItems()) {
			lineItem.setPrepaySubscriptionRequest(prepaySubscriptionRequest);
			lineItems.add(lineItem);
		}
		prepayRequestLineItemService.createPrepayRequestLineItems(lineItems, loggedInUser, false, false);
		if (populateTransientFields) {
			prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
		}
		
		if(fetchEagerly) {
			prepaySubscriptionRequest= fetchEagerly(prepaySubscriptionRequest);
		}
		return prepaySubscriptionRequest;
	}
	
	public Long getCountByRequestType(boolean isChangeRequest) {
		return prepaySubscriptionRequestRepository.getCountForRequestType(isChangeRequest);
	}
	
	@Transactional
	public List<PrepaySubscriptionRequest> getAllsubScriptionRequests(User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<PrepaySubscriptionRequest> prepaySubscriptionRequests = prepaySubscriptionRequestRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (PrepaySubscriptionRequest prepaySubscriptionRequest : prepaySubscriptionRequests) {
				if(populateTransientFields)
					prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
				if (fetchEagerly)
					prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
			}
		}
		return prepaySubscriptionRequests;
	}
	
	@Transactional
	public Page<PrepaySubscriptionRequest> getPrepaySubscriptionRequests(Integer pageNumber, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<PrepaySubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = prepaySubscriptionRequestRepository.findByIsChangeRequest(false, pageRequest);
		Iterator<PrepaySubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) iterator.next();
			if(populateTransientFields)
				prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
			if(fetchEagerly)
				prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<PrepaySubscriptionRequest> getChangePrepaySubscriptionRequests(Integer pageNumber, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<PrepaySubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = prepaySubscriptionRequestRepository.findByIsChangeRequest(true, pageRequest);
		Iterator<PrepaySubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) iterator.next();
			if(populateTransientFields)
				prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
			if(fetchEagerly)
				prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<PrepaySubscriptionRequest> searchPrepaySubscriptionRequests(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<PrepaySubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = prepaySubscriptionRequestRepository.findAll(PrepaySubscriptionRequestSpecifications.search(searchTerm, false), pageRequest);
		
		Iterator<PrepaySubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) iterator.next();
			if(populateTransientFields)
				prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
			if(fetchEagerly)
				prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<PrepaySubscriptionRequest> searchChangePrepaySubscriptionRequests(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<PrepaySubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = prepaySubscriptionRequestRepository.findAll(PrepaySubscriptionRequestSpecifications.search(searchTerm, true), pageRequest);
		Iterator<PrepaySubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields || fetchEagerly) {
			while (iterator.hasNext()) {
				PrepaySubscriptionRequest prepaySubscriptionRequest = (PrepaySubscriptionRequest) iterator.next();
				if(populateTransientFields)
					prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
				if(fetchEagerly)
					prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public PrepaySubscriptionRequest getPrepaySubscriptionRequest(Long id, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid PrepaySubscriptionRequest id");
		}
		PrepaySubscriptionRequest prepaySubscriptionRequest = prepaySubscriptionRequestRepository.findOne(id);
		if(doPostValidation && null == prepaySubscriptionRequest) {
			throw new ProvilacException("PrepaySubscriptionRequest with this Id does not exists");
		}
		if(populateTransientFields)
			prepaySubscriptionRequest= populateTransientFields(prepaySubscriptionRequest, loggedInUser);
		if(fetchEagerly)
			prepaySubscriptionRequest= fetchEagerly(prepaySubscriptionRequest);
		
		return prepaySubscriptionRequest;
	}
	
	@Transactional
	public PrepaySubscriptionRequest getPrepaySubscriptionRequestByCode(String code, boolean doPostValidation, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid PrepaySubscriptionRequest id");
		}
		PrepaySubscriptionRequest prepaySubscriptionRequest = prepaySubscriptionRequestRepository.findByCode(code);
		if(doPostValidation && null == prepaySubscriptionRequest) {
			throw new ProvilacException("PrepaySubscriptionRequest with this Id does not exists");
		}
		if(populateTransientFields)
			prepaySubscriptionRequest= populateTransientFields(prepaySubscriptionRequest, loggedInUser);
		if(fetchEagerly)
			prepaySubscriptionRequest= fetchEagerly(prepaySubscriptionRequest);

		return prepaySubscriptionRequest;
	}
	
	@Transactional
	public List<PrepaySubscriptionRequest> getPrepaySubscriptionRequestsByCustomer(String code, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if (null == code) {
			throw new ProvilacException("Invalid User code");
		}
		List<PrepaySubscriptionRequest> prepaySubscriptionRequests = prepaySubscriptionRequestRepository.findByUser_Code(code);
		if(null==prepaySubscriptionRequests){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (PrepaySubscriptionRequest prepaySubscriptionRequest : prepaySubscriptionRequests) {
				if(populateTransientFields)
					prepaySubscriptionRequest = populateTransientFields(prepaySubscriptionRequest, loggedInUser);
				if (fetchEagerly)
					prepaySubscriptionRequest = fetchEagerly(prepaySubscriptionRequest);
				}
			}
		return prepaySubscriptionRequests;
	}
	
	/**
	 * 
	 * Updates the UserSubscription. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing UserSubscription
	 * @param UserSubscription - Updated UserSubscription
	 * @param loggedInUserSubscription - LoggedIn UserSubscription
	 * @return Updated UserSubscription
	 * 
	 * @author Vishal
	 */
	@Transactional
	public PrepaySubscriptionRequest updatePrepaySubscriptionRequest(Long id, PrepaySubscriptionRequest prepaySubscriptionRequest, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid prepaySubscriptionRequest id");
		}
		PrepaySubscriptionRequest existingPrepaySubscriptionRequest = prepaySubscriptionRequestRepository.findOne(id);
		if(null == existingPrepaySubscriptionRequest) {
			throw new ProvilacException("PrepaySubscriptionRequest with this Id does not exists");
		}
//		existingUserSubscription.setType(userSubscription.getType());
		existingPrepaySubscriptionRequest.setUser(prepaySubscriptionRequest.getUser());
		if(null!=prepaySubscriptionRequest.getPermanantNote()){
			existingPrepaySubscriptionRequest.setPermanantNote(prepaySubscriptionRequest.getPermanantNote());
		}
		existingPrepaySubscriptionRequest.setInternalNote(prepaySubscriptionRequest.getInternalNote());
		
		existingPrepaySubscriptionRequest = prepaySubscriptionRequestRepository.save(existingPrepaySubscriptionRequest);
		existingPrepaySubscriptionRequest.setCipher(EncryptionUtil.encode(prepaySubscriptionRequest.getId()+""));
		if(populateTransientFields)
			existingPrepaySubscriptionRequest =populateTransientFields(existingPrepaySubscriptionRequest, loggedInUser);
		if(fetchEagerly)
			existingPrepaySubscriptionRequest=fetchEagerly(existingPrepaySubscriptionRequest);
		return existingPrepaySubscriptionRequest;
	}
	
	public void deletePrepaySubscriptionRequest(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid PrepaySubscriptionRequest id");
		}
		PrepaySubscriptionRequest prepaySubscriptionRequest = prepaySubscriptionRequestRepository.findOne(id);
		if(null == prepaySubscriptionRequest) {
			throw new ProvilacException("PrepaySubscriptionRequest with this Id does not exists");
		}
		prepaySubscriptionRequestRepository.delete(id);
	}

	@Transactional
	public void deletePrepaySubscriptionRequestsForUser(Long userId) {
		
		if(null == userId) {
			throw new ProvilacException("Invalid user id");
		}
		prepaySubscriptionRequestRepository.deleteByUser(userId);
	}
	
	private PrepaySubscriptionRequest populateTransientFields(PrepaySubscriptionRequest prepaySubscriptionRequest, User loggedInUser) {
		
		if(null == prepaySubscriptionRequest) {
			return prepaySubscriptionRequest;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid PrepaySubscriptionRequest");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			prepaySubscriptionRequest.setIsEditAllowed(true);
			prepaySubscriptionRequest.setIsDeleteAllowed(true);
		} else {
			prepaySubscriptionRequest.setIsEditAllowed(false);
			prepaySubscriptionRequest.setIsDeleteAllowed(false);
		}
		prepaySubscriptionRequest.setCipher(EncryptionUtil.encode(prepaySubscriptionRequest.getId()+""));
		return prepaySubscriptionRequest;
	}
	
	private PrepaySubscriptionRequest fetchEagerly(PrepaySubscriptionRequest prepaySubscriptionRequest) {
		prepaySubscriptionRequest.getPrepayRequestLineItems().size();
		prepaySubscriptionRequest.getProductPricing().size();
		return prepaySubscriptionRequest;
	}
	
}

