package com.vishwakarma.provilac.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.DeliveryLineItemSpecifications;
import com.vishwakarma.provilac.repository.DeliveryLineItemRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class DeliveryLineItemService {

	@Resource
	private  DeliveryLineItemRepository  deliveryLineItemRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource 
	private UserService userService;
	
	
	@Transactional
	public  DeliveryLineItem createDeliveryLineItem( DeliveryLineItem  deliveryLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null ==  deliveryLineItem) {
			throw new ProvilacException("Invalid  deliveryLineItem");
		}
		 deliveryLineItem =  deliveryLineItemRepository.save( deliveryLineItem);
		 activityLogService.createActivityLog(loggedInUser, "add", "deliveryLineItem", deliveryLineItem.getId());
		
		if (populateTransientFields) {
			 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
		}
		
		if(fetchEagerly) {
			 deliveryLineItem= fetchEagerly( deliveryLineItem);
		}
		return  deliveryLineItem;
	}
	
	@Transactional
	public Collection<DeliveryLineItem> createDeliveryLineItems(Collection<DeliveryLineItem> deliveryLineItems,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null == deliveryLineItems){
			throw new ProvilacException("Invalid deliveryLineItem");
		}
		deliveryLineItems = deliveryLineItemRepository.save(deliveryLineItems);
		
		for(DeliveryLineItem deliveryLineItem:deliveryLineItems){
			activityLogService.createActivityLog(loggedInUser, "add", "deliveryLineItem", deliveryLineItem.getId());
		}
		if(populateTransientFields||fetchEagerly){
			for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
				if(populateTransientFields)
					deliveryLineItem = populateTransientFields(deliveryLineItem, loggedInUser);
				if(fetchEagerly)
					deliveryLineItem = fetchEagerly(deliveryLineItem);
			}
		}
		return deliveryLineItems;
	}
	
	@Transactional
	public List< DeliveryLineItem> getAllDeliveryLineItems(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List< DeliveryLineItem>  deliveryLineItems =  deliveryLineItemRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for ( DeliveryLineItem  deliveryLineItem :  deliveryLineItems) {
			if(populateTransientFields)
				 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
			if (fetchEagerly)
				 deliveryLineItem = fetchEagerly( deliveryLineItem);
			}
		}
		return  deliveryLineItems;
	}
	
	@Transactional
	public List< DeliveryLineItem> getAllDeliveryLineItemsByDeliverySchedule(Long scheduleId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List< DeliveryLineItem>  deliveryLineItems =  deliveryLineItemRepository.findByDeliveryScheduleId(scheduleId);
		if(populateTransientFields || fetchEagerly){
		for ( DeliveryLineItem  deliveryLineItem :  deliveryLineItems) {
			if(populateTransientFields)
				 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
			if (fetchEagerly)
				 deliveryLineItem = fetchEagerly( deliveryLineItem);
			}
		}
		return  deliveryLineItems;
	}
	
	@Transactional
	public List< DeliveryLineItem> getAllDeliveryLineItemsByDeliverySchedule(List<Long> scheduleIds,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List< DeliveryLineItem>  deliveryLineItems =  deliveryLineItemRepository.findByDeliveryScheduleIdIn(scheduleIds);
		if(populateTransientFields || fetchEagerly) {
			for ( DeliveryLineItem  deliveryLineItem :  deliveryLineItems) {
				if(populateTransientFields)
					 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
				if (fetchEagerly)
					 deliveryLineItem = fetchEagerly( deliveryLineItem);
			}
		}
		return  deliveryLineItems;
	}
	
	
	@Transactional
	public Page< DeliveryLineItem> getDeliveryLineItems(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page< DeliveryLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  deliveryLineItemRepository.findAll(pageRequest);
		Iterator< DeliveryLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 DeliveryLineItem  deliveryLineItem = ( DeliveryLineItem) iterator.next();
			if(populateTransientFields)
				 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
			if(fetchEagerly)
				 deliveryLineItem = fetchEagerly( deliveryLineItem);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page< DeliveryLineItem> searchDeliveryLineItem(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page< DeliveryLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  deliveryLineItemRepository.findAll( DeliveryLineItemSpecifications.search(searchTerm), pageRequest);
		Iterator< DeliveryLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 DeliveryLineItem  deliveryLineItem = ( DeliveryLineItem) iterator.next();
			if(populateTransientFields)
				 deliveryLineItem = populateTransientFields( deliveryLineItem, loggedInUser);
			if(fetchEagerly)
				 deliveryLineItem = fetchEagerly( deliveryLineItem);
			}
		}
		return page;
	}
	
	public  DeliveryLineItem getDeliveryLineItem(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  DeliveryLineItem id");
		}
		 DeliveryLineItem  deliveryLineItem = getDeliveryLineItem(id, false);
		if(doPostValidation && null ==  deliveryLineItem) {
			throw new ProvilacException(" DeliveryLineItem with this Id does not exists");
		}
		if(populateTransientFields)
			 deliveryLineItem= populateTransientFields( deliveryLineItem, loggedInUser);
		if(fetchEagerly)
			 deliveryLineItem= fetchEagerly( deliveryLineItem);
		
		return  deliveryLineItem;
	}
	
	/**
	 * 
	 * @param code
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 */
	public  DeliveryLineItem getDeliveryLineItemByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid  DeliveryLineItem code");
		}
		DeliveryLineItem  deliveryLineItem = deliveryLineItemRepository.findByCode(code);
		if(doPostValidation && null ==  deliveryLineItem) {
			throw new ProvilacException(" DeliveryLineItem with this Id does not exists");
		}
		if(populateTransientFields)
			deliveryLineItem= populateTransientFields( deliveryLineItem, loggedInUser);
		if(fetchEagerly)
			deliveryLineItem= fetchEagerly( deliveryLineItem);

		return  deliveryLineItem;
	}
	/**
	 * returns @ DeliveryLineItem without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public  DeliveryLineItem getDeliveryLineItem(Long id, boolean doPostValidation) {
		
		 DeliveryLineItem  deliveryLineItem;
		if(null == id) {
			throw new ProvilacException("Invalid  deliveryLineItem id");
		}
		 deliveryLineItem =  deliveryLineItemRepository.findOne(id);
		if(doPostValidation && null ==  deliveryLineItem) {
			throw new ProvilacException(" deliveryLineItem with this Id does not exists");
		}
		return  deliveryLineItem;
	}
	
	/**
	 * 
	 * Updates the  DeliveryLineItem. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing  DeliveryLineItem
	 * @param  DeliveryLineItem - Updated  DeliveryLineItem
	 * @param loggedIn DeliveryLineItem - LoggedIn  DeliveryLineItem
	 * @return Updated  DeliveryLineItem
	 * 
	 * @author Vishal
	 */
	public  DeliveryLineItem updateDeliveryLineItem(Long id,  DeliveryLineItem  deliveryLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  DeliveryLineItem id");
		}
		 DeliveryLineItem existingDeliveryLineItem =  deliveryLineItemRepository.findOne(id);
		if(null == existingDeliveryLineItem) {
			throw new ProvilacException(" DeliveryLineItem with this Id does not exists");
		}
		existingDeliveryLineItem.setDay(deliveryLineItem.getDay());
		existingDeliveryLineItem.setProduct(deliveryLineItem.getProduct());
		existingDeliveryLineItem.setQuantity(deliveryLineItem.getQuantity());
		existingDeliveryLineItem =  deliveryLineItemRepository.save(existingDeliveryLineItem);
		activityLogService.createActivityLog(loggedInUser, "update", "deliveryLineItem", id);
		existingDeliveryLineItem.setCipher(EncryptionUtil.encode( deliveryLineItem.getId()+""));
		if(populateTransientFields)
			existingDeliveryLineItem =populateTransientFields(existingDeliveryLineItem, loggedInUser);
		if(fetchEagerly)
			existingDeliveryLineItem=fetchEagerly(existingDeliveryLineItem);
		return existingDeliveryLineItem;
	}
	
	@Transactional
	public List<DeliveryLineItem> updatePriceAndTotalPrice(List<DeliveryLineItem> deliveryLineItems, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		List<DeliveryLineItem> existingLineItems = new ArrayList<DeliveryLineItem>();
		for (DeliveryLineItem deliveryLineItem : deliveryLineItems) {
			
			DeliveryLineItem existingLineItem = deliveryLineItemRepository.findOne(deliveryLineItem.getId());
			existingLineItem.setPricePerUnit(deliveryLineItem.getPricePerUnit());
			existingLineItem.setTotalPrice(deliveryLineItem.getTotalPrice());
			existingLineItems.add(existingLineItem);
		}
		existingLineItems = deliveryLineItemRepository.save(existingLineItems);
		if(populateTransientFields || fetchEagerly) {
			for (DeliveryLineItem deliveryLineItem : existingLineItems) {
				if(populateTransientFields) 
					deliveryLineItem = populateTransientFields(deliveryLineItem, loggedInUser);
				if(fetchEagerly)
					deliveryLineItem = fetchEagerly(deliveryLineItem);
			}
		}
		return existingLineItems;
	}
	
	@Transactional
	public DeliveryLineItem updatePriceAndTotalPrice(DeliveryLineItem deliveryLineItem, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if(null == deliveryLineItem) {
			throw new ProvilacException("Invalid delivery item");
		}
		DeliveryLineItem existingLineItem = deliveryLineItemRepository.findOne(deliveryLineItem.getId());
		existingLineItem.setPricePerUnit(deliveryLineItem.getPricePerUnit());
		existingLineItem.setTotalPrice(deliveryLineItem.getTotalPrice());
		existingLineItem = deliveryLineItemRepository.save(existingLineItem);
		
		if(populateTransientFields) 
			deliveryLineItem = populateTransientFields(deliveryLineItem, loggedInUser);
		if(fetchEagerly)
			deliveryLineItem = fetchEagerly(deliveryLineItem);
		
		return deliveryLineItem;
	}
	
	public void deleteDeliveryLineItem(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  DeliveryLineItem id");
		}
		 DeliveryLineItem  deliveryLineItem =  deliveryLineItemRepository.findOne(id);
		if(null ==  deliveryLineItem) {
			throw new ProvilacException(" DeliveryLineItem with this Id does not exists");
		}
		 deliveryLineItemRepository.delete(id);
		 activityLogService.createActivityLog(null, "delete", "DeliveryLineItem", id);
	}
	
	public void deleteAllDeliveryLineItem(Collection<DeliveryLineItem> deliveryLineItems) {

		if (deliveryLineItems.isEmpty()) {
			throw new ProvilacException("Invalid  DeliveryLineItems");
		}
		deliveryLineItemRepository.delete(deliveryLineItems);
		for(DeliveryLineItem deliveryLineItem:deliveryLineItems){
			activityLogService.createActivityLog(null, "delete", "deliveryLineItem", deliveryLineItem.getId());
			
		}
	}
	
	
	private  DeliveryLineItem populateTransientFields( DeliveryLineItem  deliveryLineItem, User loggedInUser) {
		
		if(null ==  deliveryLineItem) {
			return  deliveryLineItem;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid  DeliveryLineItem");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			 deliveryLineItem.setIsEditAllowed(true);
			 deliveryLineItem.setIsDeleteAllowed(true);
		} else {
			 deliveryLineItem.setIsEditAllowed(false);
			 deliveryLineItem.setIsDeleteAllowed(false);
		}
		 deliveryLineItem.setCipher(EncryptionUtil.encode( deliveryLineItem.getId()+""));
		return  deliveryLineItem;
	}
	
	private  DeliveryLineItem fetchEagerly( DeliveryLineItem  deliveryLineItem) {
		
		return  deliveryLineItem;
	}
	
}

