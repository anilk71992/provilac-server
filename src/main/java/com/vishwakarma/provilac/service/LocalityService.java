package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Locality;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.LocalitySpecifications;
import com.vishwakarma.provilac.repository.LocalityRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;

/**
 * 
 * @author Rohit
 *
 */

@Service
public class LocalityService {

	@Resource
	private LocalityRepository localityRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	/**
	 * 
	 * @param locality
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return locality object 
	 */
	@Transactional
	public Locality createLocality(Locality locality, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == locality) {
			throw new ProvilacException("Invalid locality");
		}
		locality = localityRepository.save(locality);
		activityLogService.createActivityLog(loggedInUser, "create", "Locality", locality.getId());
		
		if (populateTransientFields) {
			locality = populateTransientFields(locality, loggedInUser);
		}
		
		if(fetchEagerly) {
			locality= fetchEagerly(locality);
		}
		return locality;
	}
	
	/**
	 * 
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Set of all Localities
	 */
	@Transactional
	public List<Locality> getAllLocalities(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Locality> localities = localityRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Locality locality : localities) {
			if(populateTransientFields)
				locality = populateTransientFields(locality, loggedInUser);
			if (fetchEagerly)
				locality = fetchEagerly(locality);
			}
		}
		return localities;
	}
	
	@Transactional
	public List<Locality> getLocalitiesByCity(Long cityId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Locality> localities = localityRepository.findByCity_Id(cityId);
		if(populateTransientFields || fetchEagerly){
		for (Locality locality : localities) {
			if(populateTransientFields)
				locality = populateTransientFields(locality, loggedInUser);
			if (fetchEagerly)
				locality = fetchEagerly(locality);
			}
		}
		return localities;
	}
	

	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return page of Localities
	 */
		
	@Transactional
	public Page<Locality> getLocalities(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Locality> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = localityRepository.findAll(pageRequest);
		Iterator<Locality> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Locality locality = (Locality) iterator.next();
			if(populateTransientFields)
				locality = populateTransientFields(locality, loggedInUser);
			if(fetchEagerly)
				locality = fetchEagerly(locality);
			}
		}
		return page;
	}
	
	
	/**
	 * 
	 * @param pageNumber
	 * @param searchTerm
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return page of localities according to pageNumber and searchterm.
	 */
	@Transactional
	public Page<Locality> searchLocality(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Locality> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = localityRepository.findAll(LocalitySpecifications.search(searchTerm), pageRequest);
		Iterator<Locality> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Locality locality = (Locality) iterator.next();
				if(populateTransientFields)
					locality = populateTransientFields(locality, loggedInUser);
				if(fetchEagerly)
					locality = fetchEagerly(locality);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param id
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return locality with specified id
	 */
	@Transactional
	public Locality getLocality(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid locality id");
		}
		Locality locality = localityRepository.findOne(id);
		if(doPostValidation && null == locality) {
			throw new ProvilacException("Locality with this id does not exists");
		}
		if(locality == null){
			return null;
		}
		if(populateTransientFields)
			locality= populateTransientFields(locality, loggedInUser);
		if(fetchEagerly)
			locality= fetchEagerly(locality);
		
		return locality;
	}
	
	/**
	 * 
	 * @param localityName
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return locality with specified name
	 */
	@Transactional
	public Locality getLocalityByNameAndCity(String localityName,String cityCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(localityName)) {
			throw new ProvilacException("Invalid locality name");
		}
		Locality locality = localityRepository.findByNameAndCity_Code(localityName, cityCode);
		if(doPostValidation && null == locality) {
			throw new ProvilacException("Locality with this name does not exists");
		}
		if(locality == null){
			return null;
		}
		if(populateTransientFields)
			locality= populateTransientFields(locality, loggedInUser);
		if(fetchEagerly)
			locality= fetchEagerly(locality);
		
		return locality;
	}
	
	@Transactional
	public Locality getLocalityByName(String localityName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(localityName)) {
			throw new ProvilacException("Invalid locality name");
		}
		Locality locality = localityRepository.findByName(localityName);
		if(doPostValidation && null == locality) {
			throw new ProvilacException("Locality with this name does not exists");
		}
		if(locality == null){
			return null;
		}
		if(populateTransientFields)
			locality= populateTransientFields(locality, loggedInUser);
		if(fetchEagerly)
			locality= fetchEagerly(locality);
		
		return locality;
	}
	
	/**
	 *@author rohit 
	 * @param localityCode
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return locality by specified code.
	 */
	@Transactional
	public Locality getLocalityByCode(String localityCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(localityCode)) {
			throw new ProvilacException("Invalid locality code");
		}
		Locality locality = localityRepository.findByCode(localityCode);
		if(doPostValidation && null == locality) {
			throw new ProvilacException("Locality with this code does not exists");
		}
		else if (locality==null)
		{
			return null;
		}
		if(populateTransientFields)
			locality= populateTransientFields(locality, loggedInUser);
		if(fetchEagerly)
			locality= fetchEagerly(locality);
		
		return locality;
	}
	
	/**
	 * 
	 * 
	 * @param id - Id of existing Locality
	 * @param Locality - Updated Locality
	 * @return Updated Locality
	 */
	@Transactional
	public Locality updateLocality(Long id, Locality locality, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid locality id");
		}
		Locality existingLocality = localityRepository.findOne(id);
		if(null == existingLocality) {
			throw new ProvilacException("Locality with this id does not exists");
		}
		
		existingLocality.setPincode(locality.getPincode());
		existingLocality.setName(locality.getName());
		existingLocality.setCity(locality.getCity());
		existingLocality = localityRepository.save(existingLocality);
		activityLogService.createActivityLog(loggedInUser, "update", "Locality", locality.getId());
		existingLocality.setCipher(EncryptionUtil.encode(locality.getId()+""));
		
		if(populateTransientFields)
			existingLocality =populateTransientFields(existingLocality, loggedInUser);
		if(fetchEagerly)
			existingLocality=fetchEagerly(existingLocality);
		return existingLocality;
	}
	
	public void deleteLocality(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid locality id");
		}
		Locality locality = localityRepository.findOne(id);
		if(null == locality) {
			throw new ProvilacException("Locality with this id does not exists");
		}
		localityRepository.delete(id);
		
		User loggedInUser = userService.getLoggedInUser();
		activityLogService.createActivityLog(loggedInUser, "delete", "Locality", locality.getId());
				
	}
	
	private Locality populateTransientFields(Locality locality, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid locality");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			locality.setIsEditAllowed(true);
			locality.setIsDeleteAllowed(true);
		} else {
			locality.setIsEditAllowed(false);
			locality.setIsDeleteAllowed(false);
		}
		locality.setCipher(EncryptionUtil.encode(locality.getId()+""));
		return locality;
	}
	
	private Locality fetchEagerly(Locality locality) {
		
		return locality;
	}

	
	
}