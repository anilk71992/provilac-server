package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.SummarySheetRecordSpecifications;
import com.vishwakarma.provilac.repository.SummarySheetRecordRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class SummarySheetRecordService {

	@Resource
	private SummarySheetRecordRepository summarySheetRecordRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	@Resource
	private UserService userService;
	
	@Transactional
	public SummarySheetRecord createSummarySheetRecord(SummarySheetRecord summarySheetRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == summarySheetRecord) {
			throw new ProvilacException("Invalid summarySheetRecord");
		}
		summarySheetRecord = summarySheetRecordRepository.save(summarySheetRecord);
		activityLogService.createActivityLog(loggedInUser, "add", "SummarySheetRecord", summarySheetRecord.getId());
		if (populateTransientFields) {
			summarySheetRecord = populateTransientFields(summarySheetRecord, loggedInUser);
		}
		
		if(fetchEagerly) {
			summarySheetRecord= fetchEagerly(summarySheetRecord);
		}
		return summarySheetRecord;
	}
	
	@Transactional
	public List<SummarySheetRecord> getAllSummarySheetRecords(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<SummarySheetRecord> summarySheetRecords = summarySheetRecordRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (SummarySheetRecord summarySheetRecord : summarySheetRecords) {
			if(populateTransientFields)
				summarySheetRecord = populateTransientFields(summarySheetRecord, loggedInUser);
			if (fetchEagerly)
				summarySheetRecord = fetchEagerly(summarySheetRecord);
			}
		}
		return summarySheetRecords;
	}
	
	@Transactional
	public Page<SummarySheetRecord> getSummarySheetRecords(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<SummarySheetRecord> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = summarySheetRecordRepository.findAll(pageRequest);
		Iterator<SummarySheetRecord> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SummarySheetRecord summarySheetRecord = (SummarySheetRecord) iterator.next();
			if(populateTransientFields)
				summarySheetRecord = populateTransientFields(summarySheetRecord, loggedInUser);
			if(fetchEagerly)
				summarySheetRecord = fetchEagerly(summarySheetRecord);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<SummarySheetRecord> searchSummarySheetRecord(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<SummarySheetRecord> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = summarySheetRecordRepository.findAll(SummarySheetRecordSpecifications.search(searchTerm), pageRequest);
		Iterator<SummarySheetRecord> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SummarySheetRecord summarySheetRecord = (SummarySheetRecord) iterator.next();
			if(populateTransientFields)
				summarySheetRecord = populateTransientFields(summarySheetRecord, loggedInUser);
			if(fetchEagerly)
				summarySheetRecord = fetchEagerly(summarySheetRecord);
			}
		}
		return page;
	}
	
	public SummarySheetRecord getSummarySheetRecord(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheetRecord id");
		}
		SummarySheetRecord summarySheetRecord = getSummarySheetRecord(id, false);
		if(doPostValidation && null == summarySheetRecord) {
			throw new ProvilacException("SummarySheetRecord with this Id does not exists");
		}
		if(populateTransientFields)
			summarySheetRecord= populateTransientFields(summarySheetRecord, loggedInUser);
		if(fetchEagerly)
			summarySheetRecord= fetchEagerly(summarySheetRecord);
		
		return summarySheetRecord;
	}
	
	public SummarySheetRecord getSummarySheetRecordByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == code) {
			throw new ProvilacException("Invalid SummarySheetRecord code");
		}
		SummarySheetRecord summarySheetRecord = summarySheetRecordRepository.findByCode(code);
		if(doPostValidation && null == summarySheetRecord) {
			throw new ProvilacException("SummarySheetRecord with this Id does not exists");
		}
		if(populateTransientFields)
			summarySheetRecord= populateTransientFields(summarySheetRecord, loggedInUser);
		if(fetchEagerly)
			summarySheetRecord= fetchEagerly(summarySheetRecord);
		
		return summarySheetRecord;
	}

	/**
	 * returns @SummarySheetRecord without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public SummarySheetRecord getSummarySheetRecord(Long id, boolean doPostValidation) {
		
		SummarySheetRecord summarySheetRecord;
		if(null == id) {
			throw new ProvilacException("Invalid summarySheetRecord id");
		}
		summarySheetRecord = summarySheetRecordRepository.findOne(id);
		if(doPostValidation && null == summarySheetRecord) {
			throw new ProvilacException("summarySheetRecord with this Id does not exists");
		}
		return summarySheetRecord;
	}
	
	/**
	 * 
	 * Updates the SummarySheetRecord. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing SummarySheetRecord
	 * @param SummarySheetRecord - Updated SummarySheetRecord
	 * @param loggedInSummarySheetRecord - LoggedIn SummarySheetRecord
	 * @return Updated SummarySheetRecord
	 * 
	 * @author Vishal
	 */
	public SummarySheetRecord updateSummarySheetRecord(Long id, SummarySheetRecord summarySheetRecord, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheetRecord id");
		}
		SummarySheetRecord existingSummarySheetRecord = summarySheetRecordRepository.findOne(id);
		if(null == existingSummarySheetRecord) {
			throw new ProvilacException("SummarySheetRecord with this Id does not exists");
		}
		
		existingSummarySheetRecord.setAdjustment(summarySheetRecord.getAdjustment());
		existingSummarySheetRecord.setChequeDate(summarySheetRecord.getChequeDate());
		existingSummarySheetRecord.setChequeNumber(summarySheetRecord.getChequeNumber());
		existingSummarySheetRecord.setPaymentMethod(summarySheetRecord.getPaymentMethod());
		existingSummarySheetRecord.setReceiptNumber(summarySheetRecord.getReceiptNumber());
		existingSummarySheetRecord.setReceivedBy(summarySheetRecord.getReceivedBy());
		existingSummarySheetRecord.setReceivedDate(summarySheetRecord.getReceivedDate());
		
		existingSummarySheetRecord = summarySheetRecordRepository.save(existingSummarySheetRecord);
		activityLogService.createActivityLog(loggedInUser, "upadte", "SummarySheetRecord", id);
		existingSummarySheetRecord.setCipher(EncryptionUtil.encode(summarySheetRecord.getId()+""));
		if(populateTransientFields)
			existingSummarySheetRecord =populateTransientFields(existingSummarySheetRecord, loggedInUser);
		if(fetchEagerly)
			existingSummarySheetRecord=fetchEagerly(existingSummarySheetRecord);
		return existingSummarySheetRecord;
	}
	
	public void deleteSummarySheetRecord(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheetRecord id");
		}
		SummarySheetRecord summarySheetRecord = summarySheetRecordRepository.findOne(id);
		if(null == summarySheetRecord) {
			throw new ProvilacException("SummarySheetRecord with this Id does not exists");
		}
		summarySheetRecordRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "SummarySheetRecord", id);
	}
	
	private SummarySheetRecord populateTransientFields(SummarySheetRecord summarySheetRecord, User loggedInUser) {
		
		if(null == summarySheetRecord) {
			return summarySheetRecord;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid SummarySheetRecord");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			summarySheetRecord.setIsEditAllowed(true);
			summarySheetRecord.setIsDeleteAllowed(true);
		} else {
			summarySheetRecord.setIsEditAllowed(false);
			summarySheetRecord.setIsDeleteAllowed(false);
		}
		summarySheetRecord.setCipher(EncryptionUtil.encode(summarySheetRecord.getId()+""));
		return summarySheetRecord;
	}
	
	private SummarySheetRecord fetchEagerly(SummarySheetRecord summarySheetRecord) {
		
		return summarySheetRecord;
	}
	
}

