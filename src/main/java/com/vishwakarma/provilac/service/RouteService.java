package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.RouteSpecifications;
import com.vishwakarma.provilac.repository.RouteRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class RouteService {

	@Resource
	private RouteRepository routeRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private RouteRecordService routeRecordService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService; 
	
	@Transactional
	public Route createRoute(Route route, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == route) {
			throw new ProvilacException("Invalid route");
		}
		route = routeRepository.save(route);
		activityLogService.createActivityLog(loggedInUser, "add", "Route", route.getId());
		
		if (populateTransientFields) {
			route = populateTransientFields(route, loggedInUser);
		}
		
		if(fetchEagerly) {
			route= fetchEagerly(route);
		}
		return route;
	}
	
	@Transactional
	public List<Route> getAllRoutes(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Route> routes = routeRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Route route : routes) {
			if(populateTransientFields)
				route = populateTransientFields(route, loggedInUser);
			if (fetchEagerly)
				route = fetchEagerly(route);
			}
		}
		return routes;
	}
	
	@Transactional
	public Page<Route> getRoutes(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Route> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = routeRepository.findAll(pageRequest);
		Iterator<Route> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Route route = (Route) iterator.next();
			if(populateTransientFields)
				route = populateTransientFields(route, loggedInUser);
			if(fetchEagerly)
				route = fetchEagerly(route);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Route> searchRoute(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Route> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = routeRepository.findAll(RouteSpecifications.search(searchTerm), pageRequest);
		Iterator<Route> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Route route = (Route) iterator.next();
			if(populateTransientFields)
				route = populateTransientFields(route, loggedInUser);
			if(fetchEagerly)
				route = fetchEagerly(route);
			}
		}
		return page;
	}
	
	public Route getRoute(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Route id");
		}
		Route route = getRoute(id, false);
		if(doPostValidation && null == route) {
			throw new ProvilacException("Route with this Id does not exists");
		}
		if(populateTransientFields)
			route= populateTransientFields(route, loggedInUser);
		if(fetchEagerly)
			route= fetchEagerly(route);
		
		return route;
	}
	
	public Route getRouteByName(String name, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == name) {
			return null;
		}
		Route route = routeRepository.findByName(name);
		if(doPostValidation && null == route) {
			throw new ProvilacException("Route with this Id does not exists");
		}
		if(populateTransientFields)
			route= populateTransientFields(route, loggedInUser);
		if(fetchEagerly)
			route= fetchEagerly(route);

		return route;
	}
	
	public Route getRouteByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Route Code");
		}
		Route route = routeRepository.findByCode(code);
		if(doPostValidation && null == route) {
			throw new ProvilacException("Route with this Code does not exists");
		}
		if(populateTransientFields)
			route= populateTransientFields(route, loggedInUser);
		if(fetchEagerly)
			route= fetchEagerly(route);

		return route;
	}



	/**
	 * returns @Route without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Route getRoute(Long id, boolean doPostValidation) {
		
		Route route;
		if(null == id) {
			throw new ProvilacException("Invalid route id");
		}
		route = routeRepository.findOne(id);
		if(doPostValidation && null == route) {
			throw new ProvilacException("route with this Id does not exists");
		}
		return route;
	}
	
	/**
	 * 
	 * Updates the Route. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Route
	 * @param Route - Updated Route
	 * @param loggedInRoute - LoggedIn Route
	 * @return Updated Route
	 * 
	 * @author Vishal
	 */
	public Route updateRoute(Long id, Route route, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Route id");
		}
		Route existingRoute = routeRepository.findOne(id);
		if(null == existingRoute) {
			throw new ProvilacException("Route with this Id does not exists");
		}
		existingRoute.setName(route.getName());
		/*for(RouteRecord record:route.getRouteRecords()){
			existingRoute.getRouteRecords().add(record);
			PlivoSMSHelper.sendSMS(record.getCustomer().getMobileNumber(), "Hi, Your has been updated to new Route "+route.getName());
		}*/
		existingRoute = routeRepository.save(existingRoute);
		activityLogService.createActivityLog(loggedInUser, "update", "Route", id);
		existingRoute.setCipher(EncryptionUtil.encode(route.getId()+""));
		if(populateTransientFields)
			existingRoute =populateTransientFields(existingRoute, loggedInUser);
		if(fetchEagerly)
			existingRoute=fetchEagerly(existingRoute);
		return existingRoute;
	}
	
	public void deleteRoute(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Route id");
		}
		Route route = routeRepository.findOne(id);
		if(null == route) {
			throw new ProvilacException("Route with this Id does not exists");
		}
		routeRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Route", id);
		
	}
	
	private Route populateTransientFields(Route route, User loggedInUser) {
		
		if(null == route) {
			return route;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Route");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			route.setIsEditAllowed(true);
			route.setIsDeleteAllowed(true);
		} else {
			route.setIsEditAllowed(false);
			route.setIsDeleteAllowed(false);
		}
		route.setCipher(EncryptionUtil.encode(route.getId()+""));
		return route;
	}
	
	private Route fetchEagerly(Route route) {
		if(null!=route)
			route.getRouteRecords().size();
		return route;
	}
	
}

