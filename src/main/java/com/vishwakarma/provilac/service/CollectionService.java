package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Collection;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.CollectionSpecifications;
import com.vishwakarma.provilac.repository.CollectionRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;


@Service
public class CollectionService {

	@Resource
	private CollectionRepository collectionRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	public Collection createCollection(Collection collection, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == collection) {
			throw new ProvilacException("Invalid collection");
		}
		
		collection = collectionRepository.save(collection);
		activityLogService.createActivityLog(loggedInUser, "add", "collection", collection.getId());
		if (populateTransientFields) {
			collection = populateTransientFields(collection, loggedInUser);
		}
		
		if(fetchEagerly) {
			collection= fetchEagerly(collection);
		}
		return collection;
	}
	
	@Transactional
	public List<Collection> getAllCollections(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Collection> collections = collectionRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Collection collection : collections) {
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if (fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return collections;
	}
/**
 * 	
 * @param userCode
 * @param loggedInUser
 * @param populateTransientFields
 * @param fetchEagerly
 * @return List of Collections for particular customer
 */
	@Transactional
	public List<Collection> getAllCollectionsByCollectionBoy(String userCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Collection> collections = collectionRepository.findByCollectionBoy_Code(userCode);
		if(populateTransientFields || fetchEagerly){
		for (Collection collection : collections) {
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if (fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return collections;
	}
	@Transactional
	public Page<Collection> getAllCollectionsByCollectionBoy(Integer pageNumber, String userCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		Page<Collection> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		
		page = collectionRepository.findByCollectionBoy_Code(userCode,pageRequest);
		Iterator<Collection> iterator = page.iterator();
		if(populateTransientFields || fetchEagerly){
		while (iterator.hasNext()) {
			Collection collection = (Collection) iterator.next();
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if (fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return Page of Collections
	 */
	@Transactional
	public Page<Collection> getCollections(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Collection> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = collectionRepository.findAll(pageRequest);
		Iterator<Collection> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Collection collection = (Collection) iterator.next();
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if(fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return page;
	}
	

	@Transactional
	public Page<Collection> getCollectionsByCustomerCode(String customerCode,Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Collection> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = collectionRepository.findByCollectionBoy_Code(customerCode, pageRequest);
		Iterator<Collection> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Collection collection = (Collection) iterator.next();
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if(fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return page;
	}
	
	/**
	 * Get Collections by CustomerId and date Range
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return
	 * 
	 * @author Harshal
	 */
	@Transactional
	public List<Collection> getCollectionsByCustomerIdAndDateRange(Long customerId,Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Collection> collections = collectionRepository.findAll(CollectionSpecifications.searchByCollectionBoyAndDateRange(customerId, fromDate, toDate));
		if(populateTransientFields || fetchEagerly){
			for (Collection collection : collections) {
				if(populateTransientFields){
					collection = populateTransientFields(collection, loggedInUser);
				}
				if (fetchEagerly){
					collection = fetchEagerly(collection);
				}
			}
		}
		return collections;
	}
	
	@Transactional
	public Page<Collection> searchCollection(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Collection> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = collectionRepository.findAll(CollectionSpecifications.search(searchTerm), pageRequest);
		Iterator<Collection> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Collection collection = (Collection) iterator.next();
			if(populateTransientFields)
				collection = populateTransientFields(collection, loggedInUser);
			if(fetchEagerly)
				collection = fetchEagerly(collection);
			}
		}
		return page;
	}
	
	@Transactional
	public Collection getCollection(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Collection id");
		}
		Collection collection = getCollection(id, false);
		if(doPostValidation && null == collection) {
			throw new ProvilacException("Collection with this Id does not exists");
		}
		if(populateTransientFields)
			collection= populateTransientFields(collection, loggedInUser);
		if(fetchEagerly)
			collection= fetchEagerly(collection);
		
		return collection;
	}
	
	public Collection getCollectionByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid Collection code");
		}
		Collection collection = collectionRepository.findByCode(code);
		if(doPostValidation && null == collection) {
			throw new ProvilacException("Collection with this Code does not exists");
		}
		if(populateTransientFields)
			collection= populateTransientFields(collection, loggedInUser);
		if(fetchEagerly)
			collection= fetchEagerly(collection);

		return collection;
	}

	/**
	 * returns @Collection without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Collection getCollection(Long id, boolean doPostValidation) {
		
		Collection collection;
		if(null == id) {
			throw new ProvilacException("Invalid collection id");
		}
		collection = collectionRepository.findOne(id);
		if(doPostValidation && null == collection) {
			throw new ProvilacException("collection with this Id does not exists");
		}
		return collection;
	}
	
	/**
	 * 
	 * Updates the Collection. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Collection
	 * @param Collection - Updated Collection
	 * @param loggedInCollection - LoggedIn Collection
	 * @return Updated Collection
	 * 
	 * @author Vishal
	 */
	@Transactional
	public Collection updateCollection(Long id, Collection collection, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Collection id");
		}
		Collection existingCollection = collectionRepository.findOne(id);
		if(null == existingCollection) {
			throw new ProvilacException("Collection with this Id does not exists");
		}

		existingCollection.setCashAmount(collection.getCashAmount());
		existingCollection.setTotalAmount(collection.getTotalAmount());
		existingCollection.setChequeAmount(collection.getChequeAmount());
		existingCollection.setNoOfCheques(collection.getNoOfCheques());
		existingCollection.setPayments(collection.getPayments());
		existingCollection = collectionRepository.save(existingCollection);
		activityLogService.createActivityLog(loggedInUser, "update", "Collection",id);
		
		existingCollection.setCipher(EncryptionUtil.encode(collection.getId() + ""));
		if (populateTransientFields){
			existingCollection = populateTransientFields(existingCollection, loggedInUser);
		}

		if (fetchEagerly){
			existingCollection = fetchEagerly(existingCollection);
		}
		
		return existingCollection;
	}
	
	public void deleteCollection(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Collection id");
		}
		Collection collection = collectionRepository.findOne(id);
		if(null == collection) {
			throw new ProvilacException("Collection with this Id does not exists");
		}
		collectionRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Collection",id);
	}
	
	private Collection populateTransientFields(Collection collection, User loggedInUser) {
		
		if(null == collection) {
			return collection;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Collection");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			collection.setIsEditAllowed(true);
			collection.setIsDeleteAllowed(true);
		} else {
			collection.setIsEditAllowed(false);
			collection.setIsDeleteAllowed(false);
		}
		collection.setCipher(EncryptionUtil.encode(collection.getId()+""));
		return collection;
	}
	
	private Collection fetchEagerly(Collection collection) {
		collection.getPayments().size();
		return collection;
	}
	
}

