package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.AddressSpecifications;
import com.vishwakarma.provilac.repository.AddressRepository;
import com.vishwakarma.provilac.repository.LocalityRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;


/**
 * 
 * @author Rohit
 *
 */

@Service
public class AddressService {

	@Resource
	private AddressRepository addressRepository;
	
	@Resource
	private LocalityRepository localityRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public Address createAddress(Address address, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == address) {
			throw new ProvilacException("Invalid address");
		}
		address = addressRepository.save(address);
		//activityLogService.createActivityLog(userService.getLoggedInUser(), "add", "Address", address.getId());
		if (populateTransientFields) {
			address = populateTransientFields(address, loggedInUser);
		}
		
		if(fetchEagerly) {
			address= fetchEagerly(address);
		}
		return address;
	}
	
	/**
	 * 
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return all addresses
	 */
	@Transactional
	public List<Address> getAllAddresses(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Address> addresses = addressRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Address address : addresses) {
			if(populateTransientFields)
				address = populateTransientFields(address, loggedInUser);
			if (fetchEagerly)
				address = fetchEagerly(address);
			}
		}
		return addresses;
	}
	
	
	@Transactional
	public List<Address> getDefaultAddressesByCity(Long cityId,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Address> addresses = addressRepository.findByIsDefaultAndCity_Id(true, cityId);
		if(populateTransientFields || fetchEagerly){
		for (Address address : addresses) {
			if(populateTransientFields)
				address = populateTransientFields(address, loggedInUser);
			if (fetchEagerly)
				address = fetchEagerly(address);
			}
		}
		return addresses;
	}
	
	
	/**
	 * 
	 * @param pageNumber
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return page of addresses with specified pageNumber
	 */
	@Transactional
	public Page<Address> getAddresses(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Address> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = addressRepository.findAll(pageRequest);
		Iterator<Address> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Address address = (Address) iterator.next();
			if(populateTransientFields)
				address = populateTransientFields(address, loggedInUser);
			if(fetchEagerly)
				address = fetchEagerly(address);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param pageNumber
	 * @param searchTerm
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return address or list of addresses according to search term 
	 */
	@Transactional
	public Page<Address> searchAddress(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Address> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = addressRepository.findAll(AddressSpecifications.search(searchTerm), pageRequest);
		Iterator<Address> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Address address = (Address) iterator.next();
				if(populateTransientFields)
					address = populateTransientFields(address, loggedInUser);
				if(fetchEagerly)
					address = fetchEagerly(address);
			}
		}
		return page;
	}
	
	/**
	 * 
	 * @param id
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return address
	 */
	@Transactional
	public Address getAddress(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Address id");
		}
		Address address = addressRepository.findOne(id);
		if(doPostValidation && null == address) {
			throw new ProvilacException("Address with this Id does not exists");
		}
		if(null == address){
			return null;
		}
		if(populateTransientFields)
			address= populateTransientFields(address, loggedInUser);
		if(fetchEagerly)
			address= fetchEagerly(address);
		
		return address;
	}
	
	/**
	 * 
	 * @param code
	 * @param doPostValidation
	 * @param loggedInUser
	 * @param populateTransientFields
	 * @param fetchEagerly
	 * @return address with specified code
	 */
	@Transactional
	public Address getAddressByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(StringUtils.isBlank(code)) {
			throw new ProvilacException("Invalid Address code");
		}
		Address address = addressRepository.findByCode(code);
		if(doPostValidation && null == address) {
			throw new ProvilacException("Address with this Code does not exists");
		}
		if(null == address){
			return null;
		}
		if(populateTransientFields)
			address= populateTransientFields(address, loggedInUser);
		if(fetchEagerly)
			address= fetchEagerly(address);

		return address;
	}
	
	
	public Address getDefaultAddressByCustomer(String customerCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(StringUtils.isBlank(customerCode)) {
			throw new ProvilacException("Invalid Customer code");
		}
		List<Address> addresses = addressRepository.findByCustomer_CodeAndIsDefault(customerCode, true);
		if(doPostValidation && addresses.isEmpty()) {
			throw new ProvilacException("Address for this customer does not exists");
		}
		if(addresses.isEmpty()){
			return null;
		}
		Address address = addresses.get(0);
		if(populateTransientFields)
			address= populateTransientFields(address, loggedInUser);
		if(fetchEagerly)
			address= fetchEagerly(address);

		return address;
	}
	
	/**
	 * 
	 * 
	 * @param id - Id of existing Address
	 * @param Address - Updated Address
	 * @param loggedInAddress - LoggedIn Address
	 * @return Updated Address
	 * 
	 */
	@Transactional
	public Address updateAddress(Long id, Address address, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Address id");
		}
		Address existingAddress = addressRepository.findOne(id);
		if(null == existingAddress) {
			throw new ProvilacException("Address with this Id does not exists");
		}
		 existingAddress.setName(address.getName());
		 existingAddress.setAddressLine1(address.getAddressLine1());
		 existingAddress.setAddressLine2(address.getAddressLine2());
		 existingAddress.setLandmark(address.getLandmark());
		 existingAddress.setLat(address.getLat());
		 existingAddress.setLng(address.getLng());
		 existingAddress.setIsDefault(address.getIsDefault());
		 existingAddress.setCustomer(address.getCustomer());
		 existingAddress.setCity(address.getCity());
		 existingAddress.setLocality(address.getLocality());
		 existingAddress.setPincode(address.getPincode());
		 
			
		
		existingAddress = addressRepository.save(existingAddress);
		activityLogService.createActivityLog(loggedInUser, "update", "Address",id);
		existingAddress.setCipher(EncryptionUtil.encode(address.getId()+""));
		if(populateTransientFields)
			existingAddress =populateTransientFields(existingAddress, loggedInUser);
		if(fetchEagerly)
			existingAddress=fetchEagerly(existingAddress);
		return existingAddress;
	}
	
	public void deleteAddress(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Address id");
		}
		Address address = addressRepository.findOne(id);
		if(null == address) {
			throw new ProvilacException("Address with this Id does not exists");
		}
		addressRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Address", id);
	}
	
	@Transactional
	public void deleteAllAddressesForCustomer(Long customerId) {
		addressRepository.deleteAllAddressesForCustomer(customerId);
	}
	
	private Address populateTransientFields(Address address, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Address");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			address.setIsEditAllowed(true);
			address.setIsDeleteAllowed(true);
		} else {
			address.setIsEditAllowed(false);
			address.setIsDeleteAllowed(false);
		}
		address.setCipher(EncryptionUtil.encode(address.getId()+""));
		return address;
	}
	
	private Address fetchEagerly(Address address) {
		
		return address;
	}

	@Transactional
	public List<Address> getAddressesByCustomer(String customerCode,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		List<Address> addresses = addressRepository.findByCustomer_Code(customerCode);
		
		if(fetchEagerly||populateTransientFields){
			for (Address address : addresses) {
				if(populateTransientFields)
					address = populateTransientFields(address, loggedInUser);
				if(fetchEagerly)
					address = fetchEagerly(address);
			}
		}
		
		return addresses;
	}	
	
}

