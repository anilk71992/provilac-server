package com.vishwakarma.provilac.service;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vishwakarma.provilac.messaging.EmailHelper;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.utils.NotificationHelper;

@Service
public class AccessService {

	@Autowired
	private NotificationHelper notificationHelper;
	
	@Resource
	private UserService userService;
	
	@Resource
	private EmailHelper emailHelper;

	private final static Logger log = LoggerFactory.getLogger(AccessService.class);

	@Deprecated
	public boolean recoverUserPassword(String email) {
		String password;
		User user = userService.getUserByEmail(email, true, null, false, false);
		password = userService.recoverPassword(user.getId());
		String messageBody = notificationHelper.getEmailBodyForPasswordReset(user, password);
		try {
//			emailHelper.sendSimpleEmail(user.getEmail(), "Password Reset Request", messageBody);
			emailHelper.sendMailForPasswordRecovery(email,"Password Reset Request", messageBody);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
