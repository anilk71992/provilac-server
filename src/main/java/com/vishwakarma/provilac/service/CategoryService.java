package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.CategorySpecifications;
import com.vishwakarma.provilac.repository.CategoryRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class CategoryService {

	@Resource
	private CategoryRepository categoryRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	@Transactional
	public Category createCategory(Category category, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == category) {
			throw new ProvilacException("Invalid category");
		}
		category = categoryRepository.save(category);
		activityLogService.createActivityLog(loggedInUser, "add", "Category", category.getId());
		
		if(null != category.getCategoryData()){
			appFileUtils.saveData(AppConstants.CATEGORY_PICS_FOLDER,category.getId()+"", category.getCategoryData());
		}
		
		if (populateTransientFields) {
			category = populateTransientFields(category, loggedInUser);
		}
		
		if(fetchEagerly) {
			category= fetchEagerly(category);
		}
		return category;
	}
	
	@Transactional
	public List<Category> getAllCategorys(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<Category> categorys = categoryRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (Category category : categorys) {
			if(populateTransientFields)
				category = populateTransientFields(category, loggedInUser);
			if (fetchEagerly)
				category = fetchEagerly(category);
			}
		}
		return categorys;
	}
	
	@Transactional
	public Page<Category> getCategorys(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Category> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = categoryRepository.findAll(pageRequest);
		Iterator<Category> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Category category = (Category) iterator.next();
			if(populateTransientFields)
				category = populateTransientFields(category, loggedInUser);
			if(fetchEagerly)
				category = fetchEagerly(category);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Category> searchCategory(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Category> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = categoryRepository.findAll(CategorySpecifications.search(searchTerm), pageRequest);
		Iterator<Category> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			Category category = (Category) iterator.next();
			if(populateTransientFields)
				category = populateTransientFields(category, loggedInUser);
			if(fetchEagerly)
				category = fetchEagerly(category);
			}
		}
		return page;
	}
	
	public Category getCategory(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Category id");
		}
		Category category = getCategory(id, false);
		if(doPostValidation && null == category) {
			throw new ProvilacException("Category with this Id does not exists");
		}
		if(populateTransientFields)
			category= populateTransientFields(category, loggedInUser);
		if(fetchEagerly)
			category= fetchEagerly(category);
		
		return category;
	}
	
	public Category getCategoryByName(String categoryName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == categoryName) {
			throw new ProvilacException("Invalid Category Name");
		}
		Category category = categoryRepository.findByName(categoryName);
		if(doPostValidation && null == category) {
			throw new ProvilacException("Category with this name does not exists");
		}
		if(populateTransientFields)
			category= populateTransientFields(category, loggedInUser);
		if(fetchEagerly)
			category= fetchEagerly(category);
		
		return category;
	}
	
public Category getCategoryByCode(String categoryCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == categoryCode) {
			throw new ProvilacException("Invalid Category code");
		}
		Category category = categoryRepository.findByCode(categoryCode);
		if(doPostValidation && null == category) {
			throw new ProvilacException("Category with this code does not exists");
		}
		if(populateTransientFields)
			category= populateTransientFields(category, loggedInUser);
		if(fetchEagerly)
			category= fetchEagerly(category);
		
		return category;
	}
	
	/**
	 * returns @Category without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public Category getCategory(Long id, boolean doPostValidation) {
		
		Category category;
		if(null == id) {
			throw new ProvilacException("Invalid category id");
		}
		category = categoryRepository.findOne(id);
		if(doPostValidation && null == category) {
			throw new ProvilacException("category with this Id does not exists");
		}
		return category;
	}
	
	/**
	 * 
	 * Updates the Category. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing Category
	 * @param Category - Updated Category
	 * @param loggedInCategory - LoggedIn Category
	 * @return Updated Category
	 * 
	 * @author Vishal
	 */
	public Category updateCategory(Long id, Category category, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Category id");
		}
		Category existingCategory = categoryRepository.findOne(id);
		if(null == existingCategory) {
			throw new ProvilacException("Category with this Id does not exists");
		}
		existingCategory.setName(category.getName());
		existingCategory.setDescription(category.getDescription());
		
		//TODO update categoryPic.
		if(null != category.getCategoryData()){
			appFileUtils.saveData(AppConstants.CATEGORY_PICS_FOLDER,category.getId()+"", category.getCategoryData());
		}
		
		existingCategory = categoryRepository.save(existingCategory);
		activityLogService.createActivityLog(loggedInUser, "update", "Category",id);
		existingCategory.setCipher(EncryptionUtil.encode(category.getId()+""));
		if(populateTransientFields)
			existingCategory =populateTransientFields(existingCategory, loggedInUser);
		if(fetchEagerly)
			existingCategory=fetchEagerly(existingCategory);
		return existingCategory;
	}
	
	public void deleteCategory(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Category id");
		}
		Category category = categoryRepository.findOne(id);
		if(null == category) {
			throw new ProvilacException("Category with this Id does not exists");
		}
		categoryRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Category",id);
	}
	
	private Category populateTransientFields(Category category, User loggedInUser) {
		
		if(null == category) {
			return category;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid Category");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			category.setIsEditAllowed(true);
			category.setIsDeleteAllowed(true);
		} else {
			category.setIsEditAllowed(false);
			category.setIsDeleteAllowed(false);
		}
		category.setCipher(EncryptionUtil.encode(category.getId()+""));
		return category;
	}
	
	private Category fetchEagerly(Category category) {
		
		return category;
	}
	
}

