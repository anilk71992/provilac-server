package com.vishwakarma.provilac.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.SubscriptionLineItemSpecifications;
import com.vishwakarma.provilac.repository.SubscriptionLineItemRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class SubscriptionLineItemService {

	@Resource
	private  SubscriptionLineItemRepository  subscriptionLineItemRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService; 
	
	@Transactional
	public  SubscriptionLineItem createSubscriptionLineItem( SubscriptionLineItem  subscriptionLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null ==  subscriptionLineItem) {
			throw new ProvilacException("Invalid  subscriptionLineItem");
		}
		 subscriptionLineItem =  subscriptionLineItemRepository.save( subscriptionLineItem);
		 activityLogService.createActivityLog(loggedInUser, "add", "subscriptionLineItem", subscriptionLineItem.getId());
		
		if (populateTransientFields) {
			 subscriptionLineItem = populateTransientFields( subscriptionLineItem, loggedInUser);
		}
		
		if(fetchEagerly) {
			 subscriptionLineItem= fetchEagerly( subscriptionLineItem);
		}
		return  subscriptionLineItem;
	}
	
	@Transactional
	public Collection<SubscriptionLineItem> createSubscriptionLineItems(Collection<SubscriptionLineItem> subscriptionLineItems,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null == subscriptionLineItems){
			throw new ProvilacException("Invalid subscriptionLineItem");
		}
		subscriptionLineItems = subscriptionLineItemRepository.save(subscriptionLineItems);
		for(SubscriptionLineItem subscriptionsLineItem:subscriptionLineItems){
			activityLogService.createActivityLog(loggedInUser, "add", "subscriptionLineItem",subscriptionsLineItem.getId());
		}
		if(populateTransientFields||fetchEagerly){
			for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
				if(populateTransientFields)
					subscriptionLineItem = populateTransientFields(subscriptionLineItem, loggedInUser);
				if(fetchEagerly)
					subscriptionLineItem = fetchEagerly(subscriptionLineItem);
			}
		}
		return subscriptionLineItems;
	}
	
	@Transactional
	public List< SubscriptionLineItem> getAllSubscriptionLineItems(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List< SubscriptionLineItem>  subscriptionLineItems =  subscriptionLineItemRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for ( SubscriptionLineItem  subscriptionLineItem :  subscriptionLineItems) {
			if(populateTransientFields)
				 subscriptionLineItem = populateTransientFields( subscriptionLineItem, loggedInUser);
			if (fetchEagerly)
				 subscriptionLineItem = fetchEagerly( subscriptionLineItem);
			}
		}
		return  subscriptionLineItems;
	}
	
	@Transactional
	public Page< SubscriptionLineItem> getSubscriptionLineItems(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page< SubscriptionLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  subscriptionLineItemRepository.findAll(pageRequest);
		Iterator< SubscriptionLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 SubscriptionLineItem  subscriptionLineItem = ( SubscriptionLineItem) iterator.next();
			if(populateTransientFields)
				 subscriptionLineItem = populateTransientFields( subscriptionLineItem, loggedInUser);
			if(fetchEagerly)
				 subscriptionLineItem = fetchEagerly( subscriptionLineItem);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page< SubscriptionLineItem> searchSubscriptionLineItem(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page< SubscriptionLineItem> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page =  subscriptionLineItemRepository.findAll( SubscriptionLineItemSpecifications.search(searchTerm), pageRequest);
		Iterator< SubscriptionLineItem> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			 SubscriptionLineItem  subscriptionLineItem = ( SubscriptionLineItem) iterator.next();
			if(populateTransientFields)
				 subscriptionLineItem = populateTransientFields( subscriptionLineItem, loggedInUser);
			if(fetchEagerly)
				 subscriptionLineItem = fetchEagerly( subscriptionLineItem);
			}
		}
		return page;
	}
	
	public  SubscriptionLineItem getSubscriptionLineItem(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  SubscriptionLineItem id");
		}
		 SubscriptionLineItem  subscriptionLineItem = getSubscriptionLineItem(id, false);
		if(doPostValidation && null ==  subscriptionLineItem) {
			throw new ProvilacException(" SubscriptionLineItem with this Id does not exists");
		}
		if(populateTransientFields)
			 subscriptionLineItem= populateTransientFields( subscriptionLineItem, loggedInUser);
		if(fetchEagerly)
			 subscriptionLineItem= fetchEagerly( subscriptionLineItem);
		
		return  subscriptionLineItem;
	}
	

	/**
	 * returns @ SubscriptionLineItem without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public  SubscriptionLineItem getSubscriptionLineItem(Long id, boolean doPostValidation) {
		
		 SubscriptionLineItem  subscriptionLineItem;
		if(null == id) {
			throw new ProvilacException("Invalid  subscriptionLineItem id");
		}
		 subscriptionLineItem =  subscriptionLineItemRepository.findOne(id);
		if(doPostValidation && null ==  subscriptionLineItem) {
			throw new ProvilacException(" subscriptionLineItem with this Id does not exists");
		}
		return  subscriptionLineItem;
	}
	
	/**
	 * 
	 * Updates the  SubscriptionLineItem. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing  SubscriptionLineItem
	 * @param  SubscriptionLineItem - Updated  SubscriptionLineItem
	 * @param loggedIn SubscriptionLineItem - LoggedIn  SubscriptionLineItem
	 * @return Updated  SubscriptionLineItem
	 * 
	 * @author Vishal
	 */
	public  SubscriptionLineItem updateSubscriptionLineItem(Long id,  SubscriptionLineItem  subscriptionLineItem, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  SubscriptionLineItem id");
		}
		 SubscriptionLineItem existingSubscriptionLineItem =  subscriptionLineItemRepository.findOne(id);
		if(null == existingSubscriptionLineItem) {
			throw new ProvilacException(" SubscriptionLineItem with this Id does not exists");
		}
		existingSubscriptionLineItem.setDay(subscriptionLineItem.getDay());
		existingSubscriptionLineItem.setProduct(subscriptionLineItem.getProduct());
		existingSubscriptionLineItem.setQuantity(subscriptionLineItem.getQuantity());
		existingSubscriptionLineItem.setCurrentIndex(subscriptionLineItem.getCurrentIndex());
		existingSubscriptionLineItem = subscriptionLineItemRepository.save(existingSubscriptionLineItem);
		activityLogService.createActivityLog(loggedInUser, "update", "subscriptionLineItem", id);
		existingSubscriptionLineItem.setCipher(EncryptionUtil.encode( subscriptionLineItem.getId()+""));
		if(populateTransientFields)
			existingSubscriptionLineItem =populateTransientFields(existingSubscriptionLineItem, loggedInUser);
		if(fetchEagerly)
			existingSubscriptionLineItem=fetchEagerly(existingSubscriptionLineItem);
		return existingSubscriptionLineItem;
	}
	
	@Transactional
	public Collection<SubscriptionLineItem> updateSubscriptionLineItems(Collection<SubscriptionLineItem> subscriptionLineItems,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		if(null == subscriptionLineItems){
			throw new ProvilacException("Invalid subscriptionLineItem");
		}
		subscriptionLineItems = subscriptionLineItemRepository.save(subscriptionLineItems);
		for(SubscriptionLineItem subscriptionsLineItem:subscriptionLineItems){
			activityLogService.createActivityLog(loggedInUser, "add", "subscriptionLineItem",subscriptionsLineItem.getId());
		}
		if(populateTransientFields||fetchEagerly){
			for (SubscriptionLineItem subscriptionLineItem : subscriptionLineItems) {
				if(populateTransientFields)
					subscriptionLineItem = populateTransientFields(subscriptionLineItem, loggedInUser);
				if(fetchEagerly)
					subscriptionLineItem = fetchEagerly(subscriptionLineItem);
			}
		}
		return subscriptionLineItems;
	}
	public void deleteSubscriptionLineItem(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid  SubscriptionLineItem id");
		}
		 SubscriptionLineItem  subscriptionLineItem =  subscriptionLineItemRepository.findOne(id);
		if(null ==  subscriptionLineItem) {
			throw new ProvilacException(" SubscriptionLineItem with this Id does not exists");
		}
		 subscriptionLineItemRepository.delete(id);
		 activityLogService.createActivityLog(null, "delete", "SubscriptionLineItem", id);
	}
	
	private  SubscriptionLineItem populateTransientFields( SubscriptionLineItem  subscriptionLineItem, User loggedInUser) {
		
		if(null ==  subscriptionLineItem) {
			return  subscriptionLineItem;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid  SubscriptionLineItem");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			 subscriptionLineItem.setIsEditAllowed(true);
			 subscriptionLineItem.setIsDeleteAllowed(true);
		} else {
			 subscriptionLineItem.setIsEditAllowed(false);
			 subscriptionLineItem.setIsDeleteAllowed(false);
		}
		 subscriptionLineItem.setCipher(EncryptionUtil.encode( subscriptionLineItem.getId()+""));
		return  subscriptionLineItem;
	}
	
	private  SubscriptionLineItem fetchEagerly( SubscriptionLineItem  subscriptionLineItem) {
		
		return  subscriptionLineItem;
	}
	
}

