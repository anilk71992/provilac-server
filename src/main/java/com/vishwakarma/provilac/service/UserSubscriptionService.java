package com.vishwakarma.provilac.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.SystemProperty;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.specifications.UserSubscriptionSpecifications;
import com.vishwakarma.provilac.repository.UserSubscriptionRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.SystemPropertyHelper;

 
@Service
public class UserSubscriptionService {

	@Resource
	private UserSubscriptionRepository userSubscriptionRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private SubscriptionLineItemService subscriptionLineItemService;
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private SystemPropertyHelper systemPropertyHelper;
	
	@Transactional
	public UserSubscription createUserSubscription(UserSubscription userSubscription, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == userSubscription) {
			throw new ProvilacException("Invalid userSubscription");
		}
		userSubscription = userSubscriptionRepository.save(userSubscription);
		activityLogService.createActivityLog(loggedInUser, "add", "UserSubscription", userSubscription.getId());
		Set<SubscriptionLineItem> lineItems= new HashSet<SubscriptionLineItem>();
		for (SubscriptionLineItem lineItem : userSubscription.getSubscriptionLineItems()) {
			lineItem.setUserSubscription(userSubscription);
			lineItems.add(lineItem);
		}
		subscriptionLineItemService.createSubscriptionLineItems(lineItems, loggedInUser, false, false);
		User customer = userService.getUser(userSubscription.getUser().getId(), true, loggedInUser, false, false);
		if(!customer.getAllocatedFirstTimeReferralPoints()){
			if(null != customer.getReferredBy()){
				User referredBy = userService.getUser(customer.getReferredBy().getId(), true, loggedInUser, false, false);
				if(null != referredBy){
					SystemProperty referrerPoints = systemPropertyHelper.getSystemProperty("REFERRER_POINTS");
					SystemProperty referredToPoints = systemPropertyHelper.getSystemProperty("REFERRED_TO_POINTS");
					customer.setAccumulatedPoints(customer.getAccumulatedPoints()+Integer.parseInt(referrerPoints.getPropValue()));
					customer.setAllocatedFirstTimeReferralPoints(true);
					userService.updateReferralPoints(customer.getId(), customer, loggedInUser, false, false);
					referredBy.setAccumulatedPoints(referredBy.getAccumulatedPoints()+Integer.parseInt(referredToPoints.getPropValue()));
					userService.updateReferralPoints(referredBy.getId(), referredBy, loggedInUser, false, false);
				}
			}
		}
		
		if (populateTransientFields) {
			userSubscription = populateTransientFields(userSubscription, loggedInUser);
		}
		
		if(fetchEagerly) {
			userSubscription= fetchEagerly(userSubscription);
		}
		return userSubscription;
	}
	
	@Transactional
	public List<UserSubscription> getAllUserSubscriptions(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
			}
		}
		return userSubscriptions;
	}
	
	@Transactional
	public List<UserSubscription> getAllCurrentSubscriptions(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findByIsCurrent(true);
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
			}
		}
		return userSubscriptions;
	}
	
	@Transactional
	public Page<UserSubscription> getUserSubscriptions(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<UserSubscription> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = userSubscriptionRepository.findAll(pageRequest);
		Iterator<UserSubscription> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			UserSubscription userSubscription = (UserSubscription) iterator.next();
			if(populateTransientFields)
				userSubscription = populateTransientFields(userSubscription, loggedInUser);
			if(fetchEagerly)
				userSubscription = fetchEagerly(userSubscription);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<UserSubscription> searchUserSubscription(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<UserSubscription> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = userSubscriptionRepository.findAll(UserSubscriptionSpecifications.search(searchTerm), pageRequest);
		Iterator<UserSubscription> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			UserSubscription userSubscription = (UserSubscription) iterator.next();
			if(populateTransientFields)
				userSubscription = populateTransientFields(userSubscription, loggedInUser);
			if(fetchEagerly)
				userSubscription = fetchEagerly(userSubscription);
			}
		}
		return page;
	}
	
	@Transactional
	public UserSubscription getUserSubscriptionByCustomerAndIsCurrent(String code, boolean isCurrent, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if (null == code) {
			throw new ProvilacException("Invalid User code");
		}
		UserSubscription userSubscription = userSubscriptionRepository.findByUser_CodeAndIsCurrent(code, isCurrent);
		if (userSubscription == null) {
			return null;
		}
		if (populateTransientFields)
			userSubscription = populateTransientFields(userSubscription, loggedInUser);
		if (fetchEagerly)
			userSubscription = fetchEagerly(userSubscription);

		return userSubscription;
	}		
	
	
	@Transactional
	public UserSubscription getUserSubscription(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserSubscription id");
		}
		UserSubscription userSubscription = userSubscriptionRepository.findOne(id);
		if(doPostValidation && null == userSubscription) {
			throw new ProvilacException("UserSubscription with this Id does not exists");
		}
		if(populateTransientFields)
			userSubscription= populateTransientFields(userSubscription, loggedInUser);
		if(fetchEagerly)
			userSubscription= fetchEagerly(userSubscription);
		
		return userSubscription;
	}
	
	public UserSubscription getUserSubscriptionByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid UserSubscription id");
		}
		UserSubscription userSubscription = userSubscriptionRepository.findByCode(code);
		if(doPostValidation && null == userSubscription) {
			throw new ProvilacException("UserSubscription with this Id does not exists");
		}
		if(populateTransientFields)
			userSubscription= populateTransientFields(userSubscription, loggedInUser);
		if(fetchEagerly)
			userSubscription= fetchEagerly(userSubscription);

		return userSubscription;
	}
	
	@Transactional
	@Deprecated
	public List<UserSubscription> getUserSubscriptionByCustomerMobileNumber(String customerMobileNumber,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findAll(UserSubscriptionSpecifications.searchByCustomermobileNumber(customerMobileNumber));
		
		if(null==userSubscriptions){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
				}
			}
		return userSubscriptions;
	}
	
	@Transactional
	public List<UserSubscription> getCurrentUserSubscriptionByStartDate(Date fromDate,Date toDate,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findAll(UserSubscriptionSpecifications.searchBystartDate(true, fromDate, toDate));
		
		if(null==userSubscriptions){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
				}
			}
		return userSubscriptions;
	}
	
	@Transactional
	public List<UserSubscription> getUserSubscriptionByIsCurrentAndAssignedToUser(String assignedToUserCode,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findAll(UserSubscriptionSpecifications.searchByIsCurrentAndAssignedToUSer(assignedToUserCode, true));
		
		if(null==userSubscriptions){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
				}
			}
		return userSubscriptions;
	}
	
	@Transactional
	public List<UserSubscription> getUserSubscriptionsByCustomerAndIsCurrent(String code, boolean isCurrent, User loggedInUser, boolean populateTransientFields, boolean fetchEagerly) {
		
		if (null == code) {
			throw new ProvilacException("Invalid User code");
		}
		List<UserSubscription> userSubscriptions = userSubscriptionRepository.findByIsCurrentAndUser_Code(isCurrent, code);
		if(null==userSubscriptions){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (UserSubscription userSubscription : userSubscriptions) {
				if(populateTransientFields)
					userSubscription = populateTransientFields(userSubscription, loggedInUser);
				if (fetchEagerly)
					userSubscription = fetchEagerly(userSubscription);
				}
			}
		return userSubscriptions;
	}
	
	@Transactional
	public boolean isUserSubscriptionExistForCustomer(Long customerId) {
		Page<UserSubscription> page = null;
		PageRequest pageRequest = new PageRequest(0, 1, Direction.ASC, "id");
		page = userSubscriptionRepository.findAll(UserSubscriptionSpecifications.searchByCustomerId(customerId),pageRequest);
		if(page.hasContent()){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 
	 * Updates the UserSubscription. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing UserSubscription
	 * @param UserSubscription - Updated UserSubscription
	 * @param loggedInUserSubscription - LoggedIn UserSubscription
	 * @return Updated UserSubscription
	 * 
	 * @author Vishal
	 */
	@Transactional
	public UserSubscription updateUserSubscription(Long id, UserSubscription userSubscription, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserSubscription id");
		}
		UserSubscription existingUserSubscription = userSubscriptionRepository.findOne(id);
		if(null == existingUserSubscription) {
			throw new ProvilacException("UserSubscription with this Id does not exists");
		}
//		existingUserSubscription.setType(userSubscription.getType());
		existingUserSubscription.setUser(userSubscription.getUser());
		existingUserSubscription.setIsCurrent(userSubscription.getIsCurrent());
		if(null!=userSubscription.getPermanantNote()){
			existingUserSubscription.setPermanantNote(userSubscription.getPermanantNote());
		}
		existingUserSubscription.setInternalNote(userSubscription.getInternalNote());
		
		existingUserSubscription = userSubscriptionRepository.save(existingUserSubscription);
		activityLogService.createActivityLog(loggedInUser, "updated", "UserSubscription", id);
		existingUserSubscription.setCipher(EncryptionUtil.encode(userSubscription.getId()+""));
		if(populateTransientFields)
			existingUserSubscription =populateTransientFields(existingUserSubscription, loggedInUser);
		if(fetchEagerly)
			existingUserSubscription=fetchEagerly(existingUserSubscription);
		return existingUserSubscription;
	}
	
	@Transactional
	public UserSubscription updateUserSubscriptionByCustomer(Long id, UserSubscription userSubscription, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserSubscription id");
		}
		UserSubscription existingUserSubscription = userSubscriptionRepository.findOne(id);
		if(null == existingUserSubscription) {
			throw new ProvilacException("UserSubscription with this Id does not exists");
		}
		
		existingUserSubscription.setEndDate(userSubscription.getEndDate());
		existingUserSubscription.setTotalAmount(userSubscription.getTotalAmount());
		existingUserSubscription.setFinalAmount(userSubscription.getFinalAmount());
		existingUserSubscription.setTxnId(userSubscription.getTxnId());
		
		existingUserSubscription = userSubscriptionRepository.save(existingUserSubscription);
		activityLogService.createActivityLog(loggedInUser, "updated", "UserSubscription", id);
		existingUserSubscription.setCipher(EncryptionUtil.encode(userSubscription.getId()+""));
		if(populateTransientFields)
			existingUserSubscription =populateTransientFields(existingUserSubscription, loggedInUser);
		if(fetchEagerly)
			existingUserSubscription=fetchEagerly(existingUserSubscription);
		return existingUserSubscription;
	}
	
	public void deleteUserSubscription(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid UserSubscription id");
		}
		UserSubscription userSubscription = userSubscriptionRepository.findOne(id);
		if(null == userSubscription) {
			throw new ProvilacException("UserSubscription with this Id does not exists");
		}
		userSubscriptionRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "UserSubscription", id);
	}
	
	private UserSubscription populateTransientFields(UserSubscription userSubscription, User loggedInUser) {
		
		if(null == userSubscription) {
			return userSubscription;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid UserSubscription");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			userSubscription.setIsEditAllowed(true);
			userSubscription.setIsDeleteAllowed(true);
		} else {
			userSubscription.setIsEditAllowed(false);
			userSubscription.setIsDeleteAllowed(false);
		}
		userSubscription.setCipher(EncryptionUtil.encode(userSubscription.getId()+""));
		return userSubscription;
	}
	
	private UserSubscription fetchEagerly(UserSubscription userSubscription) {
		userSubscription.getSubscriptionLineItems().size();
		userSubscription.getProductPricing().size();
		return userSubscription;
	}
	
}

