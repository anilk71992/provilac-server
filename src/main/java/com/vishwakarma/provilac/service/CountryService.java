package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Country;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.CountrySpecifications;
import com.vishwakarma.provilac.repository.CountryRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;


@Service
public class CountryService {

	@Resource
	private CountryRepository countryRepository;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public Country createCountry(Country country, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == country) {
			throw new ProvilacException("Invalid country");
		}
		country = countryRepository.save(country);
		activityLogService.createActivityLog(loggedInUser, "add", "Country", country.getId());
		if (populateTransientFields) {
			country = populateTransientFields(country, loggedInUser);
		}

		if(fetchEagerly) {
			country= fetchEagerly(country);
		}
		return country;
	}
	
	@Transactional
	public List<Country> getAllCountries(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {

		List<Country> countries = countryRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (Country country : countries) {
				if(populateTransientFields)
					country = populateTransientFields(country, loggedInUser);
				if (fetchEagerly)
					country = fetchEagerly(country);
			}
		}
		return countries;
	}
	
	@Transactional
	public Page<Country> getCountries(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<Country> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = countryRepository.findAll(pageRequest);
		Iterator<Country> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Country country = (Country) iterator.next();
				if(populateTransientFields)
					country = populateTransientFields(country, loggedInUser);
				if(fetchEagerly)
					country = fetchEagerly(country);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<Country> searchCountry(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<Country> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.DESC, "id");
		page = countryRepository.findAll(CountrySpecifications.search(searchTerm), pageRequest);
		Iterator<Country> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
				Country country = (Country) iterator.next();
				if(populateTransientFields)
					country = populateTransientFields(country, loggedInUser);
				if(fetchEagerly)
					country = fetchEagerly(country);
			}
		}
		return page;
	}
	
	public Country getCountry(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid country id");
		}
		Country country = countryRepository.findOne(id);
		if(doPostValidation && null == country) {
			throw new ProvilacException("Country with this id does not exists");
		}
		if(null == country){
			return null;
		}
		if(populateTransientFields)
			country= populateTransientFields(country, loggedInUser);
		if(fetchEagerly)
			country= fetchEagerly(country);
		
		return country;
	}
	
	public Country getCountryByName(String countryName, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(StringUtils.isBlank(countryName)) {
			throw new ProvilacException("Invalid country name");
		}
		Country country = countryRepository.findByName(countryName);
		if(doPostValidation && null == country) {
			throw new ProvilacException("Country with this name does not exists");
		}
		if(country == null){
			return null;
		}
		if(populateTransientFields)
			country= populateTransientFields(country, loggedInUser);
		if(fetchEagerly)
			country= fetchEagerly(country);
		
		return country;
	}
	
	/**
	 * 
	 * Updates the Country.
	 * @param id - Id of existing Country
	 * @param Country - Updated Country
	 * @return Updated Country
	 */
	public Country updateCountry(Long id, Country country, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid Country id");
		}
		Country existingCountry = countryRepository.findOne(id);
		
		if(null == existingCountry) {
			throw new ProvilacException("Country with this id does not exists");
		}
		existingCountry.setName(country.getName());
		existingCountry = countryRepository.save(existingCountry);
		activityLogService.createActivityLog(loggedInUser, "update", "Country",id);
		existingCountry.setCipher(EncryptionUtil.encode(country.getId()+""));
		if(populateTransientFields)
			existingCountry =populateTransientFields(existingCountry, loggedInUser);
		if(fetchEagerly)
			existingCountry=fetchEagerly(existingCountry);
		return existingCountry;
	}
	
	public void deleteCountry(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid country id");
		}
		Country country = countryRepository.findOne(id);
		if(null == country) {
			throw new ProvilacException("Country with this id does not exists");
		}
		countryRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "Country",id);
	}
	
	private Country populateTransientFields(Country country, User loggedInUser) {
		
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid loggedInUser");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			country.setIsEditAllowed(true);
			country.setIsDeleteAllowed(true);
		} else {
			country.setIsEditAllowed(false);
			country.setIsDeleteAllowed(false);
		}
		country.setCipher(EncryptionUtil.encode(country.getId()+""));
		return country;
	}
	
	private Country fetchEagerly(Country country) {
		
		return country;
	}
	
}