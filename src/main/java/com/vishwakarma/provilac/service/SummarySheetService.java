package com.vishwakarma.provilac.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SummarySheet;
import com.vishwakarma.provilac.model.SummarySheetRecord;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.SummarySheetSpecifications;
import com.vishwakarma.provilac.repository.SummarySheetRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;
import com.vishwakarma.provilac.utils.RoleCache;


@Service
public class SummarySheetService {

	@Resource
	private SummarySheetRepository summarySheetRepository;
	
	@Autowired 
	private RoleCache roleCache;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private ActivityLogService activityLogService;
	@Resource
	private UserService userService;
	@Transactional
	public SummarySheet createSummarySheet(SummarySheet summarySheet, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == summarySheet) {
			throw new ProvilacException("Invalid summarySheet");
		}
		summarySheet = summarySheetRepository.save(summarySheet);
		activityLogService.createActivityLog(loggedInUser, "add", "SummarySheet", summarySheet.getId());
		
		if (populateTransientFields) {
			summarySheet = populateTransientFields(summarySheet, loggedInUser);
		}
		
		if(fetchEagerly) {
			summarySheet= fetchEagerly(summarySheet);
		}
		return summarySheet;
	}
	
	@Transactional
	public List<SummarySheet> getAllSummarySheets(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<SummarySheet> summarySheets = summarySheetRepository.findAll();
		if(populateTransientFields || fetchEagerly){
		for (SummarySheet summarySheet : summarySheets) {
			if(populateTransientFields)
				summarySheet = populateTransientFields(summarySheet, loggedInUser);
			if (fetchEagerly)
				summarySheet = fetchEagerly(summarySheet);
			}
		}
		return summarySheets;
	}
	
	@Transactional
	public Page<SummarySheet> getSummarySheets(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<SummarySheet> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = summarySheetRepository.findAll(pageRequest);
		Iterator<SummarySheet> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SummarySheet summarySheet = (SummarySheet) iterator.next();
			if(populateTransientFields)
				summarySheet = populateTransientFields(summarySheet, loggedInUser);
			if(fetchEagerly)
				summarySheet = fetchEagerly(summarySheet);
			}
		}
		return page;
	}
	
	
	@Transactional
	public Page<SummarySheet> searchSummarySheet(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<SummarySheet> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = summarySheetRepository.findAll(SummarySheetSpecifications.search(searchTerm), pageRequest);
		Iterator<SummarySheet> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SummarySheet summarySheet = (SummarySheet) iterator.next();
			if(populateTransientFields)
				summarySheet = populateTransientFields(summarySheet, loggedInUser);
			if(fetchEagerly)
				summarySheet = fetchEagerly(summarySheet);
			}
		}
		return page;
	}
	
	public SummarySheet getSummarySheet(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheet id");
		}
		SummarySheet summarySheet = getSummarySheet(id, false);
		if(doPostValidation && null == summarySheet) {
			throw new ProvilacException("SummarySheet with this Id does not exists");
		}
		if(populateTransientFields)
			summarySheet= populateTransientFields(summarySheet, loggedInUser);
		if(fetchEagerly)
			summarySheet= fetchEagerly(summarySheet);
		
		return summarySheet;
	}
	
	public SummarySheet getSummarySheetByCode(String summarySheetCode, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == summarySheetCode) {
			throw new ProvilacException("Invalid SummarySheet id");
		}
		SummarySheet summarySheet = summarySheetRepository.findByCode(summarySheetCode);
		if(doPostValidation && null == summarySheet) {
			throw new ProvilacException("SummarySheet with this Id does not exists");
		}
		if(populateTransientFields)
			summarySheet= populateTransientFields(summarySheet, loggedInUser);
		if(fetchEagerly)
			summarySheet= fetchEagerly(summarySheet);
		
		return summarySheet;
	}

	/**
	 * returns @SummarySheet without Post validation & without populating transient fields
	 * @param id
	 * @return
	 */
	public SummarySheet getSummarySheet(Long id, boolean doPostValidation) {
		
		SummarySheet summarySheet;
		if(null == id) {
			throw new ProvilacException("Invalid summarySheet id");
		}
		summarySheet = summarySheetRepository.findOne(id);
		if(doPostValidation && null == summarySheet) {
			throw new ProvilacException("summarySheet with this Id does not exists");
		}
		return summarySheet;
	}
	
	/**
	 * 
	 * Updates the SummarySheet. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing SummarySheet
	 * @param SummarySheet - Updated SummarySheet
	 * @param loggedInSummarySheet - LoggedIn SummarySheet
	 * @return Updated SummarySheet
	 * 
	 * @author Vishal
	 */
	public SummarySheet updateSummarySheet(Long id, SummarySheet summarySheet, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheet id");
		}
		SummarySheet existingSummarySheet = summarySheetRepository.findOne(id);
		if(null == existingSummarySheet) {
			throw new ProvilacException("SummarySheet with this Id does not exists");
		}
		
		for(SummarySheetRecord sheetRecord:summarySheet.getSummarySheetRecords()){
			summarySheet.getSummarySheetRecords().add(sheetRecord);
		}
		existingSummarySheet = summarySheetRepository.save(existingSummarySheet);
		activityLogService.createActivityLog(loggedInUser, "update", "SummarySheet", id);
		existingSummarySheet.setCipher(EncryptionUtil.encode(summarySheet.getId()+""));
		if(populateTransientFields)
			existingSummarySheet =populateTransientFields(existingSummarySheet, loggedInUser);
		if(fetchEagerly)
			existingSummarySheet=fetchEagerly(existingSummarySheet);
		return existingSummarySheet;
	}
	
	public void deleteSummarySheet(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SummarySheet id");
		}
		SummarySheet summarySheet = summarySheetRepository.findOne(id);
		if(null == summarySheet) {
			throw new ProvilacException("SummarySheet with this Id does not exists");
		}
		summarySheetRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "SummarySheet", id);
	}
	
	private SummarySheet populateTransientFields(SummarySheet summarySheet, User loggedInUser) {
		
		if(null == summarySheet) {
			return summarySheet;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid SummarySheet");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			summarySheet.setIsEditAllowed(true);
			summarySheet.setIsDeleteAllowed(true);
		} else {
			summarySheet.setIsEditAllowed(false);
			summarySheet.setIsDeleteAllowed(false);
		}
		summarySheet.setCipher(EncryptionUtil.encode(summarySheet.getId()+""));
		return summarySheet;
	}
	
	private SummarySheet fetchEagerly(SummarySheet summarySheet) {
		summarySheet.getSummarySheetRecords().size();
		return summarySheet;
	}
	
}

