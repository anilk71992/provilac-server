package com.vishwakarma.provilac.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.RequestLineItem;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.SubscriptionRequest;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.specifications.SubscriptionRequestSpecifications;
import com.vishwakarma.provilac.repository.SubscriptionRequestRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.NotificationHelper;

 
@Service
public class SubscriptionRequestService {

	@Resource
	private SubscriptionRequestRepository subscriptionRequestRepository;
	
	@Resource
	private NotificationHelper notificationHelper;
	
	@Resource
	private RequestLineItemService requestLineItemService;
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	public SubscriptionRequest createSubscriptionRequest(SubscriptionRequest subscriptionRequest, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if (null == subscriptionRequest) {
			throw new ProvilacException("Invalid subscriptionRequest");
		}
		subscriptionRequest = subscriptionRequestRepository.save(subscriptionRequest);
		activityLogService.createActivityLog(loggedInUser, "add", "subscriptionRequest", subscriptionRequest.getId());
		Set<RequestLineItem> lineItems= new HashSet<RequestLineItem>();
		for (RequestLineItem lineItem : subscriptionRequest.getRequestLineItems()) {
			lineItem.setSubscriptionRequest(subscriptionRequest);
			lineItems.add(lineItem);
		}
		requestLineItemService.createRequestLineItems(lineItems, loggedInUser, false, false);
		if (populateTransientFields) {
			subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
		}
		
		if(fetchEagerly) {
			subscriptionRequest= fetchEagerly(subscriptionRequest);
		}
		return subscriptionRequest;
	}
	
	public Long getCountByRequestType(boolean isChangeRequest) {
		return subscriptionRequestRepository.getCountForRequestType(isChangeRequest);
	}
	
	@Transactional
	public List<SubscriptionRequest> getAllsubScriptionRequests(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<SubscriptionRequest> subscriptionRequests = subscriptionRequestRepository.findAll();
		if(populateTransientFields || fetchEagerly){
			for (SubscriptionRequest subscriptionRequest : subscriptionRequests) {
				if(populateTransientFields)
					subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
				if (fetchEagerly)
					subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return subscriptionRequests;
	}
	
	@Transactional
	public List<SubscriptionRequest> getAllCurrentSubscriptionRequests(User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		List<SubscriptionRequest> subscriptionRequests = subscriptionRequestRepository.findByIsCurrent(true);
		if(populateTransientFields || fetchEagerly){
			for (SubscriptionRequest subscriptionRequest : subscriptionRequests) {
				if(populateTransientFields)
					subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
				if (fetchEagerly)
					subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return subscriptionRequests;
	}
	
	@Transactional
	public Page<SubscriptionRequest> getSubscriptionRequests(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<SubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = subscriptionRequestRepository.findByIsChangeRequest(false, pageRequest);
		Iterator<SubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SubscriptionRequest subscriptionRequest = (SubscriptionRequest) iterator.next();
			if(populateTransientFields)
				subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
			if(fetchEagerly)
				subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<SubscriptionRequest> getChangeSubscriptionRequests(Integer pageNumber, User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		Page<SubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = subscriptionRequestRepository.findByIsChangeRequest(true, pageRequest);
		Iterator<SubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SubscriptionRequest subscriptionRequest = (SubscriptionRequest) iterator.next();
			if(populateTransientFields)
				subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
			if(fetchEagerly)
				subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<SubscriptionRequest> searchSubscriptionRequests(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<SubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = subscriptionRequestRepository.findAll(SubscriptionRequestSpecifications.search(searchTerm, false), pageRequest);
		
		Iterator<SubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields||fetchEagerly) {
			while (iterator.hasNext()) {
			SubscriptionRequest subscriptionRequest = (SubscriptionRequest) iterator.next();
			if(populateTransientFields)
				subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
			if(fetchEagerly)
				subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public Page<SubscriptionRequest> searchChangeSubscriptionRequests(Integer pageNumber, String searchTerm, User loggedInUser, boolean populateTransientFields,boolean fetchEagerly) {
		
		Page<SubscriptionRequest> page = null;
		PageRequest pageRequest = new PageRequest(pageNumber - 1, AppConstants.PAGE_SIZE, Direction.ASC, "id");
		page = subscriptionRequestRepository.findAll(SubscriptionRequestSpecifications.search(searchTerm, true), pageRequest);
		Iterator<SubscriptionRequest> iterator = page.iterator();
		if(populateTransientFields || fetchEagerly) {
			while (iterator.hasNext()) {
				SubscriptionRequest subscriptionRequest = (SubscriptionRequest) iterator.next();
				if(populateTransientFields)
					subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
				if(fetchEagerly)
					subscriptionRequest = fetchEagerly(subscriptionRequest);
			}
		}
		return page;
	}
	
	@Transactional
	public SubscriptionRequest getSubscriptionRequestByCustomerAndIsCurrent(String code ,boolean isCurrent ,User loggedInUser , boolean populateTransientFields,boolean fetchEagerly) {
		
		if (null == code) {
			throw new ProvilacException("Invalid User code");
		}
		SubscriptionRequest subscriptionRequest = subscriptionRequestRepository.findByUser_CodeAndIsCurrent(code, isCurrent);
		if (subscriptionRequest == null) {
			return null;
		}
		if (populateTransientFields)
			subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
		if (fetchEagerly)
			subscriptionRequest = fetchEagerly(subscriptionRequest);

		return subscriptionRequest;
	}		
	
	
	@Transactional
	public SubscriptionRequest getSubscriptionRequest(Long id, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SubscriptionRequest id");
		}
		SubscriptionRequest subscriptionRequest = getSubscriptionRequest(id, false);
		if(doPostValidation && null == subscriptionRequest) {
			throw new ProvilacException("SubscriptionRequest with this Id does not exists");
		}
		if(populateTransientFields)
			subscriptionRequest= populateTransientFields(subscriptionRequest, loggedInUser);
		if(fetchEagerly)
			subscriptionRequest= fetchEagerly(subscriptionRequest);
		
		return subscriptionRequest;
	}
	
	
	@Transactional
	public SubscriptionRequest getSubscriptionRequestByCode(String code, boolean doPostValidation, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {

		if(null == code) {
			throw new ProvilacException("Invalid SubscriptionRequest id");
		}
		SubscriptionRequest subscriptionRequest = subscriptionRequestRepository.findByCode(code);
		if(doPostValidation && null == subscriptionRequest) {
			throw new ProvilacException("SubscriptionRequest with this Id does not exists");
		}
		if(populateTransientFields)
			subscriptionRequest= populateTransientFields(subscriptionRequest, loggedInUser);
		if(fetchEagerly)
			subscriptionRequest= fetchEagerly(subscriptionRequest);

		return subscriptionRequest;
	}
	
	@Transactional
	@Deprecated
	public SubscriptionRequest getSubscriptionRequestByCustomer(String customerMobileNumber,User loggedInUser,boolean populateTransientFields, boolean fetchEagerly) {
		
		SubscriptionRequest subscriptionRequest = subscriptionRequestRepository.findOne(SubscriptionRequestSpecifications.searchByCustomermobileNumber(customerMobileNumber));
		
		if(null==subscriptionRequest){
			return null;
		}
			
			if(populateTransientFields)
				subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
			if (fetchEagerly)
				subscriptionRequest = fetchEagerly(subscriptionRequest);
			
		return subscriptionRequest;
	}
	
	@Transactional
	public List<SubscriptionRequest> getSubscriptionRequestsByCustomer(String code ,User loggedInUser , boolean populateTransientFields,boolean fetchEagerly) {
		
		if (null == code) {
			throw new ProvilacException("Invalid User code");
		}
		List<SubscriptionRequest> subscriptionRequests = subscriptionRequestRepository.findByUser_Code(code);
		if(null==subscriptionRequests){
			return null;
		}
		if(populateTransientFields || fetchEagerly){
			for (SubscriptionRequest subscriptionRequest : subscriptionRequests) {
				if(populateTransientFields)
					subscriptionRequest = populateTransientFields(subscriptionRequest, loggedInUser);
				if (fetchEagerly)
					subscriptionRequest = fetchEagerly(subscriptionRequest);
				}
			}
		return subscriptionRequests;
	}
	
	@Deprecated
	public SubscriptionRequest getSubscriptionRequest(Long id, boolean doPostValidation) {
		
		SubscriptionRequest  subscriptionRequest;
		if(null == id) {
			throw new ProvilacException("Invalid SubscriptionRequest id");
		}
		subscriptionRequest = subscriptionRequestRepository.findOne(id);
		if(doPostValidation && null == subscriptionRequest) {
			throw new ProvilacException("SubscriptionRequest with this Id does not exists");
		}
		return subscriptionRequest;
	}
	
	/**
	 * 
	 * Updates the UserSubscription. Do not updates email, password, roles & verification code
	 * 
	 * @param id - Id of existing UserSubscription
	 * @param UserSubscription - Updated UserSubscription
	 * @param loggedInUserSubscription - LoggedIn UserSubscription
	 * @return Updated UserSubscription
	 * 
	 * @author Vishal
	 */
	@Transactional
	public SubscriptionRequest updateSubscriptionRequest(Long id, SubscriptionRequest subscriptionRequest, User loggedInUser,boolean populateTransientFields,boolean fetchEagerly) {
		
		if(null == id) {
			throw new ProvilacException("Invalid subscriptionRequest id");
		}
		SubscriptionRequest existingSubscriptionRequest = subscriptionRequestRepository.findOne(id);
		if(null == existingSubscriptionRequest) {
			throw new ProvilacException("SubscriptionRequest with this Id does not exists");
		}
//		existingUserSubscription.setType(userSubscription.getType());
		existingSubscriptionRequest.setUser(subscriptionRequest.getUser());
		existingSubscriptionRequest.setIsCurrent(subscriptionRequest.getIsCurrent());
		if(null!=subscriptionRequest.getPermanantNote()){
			existingSubscriptionRequest.setPermanantNote(subscriptionRequest.getPermanantNote());
		}
		existingSubscriptionRequest.setInternalNote(subscriptionRequest.getInternalNote());
		existingSubscriptionRequest.setIsSubscriptionRequest(subscriptionRequest.getIsSubscriptionRequest());
		
		existingSubscriptionRequest = subscriptionRequestRepository.save(existingSubscriptionRequest);
		activityLogService.createActivityLog(loggedInUser, "updated", "SubscriptionRequests", id);
		existingSubscriptionRequest.setCipher(EncryptionUtil.encode(subscriptionRequest.getId()+""));
		if(populateTransientFields)
			existingSubscriptionRequest =populateTransientFields(existingSubscriptionRequest, loggedInUser);
		if(fetchEagerly)
			existingSubscriptionRequest=fetchEagerly(existingSubscriptionRequest);
		return existingSubscriptionRequest;
	}
	
	public void deleteSubscriptionRequest(Long id) {
		
		if(null == id) {
			throw new ProvilacException("Invalid SubscriptionRequest id");
		}
		SubscriptionRequest subscriptionRequest = subscriptionRequestRepository.findOne(id);
		if(null == subscriptionRequest) {
			throw new ProvilacException("SubscriptionRequest with this Id does not exists");
		}
		subscriptionRequestRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "SubscriptionRequest", id);
	}

	@Transactional
	public void deleteSubscriptionRequestsForUser(Long userId) {
		
		if(null == userId) {
			throw new ProvilacException("Invalid user id");
		}
		subscriptionRequestRepository.deleteByUser(userId);
	}
	
	private SubscriptionRequest populateTransientFields(SubscriptionRequest subscriptionRequest, User loggedInUser) {
		
		if(null == subscriptionRequest) {
			return subscriptionRequest;
		}
		if(null == loggedInUser) {
			throw new ProvilacException("Invalid SubscriptionRequest");
		}
		if(loggedInUser.hasRole(Role.ROLE_SUPER_ADMIN)) {
			subscriptionRequest.setIsEditAllowed(true);
			subscriptionRequest.setIsDeleteAllowed(true);
		} else {
			subscriptionRequest.setIsEditAllowed(false);
			subscriptionRequest.setIsDeleteAllowed(false);
		}
		subscriptionRequest.setCipher(EncryptionUtil.encode(subscriptionRequest.getId()+""));
		return subscriptionRequest;
	}
	
	private SubscriptionRequest fetchEagerly(SubscriptionRequest subscriptionRequest) {
		subscriptionRequest.getRequestLineItems().size();
		return subscriptionRequest;
	}
	
}

