package com.vishwakarma.provilac.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.exception.ProvilacException;
import com.vishwakarma.provilac.model.ProvilacImage;
import com.vishwakarma.provilac.model.Role;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.repository.ProvilacImageRepository;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.AppFileUtils;
import com.vishwakarma.provilac.utils.EncryptionUtil;

@Service
public class ProvilacImageService {

	@Resource
	private ProvilacImageRepository provilacImageRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
	private AppFileUtils appFileUtils;
	
	
	@Resource
	private ActivityLogService activityLogService;
	
	@Transactional
	public ProvilacImage createProvilacImage(ProvilacImage provilacImage,User loggedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		ProvilacImage saveProvilacImage=provilacImageRepository.save(provilacImage);
		activityLogService.createActivityLog(loggedInUser, "add", "ProvilacImage", provilacImage.getId());
		
		if(null!=saveProvilacImage){
			if(null!=saveProvilacImage.getPicData()){
					appFileUtils.saveData(AppConstants.PROVILAC_IMAGES_FOLDER, saveProvilacImage.getId() +"", saveProvilacImage.getPicData());
			}
		}
		if(fetchEagerly||populateTransientFields){
			if(null!=saveProvilacImage){
				if(populateTransientFields){
					saveProvilacImage=populateTransientFields(saveProvilacImage, loggedInUser);
				}
				if(fetchEagerly){
					saveProvilacImage=fetchEagerly(saveProvilacImage);
				}
			}
		}
		return saveProvilacImage;
	}
	
	@Transactional
	public List<ProvilacImage> getallProvilacImages(User logInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		List<ProvilacImage> provilacImages=provilacImageRepository.findAll();
		if(populateTransientFields||fetchEagerly){
			for(ProvilacImage provilacImage:provilacImages){
				if(populateTransientFields){
				provilacImage=populateTransientFields(provilacImage, logInUser);	
				}
				if(fetchEagerly){
					provilacImage=fetchEagerly(provilacImage);
				}
			}
			
		}
		
		return provilacImages;
	}
	
	@Transactional
	public ProvilacImage updateProvilacImage(Long id,ProvilacImage provilacImage,User logedInUser,boolean populateTransientFields,boolean fetchEagerly){
		
		ProvilacImage existingProvilacImage=provilacImageRepository.findOne(id);
		if(null==existingProvilacImage){
			throw new ProvilacException("Provilac Image Not Found for this Id");
		}
		
		if(null!=provilacImage){
			if(null!=provilacImage.getPicData()){
				appFileUtils.saveData(AppConstants.PROVILAC_IMAGES_FOLDER, existingProvilacImage.getId()+"", provilacImage.getPicData());
			}
		}
		
		existingProvilacImage.setPicData(provilacImage.getPicData());
		existingProvilacImage.setPicUrl(provilacImage.getPicUrl());
		existingProvilacImage.setTempFileName(provilacImage.getTempFileName());
		existingProvilacImage.setLinkTo(provilacImage.getLinkTo());
		existingProvilacImage=provilacImageRepository.save(existingProvilacImage);
		activityLogService.createActivityLog(logedInUser, "update", "ProvilacImage", id);
		
		if(populateTransientFields||fetchEagerly){
			if(populateTransientFields){
			existingProvilacImage=populateTransientFields(existingProvilacImage, logedInUser);
			}
			if(fetchEagerly){
				existingProvilacImage=fetchEagerly(existingProvilacImage);
			}
		}
		return existingProvilacImage;
	}
	
	@Transactional
	public void deleteProvilacImage(Long id){
		
		if(null==id){
			throw new ProvilacException("Invalid Log-in User");
		}
		
		ProvilacImage provilacImage=provilacImageRepository.findOne(id);
		if(null==provilacImage){
			throw new ProvilacException("Provilac Image with This Id does not Exists");
		}
		
		provilacImageRepository.delete(id);
		activityLogService.createActivityLog(null, "delete", "ProvilacImage", id);
		
	}
	
	private ProvilacImage populateTransientFields(ProvilacImage provilacImage,User logInUser){
		if(null==logInUser){
			throw new ProvilacException("Invalid Log-in User");
		}
		
		if(logInUser.hasRole(Role.ROLE_SUPER_ADMIN)){
			provilacImage.setIsEditAllowed(true);
			provilacImage.setIsDeleteAllowed(true);
		}else{
			provilacImage.setIsEditAllowed(false);
			provilacImage.setIsDeleteAllowed(false);
		}
		
		provilacImage.setCipher(EncryptionUtil.encode(provilacImage.getId()+""));
		return provilacImage;
	}
	
	private ProvilacImage fetchEagerly(ProvilacImage provilacImage){
		return provilacImage;
	}
}
