package com.vishwakarma.provilac.messaging;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MessageContent;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.model.OrderLineItem;
import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;
import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.model.SubscriptionLineItem;
import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.utils.AppConstants;
import com.vishwakarma.provilac.utils.DateHelper;
import com.vishwakarma.provilac.utils.EncryptionUtil;
import com.vishwakarma.provilac.utils.AppConstants.Day;

@Component
public class EmailHelper {

	@Resource
	private JavaMailSenderImpl javaMailSenderImpl;
	
	@Resource
	private Environment environment;

	@Resource
	private VelocityEngine velocityEngine;
	
	private boolean isEnabled;
	
	private MandrillApi mandrilApi;
	
	@PostConstruct
	public void doInit() {
		isEnabled = Boolean.parseBoolean(environment.getProperty("email.enabled", "false"));
		mandrilApi = new MandrillApi(environment.getProperty("email.password", ""));
	}

	private void sendMail(SimpleMailMessage message) {
		if(isEnabled)
			javaMailSenderImpl.send(message);
	}
	
	private void sendMail(MimeMessagePreparator messagePreparator) {
		if(isEnabled)
			javaMailSenderImpl.send(messagePreparator);
	}
	
	@Async
	public void sendSimpleEmail(String email, String subject, String messageBody) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom(environment.getProperty("email.senderName"));
		simpleMailMessage.setTo(email);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(messageBody);
		simpleMailMessage.setReplyTo(environment.getProperty("email.replyTo"));
		sendMail(simpleMailMessage);
	}
	
	@Async
	@Deprecated
	public void sendMailForPasswordRecovery(final String email,final String subject,final String messageBody) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(email);
				mimeMessageHelper.setSubject(subject);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForCronJobCompletion(final String email,final String subject,final String messageBody) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(email);
				mimeMessageHelper.setSubject(subject);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForPaymentReceived(final User member,final Payment payment, final Double lastPendingAmount) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(member.getEmail());
				mimeMessageHelper.setSubject("Thanks Payment Received");

				Map modelMap = new HashMap();
				modelMap.put("username", member.getFullName());
				modelMap.put("amount", payment.getAmount());
				modelMap.put("totalAmount", member.getLastPendingDues());
				modelMap.put("pendingAmount", lastPendingAmount);
				modelMap.put("receiptNo", payment.getReceiptNo());
				modelMap.put("date", payment.getDate());
				modelMap.put("paymentType", payment.getPaymentMethod());
				modelMap.put("url", environment.getProperty("server.url"));
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/paymentReceived_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForInvoiceGeneration(final User member,
												final String monthAndYear, 
												final File file,
												final Invoice invoice) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(member.getEmail());
				mimeMessageHelper.setSubject("Provilac Invoice For "+monthAndYear);

				Map modelMap = new HashMap();
				modelMap.put("username", member.getFullName());
				modelMap.put("date", new Date());
				modelMap.put("monthAndYear", monthAndYear);

				String serverUrl = environment.getProperty("server.url");
				modelMap.put("url", serverUrl);
				if(serverUrl.endsWith("/")) {
					serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
				}
				serverUrl = serverUrl + AppConstants.WEB_PAYMENT_URL + "?" + AppConstants.WEB_PAYMENT_PARAM_NAME + "=" + EncryptionUtil.encode(invoice.getId() + "");
				modelMap.put("paymentUrl", serverUrl);
				
				Date dueDate = DateHelper.addDays(invoice.getToDate(), 15);
				modelMap.put("dueDate", DateHelper.getFormattedDateSimple(dueDate));
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/invoiceGenerated_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
				mimeMessageHelper.addAttachment("Invoice.pdf", file);
			}
		};
		isEnabled = true;
		sendMail(messagePreparator);
	}
	
	@Async
	@Deprecated
	public void sendMailForPaymentBounced(final User member,final Payment payment) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(member.getEmail());
				mimeMessageHelper.setSubject("Payment Bounced ");

				Map modelMap = new HashMap();
				modelMap.put("username", member.getFullName());
				modelMap.put("amount", payment.getAmount());
				modelMap.put("receiptNo", payment.getReceiptNo());
				modelMap.put("date", payment.getDate());
				SimpleDateFormat format = new SimpleDateFormat("MMM");
				modelMap.put("month", format.format(payment.getDate()));
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/paymentBounced_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForNonDeliverdOrder(final DeliverySchedule deliverySchedule) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(deliverySchedule.getCustomer().getEmail());
				mimeMessageHelper.setSubject("Not Deliverd Order");

				Map modelMap = new HashMap();
				modelMap.put("username", deliverySchedule.getCustomer().getFullName());
				modelMap.put("date", deliverySchedule.getDate());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/nonDeliverdOrder_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	@Deprecated
	public void sendPromotionalEmail(final User user) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(user.getEmail());
				mimeMessageHelper.setSubject("Provilac Refer & Earn");

				Map modelMap = new HashMap();
				modelMap.put("name", user.getFullName());
				modelMap.put("code", user.getCode());

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/promotionalMessage_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForSubscriptionCreation(final UserSubscription subscription,final Date endDate) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription Created");

				Map modelMap = new HashMap();
				modelMap.put("username", subscription.getUser().getFullName());
				modelMap.put("startDate", subscription.getStartDate());
				modelMap.put("endDate", endDate);
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/subscriptionAdded_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForSubscriptionCreationFirstTime(final UserSubscription subscription){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription Created first time");
				Date startDate = subscription.getStartDate();
				DateHelper.setToStartOfDay(startDate);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(startDate);
				calendar.add(Calendar.MONTH, 2);
				Date endDate = calendar.getTime();
				DateHelper.setToEndOfDay(endDate);
				Map modelMap = new HashMap();
				modelMap.put("username", subscription.getUser().getFirstName());
				modelMap.put("startDate", DateHelper.getFormattedDate(subscription.getStartDate()));
				modelMap.put("endDate", DateHelper.getFormattedDate(endDate));
				modelMap.put("subscriptionLineItems", subscription.getSubscriptionLineItems());
				modelMap.put("url", environment.getProperty("server.url"));
			
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/subscriptionAddedFirstTime_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForLowPrepayBalance(final User user){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(user.getEmail());
				mimeMessageHelper.setSubject("Subscription Created first time");
				
				String username = user.getFirstName();
				if(StringUtils.isEmpty(user.getLastName())) {
					username = username + " " + user.getLastName();
				}
				
				Map modelMap = new HashMap();
				modelMap.put("username", username);
				modelMap.put("url", environment.getProperty("server.url"));
			
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/prepay_balance_running_low.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailOnPrepayStopped(final User user){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(user.getEmail());
				mimeMessageHelper.setSubject("Subscription Created first time");
				
				String username = user.getFirstName();
				if(StringUtils.isEmpty(user.getLastName())) {
					username = username + " " + user.getLastName();
				}
				
				Map modelMap = new HashMap();
				modelMap.put("username", username);
				modelMap.put("url", environment.getProperty("server.url"));
			
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/prepay_balance_running_low.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForPrepaySubscriptionApproval(final UserSubscription subscription, final Map<Product, Double> effectivePriceMap){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription Created first time");
				Date startDate = subscription.getStartDate();
				DateHelper.setToStartOfDay(startDate);
				Date endDate = subscription.getEndDate();
				DateHelper.setToEndOfDay(endDate);
				List<Map<String, Object>> lineItems = new ArrayList<Map<String,Object>>();
				
				//Calculate total quantity and total price for individual lineitem
				Date runningDate = new Date(startDate.getTime());
				for(SubscriptionLineItem lineItem : subscription.getSubscriptionLineItems()) {
					
					Map<String, Object> lineItemMap = new HashMap<String, Object>();
					lineItemMap.put("product", lineItem.getProduct());
					lineItemMap.put("type", lineItem.getType().name());
					
					int totalQuantity = 0;
					double totalAmount = 0;
					
					double pricePerUnit = 0;
					if(effectivePriceMap.containsKey(lineItem.getProduct())) {
						pricePerUnit = effectivePriceMap.get(lineItem.getProduct());
					} else {
						pricePerUnit = lineItem.getProduct().getPrice();
					}
					
					if(lineItem.getType() == SubscriptionType.Daily) {
						
						while(runningDate.before(endDate)) {
							totalQuantity = totalQuantity + lineItem.getQuantity();
							totalAmount = totalAmount + (pricePerUnit * lineItem.getQuantity());
							runningDate = DateHelper.addDays(runningDate, 1);
						}
						lineItemMap.put("quantity", lineItem.getQuantity());
					} else if(lineItem.getType() == SubscriptionType.Weekly) {
						
						while(runningDate.before(endDate)) {
							int dayOfWeek = DateHelper.getDateField(runningDate, Calendar.DAY_OF_WEEK);
							Day currentDay = Day.values()[dayOfWeek - 1];
							if(lineItem.getDay() == currentDay && lineItem.getQuantity() > 0) {
								totalQuantity = totalQuantity + lineItem.getQuantity();
								totalAmount = totalAmount + (pricePerUnit * lineItem.getQuantity());
							}
							runningDate = DateHelper.addDays(runningDate, 1);
						}
						lineItemMap.put("quantity", lineItem.getQuantity());
						String day = "";
						switch(lineItem.getDay().ordinal() + 1) {
							case Calendar.SUNDAY:
								day = "Sunday";
								break;
							case Calendar.MONDAY:
								day = "Monday";
								break;
							case Calendar.TUESDAY:
								day = "Tuesday";
								break;
							case Calendar.WEDNESDAY:
								day = "Wednesday";
								break;
							case Calendar.THURSDAY:
								day = "Thursday";
								break;
							case Calendar.FRIDAY:
								day = "Friday";
								break;
							case Calendar.SATURDAY:
								day = "Saturday";
								break;
						}
						lineItemMap.put("day", day);
					} else {
						
						int currentIndex = 0;
						String arr[] = lineItem.getCustomPattern().split("-");
						int quantity[] = new int[arr.length]; 
						for (int i = 0; i < arr.length; i++) {
							String qtyString = arr[i];
							quantity[i] = Integer.parseInt(qtyString);
						}
						while (runningDate.before(endDate)) {
							totalQuantity = totalQuantity + quantity[currentIndex];
							totalAmount = totalAmount + (pricePerUnit * quantity[currentIndex]);
							currentIndex = currentIndex + 1;
							if(currentIndex >= quantity.length) {
								currentIndex = 0;
							}
							runningDate = DateHelper.addDays(runningDate, 1);
						}
						lineItemMap.put("customPattern", lineItem.getCustomPattern());
					}
					
					lineItemMap.put("totalQuantity", totalQuantity);
					lineItemMap.put("totalAmount", totalAmount);
					
					lineItems.add(lineItemMap);
				}
				
				String username = subscription.getUser().getFirstName();
				if(StringUtils.isEmpty(subscription.getUser().getLastName())) {
					username = username + " " + subscription.getUser().getLastName();
				}
				
				Map modelMap = new HashMap();
				modelMap.put("username", username);
				modelMap.put("startDate", DateHelper.getFormattedDate(startDate));
				modelMap.put("endDate", DateHelper.getFormattedDate(endDate));
				modelMap.put("lineItems", lineItems);
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("effectivePriceMap", effectivePriceMap);
				modelMap.put("subscriptionId", subscription.getId());
			
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/prepay_subscription_approved_template.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	@Deprecated
	public void sendMailForSubscriptionUpdate(final UserSubscription subscription,final Date endDate) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription Edited");

				Map modelMap = new HashMap();
				modelMap.put("username", subscription.getUser().getFullName());
				modelMap.put("startDate", subscription.getStartDate());
				modelMap.put("endDate", endDate);
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/subscriptionAdded_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForSubscriptionStop(final UserSubscription subscription) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription Stop");

				Map modelMap = new HashMap();
				modelMap.put("username", subscription.getUser().getFullName());
				modelMap.put("Date", subscription.getStartDate());
				modelMap.put("lastPendingDues", subscription.getUser().getLastPendingDues());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/subscriptionStop_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
				//mimeMessageHelper.addAttachment("Invoice.pdf", invoice);
			}
		};
		sendMail(messagePreparator);
	}
	
	
	@Async
	public void sendMailForDeliveryScheduleChange(final DeliverySchedule deliverySchedule) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(deliverySchedule.getCustomer().getEmail());
				mimeMessageHelper.setSubject("Delivery Schedule Change");

				Map modelMap = new HashMap();
				modelMap.put("username", deliverySchedule.getCustomer().getFullName());
				modelMap.put("date", DateHelper.getFormattedDate(deliverySchedule.getDate()));
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/deliveryScheduleChange_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	
	@Async
	public void sendMailForSubscriptionOnHold(final UserSubscription subscription) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(subscription.getUser().getEmail());
				mimeMessageHelper.setSubject("Subscription On Hold");

				Map modelMap = new HashMap();
				modelMap.put("username", subscription.getUser().getFullName());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/subscriptionOnHold_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	
	@Async
	@Deprecated
	public void sendMailForReferralBalanceAdded(final User customer) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				mimeMessageHelper.setSubject("Referral Balance Added");

				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/referralBalanceAdded_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	@Deprecated
	public void sendMailForTrialDeliveryToday(final User customer) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				mimeMessageHelper.setSubject("Trial Delivery Today");

				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/trialDeliveryToday_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	
	@Async
	public void sendMailForTrialDeliveryTommorow(final User customer) {

		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				mimeMessageHelper.setSubject("Trial Delivery Tommorow");

				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));

				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/trialDeliveryTommorow_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForOneTimeOrderNotification(final User customer,final OneTimeOrder oneTimeOrder){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				// TODO Auto-generated method stub
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				mimeMessageHelper.setSubject("One Time Order");
				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("orderId", oneTimeOrder.getId());
				modelMap.put("date",oneTimeOrder.getDate());
				modelMap.put("deliveryDate", DateHelper.getFormattedDate(oneTimeOrder.getDate()));
				//modelMap.put("deliveryDate", DateHelper.getFormattedDate(oneTimeOrder.getDeliveryTime()));
				modelMap.put("totalAmount", oneTimeOrder.getTotalAmount());
				modelMap.put("pointsDiscount", oneTimeOrder.getPointsDiscount());
				modelMap.put("finalAmount", oneTimeOrder.getFinalAmount());
				modelMap.put("orderLineItems", oneTimeOrder.getOrderLineItems());
				
				String addressLine1 = oneTimeOrder.getAddress().getAddressLine1();
				if(StringUtils.isBlank(addressLine1) || addressLine1.toLowerCase().startsWith("json")) {
					addressLine1 = "";
				}
				modelMap.put("addressLine1", addressLine1);
				
				String addressLine2 = oneTimeOrder.getAddress().getAddressLine2();
				if(StringUtils.isBlank(addressLine2) || addressLine2.toLowerCase().startsWith("json")) {
					addressLine2 = "";
				}
				modelMap.put("addressLine2", addressLine2);

				String locality = oneTimeOrder.getAddress().getLocality();
				if(StringUtils.isBlank(locality) || locality.toLowerCase().startsWith("json")) {
					locality = "";
				}
				modelMap.put("locality", locality);

				String landmark = oneTimeOrder.getAddress().getLandmark();
				if(StringUtils.isBlank(landmark) || landmark.toLowerCase().startsWith("json")) {
					landmark = "";
				}
				modelMap.put("Landmark", landmark);

				String cityName = oneTimeOrder.getAddress().getCity().getName();
				if(StringUtils.isBlank(cityName) || cityName.toLowerCase().startsWith("json")) {
					cityName = "";
				}
				modelMap.put("cityName", cityName);

				String pincode = oneTimeOrder.getAddress().getPincode();
				if(StringUtils.isBlank(pincode) || pincode.toLowerCase().startsWith("json")) {
					pincode = "";
				}
				modelMap.put("pinCode", pincode);
				
				Map<String, Integer> product = new HashMap<String, Integer>() ;
				
				for (OrderLineItem item :oneTimeOrder.getOrderLineItems()) {
					product.put(item.getProduct().getName(), item.getQuantity());
				}
				modelMap.put("product", product);
			
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/oneTimeOrder_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				//mimeMessageHelper.setSubject("Order Delivered");
				//mimeMessageHelper.addAttachment("Invoice.pdf", invoice);
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	@Deprecated
	public void sendMailForOneTimeOrderOnDeliveredNotification(final User customer, final OneTimeOrder oneTimeOrder){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				// TODO Auto-generated method stub
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("date", oneTimeOrder.getDate());
				
				Map<String, Integer>  product = new HashMap<String, Integer>();
				for (OrderLineItem item : oneTimeOrder.getOrderLineItems()) {
					product.put(item.getProduct().getName(), item.getQuantity());	
				}
				modelMap.put("product", product);	
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/oneTimeOrderDelivered_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));	
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForSelectedCustomer(final User customer, final String message, final String title, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				// TODO Auto-generated method stub
				MimeMessageHelper mimeMessageHelper;
				
				if(null != attachmentStream) {

					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(customer.getEmail());
				mimeMessageHelper.setSubject(title);
				Map modelMap = new HashMap();
				modelMap.put("username", customer.getFullName());
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("date", new Date());
				modelMap.put("message", message);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/sendFromWebEmail_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public boolean sendMailWithMandrilTemplate(List<String> emailIds, String subject, String templateName, InputStream picAttachment) {
		
		if(!isEnabled)
			return true;
		
		MandrillMessage mandrillMessage = new MandrillMessage();
		mandrillMessage.setSubject(subject);
		mandrillMessage.setFromEmail(environment.getProperty("email.senderEmail"));
		mandrillMessage.setFromName(environment.getProperty("email.senderName"));
		
		List<Recipient> recipients = new ArrayList<MandrillMessage.Recipient>();
		for (String emailId : emailIds) {
			Recipient recipient = new Recipient();
			recipient.setEmail(emailId);
			recipients.add(recipient);
		}
		mandrillMessage.setTo(recipients);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Reply-To", environment.getProperty("email.replyTo"));
		mandrillMessage.setHeaders(headers);
		
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("website", "www.provilac.com");
		mandrillMessage.setMetadata(metadata);
		
		if(null != picAttachment) {

			List<MessageContent> attachments = new ArrayList<MandrillMessage.MessageContent>();
			MessageContent content = new MessageContent();
			content.setName("attachment.jpg");
			content.setType("image/jpeg");
			try {
				content.setContent(Base64.getEncoder().encodeToString(IOUtils.toByteArray(picAttachment)));
				attachments.add(content);
				mandrillMessage.setAttachments(attachments);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			mandrilApi.messages().sendTemplate(templateName, new HashMap<String, String>(), mandrillMessage, true);
			return true;
		} catch (MandrillApiError e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Async
	public void sendMail(final String emailId, final String message, final String subject, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper;
				if(null != attachmentStream) {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("date", new Date());
				modelMap.put("message", message);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/dashboard_anonymous_email_template.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForPostpaid(final String emailId, final List<Map<String, Object>> subscriptionItems, final Double averageMonthlyBill, final String startedFrom, final String subject, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper;
				if(null != attachmentStream) {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("averageMonthlyBill", averageMonthlyBill);
				modelMap.put("startedFrom", startedFrom);
				modelMap.put("subscriptionItems", subscriptionItems);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/end-user-templet/postpaid_invoice_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForPrePay(final String emailId, final List<Map<String, Object>> subscriptionItems, final PrepaySubscriptionRequest prepaySubscriptionRequest,  final Double lastPendingdues, final Double totalUnbilledAmount, final Double finalAmount, final String subject, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper;
				if(null != attachmentStream) {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("subscriptionItems", subscriptionItems);
				modelMap.put("prepaySubscriptionRequest", prepaySubscriptionRequest);
				modelMap.put("lastPendingdues", lastPendingdues);
				modelMap.put("totalUnbilledAmount", totalUnbilledAmount);
				modelMap.put("finalAmount", finalAmount);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/end-user-templet/prepaid_invoice_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForExtendPrePay(final String emailId, final List<Map<String, Object>> subscriptionItems, final String txnid, final String paymentMethod, final Double amount, final String subject, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper;
				if(null != attachmentStream) {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("subscriptionItems", subscriptionItems);
				modelMap.put("txnid", txnid);
				modelMap.put("paymentMethod", paymentMethod);
				modelMap.put("amount", amount);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/end-user-templet/extend_prepaid_invoice_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendMailForCreatePrePay(final String emailId, final List<Map<String, Object>> subscriptionItems, final PrepaySubscriptionRequest prepaySubscriptionRequest,  final Double lastPendingdues, final Double finalAmount, final String subject, final InputStream attachmentStream){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper;
				if(null != attachmentStream) {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
					InputStreamSource inputStreamSource = new ByteArrayResource(IOUtils.toByteArray(attachmentStream));
					mimeMessageHelper.addAttachment("attachment.jpg", inputStreamSource);
				} else {
					mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				}
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("subscriptionItems", subscriptionItems);
				modelMap.put("prepaySubscriptionRequest", prepaySubscriptionRequest);
				modelMap.put("lastPendingdues", lastPendingdues);
				modelMap.put("finalAmount", finalAmount);
				
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/end-user-templet/create_subscription_prepaid_invoice_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}
	
	@Async
	public void sendInvoiceForOneTimeOrder(final String emailId, final List<OrderLineItem> orderLineItems, final OneTimeOrder oneTimeOrder, final String subject, final String createdDate, final String deliveryDate){
		MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
				
				mimeMessageHelper.setFrom(environment.getProperty("email.senderEmail"));
				mimeMessageHelper.setTo(emailId);
				mimeMessageHelper.setSubject(subject);
				Map modelMap = new HashMap();
				modelMap.put("url", environment.getProperty("server.url"));
				modelMap.put("oneTimeOrder", oneTimeOrder);
				modelMap.put("orderLineItems", orderLineItems);
				modelMap.put("createdDate", createdDate);
				modelMap.put("deliveryDate", deliveryDate);
				String messageBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/end-user-templet/invoice_oneTimeOrder_templet.vm", "UTF-8", modelMap);
				mimeMessageHelper.setText(messageBody, true);
				mimeMessageHelper.setReplyTo(environment.getProperty("email.replyTo"));
				
			}
		};
		sendMail(messagePreparator);
	}

}
