package com.vishwakarma.provilac.exception;

public class ProvilacCheckedException extends Exception{
		 
		protected String exceptionMsg;
	 
		public ProvilacCheckedException() {
			super();
		}
				
		public ProvilacCheckedException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}


		public ProvilacCheckedException(Throwable arg0) {
			super(arg0);
		}

		public ProvilacCheckedException(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
		
		public String getExceptionMsg(){
			return this.exceptionMsg;
		}
		
		public void setExceptionMsg(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
}

