package com.vishwakarma.provilac.exception;

public class ProvilacException extends RuntimeException{
		 
		private String exceptionMsg;
		private String errorCode = "400";
	 
		public ProvilacException() {
			super();
		}
		
		public ProvilacException(String exceptionMsg, String errorCode) {
			super();
			this.exceptionMsg = exceptionMsg;
			this.errorCode = errorCode;
		}

		public ProvilacException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}

		public ProvilacException(Throwable arg0) {
			super(arg0);
		}

		public ProvilacException(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}
		
		public String getExceptionMsg(){
			return this.exceptionMsg;
		}
		
		public void setExceptionMsg(String exceptionMsg) {
			this.exceptionMsg = exceptionMsg;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
}

