package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Notification;


public interface NotificationRepository extends JpaRepository<Notification, Long>,JpaSpecificationExecutor<Notification> {

}

