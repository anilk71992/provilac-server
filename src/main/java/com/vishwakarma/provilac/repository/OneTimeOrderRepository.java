package com.vishwakarma.provilac.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.vishwakarma.provilac.model.OneTimeOrder;

@Repository
public interface OneTimeOrderRepository extends JpaRepository<OneTimeOrder,Long>,JpaSpecificationExecutor<OneTimeOrder>{

	public List<OneTimeOrder> findByCustomer_Id(Long id);
	
	public OneTimeOrder findByCode(String code);
	
	public List<OneTimeOrder> findByDate(Date date);
	
	public Page<OneTimeOrder> findByDate(Date date,Pageable pageable);
	
	public List<OneTimeOrder> findByDeliveryBoyIdAndDate(Long deliveryBoyId,Date date);
	
	public List<OneTimeOrder> findByStatusAndDate(String status,Date date);
	
	public OneTimeOrder findByTxnId(String txn);
	
	public OneTimeOrder findByPaymentOrderId(String paymentOrderId);
}
