package com.vishwakarma.provilac.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vishwakarma.provilac.model.DeliverySchedule;

public interface DeliveryScheduleRepository extends JpaRepository<DeliverySchedule, Long>,JpaSpecificationExecutor<DeliverySchedule> {
	
	public DeliverySchedule findByCode(String Code);
	
	public DeliverySchedule findByCustomer_CodeAndDate(String customerCode,Date date);
	
	public List<DeliverySchedule> findByCustomer_Id(Long customerId);
	
	public List<DeliverySchedule> findByCustomer_IdAndDateGreaterThanEqual(Long customerId, Date fromDate);
	
	public List<DeliverySchedule> findByRoute_Id(Long routeId);
	
	public List<DeliverySchedule> findByDeliverySheet(Long deliverySheduleId);
	
	public List<DeliverySchedule> findByCustomer_Code(String customerCode, Pageable pageable);
	
}

