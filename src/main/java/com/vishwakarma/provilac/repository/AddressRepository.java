package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vishwakarma.provilac.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long>,JpaSpecificationExecutor<Address> {
	
	public Address findByCode(String code);

	public List<Address> findByCustomer_Code(String customerCode);	
	
	public List<Address> findByIsDefaultAndCity_Id(Boolean isDefault,Long cityId);
	
	public List<Address> findByCustomer_CodeAndIsDefault(String customerCode,boolean isDefault);
	
	@Modifying
	@Query("Delete from Address a where a.customer.id =:customerId")
	public void deleteAllAddressesForCustomer(@Param("customerId") Long customerId);
}

