package com.vishwakarma.provilac.repository;

import com.vishwakarma.provilac.model.Privilege;


public interface PrivilegeRepository extends CustomRepository<Privilege, Long> {

	public Privilege findByName(final String name);
	
}
