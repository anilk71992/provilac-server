package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.vishwakarma.provilac.model.PrepayRequestLineItem;

@Repository
public interface PrepayRequestLineItemRepository extends JpaRepository<PrepayRequestLineItem, Long>,JpaSpecificationExecutor<PrepayRequestLineItem> {
	
	
	
}

