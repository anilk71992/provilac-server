package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.RequestLineItem;

public interface RequestLineItemRepository extends JpaRepository<RequestLineItem, Long>,JpaSpecificationExecutor<RequestLineItem> {
	
	
	
}

