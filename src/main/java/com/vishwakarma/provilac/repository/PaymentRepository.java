package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long>,JpaSpecificationExecutor<Payment> {
	
	public List<Payment> findByCustomer_Code(String userCode);
	
	public Payment findByCode(String code);
	
	public Payment findBytxnId(String txnId);
	
	public List<Payment>findByIsBounced(boolean isBounced);
	
	public Page<Payment> findByCustomer_Code(String customerCode,Pageable pageable);
	public List<Payment> findByreceivedBy_Id(Long customerId);
	
	public Payment findByChequeNumber(String chequeNumber);
	
	public Page<Payment>findByHasDeposited(boolean hasDeposited,Pageable pageable);
	
	public List<Payment> findByInvoice_Code(String code);
	
}

