package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.provilac.model.ServiceCredit;

public interface ServiceCreditRepository extends JpaRepository<ServiceCredit, Long> {
	public ServiceCredit findByServiceName(String serviceName);
}

