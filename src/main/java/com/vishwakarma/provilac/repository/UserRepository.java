package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vishwakarma.provilac.model.User;
import com.vishwakarma.provilac.model.User.Status;

public interface UserRepository extends JpaRepository<User, Long>,JpaSpecificationExecutor<User> {
	
	//User findByUsername(String username);
	User findByEmail(String email);
	
	User findByMobileNumber(String mobileNumber);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId order by u.id asc")
	List<User> findByRole(@Param("roleId") Long roleId);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id in :roleIds")
	List<User> findByRoles (@Param("roleIds") List<Long> roleIds, Pageable p);

	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id not in :roleIds")
	List<User> findByRolesNotIn (@Param("roleIds") List<Long> roleIds, Pageable p);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id in :roleIds order by u.id asc")
	List<User> findByRoles (@Param("roleIds") List<Long> roleIds);
	
	public User findByCode(String code);
	
	public List<User> findByStatus(Status status);
	
	//public User findByEmailOrMobileNumber(String email);
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId order by u.id asc")
	Page<User> findUserByRole(@Param("roleId") Long roleId ,Pageable pageable);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where NOT role.id = :roleId order by u.id asc")
	Page<User> findUserByRoleNotEqual(@Param("roleId") Long roleId ,Pageable pageable);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND u.status = :status order by u.id asc")
	Page<User> findUserByRoleAndStatus(@Param("roleId") Long roleId ,@Param("status") Status status, Pageable pageable);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND u.status = :status order by u.id asc")
	List<User> findUserByRoleAndStatus(@Param("roleId") Long roleId ,@Param("status") Status status);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND u.status = :status AND u.isRouteAssigned =:isRouteAssigned order by u.id asc")
	Page<User> findUserByRoleIsRouteAssignAndStatus(@Param("roleId") Long roleId ,@Param("status") Status status,@Param("isRouteAssigned") Boolean isRouteAssigned, Pageable pageable);

	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND NOT u.status = :status order by u.id asc")
	Page<User> findUserByRoleAndStatusNotEqualToNew(@Param("roleId") Long roleId ,@Param("status") Status status, Pageable pageable);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where NOT role.id = :roleId order by u.id asc")
	List<User> findUserByRoleNotEqualToCustomer(@Param("roleId") Long roleId);
	
	public List<User> findByAssignedTo_Code(String assignedToUserCode);
	public List<User> findByAssignedTo_CodeAndStatus(String assignedToUserCode,Status status);
	
	public List<User> findByIsRouteAssigned(boolean isRouteAssigned);
	
	public List<User> findByStatusAndIsRouteAssigned(Status status,boolean isRouteAssigned);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND u.lastPendingDues > :lastPendingDues AND u.isRouteAssigned =:isRouteAssigned order by u.id asc")
	Page<User> findUserByRoleIsRouteAssignAndLastPendingDues(@Param("roleId") Long roleId ,@Param("lastPendingDues") double lastPendingDues,@Param("isRouteAssigned") Boolean isRouteAssigned, Pageable pageable);
	
	public List<User> findByReferralCode(String referralCode);
	
	@Query("select distinct u from User u INNER JOIN u.userRoles role where role.id = :roleId AND NOT u.status = :status order by u.id asc")
	List<User> findUserByRoleAndStatusNotEqualToActive(@Param("roleId") Long roleId ,@Param("status") Status status);
	
}

