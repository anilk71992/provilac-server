package com.vishwakarma.provilac.repository;

import com.vishwakarma.provilac.model.Country;


public interface CountryRepository extends CustomRepository<Country, Long>{

	public Country findByCode(String code);
	
	public Country findByName(String name);
}

