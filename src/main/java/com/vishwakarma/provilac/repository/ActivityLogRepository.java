package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.model.ActivityLog;

public interface ActivityLogRepository extends JpaRepository<ActivityLog, Long>,JpaSpecificationExecutor<ActivityLog> {
	
	List<ActivityLog> findByEntityIdAndChangeStatusNotNull(Long entityId);

	@Modifying
	@Transactional
	@Query("delete from ActivityLog a where a.user.id =:userId")
	public void deleteByUser(@Param("userId") Long userId);
	
}