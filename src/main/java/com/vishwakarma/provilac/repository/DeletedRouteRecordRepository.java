package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.model.DeletedRouteRecord;
import com.vishwakarma.provilac.model.User;

public interface DeletedRouteRecordRepository extends JpaRepository<DeletedRouteRecord, Long>,JpaSpecificationExecutor<DeletedRouteRecord> {
	
	public DeletedRouteRecord findByCustomer_Code(String customerCode) ;
	
	public List<DeletedRouteRecord> findByCustomer(User customer);
	
	@Modifying
	@Transactional
	@Query("delete from DeletedRouteRecord d where d.customer.id =:userId")
	public void deleteAllRecordsForCustomer(@Param("userId") Long customerId); 
	
}

