package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.model.User;

public interface RouteRecordRepository extends JpaRepository<RouteRecord, Long>,JpaSpecificationExecutor<RouteRecord> {
	
	public RouteRecord findByCustomer_Code(String customerCode) ;
	
	public List<RouteRecord> findByCustomer(User customer);
	
	public List<RouteRecord> findByRouteCode(String id);
	
}

