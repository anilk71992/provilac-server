package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>,JpaSpecificationExecutor<Category> {
	
	public Category findByCode(String code);
	
	public Category findByName(String name);
	
}

