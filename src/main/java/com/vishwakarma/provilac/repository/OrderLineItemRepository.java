package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.vishwakarma.provilac.model.OrderLineItem;

@Repository
public interface OrderLineItemRepository extends JpaRepository<OrderLineItem,Long>,JpaSpecificationExecutor<OrderLineItem>{

	public List<OrderLineItem> findByOneTimeOrder_Code(String orderCode);
}
