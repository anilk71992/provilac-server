package com.vishwakarma.provilac.repository;


import org.springframework.data.repository.CrudRepository;

import com.vishwakarma.provilac.model.Role;


public interface RoleRepository extends CrudRepository<Role, Long> {

	public Role findByRole(final String roleName);
	
	
}
