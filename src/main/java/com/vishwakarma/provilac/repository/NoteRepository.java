package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Note;


public interface NoteRepository extends JpaRepository<Note, Long>,JpaSpecificationExecutor<Note> {
	
	List<Note> findByCustomerCode(String code);
}