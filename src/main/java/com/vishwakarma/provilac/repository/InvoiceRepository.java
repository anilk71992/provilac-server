package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long>,JpaSpecificationExecutor<Invoice> {
	
	public Invoice findByCode(String code);
	
	public List<Invoice> findByCustomer_Code(String customerCode);
	public Page<Invoice> findByCustomer_Code(String customerCode,Pageable pageable);
	
	public List<Invoice> findByCustomerIdAndIsPaid(Long customerId,boolean isPaid);
	
}
 
