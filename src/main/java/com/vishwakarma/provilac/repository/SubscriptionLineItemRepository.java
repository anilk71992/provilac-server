package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.SubscriptionLineItem;

public interface SubscriptionLineItemRepository extends JpaRepository<SubscriptionLineItem, Long>,JpaSpecificationExecutor<SubscriptionLineItem> {
	
	
	
}

