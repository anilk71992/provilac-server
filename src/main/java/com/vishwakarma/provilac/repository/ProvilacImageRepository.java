package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.vishwakarma.provilac.model.ProvilacImage;

@Repository
public interface ProvilacImageRepository extends JpaRepository<ProvilacImage, Long> ,JpaSpecificationExecutor<ProvilacImage> {

}
