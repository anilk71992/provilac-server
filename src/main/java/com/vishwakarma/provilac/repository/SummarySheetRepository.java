package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.SummarySheet;

public interface SummarySheetRepository extends JpaRepository<SummarySheet, Long>,JpaSpecificationExecutor<SummarySheet> {
	
	public SummarySheet findByCode(String code);
	
}

