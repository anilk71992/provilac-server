package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Deposit;

public interface DepositRepository extends JpaRepository<Deposit, Long>,JpaSpecificationExecutor<Deposit> {
	
	public List<Deposit> findByCollectionBoy_Code(String code);
	
	public Deposit findByCode(String code);
	
	public Page<Deposit> findByCollectionBoy_Code(String code,Pageable pageable);
}

