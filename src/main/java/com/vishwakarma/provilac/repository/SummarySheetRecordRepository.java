package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.SummarySheetRecord;

public interface SummarySheetRecordRepository extends JpaRepository<SummarySheetRecord, Long>,JpaSpecificationExecutor<SummarySheetRecord> {
	
	public SummarySheetRecord findByCode(String code);
}

