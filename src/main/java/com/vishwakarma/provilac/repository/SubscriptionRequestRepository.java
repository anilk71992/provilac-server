package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.model.SubscriptionRequest;

public interface SubscriptionRequestRepository extends JpaRepository<SubscriptionRequest, Long>,JpaSpecificationExecutor<SubscriptionRequest> {
	
	public SubscriptionRequest findByCode(String code);
	
	public SubscriptionRequest findByUser_MobileNumber(String mobileNumber);
	
	public SubscriptionRequest findByUser_CodeAndIsCurrent(String code ,boolean isCurrent);
	
	public List<SubscriptionRequest> findByIsCurrent(boolean isCurrent);
	
	public Page<SubscriptionRequest> findByIsSubscriptionRequest(boolean isSubscriptionRequest,Pageable pageable);	

	public List<SubscriptionRequest> findByUser_Code(String code);

	@Modifying
	@Transactional
	@Query("delete from SubscriptionRequest s where s.user.id =:userId")
	public void deleteByUser(@Param("userId") Long userId);

	public Page<SubscriptionRequest> findByIsChangeRequest(boolean isChangeRequest, Pageable pageable);
	
	@Query("select count(s) from SubscriptionRequest s where s.isChangeRequest =:isChangeRequest")
	public Long getCountForRequestType(@Param("isChangeRequest") boolean isChangeRequest);
}

