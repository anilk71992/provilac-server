package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.UserRoute;

public interface UserRouteRepository extends JpaRepository<UserRoute, Long>,JpaSpecificationExecutor<UserRoute> {
	
	public UserRoute findByRoute_Name(String name);
	
	public UserRoute findByDeliveryBoy_CodeAndRoute_name(String code,String name);
	
	public List<UserRoute> findByDeliveryBoy_Code(String code);
	public List<UserRoute> findByCollectionBoy_Id(Long id);
	
	public UserRoute findByCollectionBoy_IdAndRoute_Code(Long id,String routeCode);
	 
}

