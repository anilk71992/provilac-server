package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;

@Repository
public interface PrepaySubscriptionRequestRepository extends JpaRepository<PrepaySubscriptionRequest, Long>,JpaSpecificationExecutor<PrepaySubscriptionRequest> {
	
	public PrepaySubscriptionRequest findByCode(String code);
	
	public PrepaySubscriptionRequest findByUser_MobileNumber(String mobileNumber);
	
	public List<PrepaySubscriptionRequest> findByUser_Code(String code);

	@Modifying
	@Transactional
	@Query("delete from PrepaySubscriptionRequest s where s.user.id =:userId")
	public void deleteByUser(@Param("userId") Long userId);

	public Page<PrepaySubscriptionRequest> findByIsChangeRequest(boolean isChangeRequest, Pageable pageable);
	
	@Query("select count(s) from PrepaySubscriptionRequest s where s.isChangeRequest =:isChangeRequest")
	public Long getCountForRequestType(@Param("isChangeRequest") boolean isChangeRequest);
}

