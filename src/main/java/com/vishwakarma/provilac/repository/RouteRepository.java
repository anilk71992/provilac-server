package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Route;

public interface RouteRepository extends JpaRepository<Route, Long>,JpaSpecificationExecutor<Route> {
	
	public Route findByName(String name);
	
	public Route findByCode(String code);
	
}

