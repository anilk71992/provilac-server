package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Locality;


public interface LocalityRepository extends CustomRepository<Locality, Long>,JpaSpecificationExecutor<Locality> {

public Locality findByCode(String code);

public Locality findByNameAndCity_Code(String name, String cityCode);

public Locality findByName(String name);

public List<Locality> findByCity_Id(long cityId);

}
