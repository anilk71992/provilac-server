package com.vishwakarma.provilac.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.UserSubscription;

public interface UserSubscriptionRepository extends JpaRepository<UserSubscription, Long>,JpaSpecificationExecutor<UserSubscription> {
	
	public UserSubscription findByCode(String code);
	
	public UserSubscription findByUser_MobileNumber(String mobileNumber);
	
	public UserSubscription findByUser_CodeAndStartDate(String code, Date startDate);
	
	public UserSubscription findByUser_CodeAndIsCurrent(String code, boolean isCurrent);
	
	public List<UserSubscription> findByIsCurrent(boolean isCurrent);
	
	public List<UserSubscription> findByIsCurrentAndUser_Code(boolean isCurrent,String code);
}

