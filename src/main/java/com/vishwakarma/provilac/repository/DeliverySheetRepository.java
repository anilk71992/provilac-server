package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.DeliverySheet;

public interface DeliverySheetRepository extends JpaRepository<DeliverySheet, Long>,JpaSpecificationExecutor<DeliverySheet> {
	public DeliverySheet findByCode(String Code);
	public DeliverySheet findByUserRoute_Code(String userRouteCode);
}

