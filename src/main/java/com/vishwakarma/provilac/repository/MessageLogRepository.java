package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.provilac.model.MessageLog;

public interface MessageLogRepository extends JpaRepository<MessageLog, Long> {

}

