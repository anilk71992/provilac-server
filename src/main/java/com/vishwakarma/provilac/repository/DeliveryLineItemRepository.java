package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.DeliveryLineItem;

public interface DeliveryLineItemRepository extends JpaRepository<DeliveryLineItem, Long>,JpaSpecificationExecutor<DeliveryLineItem> {
	
	public DeliveryLineItem findByCode(String code);
	public List<DeliveryLineItem> findByDeliveryScheduleId(long scheduleId);
	public List<DeliveryLineItem> findByDeliveryScheduleIdIn(List<Long> scheduleIds);
}

