package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>,JpaSpecificationExecutor<Product> {
	
	public Product findByCode(String code);
	
	public Product findByName(String name);
	
	public Product findByNameAndCategory(String name,Category category);
	
	List<Product> findByIsAvailableSubscription(boolean isAvailableSubscription);
	
	List<Product> findByCategory_Code(String code);
	
	List<Product> findByIsAvailableOneTimeOrderAndCategory_Code(boolean isAvailableOneTimeOrder, String code);
	
	public Page<Product> findByCategory_Code(String code, Pageable pageable);
	
}

