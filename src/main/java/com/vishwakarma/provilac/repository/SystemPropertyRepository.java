package com.vishwakarma.provilac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vishwakarma.provilac.model.SystemProperty;

public interface SystemPropertyRepository extends JpaRepository<SystemProperty, Long> {
	public SystemProperty findByPropName(String propName);
}

