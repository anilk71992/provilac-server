package com.vishwakarma.provilac.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.vishwakarma.provilac.model.Collection;

public interface CollectionRepository extends JpaRepository<Collection, Long>,JpaSpecificationExecutor<Collection> {
	
	public List<Collection> findByCollectionBoy_Code(String code);
	
	public Collection findByCode(String code);
	
	public Page<Collection> findByCollectionBoy_Code(String code,Pageable pageable);
}

