package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.repository.PaymentRepository;

public class PaymentPropertyEditor extends PropertyEditorSupport{

	private PaymentRepository paymentRepository;
	
	private Logger log = LoggerFactory.getLogger(PaymentPropertyEditor.class);
	
	public PaymentPropertyEditor(PaymentRepository paymentRepository) {
		this.paymentRepository = paymentRepository;
	}
	
	@Override
	public String getAsText() {
		Payment obj = (Payment) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Payment payment = paymentRepository.findOne(id);
			
			if(null != payment) {

				super.setValue(payment);
			} else {
				log.error("Binding Error:Can not find Payment with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Payment with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Payment with id - " + text);
		}
	}

	
}
