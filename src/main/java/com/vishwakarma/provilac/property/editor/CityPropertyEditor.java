package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.repository.CityRepository;


public class CityPropertyEditor extends PropertyEditorSupport{

	private CityRepository cityRepository;
	
	private Logger log = LoggerFactory.getLogger(CityPropertyEditor.class);
	
	public CityPropertyEditor(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}
	
	@Override
	public String getAsText() {
		City obj = (City) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			City city = cityRepository.findOne(id);
			
			if(null != city) {

				super.setValue(city);
			} else {
				log.error("Binding Error:Can not find City with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find City with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
		}
	}

	
}
