package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.DeliverySheet;
import com.vishwakarma.provilac.repository.DeliverySheetRepository;

public class DeliverySheetPropertyEditor extends PropertyEditorSupport{

	private DeliverySheetRepository deliverySheetRepository;
	
	private Logger log = LoggerFactory.getLogger(DeliverySheetPropertyEditor.class);
	
	public DeliverySheetPropertyEditor(DeliverySheetRepository deliverySheetRepository) {
		this.deliverySheetRepository = deliverySheetRepository;
	}
	
	@Override
	public String getAsText() {
		DeliverySheet obj = (DeliverySheet) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			DeliverySheet DeliverySheet = deliverySheetRepository.findOne(id);
			
			if(null != DeliverySheet) {

				super.setValue(DeliverySheet);
			} else {
				log.error("Binding Error:Can not find DeliverySheet with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find DeliverySheet with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find DeliverySheet with id - " + text);
		}
	}

	
}
