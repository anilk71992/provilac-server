package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.repository.RouteRepository;

public class RoutePropertyEditor extends PropertyEditorSupport{

	private RouteRepository routeRepository;
	
	private Logger log = LoggerFactory.getLogger(RoutePropertyEditor.class);
	
	public RoutePropertyEditor(RouteRepository routeRepository) {
		this.routeRepository = routeRepository;
	}
	
	@Override
	public String getAsText() {
		Route obj = (Route) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Route Route = routeRepository.findOne(id);
			
			if(null != Route) {

				super.setValue(Route);
			} else {
				log.error("Binding Error:Can not find Route with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Route with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Route with id - " + text);
		}
	}

	
}
