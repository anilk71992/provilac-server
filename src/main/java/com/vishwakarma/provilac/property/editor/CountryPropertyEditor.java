package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Country;
import com.vishwakarma.provilac.repository.CountryRepository;


public class CountryPropertyEditor extends PropertyEditorSupport{

	private CountryRepository countryRepository;
	
	private Logger log = LoggerFactory.getLogger(CountryPropertyEditor.class);
	
	public CountryPropertyEditor(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}
	
	@Override
	public String getAsText() {
		Country obj = (Country) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Country Country = countryRepository.findOne(id);
			
			if(null != Country) {

				super.setValue(Country);
			} else {
				log.error("Binding Error:Can not find Country with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Country with id - " + text);
		}
	}

	
}
