package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.repository.InvoiceRepository;

public class InvoicePropertyEditor extends PropertyEditorSupport{

	private InvoiceRepository invoiceRepository;
	
	private Logger log = LoggerFactory.getLogger(InvoicePropertyEditor.class);
	
	public InvoicePropertyEditor(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}
	
	@Override
	public String getAsText() {
		Invoice obj = (Invoice) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Invoice Invoice = invoiceRepository.findOne(id);
			
			if(null != Invoice) {

				super.setValue(Invoice);
			} else {
				log.error("Binding Error:Can not find Invoice with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Invoice with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Invoice with id - " + text);
		}
	}

	
}
