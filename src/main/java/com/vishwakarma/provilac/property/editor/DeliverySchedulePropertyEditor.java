package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.repository.DeliveryScheduleRepository;

public class DeliverySchedulePropertyEditor extends PropertyEditorSupport{

	private DeliveryScheduleRepository deliveryScheduleRepository;
	
	private Logger log = LoggerFactory.getLogger(DeliverySchedulePropertyEditor.class);
	
	public DeliverySchedulePropertyEditor(DeliveryScheduleRepository deliveryScheduleRepository) {
		this.deliveryScheduleRepository = deliveryScheduleRepository;
	}
	
	@Override
	public String getAsText() {
		DeliverySchedule obj = (DeliverySchedule) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			DeliverySchedule DeliverySchedule = deliveryScheduleRepository.findOne(id);
			
			if(null != DeliverySchedule) {

				super.setValue(DeliverySchedule);
			} else {
				log.error("Binding Error:Can not find DeliverySchedule with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find DeliverySchedule with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find DeliverySchedule with id - " + text);
		}
	}

	
}
