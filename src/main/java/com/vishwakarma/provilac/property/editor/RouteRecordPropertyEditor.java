package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.repository.RouteRecordRepository;

public class RouteRecordPropertyEditor extends PropertyEditorSupport{

	private RouteRecordRepository routeRecordRepository;
	
	private Logger log = LoggerFactory.getLogger(RouteRecordPropertyEditor.class);
	
	public RouteRecordPropertyEditor(RouteRecordRepository routeRecordRepository) {
		this.routeRecordRepository = routeRecordRepository;
	}
	
	@Override
	public String getAsText() {
		RouteRecord obj = (RouteRecord) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			RouteRecord routeRecord = routeRecordRepository.findOne(id);
			
			if(null != routeRecord) {

				super.setValue(routeRecord);
			} else {
				log.error("Binding Error:Can not find RouteRecord with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find RouteRecord with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find RouteRecord with id - " + text);
		}
	}

	
}
