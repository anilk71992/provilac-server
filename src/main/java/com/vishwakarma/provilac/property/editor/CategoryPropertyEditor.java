package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.repository.CategoryRepository;

public class CategoryPropertyEditor extends PropertyEditorSupport{

	private CategoryRepository categoryRepository;
	
	private Logger log = LoggerFactory.getLogger(CategoryPropertyEditor.class);
	
	public CategoryPropertyEditor(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
	
	@Override
	public String getAsText() {
		Category obj = (Category) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Category Category = categoryRepository.findOne(id);
			
			if(null != Category) {

				super.setValue(Category);
			} else {
				log.error("Binding Error:Can not find Category with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Category with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Category with id - " + text);
		}
	}

	
}
