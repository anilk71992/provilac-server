package com.vishwakarma.provilac.property.editor;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Locality;
import com.vishwakarma.provilac.repository.LocalityRepository;


/**
 * 
 * @author Rohit
 *
 */
public class LocalityPropertyEditor extends PropertyEditorSupport{

	private LocalityRepository localityRepository;
	
	private Logger log = LoggerFactory.getLogger(LocalityPropertyEditor.class);
	
	public LocalityPropertyEditor(LocalityRepository localityRepository) {
		this.localityRepository = localityRepository;
	}
	
	@Override
	public String getAsText() {
		Locality obj = (Locality) getValue();
		if(obj == null) {
			return "";
		}
		return obj.toString();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Long id = Long.parseLong(text);
			if(id < 0) {
				super.setValue(null);
				return;
			}
			Locality locality = localityRepository.findOne(id);
			
			if(null != locality) {

				super.setValue(locality);
			} else {
				log.error("Binding Error:Can not find Locality with Id - " + text);
				throw new IllegalArgumentException("Binding Error:Can not find Locality with id - " + text);
			}
		} catch (NumberFormatException e) {
			log.error("Binding Error:Invalid Id - " + text);
			throw new IllegalArgumentException("Binding Error:Can not find Locality with id - " + text);
		}
	}

	
}
