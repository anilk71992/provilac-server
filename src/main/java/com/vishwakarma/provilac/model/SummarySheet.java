package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="SummarySheet",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class SummarySheet extends BaseEntity{

	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	
	@OneToMany(mappedBy="summarySheet", orphanRemoval=true, fetch= FetchType.EAGER)
	@OrderBy("id ASC")
	private Set<SummarySheetRecord> summarySheetRecords = new HashSet<SummarySheetRecord>();

	@PostPersist
	public void populateCode() {
		setCode("SS-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Set<SummarySheetRecord> getSummarySheetRecords() {
		return summarySheetRecords;
	}

	public void setSummarySheetRecords(Set<SummarySheetRecord> summarySheetRecords) {
		this.summarySheetRecords = summarySheetRecords;
	}
	
	
}
