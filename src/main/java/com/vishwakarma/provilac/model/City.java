package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name="City",uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class City extends BaseEntity{

	private String code;
	
	@NotEmpty
	@NotNull
	private String name;
	
	private String state;
	
	@ManyToOne
	@JoinColumn(name="countryId",nullable=false,referencedColumnName="id")
	private Country country;


	@PostPersist
	public void populateCode(){
		setCode("CT-"+getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	
	
	
}
