/**
 * 
 */
package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Vishal
 *
 */
@Entity
@Table(name="OneTimeOrder", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class OneTimeOrder extends BaseEntity {

	public static enum OrderStatus {
		Delivered,
		NotDelivered
	}
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="customerId",nullable= false,referencedColumnName="id")
	private User customer;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date, deliveryTime;
	
	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	
	private String instructions;
	
	@ManyToOne
	@JoinColumn(name="deliveryBoyId", nullable=true, referencedColumnName="id")
	private User deliveryBoy;
	
	@NotNull
	@NotEmpty
	private String txnId,paymentOrderId;
	
	private double totalAmount = 0, pointsDiscount = 0, finalAmount = 0;
	
	private int pointsRedemed;
	
	private String paymentGateway;
	
	@OneToMany(mappedBy="oneTimeOrder", fetch=FetchType.EAGER, orphanRemoval=true)
	private Set<OrderLineItem> orderLineItems = new HashSet<OrderLineItem>();
	
	@ManyToOne
	@JoinColumn(name="addressId", nullable=false, referencedColumnName="id")
	private Address address;
	
	private int priority;
	
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isApprovedByPG=true;

	@PostPersist
	public void populateCode() {
		setCode("OT-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public User getDeliveryBoy() {
		return deliveryBoy;
	}

	public void setDeliveryBoy(User deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getPointsDiscount() {
		return pointsDiscount;
	}

	public void setPointsDiscount(double pointsDiscount) {
		this.pointsDiscount = pointsDiscount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public int getPointsRedemed() {
		return pointsRedemed;
	}

	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public Set<OrderLineItem> getOrderLineItems() {
		return orderLineItems;
	}

	public void setOrderLineItems(Set<OrderLineItem> orderLineItems) {
		this.orderLineItems = orderLineItems;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getPaymentOrderId() {
		return paymentOrderId;
	}

	public void setPaymentOrderId(String paymentOrderId) {
		this.paymentOrderId = paymentOrderId;
	}

	public boolean isApprovedByPG() {
		return isApprovedByPG;
	}

	public void setApprovedByPG(boolean isApprovedByPG) {
		this.isApprovedByPG = isApprovedByPG;
	}
	
	
}
