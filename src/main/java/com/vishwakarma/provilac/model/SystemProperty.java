package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity(name="SystemProperty")
@Table(name="SystemProperty")
public class SystemProperty   {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@NotEmpty
	private String propName;
	
	private String propValue;

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public String getPropValue() {
		return propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}
	
	public static final String MASTER_DATA_VERSION = "MASTER_DATA_VERSION";
	public static final String DEFAULT_TIMEZONE = "DEFAULT_TIMEZONE";
	public static final String API_KEY = "API_KEY";
	public static final String ORG_NAME = "ORG_NAME";
	public static final String VERSION_CODE = "VERSION_CODE";
	public static final String VERSION_NAME = "VERSION_NAME";
	public static final String IS_MANDATORY_UPDATE = "IS_MANDATORY_UPDATE";
	public static final String POINT_VALUE = "POINT_VALUE";
	public static final String REFERRER_POINTS = "REFERRER_POINTS";
	public static final String REFERRED_TO_POINTS = "REFERRED_TO_POINTS";
	//PAYTM details 
	public static final String PAYTM_MID = "PAYTM_MID";
	public static final String PAYTM_WEBSITE  =  "PAYTM_WEBSITE";
	public static final String PAYTM_MERCHANT_KEY = "PAYTM_MERCHANT_KEY";
	public static final String PAYTM_CHANNEL_ID = "PAYTM_CHANNEL_ID";
	public static final String PAYTM_INDUSTRY_TYPE_ID = "PAYTM_INDUSTRY_TYPE_ID";
	//Cashback Related
	public static final String MIN_BALANCE_FOR_CASHBACK = "MIN_BALANCE_FOR_CASHBACK";
	public static final String CASHBACK_PECENTAGE = "CASHBACK_PECENTAGE";
	//PayU Details
	public static final String PAYUMONEY_SALT = "PAYUMONEY_SALT";
	public static final String PAYUMONEY_KEY = "PAYUMONEY_KEY";
	public static final String PAYUMONEY_MERCHANT_ID = "PAYUMONEY_MERCHANT_ID";
}
