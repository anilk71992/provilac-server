package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name="Country", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class Country extends BaseEntity{

	private String code;
	
	@NotNull
	@NotEmpty
	private String name;
	
	@PostPersist
	public void populateCode(){
		setCode("C-"+getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
