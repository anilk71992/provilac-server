package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/*
 * @author Reshma
 */
@Entity
@Table(name="Notification")
public class Notification extends BaseEntity {

	public static enum NotificationType{
		PUSH,
		SMS,
		EMAIL
	}
	
	private String code;
	
	private String smsMessage;
	
	private String emailSubject, mailchimpCampaignId;
	
	private String pushMessage, pushTitle;

	@ManyToMany
	@JoinTable(name="NotificationRoute", 
		joinColumns=@JoinColumn(name="notificationId", nullable=false, referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="routeId", nullable=false, referencedColumnName="id"))
	private Set<Route> routes = new HashSet<Route>();
	
	@ElementCollection
	@Enumerated(EnumType.STRING)
	private Set<NotificationType> notificationTypes ;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<String> status ;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@PostPersist
	public void populateCode(){
		setCode("N-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getMailchimpCampaignId() {
		return mailchimpCampaignId;
	}

	public void setMailchimpCampaignId(String mailchimpCampaignId) {
		this.mailchimpCampaignId = mailchimpCampaignId;
	}

	public String getPushMessage() {
		return pushMessage;
	}

	public void setPushMessage(String pushMessage) {
		this.pushMessage = pushMessage;
	}
	
	public String getPushTitle() {
		return pushTitle;
	}

	public void setPushTitle(String pushTitle) {
		this.pushTitle = pushTitle;
	}
	
	public Set<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(Set<Route> routes) {
		this.routes = routes;
	}

	public Set<NotificationType> getNotificationTypes() {
		return notificationTypes;
	}

	public void setNotificationTypes(Set<NotificationType> notificationTypes) {
		this.notificationTypes = notificationTypes;
	}

	public Set<String> getStatus() {
		return status;
	}

	public void setStatus(Set<String> status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
