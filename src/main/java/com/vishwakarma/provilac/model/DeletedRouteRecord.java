package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Harshal Patil
 *
 */
@Entity
@Table(name="DeletedRouteRecord",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class DeletedRouteRecord extends BaseEntity{

	private String code;
	
	@ManyToOne
	@JoinColumn(name="routeId",nullable=false,referencedColumnName="id")
	private Route route;
	
	@ManyToOne
	@JoinColumn(name="userId",nullable = true,referencedColumnName="id")
	private User customer;

	@PostPersist
	public void populateCode() {
		setCode("DRR-" + getId());
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof DeletedRouteRecord)) {
			return false;
		}
		DeletedRouteRecord routeRecord = (DeletedRouteRecord) obj;
		if(null == this.getId() || null == routeRecord.getId()) {
			return super.equals(obj);
		}
		return this.getId().equals(routeRecord.getId());
	}
	
	@Override
	public int hashCode() {
		
		if(null == this.getId()) {
			return super.hashCode();
		}
		return this.getId().hashCode();
	}
	
	
	
}
