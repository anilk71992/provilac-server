package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="Invoice",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Invoice  extends BaseEntity{
	
	public static enum CollectionStatus{
		Collected,
		Not_Collected
	}
	
	private String code;
	
	@Temporal(TemporalType.DATE)
	private Date fromDate, toDate;
	
	private double totalAmount, lastPendingDues;
	
	//TODO: String->double
	private double provilacOutStanding;
	
	private String collectionBoyNote,reason;
	
	@Enumerated(EnumType.STRING)
	private CollectionStatus collectionStatus;
	
	@ManyToOne
	@JoinColumn(name="customerId",nullable= false,referencedColumnName="id")
	private User customer;
	
	@OneToMany(mappedBy="invoice")
	private Set<DeliverySchedule> deliverySchedule = new HashSet<DeliverySchedule>();
	
	//TODO take isPaid Flag
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isPaid;

	@PostPersist
	public void populateCode() {
		setCode("I-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getLastPendingDues() {
		return lastPendingDues;
	}

	public void setLastPendingDues(double lastPendingDues) {
		this.lastPendingDues = lastPendingDues;
	}

	public double getProvilacOutStanding() {
		return provilacOutStanding;
	}
	public void setProvilacOutStanding(double provilacOutStanding) {
		this.provilacOutStanding = provilacOutStanding;
	}
	
	public String getCollectionBoyNote() {
		return collectionBoyNote;
	}

	public void setCollectionBoyNote(String collectionBoyNote) {
		this.collectionBoyNote = collectionBoyNote;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public Set<DeliverySchedule> getDeliverySchedule() {
		return deliverySchedule;
	}

	public void setDeliverySchedule(Set<DeliverySchedule> deliverySchedule) {
		this.deliverySchedule = deliverySchedule;
	}

	public boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public CollectionStatus getCollectionStatus() {
		return collectionStatus;
	}

	public void setCollectionStatus(CollectionStatus collectionStatus) {
		this.collectionStatus = collectionStatus;
	}


	
	

}
