package com.vishwakarma.provilac.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="Product",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Product extends BaseEntity{
	
	//TODO: Remove Category, and take it as separate model
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String name;
	
	private double price, tax;
	
	private String colorCode;
	
	@Column(columnDefinition="MediumText")
	private String description;  
	
	@ManyToOne
	@JoinColumn(name="categoryId",nullable=false,referencedColumnName="id")
	private Category category;
	
	private String productPicUrl;
	
	@Transient
	private byte[] picData;
	
	@Transient
	private String tempFileName;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isAvailableSubscription, isAvailableOneTimeOrder;

	private String productBannerUrl, productCartPicUrl, productListPicUrl, productDetailsPicUrl;
	
	@ElementCollection(fetch=FetchType.LAZY)
	private Map<Integer, Double> pricingMap = new HashMap<Integer, Double>();
	
	@Transient
	private byte[] bannerData, productCartPicData, productListPicData, productDetailsPicData;
	
	@Transient
	private String bannerFileName, productCartPicFileName, productListPicFileName, productDetailsPicFileName;
	
	
	@PostPersist
	public void populateCode() {
		setCode("PR-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductPicUrl() {
		return productPicUrl;
	}

	public void setProductPicUrl(String productPicUrl) {
		this.productPicUrl = productPicUrl;
	}

	public byte[] getPicData() {
		return picData;
	}

	public void setPicData(byte[] picData) {
		this.picData = picData;
	}

	public String getTempFileName() {
		return tempFileName;
	}

	public void setTempFileName(String tempFileName) {
		this.tempFileName = tempFileName;
	}

	public boolean isAvailableSubscription() {
		return isAvailableSubscription;
	}

	public void setAvailableSubscription(boolean isAvailableSubscription) {
		this.isAvailableSubscription = isAvailableSubscription;
	}

	public boolean isAvailableOneTimeOrder() {
		return isAvailableOneTimeOrder;
	}

	public void setAvailableOneTimeOrder(boolean isAvailableOneTimeOrder) {
		this.isAvailableOneTimeOrder = isAvailableOneTimeOrder;
	}

	public String getProductBannerUrl() {
		return productBannerUrl;
	}

	public void setProductBannerUrl(String productBannerUrl) {
		this.productBannerUrl = productBannerUrl;
	}

	public Map<Integer, Double> getPricingMap() {
		return pricingMap;
	}

	public void setPricingMap(Map<Integer, Double> pricingMap) {
		this.pricingMap = pricingMap;
	}

	public byte[] getBannerData() {
		return bannerData;
	}

	public void setBannerData(byte[] bannerData) {
		this.bannerData = bannerData;
	}

	public String getBannerFileName() {
		return bannerFileName;
	}

	public void setBannerFileName(String bannerFileName) {
		this.bannerFileName = bannerFileName;
	}

	public String getProductCartPicUrl() {
		return productCartPicUrl;
	}

	public String getProductListPicUrl() {
		return productListPicUrl;
	}

	public String getProductDetailsPicUrl() {
		return productDetailsPicUrl;
	}

	public byte[] getProductCartPicData() {
		return productCartPicData;
	}

	public byte[] getProductListPicData() {
		return productListPicData;
	}

	public byte[] getProductDetailsPicData() {
		return productDetailsPicData;
	}

	public String getProductCartPicFileName() {
		return productCartPicFileName;
	}

	public String getProductListPicFileName() {
		return productListPicFileName;
	}

	public String getProductDetailsPicFileName() {
		return productDetailsPicFileName;
	}

	public void setProductCartPicUrl(String productCartPicUrl) {
		this.productCartPicUrl = productCartPicUrl;
	}

	public void setProductListPicUrl(String productListPicUrl) {
		this.productListPicUrl = productListPicUrl;
	}

	public void setProductDetailsPicUrl(String productDetailsPicUrl) {
		this.productDetailsPicUrl = productDetailsPicUrl;
	}

	public void setProductCartPicData(byte[] productCartPicData) {
		this.productCartPicData = productCartPicData;
	}

	public void setProductListPicData(byte[] productListPicData) {
		this.productListPicData = productListPicData;
	}

	public void setProductDetailsPicData(byte[] productDetailsPicData) {
		this.productDetailsPicData = productDetailsPicData;
	}

	public void setProductCartPicFileName(String productCartPicFileName) {
		this.productCartPicFileName = productCartPicFileName;
	}

	public void setProductListPicFileName(String productListPicFileName) {
		this.productListPicFileName = productListPicFileName;
	}

	public void setProductDetailsPicFileName(String productDetailsPicFileName) {
		this.productDetailsPicFileName = productDetailsPicFileName;
	}
	
	 
}
