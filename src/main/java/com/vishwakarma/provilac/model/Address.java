package com.vishwakarma.provilac.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
/**
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name="Address", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class Address extends BaseEntity {

	private String code;
	
	@NotNull
	@NotEmpty
	private String addressLine1,name;
	
	private String addressLine2, landmark,locality,pincode;
	
	private Double lat, lng;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isDefault;
	
	@ManyToOne
	@JoinColumn(name="customerId", nullable=false, referencedColumnName="id")
	private User customer;
	
	@ManyToOne
	@JoinColumn(name="cityId", nullable=false, referencedColumnName="id")
	private City city;

	
	@PostPersist
	public void populateCode() {
		setCode("A-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	public String getFullAddress() {
		
		String address = addressLine1;
		
		if(StringUtils.isNotBlank(addressLine2) && !addressLine2.startsWith("JSON"))
			address = address + ", " + addressLine2;
		
		if(StringUtils.isNotBlank(landmark) && !landmark.startsWith("JSON"))
			address = address + ", " + landmark;
		
		if(StringUtils.isNotBlank(locality) && !locality.startsWith("JSON"))
			address = address + ", " + locality;
		
		address = address + ", " + city.getName();
		
		if(StringUtils.isNotBlank(pincode) && !pincode.startsWith("JSON")) 
			address = address + "-" + pincode;
			
		return address;
	}
}
