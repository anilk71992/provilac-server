package com.vishwakarma.provilac.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.vishwakarma.provilac.model.Payment.PaymentMethod;

@Entity
@Table(name="SummarySheetRecord",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class SummarySheetRecord extends BaseEntity {
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="customerId",nullable=false,referencedColumnName="id")
	private User customer;
	
	@ManyToOne
	@JoinColumn(name="summarySheetId",nullable=false,referencedColumnName="id")
	private SummarySheet summarySheet;
	
	
	@ManyToOne
	@JoinColumn(name="invoiceId",nullable= true,referencedColumnName="id")
	private Invoice invoice;

	private double previousMonth;
	
	private double adjustment;
	
	@Enumerated(EnumType.STRING)
	private PaymentMethod paymentMethod;
	
	private String chequeNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date chequeDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedDate;
	
	private String receiptNumber;
	
	private double balanceAmount;
	
	private double totalAmount;
	
	@ManyToOne
	@JoinColumn(name="receivedById",nullable=true,referencedColumnName="id")
	private User receivedBy;
	
	@PostPersist
	public void populateCode() {
		setCode("SS-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public SummarySheet getSummarySheet() {
		return summarySheet;
	}

	public void setSummarySheet(SummarySheet summarySheet) {
		this.summarySheet = summarySheet;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public double getPreviousMonth() {
		return previousMonth;
	}

	public void setPreviousMonth(double previousMonth) {
		this.previousMonth = previousMonth;
	}

	public double getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(double adjustment) {
		this.adjustment = adjustment;
	}


	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public User getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(User receivedBy) {
		this.receivedBy = receivedBy;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
