package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Vishal
 *
 */
@Entity
@Table(name="OrderLineItem", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class OrderLineItem extends BaseEntity {
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="orderId", nullable=false, referencedColumnName="id")
	private OneTimeOrder oneTimeOrder;
	
	@ManyToOne
	@JoinColumn(name="productId", nullable=false, referencedColumnName="id")
	private Product product;
	
	private int quantity = 0;
	
	private double amount;
	
	@PostPersist
	public void populateCode() {
		setCode("OL-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public OneTimeOrder getOneTimeOrder() {
		return oneTimeOrder;
	}

	public void setOneTimeOrder(OneTimeOrder oneTimeOrder) {
		this.oneTimeOrder = oneTimeOrder;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
