package com.vishwakarma.provilac.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="Note")
public class Note extends BaseEntity {

	private String code;
	
	private String callDetails;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="customerId", nullable=false, referencedColumnName="id")
	private User customer;
	
	
	@PostPersist
	public void populateCode(){
		setCode("NT-"+getId());
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getCallDetails() {
		return callDetails;
	}


	public void setCallDetails(String callDetails) {
		this.callDetails = callDetails;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public User getCustomer() {
		return customer;
	}


	public void setCustomer(User customer) {
		this.customer = customer;
	}
	
}
