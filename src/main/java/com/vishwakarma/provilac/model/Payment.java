package com.vishwakarma.provilac.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="Payment",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Payment extends BaseEntity {

	public static enum PaymentMethod {
		Cash,
		Online,
		Cheque,
		NEFT,
		PAYTM,
		PAYU,
		REFERRAL_CREDIT
		
	}
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String txnId;
	
	private String receiptNo;
	
	private double amount, adjustmentAmount, pointsAmount, cashback;
	
	private int pointsRedemed;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Enumerated(EnumType.STRING)
	private PaymentMethod paymentMethod;
	
	private String chequeNumber, bankName;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isBounced, hasDeposited, isVerified;
	
	@ManyToOne
	@JoinColumn(name="customerId", nullable=false,referencedColumnName="id")
	private User customer;
	
	@ManyToOne
	@JoinColumn(name="userId", nullable=true, referencedColumnName="id")
	private User receivedBy;
	
	@ManyToOne
	@JoinColumn(name="invoiceId", nullable=true, referencedColumnName="id")
	private Invoice invoice;

	@ManyToOne
	@JoinColumn(name="collectionId", nullable=true, referencedColumnName="id")
	private Collection collection;
	
	@ManyToOne
	@JoinColumn(name="depositId", nullable=true, referencedColumnName="id")
	private Deposit deposit;
	
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isApprovedByPG = true;
	
	@PostPersist
	public void populateCode() {
		setCode("P-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	
	public String getReceiptNo() {
		return receiptNo;
	}
	
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public boolean getIsBounced() {
		return isBounced;
	}

	public void setIsBounced(boolean isBounced) {
		this.isBounced = isBounced;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public User getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(User receivedBy) {
		this.receivedBy = receivedBy;
	}
	
	public Invoice getInvoice() {
		return invoice;
	}
	
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	
	public double getAdjustmentAmount() {
		return adjustmentAmount;
	}
	
	public void setAdjustmentAmount(double adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	public boolean getHasDeposited() {
		return hasDeposited;
	}

	public void setHasDeposited(boolean hasDeposited) {
		this.hasDeposited = hasDeposited;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Collection getCollection() {
		return collection;
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	public Deposit getDeposit() {
		return deposit;
	}

	public void setDeposit(Deposit deposit) {
		this.deposit = deposit;
	}

	public double getPointsAmount() {
		return pointsAmount;
	}

	public void setPointsAmount(double pointsAmount) {
		this.pointsAmount = pointsAmount;
	}

	public double getCashback() {
		return cashback;
	}

	public void setCashback(double cashback) {
		this.cashback = cashback;
	}

	public int getPointsRedemed() {
		return pointsRedemed;
	}

	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}

	public boolean isApprovedByPG() {
		return isApprovedByPG;
	}

	public void setApprovedByPG(boolean isApprovedByPG) {
		this.isApprovedByPG = isApprovedByPG;
	}

}
