package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.utils.DateHelper;

public class PaymentSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("firstName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("lastName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("email"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("username"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("mobileNumber"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("txnId"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("receiptNo"), "%" + searchTerm + "%")));
				
				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchByIsBounced(final boolean isBounced) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.equal(item.get("isBounced"), isBounced));
				return predicate;

			}
		};
	}
	
	
	@SuppressWarnings("rawtypes")
	public static Specification searchUndepositedCheques(final boolean hasDeposited,final PaymentMethod paymentMethod) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.equal(item.get("hasDeposited"), hasDeposited));
				predicate.getExpressions().add(builder.equal(item.get("paymentMethod"), paymentMethod));
				return predicate;

			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchUnVerifieddepositedCheques(final boolean hasDeposited,final PaymentMethod paymentMethod, final boolean isVerified) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.equal(item.get("hasDeposited"), hasDeposited));
				predicate.getExpressions().add(builder.equal(item.get("paymentMethod"), paymentMethod));
				predicate.getExpressions().add(builder.equal(item.get("isVerified"), isVerified));
				return predicate;

			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByCustomerAndDateRange(final Long customerId,final Date fromDate,final Date toDate) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), fromDate)));
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), toDate)));
				return predicate;

			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByCustomer(final Long customerId) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
				return predicate;

			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification getPaymentsFromDateToDateAndCollectionBoyIdAndPaymentMethodAndAddFrom(final Long receivedById,final Date date,final PaymentMethod paymentMethod1,final PaymentMethod paymentMethod2,final Long collectionId) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				return builder.and(getPredicateForPaymentMethodCashAndCheque(paymentMethod1,paymentMethod2,item,query,builder),getPredicateForReceivedByAndDate(receivedById,date,collectionId,item,query,builder));
			}
		};
	}
	
	public static Predicate getPredicateForPaymentMethodCashAndCheque(PaymentMethod paymentMethod1,PaymentMethod paymentMethod2,Root item, CriteriaQuery query, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.equal(item.get("paymentMethod"), paymentMethod1));
		predicate.getExpressions().add(builder.equal(item.get("paymentMethod"), paymentMethod2));
		return predicate;

	}
	
	public static Predicate getPredicateForReceivedByAndDate(Long receivedById,Date date,Long collectionId,Root item, CriteriaQuery query, CriteriaBuilder builder) {
		
		Date startOfTheDay = new Date(), endOfTheDay = new Date();
		
		startOfTheDay.setTime(date.getTime());
		endOfTheDay.setTime(date.getTime());
		
		DateHelper.setToStartOfDay(startOfTheDay);
		DateHelper.setToEndOfDay(endOfTheDay);
		Predicate predicate = builder.and();
		predicate.getExpressions().add(builder.and(builder.equal(item.get("receivedBy").get("id"), receivedById)));
		predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
        predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
        predicate.getExpressions().add(builder.and(builder.isNull((item.get("collection")))));
		return predicate;
	}
}
