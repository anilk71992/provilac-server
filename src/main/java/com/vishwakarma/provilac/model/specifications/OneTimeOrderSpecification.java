package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.utils.DateHelper;

public class OneTimeOrderSpecification {

	public static Specification search(final String searchTerm){
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
				// TODO Auto-generated method stub
				Predicate predicate=builder.or();
						predicate.getExpressions().add(builder.or(builder.like(item.get("code"), searchTerm)));
				return predicate;
			}
		
		};
	}
	
	/**
	 * This method will not check for status if status param is null. Do not use this method if you want to 
	 * get orders with null status.
	 * @return
	 */
	public static Specification getByDateRangeAndStatusAndDeliveryBoy(final Date date, final OneTimeOrder.OrderStatus status, final Long deliveryBoyId) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
				
				if(null != date) {
					cb.and(getPredicateForDateRange(date, root, cb));
				}
				
				if(null != status) {
					cb.and(getPredicateForStatus(status, root, cb));
				}
				
				if(null != deliveryBoyId) {
					cb.and(getPredicateForDeliveryBoy(deliveryBoyId, root, cb));
				}
				
				return cb.conjunction();
			}
		};
	}
	
	private static Predicate getPredicateForStatus(OneTimeOrder.OrderStatus status, Root root, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.equal(root.get("status"), status));
		return predicate;
	}
	
	private static Predicate getPredicateForDeliveryBoy(Long deliveryBoyId, Root root, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.equal(root.get("deliveryBoy").get("id"), deliveryBoyId));
		return predicate;
	}
	
	private static Predicate getPredicateForDateRange(Date date, Root root, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		
		/*Date startOfTheDay = new Date(), endOfTheDay = new Date();

		startOfTheDay.setTime(date.getTime());
		endOfTheDay.setTime(date.getTime());
		
		DateHelper.setToStartOfDay(startOfTheDay);
		DateHelper.setToEndOfDay(endOfTheDay);
		*/
		
		//predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(root.get("date"), startOfTheDay)));
		DateHelper.setToStartOfDay(date);
		predicate.getExpressions().add(builder.and(builder.equal(root.get("date"), date)));
        
		return predicate;
	}
	
}
