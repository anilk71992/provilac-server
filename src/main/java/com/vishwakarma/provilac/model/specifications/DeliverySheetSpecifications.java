package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.utils.DateHelper;

public class DeliverySheetSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.or();
                predicate.getExpressions().add(builder.or(builder.like(item.get("code"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("deliveryBoy").get("firstName"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("deliveryBoy").get("lastName"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("deliveryBoy").get("mobileNumber"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("deliveryBoy").get("email"), "%"+searchTerm+"%")));
                
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("collectionBoy").get("firstName"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("collectionBoy").get("lastName"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("collectionBoy").get("mobileNumber"), "%"+searchTerm+"%")));
                predicate.getExpressions().add(builder.or(builder.like(item.get("userRoute").get("collectionBoy").get("email"), "%"+searchTerm+"%")));
 				return predicate;
			}
			
		};
		
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByDate(final Date date) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
				
				return predicate;
				
			}
		};
	}
}
