package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.model.DeliverySchedule;
import com.vishwakarma.provilac.model.Invoice;
import com.vishwakarma.provilac.model.Invoice.CollectionStatus;

public class InvoiceSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("firstName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("lastName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("email"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("username"), "%" + searchTerm + "%")));

				return predicate;
			}

		};
	}


	public static Specification searchForNotCollectedInvoices(final String searchTerm,final CollectionStatus status) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				
				return builder.and(getPredicateByCollectionStatus(status, item, builder),getPredicateBySearchTerm(searchTerm, item, builder));
			}

		};
	}
	
	public static Specification searchByCustomerIdAndDateRange(final Long customerId, final Date fromDate, final Date toDate) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("toDate"), toDate)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("fromDate"), fromDate)));

				return predicate;
			}

		};

	}
	public static Specification searchByDateRange( final Date fromDate, final Date toDate) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("toDate"), toDate)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("fromDate"), fromDate)));

				return predicate;
			}

		};

	}
	
	public static Specification searchByRouteAndDateRangeAndCollectionStatus(final Long routeId,final CollectionStatus status, final Date fromDate, final Date toDate) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				
				return builder.and(getPredicateByRoute(routeId, item, builder, query),getPredicateByCollectionStatusAndDateRange(status, fromDate, toDate, item, builder));
			}

		};
	}
	
	
	public static Predicate getPredicateBySearchTerm(final String searchTerm , Root item , CriteriaBuilder builder){

		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("firstName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("lastName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("email"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("customer").get("mobileNumber"), "%" + searchTerm + "%")));

		return predicate;
	}


	public static Predicate getPredicateByCollectionStatusAndDateRange(final CollectionStatus status, final Date fromDate, final Date toDate ,Root item , CriteriaBuilder builder){

		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.and(builder.equal(item.get("collectionStatus"), status)));
		predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("toDate"), toDate)));
		predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("fromDate"), fromDate)));


		return predicate;
	}


	public static Predicate getPredicateByCollectionStatus(final CollectionStatus status , Root item , CriteriaBuilder builder){

		Predicate predicate = builder.and();
		predicate.getExpressions().add(builder.and(builder.equal(item.get("collectionStatus"), status)));

		return predicate;
	}

	public static Predicate getPredicateByRoute(final Long routeId , Root<Invoice> item , CriteriaBuilder builder,CriteriaQuery<?> query){

		Predicate predicate = builder.and();

		Join<Invoice, DeliverySchedule>schedules = item.join("deliverySchedule");
		predicate.getExpressions().add(builder.and(builder.equal(schedules.get("route").get("id"), routeId)));
		query.distinct(true);

		return predicate;

	}

	public static Specification searchByCustomerId(final Long customerId) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
				return predicate;
			}

		};

	}
}
