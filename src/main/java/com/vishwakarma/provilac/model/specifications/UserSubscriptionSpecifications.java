package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class UserSubscriptionSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("firstName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("lastName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("email"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("username"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("mobileNumber"), "%" + searchTerm + "%")));

				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchByCustomerAndDate(final String customerCode ,final Date date) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
			    predicate.getExpressions().add(builder.and(builder.equal(item.get("user").get("code"), customerCode)));
				predicate.getExpressions().add(builder.and(builder.equal(item.get("startDate"), date)));
				return predicate;
				
			}
		};
	}

	public static Specification searchByCustomermobileNumber(final String mobileNumber) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("user").get("mobileNumber"), mobileNumber)));

				return predicate;
			}

		};

	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByIsCurrentAndAssignedToUSer(final String assignedToCode ,final Boolean isCurrent) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
			    predicate.getExpressions().add(builder.and(builder.equal(item.get("user").get("assignedTo").get("code"), assignedToCode)));
				predicate.getExpressions().add(builder.and(builder.equal(item.get("isCurrent"), isCurrent)));
				return predicate;
				
			}
		};
	}
	@SuppressWarnings("rawtypes")
	public static Specification searchBystartDate(final Boolean isCurrent ,final Date startDate,final Date endDate) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("startDate"), startDate)));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("startDate"), endDate)));
				predicate.getExpressions().add(builder.and(builder.equal(item.get("isCurrent"), isCurrent)));
				return predicate;
				
			}
		};
	}
	
	public static Specification searchByCustomerId(final Long id) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("user").get("id"), id)));

				return predicate;
			}

		};

	}
}
