package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class CollectionSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("collectionBoy").get("firstName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("collectionBoy").get("lastName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("collectionBoy").get("email"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("collectionBoy").get("username"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("collectionBoy").get("mobileNumber"), "%" + searchTerm + "%")));
				
				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchByCollectionBoyAndDateRange(final Long collectionBoyId,final Date fromDate,final Date toDate) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("collectionBoy").get("id"), collectionBoyId)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), fromDate)));
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), toDate)));
				return predicate;

			}
		};
	}
}
