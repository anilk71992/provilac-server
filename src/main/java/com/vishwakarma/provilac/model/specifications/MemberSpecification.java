package com.vishwakarma.provilac.model.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class MemberSpecification {

	public static Specification search(final String searchTerm, final String companyCode) {
		
		return new Specification() {
			
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				
				Predicate searchPredicate = null;
				if(StringUtils.isNotBlank(searchTerm)) {
					searchPredicate = builder.or();
					searchPredicate.getExpressions().add(builder.like(item.get("firstName"), "%" + searchTerm + "%"));
					searchPredicate.getExpressions().add(builder.like(item.get("lastName"), "%" + searchTerm + "%"));
					searchPredicate.getExpressions().add(builder.like(item.get("email"), "%" + searchTerm + "%"));
				}
				
				Predicate companyPredicate = null;
				if(StringUtils.isNotBlank(companyCode)) {
					companyPredicate = builder.or();
					companyPredicate.getExpressions().add(builder.equal(item.get("company").get("code"), companyCode));
				}
				
				query.distinct(true);
				
				if(null != companyPredicate && null != searchPredicate) {
					return builder.and(searchPredicate, companyPredicate);
				} else if(null != companyPredicate) {
					return companyPredicate;
				}
				return searchPredicate;
			}
		};
	}
	
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByManagerId(final Long searchTerm) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.equal(item.get("manager").get("id"), searchTerm));
				return predicate;
			}
		};
	}

}
