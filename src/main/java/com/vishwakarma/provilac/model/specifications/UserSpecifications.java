package com.vishwakarma.provilac.model.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.model.User.Status;

public class UserSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {

			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.like(item.get("code"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("firstName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("fullName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("lastName"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("email"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("username"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("mobileNumber"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("alterNateMobileNumber"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("address"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("buildingAddress"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.like(item.get("referralCode"), "%" + searchTerm + "%")));
				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchBySearchTermAndRole(final String searchTerm, final String role) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

				return builder.and(getPredicateBySearchTerm(searchTerm, item, builder), getPredicateForRole(role, item, builder));
			}

		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchBySearchTermAndNotCustomerRole(final String searchTerm, final String role) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

				
				Predicate searchTermPredicate = null;
				Predicate rolePredicate = null;

				if (null != searchTerm) {
					searchTermPredicate = builder.or();
					searchTermPredicate = getPredicateBySearchTerm(searchTerm, item, builder);
				}

				if (null != role) {
					rolePredicate = builder.or();
					rolePredicate = getPredicateForRole(role, item, builder).not();
				}

				if (null != searchTermPredicate && null != rolePredicate) {
					return builder.and(rolePredicate, searchTermPredicate);
				} else if (null != searchTermPredicate) {
					return searchTermPredicate;
				} else if (null != rolePredicate) {
					return rolePredicate;
				}
				query.distinct(true);
				return builder.and();				
				
			}

		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchBySearchTermAndRoleAndIsRouteAssignedAndStatus(final String searchTerm, final String role, final boolean isRouteAssigned ,final Status status) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

				Predicate searchTermPredicate = null;
				Predicate rolePredicate = null;
				//Predicate isRouteAssignedPredicate = null;
				Predicate statusPredicate = null;

				if (null != searchTerm) {
					searchTermPredicate = builder.or();
					searchTermPredicate = getPredicateBySearchTerm(searchTerm, item, builder);
				}

				if (null != role) {
					rolePredicate = builder.or();
					rolePredicate = getPredicateForRole(role, item, builder);
				}
				
					/*isRouteAssignedPredicate= builder.or();
					isRouteAssignedPredicate = getPredicateByIsRouteAssigned(isRouteAssigned, item, builder);//because in controller is routeAsssigned value set to false only seach false route so.. it remove
*/
				if (null != status) {
					statusPredicate = builder.or();
					statusPredicate = getPredicateForStatus(status, item, builder);
				}
				return builder.and(searchTermPredicate ,rolePredicate ,statusPredicate);	
			}
		};
	}

	public static Specification searchByPhoneNumber(final Long userId, final String searchTerm) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.or(builder.like(item.get("mobileNumber"), "%" + searchTerm + "%")));
				predicate.getExpressions().add(builder.or(builder.notEqual(item.get("id"), userId)));
				return predicate;
			}

		};
	}

	@SuppressWarnings("rawtypes")	
	public static Predicate getPredicateForStatus(Status status, Root root, CriteriaBuilder cb) {
		
		Predicate predicate = cb.or();
		predicate.getExpressions().add(cb.equal(root.get("status"), status));
		return predicate;
	}
	
	/**
	 * Predicate for last pending dues greater than zero
	 * @param lastPendingDues
	 * @param root
	 * @param cb
	 * @return
	 */
	@SuppressWarnings("rawtypes")	
	public static Predicate getPredicateForLastPendingDues(double lastPendingDues, Root root, CriteriaBuilder cb) {
		
		Predicate predicate = cb.or();
		predicate.getExpressions().add(cb.greaterThan(root.get("lastPendingDues"), lastPendingDues));
		return predicate;
	}

	public static Predicate getPredicateByIsRouteAssigned(boolean isRouteAssign , Root root , CriteriaBuilder cb){
		
		Predicate predicate = cb.or();
		predicate.getExpressions().add(cb.equal(root.get("isRouteAssigned"), isRouteAssign));
		return predicate;
	}

	@SuppressWarnings("unchecked")
	public static Predicate getPredicateBySearchTerm(String searchTerm, Root root, CriteriaBuilder builder) {

		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.or(builder.like(root.get("code"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("firstName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("lastName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("email"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("username"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("mobileNumber"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("alterNateMobileNumber"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("address"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("buildingAddress"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(root.get("referralCode"), "%" + searchTerm + "%")));
		return predicate;
	}
	
	public static Specification getPredicateByMobileOrUserNameOrEmail(final String searchTerm) {
		return new Specification() {

			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.or(builder.equal(item.get("email"), searchTerm)));
				predicate.getExpressions().add(builder.or(builder.equal(item.get("username"), searchTerm)));
				predicate.getExpressions().add(builder.or(builder.equal(item.get("mobileNumber"), searchTerm)));
				predicate.getExpressions().add(builder.or(builder.like(item.get("alterNateMobileNumber"), "%" + searchTerm + "%")));
				return predicate;
			}

		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Predicate getPredicateForRole(String role, Root root, CriteriaBuilder cb) {

		Predicate predicate = cb.and();
		Join join = root.joinSet("userRoles");
		predicate.getExpressions().add((join.get("role").in(role)));
		return predicate;
	}
	
	@SuppressWarnings("rawtypes")
	public static Predicate getPredicateForRoleAndStatus(String role,final Status status, Root root, CriteriaBuilder cb) {

		Predicate predicate = cb.and();
		Join join = root.joinSet("userRoles");
		predicate.getExpressions().add((join.get("role").in(role)));
		predicate.getExpressions().add(cb.or(cb.equal(root.get("status"), status)));
		return predicate;
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchCustomersByStatusActiveAndHold(final Status activeStatus,final Status holdStatus,final String role) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				
				return builder.or(getPredicateForRoleAndStatus(role, activeStatus, item, builder),getPredicateForRoleAndStatus(role, holdStatus, item, builder));
			}

		};
	}
	public static Specification searchByStatusNotEqualToNew(final Status status,final String role) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				getPredicateForRole(role, item, builder);
				predicate.getExpressions().add(builder.and(builder.notEqual(item.get("status"), status)));
				
				return predicate;
			}

		};
	}
	
	public static Specification searchByStatusAndCreatedDate(final Status status,final Date fromDate,final Date toDate) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("status"), status)));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("createdDate"), toDate)));
	            predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("createdDate"), fromDate)));
	                
				
				return predicate;
			}

		};
	}
	
	/**
	 * SPecification to get customers having pending dues greater than zero and isRouteAssign false
	 * @param searchTerm
	 * @param role
	 * @param isRouteAssigned
	 * @param lastPendingDues
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Specification searchBySearchTermAndRoleAndIsRouteAssignedAndPendingDues(final String searchTerm, final String role, final boolean isRouteAssigned ,final double lastPendingDues) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {

				Predicate searchTermPredicate = null;
				Predicate rolePredicate = null;
				Predicate isRouteAssignedPredicate = null;
				Predicate statusPredicate = null;

				if (null != searchTerm) {
					searchTermPredicate = builder.or();
					searchTermPredicate = getPredicateBySearchTerm(searchTerm, item, builder);
				}

				if (null != role) {
					rolePredicate = builder.or();
					rolePredicate = getPredicateForRole(role, item, builder);
				}
				
					isRouteAssignedPredicate= builder.or();
					isRouteAssignedPredicate = getPredicateByIsRouteAssigned(isRouteAssigned, item, builder);

					statusPredicate = builder.or();
					statusPredicate = getPredicateForLastPendingDues(lastPendingDues, item, builder);
				return builder.and(searchTermPredicate ,rolePredicate ,isRouteAssignedPredicate,statusPredicate);	
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Predicate getPredicateForRoleAndStatusNotEqual(String role,final Status status, Root root, CriteriaBuilder cb) {

		Predicate predicate = cb.and();
		Join join = root.joinSet("userRoles");
		predicate.getExpressions().add((join.get("role").in(role)));
		predicate.getExpressions().add(cb.or(cb.notEqual(root.get("status"), status)));
		return predicate;
	}
	@SuppressWarnings("rawtypes")
	public static Specification getPredicateForRoleAndStatusNotEqual(final String role,final Status status) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root root, CriteriaQuery query,
					CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				Predicate predicate = cb.and();
				Join join = root.joinSet("userRoles");
				predicate.getExpressions().add((join.get("role").in(role)));
				predicate.getExpressions().add(cb.or(cb.notEqual(root.get("status"), status)));
				return predicate;
			}
		};
	}
}
