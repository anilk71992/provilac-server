package com.vishwakarma.provilac.model.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class AnniversaryLineItemSpecification {

	public static Specification search(final String searchTerm) {
		
		return new Specification() {
			
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				
				Predicate predicate = builder.or();
				return predicate;
			}
		};
	}
	
}
