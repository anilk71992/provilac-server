package com.vishwakarma.provilac.model.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.vishwakarma.provilac.model.DeliverySchedule.DeliveryStatus;
import com.vishwakarma.provilac.model.DeliverySchedule.Type;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;
import com.vishwakarma.provilac.utils.DateHelper;

public class DeliveryScheduleSpecifications {

	public static Specification search(final String searchTerm) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.or();
                predicate.getExpressions().add(builder.or(
                        builder.like(item.get("customer").get("firstName"), "%"+searchTerm+"%")));
                                      
 				return predicate;
			}
			
		};
		
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByDeliveryStatus(final DeliveryStatus status) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.equal(item.get("status"), status));
				return predicate;
				
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByDeliveryType(final  Type type) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				predicate.getExpressions().add(builder.notEqual(item.get("type"), type));
				return predicate;
				
			}
		};
	}
	@SuppressWarnings("rawtypes")
	public static Specification searchByDeliveryStatusAndDate(final DeliveryStatus status,final Date date) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.or();
				
				Date endOfTheDay = new Date();

				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToEndOfDay(endOfTheDay);
				
				predicate.getExpressions().add(builder.equal(item.get("status"), status));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
				return predicate;
				
			}
		};
	}
	public static Specification searchByCustomerIdAndDateRangeAndTypeNot(final Long customerId,final Date fromDate,
			final Date toDate,final DeliveryStatus status, final Type notType) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
                predicate.getExpressions().add(builder.and(builder.equal(item.get("status"), status)));
                if(null != notType) {
                	predicate.getExpressions().add(builder.and(builder.notEqual(item.get("type"), notType)));
                }
                
                Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(fromDate.getTime());
				endOfTheDay.setTime(toDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
                
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
                predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
                
 				return predicate;
			}
			
		};
		
	}

	public static Specification searchByCustomerAndDate(final Long customerId,final Date date) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                Date startOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				
                predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
                predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
                
 				return predicate;
			}
			
		};
		
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByCustomerIdAndDateRange(final Long customerId, final Date fromDate, final Date toDate, final SubscriptionType subscriptionType, final Type... types) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(fromDate.getTime());
				endOfTheDay.setTime(toDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
				
				if(null != subscriptionType) {
					predicate.getExpressions().add(builder.and(builder.equal(item.get("subscriptionType"), subscriptionType)));
				}
				if(null != types && types.length > 0) {
					predicate.getExpressions().add(builder.and(item.get("type").in(types)));
				}

				return predicate;
			}
		};
		
	}
	
	
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByRouteIdAndDate(final Long routeId, final Date date,final int priority) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
				predicate.getExpressions().add(builder.and(builder.equal(item.get("route").get("id"), routeId)));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("priority"), priority)));

				return predicate;
			}
		};
	}

	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByRouteIdAndDate(final Long routeId, final Date date) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("route").get("id"), routeId)));
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));

				return predicate;
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByRouteAndDateAndPriority(final Long routeId, final Date date,final int priority) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
				predicate.getExpressions().add(builder.and(builder.equal(item.get("route").get("id"), routeId)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
				predicate.getExpressions().add(builder.and(builder.equal(item.get("priority"), priority)));

				return predicate;
			}
		};
	}
	/*
	 * Returns all the delivery schedule for particular customer in whole day
	 */
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByCustomerIdAndDate(final String customerCode, final Date date) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("code"), customerCode)));
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
				

				return predicate;
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByDate(final Date date) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
				

				return predicate;
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchDeliveryScheduleByDateAndStatus(final Date date,final DeliveryStatus status) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(date.getTime());
				endOfTheDay.setTime(date.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
     			predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
     			predicate.getExpressions().add(builder.equal(item.get("status"), status));
				

				return predicate;
			}
		};
	}
	public static Specification searchByStatusAndDateRange(final Date fromDate,
			final Date toDate,final DeliveryStatus status) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                
                Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(fromDate.getTime());
				endOfTheDay.setTime(toDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
                predicate.getExpressions().add(builder.and(builder.equal(item.get("status"), status)));
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
                predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
                
 				return predicate;
			}
			
		};
		
	}
	public static Specification searchByDateRange(final Date fromDate,
			final Date toDate) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                
                Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(fromDate.getTime());
				endOfTheDay.setTime(toDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), startOfTheDay)));
                predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), endOfTheDay)));
                
 				return predicate;
			}
			
		};
		
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification searchByDeliveryTypeAndDateRange(final  Type type,final Date startDate,final Date endDate) {
		return new Specification() {

			@SuppressWarnings("unchecked")
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(startDate.getTime());
				endOfTheDay.setTime(endDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
				predicate.getExpressions().add(builder.notEqual(item.get("type"), type));
				predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), startOfTheDay)));
	            predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), endOfTheDay)));
	                
				return predicate;
				
			}
		};
	}
	
	@SuppressWarnings("rawtypes")
	public static Specification getDeliverySchedulesFromDate(final Date fromDate) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				
				Date startOfTheFromDay = new Date();

				startOfTheFromDay.setTime(fromDate.getTime());
				DateHelper.setToStartOfDay(startOfTheFromDay);

				predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheFromDay)));
				return predicate;
			}
		};
		
	}
	
	public static Specification searchByCustomerId(final Long customerId) {
		return new Specification() {
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
                Predicate predicate = builder.and();
                predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
 				return predicate;
			}
		};
	}
	
	public static Specification searchByDateRangeAndCustomer(final Date fromDate, final Date toDate, final String customerCode) {
		return new Specification() {
 
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query,
					CriteriaBuilder builder) {
               /* Predicate predicate = builder.and();
                
                Date startOfTheDay = new Date(), endOfTheDay = new Date();

				startOfTheDay.setTime(fromDate.getTime());
				endOfTheDay.setTime(toDate.getTime());
				
				DateHelper.setToStartOfDay(startOfTheDay);
				DateHelper.setToEndOfDay(endOfTheDay);
				
                predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), startOfTheDay)));
                predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), endOfTheDay)));
                predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("id"), customerId)));
                */
                
                return builder.and(getByDateRange(fromDate, toDate,item,builder), getCustomerById(customerCode,item,builder));
                
			}
			
		};
		
	}
	
	public static Predicate getCustomerById(String customerCode,Root item,CriteriaBuilder builder) {
        Predicate predicate = builder.and();
        predicate.getExpressions().add(builder.and(builder.equal(item.get("customer").get("code"), customerCode)));
			return predicate;
	}
	
	
	public static Predicate getByDateRange(Date fromDate,Date toDate,Root item, CriteriaBuilder builder) {
        Predicate predicate = builder.and();
        
        Date startOfTheDay = new Date(), endOfTheDay = new Date();

		startOfTheDay.setTime(fromDate.getTime());
		endOfTheDay.setTime(toDate.getTime());
		
		DateHelper.setToStartOfDay(startOfTheDay);
		DateHelper.setToEndOfDay(endOfTheDay);
		
		predicate.getExpressions().add(builder.and(builder.lessThanOrEqualTo(item.get("date"), endOfTheDay)));
		predicate.getExpressions().add(builder.and(builder.greaterThanOrEqualTo(item.get("date"), startOfTheDay)));
        
			return predicate;
	}
	
}
