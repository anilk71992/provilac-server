package com.vishwakarma.provilac.model.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class PrepaySubscriptionRequestSpecifications {

	public static Specification search(final String searchTerm, final boolean isChangeRequest) {
		return new Specification() {
			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				return builder.and(getPredicateForSearch(searchTerm, item, query, builder), getPredicateForRequestType(isChangeRequest, item, query, builder));
			}
		};
	}

	private static Predicate getPredicateForRequestType(boolean isChangeRequest, Root item, CriteriaQuery query, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.equal(item.get("isChangeRequest"), isChangeRequest));
		return predicate;
	}

	public static Specification searchByCustomermobileNumber(final String mobileNumber) {
		return new Specification() {

			@Override
			public Predicate toPredicate(Root item, CriteriaQuery query, CriteriaBuilder builder) {
				Predicate predicate = builder.and();
				predicate.getExpressions().add(builder.and(builder.equal(item.get("user").get("mobileNumber"), mobileNumber)));

				return predicate;
			}

		};
	}
	
	public static Predicate getPredicateForSearch(String searchTerm,Root item, CriteriaQuery query, CriteriaBuilder builder) {
		Predicate predicate = builder.or();
		predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("firstName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("lastName"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("email"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("username"), "%" + searchTerm + "%")));
		predicate.getExpressions().add(builder.or(builder.like(item.get("user").get("mobileNumber"), "%" + searchTerm + "%")));

		return predicate;
	}
}