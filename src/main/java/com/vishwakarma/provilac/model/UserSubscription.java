package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="UserSubscription",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class UserSubscription extends  BaseEntity{

	//TODO: Set<SubscriptionLineItem>, SubscriptionLineItem-> Product, Qty, Day
	
	public static enum SubscriptionType {
		Prepaid,
		Postpaid
	}
		
	private String code;
	
	@ManyToOne
	@JoinColumn(name="userId",nullable=true, referencedColumnName="id")
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@OneToMany(mappedBy="userSubscription", orphanRemoval=true)
	private Set<SubscriptionLineItem> subscriptionLineItems = new HashSet<SubscriptionLineItem>();
	
	private String permanantNote, internalNote;
	
	private boolean isCurrent;
	
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition="varchar(255) default 'Postpaid'")
	private SubscriptionType subscriptionType;
	
	//Valid only in case of Prepaid Subscription
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@ElementCollection(fetch=FetchType.LAZY)
	private Map<Long, Double> productPricing = new HashMap<Long, Double>();
	
	//Prepay Payment Details
	private String txnId;
	
	private double totalAmount = 0, pointsDiscount = 0, finalAmount = 0;
	
	private int pointsRedemed;
	
	private String paymentGateway;
	
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isApprovedByPG=true;
	//End prepay payment fields
	
	@PostPersist
	public void populateCode() {
		setCode("US-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<SubscriptionLineItem> getSubscriptionLineItems() {
		return subscriptionLineItems;
	}

	public void setSubscriptionLineItems(
			Set<SubscriptionLineItem> subscriptionLineItems) {
		this.subscriptionLineItems = subscriptionLineItems;
	}

	public String getPermanantNote() {
		return permanantNote;
	}

	public void setPermanantNote(String permanantNote) {
		this.permanantNote = permanantNote;
	}
	
	public String getInternalNote() {
		return internalNote;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public boolean getIsCurrent() {
		return isCurrent;
	}


	public void setIsCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Map<Long, Double> getProductPricing() {
		return productPricing;
	}

	public void setProductPricing(Map<Long, Double> subscriptionPricing) {
		this.productPricing = subscriptionPricing;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getPointsDiscount() {
		return pointsDiscount;
	}

	public void setPointsDiscount(double pointsDiscount) {
		this.pointsDiscount = pointsDiscount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public int getPointsRedemed() {
		return pointsRedemed;
	}

	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public boolean getIsApprovedByPG() {
		return isApprovedByPG;
	}

	public void setIsApprovedByPG(boolean isApprovedByPG) {
		this.isApprovedByPG = isApprovedByPG;
	}
}
