package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="PrepaySubscriptionRequest",uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class PrepaySubscriptionRequest extends BaseEntity {

	private String code;
	
	@ManyToOne
	@JoinColumn(name="userId",nullable=true, referencedColumnName="id")
	private User user;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	private int duration;

	@OneToMany(mappedBy="prepaySubscriptionRequest", orphanRemoval=true)
	private Set<PrepayRequestLineItem> prepayRequestLineItems = new HashSet<PrepayRequestLineItem>();
	
	private String permanantNote, internalNote;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isChangeRequest;
	
	@NotNull
	@NotEmpty
	private String txnId;
	
	private double totalAmount = 0, pointsDiscount = 0, finalAmount = 0;
	
	private int pointsRedemed;
	
	@NotNull
	@NotEmpty
	private String paymentGateway;
	
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isApprovedByPG=true;
	
	@ElementCollection(fetch=FetchType.LAZY)
	private Map<Long, Double> productPricing = new HashMap<Long, Double>();
	
	@PostPersist
	public void populateCode() {
		setCode("PSR-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Set<PrepayRequestLineItem> getPrepayRequestLineItems() {
		return prepayRequestLineItems;
	}

	public void setPrepayRequestLineItems(Set<PrepayRequestLineItem> prepayRequestLineItems) {
		this.prepayRequestLineItems = prepayRequestLineItems;
	}

	public String getPermanantNote() {
		return permanantNote;
	}

	public void setPermanantNote(String permanantNote) {
		this.permanantNote = permanantNote;
	}

	public String getInternalNote() {
		return internalNote;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	public boolean getIsChangeRequest() {
		return isChangeRequest;
	}

	public void setIsChangeRequest(boolean isChangeRequest) {
		this.isChangeRequest = isChangeRequest;
	}
	
	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getPointsDiscount() {
		return pointsDiscount;
	}

	public void setPointsDiscount(double pointsDiscount) {
		this.pointsDiscount = pointsDiscount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public int getPointsRedemed() {
		return pointsRedemed;
	}

	public void setPointsRedemed(int pointsRedemed) {
		this.pointsRedemed = pointsRedemed;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public boolean getIsApprovedByPG() {
		return isApprovedByPG;
	}

	public void setIsApprovedByPG(boolean isApprovedByPG) {
		this.isApprovedByPG = isApprovedByPG;
	}

	public Map<Long, Double> getProductPricing() {
		return productPricing;
	}

	public void setProductPricing(Map<Long, Double> productPricing) {
		this.productPricing = productPricing;
	}
}
