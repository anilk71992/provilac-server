package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="ProvilacImage",uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class ProvilacImage extends BaseEntity{

	public static enum LinkTo {
		Product,
		Subscription,
		ReferAndEarn,
		Calendar }
	
	private String code;
	
	private String PicUrl;
	
	@Enumerated(EnumType.STRING)
	private LinkTo linkTo;
	
	@Transient
	private byte[] picData;
	
	@Transient
	private String tempFileName;
	
	@PostPersist
	public void populateCode(){
		setCode("PI-"+getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPicUrl() {
		return PicUrl;
	}

	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}

	public byte[] getPicData() {
		return picData;
	}

	public void setPicData(byte[] picData) {
		this.picData = picData;
	}

	public String getTempFileName() {
		return tempFileName;
	}

	public void setTempFileName(String tempFileName) {
		this.tempFileName = tempFileName;
	}

	public LinkTo getLinkTo() {
		return linkTo;
	}

	public void setLinkTo(LinkTo linkTo) {
		this.linkTo = linkTo;
	}
	
	
}
