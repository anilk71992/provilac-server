package com.vishwakarma.provilac.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vishwakarma.provilac.utils.AppConstants.Day;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="DeliveryLineItem",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class DeliveryLineItem extends BaseEntity {
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="productId",nullable=false,referencedColumnName="id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name="deliveryScheduleId",nullable=true,referencedColumnName="id")
	private DeliverySchedule deliverySchedule;

	private int quantity;
	
	@Enumerated(EnumType.STRING)
	private Day day;

	@Column(columnDefinition="double default '0.0'")
	private double pricePerUnit, totalPrice;
	
	@PostPersist
	public void populateCode() {
		setCode("DL-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public DeliverySchedule getDeliverySchedule() {
		return deliverySchedule;
	}

	public void setDeliverySchedule(DeliverySchedule deliverySchedule) {
		this.deliverySchedule = deliverySchedule;
	}

	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
