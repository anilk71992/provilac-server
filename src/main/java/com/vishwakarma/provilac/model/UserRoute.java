package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Reshma
 *
 */
@Entity
@Table(name="UserRoute",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class UserRoute extends BaseEntity {
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="deliveryBoyId",nullable = false,referencedColumnName="id")
	private User deliveryBoy;
	
	@ManyToOne
	@JoinColumn(name="collectionBoyId",nullable = true,referencedColumnName="id")
	private User collectionBoy;
	
	@ManyToOne
	@JoinColumn(name="routeId",nullable=false,referencedColumnName="id")
	private Route route;

	@PostPersist
	public void populateCode() {
		setCode("UR-" + getId());
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getDeliveryBoy() {
		return deliveryBoy;
	}
	public void setDeliveryBoy(User deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}
	public User getCollectionBoy() {
		return collectionBoy;
	}
	public void setCollectionBoy(User collectionBoy) {
		this.collectionBoy = collectionBoy;
	}
	
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof UserRoute)) {
			return false;
		}
		UserRoute userRoute = (UserRoute) obj;
		if(null == this.getId()) {
			return super.equals(obj);
		}
		
		return this.getId().equals(userRoute.getId());
	}
	
	@Override
	public int hashCode() {
		
		if(null == this.getId()) {
			return super.hashCode();
		}
		return this.getId().hashCode();
	}
	
	
}
