package com.vishwakarma.provilac.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Reshma
 *
 */
@Entity
@Table(name="Route",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Route extends BaseEntity {
	
	private String code;
	
	private String name;
	
	@OneToMany(mappedBy="route", orphanRemoval=true, fetch= FetchType.EAGER)
	@OrderBy("priority ASC")
	private Set<RouteRecord> routeRecords = new HashSet<RouteRecord>();
	
	@ManyToMany
	@JoinTable(name="NotificationRoute", 
		inverseJoinColumns=@JoinColumn(name="notificationId", nullable=false, referencedColumnName="id"),
		joinColumns=@JoinColumn(name="routeId", nullable=false, referencedColumnName="id"))
	private Set<Notification> notifications;

	@PostPersist
	public void populateCode() {
		setCode("R-" + getId());
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<RouteRecord> getRouteRecords() {
		return routeRecords;
	}

	public void setRouteRecords(Set<RouteRecord> routeRecords) {
		this.routeRecords = routeRecords;
	}
	
	public Set<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof Route)) {
			return false;
		}
		Route route = (Route) obj;
		if(null == this.getId() || null == route.getId()) {
			return super.equals(obj);
		}
		
		return this.getId().equals(route.getId());
	}
	
	@Override
	public int hashCode() {
		
		if(null == this.getId()) {
			return super.hashCode();
		}
		return this.getId().hashCode();
	}
	
	
}
