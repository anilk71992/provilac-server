package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="SubscriptionRequest",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class SubscriptionRequest extends  BaseEntity{

	//TODO: Set<SubscriptionLineItem>, SubscriptionLineItem-> Product, Qty, Day
		
	private String code;
	
	@ManyToOne
	@JoinColumn(name="userId",nullable=true, referencedColumnName="id")
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@OneToMany(mappedBy="subscriptionRequest", orphanRemoval=true)
	private Set<RequestLineItem> requestLineItems = new HashSet<RequestLineItem>();
	
	private String permanantNote, internalNote;
	
	//TODO: Remove these, redundant
	private boolean isCurrent, isSubscriptionRequest;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isChangeRequest;

	@PostPersist
	public void populateCode() {
		setCode("SR-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<RequestLineItem> getRequestLineItems() {
		return requestLineItems;
	}

	public void setRequestLineItems(Set<RequestLineItem> requestLineItems) {
		this.requestLineItems = requestLineItems;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getPermanantNote() {
		return permanantNote;
	}

	public void setPermanantNote(String permanantNote) {
		this.permanantNote = permanantNote;
	}
	
	public String getInternalNote() {
		return internalNote;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public boolean getIsCurrent() {
		return isCurrent;
	}


	public void setIsCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public boolean getIsSubscriptionRequest() {
		return isSubscriptionRequest;
	}

	public void setIsSubscriptionRequest(boolean isSubscriptionRequest) {
		this.isSubscriptionRequest = isSubscriptionRequest;
	}
	
	public boolean getIsChangeRequest() {
		return isChangeRequest;
	}
	
	public void setIsChangeRequest(boolean isChangeRequest) {
		this.isChangeRequest = isChangeRequest;
	}
	
}
