package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vishwakarma.provilac.model.SubscriptionLineItem.SubscriptionType;
import com.vishwakarma.provilac.utils.AppConstants.Day;

@Entity
@Table(name="PrepayRequestLineItem",uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class PrepayRequestLineItem extends BaseEntity {

	private String code;

	@Enumerated(EnumType.STRING)
	private SubscriptionType type;
	
	@Enumerated(EnumType.STRING)
	private Day day;
	
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="productId",nullable=true,referencedColumnName="id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name="preapySubscriptionRequestId",nullable=true,referencedColumnName="id")
	private PrepaySubscriptionRequest prepaySubscriptionRequest;
	
	//Only used in case subscription type is custom, separated by "-" e.g. 1-2-5-6
	private String customPattern;
	
	@PostPersist
	public void populateCode() {
		setCode("PRLI-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SubscriptionType getType() {
		return type;
	}

	public void setType(SubscriptionType type) {
		this.type = type;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public PrepaySubscriptionRequest getPrepaySubscriptionRequest() {
		return prepaySubscriptionRequest;
	}

	public void setPrepaySubscriptionRequest(PrepaySubscriptionRequest prepaySubscriptionRequest) {
		this.prepaySubscriptionRequest = prepaySubscriptionRequest;
	}

	public String getCustomPattern() {
		return customPattern;
	}

	public void setCustomPattern(String customPattern) {
		this.customPattern = customPattern;
	}
}
