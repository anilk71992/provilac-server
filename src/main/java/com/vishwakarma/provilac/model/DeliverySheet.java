package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * @author Harshal
 */
@Entity
@Table(name="DeliverySheet",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class DeliverySheet extends BaseEntity{
	private String code;
	
	@OneToMany(mappedBy="deliverySheet",orphanRemoval=true,fetch=FetchType.EAGER)
	@OrderBy("priority ASC")
	private Set<DeliverySchedule> deliverySchedules = new HashSet<DeliverySchedule>();
	
	@ManyToOne
	@JoinColumn(name="userRouteId",nullable=false,referencedColumnName="id")
	private UserRoute userRoute;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	private boolean isCompleted = false;
	
	@PostPersist
	public void populateCode() {
		setCode("DS-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<DeliverySchedule> getDeliverySchedules() {
		return deliverySchedules;
	}

	public void setDeliverySchedules(Set<DeliverySchedule> deliverySchedules) {
		this.deliverySchedules = deliverySchedules;
	}

	public UserRoute getUserRoute() {
		return userRoute;
	}

	public void setUserRoute(UserRoute userRoute) {
		this.userRoute = userRoute;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
}
