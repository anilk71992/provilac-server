package com.vishwakarma.provilac.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author Harshal
 *
 */
@Entity
@Table(name="Deposit",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Deposit extends BaseEntity {
	
	private String code;
	
	//ChequeAmount and noOfCheques is redundant, but kept it for minimum db operation, otherwise will have to fetch
	//payments set each time
	private double totalAmount, cashAmount, chequeAmount;
	
	private Integer noOfCheques;
	
	private String cashTransactionId;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="userId", nullable=false, referencedColumnName="id")
	private User collectionBoy;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="deposit")
	private List<Payment> chequePayments = new ArrayList<Payment>();

	@PostPersist
	public void populateCode() {
		setCode("DC-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public Integer getNoOfCheques() {
		return noOfCheques;
	}

	public void setNoOfCheques(Integer noOfCheque) {
		this.noOfCheques = noOfCheque;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getCollectionBoy() {
		return collectionBoy;
	}

	public void setCollectionBoy(User collectionBoy) {
		this.collectionBoy = collectionBoy;
	}

	public List<Payment> getChequePayments() {
		return chequePayments;
	}

	public void setChequePayments(List<Payment> chequePayments) {
		this.chequePayments = chequePayments;
	}

	public String getCashTransactionId() {
		return cashTransactionId;
	}

	public void setCashTransactionId(String cashTrnsactionId) {
		this.cashTransactionId = cashTrnsactionId;
	}
	
}
