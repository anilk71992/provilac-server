package com.vishwakarma.provilac.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vishwakarma.provilac.utils.AppConstants.Day;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="SubscriptionLineItem",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class SubscriptionLineItem extends  BaseEntity{

	//TODO: Set<SubscriptionLineItem>, SubscriptionLineItem-> Product, Qty, Day
	
	public static enum SubscriptionType{
		Daily,
		Weekly,
		Custom
	}
	
	private String code;

	@Enumerated(EnumType.STRING)
	private SubscriptionType type;
	
	@Enumerated(EnumType.STRING)
	private Day day;
	
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="productId",nullable=true,referencedColumnName="id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name="userSubscriptionId",nullable=true,referencedColumnName="id")
	private UserSubscription userSubscription;
	
	//Only used in case subscription type is custom, separated by "," e.g. 1,2,5,6
	private String customPattern;
	
	private int currentIndex;

	@PostPersist
	public void populateCode() {
		setCode("SL-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SubscriptionType getType() {
		return type;
	}

	public void setType(SubscriptionType type) {
		this.type = type;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}


	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public UserSubscription getUserSubscription() {
		return userSubscription;
	}

	public void setUserSubscription(UserSubscription userSubscription) {
		this.userSubscription = userSubscription;
	}

	public String getCustomPattern() {
		return customPattern;
	}

	public void setCustomPattern(String customPattern) {
		this.customPattern = customPattern;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	
	
}
