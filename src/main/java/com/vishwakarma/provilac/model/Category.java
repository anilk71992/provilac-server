package com.vishwakarma.provilac.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="Category",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class Category extends BaseEntity{
	
	//TODO: Remove Category, and take it as separate model
	
	private String code;
	
	@NotNull
	@NotEmpty
	private String name;
	
	@Column(columnDefinition="MediumText")
	private String description;
	
	private String categoryPicUrl;
	
	@Transient
	private byte[] categoryData;
	
	@Transient
	private String categoryFileName;
	
	@PostPersist
	public void populateCode() {
		setCode("C-" + getId());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}


	public String getCategoryPicUrl() {
		return categoryPicUrl;
	}


	public byte[] getCategoryData() {
		return categoryData;
	}


	public String getCategoryFileName() {
		return categoryFileName;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setCategoryPicUrl(String categoryPicUrl) {
		this.categoryPicUrl = categoryPicUrl;
	}


	public void setCategoryData(byte[] categoryData) {
		this.categoryData = categoryData;
	}


	public void setCategoryFileName(String categoryFileName) {
		this.categoryFileName = categoryFileName;
	}
	
}
