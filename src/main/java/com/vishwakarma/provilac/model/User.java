package com.vishwakarma.provilac.model;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vishwakarma.provilac.model.Payment.PaymentMethod;
import com.vishwakarma.provilac.utils.CommonUtils;

/**
 * @author sachin
 *
 */
@Entity(name="User")
@Table(name="User", uniqueConstraints={
		@UniqueConstraint(columnNames = "mobileNumber")})
@Inheritance(strategy=InheritanceType.JOINED)
public class User extends BaseEntity{
	
	//TODO: Address, status, otherBrand, consumptionQty, referredMedia(enum), referredBy
	/**
	 * Status Enum: HOLD, ACTIVE, INACTIVE, NEW
	 */
	
	public static enum Status{
		HOLD,
		ACTIVE,
		INACTIVE,
		NEW
	}
	
	public static enum ReferredMedia{
		Social_Media,
		Website,
		Newspaper,
		Radio,
		Event,
		Friend
	}
	
	public static enum ReasonToStop{
		Shifted_City,
		Purchase_From_Competitor,
		Milk_Quality_Or_Taste,
		Bottle_Quality,
		Expensive,
		Variants_Not_Available,
		Others
	}
	
	private String code;
	
	private String firstName;
	
	private String lastName;
	
	@Email
	@Column(nullable=true)
	private String email;
	
	@NotNull
	@NotEmpty
	private String mobileNumber;
	
	private String alterNateMobileNumber;
	
	@Column(nullable=true)
	private String password;
	
	@Column(unique=true,nullable=false,updatable=false)
	private String username;

	private String fbId, gPlusId, twitterId;
	
	//TODO: Rename to preferred paymentMethod
	@Enumerated(EnumType.STRING)
	private PaymentMethod paymentMethod;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Enumerated(EnumType.STRING)
	private ReferredMedia referredMedia;

	private String notes;
	
	//Address is Google Address, Actual address referring building address  
	private String address ,buildingAddress;
	
	private String otherBrand;
	
	/*Can not use lastPendingDues = Remaining Prepay Balance for Prepaid Customers, since in that case we need to pass 
	 * 'runnableBalance' to createExcessivePrepayDeliveries engine but issue comes up when there are partial amomunt left after executiom of the engine
	 * So, lastPendingDues, helps us in taking care of remaining partial balance that gets left over after the execution of the engine.
	 * Remaining Prepay Balance updates over the night and whenever deliveries from past are modified
	 */
	@Column(columnDefinition="Double default '0.0'")
	private double consumptionQty, lastPendingDues = 0.0, collectionOuststanding, remainingPrepayBalance;
	
	private Double  lat, lng;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isRouteAssigned;
	
	@ManyToOne
	@JoinColumn(name="referredBy", nullable=true, referencedColumnName="id")
	private User referredBy;
	
	private String referralCode;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean allocatedFirstTimeReferralPoints;
	
	@Column(columnDefinition="int(11) default '0'")
	private int accumulatedPoints = 0;
	
	@ManyToOne
	@JoinColumn(name="assignedTo", nullable=true, referencedColumnName="id")
	private User assignedTo;
	
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean gender;

	private Date dob, joiningDate;

	private String profilePicUrl;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "UserRole", joinColumns =@JoinColumn(name = "userId"), inverseJoinColumns =@JoinColumn(name = "roleId"))
	private Set<Role> userRoles=new HashSet<Role>();	
	
	@NotNull
	@Column(columnDefinition="tinyint(1) default '1'")
	private boolean isEnabled;
	
	@NotNull
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isVerified;
	
	private String verificationCode;
	
	@Enumerated(EnumType.STRING)
	private ReasonToStop reasonToStop;
	
	@Transient
	private byte[] profilePicData;
	
	@Transient
	private String fileName;
	
	@Transient
	private Logger logger = LoggerFactory.getLogger(User.class);
	
	@PrePersist
	public void setDefaults() {
		if (StringUtils.isEmpty(username)) {
			username = mobileNumber;
		}
	}
	
	@PostPersist
	public void onPostPersist() {
		setCode("U-" + getId());
		createProfilePicFile();
		if(StringUtils.isBlank(getReferralCode()))
			setReferralCode(CommonUtils.getReferralCodeForUser(this));
	}
	
	public void createProfilePicFile() {
		if(null != profilePicData) {
			try {
				File profilePic = getProfilePicFile();
				FileUtils.writeByteArrayToFile(profilePic, profilePicData);
				setProfilePicUrl(profilePic.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlterNateMobileNumber() {
		return alterNateMobileNumber;
	}

	public void setAlterNateMobileNumber(String alterNateMobileNumber) {
		this.alterNateMobileNumber = alterNateMobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getgPlusId() {
		return gPlusId;
	}

	public void setgPlusId(String gPlusId) {
		this.gPlusId = gPlusId;
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}
	
	public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public boolean getIsRouteAssigned() {
		return isRouteAssigned;
	}

	public void setIsRouteAssigned(boolean isRouteAssigned) {
		this.isRouteAssigned = isRouteAssigned;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public Set<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<Role> userRoles) {
		this.userRoles = userRoles;
	}
	
	public boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public byte[] getProfilePicData() {
		return profilePicData;
	}
	

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBuildingAddress() {
		return buildingAddress;
	}

	public void setBuildingAddress(String buildingAddress) {
		this.buildingAddress = buildingAddress;
	}

	public String getOtherBrand() {
		return otherBrand;
	}

	public void setOtherBrand(String otherBrand) {
		this.otherBrand = otherBrand;
	}

	public double getConsumptionQty() {
		return consumptionQty;
	}

	public void setConsumptionQty(double consumptionQty) {
		this.consumptionQty = consumptionQty;
	}

	public double getLastPendingDues() {
		return lastPendingDues;
	}

	public void setLastPendingDues(double lastPendingDues) {
		this.lastPendingDues = lastPendingDues;
	}

	public User getReferredBy() {
		return referredBy;
	}
	
	public void setReferredBy(User referredBy) {
		this.referredBy = referredBy;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public boolean getAllocatedFirstTimeReferralPoints() {
		return allocatedFirstTimeReferralPoints;
	}

	public void setAllocatedFirstTimeReferralPoints(boolean allocatedFirstTimeReferralPoints) {
		this.allocatedFirstTimeReferralPoints = allocatedFirstTimeReferralPoints;
	}
	
	public int getAccumulatedPoints() {
		return accumulatedPoints;
	}

	public void setAccumulatedPoints(int accumulatedPoints) {
		this.accumulatedPoints = accumulatedPoints;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}
	

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ReferredMedia getReferredMedia() {
		return referredMedia;
	}

	public void setReferredMedia(ReferredMedia referredMedia) {
		this.referredMedia = referredMedia;
	}
	
	public User getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(User assignedTo) {
		this.assignedTo = assignedTo;
	}

	public ReasonToStop getReasonToStop() {
		return reasonToStop;
	}

	public void setReasonToStop(ReasonToStop reasonToStop) {
		this.reasonToStop = reasonToStop;
	}

	/**
	 * Don't forget to call {@linkplain setFileName()}
	 * @param profilePicData
	 */
	public void setProfilePicData(byte[] profilePicData) {
		this.profilePicData = profilePicData;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setUserRole(Role role) {
		this.userRoles = new HashSet<Role>();
		this.userRoles.add(role);
	}
	
	public boolean hasRole(String roleName) {
		for (Role role : userRoles) {
			if (role.getRole().equals(roleName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns whether user has any role specified in CSV string
	 * @param roleCSV - Comma Separated Role Names
	 * @return
	 */
	public boolean hasAnyRole(String roleCSV) {
		String[] rolesArray = roleCSV.split(",");
		for (int i = 0; i < rolesArray.length; i++) {
			rolesArray[i] = rolesArray[i].trim();
		}
		List<String> roles = Arrays.asList(rolesArray);
		for (Role role : userRoles) {
			if(roles.contains(role.getRole())) {
				return true;
			}
		}
		return false;
	}
	
	public void addRole(Role role) {
		if(this.userRoles == null) {
			this.userRoles = new HashSet<Role>();
		}
		this.userRoles.add(role);
	}
	
	public void removeRole(Role role) {
		if(this.userRoles != null) {
			this.userRoles.remove(role);
		}
	}

	public String getFullName() {
		return firstName+" "+lastName;
	}
	
	@Override
	public String toString() {
		return firstName+" "+lastName;
	}
	
	private File getProfilePicFile() {
		
		File baseDir = new File(System.getProperty("catalina.base"), "userProfilePics");
		if(!baseDir.exists() || !baseDir.isDirectory()) {
			baseDir.mkdir();
		}
		File profilePic = new File(baseDir, this.getId() + "_" + this.getFileName());
		if(profilePic.exists() && !profilePic.isDirectory()) {
			profilePic.delete();
		}
		return profilePic;
	}

	public double getCollectionOuststanding() {
		return collectionOuststanding;
	}

	public void setCollectionOuststanding(double collectionOuststanding) {
		this.collectionOuststanding = collectionOuststanding;
	}

	public double getRemainingPrepayBalance() {
		return remainingPrepayBalance;
	}

	public void setRemainingPrepayBalance(double remainingPrepayBalance) {
		this.remainingPrepayBalance = remainingPrepayBalance;
	}
	
}
