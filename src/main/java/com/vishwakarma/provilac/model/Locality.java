package com.vishwakarma.provilac.model;


import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name="Locality", uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
public class Locality extends BaseEntity {

	private String code;
	
	@NotNull
	@NotEmpty
	private String name;
	
	private Long pincode;
	
	@ManyToOne
	@JoinColumn(name="cityId", nullable=false, referencedColumnName="id")
	private City city;
	
	@PostPersist
	public void populateCode() {
		setCode("L-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

}