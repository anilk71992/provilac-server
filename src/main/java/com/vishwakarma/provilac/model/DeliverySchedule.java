package com.vishwakarma.provilac.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;

/**
 * 
 * @author Reshma
 *
 */

@Entity
@Table(name="DeliverySchedule",uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
public class DeliverySchedule extends BaseEntity {
	
	public static enum DeliveryStatus{
		Delivered,
		NotDelivered,
		Waved_Off
	}
	
	public static enum Type{
		Normal,
		Free_Trial,
		Paid_Trial,
	}
	
	private String code;
	
	@ManyToOne
	@JoinColumn(name="customerId",nullable= false,referencedColumnName="id")
	private User customer;
	
	@ManyToOne
	@JoinColumn(name="invoiceId",nullable= true,referencedColumnName="id")
	private Invoice invoice;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date, deliveryTime;
	
	@OneToMany(mappedBy="deliverySchedule", orphanRemoval=true)
	private Set<DeliveryLineItem> deliveryLineItems = new HashSet<DeliveryLineItem>();
	
	@ManyToOne
	@JoinColumn(name="deliverySheetId",nullable=true,referencedColumnName="id")
	private DeliverySheet deliverySheet;
	
	@ManyToOne
	@JoinColumn(name="routeId",nullable=false,referencedColumnName="id")
	private Route route;
	
	private int priority;
	
	@Column(columnDefinition="MediumText")
	private String reason;
	
	private String permanantNote;
	
	private String tempararyNote;
	
	//TODO: Wrapper Class
	private Double lat,lng;
	
	@Enumerated(EnumType.STRING)
	private DeliveryStatus status;
	
	@Enumerated(EnumType.STRING)
	private Type type;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isUpdatedFromWeb;
	
	@Column(columnDefinition="tinyint(1) default '0'")
	private boolean isUpdatedFromMobile;
	
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition="varchar(255) default null")
	private SubscriptionType subscriptionType;
	
	@PostPersist
	public void populateCode() {
		setCode("DS-" + getId());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	
	public String getPermanantNote() {
		return permanantNote;
	}

	public void setPermanantNote(String permanantNote) {
		this.permanantNote = permanantNote;
	}

	public String getTempararyNote() {
		return tempararyNote;
	}

	public void setTempararyNote(String tempararyNote) {
		this.tempararyNote = tempararyNote;
	}

	public Double getLat() {
		return lat;
	}

	public Double getLng() {
		return lng;
	}

	public DeliveryStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatus status) {
		this.status = status;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Set<DeliveryLineItem> getDeliveryLineItems() {
		return deliveryLineItems;
	}

	public void setDeliveryLineItems(Set<DeliveryLineItem> deliveryLineItems) {
		this.deliveryLineItems = deliveryLineItems;
	}

	public DeliverySheet getDeliverySheet() {
		return deliverySheet;
	}

	public void setDeliverySheet(DeliverySheet deliverySheet) {
		this.deliverySheet = deliverySheet;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public boolean isUpdatedFromWeb() {
		return isUpdatedFromWeb;
	}

	public void setUpdatedFromWeb(boolean isUpdatedFromWeb) {
		this.isUpdatedFromWeb = isUpdatedFromWeb;
	}

	public boolean isUpdatedFromMobile() {
		return isUpdatedFromMobile;
	}

	public void setUpdatedFromMobile(boolean isUpdatedFromMobile) {
		this.isUpdatedFromMobile = isUpdatedFromMobile;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DeliverySchedule)) {
			return false;
		}
		DeliverySchedule other = (DeliverySchedule) obj;
		if (code == null || !code.equals(other.code)) {
			return false;
		}
		return true;
	}
	
	
}
