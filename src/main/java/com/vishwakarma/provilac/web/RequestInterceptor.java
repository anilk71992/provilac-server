package com.vishwakarma.provilac.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.vishwakarma.provilac.dto.CartItem;
import com.vishwakarma.provilac.dto.ProductDTO;
import com.vishwakarma.provilac.model.Product;

public class RequestInterceptor extends HandlerInterceptorAdapter {

	@Resource
	private HttpSession httpSession;

	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(httpSession.getAttribute("cartItems") == null) {
			List<CartItem> cart = new ArrayList<CartItem>();
			httpSession.setAttribute("cartItems", cart);
		}
		return super.preHandle(request, response, handler);
	}
	
	public List<CartItem> addItemToCart(Product product) {		
		List<CartItem> cartItems = (List<CartItem>) httpSession.getAttribute("cartItems");
		for(CartItem cartItem : cartItems) {
			if(cartItem.getProduct().getCode().contentEquals(product.getCode())) {
				int quantity = cartItem.getQuantity();				
				return addItemToCart(product, quantity + 1);
			}
		}		
		return addItemToCart(product, 1);
	}
	
	public List<CartItem> addItemToCart(Product product, int quantity) {	
		
		List<CartItem> cartItems = (List<CartItem>) httpSession.getAttribute("cartItems");
		for (CartItem cartItem : cartItems) {
			if(cartItem.getProduct().getCode().contentEquals(product.getCode())) {
				cartItem.setQuantity(quantity);
				return cartItems;
			}
		}
		cartItems.add(new CartItem(new ProductDTO(product), quantity));
		return cartItems;
	}
	
	public List<CartItem> deleteItemFromCart(String productCode) {		
		List<CartItem> cartItems = (List<CartItem>) httpSession.getAttribute("cartItems");
		for(CartItem cartItem : cartItems) {
			if(cartItem.getProduct().getCode().contentEquals(productCode)) {
				cartItems.remove(cartItem);
				break;
			}
		}
		return cartItems;
	}
	
	public List<CartItem> getCartItems() {		
		List<CartItem> cartItems = (List<CartItem>) httpSession.getAttribute("cartItems");
		return cartItems;
	}
	
	public void addDataToSession(String key, Object value) {
		httpSession.setAttribute(key, value);
	}
	
	public Object getDataFromSession(String key) {
		return httpSession.getAttribute(key);
	}
	
	public void deleteAllItemsFromCart() {		
		List<CartItem> cartItems = (List<CartItem>) httpSession.getAttribute("cartItems");
		cartItems.clear();				
	}
	
}
