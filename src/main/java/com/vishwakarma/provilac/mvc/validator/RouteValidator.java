package com.vishwakarma.provilac.mvc.validator;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Route;
import com.vishwakarma.provilac.model.RouteRecord;
import com.vishwakarma.provilac.service.RouteService;


	@Component
	public class RouteValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private RouteService routeService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return Route.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Route route = (Route) target;
			
			List<RouteRecord> routeRecords = new ArrayList<RouteRecord>(route.getRouteRecords());
			for(int i = 0; i < routeRecords.size(); i++) {
				RouteRecord routeRecord = routeRecords.get(i);
				
				for( int j = 0; j < routeRecords.size(); j++ ) {
					
					if(i!= j){
						RouteRecord routeRecord1 = routeRecords.get(j);
						
							if(routeRecord.getPriority()==routeRecord1.getPriority()) {
								errors.rejectValue("routeRecords", "Priority should be unique", "Priority should be unique");
							}
					}
						
				}
			}
			
			List<Route> routes = routeService.getAllRoutes(null, false, true);
			List<RouteRecord>routeRecords2 = new ArrayList<RouteRecord>();
			for (Route route2 : routes) {
				for (RouteRecord routeRecord : route2.getRouteRecords()) {
					routeRecords2.add(routeRecord);
				}
			}
			for(int i = 0; i < routeRecords.size(); i++) {
				RouteRecord routeRecord = routeRecords.get(i);
				
				for( int j = 0; j < routeRecords2.size(); j++ ) {
					RouteRecord routeRecord1 = routeRecords2.get(j);
					 if(routeRecord.getCustomer().getId().equals(routeRecord1.getCustomer().getId())) {
						errors.rejectValue("routeRecords", "RouteRecord for this Customer Already Exists", "RouteRecord for this Customer Already Exists");
					}
				}
			}
			
			if(null!=route.getName()){
				Route route1= routeService.getRouteByName(route.getName(), false, null,false, false);
				if(null!=route1&& route1.getName().equals(route.getName())&& route1.getId()!=route.getId()){
					errors.rejectValue("name", "Route with this name already exists", "Route with this name already exists");
				}
			}
			
			validator.validate(route, errors);
		}
		
	}


