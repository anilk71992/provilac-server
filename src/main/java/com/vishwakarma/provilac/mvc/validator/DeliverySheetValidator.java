package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.DeliverySheet;


	@Component
	public class DeliverySheetValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return DeliverySheet.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			DeliverySheet deliverySheet = (DeliverySheet) target;
			
			validator.validate(deliverySheet, errors);
		}
		
	}


