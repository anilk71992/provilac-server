package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Category;
import com.vishwakarma.provilac.service.CategoryService;


	@Component
	public class CategoryValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private CategoryService categoryService; 
		@Override
		public boolean supports(Class<?> clazz) {
			return Category.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Category category = (Category) target;
			
				Category existingCategory=categoryService.getCategoryByName(category.getName().trim(), false, null, false, false);
			
				if (!category.isUpdateOperation()) {
					if ( null != existingCategory) {
							errors.rejectValue("name", "Category  with this name already exists", "Category  with this name  already exists");
						}
				}else{
					if(null != existingCategory && category.getId() != existingCategory.getId()){
						errors.rejectValue("name", "Category  with this name already exists", "Category  with this name  already exists");
					}
				}
				
			validator.validate(category, errors);
		}
		
	}


