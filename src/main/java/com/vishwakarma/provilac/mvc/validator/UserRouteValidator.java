package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.UserRoute;
import com.vishwakarma.provilac.service.UserRouteService;
  
	@Component
	public class UserRouteValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private UserRouteService userRouteService;
		
		
		@Override
		public boolean supports(Class<?> clazz) {
			return UserRoute.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			UserRoute userRoute = (UserRoute) target;
			
			validator.validate(userRoute, errors);
		}
		
	}


