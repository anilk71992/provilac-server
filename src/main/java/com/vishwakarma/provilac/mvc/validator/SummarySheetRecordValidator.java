package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.SummarySheetRecord;


	@Component
	public class SummarySheetRecordValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return SummarySheetRecord.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			SummarySheetRecord summarySheetRecord = (SummarySheetRecord) target;
			
			validator.validate(summarySheetRecord, errors);
		}
		
	}


