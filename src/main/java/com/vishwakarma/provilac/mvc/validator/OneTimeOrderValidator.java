package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.OneTimeOrder;
import com.vishwakarma.provilac.service.OneTimeOrderService;


	@Component
	public class OneTimeOrderValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private OneTimeOrderService oneTimeOrderService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return OneTimeOrder.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			OneTimeOrder oneTimeOrder = (OneTimeOrder) target;
			
			validator.validate(oneTimeOrder, errors);
		}
		
	}


