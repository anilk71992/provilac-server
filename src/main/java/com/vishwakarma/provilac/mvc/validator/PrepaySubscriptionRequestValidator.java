package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.PrepaySubscriptionRequest;

@Component
public class PrepaySubscriptionRequestValidator implements Validator {

	@Resource(name = "mvcValidator")
	private Validator validator;

	@Override
	public boolean supports(Class<?> clazz) {
		return PrepaySubscriptionRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PrepaySubscriptionRequest prepayPrepaySubscriptionRequest = (PrepaySubscriptionRequest) target;

		validator.validate(prepayPrepaySubscriptionRequest, errors);
	}

}
