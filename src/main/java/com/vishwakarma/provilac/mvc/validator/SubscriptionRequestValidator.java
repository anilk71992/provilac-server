package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.SubscriptionRequest;


	@Component
	public class SubscriptionRequestValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return SubscriptionRequest.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			SubscriptionRequest subscriptionRequest = (SubscriptionRequest) target;
			
			validator.validate(subscriptionRequest, errors);
		}
		
	}


