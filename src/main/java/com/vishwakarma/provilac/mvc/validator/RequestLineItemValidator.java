package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.RequestLineItem;


	@Component
	public class RequestLineItemValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return RequestLineItem.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			RequestLineItem requestLineItem = (RequestLineItem) target;
			
			validator.validate(requestLineItem, errors);
		}
		
	}


