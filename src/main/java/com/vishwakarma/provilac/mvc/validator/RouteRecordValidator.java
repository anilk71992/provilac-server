package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.RouteRecord;


	@Component
	public class RouteRecordValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return RouteRecord.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			RouteRecord routeRecord = (RouteRecord) target;
			
			validator.validate(routeRecord, errors);
		}
		
	}


