package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Country;
import com.vishwakarma.provilac.service.CountryService;


@Component
public class CountryValidator implements Validator{

	@Resource(name="mvcValidator")
	 private Validator validator;
	
	@Resource
	private CountryService countryService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Country.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Country country = (Country) target;
		if(!StringUtils.isBlank(country.getName().trim()) && !country.isUpdateOperation()){
			Country existingCountry = countryService.getCountryByName(country.getName().trim(),false,null,false,false);
			if(null != existingCountry) {
				errors.rejectValue("name", "Country with same name already exists", "Country with same name already exists");
			}
		}
		if(StringUtils.isBlank(country.getName().trim())){
			errors.rejectValue("name", "Country Name may not be empty", "Country Name may not be empty");
		}
		
		if(!StringUtils.isBlank(country.getName().trim()) && country.isUpdateOperation()){
			Country existingCountry = countryService.getCountryByName(country.getName().trim(),false,null,false,false);
			if(null != existingCountry &&existingCountry.getId()!=country.getId()) {
				errors.rejectValue("name", "Country with same name already exists", "Country with same name already exists");
			}
		}
		validator.validate(country, errors);
	}

}
