package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Collection;
import com.vishwakarma.provilac.service.CollectionService;


	@Component
	public class CollectionValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource 
		private CollectionService collectionService;
		@Override
		public boolean supports(Class<?> clazz) {
			return Collection.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Collection collection = (Collection) target;

			if(collection.getTotalAmount() >= 0 || collection.getTotalAmount() == 0) {
				errors.rejectValue("date", "Collection amount should greater than 0", "Collection amount should greater than 0");
			}
			if(collection.getCashAmount() >= 0) {
				errors.rejectValue("date", "Collection Cash amount should greater than 0", "Collection Cash amount should greater than 0");
			}
			if(collection.getChequeAmount() >= 0) {
				errors.rejectValue("date", "Collection Cheque amount should greater than 0", "Collection Cheque amount should greater than 0");
			}
			
			if(null == collection.getCollectionBoy()) {
				errors.rejectValue("date", "Please select collection boy", "Please select collection boy");
			}
			validator.validate(collection, errors);
		}
		
	}


