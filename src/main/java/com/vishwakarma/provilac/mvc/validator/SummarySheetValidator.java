package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.SummarySheet;


	@Component
	public class SummarySheetValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return SummarySheet.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			SummarySheet summarySheet = (SummarySheet) target;
			
			validator.validate(summarySheet, errors);
		}
		
	}


