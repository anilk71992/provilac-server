package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Deposit;
import com.vishwakarma.provilac.service.DepositService;


	@Component
	public class DepositValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource 
		private DepositService depositService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return Deposit.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Deposit deposit = (Deposit) target;

			if(deposit.getTotalAmount() >= 0 || deposit.getTotalAmount() == 0) {
				errors.rejectValue("date", "Deposit amount should greater than 0", "Deposit amount should greater than 0");
			}
			if(deposit.getCashAmount() >= 0) {
				errors.rejectValue("date", "Deposit Cash amount should greater than 0", "Deposit Cash amount should greater than 0");
			}
			if(deposit.getChequeAmount() >= 0) {
				errors.rejectValue("date", "Deposit Cheque amount should greater than 0", "Deposit Cheque amount should greater than 0");
			}
			
			if(null == deposit.getCollectionBoy()) {
				errors.rejectValue("date", "Please select Collection boy", "Please select Collection boy");
			}
			validator.validate(deposit, errors);
		}
		
	}


