package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Locality;
import com.vishwakarma.provilac.service.LocalityService;

/**
 * @author Rohit
 *
 */

@Component
public class LocalityValidator implements Validator{

	@Resource
	LocalityService localityService;
	
	@Resource(name="mvcValidator")
	Validator validator;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Locality.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Locality locality = (Locality) target;
		if(!StringUtils.isBlank(locality.getName().trim()) && !locality.isUpdateOperation()){
			Locality existingLocality = localityService.getLocalityByNameAndCity(locality.getName().trim(),locality.getCity().getCode(),false,null,false,false);
			if(null != existingLocality && locality.getCity().getCode().equalsIgnoreCase(existingLocality.getCity().getCode())) {
				errors.rejectValue("name", "Locality with same name already exists", "Locality with same name already exists");
			}
		}
		if(StringUtils.isBlank(locality.getName().trim())){
			errors.rejectValue("name", "Locality Name may not be empty", "Locality Name may not be empty");
		}
		validator.validate(locality, errors);
	}

}