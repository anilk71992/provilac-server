package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.City;
import com.vishwakarma.provilac.service.CityService;


	@Component
	public class CityValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private CityService cityService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return City.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			City city = (City) target;
			if(!StringUtils.isBlank(city.getName().trim()) && !city.isUpdateOperation()){
				City existingCity = cityService.getCityByNameAndCountry(city.getName().trim(),city.getCountry().getName().trim(), false,null,false,false);
				if(null != existingCity) {
					errors.rejectValue("name", "City with same name already exists", "City with same name already exists");
				}
			}
			if(StringUtils.isBlank(city.getName().trim())){
				errors.rejectValue("name", "City Name may not be empty", "City Name may not be empty");
			}
			if(!StringUtils.isBlank(city.getName().trim()) && city.isUpdateOperation()){
				City existingCity = cityService.getCityByNameAndCountry(city.getName().trim(),city.getCountry().getName().trim(), false,null,false,false);
				if(null != existingCity && existingCity.getId()!=city.getId()) {
					errors.rejectValue("name", "City with same name already exists", "City with same name already exists");
				}
			}
			validator.validate(city, errors);
		}
		
	}


