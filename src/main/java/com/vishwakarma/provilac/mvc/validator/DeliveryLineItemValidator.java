package com.vishwakarma.provilac.mvc.validator;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.DeliveryLineItem;
import com.vishwakarma.provilac.service.DeliveryLineItemService;


	@Component
	public class DeliveryLineItemValidator implements Validator{
		
		@Resource
		private DeliveryLineItemService deliveryLineItemService;
		
		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return DeliveryLineItem.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			DeliveryLineItem deliveryLineItem = (DeliveryLineItem) target;
			List<DeliveryLineItem>lineItems = deliveryLineItemService.getAllDeliveryLineItemsByDeliverySchedule(deliveryLineItem.getDeliverySchedule().getId(), null, false, false);
			if(lineItems.size()!=0){
				for (DeliveryLineItem deliveryLineItem2 : lineItems) {
					if(deliveryLineItem2.getProduct().getId()==deliveryLineItem.getProduct().getId()){
						errors.rejectValue("product", "DeliveryLineItem with this Product Already Exists,Please try with other product", "DeliveryLineItem with this Product Already Exists,Please try with other product");
					}
				}
			}
			validator.validate(deliveryLineItem, errors);
		}
	}


