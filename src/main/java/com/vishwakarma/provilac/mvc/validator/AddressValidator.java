package com.vishwakarma.provilac.mvc.validator;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Address;
import com.vishwakarma.provilac.service.AddressService;



	@Component
	public class AddressValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private AddressService addressService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return Address.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Address address = (Address) target;
			if(!address.isUpdateOperation()){
				if(null != address.getName()){
					List<Address> addresses = addressService.getAddressesByCustomer(address.getCustomer().getCode(), null, false, false);
					for (Address address2 : addresses) {
						if(address2.getName().trim().equalsIgnoreCase(address.getName().trim())){
							errors.rejectValue("name", "The address with this name is already exists. Please enter another name.", "The address with this name is already exists. Please enter another name.");
						}
					}
				}
			}else{
				if(null!=address.getName()){
					List<Address> addresses = addressService.getAddressesByCustomer(address.getCustomer().getCode(), null, false, false);
					for (Address address2 : addresses) {
						if(!address.getCode().equals(address2.getCode()) && address2.getName().trim().equalsIgnoreCase(address.getName().trim())){
							errors.rejectValue("name", "The address with this name is already exists. Please enter another name.", "The address with this name is already exists. Please enter another name.");
						
						}
					}
				}
			}
			
			validator.validate(address, errors);
		}
		
	}


