package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.DeliverySchedule;


	@Component
	public class DeliveryScheduleValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return DeliverySchedule.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			DeliverySchedule deliverySchedule = (DeliverySchedule) target;
			
			validator.validate(deliverySchedule, errors);
		}
		
	}


