package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.PrepayRequestLineItem;

@Component
public class PrepayRequestLineItemValidator implements Validator {

	@Resource(name = "mvcValidator")
	private Validator validator;

	@Override
	public boolean supports(Class<?> clazz) {
		return PrepayRequestLineItem.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PrepayRequestLineItem prepayPrepayRequestLineItem = (PrepayRequestLineItem) target;

		validator.validate(prepayPrepayRequestLineItem, errors);
	}

}
