package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.SubscriptionLineItem;


	@Component
	public class SubscriptionLineItemValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return SubscriptionLineItem.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			SubscriptionLineItem subscriptionLineItem = (SubscriptionLineItem) target;
			
			validator.validate(subscriptionLineItem, errors);
		}
		
	}


