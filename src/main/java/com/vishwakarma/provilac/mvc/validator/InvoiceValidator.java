package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Invoice;


	@Component
	public class InvoiceValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return Invoice.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Invoice invoice = (Invoice) target;
			
			validator.validate(invoice, errors);
		}
		
	}


