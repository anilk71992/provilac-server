package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.UserSubscription;
import com.vishwakarma.provilac.model.UserSubscription.SubscriptionType;


	@Component
	public class UserSubscriptionValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return UserSubscription.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			
			UserSubscription userSubscription = (UserSubscription) target;
			if(userSubscription.getUser() != null && userSubscription.getSubscriptionType() == SubscriptionType.Prepaid) {
				if(userSubscription.getUser().getLastPendingDues() > 0) {
					errors.rejectValue("user", "User has pending dues", "User has pending dues");
				}
			}
			if(userSubscription.getSubscriptionLineItems() == null || userSubscription.getSubscriptionLineItems().isEmpty()) {
				errors.rejectValue("subscriptionLineItems", "Please add at-least one subscription item", "Please add at-least one subscription item");
			}
			validator.validate(userSubscription, errors);
		}
		
	}


