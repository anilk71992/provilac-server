package com.vishwakarma.provilac.mvc.validator;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Payment;
import com.vishwakarma.provilac.service.PaymentService;


	@Component
	public class PaymentValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource 
		private PaymentService paymentService;
		@Override
		public boolean supports(Class<?> clazz) {
			return Payment.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Payment payment = (Payment) target;
			
			Date date = new Date();
			if(payment.getDate().after(date)) {
				errors.rejectValue("date", "Payment Date should not be after current date", "Payment Date should not be after current date");
			}
			if(payment.getPaymentMethod() == null){
				errors.rejectValue("paymentMethod", "Please select Payment Method", "Please select Payment Method");
			}
			if(!payment.isUpdateOperation()){
				Payment existingPayment = paymentService.getPaymentByTxnId(payment.getTxnId(), null, false, false);
				if(null!= existingPayment) {
					if(existingPayment.getTxnId().equals(payment.getTxnId())) {

						errors.rejectValue("txnId", "TransactionId should be unique", "TransactionId should be unique");
					}
				}
			}
			
//			if(null!=payment && payment.getPaymentMethod().toString().equals(PaymentMethod.Cheque.toString())){
//				
//				Payment paymentChequeNumber=paymentService.getPaymentByChequeNumber(payment.getChequeNumber(), false, null, false, false);
//				if(paymentChequeNumber!=null){
//					errors.rejectValue("chequeNumber", "Cheque Number alredy used for other payment", "Cheque Number alredy used for other Payment");
//				}
//			}
			
			validator.validate(payment, errors);
		}
		
	}


