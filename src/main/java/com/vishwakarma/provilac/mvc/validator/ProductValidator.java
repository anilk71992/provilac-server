package com.vishwakarma.provilac.mvc.validator;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.vishwakarma.provilac.model.Product;
import com.vishwakarma.provilac.service.ProductService;


	@Component
	public class ProductValidator implements Validator{

		@Resource(name="mvcValidator")
		private Validator validator;
		
		@Resource
		private ProductService productService;
		
		@Override
		public boolean supports(Class<?> clazz) {
			return Product.class.equals(clazz);
		}

		@Override
		public void validate(Object target, Errors errors) {
			Product product = (Product) target;
			
			Product existingProduct=productService.getProductByNameAndCategory(product.getName().trim(), product.getCategory(), false, null, false, false);
			if (!product.isUpdateOperation()) {
				if ( null != existingProduct) {
						errors.rejectValue("name", "Product  with this name and Category already exists", "Product  with this name  and Category already exists");
					}
			}else{
				if(null != existingProduct && product.getId() != existingProduct.getId()){
					errors.rejectValue("name", "Product  with this name and Category already exists", "Product  with this name  and Category already exists");
				}
			}
			
			validator.validate(product, errors);
		}
		
	}


