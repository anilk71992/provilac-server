package com.vishwakarma.provilac.reports;

import javax.annotation.Resource;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource({"classpath:com/vishwakarma/provilac/db.properties"})
public class ReportScheduler {
	
	@Resource 
	private Environment env;
}
